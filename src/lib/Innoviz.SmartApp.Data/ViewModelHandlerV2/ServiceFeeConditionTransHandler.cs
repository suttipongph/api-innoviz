using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ServiceFeeConditionTransHandler
	{
		#region ServiceFeeConditionTransListView
		public static List<ServiceFeeConditionTransListView> GetServiceFeeConditionTransListViewValidation(this List<ServiceFeeConditionTransListView> serviceFeeConditionTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return serviceFeeConditionTransListViews;
			}
			var result = new List<ServiceFeeConditionTransListView>();
			try
			{
				foreach (ServiceFeeConditionTransListView item in serviceFeeConditionTransListViews)
				{
					result.Add(item.GetServiceFeeConditionTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeConditionTransListView GetServiceFeeConditionTransListViewValidation(this ServiceFeeConditionTransListView serviceFeeConditionTransListView)
		{
			try
			{
				serviceFeeConditionTransListView.RowAuthorize = serviceFeeConditionTransListView.GetListRowAuthorize();
				return serviceFeeConditionTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ServiceFeeConditionTransListView serviceFeeConditionTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ServiceFeeConditionTransListView
		#region ServiceFeeConditionTransItemView
		public static ServiceFeeConditionTransItemView GetServiceFeeConditionTransItemViewValidation(this ServiceFeeConditionTransItemView serviceFeeConditionTransItemView)
		{
			try
			{
				serviceFeeConditionTransItemView.RowAuthorize = serviceFeeConditionTransItemView.GetItemRowAuthorize();
				return serviceFeeConditionTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ServiceFeeConditionTransItemView serviceFeeConditionTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ServiceFeeConditionTrans ToServiceFeeConditionTrans(this ServiceFeeConditionTransItemView serviceFeeConditionTransItemView)
		{
			try
			{
				ServiceFeeConditionTrans serviceFeeConditionTrans = new ServiceFeeConditionTrans();
				serviceFeeConditionTrans.CompanyGUID = serviceFeeConditionTransItemView.CompanyGUID.StringToGuid();
				serviceFeeConditionTrans.CreatedBy = serviceFeeConditionTransItemView.CreatedBy;
				serviceFeeConditionTrans.CreatedDateTime = serviceFeeConditionTransItemView.CreatedDateTime.StringToSystemDateTime();
				serviceFeeConditionTrans.ModifiedBy = serviceFeeConditionTransItemView.ModifiedBy;
				serviceFeeConditionTrans.ModifiedDateTime = serviceFeeConditionTransItemView.ModifiedDateTime.StringToSystemDateTime();
				serviceFeeConditionTrans.Owner = serviceFeeConditionTransItemView.Owner;
				serviceFeeConditionTrans.OwnerBusinessUnitGUID = serviceFeeConditionTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				serviceFeeConditionTrans.ServiceFeeConditionTransGUID = serviceFeeConditionTransItemView.ServiceFeeConditionTransGUID.StringToGuid();
				serviceFeeConditionTrans.AmountBeforeTax = serviceFeeConditionTransItemView.AmountBeforeTax;
				serviceFeeConditionTrans.CreditAppRequestTableGUID = serviceFeeConditionTransItemView.CreditAppRequestTableGUID.StringToGuidNull();
				serviceFeeConditionTrans.Description = serviceFeeConditionTransItemView.Description;
				serviceFeeConditionTrans.Inactive = serviceFeeConditionTransItemView.Inactive;
				serviceFeeConditionTrans.InvoiceRevenueTypeGUID = serviceFeeConditionTransItemView.InvoiceRevenueTypeGUID.StringToGuid();
				serviceFeeConditionTrans.Ordering = serviceFeeConditionTransItemView.Ordering;
				serviceFeeConditionTrans.RefGUID = serviceFeeConditionTransItemView.RefGUID.StringToGuid();
				serviceFeeConditionTrans.RefServiceFeeConditionTransGUID = serviceFeeConditionTransItemView.RefServiceFeeConditionTransGUID.StringToGuidNull();
				serviceFeeConditionTrans.RefType = serviceFeeConditionTransItemView.RefType;
				
				serviceFeeConditionTrans.RowVersion = serviceFeeConditionTransItemView.RowVersion;
				return serviceFeeConditionTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ServiceFeeConditionTrans> ToServiceFeeConditionTrans(this IEnumerable<ServiceFeeConditionTransItemView> serviceFeeConditionTransItemViews)
		{
			try
			{
				List<ServiceFeeConditionTrans> serviceFeeConditionTranss = new List<ServiceFeeConditionTrans>();
				foreach (ServiceFeeConditionTransItemView item in serviceFeeConditionTransItemViews)
				{
					serviceFeeConditionTranss.Add(item.ToServiceFeeConditionTrans());
				}
				return serviceFeeConditionTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeConditionTransItemView ToServiceFeeConditionTransItemView(this ServiceFeeConditionTrans serviceFeeConditionTrans)
		{
			try
			{
				ServiceFeeConditionTransItemView serviceFeeConditionTransItemView = new ServiceFeeConditionTransItemView();
				serviceFeeConditionTransItemView.CompanyGUID = serviceFeeConditionTrans.CompanyGUID.GuidNullToString();
				serviceFeeConditionTransItemView.CreatedBy = serviceFeeConditionTrans.CreatedBy;
				serviceFeeConditionTransItemView.CreatedDateTime = serviceFeeConditionTrans.CreatedDateTime.DateTimeToString();
				serviceFeeConditionTransItemView.ModifiedBy = serviceFeeConditionTrans.ModifiedBy;
				serviceFeeConditionTransItemView.ModifiedDateTime = serviceFeeConditionTrans.ModifiedDateTime.DateTimeToString();
				serviceFeeConditionTransItemView.Owner = serviceFeeConditionTrans.Owner;
				serviceFeeConditionTransItemView.OwnerBusinessUnitGUID = serviceFeeConditionTrans.OwnerBusinessUnitGUID.GuidNullToString();
				serviceFeeConditionTransItemView.ServiceFeeConditionTransGUID = serviceFeeConditionTrans.ServiceFeeConditionTransGUID.GuidNullToString();
				serviceFeeConditionTransItemView.AmountBeforeTax = serviceFeeConditionTrans.AmountBeforeTax;
				serviceFeeConditionTransItemView.CreditAppRequestTableGUID = serviceFeeConditionTrans.CreditAppRequestTableGUID.GuidNullToString();
				serviceFeeConditionTransItemView.Description = serviceFeeConditionTrans.Description;
				serviceFeeConditionTransItemView.Inactive = serviceFeeConditionTrans.Inactive;
				serviceFeeConditionTransItemView.InvoiceRevenueTypeGUID = serviceFeeConditionTrans.InvoiceRevenueTypeGUID.GuidNullToString();
				serviceFeeConditionTransItemView.Ordering = serviceFeeConditionTrans.Ordering;
				serviceFeeConditionTransItemView.RefGUID = serviceFeeConditionTrans.RefGUID.GuidNullToString();
				serviceFeeConditionTransItemView.RefServiceFeeConditionTransGUID = serviceFeeConditionTrans.RefServiceFeeConditionTransGUID.GuidNullToString();
				serviceFeeConditionTransItemView.RefType = serviceFeeConditionTrans.RefType;
				
				serviceFeeConditionTransItemView.RowVersion = serviceFeeConditionTrans.RowVersion;
				return serviceFeeConditionTransItemView.GetServiceFeeConditionTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ServiceFeeConditionTransItemView
		#region ToDropDown
		public static SelectItem<ServiceFeeConditionTransItemView> ToDropDownItem(this ServiceFeeConditionTransItemView serviceFeeConditionTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ServiceFeeConditionTransItemView> selectItem = new SelectItem<ServiceFeeConditionTransItemView>();
				selectItem.Label = null;
				selectItem.Value = serviceFeeConditionTransView.ServiceFeeConditionTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: serviceFeeConditionTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ServiceFeeConditionTransItemView>> ToDropDownItem(this IEnumerable<ServiceFeeConditionTransItemView> serviceFeeConditionTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ServiceFeeConditionTransItemView>> selectItems = new List<SelectItem<ServiceFeeConditionTransItemView>>();
				foreach (ServiceFeeConditionTransItemView item in serviceFeeConditionTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

