using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AddressTransHandler
	{
		#region AddressTransListView
		public static List<AddressTransListView> GetAddressTransListViewValidation(this List<AddressTransListView> addressTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return addressTransListViews;
			}
			var result = new List<AddressTransListView>();
			try
			{
				foreach (AddressTransListView item in addressTransListViews)
				{
					result.Add(item.GetAddressTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressTransListView GetAddressTransListViewValidation(this AddressTransListView addressTransListView)
		{
			try
			{
				addressTransListView.RowAuthorize = addressTransListView.GetListRowAuthorize();
				return addressTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AddressTransListView addressTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AddressTransListView
		#region AddressTransItemView
		public static AddressTransItemView GetAddressTransItemViewValidation(this AddressTransItemView addressTransItemView)
		{
			try
			{
				addressTransItemView.RowAuthorize = addressTransItemView.GetItemRowAuthorize();
				return addressTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AddressTransItemView addressTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AddressTrans ToAddressTrans(this AddressTransItemView addressTransItemView)
		{
			try
			{
				AddressTrans addressTrans = new AddressTrans();
				addressTrans.CompanyGUID = addressTransItemView.CompanyGUID.StringToGuid();
				addressTrans.CreatedBy = addressTransItemView.CreatedBy;
				addressTrans.CreatedDateTime = addressTransItemView.CreatedDateTime.StringToSystemDateTime();
				addressTrans.ModifiedBy = addressTransItemView.ModifiedBy;
				addressTrans.ModifiedDateTime = addressTransItemView.ModifiedDateTime.StringToSystemDateTime();
				addressTrans.Owner = addressTransItemView.Owner;
				addressTrans.OwnerBusinessUnitGUID = addressTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				addressTrans.AddressTransGUID = addressTransItemView.AddressTransGUID.StringToGuid();
				addressTrans.Address1 = addressTransItemView.Address1;
				addressTrans.Address2 = addressTransItemView.Address2;
				addressTrans.AddressCountryGUID = addressTransItemView.AddressCountryGUID.StringToGuid();
				addressTrans.AddressDistrictGUID = addressTransItemView.AddressDistrictGUID.StringToGuid();
				addressTrans.AddressPostalCodeGUID = addressTransItemView.AddressPostalCodeGUID.StringToGuid();
				addressTrans.AddressProvinceGUID = addressTransItemView.AddressProvinceGUID.StringToGuid();
				addressTrans.AddressSubDistrictGUID = addressTransItemView.AddressSubDistrictGUID.StringToGuid();
				addressTrans.CurrentAddress = addressTransItemView.CurrentAddress;
				addressTrans.IsTax = addressTransItemView.IsTax;
				addressTrans.Name = addressTransItemView.Name;
				addressTrans.OwnershipGUID = addressTransItemView.OwnershipGUID.StringToGuidNull();
				addressTrans.Primary = addressTransItemView.Primary;
				addressTrans.PropertyTypeGUID = addressTransItemView.PropertyTypeGUID.StringToGuidNull();
				addressTrans.RefGUID = addressTransItemView.RefGUID.StringToGuidNull();
				addressTrans.RefType = addressTransItemView.RefType;
				addressTrans.TaxBranchId = addressTransItemView.TaxBranchId;
				addressTrans.TaxBranchName = addressTransItemView.TaxBranchName;
				addressTrans.AltAddress = addressTransItemView.AltAddress;
				
				addressTrans.RowVersion = addressTransItemView.RowVersion;
				return addressTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AddressTrans> ToAddressTrans(this IEnumerable<AddressTransItemView> addressTransItemViews)
		{
			try
			{
				List<AddressTrans> addressTranss = new List<AddressTrans>();
				foreach (AddressTransItemView item in addressTransItemViews)
				{
					addressTranss.Add(item.ToAddressTrans());
				}
				return addressTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressTransItemView ToAddressTransItemView(this AddressTrans addressTrans)
		{
			try
			{
				AddressTransItemView addressTransItemView = new AddressTransItemView();
				addressTransItemView.CompanyGUID = addressTrans.CompanyGUID.GuidNullToString();
				addressTransItemView.CreatedBy = addressTrans.CreatedBy;
				addressTransItemView.CreatedDateTime = addressTrans.CreatedDateTime.DateTimeToString();
				addressTransItemView.ModifiedBy = addressTrans.ModifiedBy;
				addressTransItemView.ModifiedDateTime = addressTrans.ModifiedDateTime.DateTimeToString();
				addressTransItemView.Owner = addressTrans.Owner;
				addressTransItemView.OwnerBusinessUnitGUID = addressTrans.OwnerBusinessUnitGUID.GuidNullToString();
				addressTransItemView.AddressTransGUID = addressTrans.AddressTransGUID.GuidNullToString();
				addressTransItemView.Address1 = addressTrans.Address1;
				addressTransItemView.Address2 = addressTrans.Address2;
				addressTransItemView.AddressCountryGUID = addressTrans.AddressCountryGUID.GuidNullToString();
				addressTransItemView.AddressDistrictGUID = addressTrans.AddressDistrictGUID.GuidNullToString();
				addressTransItemView.AddressPostalCodeGUID = addressTrans.AddressPostalCodeGUID.GuidNullToString();
				addressTransItemView.AddressProvinceGUID = addressTrans.AddressProvinceGUID.GuidNullToString();
				addressTransItemView.AddressSubDistrictGUID = addressTrans.AddressSubDistrictGUID.GuidNullToString();
				addressTransItemView.CurrentAddress = addressTrans.CurrentAddress;
				addressTransItemView.IsTax = addressTrans.IsTax;
				addressTransItemView.Name = addressTrans.Name;
				addressTransItemView.OwnershipGUID = addressTrans.OwnershipGUID.GuidNullToString();
				addressTransItemView.Primary = addressTrans.Primary;
				addressTransItemView.PropertyTypeGUID = addressTrans.PropertyTypeGUID.GuidNullToString();
				addressTransItemView.RefGUID = addressTrans.RefGUID.GuidNullToString();
				addressTransItemView.RefType = addressTrans.RefType;
				addressTransItemView.TaxBranchId = addressTrans.TaxBranchId;
				addressTransItemView.TaxBranchName = addressTrans.TaxBranchName;
				addressTransItemView.AltAddress = addressTrans.AltAddress;
				
				addressTransItemView.RowVersion = addressTrans.RowVersion;
				return addressTransItemView.GetAddressTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AddressTransItemView
		#region ToDropDown
		public static SelectItem<AddressTransItemView> ToDropDownItem(this AddressTransItemView addressTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AddressTransItemView> selectItem = new SelectItem<AddressTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(addressTransView.Name, addressTransView.Address1);
				selectItem.Value = addressTransView.AddressTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: addressTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AddressTransItemView>> ToDropDownItem(this IEnumerable<AddressTransItemView> addressTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AddressTransItemView>> selectItems = new List<SelectItem<AddressTransItemView>>();
				foreach (AddressTransItemView item in addressTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

