using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ServiceFeeCondTemplateTableHandler
	{
		#region ServiceFeeCondTemplateTableListView
		public static List<ServiceFeeCondTemplateTableListView> GetServiceFeeCondTemplateTableListViewValidation(this List<ServiceFeeCondTemplateTableListView> serviceFeeCondTemplateTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return serviceFeeCondTemplateTableListViews;
			}
			var result = new List<ServiceFeeCondTemplateTableListView>();
			try
			{
				foreach (ServiceFeeCondTemplateTableListView item in serviceFeeCondTemplateTableListViews)
				{
					result.Add(item.GetServiceFeeCondTemplateTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeCondTemplateTableListView GetServiceFeeCondTemplateTableListViewValidation(this ServiceFeeCondTemplateTableListView serviceFeeCondTemplateTableListView)
		{
			try
			{
				serviceFeeCondTemplateTableListView.RowAuthorize = serviceFeeCondTemplateTableListView.GetListRowAuthorize();
				return serviceFeeCondTemplateTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ServiceFeeCondTemplateTableListView serviceFeeCondTemplateTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ServiceFeeCondTemplateTableListView
		#region ServiceFeeCondTemplateTableItemView
		public static ServiceFeeCondTemplateTableItemView GetServiceFeeCondTemplateTableItemViewValidation(this ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableItemView)
		{
			try
			{
				serviceFeeCondTemplateTableItemView.RowAuthorize = serviceFeeCondTemplateTableItemView.GetItemRowAuthorize();
				return serviceFeeCondTemplateTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ServiceFeeCondTemplateTable ToServiceFeeCondTemplateTable(this ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableItemView)
		{
			try
			{
				ServiceFeeCondTemplateTable serviceFeeCondTemplateTable = new ServiceFeeCondTemplateTable();
				serviceFeeCondTemplateTable.CompanyGUID = serviceFeeCondTemplateTableItemView.CompanyGUID.StringToGuid();
				serviceFeeCondTemplateTable.CreatedBy = serviceFeeCondTemplateTableItemView.CreatedBy;
				serviceFeeCondTemplateTable.CreatedDateTime = serviceFeeCondTemplateTableItemView.CreatedDateTime.StringToSystemDateTime();
				serviceFeeCondTemplateTable.ModifiedBy = serviceFeeCondTemplateTableItemView.ModifiedBy;
				serviceFeeCondTemplateTable.ModifiedDateTime = serviceFeeCondTemplateTableItemView.ModifiedDateTime.StringToSystemDateTime();
				serviceFeeCondTemplateTable.Owner = serviceFeeCondTemplateTableItemView.Owner;
				serviceFeeCondTemplateTable.OwnerBusinessUnitGUID = serviceFeeCondTemplateTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateTableItemView.ServiceFeeCondTemplateTableGUID.StringToGuid();
				serviceFeeCondTemplateTable.Description = serviceFeeCondTemplateTableItemView.Description;
				serviceFeeCondTemplateTable.ProductType = serviceFeeCondTemplateTableItemView.ProductType;
				serviceFeeCondTemplateTable.ServiceFeeCondTemplateId = serviceFeeCondTemplateTableItemView.ServiceFeeCondTemplateId;
				
				serviceFeeCondTemplateTable.RowVersion = serviceFeeCondTemplateTableItemView.RowVersion;
				return serviceFeeCondTemplateTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ServiceFeeCondTemplateTable> ToServiceFeeCondTemplateTable(this IEnumerable<ServiceFeeCondTemplateTableItemView> serviceFeeCondTemplateTableItemViews)
		{
			try
			{
				List<ServiceFeeCondTemplateTable> serviceFeeCondTemplateTables = new List<ServiceFeeCondTemplateTable>();
				foreach (ServiceFeeCondTemplateTableItemView item in serviceFeeCondTemplateTableItemViews)
				{
					serviceFeeCondTemplateTables.Add(item.ToServiceFeeCondTemplateTable());
				}
				return serviceFeeCondTemplateTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ServiceFeeCondTemplateTableItemView ToServiceFeeCondTemplateTableItemView(this ServiceFeeCondTemplateTable serviceFeeCondTemplateTable)
		{
			try
			{
				ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableItemView = new ServiceFeeCondTemplateTableItemView();
				serviceFeeCondTemplateTableItemView.CompanyGUID = serviceFeeCondTemplateTable.CompanyGUID.GuidNullToString();
				serviceFeeCondTemplateTableItemView.CreatedBy = serviceFeeCondTemplateTable.CreatedBy;
				serviceFeeCondTemplateTableItemView.CreatedDateTime = serviceFeeCondTemplateTable.CreatedDateTime.DateTimeToString();
				serviceFeeCondTemplateTableItemView.ModifiedBy = serviceFeeCondTemplateTable.ModifiedBy;
				serviceFeeCondTemplateTableItemView.ModifiedDateTime = serviceFeeCondTemplateTable.ModifiedDateTime.DateTimeToString();
				serviceFeeCondTemplateTableItemView.Owner = serviceFeeCondTemplateTable.Owner;
				serviceFeeCondTemplateTableItemView.OwnerBusinessUnitGUID = serviceFeeCondTemplateTable.OwnerBusinessUnitGUID.GuidNullToString();
				serviceFeeCondTemplateTableItemView.ServiceFeeCondTemplateTableGUID = serviceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID.GuidNullToString();
				serviceFeeCondTemplateTableItemView.Description = serviceFeeCondTemplateTable.Description;
				serviceFeeCondTemplateTableItemView.ProductType = serviceFeeCondTemplateTable.ProductType;
				serviceFeeCondTemplateTableItemView.ServiceFeeCondTemplateId = serviceFeeCondTemplateTable.ServiceFeeCondTemplateId;
				
				serviceFeeCondTemplateTableItemView.RowVersion = serviceFeeCondTemplateTable.RowVersion;
				return serviceFeeCondTemplateTableItemView.GetServiceFeeCondTemplateTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ServiceFeeCondTemplateTableItemView
		#region ToDropDown
		public static SelectItem<ServiceFeeCondTemplateTableItemView> ToDropDownItem(this ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ServiceFeeCondTemplateTableItemView> selectItem = new SelectItem<ServiceFeeCondTemplateTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(serviceFeeCondTemplateTableView.ServiceFeeCondTemplateId, serviceFeeCondTemplateTableView.Description);
				selectItem.Value = serviceFeeCondTemplateTableView.ServiceFeeCondTemplateTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: serviceFeeCondTemplateTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ServiceFeeCondTemplateTableItemView>> ToDropDownItem(this IEnumerable<ServiceFeeCondTemplateTableItemView> serviceFeeCondTemplateTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ServiceFeeCondTemplateTableItemView>> selectItems = new List<SelectItem<ServiceFeeCondTemplateTableItemView>>();
				foreach (ServiceFeeCondTemplateTableItemView item in serviceFeeCondTemplateTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

