using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditScoringHandler
	{
		#region CreditScoringListView
		public static List<CreditScoringListView> GetCreditScoringListViewValidation(this List<CreditScoringListView> creditScoringListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditScoringListViews;
			}
			var result = new List<CreditScoringListView>();
			try
			{
				foreach (CreditScoringListView item in creditScoringListViews)
				{
					result.Add(item.GetCreditScoringListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditScoringListView GetCreditScoringListViewValidation(this CreditScoringListView creditScoringListView)
		{
			try
			{
				creditScoringListView.RowAuthorize = creditScoringListView.GetListRowAuthorize();
				return creditScoringListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditScoringListView creditScoringListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditScoringListView
		#region CreditScoringItemView
		public static CreditScoringItemView GetCreditScoringItemViewValidation(this CreditScoringItemView creditScoringItemView)
		{
			try
			{
				creditScoringItemView.RowAuthorize = creditScoringItemView.GetItemRowAuthorize();
				return creditScoringItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditScoringItemView creditScoringItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditScoring ToCreditScoring(this CreditScoringItemView creditScoringItemView)
		{
			try
			{
				CreditScoring creditScoring = new CreditScoring();
				creditScoring.CompanyGUID = creditScoringItemView.CompanyGUID.StringToGuid();
				creditScoring.CreatedBy = creditScoringItemView.CreatedBy;
				creditScoring.CreatedDateTime = creditScoringItemView.CreatedDateTime.StringToSystemDateTime();
				creditScoring.ModifiedBy = creditScoringItemView.ModifiedBy;
				creditScoring.ModifiedDateTime = creditScoringItemView.ModifiedDateTime.StringToSystemDateTime();
				creditScoring.Owner = creditScoringItemView.Owner;
				creditScoring.OwnerBusinessUnitGUID = creditScoringItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditScoring.CreditScoringGUID = creditScoringItemView.CreditScoringGUID.StringToGuid();
				creditScoring.CreditScoringId = creditScoringItemView.CreditScoringId;
				creditScoring.Description = creditScoringItemView.Description;
				
				creditScoring.RowVersion = creditScoringItemView.RowVersion;
				return creditScoring;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditScoring> ToCreditScoring(this IEnumerable<CreditScoringItemView> creditScoringItemViews)
		{
			try
			{
				List<CreditScoring> creditScorings = new List<CreditScoring>();
				foreach (CreditScoringItemView item in creditScoringItemViews)
				{
					creditScorings.Add(item.ToCreditScoring());
				}
				return creditScorings;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditScoringItemView ToCreditScoringItemView(this CreditScoring creditScoring)
		{
			try
			{
				CreditScoringItemView creditScoringItemView = new CreditScoringItemView();
				creditScoringItemView.CompanyGUID = creditScoring.CompanyGUID.GuidNullToString();
				creditScoringItemView.CreatedBy = creditScoring.CreatedBy;
				creditScoringItemView.CreatedDateTime = creditScoring.CreatedDateTime.DateTimeToString();
				creditScoringItemView.ModifiedBy = creditScoring.ModifiedBy;
				creditScoringItemView.ModifiedDateTime = creditScoring.ModifiedDateTime.DateTimeToString();
				creditScoringItemView.Owner = creditScoring.Owner;
				creditScoringItemView.OwnerBusinessUnitGUID = creditScoring.OwnerBusinessUnitGUID.GuidNullToString();
				creditScoringItemView.CreditScoringGUID = creditScoring.CreditScoringGUID.GuidNullToString();
				creditScoringItemView.CreditScoringId = creditScoring.CreditScoringId;
				creditScoringItemView.Description = creditScoring.Description;
				
				creditScoringItemView.RowVersion = creditScoring.RowVersion;
				return creditScoringItemView.GetCreditScoringItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditScoringItemView
		#region ToDropDown
		public static SelectItem<CreditScoringItemView> ToDropDownItem(this CreditScoringItemView creditScoringView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditScoringItemView> selectItem = new SelectItem<CreditScoringItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(creditScoringView.CreditScoringId, creditScoringView.Description);
				selectItem.Value = creditScoringView.CreditScoringGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditScoringView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditScoringItemView>> ToDropDownItem(this IEnumerable<CreditScoringItemView> creditScoringItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditScoringItemView>> selectItems = new List<SelectItem<CreditScoringItemView>>();
				foreach (CreditScoringItemView item in creditScoringItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

