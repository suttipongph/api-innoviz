using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AddressSubDistrictHandler
	{
		#region AddressSubDistrictListView
		public static List<AddressSubDistrictListView> GetAddressSubDistrictListViewValidation(this List<AddressSubDistrictListView> addressSubDistrictListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return addressSubDistrictListViews;
			}
			var result = new List<AddressSubDistrictListView>();
			try
			{
				foreach (AddressSubDistrictListView item in addressSubDistrictListViews)
				{
					result.Add(item.GetAddressSubDistrictListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressSubDistrictListView GetAddressSubDistrictListViewValidation(this AddressSubDistrictListView addressSubDistrictListView)
		{
			try
			{
				addressSubDistrictListView.RowAuthorize = addressSubDistrictListView.GetListRowAuthorize();
				return addressSubDistrictListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AddressSubDistrictListView addressSubDistrictListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AddressSubDistrictListView
		#region AddressSubDistrictItemView
		public static AddressSubDistrictItemView GetAddressSubDistrictItemViewValidation(this AddressSubDistrictItemView addressSubDistrictItemView)
		{
			try
			{
				addressSubDistrictItemView.RowAuthorize = addressSubDistrictItemView.GetItemRowAuthorize();
				return addressSubDistrictItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AddressSubDistrictItemView addressSubDistrictItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AddressSubDistrict ToAddressSubDistrict(this AddressSubDistrictItemView addressSubDistrictItemView)
		{
			try
			{
				AddressSubDistrict addressSubDistrict = new AddressSubDistrict();
				addressSubDistrict.CompanyGUID = addressSubDistrictItemView.CompanyGUID.StringToGuid();
				addressSubDistrict.CreatedBy = addressSubDistrictItemView.CreatedBy;
				addressSubDistrict.CreatedDateTime = addressSubDistrictItemView.CreatedDateTime.StringToSystemDateTime();
				addressSubDistrict.ModifiedBy = addressSubDistrictItemView.ModifiedBy;
				addressSubDistrict.ModifiedDateTime = addressSubDistrictItemView.ModifiedDateTime.StringToSystemDateTime();
				addressSubDistrict.Owner = addressSubDistrictItemView.Owner;
				addressSubDistrict.OwnerBusinessUnitGUID = addressSubDistrictItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				addressSubDistrict.AddressSubDistrictGUID = addressSubDistrictItemView.AddressSubDistrictGUID.StringToGuid();
				addressSubDistrict.AddressDistrictGUID = addressSubDistrictItemView.AddressDistrictGUID.StringToGuid();
				addressSubDistrict.Name = addressSubDistrictItemView.Name;
				addressSubDistrict.SubDistrictId = addressSubDistrictItemView.SubDistrictId;
				
				addressSubDistrict.RowVersion = addressSubDistrictItemView.RowVersion;
				return addressSubDistrict;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AddressSubDistrict> ToAddressSubDistrict(this IEnumerable<AddressSubDistrictItemView> addressSubDistrictItemViews)
		{
			try
			{
				List<AddressSubDistrict> addressSubDistricts = new List<AddressSubDistrict>();
				foreach (AddressSubDistrictItemView item in addressSubDistrictItemViews)
				{
					addressSubDistricts.Add(item.ToAddressSubDistrict());
				}
				return addressSubDistricts;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AddressSubDistrictItemView ToAddressSubDistrictItemView(this AddressSubDistrict addressSubDistrict)
		{
			try
			{
				AddressSubDistrictItemView addressSubDistrictItemView = new AddressSubDistrictItemView();
				addressSubDistrictItemView.CompanyGUID = addressSubDistrict.CompanyGUID.GuidNullToString();
				addressSubDistrictItemView.CreatedBy = addressSubDistrict.CreatedBy;
				addressSubDistrictItemView.CreatedDateTime = addressSubDistrict.CreatedDateTime.DateTimeToString();
				addressSubDistrictItemView.ModifiedBy = addressSubDistrict.ModifiedBy;
				addressSubDistrictItemView.ModifiedDateTime = addressSubDistrict.ModifiedDateTime.DateTimeToString();
				addressSubDistrictItemView.Owner = addressSubDistrict.Owner;
				addressSubDistrictItemView.OwnerBusinessUnitGUID = addressSubDistrict.OwnerBusinessUnitGUID.GuidNullToString();
				addressSubDistrictItemView.AddressSubDistrictGUID = addressSubDistrict.AddressSubDistrictGUID.GuidNullToString();
				addressSubDistrictItemView.AddressDistrictGUID = addressSubDistrict.AddressDistrictGUID.GuidNullToString();
				addressSubDistrictItemView.Name = addressSubDistrict.Name;
				addressSubDistrictItemView.SubDistrictId = addressSubDistrict.SubDistrictId;
				
				addressSubDistrictItemView.RowVersion = addressSubDistrict.RowVersion;
				return addressSubDistrictItemView.GetAddressSubDistrictItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AddressSubDistrictItemView
		#region ToDropDown
		public static SelectItem<AddressSubDistrictItemView> ToDropDownItem(this AddressSubDistrictItemView addressSubDistrictView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AddressSubDistrictItemView> selectItem = new SelectItem<AddressSubDistrictItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(addressSubDistrictView.SubDistrictId, addressSubDistrictView.Name);
				selectItem.Value = addressSubDistrictView.AddressSubDistrictGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: addressSubDistrictView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AddressSubDistrictItemView>> ToDropDownItem(this IEnumerable<AddressSubDistrictItemView> addressSubDistrictItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AddressSubDistrictItemView>> selectItems = new List<SelectItem<AddressSubDistrictItemView>>();
				foreach (AddressSubDistrictItemView item in addressSubDistrictItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

