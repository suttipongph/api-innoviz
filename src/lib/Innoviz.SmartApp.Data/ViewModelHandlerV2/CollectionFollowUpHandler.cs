using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CollectionFollowUpHandler
	{
		#region CollectionFollowUpListView
		public static List<CollectionFollowUpListView> GetCollectionFollowUpListViewValidation(this List<CollectionFollowUpListView> collectionFollowUpListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return collectionFollowUpListViews;
			}
			var result = new List<CollectionFollowUpListView>();
			try
			{
				foreach (CollectionFollowUpListView item in collectionFollowUpListViews)
				{
					result.Add(item.GetCollectionFollowUpListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CollectionFollowUpListView GetCollectionFollowUpListViewValidation(this CollectionFollowUpListView collectionFollowUpListView)
		{
			try
			{
				collectionFollowUpListView.RowAuthorize = collectionFollowUpListView.GetListRowAuthorize();
				return collectionFollowUpListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CollectionFollowUpListView collectionFollowUpListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CollectionFollowUpListView
		#region CollectionFollowUpItemView
		public static CollectionFollowUpItemView GetCollectionFollowUpItemViewValidation(this CollectionFollowUpItemView collectionFollowUpItemView)
		{
			try
			{
				collectionFollowUpItemView.RowAuthorize = collectionFollowUpItemView.GetItemRowAuthorize();
				return collectionFollowUpItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CollectionFollowUpItemView collectionFollowUpItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CollectionFollowUp ToCollectionFollowUp(this CollectionFollowUpItemView collectionFollowUpItemView)
		{
			try
			{
				CollectionFollowUp collectionFollowUp = new CollectionFollowUp();
				collectionFollowUp.CompanyGUID = collectionFollowUpItemView.CompanyGUID.StringToGuid();
				collectionFollowUp.CreatedBy = collectionFollowUpItemView.CreatedBy;
				collectionFollowUp.CreatedDateTime = collectionFollowUpItemView.CreatedDateTime.StringToSystemDateTime();
				collectionFollowUp.ModifiedBy = collectionFollowUpItemView.ModifiedBy;
				collectionFollowUp.ModifiedDateTime = collectionFollowUpItemView.ModifiedDateTime.StringToSystemDateTime();
				collectionFollowUp.Owner = collectionFollowUpItemView.Owner;
				collectionFollowUp.OwnerBusinessUnitGUID = collectionFollowUpItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				collectionFollowUp.CollectionFollowUpGUID = collectionFollowUpItemView.CollectionFollowUpGUID.StringToGuid();
				collectionFollowUp.BuyerTableGUID = collectionFollowUpItemView.BuyerTableGUID.StringToGuid();
				collectionFollowUp.ChequeAmount = collectionFollowUpItemView.ChequeAmount;
				collectionFollowUp.ChequeBankAccNo = collectionFollowUpItemView.ChequeBankAccNo;
				collectionFollowUp.ChequeBankGroupGUID = collectionFollowUpItemView.ChequeBankGroupGUID.StringToGuidNull();
				collectionFollowUp.ChequeBranch = collectionFollowUpItemView.ChequeBranch;
				collectionFollowUp.ChequeCollectionResult = collectionFollowUpItemView.ChequeCollectionResult;
				collectionFollowUp.ChequeDate = collectionFollowUpItemView.ChequeDate.StringNullToDateNull();
				collectionFollowUp.ChequeNo = collectionFollowUpItemView.ChequeNo;
				collectionFollowUp.CollectionAmount = collectionFollowUpItemView.CollectionAmount;
				collectionFollowUp.CollectionDate = collectionFollowUpItemView.CollectionDate.StringToDate();
				collectionFollowUp.CollectionFollowUpResult = collectionFollowUpItemView.CollectionFollowUpResult;
				collectionFollowUp.CreditAppLineGUID = collectionFollowUpItemView.CreditAppLineGUID.StringToGuidNull();
				collectionFollowUp.CustomerTableGUID = collectionFollowUpItemView.CustomerTableGUID.StringToGuid();
				collectionFollowUp.Description = collectionFollowUpItemView.Description;
				collectionFollowUp.DocumentId = collectionFollowUpItemView.DocumentId;
				collectionFollowUp.MethodOfPaymentGUID = collectionFollowUpItemView.MethodOfPaymentGUID.StringToGuid();
				collectionFollowUp.NewCollectionDate = collectionFollowUpItemView.NewCollectionDate.StringNullToDateNull();
				collectionFollowUp.ReceivedFrom = collectionFollowUpItemView.ReceivedFrom;
				collectionFollowUp.RecipientName = collectionFollowUpItemView.RecipientName;
				collectionFollowUp.RefGUID = collectionFollowUpItemView.RefGUID.StringToGuidNull();
				collectionFollowUp.RefType = collectionFollowUpItemView.RefType;
				collectionFollowUp.Remark = collectionFollowUpItemView.Remark;
				
				collectionFollowUp.RowVersion = collectionFollowUpItemView.RowVersion;
				return collectionFollowUp;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CollectionFollowUp> ToCollectionFollowUp(this IEnumerable<CollectionFollowUpItemView> collectionFollowUpItemViews)
		{
			try
			{
				List<CollectionFollowUp> collectionFollowUps = new List<CollectionFollowUp>();
				foreach (CollectionFollowUpItemView item in collectionFollowUpItemViews)
				{
					collectionFollowUps.Add(item.ToCollectionFollowUp());
				}
				return collectionFollowUps;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CollectionFollowUpItemView ToCollectionFollowUpItemView(this CollectionFollowUp collectionFollowUp)
		{
			try
			{
				CollectionFollowUpItemView collectionFollowUpItemView = new CollectionFollowUpItemView();
				collectionFollowUpItemView.CompanyGUID = collectionFollowUp.CompanyGUID.GuidNullToString();
				collectionFollowUpItemView.CreatedBy = collectionFollowUp.CreatedBy;
				collectionFollowUpItemView.CreatedDateTime = collectionFollowUp.CreatedDateTime.DateTimeToString();
				collectionFollowUpItemView.ModifiedBy = collectionFollowUp.ModifiedBy;
				collectionFollowUpItemView.ModifiedDateTime = collectionFollowUp.ModifiedDateTime.DateTimeToString();
				collectionFollowUpItemView.Owner = collectionFollowUp.Owner;
				collectionFollowUpItemView.OwnerBusinessUnitGUID = collectionFollowUp.OwnerBusinessUnitGUID.GuidNullToString();
				collectionFollowUpItemView.CollectionFollowUpGUID = collectionFollowUp.CollectionFollowUpGUID.GuidNullToString();
				collectionFollowUpItemView.BuyerTableGUID = collectionFollowUp.BuyerTableGUID.GuidNullToString();
				collectionFollowUpItemView.ChequeAmount = collectionFollowUp.ChequeAmount;
				collectionFollowUpItemView.ChequeBankAccNo = collectionFollowUp.ChequeBankAccNo;
				collectionFollowUpItemView.ChequeBankGroupGUID = collectionFollowUp.ChequeBankGroupGUID.GuidNullToString();
				collectionFollowUpItemView.ChequeBranch = collectionFollowUp.ChequeBranch;
				collectionFollowUpItemView.ChequeCollectionResult = collectionFollowUp.ChequeCollectionResult;
				collectionFollowUpItemView.ChequeDate = collectionFollowUp.ChequeDate.DateNullToString();
				collectionFollowUpItemView.ChequeNo = collectionFollowUp.ChequeNo;
				collectionFollowUpItemView.CollectionAmount = collectionFollowUp.CollectionAmount;
				collectionFollowUpItemView.CollectionDate = collectionFollowUp.CollectionDate.DateToString();
				collectionFollowUpItemView.CollectionFollowUpResult = collectionFollowUp.CollectionFollowUpResult;
				collectionFollowUpItemView.CreditAppLineGUID = collectionFollowUp.CreditAppLineGUID.GuidNullToString();
				collectionFollowUpItemView.CustomerTableGUID = collectionFollowUp.CustomerTableGUID.GuidNullToString();
				collectionFollowUpItemView.Description = collectionFollowUp.Description;
				collectionFollowUpItemView.DocumentId = collectionFollowUp.DocumentId;
				collectionFollowUpItemView.MethodOfPaymentGUID = collectionFollowUp.MethodOfPaymentGUID.GuidNullToString();
				collectionFollowUpItemView.NewCollectionDate = collectionFollowUp.NewCollectionDate.DateNullToString();
				collectionFollowUpItemView.ReceivedFrom = collectionFollowUp.ReceivedFrom;
				collectionFollowUpItemView.RecipientName = collectionFollowUp.RecipientName;
				collectionFollowUpItemView.RefGUID = collectionFollowUp.RefGUID.GuidNullToString();
				collectionFollowUpItemView.RefType = collectionFollowUp.RefType;
				collectionFollowUpItemView.Remark = collectionFollowUp.Remark;
				
				collectionFollowUpItemView.RowVersion = collectionFollowUp.RowVersion;
				return collectionFollowUpItemView.GetCollectionFollowUpItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CollectionFollowUpItemView
		#region ToDropDown
		public static SelectItem<CollectionFollowUpItemView> ToDropDownItem(this CollectionFollowUpItemView collectionFollowUpView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CollectionFollowUpItemView> selectItem = new SelectItem<CollectionFollowUpItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(collectionFollowUpView.CollectionDate.ToString(), collectionFollowUpView.Description);
				selectItem.Value = collectionFollowUpView.CollectionFollowUpGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: collectionFollowUpView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CollectionFollowUpItemView>> ToDropDownItem(this IEnumerable<CollectionFollowUpItemView> collectionFollowUpItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CollectionFollowUpItemView>> selectItems = new List<SelectItem<CollectionFollowUpItemView>>();
				foreach (CollectionFollowUpItemView item in collectionFollowUpItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

