using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ExposureGroupHandler
	{
		#region ExposureGroupListView
		public static List<ExposureGroupListView> GetExposureGroupListViewValidation(this List<ExposureGroupListView> exposureGroupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return exposureGroupListViews;
			}
			var result = new List<ExposureGroupListView>();
			try
			{
				foreach (ExposureGroupListView item in exposureGroupListViews)
				{
					result.Add(item.GetExposureGroupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ExposureGroupListView GetExposureGroupListViewValidation(this ExposureGroupListView exposureGroupListView)
		{
			try
			{
				exposureGroupListView.RowAuthorize = exposureGroupListView.GetListRowAuthorize();
				return exposureGroupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ExposureGroupListView exposureGroupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ExposureGroupListView
		#region ExposureGroupItemView
		public static ExposureGroupItemView GetExposureGroupItemViewValidation(this ExposureGroupItemView exposureGroupItemView)
		{
			try
			{
				exposureGroupItemView.RowAuthorize = exposureGroupItemView.GetItemRowAuthorize();
				return exposureGroupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ExposureGroupItemView exposureGroupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ExposureGroup ToExposureGroup(this ExposureGroupItemView exposureGroupItemView)
		{
			try
			{
				ExposureGroup exposureGroup = new ExposureGroup();
				exposureGroup.CompanyGUID = exposureGroupItemView.CompanyGUID.StringToGuid();
				exposureGroup.CreatedBy = exposureGroupItemView.CreatedBy;
				exposureGroup.CreatedDateTime = exposureGroupItemView.CreatedDateTime.StringToSystemDateTime();
				exposureGroup.ModifiedBy = exposureGroupItemView.ModifiedBy;
				exposureGroup.ModifiedDateTime = exposureGroupItemView.ModifiedDateTime.StringToSystemDateTime();
				exposureGroup.Owner = exposureGroupItemView.Owner;
				exposureGroup.OwnerBusinessUnitGUID = exposureGroupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				exposureGroup.ExposureGroupGUID = exposureGroupItemView.ExposureGroupGUID.StringToGuid();
				exposureGroup.Description = exposureGroupItemView.Description;
				exposureGroup.ExposureGroupId = exposureGroupItemView.ExposureGroupId;
				
				exposureGroup.RowVersion = exposureGroupItemView.RowVersion;
				return exposureGroup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ExposureGroup> ToExposureGroup(this IEnumerable<ExposureGroupItemView> exposureGroupItemViews)
		{
			try
			{
				List<ExposureGroup> exposureGroups = new List<ExposureGroup>();
				foreach (ExposureGroupItemView item in exposureGroupItemViews)
				{
					exposureGroups.Add(item.ToExposureGroup());
				}
				return exposureGroups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ExposureGroupItemView ToExposureGroupItemView(this ExposureGroup exposureGroup)
		{
			try
			{
				ExposureGroupItemView exposureGroupItemView = new ExposureGroupItemView();
				exposureGroupItemView.CompanyGUID = exposureGroup.CompanyGUID.GuidNullToString();
				exposureGroupItemView.CreatedBy = exposureGroup.CreatedBy;
				exposureGroupItemView.CreatedDateTime = exposureGroup.CreatedDateTime.DateTimeToString();
				exposureGroupItemView.ModifiedBy = exposureGroup.ModifiedBy;
				exposureGroupItemView.ModifiedDateTime = exposureGroup.ModifiedDateTime.DateTimeToString();
				exposureGroupItemView.Owner = exposureGroup.Owner;
				exposureGroupItemView.OwnerBusinessUnitGUID = exposureGroup.OwnerBusinessUnitGUID.GuidNullToString();
				exposureGroupItemView.ExposureGroupGUID = exposureGroup.ExposureGroupGUID.GuidNullToString();
				exposureGroupItemView.Description = exposureGroup.Description;
				exposureGroupItemView.ExposureGroupId = exposureGroup.ExposureGroupId;
				
				exposureGroupItemView.RowVersion = exposureGroup.RowVersion;
				return exposureGroupItemView.GetExposureGroupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ExposureGroupItemView
		#region ToDropDown
		public static SelectItem<ExposureGroupItemView> ToDropDownItem(this ExposureGroupItemView exposureGroupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ExposureGroupItemView> selectItem = new SelectItem<ExposureGroupItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(exposureGroupView.ExposureGroupId, exposureGroupView.Description);
				selectItem.Value = exposureGroupView.ExposureGroupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: exposureGroupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ExposureGroupItemView>> ToDropDownItem(this IEnumerable<ExposureGroupItemView> exposureGroupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ExposureGroupItemView>> selectItems = new List<SelectItem<ExposureGroupItemView>>();
				foreach (ExposureGroupItemView item in exposureGroupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

