using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class InvoiceNumberSeqSetupHandler
	{
		#region InvoiceNumberSeqSetupListView
		public static List<InvoiceNumberSeqSetupListView> GetInvoiceNumberSeqSetupListViewValidation(this List<InvoiceNumberSeqSetupListView> invoiceNumberSeqSetupListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return invoiceNumberSeqSetupListViews;
			}
			var result = new List<InvoiceNumberSeqSetupListView>();
			try
			{
				foreach (InvoiceNumberSeqSetupListView item in invoiceNumberSeqSetupListViews)
				{
					result.Add(item.GetInvoiceNumberSeqSetupListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceNumberSeqSetupListView GetInvoiceNumberSeqSetupListViewValidation(this InvoiceNumberSeqSetupListView invoiceNumberSeqSetupListView)
		{
			try
			{
				invoiceNumberSeqSetupListView.RowAuthorize = invoiceNumberSeqSetupListView.GetListRowAuthorize();
				return invoiceNumberSeqSetupListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this InvoiceNumberSeqSetupListView invoiceNumberSeqSetupListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion InvoiceNumberSeqSetupListView
		#region InvoiceNumberSeqSetupItemView
		public static InvoiceNumberSeqSetupItemView GetInvoiceNumberSeqSetupItemViewValidation(this InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupItemView)
		{
			try
			{
				invoiceNumberSeqSetupItemView.RowAuthorize = invoiceNumberSeqSetupItemView.GetItemRowAuthorize();
				return invoiceNumberSeqSetupItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static InvoiceNumberSeqSetup ToInvoiceNumberSeqSetup(this InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupItemView)
		{
			try
			{
				InvoiceNumberSeqSetup invoiceNumberSeqSetup = new InvoiceNumberSeqSetup();
				invoiceNumberSeqSetup.CompanyGUID = invoiceNumberSeqSetupItemView.CompanyGUID.StringToGuid();
				invoiceNumberSeqSetup.CreatedBy = invoiceNumberSeqSetupItemView.CreatedBy;
				invoiceNumberSeqSetup.CreatedDateTime = invoiceNumberSeqSetupItemView.CreatedDateTime.StringToSystemDateTime();
				invoiceNumberSeqSetup.ModifiedBy = invoiceNumberSeqSetupItemView.ModifiedBy;
				invoiceNumberSeqSetup.ModifiedDateTime = invoiceNumberSeqSetupItemView.ModifiedDateTime.StringToSystemDateTime();
				invoiceNumberSeqSetup.Owner = invoiceNumberSeqSetupItemView.Owner;
				invoiceNumberSeqSetup.OwnerBusinessUnitGUID = invoiceNumberSeqSetupItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				invoiceNumberSeqSetup.InvoiceNumberSeqSetupGUID = invoiceNumberSeqSetupItemView.InvoiceNumberSeqSetupGUID.StringToGuid();
				invoiceNumberSeqSetup.InvoiceTypeGUID = invoiceNumberSeqSetupItemView.InvoiceTypeGUID.StringToGuid();
				invoiceNumberSeqSetup.ProductType = invoiceNumberSeqSetupItemView.ProductType;
				invoiceNumberSeqSetup.NumberSeqTableGUID = invoiceNumberSeqSetupItemView.NumberSeqTableGUID.StringToGuidNull();
				invoiceNumberSeqSetup.BranchGUID = invoiceNumberSeqSetupItemView.BranchGUID.StringToGuid();
				
				invoiceNumberSeqSetup.RowVersion = invoiceNumberSeqSetupItemView.RowVersion;
				return invoiceNumberSeqSetup;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<InvoiceNumberSeqSetup> ToInvoiceNumberSeqSetup(this IEnumerable<InvoiceNumberSeqSetupItemView> invoiceNumberSeqSetupItemViews)
		{
			try
			{
				List<InvoiceNumberSeqSetup> invoiceNumberSeqSetups = new List<InvoiceNumberSeqSetup>();
				foreach (InvoiceNumberSeqSetupItemView item in invoiceNumberSeqSetupItemViews)
				{
					invoiceNumberSeqSetups.Add(item.ToInvoiceNumberSeqSetup());
				}
				return invoiceNumberSeqSetups;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static InvoiceNumberSeqSetupItemView ToInvoiceNumberSeqSetupItemView(this InvoiceNumberSeqSetup invoiceNumberSeqSetup)
		{
			try
			{
				InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupItemView = new InvoiceNumberSeqSetupItemView();
				invoiceNumberSeqSetupItemView.CompanyGUID = invoiceNumberSeqSetup.CompanyGUID.GuidNullToString();
				invoiceNumberSeqSetupItemView.CreatedBy = invoiceNumberSeqSetup.CreatedBy;
				invoiceNumberSeqSetupItemView.CreatedDateTime = invoiceNumberSeqSetup.CreatedDateTime.DateTimeToString();
				invoiceNumberSeqSetupItemView.ModifiedBy = invoiceNumberSeqSetup.ModifiedBy;
				invoiceNumberSeqSetupItemView.ModifiedDateTime = invoiceNumberSeqSetup.ModifiedDateTime.DateTimeToString();
				invoiceNumberSeqSetupItemView.Owner = invoiceNumberSeqSetup.Owner;
				invoiceNumberSeqSetupItemView.OwnerBusinessUnitGUID = invoiceNumberSeqSetup.OwnerBusinessUnitGUID.GuidNullToString();
				invoiceNumberSeqSetupItemView.InvoiceNumberSeqSetupGUID = invoiceNumberSeqSetup.InvoiceNumberSeqSetupGUID.GuidNullToString();
				invoiceNumberSeqSetupItemView.InvoiceTypeGUID = invoiceNumberSeqSetup.InvoiceTypeGUID.GuidNullToString();
				invoiceNumberSeqSetupItemView.ProductType = invoiceNumberSeqSetup.ProductType;
				invoiceNumberSeqSetupItemView.NumberSeqTableGUID = invoiceNumberSeqSetup.NumberSeqTableGUID.GuidNullToString();
				invoiceNumberSeqSetupItemView.BranchGUID = invoiceNumberSeqSetup.BranchGUID.GuidNullToString();
				
				invoiceNumberSeqSetupItemView.RowVersion = invoiceNumberSeqSetup.RowVersion;
				return invoiceNumberSeqSetupItemView.GetInvoiceNumberSeqSetupItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion InvoiceNumberSeqSetupItemView
		#region ToDropDown
		public static SelectItem<InvoiceNumberSeqSetupItemView> ToDropDownItem(this InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<InvoiceNumberSeqSetupItemView> selectItem = new SelectItem<InvoiceNumberSeqSetupItemView>();
				selectItem.Label = null;
				selectItem.Value = invoiceNumberSeqSetupView.InvoiceNumberSeqSetupGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: invoiceNumberSeqSetupView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<InvoiceNumberSeqSetupItemView>> ToDropDownItem(this IEnumerable<InvoiceNumberSeqSetupItemView> invoiceNumberSeqSetupItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<InvoiceNumberSeqSetupItemView>> selectItems = new List<SelectItem<InvoiceNumberSeqSetupItemView>>();
				foreach (InvoiceNumberSeqSetupItemView item in invoiceNumberSeqSetupItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

