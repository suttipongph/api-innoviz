using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class GuarantorAgreementLineHandler
	{
		#region GuarantorAgreementLineListView
		public static List<GuarantorAgreementLineListView> GetGuarantorAgreementLineListViewValidation(this List<GuarantorAgreementLineListView> guarantorAgreementLineListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return guarantorAgreementLineListViews;
			}
			var result = new List<GuarantorAgreementLineListView>();
			try
			{
				foreach (GuarantorAgreementLineListView item in guarantorAgreementLineListViews)
				{
					result.Add(item.GetGuarantorAgreementLineListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorAgreementLineListView GetGuarantorAgreementLineListViewValidation(this GuarantorAgreementLineListView guarantorAgreementLineListView)
		{
			try
			{
				guarantorAgreementLineListView.RowAuthorize = guarantorAgreementLineListView.GetListRowAuthorize();
				return guarantorAgreementLineListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this GuarantorAgreementLineListView guarantorAgreementLineListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion GuarantorAgreementLineListView
		#region GuarantorAgreementLineItemView
		public static GuarantorAgreementLineItemView GetGuarantorAgreementLineItemViewValidation(this GuarantorAgreementLineItemView guarantorAgreementLineItemView)
		{
			try
			{
				guarantorAgreementLineItemView.RowAuthorize = guarantorAgreementLineItemView.GetItemRowAuthorize();
				return guarantorAgreementLineItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this GuarantorAgreementLineItemView guarantorAgreementLineItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static GuarantorAgreementLine ToGuarantorAgreementLine(this GuarantorAgreementLineItemView guarantorAgreementLineItemView)
		{
			try
			{
				GuarantorAgreementLine guarantorAgreementLine = new GuarantorAgreementLine();
				guarantorAgreementLine.CompanyGUID = guarantorAgreementLineItemView.CompanyGUID.StringToGuid();
				guarantorAgreementLine.CreatedBy = guarantorAgreementLineItemView.CreatedBy;
				guarantorAgreementLine.CreatedDateTime = guarantorAgreementLineItemView.CreatedDateTime.StringToSystemDateTime();
				guarantorAgreementLine.ModifiedBy = guarantorAgreementLineItemView.ModifiedBy;
				guarantorAgreementLine.ModifiedDateTime = guarantorAgreementLineItemView.ModifiedDateTime.StringToSystemDateTime();
				guarantorAgreementLine.Owner = guarantorAgreementLineItemView.Owner;
				guarantorAgreementLine.OwnerBusinessUnitGUID = guarantorAgreementLineItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				guarantorAgreementLine.GuarantorAgreementLineGUID = guarantorAgreementLineItemView.GuarantorAgreementLineGUID.StringToGuid();
				guarantorAgreementLine.Age = guarantorAgreementLineItemView.Age;
				guarantorAgreementLine.DateOfBirth = guarantorAgreementLineItemView.DateOfBirth.StringNullToDateNull();
				guarantorAgreementLine.DateOfIssue = guarantorAgreementLineItemView.DateOfIssue.StringNullToDateNull();
				guarantorAgreementLine.GuaranteeLand = guarantorAgreementLineItemView.GuaranteeLand;
				guarantorAgreementLine.GuarantorAgreementTableGUID = guarantorAgreementLineItemView.GuarantorAgreementTableGUID.StringToGuid();
				guarantorAgreementLine.GuarantorTransGUID = guarantorAgreementLineItemView.GuarantorTransGUID.StringToGuid();
				guarantorAgreementLine.Name = guarantorAgreementLineItemView.Name;
				guarantorAgreementLine.NationalityGUID = guarantorAgreementLineItemView.NationalityGUID.StringToGuidNull();
				guarantorAgreementLine.Ordering = guarantorAgreementLineItemView.Ordering;
				guarantorAgreementLine.PassportId = guarantorAgreementLineItemView.PassportId;
				guarantorAgreementLine.PrimaryAddress1 = guarantorAgreementLineItemView.PrimaryAddress1;
				guarantorAgreementLine.PrimaryAddress2 = guarantorAgreementLineItemView.PrimaryAddress2;
				guarantorAgreementLine.RaceGUID = guarantorAgreementLineItemView.RaceGUID.StringToGuidNull();
				guarantorAgreementLine.TaxId = guarantorAgreementLineItemView.TaxId;
				guarantorAgreementLine.WorkPermitId = guarantorAgreementLineItemView.WorkPermitId;
				guarantorAgreementLine.MaximumGuaranteeAmount = guarantorAgreementLineItemView.MaximumGuaranteeAmount;
				guarantorAgreementLine.CustomerTableGUID = guarantorAgreementLineItemView.CustomerTableGUID.StringToGuidNull();
				guarantorAgreementLine.CreditAppTableGUID = guarantorAgreementLineItemView.CreditAppTableGUID.StringToGuidNull();
				guarantorAgreementLine.ApprovedCreditLimit = guarantorAgreementLineItemView.ApprovedCreditLimit;
				guarantorAgreementLine.OperatedBy = guarantorAgreementLineItemView.OperatedBy;
				
				guarantorAgreementLine.RowVersion = guarantorAgreementLineItemView.RowVersion;
				return guarantorAgreementLine;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<GuarantorAgreementLine> ToGuarantorAgreementLine(this IEnumerable<GuarantorAgreementLineItemView> guarantorAgreementLineItemViews)
		{
			try
			{
				List<GuarantorAgreementLine> guarantorAgreementLines = new List<GuarantorAgreementLine>();
				foreach (GuarantorAgreementLineItemView item in guarantorAgreementLineItemViews)
				{
					guarantorAgreementLines.Add(item.ToGuarantorAgreementLine());
				}
				return guarantorAgreementLines;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static GuarantorAgreementLineItemView ToGuarantorAgreementLineItemView(this GuarantorAgreementLine guarantorAgreementLine)
		{
			try
			{
				GuarantorAgreementLineItemView guarantorAgreementLineItemView = new GuarantorAgreementLineItemView();
				guarantorAgreementLineItemView.CompanyGUID = guarantorAgreementLine.CompanyGUID.GuidNullToString();
				guarantorAgreementLineItemView.CreatedBy = guarantorAgreementLine.CreatedBy;
				guarantorAgreementLineItemView.CreatedDateTime = guarantorAgreementLine.CreatedDateTime.DateTimeToString();
				guarantorAgreementLineItemView.ModifiedBy = guarantorAgreementLine.ModifiedBy;
				guarantorAgreementLineItemView.ModifiedDateTime = guarantorAgreementLine.ModifiedDateTime.DateTimeToString();
				guarantorAgreementLineItemView.Owner = guarantorAgreementLine.Owner;
				guarantorAgreementLineItemView.OwnerBusinessUnitGUID = guarantorAgreementLine.OwnerBusinessUnitGUID.GuidNullToString();
				guarantorAgreementLineItemView.GuarantorAgreementLineGUID = guarantorAgreementLine.GuarantorAgreementLineGUID.GuidNullToString();
				guarantorAgreementLineItemView.Age = guarantorAgreementLine.Age;
				guarantorAgreementLineItemView.DateOfBirth = guarantorAgreementLine.DateOfBirth.DateNullToString();
				guarantorAgreementLineItemView.DateOfIssue = guarantorAgreementLine.DateOfIssue.DateNullToString();
				guarantorAgreementLineItemView.GuaranteeLand = guarantorAgreementLine.GuaranteeLand;
				guarantorAgreementLineItemView.GuarantorAgreementTableGUID = guarantorAgreementLine.GuarantorAgreementTableGUID.GuidNullToString();
				guarantorAgreementLineItemView.GuarantorTransGUID = guarantorAgreementLine.GuarantorTransGUID.GuidNullToString();
				guarantorAgreementLineItemView.Name = guarantorAgreementLine.Name;
				guarantorAgreementLineItemView.NationalityGUID = guarantorAgreementLine.NationalityGUID.GuidNullToString();
				guarantorAgreementLineItemView.Ordering = guarantorAgreementLine.Ordering;
				guarantorAgreementLineItemView.PassportId = guarantorAgreementLine.PassportId;
				guarantorAgreementLineItemView.PrimaryAddress1 = guarantorAgreementLine.PrimaryAddress1;
				guarantorAgreementLineItemView.PrimaryAddress2 = guarantorAgreementLine.PrimaryAddress2;
				guarantorAgreementLineItemView.RaceGUID = guarantorAgreementLine.RaceGUID.GuidNullToString();
				guarantorAgreementLineItemView.TaxId = guarantorAgreementLine.TaxId;
				guarantorAgreementLineItemView.WorkPermitId = guarantorAgreementLine.WorkPermitId;
				guarantorAgreementLineItemView.MaximumGuaranteeAmount = guarantorAgreementLine.MaximumGuaranteeAmount;
				guarantorAgreementLineItemView.CustomerTableGUID = guarantorAgreementLine.CustomerTableGUID.GuidNullToString();
				guarantorAgreementLineItemView.CreditAppTableGUID = guarantorAgreementLine.CreditAppTableGUID.GuidNullToString();
				guarantorAgreementLineItemView.ApprovedCreditLimit = guarantorAgreementLine.ApprovedCreditLimit;
				guarantorAgreementLineItemView.OperatedBy = guarantorAgreementLine.OperatedBy;
				
				guarantorAgreementLineItemView.RowVersion = guarantorAgreementLine.RowVersion;
				return guarantorAgreementLineItemView.GetGuarantorAgreementLineItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion GuarantorAgreementLineItemView
		#region ToDropDown
		public static SelectItem<GuarantorAgreementLineItemView> ToDropDownItem(this GuarantorAgreementLineItemView guarantorAgreementLineView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<GuarantorAgreementLineItemView> selectItem = new SelectItem<GuarantorAgreementLineItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(guarantorAgreementLineView.GuarantorAgreementTableGUID.ToString(), guarantorAgreementLineView.Name);
				selectItem.Value = guarantorAgreementLineView.GuarantorAgreementLineGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: guarantorAgreementLineView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<GuarantorAgreementLineItemView>> ToDropDownItem(this IEnumerable<GuarantorAgreementLineItemView> guarantorAgreementLineItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<GuarantorAgreementLineItemView>> selectItems = new List<SelectItem<GuarantorAgreementLineItemView>>();
				foreach (GuarantorAgreementLineItemView item in guarantorAgreementLineItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

