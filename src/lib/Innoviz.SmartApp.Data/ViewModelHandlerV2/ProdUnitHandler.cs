using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ProdUnitHandler
	{
		#region ProdUnitListView
		public static List<ProdUnitListView> GetProdUnitListViewValidation(this List<ProdUnitListView> prodUnitListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return prodUnitListViews;
			}
			var result = new List<ProdUnitListView>();
			try
			{
				foreach (ProdUnitListView item in prodUnitListViews)
				{
					result.Add(item.GetProdUnitListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProdUnitListView GetProdUnitListViewValidation(this ProdUnitListView prodUnitListView)
		{
			try
			{
				prodUnitListView.RowAuthorize = prodUnitListView.GetListRowAuthorize();
				return prodUnitListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ProdUnitListView prodUnitListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ProdUnitListView
		#region ProdUnitItemView
		public static ProdUnitItemView GetProdUnitItemViewValidation(this ProdUnitItemView prodUnitItemView)
		{
			try
			{
				prodUnitItemView.RowAuthorize = prodUnitItemView.GetItemRowAuthorize();
				return prodUnitItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ProdUnitItemView prodUnitItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ProdUnit ToProdUnit(this ProdUnitItemView prodUnitItemView)
		{
			try
			{
				ProdUnit prodUnit = new ProdUnit();
				prodUnit.CompanyGUID = prodUnitItemView.CompanyGUID.StringToGuid();
				prodUnit.CreatedBy = prodUnitItemView.CreatedBy;
				prodUnit.CreatedDateTime = prodUnitItemView.CreatedDateTime.StringToSystemDateTime();
				prodUnit.ModifiedBy = prodUnitItemView.ModifiedBy;
				prodUnit.ModifiedDateTime = prodUnitItemView.ModifiedDateTime.StringToSystemDateTime();
				prodUnit.Owner = prodUnitItemView.Owner;
				prodUnit.OwnerBusinessUnitGUID = prodUnitItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				prodUnit.ProdUnitGUID = prodUnitItemView.ProdUnitGUID.StringToGuid();
				prodUnit.Description = prodUnitItemView.Description;
				prodUnit.UnitId = prodUnitItemView.UnitId;
				
				prodUnit.RowVersion = prodUnitItemView.RowVersion;
				return prodUnit;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ProdUnit> ToProdUnit(this IEnumerable<ProdUnitItemView> prodUnitItemViews)
		{
			try
			{
				List<ProdUnit> prodUnits = new List<ProdUnit>();
				foreach (ProdUnitItemView item in prodUnitItemViews)
				{
					prodUnits.Add(item.ToProdUnit());
				}
				return prodUnits;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProdUnitItemView ToProdUnitItemView(this ProdUnit prodUnit)
		{
			try
			{
				ProdUnitItemView prodUnitItemView = new ProdUnitItemView();
				prodUnitItemView.CompanyGUID = prodUnit.CompanyGUID.GuidNullToString();
				prodUnitItemView.CreatedBy = prodUnit.CreatedBy;
				prodUnitItemView.CreatedDateTime = prodUnit.CreatedDateTime.DateTimeToString();
				prodUnitItemView.ModifiedBy = prodUnit.ModifiedBy;
				prodUnitItemView.ModifiedDateTime = prodUnit.ModifiedDateTime.DateTimeToString();
				prodUnitItemView.Owner = prodUnit.Owner;
				prodUnitItemView.OwnerBusinessUnitGUID = prodUnit.OwnerBusinessUnitGUID.GuidNullToString();
				prodUnitItemView.ProdUnitGUID = prodUnit.ProdUnitGUID.GuidNullToString();
				prodUnitItemView.Description = prodUnit.Description;
				prodUnitItemView.UnitId = prodUnit.UnitId;
				
				prodUnitItemView.RowVersion = prodUnit.RowVersion;
				return prodUnitItemView.GetProdUnitItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ProdUnitItemView
		#region ToDropDown
		public static SelectItem<ProdUnitItemView> ToDropDownItem(this ProdUnitItemView prodUnitView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ProdUnitItemView> selectItem = new SelectItem<ProdUnitItemView>();
				selectItem.Label = null;
				selectItem.Value = prodUnitView.ProdUnitGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: prodUnitView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ProdUnitItemView>> ToDropDownItem(this IEnumerable<ProdUnitItemView> prodUnitItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ProdUnitItemView>> selectItems = new List<SelectItem<ProdUnitItemView>>();
				foreach (ProdUnitItemView item in prodUnitItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

