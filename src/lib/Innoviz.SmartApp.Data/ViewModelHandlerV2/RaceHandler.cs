using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class RaceHandler
	{
		#region RaceListView
		public static List<RaceListView> GetRaceListViewValidation(this List<RaceListView> raceListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return raceListViews;
			}
			var result = new List<RaceListView>();
			try
			{
				foreach (RaceListView item in raceListViews)
				{
					result.Add(item.GetRaceListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RaceListView GetRaceListViewValidation(this RaceListView raceListView)
		{
			try
			{
				raceListView.RowAuthorize = raceListView.GetListRowAuthorize();
				return raceListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this RaceListView raceListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion RaceListView
		#region RaceItemView
		public static RaceItemView GetRaceItemViewValidation(this RaceItemView raceItemView)
		{
			try
			{
				raceItemView.RowAuthorize = raceItemView.GetItemRowAuthorize();
				return raceItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this RaceItemView raceItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static Race ToRace(this RaceItemView raceItemView)
		{
			try
			{
				Race race = new Race();
				race.CompanyGUID = raceItemView.CompanyGUID.StringToGuid();
				race.CreatedBy = raceItemView.CreatedBy;
				race.CreatedDateTime = raceItemView.CreatedDateTime.StringToSystemDateTime();
				race.ModifiedBy = raceItemView.ModifiedBy;
				race.ModifiedDateTime = raceItemView.ModifiedDateTime.StringToSystemDateTime();
				race.Owner = raceItemView.Owner;
				race.OwnerBusinessUnitGUID = raceItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				race.RaceGUID = raceItemView.RaceGUID.StringToGuid();
				race.Description = raceItemView.Description;
				race.RaceId = raceItemView.RaceId;
				
				race.RowVersion = raceItemView.RowVersion;
				return race;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<Race> ToRace(this IEnumerable<RaceItemView> raceItemViews)
		{
			try
			{
				List<Race> races = new List<Race>();
				foreach (RaceItemView item in raceItemViews)
				{
					races.Add(item.ToRace());
				}
				return races;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static RaceItemView ToRaceItemView(this Race race)
		{
			try
			{
				RaceItemView raceItemView = new RaceItemView();
				raceItemView.CompanyGUID = race.CompanyGUID.GuidNullToString();
				raceItemView.CreatedBy = race.CreatedBy;
				raceItemView.CreatedDateTime = race.CreatedDateTime.DateTimeToString();
				raceItemView.ModifiedBy = race.ModifiedBy;
				raceItemView.ModifiedDateTime = race.ModifiedDateTime.DateTimeToString();
				raceItemView.Owner = race.Owner;
				raceItemView.OwnerBusinessUnitGUID = race.OwnerBusinessUnitGUID.GuidNullToString();
				raceItemView.RaceGUID = race.RaceGUID.GuidNullToString();
				raceItemView.Description = race.Description;
				raceItemView.RaceId = race.RaceId;
				
				raceItemView.RowVersion = race.RowVersion;
				return raceItemView.GetRaceItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion RaceItemView
		#region ToDropDown
		public static SelectItem<RaceItemView> ToDropDownItem(this RaceItemView raceView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<RaceItemView> selectItem = new SelectItem<RaceItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(raceView.RaceId, raceView.Description);
				selectItem.Value = raceView.RaceGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: raceView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<RaceItemView>> ToDropDownItem(this IEnumerable<RaceItemView> raceItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<RaceItemView>> selectItems = new List<SelectItem<RaceItemView>>();
				foreach (RaceItemView item in raceItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

