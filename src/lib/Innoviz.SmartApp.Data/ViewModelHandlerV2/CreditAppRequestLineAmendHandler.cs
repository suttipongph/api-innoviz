using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CreditAppRequestLineAmendHandler
	{
		#region CreditAppRequestLineAmendListView
		public static List<CreditAppRequestLineAmendListView> GetCreditAppRequestLineAmendListViewValidation(this List<CreditAppRequestLineAmendListView> creditAppRequestLineAmendListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return creditAppRequestLineAmendListViews;
			}
			var result = new List<CreditAppRequestLineAmendListView>();
			try
			{
				foreach (CreditAppRequestLineAmendListView item in creditAppRequestLineAmendListViews)
				{
					result.Add(item.GetCreditAppRequestLineAmendListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestLineAmendListView GetCreditAppRequestLineAmendListViewValidation(this CreditAppRequestLineAmendListView creditAppRequestLineAmendListView)
		{
			try
			{
				creditAppRequestLineAmendListView.RowAuthorize = creditAppRequestLineAmendListView.GetListRowAuthorize();
				return creditAppRequestLineAmendListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CreditAppRequestLineAmendListView creditAppRequestLineAmendListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CreditAppRequestLineAmendListView
		#region CreditAppRequestLineAmendItemView
		public static CreditAppRequestLineAmendItemView GetCreditAppRequestLineAmendItemViewValidation(this CreditAppRequestLineAmendItemView creditAppRequestLineAmendItemView)
		{
			try
			{
				creditAppRequestLineAmendItemView.RowAuthorize = creditAppRequestLineAmendItemView.GetItemRowAuthorize();
				return creditAppRequestLineAmendItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CreditAppRequestLineAmendItemView creditAppRequestLineAmendItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CreditAppRequestLineAmend ToCreditAppRequestLineAmend(this CreditAppRequestLineAmendItemView creditAppRequestLineAmendItemView)
		{
			try
			{
				CreditAppRequestLineAmend creditAppRequestLineAmend = new CreditAppRequestLineAmend();
				creditAppRequestLineAmend.CompanyGUID = creditAppRequestLineAmendItemView.CompanyGUID.StringToGuid();
				creditAppRequestLineAmend.CreatedBy = creditAppRequestLineAmendItemView.CreatedBy;
				creditAppRequestLineAmend.CreatedDateTime = creditAppRequestLineAmendItemView.CreatedDateTime.StringToSystemDateTime();
				creditAppRequestLineAmend.ModifiedBy = creditAppRequestLineAmendItemView.ModifiedBy;
				creditAppRequestLineAmend.ModifiedDateTime = creditAppRequestLineAmendItemView.ModifiedDateTime.StringToSystemDateTime();
				creditAppRequestLineAmend.Owner = creditAppRequestLineAmendItemView.Owner;
				creditAppRequestLineAmend.OwnerBusinessUnitGUID = creditAppRequestLineAmendItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				creditAppRequestLineAmend.CreditAppRequestLineAmendGUID = creditAppRequestLineAmendItemView.CreditAppRequestLineAmendGUID.StringToGuid();
				creditAppRequestLineAmend.AmendAssignmentMethod = creditAppRequestLineAmendItemView.AmendAssignmentMethod;
				creditAppRequestLineAmend.AmendBillingDocumentCondition = creditAppRequestLineAmendItemView.AmendBillingDocumentCondition;
				creditAppRequestLineAmend.AmendBillingInformation = creditAppRequestLineAmendItemView.AmendBillingInformation;
				creditAppRequestLineAmend.AmendRate = creditAppRequestLineAmendItemView.AmendRate;
				creditAppRequestLineAmend.AmendReceiptDocumentCondition = creditAppRequestLineAmendItemView.AmendReceiptDocumentCondition;
				creditAppRequestLineAmend.AmendReceiptInformation = creditAppRequestLineAmendItemView.AmendReceiptInformation;
				creditAppRequestLineAmend.CreditAppRequestLineGUID = creditAppRequestLineAmendItemView.CreditAppRequestLineGUID.StringToGuid();
				creditAppRequestLineAmend.OriginalAssignmentMethodGUID = creditAppRequestLineAmendItemView.OriginalAssignmentMethodGUID.StringToGuidNull();
				creditAppRequestLineAmend.OriginalAssignmentMethodRemark = creditAppRequestLineAmendItemView.OriginalAssignmentMethodRemark;
				creditAppRequestLineAmend.OriginalBillingAddressGUID = creditAppRequestLineAmendItemView.OriginalBillingAddressGUID.StringToGuidNull();
				creditAppRequestLineAmend.OriginalCreditLimitLineRequest = creditAppRequestLineAmendItemView.OriginalCreditLimitLineRequest;
				creditAppRequestLineAmend.OriginalMaxPurchasePct = creditAppRequestLineAmendItemView.OriginalMaxPurchasePct;
				creditAppRequestLineAmend.OriginalMethodOfPaymentGUID = creditAppRequestLineAmendItemView.OriginalMethodOfPaymentGUID.StringToGuidNull();
				creditAppRequestLineAmend.OriginalPurchaseFeeCalculateBase = creditAppRequestLineAmendItemView.OriginalPurchaseFeeCalculateBase;
				creditAppRequestLineAmend.OriginalPurchaseFeePct = creditAppRequestLineAmendItemView.OriginalPurchaseFeePct;
				creditAppRequestLineAmend.OriginalReceiptAddressGUID = creditAppRequestLineAmendItemView.OriginalReceiptAddressGUID.StringToGuidNull();
				creditAppRequestLineAmend.OriginalReceiptRemark = creditAppRequestLineAmendItemView.OriginalReceiptRemark;
				
				creditAppRequestLineAmend.RowVersion = creditAppRequestLineAmendItemView.RowVersion;
				return creditAppRequestLineAmend;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CreditAppRequestLineAmend> ToCreditAppRequestLineAmend(this IEnumerable<CreditAppRequestLineAmendItemView> creditAppRequestLineAmendItemViews)
		{
			try
			{
				List<CreditAppRequestLineAmend> creditAppRequestLineAmends = new List<CreditAppRequestLineAmend>();
				foreach (CreditAppRequestLineAmendItemView item in creditAppRequestLineAmendItemViews)
				{
					creditAppRequestLineAmends.Add(item.ToCreditAppRequestLineAmend());
				}
				return creditAppRequestLineAmends;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CreditAppRequestLineAmendItemView ToCreditAppRequestLineAmendItemView(this CreditAppRequestLineAmend creditAppRequestLineAmend)
		{
			try
			{
				CreditAppRequestLineAmendItemView creditAppRequestLineAmendItemView = new CreditAppRequestLineAmendItemView();
				creditAppRequestLineAmendItemView.CompanyGUID = creditAppRequestLineAmend.CompanyGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.CreatedBy = creditAppRequestLineAmend.CreatedBy;
				creditAppRequestLineAmendItemView.CreatedDateTime = creditAppRequestLineAmend.CreatedDateTime.DateTimeToString();
				creditAppRequestLineAmendItemView.ModifiedBy = creditAppRequestLineAmend.ModifiedBy;
				creditAppRequestLineAmendItemView.ModifiedDateTime = creditAppRequestLineAmend.ModifiedDateTime.DateTimeToString();
				creditAppRequestLineAmendItemView.Owner = creditAppRequestLineAmend.Owner;
				creditAppRequestLineAmendItemView.OwnerBusinessUnitGUID = creditAppRequestLineAmend.OwnerBusinessUnitGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.CreditAppRequestLineAmendGUID = creditAppRequestLineAmend.CreditAppRequestLineAmendGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.AmendAssignmentMethod = creditAppRequestLineAmend.AmendAssignmentMethod;
				creditAppRequestLineAmendItemView.AmendBillingDocumentCondition = creditAppRequestLineAmend.AmendBillingDocumentCondition;
				creditAppRequestLineAmendItemView.AmendBillingInformation = creditAppRequestLineAmend.AmendBillingInformation;
				creditAppRequestLineAmendItemView.AmendRate = creditAppRequestLineAmend.AmendRate;
				creditAppRequestLineAmendItemView.AmendReceiptDocumentCondition = creditAppRequestLineAmend.AmendReceiptDocumentCondition;
				creditAppRequestLineAmendItemView.AmendReceiptInformation = creditAppRequestLineAmend.AmendReceiptInformation;
				creditAppRequestLineAmendItemView.CreditAppRequestLineGUID = creditAppRequestLineAmend.CreditAppRequestLineGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.OriginalAssignmentMethodGUID = creditAppRequestLineAmend.OriginalAssignmentMethodGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.OriginalAssignmentMethodRemark = creditAppRequestLineAmend.OriginalAssignmentMethodRemark;
				creditAppRequestLineAmendItemView.OriginalBillingAddressGUID = creditAppRequestLineAmend.OriginalBillingAddressGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.OriginalCreditLimitLineRequest = creditAppRequestLineAmend.OriginalCreditLimitLineRequest;
				creditAppRequestLineAmendItemView.OriginalMaxPurchasePct = creditAppRequestLineAmend.OriginalMaxPurchasePct;
				creditAppRequestLineAmendItemView.OriginalMethodOfPaymentGUID = creditAppRequestLineAmend.OriginalMethodOfPaymentGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.OriginalPurchaseFeeCalculateBase = creditAppRequestLineAmend.OriginalPurchaseFeeCalculateBase;
				creditAppRequestLineAmendItemView.OriginalPurchaseFeePct = creditAppRequestLineAmend.OriginalPurchaseFeePct;
				creditAppRequestLineAmendItemView.OriginalReceiptAddressGUID = creditAppRequestLineAmend.OriginalReceiptAddressGUID.GuidNullToString();
				creditAppRequestLineAmendItemView.OriginalReceiptRemark = creditAppRequestLineAmend.OriginalReceiptRemark;
				
				creditAppRequestLineAmendItemView.RowVersion = creditAppRequestLineAmend.RowVersion;
				return creditAppRequestLineAmendItemView.GetCreditAppRequestLineAmendItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CreditAppRequestLineAmendItemView
		#region ToDropDown
		public static SelectItem<CreditAppRequestLineAmendItemView> ToDropDownItem(this CreditAppRequestLineAmendItemView creditAppRequestLineAmendView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CreditAppRequestLineAmendItemView> selectItem = new SelectItem<CreditAppRequestLineAmendItemView>();
				//selectItem.Label = SmartAppUtil.GetDropDownLabel(creditAppRequestLineAmendView.CreditAppRequestId, creditAppRequestLineAmendView.Description);
				selectItem.Value = creditAppRequestLineAmendView.CreditAppRequestLineAmendGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: creditAppRequestLineAmendView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CreditAppRequestLineAmendItemView>> ToDropDownItem(this IEnumerable<CreditAppRequestLineAmendItemView> creditAppRequestLineAmendItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CreditAppRequestLineAmendItemView>> selectItems = new List<SelectItem<CreditAppRequestLineAmendItemView>>();
				foreach (CreditAppRequestLineAmendItemView item in creditAppRequestLineAmendItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

