using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CustBankHandler
	{
		#region CustBankListView
		public static List<CustBankListView> GetCustBankListViewValidation(this List<CustBankListView> custBankListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return custBankListViews;
			}
			var result = new List<CustBankListView>();
			try
			{
				foreach (CustBankListView item in custBankListViews)
				{
					result.Add(item.GetCustBankListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustBankListView GetCustBankListViewValidation(this CustBankListView custBankListView)
		{
			try
			{
				custBankListView.RowAuthorize = custBankListView.GetListRowAuthorize();
				return custBankListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CustBankListView custBankListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CustBankListView
		#region CustBankItemView
		public static CustBankItemView GetCustBankItemViewValidation(this CustBankItemView custBankItemView)
		{
			try
			{
				custBankItemView.RowAuthorize = custBankItemView.GetItemRowAuthorize();
				return custBankItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CustBankItemView custBankItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CustBank ToCustBank(this CustBankItemView custBankItemView)
		{
			try
			{
				CustBank custBank = new CustBank();
				custBank.CompanyGUID = custBankItemView.CompanyGUID.StringToGuid();
				custBank.CreatedBy = custBankItemView.CreatedBy;
				custBank.CreatedDateTime = custBankItemView.CreatedDateTime.StringToSystemDateTime();
				custBank.ModifiedBy = custBankItemView.ModifiedBy;
				custBank.ModifiedDateTime = custBankItemView.ModifiedDateTime.StringToSystemDateTime();
				custBank.Owner = custBankItemView.Owner;
				custBank.OwnerBusinessUnitGUID = custBankItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				custBank.CustBankGUID = custBankItemView.CustBankGUID.StringToGuid();
				custBank.AccountNumber = custBankItemView.AccountNumber;
				custBank.BankAccountControl = custBankItemView.BankAccountControl;
				custBank.BankAccountName = custBankItemView.BankAccountName;
				custBank.BankBranch = custBankItemView.BankBranch;
				custBank.BankGroupGUID = custBankItemView.BankGroupGUID.StringToGuid();
				custBank.BankTypeGUID = custBankItemView.BankTypeGUID.StringToGuid();
				custBank.CustomerTableGUID = custBankItemView.CustomerTableGUID.StringToGuidNull();
				custBank.InActive = custBankItemView.InActive;
				custBank.PDC = custBankItemView.PDC;
				custBank.Primary = custBankItemView.Primary;
				
				custBank.RowVersion = custBankItemView.RowVersion;
				return custBank;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CustBank> ToCustBank(this IEnumerable<CustBankItemView> custBankItemViews)
		{
			try
			{
				List<CustBank> custBanks = new List<CustBank>();
				foreach (CustBankItemView item in custBankItemViews)
				{
					custBanks.Add(item.ToCustBank());
				}
				return custBanks;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CustBankItemView ToCustBankItemView(this CustBank custBank)
		{
			try
			{
				CustBankItemView custBankItemView = new CustBankItemView();
				custBankItemView.CompanyGUID = custBank.CompanyGUID.GuidNullToString();
				custBankItemView.CreatedBy = custBank.CreatedBy;
				custBankItemView.CreatedDateTime = custBank.CreatedDateTime.DateTimeToString();
				custBankItemView.ModifiedBy = custBank.ModifiedBy;
				custBankItemView.ModifiedDateTime = custBank.ModifiedDateTime.DateTimeToString();
				custBankItemView.Owner = custBank.Owner;
				custBankItemView.OwnerBusinessUnitGUID = custBank.OwnerBusinessUnitGUID.GuidNullToString();
				custBankItemView.CustBankGUID = custBank.CustBankGUID.GuidNullToString();
				custBankItemView.AccountNumber = custBank.AccountNumber;
				custBankItemView.BankAccountControl = custBank.BankAccountControl;
				custBankItemView.BankAccountName = custBank.BankAccountName;
				custBankItemView.BankBranch = custBank.BankBranch;
				custBankItemView.BankGroupGUID = custBank.BankGroupGUID.GuidNullToString();
				custBankItemView.BankTypeGUID = custBank.BankTypeGUID.GuidNullToString();
				custBankItemView.CustomerTableGUID = custBank.CustomerTableGUID.GuidNullToString();
				custBankItemView.InActive = custBank.InActive;
				custBankItemView.PDC = custBank.PDC;
				custBankItemView.Primary = custBank.Primary;
				
				custBankItemView.RowVersion = custBank.RowVersion;
				return custBankItemView.GetCustBankItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CustBankItemView
		#region ToDropDown
		public static SelectItem<CustBankItemView> ToDropDownItem(this CustBankItemView custBankView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CustBankItemView> selectItem = new SelectItem<CustBankItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(custBankView.BankAccountName);
				selectItem.Value = custBankView.CustBankGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: custBankView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CustBankItemView>> ToDropDownItem(this IEnumerable<CustBankItemView> custBankItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CustBankItemView>> selectItems = new List<SelectItem<CustBankItemView>>();
				foreach (CustBankItemView item in custBankItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

