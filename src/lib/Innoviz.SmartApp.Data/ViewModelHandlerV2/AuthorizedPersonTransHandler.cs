using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class AuthorizedPersonTransHandler
	{
		#region AuthorizedPersonTransListView
		public static List<AuthorizedPersonTransListView> GetAuthorizedPersonTransListViewValidation(this List<AuthorizedPersonTransListView> authorizedPersonTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return authorizedPersonTransListViews;
			}
			var result = new List<AuthorizedPersonTransListView>();
			try
			{
				foreach (AuthorizedPersonTransListView item in authorizedPersonTransListViews)
				{
					result.Add(item.GetAuthorizedPersonTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AuthorizedPersonTransListView GetAuthorizedPersonTransListViewValidation(this AuthorizedPersonTransListView authorizedPersonTransListView)
		{
			try
			{
				authorizedPersonTransListView.RowAuthorize = authorizedPersonTransListView.GetListRowAuthorize();
				return authorizedPersonTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this AuthorizedPersonTransListView authorizedPersonTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion AuthorizedPersonTransListView
		#region AuthorizedPersonTransItemView
		public static AuthorizedPersonTransItemView GetAuthorizedPersonTransItemViewValidation(this AuthorizedPersonTransItemView authorizedPersonTransItemView)
		{
			try
			{
				authorizedPersonTransItemView.RowAuthorize = authorizedPersonTransItemView.GetItemRowAuthorize();
				return authorizedPersonTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this AuthorizedPersonTransItemView authorizedPersonTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static AuthorizedPersonTrans ToAuthorizedPersonTrans(this AuthorizedPersonTransItemView authorizedPersonTransItemView)
		{
			try
			{
				AuthorizedPersonTrans authorizedPersonTrans = new AuthorizedPersonTrans();
				authorizedPersonTrans.CompanyGUID = authorizedPersonTransItemView.CompanyGUID.StringToGuid();
				authorizedPersonTrans.CreatedBy = authorizedPersonTransItemView.CreatedBy;
				authorizedPersonTrans.CreatedDateTime = authorizedPersonTransItemView.CreatedDateTime.StringToSystemDateTime();
				authorizedPersonTrans.ModifiedBy = authorizedPersonTransItemView.ModifiedBy;
				authorizedPersonTrans.ModifiedDateTime = authorizedPersonTransItemView.ModifiedDateTime.StringToSystemDateTime();
				authorizedPersonTrans.Owner = authorizedPersonTransItemView.Owner;
				authorizedPersonTrans.OwnerBusinessUnitGUID = authorizedPersonTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				authorizedPersonTrans.AuthorizedPersonTransGUID = authorizedPersonTransItemView.AuthorizedPersonTransGUID.StringToGuid();
				authorizedPersonTrans.AuthorizedPersonTypeGUID = authorizedPersonTransItemView.AuthorizedPersonTypeGUID.StringToGuidNull();
				authorizedPersonTrans.InActive = authorizedPersonTransItemView.InActive;
				authorizedPersonTrans.NCBCheckedBy = authorizedPersonTransItemView.NCBCheckedBy;
				authorizedPersonTrans.NCBCheckedDate = authorizedPersonTransItemView.NCBCheckedDate.StringNullToDateNull();
				authorizedPersonTrans.Ordering = authorizedPersonTransItemView.Ordering;
				authorizedPersonTrans.RefAuthorizedPersonTransGUID = authorizedPersonTransItemView.RefAuthorizedPersonTransGUID.StringToGuidNull();
				authorizedPersonTrans.RefGUID = authorizedPersonTransItemView.RefGUID.StringToGuid();
				authorizedPersonTrans.RefType = authorizedPersonTransItemView.RefType;
				authorizedPersonTrans.RelatedPersonTableGUID = authorizedPersonTransItemView.RelatedPersonTableGUID.StringToGuid();
				authorizedPersonTrans.Replace = authorizedPersonTransItemView.Replace;
				authorizedPersonTrans.ReplacedAuthorizedPersonTransGUID = authorizedPersonTransItemView.ReplacedAuthorizedPersonTransGUID.StringToGuidNull();
				
				authorizedPersonTrans.RowVersion = authorizedPersonTransItemView.RowVersion;
				return authorizedPersonTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<AuthorizedPersonTrans> ToAuthorizedPersonTrans(this IEnumerable<AuthorizedPersonTransItemView> authorizedPersonTransItemViews)
		{
			try
			{
				List<AuthorizedPersonTrans> authorizedPersonTranss = new List<AuthorizedPersonTrans>();
				foreach (AuthorizedPersonTransItemView item in authorizedPersonTransItemViews)
				{
					authorizedPersonTranss.Add(item.ToAuthorizedPersonTrans());
				}
				return authorizedPersonTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static AuthorizedPersonTransItemView ToAuthorizedPersonTransItemView(this AuthorizedPersonTrans authorizedPersonTrans)
		{
			try
			{
				AuthorizedPersonTransItemView authorizedPersonTransItemView = new AuthorizedPersonTransItemView();
				authorizedPersonTransItemView.CompanyGUID = authorizedPersonTrans.CompanyGUID.GuidNullToString();
				authorizedPersonTransItemView.CreatedBy = authorizedPersonTrans.CreatedBy;
				authorizedPersonTransItemView.CreatedDateTime = authorizedPersonTrans.CreatedDateTime.DateTimeToString();
				authorizedPersonTransItemView.ModifiedBy = authorizedPersonTrans.ModifiedBy;
				authorizedPersonTransItemView.ModifiedDateTime = authorizedPersonTrans.ModifiedDateTime.DateTimeToString();
				authorizedPersonTransItemView.Owner = authorizedPersonTrans.Owner;
				authorizedPersonTransItemView.OwnerBusinessUnitGUID = authorizedPersonTrans.OwnerBusinessUnitGUID.GuidNullToString();
				authorizedPersonTransItemView.AuthorizedPersonTransGUID = authorizedPersonTrans.AuthorizedPersonTransGUID.GuidNullToString();
				authorizedPersonTransItemView.AuthorizedPersonTypeGUID = authorizedPersonTrans.AuthorizedPersonTypeGUID.GuidNullToString();
				authorizedPersonTransItemView.InActive = authorizedPersonTrans.InActive;
				authorizedPersonTransItemView.NCBCheckedBy = authorizedPersonTrans.NCBCheckedBy;
				authorizedPersonTransItemView.NCBCheckedDate = authorizedPersonTrans.NCBCheckedDate.DateNullToString();
				authorizedPersonTransItemView.Ordering = authorizedPersonTrans.Ordering;
				authorizedPersonTransItemView.RefAuthorizedPersonTransGUID = authorizedPersonTrans.RefAuthorizedPersonTransGUID.GuidNullToString();
				authorizedPersonTransItemView.RefGUID = authorizedPersonTrans.RefGUID.GuidNullToString();
				authorizedPersonTransItemView.RefType = authorizedPersonTrans.RefType;
				authorizedPersonTransItemView.RelatedPersonTableGUID = authorizedPersonTrans.RelatedPersonTableGUID.GuidNullToString();
				authorizedPersonTransItemView.Replace = authorizedPersonTrans.Replace;
				authorizedPersonTransItemView.ReplacedAuthorizedPersonTransGUID = authorizedPersonTrans.ReplacedAuthorizedPersonTransGUID.GuidNullToString();
				
				authorizedPersonTransItemView.RowVersion = authorizedPersonTrans.RowVersion;
				return authorizedPersonTransItemView.GetAuthorizedPersonTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion AuthorizedPersonTransItemView
		#region ToDropDown
		public static SelectItem<AuthorizedPersonTransItemView> ToDropDownItem(this AuthorizedPersonTransItemView authorizedPersonTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<AuthorizedPersonTransItemView> selectItem = new SelectItem<AuthorizedPersonTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(authorizedPersonTransView.RelatedPersonTable_RelatedPersonId, authorizedPersonTransView.RelatedPersonTable_Name);
				selectItem.Value = authorizedPersonTransView.AuthorizedPersonTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: authorizedPersonTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<AuthorizedPersonTransItemView>> ToDropDownItem(this IEnumerable<AuthorizedPersonTransItemView> authorizedPersonTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<AuthorizedPersonTransItemView>> selectItems = new List<SelectItem<AuthorizedPersonTransItemView>>();
				foreach (AuthorizedPersonTransItemView item in authorizedPersonTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

