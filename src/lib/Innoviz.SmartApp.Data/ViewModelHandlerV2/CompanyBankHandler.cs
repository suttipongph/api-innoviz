using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class CompanyBankHandler
	{
		#region CompanyBankListView
		public static List<CompanyBankListView> GetCompanyBankListViewValidation(this List<CompanyBankListView> companyBankListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return companyBankListViews;
			}
			var result = new List<CompanyBankListView>();
			try
			{
				foreach (CompanyBankListView item in companyBankListViews)
				{
					result.Add(item.GetCompanyBankListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanyBankListView GetCompanyBankListViewValidation(this CompanyBankListView companyBankListView)
		{
			try
			{
				companyBankListView.RowAuthorize = companyBankListView.GetListRowAuthorize();
				return companyBankListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this CompanyBankListView companyBankListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion CompanyBankListView
		#region CompanyBankItemView
		public static CompanyBankItemView GetCompanyBankItemViewValidation(this CompanyBankItemView companyBankItemView)
		{
			try
			{
				companyBankItemView.RowAuthorize = companyBankItemView.GetItemRowAuthorize();
				return companyBankItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this CompanyBankItemView companyBankItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static CompanyBank ToCompanyBank(this CompanyBankItemView companyBankItemView)
		{
			try
			{
				CompanyBank companyBank = new CompanyBank();
				companyBank.CompanyGUID = companyBankItemView.CompanyGUID.StringToGuid();
				companyBank.CreatedBy = companyBankItemView.CreatedBy;
				companyBank.CreatedDateTime = companyBankItemView.CreatedDateTime.StringToSystemDateTime();
				companyBank.ModifiedBy = companyBankItemView.ModifiedBy;
				companyBank.ModifiedDateTime = companyBankItemView.ModifiedDateTime.StringToSystemDateTime();
				companyBank.Owner = companyBankItemView.Owner;
				companyBank.OwnerBusinessUnitGUID = companyBankItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				companyBank.CompanyBankGUID = companyBankItemView.CompanyBankGUID.StringToGuid();
				companyBank.AccountNumber = companyBankItemView.AccountNumber;
				companyBank.BankAccountName = companyBankItemView.BankAccountName;
				companyBank.BankBranch = companyBankItemView.BankBranch;
				companyBank.BankGroupGUID = companyBankItemView.BankGroupGUID.StringToGuid();
				companyBank.BankTypeGUID = companyBankItemView.BankTypeGUID.StringToGuid();
				companyBank.InActive = companyBankItemView.InActive;
				companyBank.Primary = companyBankItemView.Primary;
				
				companyBank.RowVersion = companyBankItemView.RowVersion;
				return companyBank;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<CompanyBank> ToCompanyBank(this IEnumerable<CompanyBankItemView> companyBankItemViews)
		{
			try
			{
				List<CompanyBank> companyBanks = new List<CompanyBank>();
				foreach (CompanyBankItemView item in companyBankItemViews)
				{
					companyBanks.Add(item.ToCompanyBank());
				}
				return companyBanks;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static CompanyBankItemView ToCompanyBankItemView(this CompanyBank companyBank)
		{
			try
			{
				CompanyBankItemView companyBankItemView = new CompanyBankItemView();
				companyBankItemView.CompanyGUID = companyBank.CompanyGUID.GuidNullToString();
				companyBankItemView.CreatedBy = companyBank.CreatedBy;
				companyBankItemView.CreatedDateTime = companyBank.CreatedDateTime.DateTimeToString();
				companyBankItemView.ModifiedBy = companyBank.ModifiedBy;
				companyBankItemView.ModifiedDateTime = companyBank.ModifiedDateTime.DateTimeToString();
				companyBankItemView.Owner = companyBank.Owner;
				companyBankItemView.OwnerBusinessUnitGUID = companyBank.OwnerBusinessUnitGUID.GuidNullToString();
				companyBankItemView.CompanyBankGUID = companyBank.CompanyBankGUID.GuidNullToString();
				companyBankItemView.AccountNumber = companyBank.AccountNumber;
				companyBankItemView.BankAccountName = companyBank.BankAccountName;
				companyBankItemView.BankBranch = companyBank.BankBranch;
				companyBankItemView.BankGroupGUID = companyBank.BankGroupGUID.GuidNullToString();
				companyBankItemView.BankTypeGUID = companyBank.BankTypeGUID.GuidNullToString();
				companyBankItemView.InActive = companyBank.InActive;
				companyBankItemView.Primary = companyBank.Primary;
				
				companyBankItemView.RowVersion = companyBank.RowVersion;
				return companyBankItemView.GetCompanyBankItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion CompanyBankItemView
		#region ToDropDown
		public static SelectItem<CompanyBankItemView> ToDropDownItem(this CompanyBankItemView companyBankView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<CompanyBankItemView> selectItem = new SelectItem<CompanyBankItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(companyBankView.BankGroup_BankGroupId, companyBankView.BankAccountName);
				selectItem.Value = companyBankView.CompanyBankGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: companyBankView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<CompanyBankItemView>> ToDropDownItem(this IEnumerable<CompanyBankItemView> companyBankItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<CompanyBankItemView>> selectItems = new List<SelectItem<CompanyBankItemView>>();
				foreach (CompanyBankItemView item in companyBankItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

