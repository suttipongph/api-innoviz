using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class BusinessCollateralTypeHandler
	{
		#region BusinessCollateralTypeListView
		public static List<BusinessCollateralTypeListView> GetBusinessCollateralTypeListViewValidation(this List<BusinessCollateralTypeListView> businessCollateralTypeListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return businessCollateralTypeListViews;
			}
			var result = new List<BusinessCollateralTypeListView>();
			try
			{
				foreach (BusinessCollateralTypeListView item in businessCollateralTypeListViews)
				{
					result.Add(item.GetBusinessCollateralTypeListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralTypeListView GetBusinessCollateralTypeListViewValidation(this BusinessCollateralTypeListView businessCollateralTypeListView)
		{
			try
			{
				businessCollateralTypeListView.RowAuthorize = businessCollateralTypeListView.GetListRowAuthorize();
				return businessCollateralTypeListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this BusinessCollateralTypeListView businessCollateralTypeListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion BusinessCollateralTypeListView
		#region BusinessCollateralTypeItemView
		public static BusinessCollateralTypeItemView GetBusinessCollateralTypeItemViewValidation(this BusinessCollateralTypeItemView businessCollateralTypeItemView)
		{
			try
			{
				businessCollateralTypeItemView.RowAuthorize = businessCollateralTypeItemView.GetItemRowAuthorize();
				return businessCollateralTypeItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this BusinessCollateralTypeItemView businessCollateralTypeItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static BusinessCollateralType ToBusinessCollateralType(this BusinessCollateralTypeItemView businessCollateralTypeItemView)
		{
			try
			{
				BusinessCollateralType businessCollateralType = new BusinessCollateralType();
				businessCollateralType.CompanyGUID = businessCollateralTypeItemView.CompanyGUID.StringToGuid();
				businessCollateralType.CreatedBy = businessCollateralTypeItemView.CreatedBy;
				businessCollateralType.CreatedDateTime = businessCollateralTypeItemView.CreatedDateTime.StringToSystemDateTime();
				businessCollateralType.ModifiedBy = businessCollateralTypeItemView.ModifiedBy;
				businessCollateralType.ModifiedDateTime = businessCollateralTypeItemView.ModifiedDateTime.StringToSystemDateTime();
				businessCollateralType.Owner = businessCollateralTypeItemView.Owner;
				businessCollateralType.OwnerBusinessUnitGUID = businessCollateralTypeItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				businessCollateralType.BusinessCollateralTypeGUID = businessCollateralTypeItemView.BusinessCollateralTypeGUID.StringToGuid();
				businessCollateralType.AgreementOrdering = businessCollateralTypeItemView.AgreementOrdering;
				businessCollateralType.BusinessCollateralTypeId = businessCollateralTypeItemView.BusinessCollateralTypeId;
				businessCollateralType.Description = businessCollateralTypeItemView.Description;
				
				businessCollateralType.RowVersion = businessCollateralTypeItemView.RowVersion;
				return businessCollateralType;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<BusinessCollateralType> ToBusinessCollateralType(this IEnumerable<BusinessCollateralTypeItemView> businessCollateralTypeItemViews)
		{
			try
			{
				List<BusinessCollateralType> businessCollateralTypes = new List<BusinessCollateralType>();
				foreach (BusinessCollateralTypeItemView item in businessCollateralTypeItemViews)
				{
					businessCollateralTypes.Add(item.ToBusinessCollateralType());
				}
				return businessCollateralTypes;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static BusinessCollateralTypeItemView ToBusinessCollateralTypeItemView(this BusinessCollateralType businessCollateralType)
		{
			try
			{
				BusinessCollateralTypeItemView businessCollateralTypeItemView = new BusinessCollateralTypeItemView();
				businessCollateralTypeItemView.CompanyGUID = businessCollateralType.CompanyGUID.GuidNullToString();
				businessCollateralTypeItemView.CreatedBy = businessCollateralType.CreatedBy;
				businessCollateralTypeItemView.CreatedDateTime = businessCollateralType.CreatedDateTime.DateTimeToString();
				businessCollateralTypeItemView.ModifiedBy = businessCollateralType.ModifiedBy;
				businessCollateralTypeItemView.ModifiedDateTime = businessCollateralType.ModifiedDateTime.DateTimeToString();
				businessCollateralTypeItemView.Owner = businessCollateralType.Owner;
				businessCollateralTypeItemView.OwnerBusinessUnitGUID = businessCollateralType.OwnerBusinessUnitGUID.GuidNullToString();
				businessCollateralTypeItemView.BusinessCollateralTypeGUID = businessCollateralType.BusinessCollateralTypeGUID.GuidNullToString();
				businessCollateralTypeItemView.AgreementOrdering = businessCollateralType.AgreementOrdering;
				businessCollateralTypeItemView.BusinessCollateralTypeId = businessCollateralType.BusinessCollateralTypeId;
				businessCollateralTypeItemView.Description = businessCollateralType.Description;
				
				businessCollateralTypeItemView.RowVersion = businessCollateralType.RowVersion;
				return businessCollateralTypeItemView.GetBusinessCollateralTypeItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion BusinessCollateralTypeItemView
		#region ToDropDown
		public static SelectItem<BusinessCollateralTypeItemView> ToDropDownItem(this BusinessCollateralTypeItemView businessCollateralTypeView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<BusinessCollateralTypeItemView> selectItem = new SelectItem<BusinessCollateralTypeItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(businessCollateralTypeView.BusinessCollateralTypeId, businessCollateralTypeView.Description);
				selectItem.Value = businessCollateralTypeView.BusinessCollateralTypeGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: businessCollateralTypeView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<BusinessCollateralTypeItemView>> ToDropDownItem(this IEnumerable<BusinessCollateralTypeItemView> businessCollateralTypeItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<BusinessCollateralTypeItemView>> selectItems = new List<SelectItem<BusinessCollateralTypeItemView>>();
				foreach (BusinessCollateralTypeItemView item in businessCollateralTypeItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

