using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ProductSettledTransHandler
	{
		#region ProductSettledTransListView
		public static List<ProductSettledTransListView> GetProductSettledTransListViewValidation(this List<ProductSettledTransListView> productSettledTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return productSettledTransListViews;
			}
			var result = new List<ProductSettledTransListView>();
			try
			{
				foreach (ProductSettledTransListView item in productSettledTransListViews)
				{
					result.Add(item.GetProductSettledTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProductSettledTransListView GetProductSettledTransListViewValidation(this ProductSettledTransListView productSettledTransListView)
		{
			try
			{
				productSettledTransListView.RowAuthorize = productSettledTransListView.GetListRowAuthorize();
				return productSettledTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ProductSettledTransListView productSettledTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ProductSettledTransListView
		#region ProductSettledTransItemView
		public static ProductSettledTransItemView GetProductSettledTransItemViewValidation(this ProductSettledTransItemView productSettledTransItemView)
		{
			try
			{
				productSettledTransItemView.RowAuthorize = productSettledTransItemView.GetItemRowAuthorize();
				return productSettledTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ProductSettledTransItemView productSettledTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ProductSettledTrans ToProductSettledTrans(this ProductSettledTransItemView productSettledTransItemView)
		{
			try
			{
				ProductSettledTrans productSettledTrans = new ProductSettledTrans();
				productSettledTrans.CompanyGUID = productSettledTransItemView.CompanyGUID.StringToGuid();
				productSettledTrans.CreatedBy = productSettledTransItemView.CreatedBy;
				productSettledTrans.CreatedDateTime = productSettledTransItemView.CreatedDateTime.StringToSystemDateTime();
				productSettledTrans.ModifiedBy = productSettledTransItemView.ModifiedBy;
				productSettledTrans.ModifiedDateTime = productSettledTransItemView.ModifiedDateTime.StringToSystemDateTime();
				productSettledTrans.Owner = productSettledTransItemView.Owner;
				productSettledTrans.OwnerBusinessUnitGUID = productSettledTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				productSettledTrans.ProductSettledTransGUID = productSettledTransItemView.ProductSettledTransGUID.StringToGuid();
				productSettledTrans.DocumentId = productSettledTransItemView.DocumentId;
				productSettledTrans.InterestCalcAmount = productSettledTransItemView.InterestCalcAmount;
				productSettledTrans.InterestDate = productSettledTransItemView.InterestDate.StringToDate();
				productSettledTrans.InterestDay = productSettledTransItemView.InterestDay;
				productSettledTrans.InvoiceSettlementDetailGUID = productSettledTransItemView.InvoiceSettlementDetailGUID.StringToGuid();
				productSettledTrans.LastReceivedDate = productSettledTransItemView.LastReceivedDate.StringNullToDateNull();
				productSettledTrans.OriginalDocumentId = productSettledTransItemView.OriginalDocumentId;
				productSettledTrans.OriginalInterestCalcAmount = productSettledTransItemView.OriginalInterestCalcAmount;
				productSettledTrans.OriginalRefGUID  = productSettledTransItemView.OriginalRefGUID .StringToGuidNull();
				productSettledTrans.PDCInterestOutstanding = productSettledTransItemView.PDCInterestOutstanding;
				productSettledTrans.PostedInterestAmount = productSettledTransItemView.PostedInterestAmount;
				productSettledTrans.ProductType = productSettledTransItemView.ProductType;
				productSettledTrans.RefGUID = productSettledTransItemView.RefGUID.StringToGuidNull();
				productSettledTrans.RefType = productSettledTransItemView.RefType;
				productSettledTrans.ReserveToBeRefund = productSettledTransItemView.ReserveToBeRefund;
				productSettledTrans.SettledDate = productSettledTransItemView.SettledDate.StringToDate();
				productSettledTrans.SettledInvoiceAmount = productSettledTransItemView.SettledInvoiceAmount;
				productSettledTrans.SettledPurchaseAmount = productSettledTransItemView.SettledPurchaseAmount;
				productSettledTrans.SettledReserveAmount = productSettledTransItemView.SettledReserveAmount;
				productSettledTrans.TotalRefundAdditionalIntAmount = productSettledTransItemView.TotalRefundAdditionalIntAmount;
				
				productSettledTrans.RowVersion = productSettledTransItemView.RowVersion;
				return productSettledTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ProductSettledTrans> ToProductSettledTrans(this IEnumerable<ProductSettledTransItemView> productSettledTransItemViews)
		{
			try
			{
				List<ProductSettledTrans> productSettledTranss = new List<ProductSettledTrans>();
				foreach (ProductSettledTransItemView item in productSettledTransItemViews)
				{
					productSettledTranss.Add(item.ToProductSettledTrans());
				}
				return productSettledTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ProductSettledTransItemView ToProductSettledTransItemView(this ProductSettledTrans productSettledTrans)
		{
			try
			{
				ProductSettledTransItemView productSettledTransItemView = new ProductSettledTransItemView();
				productSettledTransItemView.CompanyGUID = productSettledTrans.CompanyGUID.GuidNullToString();
				productSettledTransItemView.CreatedBy = productSettledTrans.CreatedBy;
				productSettledTransItemView.CreatedDateTime = productSettledTrans.CreatedDateTime.DateTimeToString();
				productSettledTransItemView.ModifiedBy = productSettledTrans.ModifiedBy;
				productSettledTransItemView.ModifiedDateTime = productSettledTrans.ModifiedDateTime.DateTimeToString();
				productSettledTransItemView.Owner = productSettledTrans.Owner;
				productSettledTransItemView.OwnerBusinessUnitGUID = productSettledTrans.OwnerBusinessUnitGUID.GuidNullToString();
				productSettledTransItemView.ProductSettledTransGUID = productSettledTrans.ProductSettledTransGUID.GuidNullToString();
				productSettledTransItemView.DocumentId = productSettledTrans.DocumentId;
				productSettledTransItemView.InterestCalcAmount = productSettledTrans.InterestCalcAmount;
				productSettledTransItemView.InterestDate = productSettledTrans.InterestDate.DateToString();
				productSettledTransItemView.InterestDay = productSettledTrans.InterestDay;
				productSettledTransItemView.InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID.GuidNullToString();
				productSettledTransItemView.LastReceivedDate = productSettledTrans.LastReceivedDate.DateNullToString();
				productSettledTransItemView.OriginalDocumentId = productSettledTrans.OriginalDocumentId;
				productSettledTransItemView.OriginalInterestCalcAmount = productSettledTrans.OriginalInterestCalcAmount;
				productSettledTransItemView.OriginalRefGUID  = productSettledTrans.OriginalRefGUID .GuidNullToString();
				productSettledTransItemView.PDCInterestOutstanding = productSettledTrans.PDCInterestOutstanding;
				productSettledTransItemView.PostedInterestAmount = productSettledTrans.PostedInterestAmount;
				productSettledTransItemView.ProductType = productSettledTrans.ProductType;
				productSettledTransItemView.RefGUID = productSettledTrans.RefGUID.GuidNullToString();
				productSettledTransItemView.RefType = productSettledTrans.RefType;
				productSettledTransItemView.ReserveToBeRefund = productSettledTrans.ReserveToBeRefund;
				productSettledTransItemView.SettledDate = productSettledTrans.SettledDate.DateToString();
				productSettledTransItemView.SettledInvoiceAmount = productSettledTrans.SettledInvoiceAmount;
				productSettledTransItemView.SettledPurchaseAmount = productSettledTrans.SettledPurchaseAmount;
				productSettledTransItemView.SettledReserveAmount = productSettledTrans.SettledReserveAmount;
				productSettledTransItemView.TotalRefundAdditionalIntAmount = productSettledTrans.TotalRefundAdditionalIntAmount;
				
				productSettledTransItemView.RowVersion = productSettledTrans.RowVersion;
				return productSettledTransItemView.GetProductSettledTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ProductSettledTransItemView
		#region ToDropDown
		public static SelectItem<ProductSettledTransItemView> ToDropDownItem(this ProductSettledTransItemView productSettledTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ProductSettledTransItemView> selectItem = new SelectItem<ProductSettledTransItemView>();
				selectItem.Label = null;
				selectItem.Value = productSettledTransView.ProductSettledTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: productSettledTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ProductSettledTransItemView>> ToDropDownItem(this IEnumerable<ProductSettledTransItemView> productSettledTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ProductSettledTransItemView>> selectItems = new List<SelectItem<ProductSettledTransItemView>>();
				foreach (ProductSettledTransItemView item in productSettledTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

