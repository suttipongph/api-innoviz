using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class TaxReportTransHandler
	{
		#region TaxReportTransListView
		public static List<TaxReportTransListView> GetTaxReportTransListViewValidation(this List<TaxReportTransListView> taxReportTransListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return taxReportTransListViews;
			}
			var result = new List<TaxReportTransListView>();
			try
			{
				foreach (TaxReportTransListView item in taxReportTransListViews)
				{
					result.Add(item.GetTaxReportTransListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxReportTransListView GetTaxReportTransListViewValidation(this TaxReportTransListView taxReportTransListView)
		{
			try
			{
				taxReportTransListView.RowAuthorize = taxReportTransListView.GetListRowAuthorize();
				return taxReportTransListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this TaxReportTransListView taxReportTransListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion TaxReportTransListView
		#region TaxReportTransItemView
		public static TaxReportTransItemView GetTaxReportTransItemViewValidation(this TaxReportTransItemView taxReportTransItemView)
		{
			try
			{
				taxReportTransItemView.RowAuthorize = taxReportTransItemView.GetItemRowAuthorize();
				return taxReportTransItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this TaxReportTransItemView taxReportTransItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static TaxReportTrans ToTaxReportTrans(this TaxReportTransItemView taxReportTransItemView)
		{
			try
			{
				TaxReportTrans taxReportTrans = new TaxReportTrans();
				taxReportTrans.CompanyGUID = taxReportTransItemView.CompanyGUID.StringToGuid();
				taxReportTrans.CreatedBy = taxReportTransItemView.CreatedBy;
				taxReportTrans.CreatedDateTime = taxReportTransItemView.CreatedDateTime.StringToSystemDateTime();
				taxReportTrans.ModifiedBy = taxReportTransItemView.ModifiedBy;
				taxReportTrans.ModifiedDateTime = taxReportTransItemView.ModifiedDateTime.StringToSystemDateTime();
				taxReportTrans.Owner = taxReportTransItemView.Owner;
				taxReportTrans.OwnerBusinessUnitGUID = taxReportTransItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				taxReportTrans.TaxReportTransGUID = taxReportTransItemView.TaxReportTransGUID.StringToGuid();
				taxReportTrans.Amount = taxReportTransItemView.Amount;
				taxReportTrans.Description = taxReportTransItemView.Description;
				taxReportTrans.Month = taxReportTransItemView.Month;
				taxReportTrans.RefGUID = taxReportTransItemView.RefGUID.StringToGuidNull();
				taxReportTrans.RefId = taxReportTransItemView.RefId;
				taxReportTrans.RefType = taxReportTransItemView.RefType;
				taxReportTrans.Year = taxReportTransItemView.Year;
				
				taxReportTrans.RowVersion = taxReportTransItemView.RowVersion;
				return taxReportTrans;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<TaxReportTrans> ToTaxReportTrans(this IEnumerable<TaxReportTransItemView> taxReportTransItemViews)
		{
			try
			{
				List<TaxReportTrans> taxReportTranss = new List<TaxReportTrans>();
				foreach (TaxReportTransItemView item in taxReportTransItemViews)
				{
					taxReportTranss.Add(item.ToTaxReportTrans());
				}
				return taxReportTranss;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static TaxReportTransItemView ToTaxReportTransItemView(this TaxReportTrans taxReportTrans)
		{
			try
			{
				TaxReportTransItemView taxReportTransItemView = new TaxReportTransItemView();
				taxReportTransItemView.CompanyGUID = taxReportTrans.CompanyGUID.GuidNullToString();
				taxReportTransItemView.CreatedBy = taxReportTrans.CreatedBy;
				taxReportTransItemView.CreatedDateTime = taxReportTrans.CreatedDateTime.DateTimeToString();
				taxReportTransItemView.ModifiedBy = taxReportTrans.ModifiedBy;
				taxReportTransItemView.ModifiedDateTime = taxReportTrans.ModifiedDateTime.DateTimeToString();
				taxReportTransItemView.Owner = taxReportTrans.Owner;
				taxReportTransItemView.OwnerBusinessUnitGUID = taxReportTrans.OwnerBusinessUnitGUID.GuidNullToString();
				taxReportTransItemView.TaxReportTransGUID = taxReportTrans.TaxReportTransGUID.GuidNullToString();
				taxReportTransItemView.Amount = taxReportTrans.Amount;
				taxReportTransItemView.Description = taxReportTrans.Description;
				taxReportTransItemView.Month = taxReportTrans.Month;
				taxReportTransItemView.RefGUID = taxReportTrans.RefGUID.GuidNullToString();
				taxReportTransItemView.RefId = taxReportTrans.RefId;
				taxReportTransItemView.RefType = taxReportTrans.RefType;
				taxReportTransItemView.Year = taxReportTrans.Year;
				
				taxReportTransItemView.RowVersion = taxReportTrans.RowVersion;
				return taxReportTransItemView.GetTaxReportTransItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion TaxReportTransItemView
		#region ToDropDown
		public static SelectItem<TaxReportTransItemView> ToDropDownItem(this TaxReportTransItemView taxReportTransView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<TaxReportTransItemView> selectItem = new SelectItem<TaxReportTransItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(taxReportTransView.Year.ToString(), taxReportTransView.Month.ToString());
				selectItem.Value = taxReportTransView.TaxReportTransGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: taxReportTransView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<TaxReportTransItemView>> ToDropDownItem(this IEnumerable<TaxReportTransItemView> taxReportTransItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<TaxReportTransItemView>> selectItems = new List<SelectItem<TaxReportTransItemView>>();
				foreach (TaxReportTransItemView item in taxReportTransItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

