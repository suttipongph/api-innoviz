using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
    public static class EmployeeTableHandler
    {
        #region EmployeeTableListView
        public static List<EmployeeTableListView> GetEmployeeTableListViewValidation(this List<EmployeeTableListView> employeeTableListViews, bool skipMethod = false)
        {
            if (skipMethod)
            {
                return employeeTableListViews;
            }
            var result = new List<EmployeeTableListView>();
            try
            {
                foreach (EmployeeTableListView item in employeeTableListViews)
                {
                    result.Add(item.GetEmployeeTableListViewValidation());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static EmployeeTableListView GetEmployeeTableListViewValidation(this EmployeeTableListView employeeTableListView)
        {
            try
            {
                employeeTableListView.RowAuthorize = employeeTableListView.GetListRowAuthorize();
                return employeeTableListView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetListRowAuthorize(this EmployeeTableListView employeeTableListView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        #endregion EmployeeTableListView
        #region EmployeeTableItemView
        public static EmployeeTableItemView GetEmployeeTableItemViewValidation(this EmployeeTableItemView employeeTableItemView)
        {
            try
            {
                employeeTableItemView.RowAuthorize = employeeTableItemView.GetItemRowAuthorize();
                return employeeTableItemView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private static int GetItemRowAuthorize(this EmployeeTableItemView employeeTableItemView)
        {
            return AccessMode.Full.GetAttrValue();
        }
        public static EmployeeTable ToEmployeeTable(this EmployeeTableItemView employeeTableItemView)
        {
            try
            {
                EmployeeTable employeeTable = new EmployeeTable();
                employeeTable.CompanyGUID = employeeTableItemView.CompanyGUID.StringToGuid();
                employeeTable.CreatedBy = employeeTableItemView.CreatedBy;
                employeeTable.CreatedDateTime = employeeTableItemView.CreatedDateTime.StringToSystemDateTime();
                employeeTable.ModifiedBy = employeeTableItemView.ModifiedBy;
                employeeTable.ModifiedDateTime = employeeTableItemView.ModifiedDateTime.StringToSystemDateTime();
                employeeTable.Owner = employeeTableItemView.Owner;
                employeeTable.OwnerBusinessUnitGUID = employeeTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
                employeeTable.EmployeeTableGUID = employeeTableItemView.EmployeeTableGUID.StringToGuid();
                employeeTable.Dimension1GUID = employeeTableItemView.Dimension1GUID.StringToGuidNull();
                employeeTable.Dimension2GUID = employeeTableItemView.Dimension2GUID.StringToGuidNull();
                employeeTable.Dimension3GUID = employeeTableItemView.Dimension3GUID.StringToGuidNull();
                employeeTable.Dimension4GUID = employeeTableItemView.Dimension4GUID.StringToGuidNull();
                employeeTable.Dimension5GUID = employeeTableItemView.Dimension5GUID.StringToGuidNull();
                employeeTable.EmployeeId = employeeTableItemView.EmployeeId;
                employeeTable.EmplTeamGUID = employeeTableItemView.EmplTeamGUID.StringToGuidNull();
                employeeTable.AssistMD = employeeTableItemView.AssistMD;
                employeeTable.InActive = employeeTableItemView.InActive;
                employeeTable.Name = employeeTableItemView.Name;
                employeeTable.Signature = employeeTableItemView.Signature;
                employeeTable.UserId = employeeTableItemView.UserId.StringToGuidNull();
                employeeTable.DepartmentGUID = employeeTableItemView.DepartmentGUID.StringToGuidNull();
                employeeTable.BusinessUnitGUID = employeeTableItemView.BusinessUnitGUID.StringToGuidNull();
                employeeTable.ReportToEmployeeTableGUID = employeeTableItemView.ReportToEmployeeTableGUID.StringToGuidNull();
                employeeTable.Position = employeeTableItemView.Position;
                
                employeeTable.RowVersion = employeeTableItemView.RowVersion;
                return employeeTable;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<EmployeeTable> ToEmployeeTable(this IEnumerable<EmployeeTableItemView> employeeTableItemViews)
        {
            try
            {
                List<EmployeeTable> employeeTables = new List<EmployeeTable>();
                foreach (EmployeeTableItemView item in employeeTableItemViews)
                {
                    employeeTables.Add(item.ToEmployeeTable());
                }
                return employeeTables;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static EmployeeTableItemView ToEmployeeTableItemView(this EmployeeTable employeeTable)
        {
            try
            {
                EmployeeTableItemView employeeTableItemView = new EmployeeTableItemView();
                employeeTableItemView.CompanyGUID = employeeTable.CompanyGUID.GuidNullToString();
                employeeTableItemView.CreatedBy = employeeTable.CreatedBy;
                employeeTableItemView.CreatedDateTime = employeeTable.CreatedDateTime.DateTimeToString();
                employeeTableItemView.ModifiedBy = employeeTable.ModifiedBy;
                employeeTableItemView.ModifiedDateTime = employeeTable.ModifiedDateTime.DateTimeToString();
                employeeTableItemView.Owner = employeeTable.Owner;
                employeeTableItemView.OwnerBusinessUnitGUID = employeeTable.OwnerBusinessUnitGUID.GuidNullToString();
                employeeTableItemView.EmployeeTableGUID = employeeTable.EmployeeTableGUID.GuidNullToString();
                employeeTableItemView.Dimension1GUID = employeeTable.Dimension1GUID.GuidNullToString();
                employeeTableItemView.Dimension2GUID = employeeTable.Dimension2GUID.GuidNullToString();
                employeeTableItemView.Dimension3GUID = employeeTable.Dimension3GUID.GuidNullToString();
                employeeTableItemView.Dimension4GUID = employeeTable.Dimension4GUID.GuidNullToString();
                employeeTableItemView.Dimension5GUID = employeeTable.Dimension5GUID.GuidNullToString();
                employeeTableItemView.EmployeeId = employeeTable.EmployeeId;
                employeeTableItemView.EmplTeamGUID = employeeTable.EmplTeamGUID.GuidNullToString();
                employeeTableItemView.AssistMD = employeeTable.AssistMD;
                employeeTableItemView.InActive = employeeTable.InActive;
                employeeTableItemView.Name = employeeTable.Name;
                employeeTableItemView.Signature = employeeTable.Signature;
                employeeTableItemView.UserId = employeeTable.UserId.GuidNullToString();
                employeeTableItemView.DepartmentGUID = employeeTable.DepartmentGUID.GuidNullToString();
                employeeTableItemView.BusinessUnitGUID = employeeTable.BusinessUnitGUID.GuidNullToString();
                employeeTableItemView.ReportToEmployeeTableGUID = employeeTable.ReportToEmployeeTableGUID.GuidNullToString();
                
                employeeTableItemView.RowVersion = employeeTable.RowVersion;
                return employeeTableItemView.GetEmployeeTableItemViewValidation();
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion EmployeeTableItemView
        #region ToDropDown
        public static SelectItem<EmployeeTableItemView> ToDropDownItem(this EmployeeTableItemView employeeTableView, bool excludeRowData = false)
        {
            try
            {
                SelectItem<EmployeeTableItemView> selectItem = new SelectItem<EmployeeTableItemView>();
                selectItem.Label = SmartAppUtil.GetDropDownLabel(employeeTableView.EmployeeId, employeeTableView.Name);
                selectItem.Value = employeeTableView.EmployeeTableGUID.GuidNullToString();
                selectItem.RowData = excludeRowData ? null: employeeTableView;
                return selectItem;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public static IEnumerable<SelectItem<EmployeeTableItemView>> ToDropDownItem(this IEnumerable<EmployeeTableItemView> employeeTableItemViews, bool excludeRowData = false)
        {
            try
            {
                List<SelectItem<EmployeeTableItemView>> selectItems = new List<SelectItem<EmployeeTableItemView>>();
                foreach (EmployeeTableItemView item in employeeTableItemViews)
                {
                    selectItems.Add(item.ToDropDownItem(excludeRowData));
                }
                return selectItems.OrderBy(o => o.Label);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion ToDropDown
    }
}

