using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class WithdrawalTableHandler
	{
		#region WithdrawalTableListView
		public static List<WithdrawalTableListView> GetWithdrawalTableListViewValidation(this List<WithdrawalTableListView> withdrawalTableListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return withdrawalTableListViews;
			}
			var result = new List<WithdrawalTableListView>();
			try
			{
				foreach (WithdrawalTableListView item in withdrawalTableListViews)
				{
					result.Add(item.GetWithdrawalTableListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithdrawalTableListView GetWithdrawalTableListViewValidation(this WithdrawalTableListView withdrawalTableListView)
		{
			try
			{
				withdrawalTableListView.RowAuthorize = withdrawalTableListView.GetListRowAuthorize();
				return withdrawalTableListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this WithdrawalTableListView withdrawalTableListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion WithdrawalTableListView
		#region WithdrawalTableItemView
		public static WithdrawalTableItemView GetWithdrawalTableItemViewValidation(this WithdrawalTableItemView withdrawalTableItemView)
		{
			try
			{
				withdrawalTableItemView.RowAuthorize = withdrawalTableItemView.GetItemRowAuthorize();
				return withdrawalTableItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this WithdrawalTableItemView withdrawalTableItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static WithdrawalTable ToWithdrawalTable(this WithdrawalTableItemView withdrawalTableItemView)
		{
			try
			{
				WithdrawalTable withdrawalTable = new WithdrawalTable();
				withdrawalTable.CompanyGUID = withdrawalTableItemView.CompanyGUID.StringToGuid();
				withdrawalTable.CreatedBy = withdrawalTableItemView.CreatedBy;
				withdrawalTable.CreatedDateTime = withdrawalTableItemView.CreatedDateTime.StringToSystemDateTime();
				withdrawalTable.ModifiedBy = withdrawalTableItemView.ModifiedBy;
				withdrawalTable.ModifiedDateTime = withdrawalTableItemView.ModifiedDateTime.StringToSystemDateTime();
				withdrawalTable.Owner = withdrawalTableItemView.Owner;
				withdrawalTable.OwnerBusinessUnitGUID = withdrawalTableItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				withdrawalTable.WithdrawalTableGUID = withdrawalTableItemView.WithdrawalTableGUID.StringToGuid();
				withdrawalTable.AssignmentAgreementTableGUID = withdrawalTableItemView.AssignmentAgreementTableGUID.StringToGuidNull();
				withdrawalTable.AssignmentAmount = withdrawalTableItemView.AssignmentAmount;
				withdrawalTable.BuyerAgreementTableGUID = withdrawalTableItemView.BuyerAgreementTableGUID.StringToGuidNull();
				withdrawalTable.BuyerTableGUID = withdrawalTableItemView.BuyerTableGUID.StringToGuidNull();
				withdrawalTable.CreditAppLineGUID = withdrawalTableItemView.CreditAppLineGUID.StringToGuidNull();
				withdrawalTable.CreditAppTableGUID = withdrawalTableItemView.CreditAppTableGUID.StringToGuid();
				withdrawalTable.CreditComment = withdrawalTableItemView.CreditComment;
				withdrawalTable.CreditMarkComment = withdrawalTableItemView.CreditMarkComment;
				withdrawalTable.CreditTermGUID = withdrawalTableItemView.CreditTermGUID.StringToGuid();
				withdrawalTable.CustomerTableGUID = withdrawalTableItemView.CustomerTableGUID.StringToGuid();
				withdrawalTable.Description = withdrawalTableItemView.Description;
				withdrawalTable.Dimension1GUID = withdrawalTableItemView.Dimension1GUID.StringToGuidNull();
				withdrawalTable.Dimension2GUID = withdrawalTableItemView.Dimension2GUID.StringToGuidNull();
				withdrawalTable.Dimension3GUID = withdrawalTableItemView.Dimension3GUID.StringToGuidNull();
				withdrawalTable.Dimension4GUID = withdrawalTableItemView.Dimension4GUID.StringToGuidNull();
				withdrawalTable.Dimension5GUID = withdrawalTableItemView.Dimension5GUID.StringToGuidNull();
				withdrawalTable.DocumentStatusGUID = withdrawalTableItemView.DocumentStatusGUID.StringToGuid();
				withdrawalTable.DueDate = withdrawalTableItemView.DueDate.StringToDate();
				withdrawalTable.ExtendInterestDate = withdrawalTableItemView.ExtendInterestDate.StringNullToDateNull();
				withdrawalTable.ExtendWithdrawalTableGUID = withdrawalTableItemView.ExtendWithdrawalTableGUID.StringToGuidNull();
				withdrawalTable.InterestCutDay = withdrawalTableItemView.InterestCutDay;
				withdrawalTable.MarketingComment = withdrawalTableItemView.MarketingComment;
				withdrawalTable.MarketingMarkComment = withdrawalTableItemView.MarketingMarkComment;
				withdrawalTable.MethodOfPaymentGUID = withdrawalTableItemView.MethodOfPaymentGUID.StringToGuidNull();
				withdrawalTable.NumberOfExtension = withdrawalTableItemView.NumberOfExtension;
				withdrawalTable.OperationComment = withdrawalTableItemView.OperationComment;
				withdrawalTable.OperationMarkComment = withdrawalTableItemView.OperationMarkComment;
				withdrawalTable.OriginalWithdrawalTableGUID = withdrawalTableItemView.OriginalWithdrawalTableGUID.StringToGuidNull();
				withdrawalTable.ProductType = withdrawalTableItemView.ProductType;
				withdrawalTable.ReceiptTempTableGUID = withdrawalTableItemView.ReceiptTempTableGUID.StringToGuidNull();
				withdrawalTable.Remark = withdrawalTableItemView.Remark;
				withdrawalTable.RetentionAmount = withdrawalTableItemView.RetentionAmount;
				withdrawalTable.RetentionCalculateBase = withdrawalTableItemView.RetentionCalculateBase;
				withdrawalTable.RetentionPct = withdrawalTableItemView.RetentionPct;
				withdrawalTable.SettleTermExtensionFeeAmount = withdrawalTableItemView.SettleTermExtensionFeeAmount;
				withdrawalTable.TermExtension = withdrawalTableItemView.TermExtension;
				withdrawalTable.TermExtensionFeeAmount = withdrawalTableItemView.TermExtensionFeeAmount;
				withdrawalTable.TermExtensionInvoiceRevenueTypeGUID = withdrawalTableItemView.TermExtensionInvoiceRevenueTypeGUID.StringToGuidNull();
				withdrawalTable.TotalInterestPct = withdrawalTableItemView.TotalInterestPct;
				withdrawalTable.WithdrawalAmount = withdrawalTableItemView.WithdrawalAmount;
				withdrawalTable.WithdrawalDate = withdrawalTableItemView.WithdrawalDate.StringToDate();
				withdrawalTable.WithdrawalId = withdrawalTableItemView.WithdrawalId;
				withdrawalTable.DocumentReasonGUID = withdrawalTableItemView.DocumentReasonGUID.StringToGuidNull();
				withdrawalTable.OperReportSignatureGUID = withdrawalTableItemView.OperReportSignatureGUID.StringToGuid();
				
				
				withdrawalTable.RowVersion = withdrawalTableItemView.RowVersion;
				return withdrawalTable;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<WithdrawalTable> ToWithdrawalTable(this IEnumerable<WithdrawalTableItemView> withdrawalTableItemViews)
		{
			try
			{
				List<WithdrawalTable> withdrawalTables = new List<WithdrawalTable>();
				foreach (WithdrawalTableItemView item in withdrawalTableItemViews)
				{
					withdrawalTables.Add(item.ToWithdrawalTable());
				}
				return withdrawalTables;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static WithdrawalTableItemView ToWithdrawalTableItemView(this WithdrawalTable withdrawalTable)
		{
			try
			{
				WithdrawalTableItemView withdrawalTableItemView = new WithdrawalTableItemView();
				withdrawalTableItemView.CompanyGUID = withdrawalTable.CompanyGUID.GuidNullToString();
				withdrawalTableItemView.CreatedBy = withdrawalTable.CreatedBy;
				withdrawalTableItemView.CreatedDateTime = withdrawalTable.CreatedDateTime.DateTimeToString();
				withdrawalTableItemView.ModifiedBy = withdrawalTable.ModifiedBy;
				withdrawalTableItemView.ModifiedDateTime = withdrawalTable.ModifiedDateTime.DateTimeToString();
				withdrawalTableItemView.Owner = withdrawalTable.Owner;
				withdrawalTableItemView.OwnerBusinessUnitGUID = withdrawalTable.OwnerBusinessUnitGUID.GuidNullToString();
				withdrawalTableItemView.WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID.GuidNullToString();
				withdrawalTableItemView.AssignmentAgreementTableGUID = withdrawalTable.AssignmentAgreementTableGUID.GuidNullToString();
				withdrawalTableItemView.AssignmentAmount = withdrawalTable.AssignmentAmount;
				withdrawalTableItemView.BuyerAgreementTableGUID = withdrawalTable.BuyerAgreementTableGUID.GuidNullToString();
				withdrawalTableItemView.BuyerTableGUID = withdrawalTable.BuyerTableGUID.GuidNullToString();
				withdrawalTableItemView.CreditAppLineGUID = withdrawalTable.CreditAppLineGUID.GuidNullToString();
				withdrawalTableItemView.CreditAppTableGUID = withdrawalTable.CreditAppTableGUID.GuidNullToString();
				withdrawalTableItemView.CreditComment = withdrawalTable.CreditComment;
				withdrawalTableItemView.CreditMarkComment = withdrawalTable.CreditMarkComment;
				withdrawalTableItemView.CreditTermGUID = withdrawalTable.CreditTermGUID.GuidNullToString();
				withdrawalTableItemView.CustomerTableGUID = withdrawalTable.CustomerTableGUID.GuidNullToString();
				withdrawalTableItemView.Description = withdrawalTable.Description;
				withdrawalTableItemView.Dimension1GUID = withdrawalTable.Dimension1GUID.GuidNullToString();
				withdrawalTableItemView.Dimension2GUID = withdrawalTable.Dimension2GUID.GuidNullToString();
				withdrawalTableItemView.Dimension3GUID = withdrawalTable.Dimension3GUID.GuidNullToString();
				withdrawalTableItemView.Dimension4GUID = withdrawalTable.Dimension4GUID.GuidNullToString();
				withdrawalTableItemView.Dimension5GUID = withdrawalTable.Dimension5GUID.GuidNullToString();
				withdrawalTableItemView.DocumentStatusGUID = withdrawalTable.DocumentStatusGUID.GuidNullToString();
				withdrawalTableItemView.DueDate = withdrawalTable.DueDate.DateToString();
				withdrawalTableItemView.ExtendInterestDate = withdrawalTable.ExtendInterestDate.DateNullToString();
				withdrawalTableItemView.ExtendWithdrawalTableGUID = withdrawalTable.ExtendWithdrawalTableGUID.GuidNullToString();
				withdrawalTableItemView.InterestCutDay = withdrawalTable.InterestCutDay;
				withdrawalTableItemView.MarketingComment = withdrawalTable.MarketingComment;
				withdrawalTableItemView.MarketingMarkComment = withdrawalTable.MarketingMarkComment;
				withdrawalTableItemView.MethodOfPaymentGUID = withdrawalTable.MethodOfPaymentGUID.GuidNullToString();
				withdrawalTableItemView.NumberOfExtension = withdrawalTable.NumberOfExtension;
				withdrawalTableItemView.OperationComment = withdrawalTable.OperationComment;
				withdrawalTableItemView.OperationMarkComment = withdrawalTable.OperationMarkComment;
				withdrawalTableItemView.OriginalWithdrawalTableGUID = withdrawalTable.OriginalWithdrawalTableGUID.GuidNullToString();
				withdrawalTableItemView.ProductType = withdrawalTable.ProductType;
				withdrawalTableItemView.ReceiptTempTableGUID = withdrawalTable.ReceiptTempTableGUID.GuidNullToString();
				withdrawalTableItemView.Remark = withdrawalTable.Remark;
				withdrawalTableItemView.RetentionAmount = withdrawalTable.RetentionAmount;
				withdrawalTableItemView.RetentionCalculateBase = withdrawalTable.RetentionCalculateBase;
				withdrawalTableItemView.RetentionPct = withdrawalTable.RetentionPct;
				withdrawalTableItemView.SettleTermExtensionFeeAmount = withdrawalTable.SettleTermExtensionFeeAmount;
				withdrawalTableItemView.TermExtension = withdrawalTable.TermExtension;
				withdrawalTableItemView.TermExtensionFeeAmount = withdrawalTable.TermExtensionFeeAmount;
				withdrawalTableItemView.TermExtensionInvoiceRevenueTypeGUID = withdrawalTable.TermExtensionInvoiceRevenueTypeGUID.GuidNullToString();
				withdrawalTableItemView.TotalInterestPct = withdrawalTable.TotalInterestPct;
				withdrawalTableItemView.WithdrawalAmount = withdrawalTable.WithdrawalAmount;
				withdrawalTableItemView.WithdrawalDate = withdrawalTable.WithdrawalDate.DateToString();
				withdrawalTableItemView.WithdrawalId = withdrawalTable.WithdrawalId;
				withdrawalTableItemView.DocumentReasonGUID = withdrawalTable.DocumentReasonGUID.GuidNullToString();
				withdrawalTableItemView.OperReportSignatureGUID = withdrawalTable.OperReportSignatureGUID.GuidNullToString();
				
				withdrawalTableItemView.RowVersion = withdrawalTable.RowVersion;
				return withdrawalTableItemView.GetWithdrawalTableItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion WithdrawalTableItemView
		#region ToDropDown
		public static SelectItem<WithdrawalTableItemView> ToDropDownItem(this WithdrawalTableItemView withdrawalTableView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<WithdrawalTableItemView> selectItem = new SelectItem<WithdrawalTableItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(withdrawalTableView.WithdrawalId, withdrawalTableView.Description);
				selectItem.Value = withdrawalTableView.WithdrawalTableGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: withdrawalTableView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<WithdrawalTableItemView>> ToDropDownItem(this IEnumerable<WithdrawalTableItemView> withdrawalTableItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<WithdrawalTableItemView>> selectItems = new List<SelectItem<WithdrawalTableItemView>>();
				foreach (WithdrawalTableItemView item in withdrawalTableItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

