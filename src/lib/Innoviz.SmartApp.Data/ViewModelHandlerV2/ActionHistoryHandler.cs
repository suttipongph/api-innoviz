using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innoviz.SmartApp.Data.ViewModelHandlerV2
{
	public static class ActionHistoryHandler
	{
		#region ActionHistoryListView
		public static List<ActionHistoryListView> GetActionHistoryListViewValidation(this List<ActionHistoryListView> actionHistoryListViews, bool skipMethod = false)
		{
			if (skipMethod)
			{
				return actionHistoryListViews;
			}
			var result = new List<ActionHistoryListView>();
			try
			{
				foreach (ActionHistoryListView item in actionHistoryListViews)
				{
					result.Add(item.GetActionHistoryListViewValidation());
				}
				return result;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ActionHistoryListView GetActionHistoryListViewValidation(this ActionHistoryListView actionHistoryListView)
		{
			try
			{
				actionHistoryListView.RowAuthorize = actionHistoryListView.GetListRowAuthorize();
				return actionHistoryListView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetListRowAuthorize(this ActionHistoryListView actionHistoryListView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		#endregion ActionHistoryListView
		#region ActionHistoryItemView
		public static ActionHistoryItemView GetActionHistoryItemViewValidation(this ActionHistoryItemView actionHistoryItemView)
		{
			try
			{
				actionHistoryItemView.RowAuthorize = actionHistoryItemView.GetItemRowAuthorize();
				return actionHistoryItemView;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		private static int GetItemRowAuthorize(this ActionHistoryItemView actionHistoryItemView)
		{
			return AccessMode.Full.GetAttrValue();
		}
		public static ActionHistory ToActionHistory(this ActionHistoryItemView actionHistoryItemView)
		{
			try
			{
				ActionHistory actionHistory = new ActionHistory();
				actionHistory.CompanyGUID = actionHistoryItemView.CompanyGUID.StringToGuid();
				actionHistory.CreatedBy = actionHistoryItemView.CreatedBy;
				actionHistory.CreatedDateTime = actionHistoryItemView.CreatedDateTime.StringToSystemDateTime();
				actionHistory.ModifiedBy = actionHistoryItemView.ModifiedBy;
				actionHistory.ModifiedDateTime = actionHistoryItemView.ModifiedDateTime.StringToSystemDateTime();
				actionHistory.Owner = actionHistoryItemView.Owner;
				actionHistory.OwnerBusinessUnitGUID = actionHistoryItemView.OwnerBusinessUnitGUID.StringToGuidNull();
				actionHistory.ActionHistoryGUID = actionHistoryItemView.ActionHistoryGUID.StringToGuid();
				actionHistory.SerialNo = actionHistoryItemView.SerialNo;
				actionHistory.WorkflowName = actionHistoryItemView.WorkflowName;
				actionHistory.ActivityName = actionHistoryItemView.ActivityName;
				actionHistory.ActionName = actionHistoryItemView.ActionName;
				actionHistory.Comment = actionHistoryItemView.Comment;
				actionHistory.RefGUID = actionHistoryItemView.RefGUID.StringToGuid();
				
				actionHistory.RowVersion = actionHistoryItemView.RowVersion;
				return actionHistory;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<ActionHistory> ToActionHistory(this IEnumerable<ActionHistoryItemView> actionHistoryItemViews)
		{
			try
			{
				List<ActionHistory> actionHistorys = new List<ActionHistory>();
				foreach (ActionHistoryItemView item in actionHistoryItemViews)
				{
					actionHistorys.Add(item.ToActionHistory());
				}
				return actionHistorys;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static ActionHistoryItemView ToActionHistoryItemView(this ActionHistory actionHistory)
		{
			try
			{
				ActionHistoryItemView actionHistoryItemView = new ActionHistoryItemView();
				actionHistoryItemView.CompanyGUID = actionHistory.CompanyGUID.GuidNullToString();
				actionHistoryItemView.CreatedBy = actionHistory.CreatedBy;
				actionHistoryItemView.CreatedDateTime = actionHistory.CreatedDateTime.DateTimeToString();
				actionHistoryItemView.ModifiedBy = actionHistory.ModifiedBy;
				actionHistoryItemView.ModifiedDateTime = actionHistory.ModifiedDateTime.DateTimeToString();
				actionHistoryItemView.Owner = actionHistory.Owner;
				actionHistoryItemView.OwnerBusinessUnitGUID = actionHistory.OwnerBusinessUnitGUID.GuidNullToString();
				actionHistoryItemView.ActionHistoryGUID = actionHistory.ActionHistoryGUID.GuidNullToString();
				actionHistoryItemView.SerialNo = actionHistory.SerialNo;
				actionHistoryItemView.WorkflowName = actionHistory.WorkflowName;
				actionHistoryItemView.ActivityName = actionHistory.ActivityName;
				actionHistoryItemView.ActionName = actionHistory.ActionName;
				actionHistoryItemView.Comment = actionHistory.Comment;
				actionHistoryItemView.RefGUID = actionHistory.RefGUID.GuidNullOrEmptyToString();
				
				actionHistoryItemView.RowVersion = actionHistory.RowVersion;
				return actionHistoryItemView.GetActionHistoryItemViewValidation();
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ActionHistoryItemView
		#region ToDropDown
		public static SelectItem<ActionHistoryItemView> ToDropDownItem(this ActionHistoryItemView actionHistoryView, bool excludeRowData = false)
		{
			try
			{
				SelectItem<ActionHistoryItemView> selectItem = new SelectItem<ActionHistoryItemView>();
				selectItem.Label = SmartAppUtil.GetDropDownLabel(actionHistoryView.WorkflowName, actionHistoryView.ActivityName);
				selectItem.Value = actionHistoryView.ActionHistoryGUID.GuidNullToString();
				selectItem.RowData = excludeRowData ? null: actionHistoryView;
				return selectItem;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public static IEnumerable<SelectItem<ActionHistoryItemView>> ToDropDownItem(this IEnumerable<ActionHistoryItemView> actionHistoryItemViews, bool excludeRowData = false)
		{
			try
			{
				List<SelectItem<ActionHistoryItemView>> selectItems = new List<SelectItem<ActionHistoryItemView>>();
				foreach (ActionHistoryItemView item in actionHistoryItemViews)
				{
					selectItems.Add(item.ToDropDownItem(excludeRowData));
				}
				return selectItems.OrderBy(o => o.Label);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		#endregion ToDropDown
	}
}

