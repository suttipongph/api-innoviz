using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IDocumentConditionTransService
    {
		DocumentConditionTransItemView GetDocumentConditionTransById(string id);
		DocumentConditionTransItemView CreateDocumentConditionTrans(DocumentConditionTransItemView documentConditionTransView);
		DocumentConditionTransItemView UpdateDocumentConditionTrans(DocumentConditionTransItemView documentConditionTransView);
		bool DeleteDocumentConditionTrans(string id);
		DocumentConditionTransItemView GetDocumentConditionTransInitialData(string refId, string refGUID, RefType refType);
		IEnumerable<DocumentConditionTrans> GetDocumentConditionTransByReference(Guid refGUID, int refType);
		#region function
		bool GetDocumentTransByDocTypeValidation(CopyDocumentConditionTemplateParamView parm);
		IEnumerable<DocumentConditionTrans> GetDocumentTransByDocType(DocConVerifyTypeModel refType);
		#endregion
		IEnumerable<DocumentConditionTrans> CopyDocumentConditionTrans(DocConVerifyType[] docConVerifyTypes, Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool fromInactive, bool IsAmend = false, string owner = null, Guid? ownerBusinessUnitGUID = null);
		void CreateDocumentConditionTransCollection(IEnumerable<DocumentConditionTransItemView> documentConditionTransItemViews);
	}
    public class DocumentConditionTransService : SmartAppService, IDocumentConditionTransService
    {
        public DocumentConditionTransService(SmartAppDbContext context) : base(context) { }
        public DocumentConditionTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public DocumentConditionTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public DocumentConditionTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public DocumentConditionTransService() : base() { }
		public DocumentConditionTransItemView GetDocumentConditionTransById(string id)
		{
			try
			{
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				return documentConditionTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTransItemView CreateDocumentConditionTrans(DocumentConditionTransItemView documentConditionTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentConditionTransView = accessLevelService.AssignOwnerBU(documentConditionTransView);
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				DocumentConditionTrans documentConditionTrans = documentConditionTransView.ToDocumentConditionTrans();
				documentConditionTrans = documentConditionTransRepo.CreateDocumentConditionTrans(documentConditionTrans);
				base.LogTransactionCreate<DocumentConditionTrans>(documentConditionTrans);
				UnitOfWork.Commit();
				return documentConditionTrans.ToDocumentConditionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTransItemView UpdateDocumentConditionTrans(DocumentConditionTransItemView documentConditionTransView)
		{
			try
			{
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				DocumentConditionTrans inputDocumentConditionTrans = documentConditionTransView.ToDocumentConditionTrans();
				DocumentConditionTrans dbDocumentConditionTrans = documentConditionTransRepo.Find(inputDocumentConditionTrans.DocumentConditionTransGUID);
				dbDocumentConditionTrans = documentConditionTransRepo.UpdateDocumentConditionTrans(dbDocumentConditionTrans, inputDocumentConditionTrans);
				base.LogTransactionUpdate<DocumentConditionTrans>(GetOriginalValues<DocumentConditionTrans>(dbDocumentConditionTrans), dbDocumentConditionTrans);
				UnitOfWork.Commit();
				return dbDocumentConditionTrans.ToDocumentConditionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentConditionTrans(string item)
		{
			try
			{
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				Guid documentConditionTransGUID = new Guid(item);
				DocumentConditionTrans documentConditionTrans = documentConditionTransRepo.Find(documentConditionTransGUID);
				documentConditionTransRepo.Remove(documentConditionTrans);
				base.LogTransactionDelete<DocumentConditionTrans>(documentConditionTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTransItemView GetDocumentConditionTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new DocumentConditionTransItemView
				{
					DocumentConditionTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<DocumentConditionTrans> GetDocumentConditionTransByReference(Guid refGUID, int refType)
		{
			try
			{
				DocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				return documentConditionTransRepo.GetDocumentConditionTransByReference(refGUID, refType);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region function
		public bool GetDocumentTransByDocTypeValidation(CopyDocumentConditionTemplateParamView parm)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				IDocumentConditionTemplateLineRepo documentConditionTemplateLineRepo = new DocumentConditionTemplateLineRepo(db);
				DocConVerifyTypeModel docConVerifyType = new DocConVerifyTypeModel();
				docConVerifyType.DocConVerifyType = parm.DocConVerifyType;
				docConVerifyType.RefGUID = parm.RefGUID.StringToGuid();
				IEnumerable<DocumentConditionTrans> documentConditionTrans = documentConditionTransRepo.GetDocumentTransByDocType(docConVerifyType);
				IEnumerable<DocumentConditionTemplateLine> documentConditionTemplateLines =
					documentConditionTemplateLineRepo.GetDocumentConditionTemplateLineByDocumentConditionTemplateTableGUID(parm.DocumentConditionTemplateTableGUID.StringToGuid());
				if (documentConditionTemplateLines.Count() > 0)
				{
					ex.AddData("CONFIRM.90003");
					return false;
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}

				return documentConditionTrans.Count() > 0 ? false : true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<DocumentConditionTrans> GetDocumentTransByDocType(DocConVerifyTypeModel docType)
		{
			try
			{
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				IEnumerable<DocumentConditionTrans> documentConditionTrans = documentConditionTransRepo.GetDocumentTransByDocType(docType);
				return documentConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion
		public IEnumerable<DocumentConditionTrans> CopyDocumentConditionTrans(DocConVerifyType[] docConVerifyTypes, Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool fromInactive, bool IsAmend = false, string owner = null, Guid? ownerBusinessUnitGUID = null)
		{
			try
			{
				List<int> _docConVerifyTypes = docConVerifyTypes.Select(s => (int)s).ToList();
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				List<DocumentConditionTrans> documentConditionTrans = documentConditionTransRepo
					.GetDocumentConditionTransByReference(fromRefGUID, fromRefType)
					.Where(w => _docConVerifyTypes.Any(a => a == w.DocConVerifyType) && w.Inactive == fromInactive)
					.ToList();
				if (documentConditionTrans.Any())
				{
					documentConditionTrans.ForEach(f =>
					{
						f.DocumentConditionTransGUID = Guid.NewGuid();
						f.RefGUID = toRefGUID;
						f.RefType = toRefType;
						f.Owner = owner;
						f.OwnerBusinessUnitGUID = ownerBusinessUnitGUID;
						if (IsAmend)
						{
							f.RefDocumentConditionTransGUID = f.DocumentConditionTransGUID;
						}
						else
						{
							f.RefDocumentConditionTransGUID = null;
						}
					});
				}
				return documentConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Migration
		public void CreateDocumentConditionTransCollection(IEnumerable<DocumentConditionTransItemView> documentConditionTransItemViews)
		{
			try
			{
				if (documentConditionTransItemViews.Any())
				{
					IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
					List<DocumentConditionTrans> documentConditionTranss = documentConditionTransItemViews.ToDocumentConditionTrans().ToList();
					documentConditionTranss.ForEach(f => f.DocumentConditionTransGUID = Guid.NewGuid());
					documentConditionTransRepo.ValidateAdd(documentConditionTranss);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(documentConditionTranss);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Migration
	}
}

