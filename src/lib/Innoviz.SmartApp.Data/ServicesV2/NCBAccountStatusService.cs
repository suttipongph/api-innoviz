using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface INCBAccountStatusService
	{

		NCBAccountStatusItemView GetNCBAccountStatusById(string id);
		NCBAccountStatusItemView CreateNCBAccountStatus(NCBAccountStatusItemView ncbAccountStatusView);
		NCBAccountStatusItemView UpdateNCBAccountStatus(NCBAccountStatusItemView ncbAccountStatusView);
		bool DeleteNCBAccountStatus(string id);
	}
	public class NCBAccountStatusService : SmartAppService, INCBAccountStatusService
	{
		public NCBAccountStatusService(SmartAppDbContext context) : base(context) { }
		public NCBAccountStatusService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public NCBAccountStatusService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NCBAccountStatusService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public NCBAccountStatusService() : base() { }

		public NCBAccountStatusItemView GetNCBAccountStatusById(string id)
		{
			try
			{
				INCBAccountStatusRepo ncbAccountStatusRepo = new NCBAccountStatusRepo(db);
				return ncbAccountStatusRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NCBAccountStatusItemView CreateNCBAccountStatus(NCBAccountStatusItemView ncbAccountStatusView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ncbAccountStatusView = accessLevelService.AssignOwnerBU(ncbAccountStatusView);
				INCBAccountStatusRepo ncbAccountStatusRepo = new NCBAccountStatusRepo(db);
				NCBAccountStatus ncbAccountStatus = ncbAccountStatusView.ToNCBAccountStatus();
				ncbAccountStatus = ncbAccountStatusRepo.CreateNCBAccountStatus(ncbAccountStatus);
				base.LogTransactionCreate<NCBAccountStatus>(ncbAccountStatus);
				UnitOfWork.Commit();
				return ncbAccountStatus.ToNCBAccountStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NCBAccountStatusItemView UpdateNCBAccountStatus(NCBAccountStatusItemView ncbAccountStatusView)
		{
			try
			{
				INCBAccountStatusRepo ncbAccountStatusRepo = new NCBAccountStatusRepo(db);
				NCBAccountStatus inputNCBAccountStatus = ncbAccountStatusView.ToNCBAccountStatus();
				NCBAccountStatus dbNCBAccountStatus = ncbAccountStatusRepo.Find(inputNCBAccountStatus.NCBAccountStatusGUID);
				dbNCBAccountStatus = ncbAccountStatusRepo.UpdateNCBAccountStatus(dbNCBAccountStatus, inputNCBAccountStatus);
				base.LogTransactionUpdate<NCBAccountStatus>(GetOriginalValues<NCBAccountStatus>(dbNCBAccountStatus), dbNCBAccountStatus);
				UnitOfWork.Commit();
				return dbNCBAccountStatus.ToNCBAccountStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteNCBAccountStatus(string item)
		{
			try
			{
				INCBAccountStatusRepo ncbAccountStatusRepo = new NCBAccountStatusRepo(db);
				Guid ncbAccountStatusGUID = new Guid(item);
				NCBAccountStatus ncbAccountStatus = ncbAccountStatusRepo.Find(ncbAccountStatusGUID);
				ncbAccountStatusRepo.Remove(ncbAccountStatus);
				base.LogTransactionDelete<NCBAccountStatus>(ncbAccountStatus);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
