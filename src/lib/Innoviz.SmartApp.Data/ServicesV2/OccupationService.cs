using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IOccupationService
	{

		OccupationItemView GetOccupationById(string id);
		OccupationItemView CreateOccupation(OccupationItemView occupationView);
		OccupationItemView UpdateOccupation(OccupationItemView occupationView);
		bool DeleteOccupation(string id);
	}
	public class OccupationService : SmartAppService, IOccupationService
	{
		public OccupationService(SmartAppDbContext context) : base(context) { }
		public OccupationService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public OccupationService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public OccupationService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public OccupationService() : base() { }

		public OccupationItemView GetOccupationById(string id)
		{
			try
			{
				IOccupationRepo occupationRepo = new OccupationRepo(db);
				return occupationRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OccupationItemView CreateOccupation(OccupationItemView occupationView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				occupationView = accessLevelService.AssignOwnerBU(occupationView);
				IOccupationRepo occupationRepo = new OccupationRepo(db);
				Occupation occupation = occupationView.ToOccupation();
				occupation = occupationRepo.CreateOccupation(occupation);
				base.LogTransactionCreate<Occupation>(occupation);
				UnitOfWork.Commit();
				return occupation.ToOccupationItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OccupationItemView UpdateOccupation(OccupationItemView occupationView)
		{
			try
			{
				IOccupationRepo occupationRepo = new OccupationRepo(db);
				Occupation inputOccupation = occupationView.ToOccupation();
				Occupation dbOccupation = occupationRepo.Find(inputOccupation.OccupationGUID);
				dbOccupation = occupationRepo.UpdateOccupation(dbOccupation, inputOccupation);
				base.LogTransactionUpdate<Occupation>(GetOriginalValues<Occupation>(dbOccupation), dbOccupation);
				UnitOfWork.Commit();
				return dbOccupation.ToOccupationItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteOccupation(string item)
		{
			try
			{
				IOccupationRepo occupationRepo = new OccupationRepo(db);
				Guid occupationGUID = new Guid(item);
				Occupation occupation = occupationRepo.Find(occupationGUID);
				occupationRepo.Remove(occupation);
				base.LogTransactionDelete<Occupation>(occupation);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
