using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IStagingTableVendorInfoService
	{

		StagingTableVendorInfoItemView GetStagingTableVendorInfoById(string id);
		StagingTableVendorInfoItemView CreateStagingTableVendorInfo(StagingTableVendorInfoItemView stagingTableVendorInfoView);
		StagingTableVendorInfoItemView UpdateStagingTableVendorInfo(StagingTableVendorInfoItemView stagingTableVendorInfoView);
		bool DeleteStagingTableVendorInfo(string id);
	}
	public class StagingTableVendorInfoService : SmartAppService, IStagingTableVendorInfoService
	{
		public StagingTableVendorInfoService(SmartAppDbContext context) : base(context) { }
		public StagingTableVendorInfoService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public StagingTableVendorInfoService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTableVendorInfoService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public StagingTableVendorInfoService() : base() { }

		public StagingTableVendorInfoItemView GetStagingTableVendorInfoById(string id)
		{
			try
			{
				IStagingTableVendorInfoRepo stagingTableVendorInfoRepo = new StagingTableVendorInfoRepo(db);
				return stagingTableVendorInfoRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableVendorInfoItemView CreateStagingTableVendorInfo(StagingTableVendorInfoItemView stagingTableVendorInfoView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				stagingTableVendorInfoView = accessLevelService.AssignOwnerBU(stagingTableVendorInfoView);
				IStagingTableVendorInfoRepo stagingTableVendorInfoRepo = new StagingTableVendorInfoRepo(db);
				StagingTableVendorInfo stagingTableVendorInfo = stagingTableVendorInfoView.ToStagingTableVendorInfo();
				stagingTableVendorInfo = stagingTableVendorInfoRepo.CreateStagingTableVendorInfo(stagingTableVendorInfo);
				base.LogTransactionCreate<StagingTableVendorInfo>(stagingTableVendorInfo);
				UnitOfWork.Commit();
				return stagingTableVendorInfo.ToStagingTableVendorInfoItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableVendorInfoItemView UpdateStagingTableVendorInfo(StagingTableVendorInfoItemView stagingTableVendorInfoView)
		{
			try
			{
				IStagingTableVendorInfoRepo stagingTableVendorInfoRepo = new StagingTableVendorInfoRepo(db);
				StagingTableVendorInfo inputStagingTableVendorInfo = stagingTableVendorInfoView.ToStagingTableVendorInfo();
				StagingTableVendorInfo dbStagingTableVendorInfo = stagingTableVendorInfoRepo.Find(inputStagingTableVendorInfo.StagingTableVendorInfoGUID);
				dbStagingTableVendorInfo = stagingTableVendorInfoRepo.UpdateStagingTableVendorInfo(dbStagingTableVendorInfo, inputStagingTableVendorInfo);
				base.LogTransactionUpdate<StagingTableVendorInfo>(GetOriginalValues<StagingTableVendorInfo>(dbStagingTableVendorInfo), dbStagingTableVendorInfo);
				UnitOfWork.Commit();
				return dbStagingTableVendorInfo.ToStagingTableVendorInfoItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteStagingTableVendorInfo(string item)
		{
			try
			{
				IStagingTableVendorInfoRepo stagingTableVendorInfoRepo = new StagingTableVendorInfoRepo(db);
				Guid stagingTableVendorInfoGUID = new Guid(item);
				StagingTableVendorInfo stagingTableVendorInfo = stagingTableVendorInfoRepo.Find(stagingTableVendorInfoGUID);
				stagingTableVendorInfoRepo.Remove(stagingTableVendorInfo);
				base.LogTransactionDelete<StagingTableVendorInfo>(stagingTableVendorInfo);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
