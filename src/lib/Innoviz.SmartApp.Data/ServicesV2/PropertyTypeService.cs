using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IPropertyTypeService
	{

		PropertyTypeItemView GetPropertyTypeById(string id);
		PropertyTypeItemView CreatePropertyType(PropertyTypeItemView propertyTypeView);
		PropertyTypeItemView UpdatePropertyType(PropertyTypeItemView propertyTypeView);
		bool DeletePropertyType(string id);
	}
	public class PropertyTypeService : SmartAppService, IPropertyTypeService
	{
		public PropertyTypeService(SmartAppDbContext context) : base(context) { }
		public PropertyTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public PropertyTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public PropertyTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public PropertyTypeService() : base() { }

		public PropertyTypeItemView GetPropertyTypeById(string id)
		{
			try
			{
				IPropertyTypeRepo propertyTypeRepo = new PropertyTypeRepo(db);
				return propertyTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PropertyTypeItemView CreatePropertyType(PropertyTypeItemView propertyTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				propertyTypeView = accessLevelService.AssignOwnerBU(propertyTypeView);
				IPropertyTypeRepo propertyTypeRepo = new PropertyTypeRepo(db);
				PropertyType propertyType = propertyTypeView.ToPropertyType();
				propertyType = propertyTypeRepo.CreatePropertyType(propertyType);
				base.LogTransactionCreate<PropertyType>(propertyType);
				UnitOfWork.Commit();
				return propertyType.ToPropertyTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PropertyTypeItemView UpdatePropertyType(PropertyTypeItemView propertyTypeView)
		{
			try
			{
				IPropertyTypeRepo propertyTypeRepo = new PropertyTypeRepo(db);
				PropertyType inputPropertyType = propertyTypeView.ToPropertyType();
				PropertyType dbPropertyType = propertyTypeRepo.Find(inputPropertyType.PropertyTypeGUID);
				dbPropertyType = propertyTypeRepo.UpdatePropertyType(dbPropertyType, inputPropertyType);
				base.LogTransactionUpdate<PropertyType>(GetOriginalValues<PropertyType>(dbPropertyType), dbPropertyType);
				UnitOfWork.Commit();
				return dbPropertyType.ToPropertyTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeletePropertyType(string item)
		{
			try
			{
				IPropertyTypeRepo propertyTypeRepo = new PropertyTypeRepo(db);
				Guid propertyTypeGUID = new Guid(item);
				PropertyType propertyType = propertyTypeRepo.Find(propertyTypeGUID);
				propertyTypeRepo.Remove(propertyType);
				base.LogTransactionDelete<PropertyType>(propertyType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
