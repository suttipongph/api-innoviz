using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IProductSubTypeService
    {

        ProductSubTypeItemView GetProductSubTypeById(string id);
        ProductSubTypeItemView CreateProductSubType(ProductSubTypeItemView productSubTypeView);
        ProductSubTypeItemView UpdateProductSubType(ProductSubTypeItemView productSubTypeView);
        bool DeleteProductSubType(string id);
        IEnumerable<SelectItem<ProductSubTypeItemView>> GetDropDownItemProductSubType(SearchParameter search);
        #region shared
        public DateTime FindActualInterestDate(DateTime date,Guid productSubTypeGuid);
        public DateTime FindActualDueDate(DateTime date, Guid productSubTypeGuid);
        #endregion
    }
    public class ProductSubTypeService : SmartAppService, IProductSubTypeService
    {
        public ProductSubTypeService(SmartAppDbContext context) : base(context) { }
        public ProductSubTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public ProductSubTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ProductSubTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public ProductSubTypeService() : base() { }

        public ProductSubTypeItemView GetProductSubTypeById(string id)
        {
            try
            {
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                return productSubTypeRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProductSubTypeItemView CreateProductSubType(ProductSubTypeItemView productSubTypeView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                productSubTypeView = accessLevelService.AssignOwnerBU(productSubTypeView);
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                ProductSubType productSubType = productSubTypeView.ToProductSubType();
                productSubType = productSubTypeRepo.CreateProductSubType(productSubType);
                base.LogTransactionCreate<ProductSubType>(productSubType);
                UnitOfWork.Commit();
                return productSubType.ToProductSubTypeItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProductSubTypeItemView UpdateProductSubType(ProductSubTypeItemView productSubTypeView)
        {
            try
            {
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                ProductSubType inputProductSubType = productSubTypeView.ToProductSubType();
                ProductSubType dbProductSubType = productSubTypeRepo.Find(inputProductSubType.ProductSubTypeGUID);
                dbProductSubType = productSubTypeRepo.UpdateProductSubType(dbProductSubType, inputProductSubType);
                base.LogTransactionUpdate<ProductSubType>(GetOriginalValues<ProductSubType>(dbProductSubType), dbProductSubType);
                UnitOfWork.Commit();
                return dbProductSubType.ToProductSubTypeItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteProductSubType(string item)
        {
            try
            {
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                Guid productSubTypeGUID = new Guid(item);
                ProductSubType productSubType = productSubTypeRepo.Find(productSubTypeGUID);
                productSubTypeRepo.Remove(productSubType);
                base.LogTransactionDelete<ProductSubType>(productSubType);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ProductSubTypeItemView>> GetDropDownItemProductSubType(SearchParameter search)
        {
            try
            {
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                search = search.GetParentCondition(ProductSubTypeCondition.ProductType);
                return productSubTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region shared
        public DateTime FindActualInterestDate(DateTime date,Guid productSubTypeGuid)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                ICalendarNonWorkingDateService calendarNonWorkingDateService = new CalendarNonWorkingDateService(db);
                ProductSubType productSubType = productSubTypeRepo.GetProductSubTypeByIdNoTracking(productSubTypeGuid);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(db.GetCompanyFilter().StringToGuid());
                if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextCompanyCalendar)
                {
                    if (companyParameter.CompanyCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.COMPANY_CALENDAR_GROUP_ID" });
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    else if (productSubType.CalcInterestDayMethod == (int)CalcInterestDayMethod.NextDay)
                    {
                        date = date.AddDays(1);
                    }
                    date = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.CompanyCalendarGroupGUID.GetValueOrDefault());
                }
                else if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextBOTCalendar)
                {
                    if (companyParameter.BOTCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.BOT_CALENDAR_GROUP_ID" });
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    else if (productSubType.CalcInterestDayMethod == (int)CalcInterestDayMethod.NextDay)
                    {
                        date = date.AddDays(1);
                    }
                    date = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.BOTCalendarGroupGUID.GetValueOrDefault());
                }
                else if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextMinCalendar || productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextMaxCalendar)
                {
                    DateTime comDate, botDate;
                    if (companyParameter.BOTCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.BOT_CALENDAR_GROUP_ID" });
                    }
                    if (companyParameter.CompanyCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.COMPANY_CALENDAR_GROUP_ID" });
                    }
                    if (ex.Data.Count > 0)
                    {
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    else if (productSubType.CalcInterestDayMethod == (int)CalcInterestDayMethod.NextDay)
                    {
                        date = date.AddDays(1);
                    }
                    comDate = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.CompanyCalendarGroupGUID.GetValueOrDefault());
                    botDate = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.BOTCalendarGroupGUID.GetValueOrDefault());
                    if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextMinCalendar)
                    {
                        date = comDate < botDate ? comDate : botDate;
                    }
                    else
                    {
                        date = comDate > botDate ? comDate : botDate;
                    }
                }
                return date;
            }
            catch (Exception e)
            {
              throw  SmartAppUtil.AddStackTrace(e);
            }
        }
        public DateTime FindActualDueDate(DateTime date, Guid productSubTypeGuid)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                ICalendarNonWorkingDateService calendarNonWorkingDateService = new CalendarNonWorkingDateService(db);
                ProductSubType productSubType = productSubTypeRepo.GetProductSubTypeByIdNoTracking(productSubTypeGuid);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(db.GetCompanyFilter().StringToGuid());
                if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextCompanyCalendar)
                {
                    if (companyParameter.CompanyCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.COMPANY_CALENDAR_GROUP_ID" });
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    date = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.CompanyCalendarGroupGUID.GetValueOrDefault());
                }
                else if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextBOTCalendar)
                {
                    if (companyParameter.BOTCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.BOT_CALENDAR_GROUP_ID" });
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    date = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.BOTCalendarGroupGUID.GetValueOrDefault());
                }
                else if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextMinCalendar || productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextMaxCalendar)
                {
                    DateTime comDate, botDate;
                    if (companyParameter.BOTCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.BOT_CALENDAR_GROUP_ID" });
                    }
                    if (companyParameter.CompanyCalendarGroupGUID == null)
                    {
                        ex.AddData("ERROR.90037", new string[] { "LABEL.COMPANY_CALENDAR_GROUP_ID" });
                    }
                    if (ex.Data.Count > 0)
                    {
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    comDate = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.CompanyCalendarGroupGUID.GetValueOrDefault());
                    botDate = calendarNonWorkingDateService.FindWorkingDate(date, companyParameter.BOTCalendarGroupGUID.GetValueOrDefault());
                    if (productSubType.CalcInterestMethod == (int)CalcInterestMethod.NextMinCalendar)
                    {
                        date = comDate < botDate ? comDate : botDate;
                    }
                    else
                    {
                        date = comDate > botDate ? comDate : botDate;
                    }
                }
                return date;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
