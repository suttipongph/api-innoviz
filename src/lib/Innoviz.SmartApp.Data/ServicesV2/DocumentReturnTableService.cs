using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDocumentReturnTableService
	{

		DocumentReturnLineItemView GetDocumentReturnLineById(string id);
		DocumentReturnLineItemView CreateDocumentReturnLine(DocumentReturnLineItemView documentReturnLineView);
		DocumentReturnLineItemView UpdateDocumentReturnLine(DocumentReturnLineItemView documentReturnLineView);
		bool DeleteDocumentReturnLine(string id);
		DocumentReturnTableItemView GetDocumentReturnTableById(string id);
		DocumentReturnTableItemView CreateDocumentReturnTable(DocumentReturnTableItemView documentReturnTableView);
		DocumentReturnTableItemView UpdateDocumentReturnTable(DocumentReturnTableItemView documentReturnTableView);
		bool DeleteDocumentReturnTable(string id);
		bool ValidateIsManualNumberSeq(string companyId);
		DocumentReturnTableItemView InitialDataDocumentReturnTable(string id);
		IEnumerable<SelectItem<AddressTransItemView>> GetAddressTransByCustomerDropDown(SearchParameter search);
		IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTransByBuyer(SearchParameter search);
		
		AccessModeView GetAccessModeByDocumentReturnTable(string documentReturnTableId);
		
		DocumentReturnLineItemView InitialDatatDocumentReturnLine(string id);
		DocumentReturnTableItemView GetManageDocumentReturnInitialData(string id, string statusId);
		DocumentReturnTableItemView UpdateManageDocumentReturnStatus(DocumentReturnTableItemView documentReturnTableView);
		bool GetDocumentStatusPostValidation(DocumentReturnTableItemView vmodel);
		bool GetDocumentStatusCloseValidation(DocumentReturnTableItemView vmodel);
		PrintDocumentReturnLabelLetterView GetPrintDocumentReturnLabelLetter(string documentReturnGUID);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusDocumentProcessDropdown(SearchParameter search);


	}
	public class DocumentReturnTableService : SmartAppService, IDocumentReturnTableService
	{
		public DocumentReturnTableService(SmartAppDbContext context) : base(context) { }
		public DocumentReturnTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DocumentReturnTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentReturnTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DocumentReturnTableService() : base() { }

		public DocumentReturnLineItemView GetDocumentReturnLineById(string id)
		{
			try
			{
				IDocumentReturnLineRepo documentReturnLineRepo = new DocumentReturnLineRepo(db);
				return documentReturnLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnLineItemView CreateDocumentReturnLine(DocumentReturnLineItemView documentReturnLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentReturnLineView = accessLevelService.AssignOwnerBU(documentReturnLineView);
				IDocumentReturnLineRepo documentReturnLineRepo = new DocumentReturnLineRepo(db);
				DocumentReturnLine documentReturnLine = documentReturnLineView.ToDocumentReturnLine();
				documentReturnLine = documentReturnLineRepo.CreateDocumentReturnLine(documentReturnLine);
				base.LogTransactionCreate<DocumentReturnLine>(documentReturnLine);
				UnitOfWork.Commit();
				return documentReturnLine.ToDocumentReturnLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnLineItemView UpdateDocumentReturnLine(DocumentReturnLineItemView documentReturnLineView)
		{
			try
			{
				IDocumentReturnLineRepo documentReturnLineRepo = new DocumentReturnLineRepo(db);
				DocumentReturnLine inputDocumentReturnLine = documentReturnLineView.ToDocumentReturnLine();
				DocumentReturnLine dbDocumentReturnLine = documentReturnLineRepo.Find(inputDocumentReturnLine.DocumentReturnLineGUID);
				dbDocumentReturnLine = documentReturnLineRepo.UpdateDocumentReturnLine(dbDocumentReturnLine, inputDocumentReturnLine);
				base.LogTransactionUpdate<DocumentReturnLine>(GetOriginalValues<DocumentReturnLine>(dbDocumentReturnLine), dbDocumentReturnLine);
				UnitOfWork.Commit();
				return dbDocumentReturnLine.ToDocumentReturnLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentReturnLine(string item)
		{
			try
			{
				IDocumentReturnLineRepo documentReturnLineRepo = new DocumentReturnLineRepo(db);
				Guid documentReturnLineGUID = new Guid(item);
				DocumentReturnLine documentReturnLine = documentReturnLineRepo.Find(documentReturnLineGUID);
				documentReturnLineRepo.Remove(documentReturnLine);
				base.LogTransactionDelete<DocumentReturnLine>(documentReturnLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public DocumentReturnTableItemView GetDocumentReturnTableById(string id)
		{
			try
			{
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				return documentReturnTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnTableItemView CreateDocumentReturnTable(DocumentReturnTableItemView documentReturnTableView)
		{
			try
			{
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentReturnTableView = accessLevelService.AssignOwnerBU(documentReturnTableView);
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				var isManual = numberSequenceService.IsManualByReferenceId(new Guid(documentReturnTableView.CompanyGUID), ReferenceId.DocumentReturn);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(documentReturnTableView.CompanyGUID.StringToGuid(), ReferenceId.DocumentReturn);
					documentReturnTableView.DocumentReturnId = numberSequenceService.GetNumber(documentReturnTableView.CompanyGUID.StringToGuid(), null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
				DocumentReturnTable documentReturnTable = documentReturnTableView.ToDocumentReturnTable();
				documentReturnTable = documentReturnTableRepo.CreateDocumentReturnTable(documentReturnTable);
				base.LogTransactionCreate<DocumentReturnTable>(documentReturnTable);
				UnitOfWork.Commit();
				return documentReturnTable.ToDocumentReturnTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public DocumentReturnTableItemView UpdateManageDocumentReturnStatus(DocumentReturnTableItemView documentReturnTableView)
		{
			try
			{
				NotificationResponse success = new NotificationResponse();
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				DocumentReturnTable inputDocumentReturnTable = documentReturnTableView.ToDocumentReturnTable();
				DocumentReturnTable dbDocumentReturnTable = documentReturnTableRepo.Find(inputDocumentReturnTable.DocumentReturnTableGUID);
				dbDocumentReturnTable = documentReturnTableRepo.UpdateDocumentReturnTable(dbDocumentReturnTable, inputDocumentReturnTable);
				base.LogTransactionUpdate<DocumentReturnTable>(GetOriginalValues<DocumentReturnTable>(dbDocumentReturnTable), dbDocumentReturnTable);
				UnitOfWork.Commit();
				var result = dbDocumentReturnTable.ToDocumentReturnTableItemView();
				success.AddData("SUCCESS.90012", new string[] { documentReturnTableView.DocumentStatus_Values, documentReturnTableView.OriginalDocumentStatus });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnTableItemView UpdateDocumentReturnTable(DocumentReturnTableItemView documentReturnTableView)
		{
			try
			{
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				DocumentReturnTable inputDocumentReturnTable = documentReturnTableView.ToDocumentReturnTable();
				DocumentReturnTable dbDocumentReturnTable = documentReturnTableRepo.Find(inputDocumentReturnTable.DocumentReturnTableGUID);
				dbDocumentReturnTable = documentReturnTableRepo.UpdateDocumentReturnTable(dbDocumentReturnTable, inputDocumentReturnTable);
				base.LogTransactionUpdate<DocumentReturnTable>(GetOriginalValues<DocumentReturnTable>(dbDocumentReturnTable), dbDocumentReturnTable);
				UnitOfWork.Commit();
				return dbDocumentReturnTable.ToDocumentReturnTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentReturnTable(string item)
		{
			try
			{
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				Guid documentReturnTableGUID = new Guid(item);
				DocumentReturnTable documentReturnTable = documentReturnTableRepo.Find(documentReturnTableGUID);
				documentReturnTableRepo.Remove(documentReturnTable);
				base.LogTransactionDelete<DocumentReturnTable>(documentReturnTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool ValidateIsManualNumberSeq(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.DocumentReturn);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public DocumentReturnTableItemView InitialDataDocumentReturnTable(string companyGUID)
		{
			try
			{
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				var documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)DocumentReturnStatus.Draft).ToString());
				string userId = db.GetUserId();
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				var employee = employeeTableRepo.GetEmployeeTableByUserIdAndCompany(userId, companyGUID);
				DocumentReturnTableItemView documentReturnTableItemView = new DocumentReturnTableItemView();
				documentReturnTableItemView.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
				documentReturnTableItemView.RequestorGUID = employee.EmployeeTableGUID.ToString();
				documentReturnTableItemView.DocumentStatus_Values = documentStatus.Description;
				documentReturnTableItemView.DocumentStatus_StatusId = documentStatus.StatusId;
				return documentReturnTableItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region DropdownByCondition
		public IEnumerable<SelectItem<AddressTransItemView>> GetAddressTransByCustomerDropDown(SearchParameter search)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				search = search.GetParentCondition(AddressTransCondition.RefGUID);
				SearchCondition searchCondition = SearchConditionService.GetAddressTransRefTypeCondition((int)RefType.Customer);
				search.Conditions.Add(searchCondition);
				return addressTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTransByBuyer(SearchParameter search)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				search = search.GetParentCondition(AddressTransCondition.RefGUID);
				SearchCondition searchCondition = SearchConditionService.GetAddressTransRefTypeCondition((int)RefType.Buyer);
				search.Conditions.Add(searchCondition);
				return addressTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropdownByCondition

		public AccessModeView GetAccessModeByDocumentReturnTable(string documentReturnTableId)
		{
			try
			{
				AccessModeView accessModeView = new AccessModeView();
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				var documentReturnModel = documentReturnTableRepo.GetByIdvw(documentReturnTableId.StringToGuid());
				var documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(documentReturnModel.DocumentStatusGUID.StringToGuid());
				var canCreate = documentStatus.StatusId == ((int)DocumentReturnStatus.Draft).ToString();
				var canDelete = documentStatus.StatusId == ((int)DocumentReturnStatus.Draft).ToString();
				accessModeView.CanCreate = canCreate;
				accessModeView.CanView = true;
				accessModeView.CanDelete = canDelete;
				return accessModeView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public DocumentReturnLineItemView InitialDatatDocumentReturnLine(string id)
		{
			try
			{
				DocumentReturnLineItemView model = new DocumentReturnLineItemView();
				IDocumentReturnLineRepo documentReturnLineRepo = new DocumentReturnLineRepo(db);
				int maxLineNum = documentReturnLineRepo.GetMaxLineNum(id.StringToGuid());
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				var documentReturnTable = documentReturnTableRepo.GetByIdvw(id.StringToGuid());
				if (documentReturnTable.BuyerTableGUID != null)
				{
					IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
					var buyerModel = buyerTableRepo.GetByIdvw(documentReturnTable.BuyerTableGUID.StringToGuid());
					model.ContactPersonName = buyerModel.BuyerName;
					model.BuyerTableGUID = documentReturnTable.BuyerTableGUID;
				}
				model.LineNum = maxLineNum + 1;
				model.DocumentReturnTableGUID = documentReturnTable.DocumentReturnTableGUID;
				model.DocumentReturnTable_DocumentReturnId = SmartAppUtil.GetDropDownLabel(documentReturnTable.DocumentReturnId, documentReturnTable.ContactPersonName);
				model.DocumentReturnTable_ContactTo = documentReturnTable.ContactTo;
				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public DocumentReturnTableItemView GetManageDocumentReturnInitialData(string id, string statusId)
		{
			try
			{
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				var documentStatusModel = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(statusId);
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				var model = documentReturnTableRepo.GetByIdvw(id.StringToGuid());
				model.OriginalDocumentStatusGUID = model.DocumentStatusGUID;
				model.OriginalDocumentStatus = model.DocumentStatus_Values;
				model.DocumentStatusGUID = documentStatusModel.DocumentStatusGUID.ToString();
				model.DocumentStatus_Values = documentStatusModel.Description;
				
				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public bool GetDocumentStatusPostValidation(DocumentReturnTableItemView vmodel)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentReturnTableRepo documentReasonRepo = new DocumentReturnTableRepo(db);
				var model = documentReasonRepo.GetByIdvw(vmodel.DocumentReturnTableGUID.StringToGuid());
				if (model.DocumentStatusGUID != vmodel.OriginalDocumentStatusGUID)
				{
					ex.AddData("ERROR.90012", new string[] { "LABEL.ORIGINAL_STATUS" });
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		public bool GetDocumentStatusCloseValidation(DocumentReturnTableItemView vmodel)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentReturnTableRepo documentReasonRepo = new DocumentReturnTableRepo(db);
				var model = documentReasonRepo.GetByIdvw(vmodel.DocumentReturnTableGUID.StringToGuid());
				if (model.DocumentStatusGUID != vmodel.OriginalDocumentStatusGUID)
				{
					ex.AddData("ERROR.90013", new string[] { "LABEL.ORIGINAL_STATUS" });
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public PrintDocumentReturnLabelLetterView GetPrintDocumentReturnLabelLetter(string documentReturnGUID)
        {
			try
			{
				IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
				DocumentReturnTableItemView documentReturnTableItemView = documentReturnTableRepo.GetByIdvw(documentReturnGUID.StringToGuid());
				PrintDocumentReturnLabelLetterView documentReturnLabelLetterView = new PrintDocumentReturnLabelLetterView
				{
					printDocumentReturnLabelLetterGUID = documentReturnTableItemView.DocumentReturnTableGUID,
					address = documentReturnTableItemView.Address,
					contactPersonName = documentReturnTableItemView.ContactPersonName,
					contactTo = documentReturnTableItemView.ContactTo,
					documentReturnId = documentReturnTableItemView.DocumentReturnId,
					DocumentReturnTableGUID = documentReturnTableItemView.DocumentReturnTableGUID
				};
				return documentReturnLabelLetterView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusDocumentProcessDropdown(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.DocumentReturn);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
