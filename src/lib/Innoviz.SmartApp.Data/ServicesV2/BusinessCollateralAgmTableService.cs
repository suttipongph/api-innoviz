using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IBusinessCollateralAgmTableService
    {

        BusinessCollateralAgmLineItemView GetBusinessCollateralAgmLineById(string id);
        BusinessCollateralAgmLineItemView CreateBusinessCollateralAgmLine(BusinessCollateralAgmLineItemView businessCollateralAgmLineView);
        BusinessCollateralAgmLineItemView UpdateBusinessCollateralAgmLine(BusinessCollateralAgmLineItemView businessCollateralAgmLineView);
        bool DeleteBusinessCollateralAgmLine(string id);
        bool GetVisibleGenerateMenu(string id);
        BusinessCollateralAgmTableItemView GetBusinessCollateralAgmTableById(string id);
        BusinessCollateralAgmTableItemView CreateBusinessCollateralAgmTable(BusinessCollateralAgmTableItemView businessCollateralAgmTableView);
        BusinessCollateralAgmTableItemView UpdateBusinessCollateralAgmTable(BusinessCollateralAgmTableItemView businessCollateralAgmTableView);
        BusinessCollateralAgmTable UpdateBusinessCollateralAgmTable(BusinessCollateralAgmTable businessCollateralAgmTable);

        bool DeleteBusinessCollateralAgmTable(string id);
        bool IsManualByInternalBusinessCollateralAgreement(string companyId);
        bool IsManualByBusinessCollateralAgreement(string companyId);
        BusinessCollateralAgmTableItemView GetBusinessCollateralAgmTableInitialData(int productType);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemBusinessCollateralAgmStatus(SearchParameter search);
        BusinessCollateralAgmLineItemView GetBusinessCollateralAgmLineInitialData(string businessCollateralAgreementTableGUID);
        IEnumerable<SelectItem<CreditAppReqBusinessCollateralItemView>> GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(SearchParameter search);
        AccessModeView GetAccessModeByBusinessCollateralAgmTable(string businessCollateralAgmTableId);
        AccessModeView GetAccessModeByBusinessCollateralAgmTableForBookmarkDocumentTrans(string businessCollateralAgmTableId);
        AccessModeView GetAccessModeBookmarkDocumentTransByBusinessCollateralAgmTable(string businessCollateralAgmTableId);
        string GetStatusIdByBusinessCollateralAgmTable(string businessCollateralAgmId);
        AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refGUID);
        BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialDataByGuarantorAgreement(string refGUID);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(string refGUID);
        ConsortiumTransItemView GetConsortuimTransInitialData(string refGUID);

        JointVentureTransItemView GetJointVentureTransInitialDataByBusinessCollateralAgmTable(string id);
        bool GetGenerateAddendumValidation(string id);
        bool GetGenerateNoticeOfCancellationValidation(string id);
        
        IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTableForBusinessCollateralAGM(string id);
        GenBusCollateralAgmAddendumView GetDatabyCreditAppRequestIDforBusiness(string id);

        GenBusCollateralAgmAddendumResultView CreateGenBusCollateralAgmAddendum(GenBusCollateralAgmAddendumView vwModel);
        List<int> GetVisibleManageMenu(string businessCollateralAgmTableGuid);
        CancelBusinessCollateralAgmResultView CreateCancelBusinessCollateralAgm(CancelBusinessCollateralAgmView vwModel);

        #region Get AccessMode
        AccessModeView GetAccessModeByStatusLessThanSigned(string businessCollateralAgmId);
        #endregion Get AccessMode
        QBusinessCollateralAgreement GetBusinessCollateralAgmBookmarkValues(Guid refGUID);
        BookmarkBusinessCollateralAgmTable GetBookmarkDocumentBusinessCollateralTable(Guid refGUID);
        BookmarkBusinessCollateralAgreementExpenceDetail GetBookmarkDocumentBusinessCollateralTableDetail(Guid refGUID);
        BookmarkCancelBusinessCollateralAgreement GetBookmarkDocumentCancelBusinessCollateralAgreement(Guid refGUID, Guid bookmarkDocumentTransGUID);
        void CreateBusinessCollateralAgmTableCollection(IEnumerable<BusinessCollateralAgmTableItemView> businessCollateralAgmTableItemViews);
        void CreateBusinessCollateralAgmLineCollection(IEnumerable<BusinessCollateralAgmLineItemView> businessCollateralAgmTableItemViews, bool ignoreValidation = false);
        IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search);
        IEnumerable<SelectItem<BusinessCollateralAgmTableItemView>> GetBusinessCollateralAgmTableDropDown(SearchParameter search);
        CopyBusinessCollateralAgmLine GetCopyBusinessCollateralAgmLineById(string id);
        CopyBusinessCollateralAgmLine UpdateCopyBusinessCollateralAgmLine(CopyBusinessCollateralAgmLine vmodel);
        bool ValidateCopyBusinessCollateralAgmLine(string id);
        MemoTransItemView GetMemoTransInitialData(string refGUID);
        GenBusCollateralAgmAddendumView GetGenBusCollateralAgmAddendumById(string id);
        CancelBusinessCollateralAgmView GetGenCancelBusCollateralAgmById(string id);
    }
    public class BusinessCollateralAgmTableService : SmartAppService, IBusinessCollateralAgmTableService
    {
        public BusinessCollateralAgmTableService(SmartAppDbContext context) : base(context) { }
        public BusinessCollateralAgmTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public BusinessCollateralAgmTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public BusinessCollateralAgmTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public BusinessCollateralAgmTableService() : base() { }

        public BusinessCollateralAgmLineItemView GetBusinessCollateralAgmLineById(string id)
        {
            try
            {
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                return businessCollateralAgmLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BusinessCollateralAgmLineItemView CreateBusinessCollateralAgmLine(BusinessCollateralAgmLineItemView businessCollateralAgmLineView)
        {
            try
            {
                BusinessCollateralAgmLine businessCollateralAgmLine = new BusinessCollateralAgmLine();
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                businessCollateralAgmLineView = accessLevelService.AssignOwnerBU(businessCollateralAgmLineView);
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(businessCollateralAgmLineView.BusinessCollateralAgmTableGUID.StringToGuid());
                if(businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation)
                {
                    BusinessCollateralAgmLine lastBusinessCollateralAgmLineByBusinessCollatralAgmTable =  businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(businessCollateralAgmLineView.BusinessCollateralAgmTableGUID.StringToGuid()).OrderByDescending(o => o.LineNum).FirstOrDefault();
                    int maxOrdering = lastBusinessCollateralAgmLineByBusinessCollatralAgmTable != null ? lastBusinessCollateralAgmLineByBusinessCollatralAgmTable.LineNum : 0;
                    businessCollateralAgmLine = businessCollateralAgmLineRepo.GetOriginalBusinessCollateralAgmLine(businessCollateralAgmLineView.OriginalBusinessCollateralAgreementLineGUID.StringToGuid());
                    businessCollateralAgmLine.BusinessCollateralAgmTableGUID = businessCollateralAgmLineView.BusinessCollateralAgmTableGUID.StringToGuid();
                    businessCollateralAgmLine.OriginalBusinessCollateralAgreementTableGUID = businessCollateralAgmLineView.OriginalBusinessCollateralAgreementTableGUID.StringToGuid();
                    businessCollateralAgmLine.OriginalBusinessCollateralAgreementLineGUID = businessCollateralAgmLineView.OriginalBusinessCollateralAgreementLineGUID.StringToGuid();
                    businessCollateralAgmLine.LineNum = maxOrdering + 1;
                    businessCollateralAgmLine = businessCollateralAgmLineRepo.CreateBusinessCollateralAgmLine(businessCollateralAgmLine);
                    base.LogTransactionCreate<BusinessCollateralAgmLine>(businessCollateralAgmLine);
                }
                else
                {
                    businessCollateralAgmLine = businessCollateralAgmLineView.ToBusinessCollateralAgmLine();
                    businessCollateralAgmLine = businessCollateralAgmLineRepo.CreateBusinessCollateralAgmLine(businessCollateralAgmLine);
                    base.LogTransactionCreate<BusinessCollateralAgmLine>(businessCollateralAgmLine);
                }
                
                
                UnitOfWork.Commit();
                return businessCollateralAgmLine.ToBusinessCollateralAgmLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BusinessCollateralAgmLineItemView UpdateBusinessCollateralAgmLine(BusinessCollateralAgmLineItemView businessCollateralAgmLineView)
        {
            try
            {
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                BusinessCollateralAgmLine inputBusinessCollateralAgmLine = businessCollateralAgmLineView.ToBusinessCollateralAgmLine();
                BusinessCollateralAgmLine dbBusinessCollateralAgmLine = businessCollateralAgmLineRepo.Find(inputBusinessCollateralAgmLine.BusinessCollateralAgmLineGUID);
                dbBusinessCollateralAgmLine = businessCollateralAgmLineRepo.UpdateBusinessCollateralAgmLine(dbBusinessCollateralAgmLine, inputBusinessCollateralAgmLine);
                base.LogTransactionUpdate<BusinessCollateralAgmLine>(GetOriginalValues<BusinessCollateralAgmLine>(dbBusinessCollateralAgmLine), dbBusinessCollateralAgmLine);
                UnitOfWork.Commit();
                return dbBusinessCollateralAgmLine.ToBusinessCollateralAgmLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteBusinessCollateralAgmLine(string item)
        {
            try
            {
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                Guid businessCollateralAgmLineGUID = new Guid(item);
                BusinessCollateralAgmLine businessCollateralAgmLine = businessCollateralAgmLineRepo.Find(businessCollateralAgmLineGUID);
                businessCollateralAgmLineRepo.Remove(businessCollateralAgmLine);
                base.LogTransactionDelete<BusinessCollateralAgmLine>(businessCollateralAgmLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BusinessCollateralAgmTableItemView GetBusinessCollateralAgmTableById(string id)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                return businessCollateralAgmTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BusinessCollateralAgmTableItemView CreateBusinessCollateralAgmTable(BusinessCollateralAgmTableItemView businessCollateralAgmTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                businessCollateralAgmTableView = accessLevelService.AssignOwnerBU(businessCollateralAgmTableView);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableView.ToBusinessCollateralAgmTable();

                GenInternalBusinessCollateralAgmNumberSeqCode(businessCollateralAgmTable);
                businessCollateralAgmTable = businessCollateralAgmTableRepo.CreateBusinessCollateralAgmTable(businessCollateralAgmTable);
                base.LogTransactionCreate<BusinessCollateralAgmTable>(businessCollateralAgmTable);

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    GenDataOnCreate(businessCollateralAgmTable);
                    UnitOfWork.Commit(transaction);
                }

                return businessCollateralAgmTable.ToBusinessCollateralAgmTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BusinessCollateralAgmTableItemView UpdateBusinessCollateralAgmTable(BusinessCollateralAgmTableItemView businessCollateralAgmTableView)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableView.ToBusinessCollateralAgmTable();
                businessCollateralAgmTable = UpdateBusinessCollateralAgmTable(businessCollateralAgmTable);
                UnitOfWork.Commit();
                return businessCollateralAgmTable.ToBusinessCollateralAgmTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BusinessCollateralAgmTable UpdateBusinessCollateralAgmTable(BusinessCollateralAgmTable businessCollateralAgmTable)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable dbBusinessCollateralAgmTable = businessCollateralAgmTableRepo.Find(businessCollateralAgmTable.BusinessCollateralAgmTableGUID);
                dbBusinessCollateralAgmTable = businessCollateralAgmTableRepo.UpdateBusinessCollateralAgmTable(dbBusinessCollateralAgmTable, businessCollateralAgmTable);
                base.LogTransactionUpdate<BusinessCollateralAgmTable>(GetOriginalValues<BusinessCollateralAgmTable>(dbBusinessCollateralAgmTable), dbBusinessCollateralAgmTable);
                return dbBusinessCollateralAgmTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteBusinessCollateralAgmTable(string item)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                Guid businessCollateralAgmTableGUID = new Guid(item);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.Find(businessCollateralAgmTableGUID);
                businessCollateralAgmTableRepo.Remove(businessCollateralAgmTable);
                base.LogTransactionDelete<BusinessCollateralAgmTable>(businessCollateralAgmTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region NumberSeq
        public void GenBusinessCollateralAgmNumberSeqCode(BusinessCollateralAgmTable businessCollateralAgmTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByBusinessCollateralAgreement(businessCollateralAgmTable.CompanyGUID.GuidNullToString());
                if (!isManual)
                {
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(businessCollateralAgmTable.CompanyGUID, ReferenceId.BusinessCollateralAgreement);
                    businessCollateralAgmTable.BusinessCollateralAgmId = numberSequenceService.GetNumber(businessCollateralAgmTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void GenInternalBusinessCollateralAgmNumberSeqCode(BusinessCollateralAgmTable businessCollateralAgmTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByInternalBusinessCollateralAgreement(businessCollateralAgmTable.CompanyGUID.GuidNullToString());
                if (!isManual)
                {
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(businessCollateralAgmTable.CompanyGUID, ReferenceId.InternalBusinessCollateralAgreement);
                    businessCollateralAgmTable.InternalBusinessCollateralAgmId = numberSequenceService.GetNumber(businessCollateralAgmTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public string GetInternalBusinessCollateralAgmNumberSeqCode(BusinessCollateralAgmTable businessCollateralAgmTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByInternalBusinessCollateralAgreement(businessCollateralAgmTable.CompanyGUID.GuidNullToString());
                if (!isManual)
                {
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(businessCollateralAgmTable.CompanyGUID, ReferenceId.InternalBusinessCollateralAgreement);
                    return numberSequenceService.GetNumber(businessCollateralAgmTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        #endregion NumberSeq
        public bool IsManualByInternalBusinessCollateralAgreement(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.InternalBusinessCollateralAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByBusinessCollateralAgreement(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.BusinessCollateralAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BusinessCollateralAgmTableItemView GetBusinessCollateralAgmTableInitialData(int productType)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)BusinessCollateralAgreementStatus.Draft);
                return new BusinessCollateralAgmTableItemView
                {
                    BusinessCollateralAgmTableGUID = new Guid().GuidNullToString(),
                    AgreementDocType = (int)AgreementDocType.New,
                    ProductType = productType,
                    AgreementExtension = 0,
                    DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
                    DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                    DocumentStatus_StatusId = documentStatus.StatusId,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BusinessCollateralAgmLineItemView GetBusinessCollateralAgmLineInitialData(string businessCollateralAgreementTableGUID)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(businessCollateralAgreementTableGUID.StringToGuid());
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking((businessCollateralAgmTable.DocumentStatusGUID));
                var model = new BusinessCollateralAgmLineItemView();
                model.BusinessCollateralAgmTableGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID.GuidNullToString();
                model.LineNum = GetMaxLineNum(businessCollateralAgmTable.BusinessCollateralAgmTableGUID);
                model.BusinessCollateralAgmTable_BusinessCollateralAgmId = businessCollateralAgmTable.BusinessCollateralAgmId;
                model.BusinessCollateralAgmTable_InternalBusinessCollateralAgmId = businessCollateralAgmTable.InternalBusinessCollateralAgmId;
                model.businessCollateralAgmTable_CreditAppRequestTableGUID = businessCollateralAgmTable.CreditAppRequestTableGUID.GuidNullToString();
                model.BusinessCollateralAgmTable_AgreementDocType = businessCollateralAgmTable.AgreementDocType;
                model.DocumentStatus_StatusId = documentStatus.StatusId;
                model.BusinessCollateralAgmTable_Values = SmartAppUtil.GetDropDownLabel(businessCollateralAgmTable.InternalBusinessCollateralAgmId, businessCollateralAgmTable.Description);
                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public int GetMaxLineNum(Guid businessCollateralAgreementTableGUID)
        {
            try
            {
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                IEnumerable<BusinessCollateralAgmLine> businessCollateralAgmLines = businessCollateralAgmLineRepo
                        .GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(businessCollateralAgreementTableGUID);
                if (businessCollateralAgmLines.Any())
                {
                    return businessCollateralAgmLines.Max(t => t.LineNum) + 1;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public void GenDataOnCreate(BusinessCollateralAgmTable businessCollateralAgmTable)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);

                AgreementTableInfo agreementTableInfo = businessCollateralAgmTableRepo.GenerateAgreementInfo(businessCollateralAgmTable);
                agreementTableInfoService.CreateAgreementTableInfo(agreementTableInfo);

                if (businessCollateralAgmTable.CustomerTableGUID != null)
                {
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IEnumerable<JointVentureTrans> jointVentureTrans = jointVentureTransService.CopyJointVentureTrans(businessCollateralAgmTable.CustomerTableGUID, businessCollateralAgmTable.BusinessCollateralAgmTableGUID, (int)RefType.Customer, (int)RefType.BusinessCollateralAgreement);
                    base.BulkInsert(jointVentureTrans.ToList(), true);
                }

                if (businessCollateralAgmTable.ConsortiumTableGUID != null)
                {
                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    IEnumerable<ConsortiumTrans> consortiumTables = consortiumTransService.CopyConsortiumTrans(businessCollateralAgmTable.ConsortiumTableGUID.Value, businessCollateralAgmTable.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement);
                    base.BulkInsert(consortiumTables.ToList(), true);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemBusinessCollateralAgmStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.BusinessCollateralAgreement);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<CreditAppReqBusinessCollateralItemView>> GetCreditAppReqBusinessCollateralByBusinessCollateralAgmLineDropDown(SearchParameter search)
        {
            try
            {
                ICreditAppReqBusinessCollateralRepo creditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                search = search.GetParentCondition( CreditAppReqBusinessCollateralCondition.CreditAppRequestTableGUID);
                SearchCondition searchCondition = SearchConditionService.GetCreditAppReqBusinessCollateralIsNewCondition(true);
                search.Conditions.Add(searchCondition);
                return creditAppReqBusinessCollateralRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BusinessCollateralAgmTableItemView>> GetBusinessCollateralAgmTableDropDown(SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                search = search.GetParentCondition(BusinessCollateralAgmTableCondition.BusinessCollateralAgmTableGUID);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)BusinessCollateralAgreementStatus.Signed).ToString());
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(search.Conditions[0].Value.StringToGuid());
                var tmp_RefBusinessCollateralAgmTableGUID = businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID != null ? businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID.GuidNullToString() : null;

                if(tmp_RefBusinessCollateralAgmTableGUID != null)
                {
                    BusinessCollateralAgmTable Condition2 = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking((Guid)businessCollateralAgmTable.RefBusinessCollateralAgmTableGUID);
                    search.Conditions[0].Value = tmp_RefBusinessCollateralAgmTableGUID;
                    search.Conditions[0].Bracket = (int)BracketType.SingleStart;
                    List<SearchCondition> searchCondition = SearchConditionService.GetOriginalBusinessCollateralAgmIDCondition(Condition2.BusinessCollateralAgmTableGUID, documentStatus.DocumentStatusGUID, (int)AgreementDocType.Addendum);
                    search.Conditions.AddRange(searchCondition);
                }
                else
                {
                    search.Conditions.Clear();
                }

                return businessCollateralAgmTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByBusinessCollateralAgmTable(string businessCollateralAgmTableId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(businessCollateralAgmTableId.StringToGuid());

                string _BusinessCollateralAgmTableStatus = GetStatusIdByBusinessCollateralAgmTable(businessCollateralAgmTableId);
                bool isLessThanSigned = (Convert.ToInt32(_BusinessCollateralAgmTableStatus) < (int)BusinessCollateralAgreementStatus.Signed);

                bool newAgreementDocType = businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.New;

                accessModeView.CanCreate = isLessThanSigned && newAgreementDocType;
                accessModeView.CanView = true;
                accessModeView.CanDelete = isLessThanSigned && newAgreementDocType;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AccessModeView GetAccessModeByBusinessCollateralAgmTableForBookmarkDocumentTrans(string businessCollateralAgmTableId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();

                string _BusinessCollateralAgmTableStatus = GetStatusIdByBusinessCollateralAgmTable(businessCollateralAgmTableId);
                bool isLessThanSigned = (Convert.ToInt32(_BusinessCollateralAgmTableStatus) <= (int)BusinessCollateralAgreementStatus.Signed);

                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = true;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public string GetStatusIdByBusinessCollateralAgmTable(string businessCollateralAgmId)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(businessCollateralAgmId.StringToGuid());
                return documentStatusRepo.GetDocumentStatusByIdNoTracking(businessCollateralAgmTable.DocumentStatusGUID).StatusId;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refGUID)
        {
            try
            {
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                string refId = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalBusinessCollateralAgmId;
                return agreementTableInfoService.GetAgreementTableInfoInitialData(refId, refGUID, Models.Enum.RefType.BusinessCollateralAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialDataByGuarantorAgreement(string refGUID)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                BusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                string refId = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalBusinessCollateralAgmId;
                return bookmarkDocumentTransService.GetBookmarkDocumentTransInitialData(refId, refGUID, Models.Enum.RefType.BusinessCollateralAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(string refGUID)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                string refId = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalBusinessCollateralAgmId;
                return serviceFeeTransService.GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(refId, refGUID, Models.Enum.RefType.BusinessCollateralAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ConsortiumTransItemView GetConsortuimTransInitialData(string refGUID)
        {
            try
            {
                IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                BusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var businessAgm = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                string refId = businessAgm.InternalBusinessCollateralAgmId;
                string CreditAppRequestGUID = businessAgm.CreditAppRequestTableGUID.GuidNullToString();
                return consortiumTransService.GetConsortiumTransInitialDataByBusinessCollateralAgmTable(refId, refGUID, CreditAppRequestGUID, Models.Enum.RefType.BusinessCollateralAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JointVentureTransItemView GetJointVentureTransInitialDataByBusinessCollateralAgmTable(string refGUID)
        {
            try
            {
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                string refId = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalBusinessCollateralAgmId;
                return jointVentureTransService.GetJointVentureTransInitialData(refId, refGUID, Models.Enum.RefType.BusinessCollateralAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetGenerateAddendumValidation(string id)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(id.StringToGuid());
                List<BusinessCollateralAgmTable> refBusinessCollateralAgmTables = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByRefBusinessCollateralAgmTableGUID(businessCollateralAgmTable.BusinessCollateralAgmTableGUID);
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Signed).ToString());
                List<DocumentStatus> businessCollatStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.BusinessCollateralAgreement).ToString()).ToList();

                if (businessCollateralAgmTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT" });
                }
                if (businessCollateralAgmTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                bool condition3 = (from refBus in refBusinessCollateralAgmTables
                                  join docStatus in businessCollatStatus
                                  on refBus.DocumentStatusGUID equals docStatus.DocumentStatusGUID
                                  where refBus.AgreementDocType == (int)AgreementDocType.Addendum
                                     && Convert.ToInt32(docStatus.StatusId) < (int)(BusinessCollateralAgreementStatus.Signed)
                                  select refBus).Any();
                if(condition3)
                {
                    ex.AddData("ERROR.90030", new string[] { SmartAppUtil.GetDropDownLabel(businessCollateralAgmTable.InternalBusinessCollateralAgmId, businessCollateralAgmTable.Description) });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetGenerateNoticeOfCancellationValidation(string id)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(id.StringToGuid());
                List<BusinessCollateralAgmTable> refBusinessCollateralAgmTables = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByRefBusinessCollateralAgmTableGUID(businessCollateralAgmTable.BusinessCollateralAgmTableGUID);
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Signed).ToString());
                List<DocumentStatus> businessCollatStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.BusinessCollateralAgreement).ToString()).ToList();
                bool businessCollateralAgmLessthanSignedList = businessCollateralAgmTableRepo.IsBusinessAgmLessthanSignedList(id.StringToGuid() ,(int)AgreementDocType.NoticeOfCancellation, (int)(BusinessCollateralAgreementStatus.Signed));
                bool businessCollateralAgmTypeAddendumLessthanSignedList = businessCollateralAgmTableRepo.IsBusinessAgmLessthanSignedList(id.StringToGuid(), (int)AgreementDocType.Addendum, (int)(BusinessCollateralAgreementStatus.Signed));

                if (businessCollateralAgmTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT" });
                }
                if (businessCollateralAgmTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }


                if (businessCollateralAgmLessthanSignedList)
                {
                    ex.AddData("ERROR.90004", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT",  SmartAppUtil.GetDropDownLabel(businessCollateralAgmTable.InternalBusinessCollateralAgmId, businessCollateralAgmTable.Description) });
                }

                if (businessCollateralAgmTypeAddendumLessthanSignedList)
                {
                    ex.AddData("ERROR.90030", new string[] { SmartAppUtil.GetDropDownLabel(businessCollateralAgmTable.InternalBusinessCollateralAgmId, businessCollateralAgmTable.Description) });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<int> GetVisibleManageMenu(string businessCollateralAgmTableGuid)
        {
            try
            {
                List<int> result = new List<int>();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(businessCollateralAgmTableGuid.StringToGuid());
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(businessCollateralAgmTable.DocumentStatusGUID);
                DocumentStatus completedBookmarkTrans = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.BusinessCollateralAgreement, businessCollateralAgmTableGuid.StringToGuid());
                if ((int)BusinessCollateralAgreementStatus.Draft == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Post);
                }
                if ((int)BusinessCollateralAgreementStatus.Posted == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Send);
                }
                if ((int)BusinessCollateralAgreementStatus.Sent == int.Parse(documentStatus.StatusId) && bookmarkDocumentTrans.All(a => a.DocumentStatusGUID == completedBookmarkTrans.DocumentStatusGUID))
                {
                    result.Add((int)ManageAgreementAction.Sign);
                }
                if ((int)BusinessCollateralAgreementStatus.Signed == int.Parse(documentStatus.StatusId)
                    && (businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.New
                        || businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum))
                {
                    result.Add((int)ManageAgreementAction.Close);
                }
                if (int.Parse(documentStatus.StatusId) < (int)BusinessCollateralAgreementStatus.Signed)
                {
                    result.Add((int)ManageAgreementAction.Cancel);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                string refId = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalBusinessCollateralAgmId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.BusinessCollateralAgreement);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Get AccessMode
        public AccessModeView GetAccessModeByStatusLessThanSigned(string businessCollateralAgmId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(businessCollateralAgmId.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(businessCollateralAgmTable.DocumentStatusGUID);
                bool isLessThanSigned = (Convert.ToInt32(documentStatus.StatusId) < (int)BusinessCollateralAgreementStatus.Signed);
                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = isLessThanSigned;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Get AccessMode
        #region Function
        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTableForBusinessCollateralAGM(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetDropDownItemForAddendumBusiness(id);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenBusCollateralAgmAddendumView GetDatabyCreditAppRequestIDforBusiness(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetDatabyCreditAppRequestIDforBusiness(id);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool checkMenuVelidation(BusinessCollateralAgmTable model)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var documentStatus_BusCollateralTable = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(model.DocumentStatusGUID);
                var Condition3 = (from businessCollateralAgmTable2 in db.Set<BusinessCollateralAgmTable>()
                                  join documentStatus in db.Set<DocumentStatus>()
                                   on businessCollateralAgmTable2.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                                  from documentStatus in ljdocumentStatus.DefaultIfEmpty()
                                  where businessCollateralAgmTable2.AgreementDocType == (int)AgreementDocType.Addendum
                                  && businessCollateralAgmTable2.RefBusinessCollateralAgmTableGUID == model.RefBusinessCollateralAgmTableGUID
                                  && Convert.ToInt32(documentStatus.StatusId) < (int)(BusinessCollateralAgreementStatus.Signed)
                                  select businessCollateralAgmTable2
                 ).FirstOrDefault();

                var Condition4 = (from businessCollateralAgmTable3 in db.Set<BusinessCollateralAgmTable>()
                                  join documentStatus in db.Set<DocumentStatus>()
                                   on businessCollateralAgmTable3.DocumentStatusGUID equals documentStatus.DocumentStatusGUID into ljdocumentStatus
                                  from documentStatus in ljdocumentStatus.DefaultIfEmpty()
                                  where businessCollateralAgmTable3.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation
                                  && businessCollateralAgmTable3.RefBusinessCollateralAgmTableGUID == model.RefBusinessCollateralAgmTableGUID
                                  && Convert.ToInt32(documentStatus.StatusId) < (int)(BusinessCollateralAgreementStatus.Cancelled)
                                  select businessCollateralAgmTable3
                                  ).FirstOrDefault();
                if (int.Parse(documentStatus_BusCollateralTable.StatusId) != (int)BusinessCollateralAgreementStatus.Signed)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT" });
                }
                if (model.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                if (Condition3 != null)
                {
                    ex.AddData("ERROR.90030", new string[] { SmartAppUtil.GetDropDownLabel(model.InternalBusinessCollateralAgmId, model.Description) });
                }
                if (Condition4 != null)
                {
                    ex.AddData("ERROR.90004", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT", SmartAppUtil.GetDropDownLabel(model.InternalBusinessCollateralAgmId, model.Description) });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenBusCollateralAgmAddendumView GetGenBusCollateralAgmAddendumById(string id)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTableItemView businessCollateralAgmTableItemView = businessCollateralAgmTableRepo.GetByIdvw(id.StringToGuid());
                GenBusCollateralAgmAddendumView view = new GenBusCollateralAgmAddendumView
                {
                    InternalBusinessCollateralAgmId = businessCollateralAgmTableItemView.InternalBusinessCollateralAgmId,
                    BusinessCollateralAgmId = businessCollateralAgmTableItemView.BusinessCollateralAgmId,
                    BusinessCollateralAgmDescription = businessCollateralAgmTableItemView.Description,
                    CustomerGUID = businessCollateralAgmTableItemView.CustomerTableGUID,
                    CustomerTable_Values = businessCollateralAgmTableItemView.CustomerTable_Values,
                    CustomerName = businessCollateralAgmTableItemView.CustomerName,
                    AgreementDocType = businessCollateralAgmTableItemView.AgreementDocType,
                    AgreementDate = businessCollateralAgmTableItemView.AgreementDate,
                    CompanyGUID = businessCollateralAgmTableItemView.CompanyGUID,
                };
                return view;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CancelBusinessCollateralAgmView GetGenCancelBusCollateralAgmById(string id)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTableItemView businessCollateralAgmTableItemView = businessCollateralAgmTableRepo.GetByIdvw(id.StringToGuid());
                CancelBusinessCollateralAgmView view = new CancelBusinessCollateralAgmView
                {
                    InternalBusinessCollateralAgmId = businessCollateralAgmTableItemView.InternalBusinessCollateralAgmId,
                    BusinessCollateralAgmId = businessCollateralAgmTableItemView.BusinessCollateralAgmId,
                    BusinessCollateralAgmDescription = businessCollateralAgmTableItemView.Description,
                    AgreementDate = businessCollateralAgmTableItemView.AgreementDate,
                    AgreementDocType = businessCollateralAgmTableItemView.AgreementDocType,
                    CustomerTable_Values = businessCollateralAgmTableItemView.CustomerTable_Values,
                    CustomerName = businessCollateralAgmTableItemView.CustomerName,
                    CompanyGUID = businessCollateralAgmTableItemView.CompanyGUID,
                };
                return view;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenBusCollateralAgmAddendumResultView CreateGenBusCollateralAgmAddendum(GenBusCollateralAgmAddendumView vwModel)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                vwModel = accessLevelService.AssignOwnerBU(vwModel);
                GenBusCollateralAgmAddendumResultView genBusCollateralAgmAddendumView = new GenBusCollateralAgmAddendumResultView();
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable BusCollateralTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(vwModel.GenBusCollateralAgmAddendumGUID.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                List<DocumentStatus> businessCollatStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.BusinessCollateralAgreement).ToString()).ToList();
                bool Check_MenuVelidation = checkMenuVelidation(BusCollateralTable);
                if (Check_MenuVelidation)
                {
                    //Create Business collateral agreement table
                    DocumentStatus draftStatus = businessCollatStatus.Where(w => w.StatusId == ((int)BusinessCollateralAgreementStatus.Draft).ToString()).FirstOrDefault();
                    DocumentStatus signStatus = businessCollatStatus.Where(w => w.StatusId == ((int)BusinessCollateralAgreementStatus.Signed).ToString()).FirstOrDefault();
                    List<BusinessCollateralAgmTable> refBusinessCollateralAgmTables = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByRefBusinessCollateralAgmTableGUID(BusCollateralTable.BusinessCollateralAgmTableGUID);
                    int maxAgreementExtension = refBusinessCollateralAgmTables.Where(w => w.DocumentStatusGUID == signStatus.DocumentStatusGUID
                                                                                           && w.AgreementDocType == (int)AgreementDocType.Addendum)
                                                                       .OrderByDescending(o => o.AgreementExtension).Select(s => s.AgreementExtension).FirstOrDefault();

                    var InternalBus_tmp = GetInternalBusinessCollateralAgmNumberSeqCode(BusCollateralTable) ?? vwModel.AddendumInternalBusineeCollateralId;

                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                    BusinessCollateralAgmTable tmp_CreateBusCollateral = new BusinessCollateralAgmTable()
                    {
                        BusinessCollateralAgmTableGUID = Guid.NewGuid(),
                        BusinessCollateralAgmId = "",
                        SigningDate = null,
                        DocumentStatusGUID = draftStatus.DocumentStatusGUID,
                        AgreementDocType = (int)AgreementDocType.Addendum,
                        Description = vwModel.Description,
                        AgreementDate = vwModel.AddendumDate.StringToDate(),
                        CreditAppRequestTableGUID = vwModel.CreditAppRequestTableGUID.StringToGuid(),
                        CustomerTableGUID = BusCollateralTable.CustomerTableGUID,
                        CustomerName = BusCollateralTable.CustomerName,
                        CustomerAltName = BusCollateralTable.CustomerAltName,
                        ProductType = BusCollateralTable.ProductType,
                        CreditAppTableGUID = BusCollateralTable.CreditAppTableGUID,
                        CreditLimitTypeGUID = BusCollateralTable.CreditLimitTypeGUID,
                        RefBusinessCollateralAgmTableGUID = BusCollateralTable.BusinessCollateralAgmTableGUID,
                        AgreementExtension = maxAgreementExtension + 1,
                        InternalBusinessCollateralAgmId = InternalBus_tmp,
                        CompanyGUID = vwModel.CompanyGUID.StringToGuid(),
                    };

                    //Create Business collateral agreement line
                    List<BusinessCollateralAgmLine> busCollateralLine_CreateList = new List<BusinessCollateralAgmLine>();
                    if (tmp_CreateBusCollateral != null)
                    {
                        ICreditAppReqBusinessCollateralRepo creditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                        List<CreditAppReqBusinessCollateral> creditAppReqBusinessCollaterals = creditAppReqBusinessCollateralRepo.GetCreditAppReqBusinessCollateralByCreditAppReqNoTracking(vwModel.CreditAppRequestTableGUID.StringToGuid());

                        busCollateralLine_CreateList = creditAppReqBusinessCollaterals.Where(w => w.IsNew == true)
                                                        .Select((s, i) => new BusinessCollateralAgmLine()
                                                        {
                                                            BusinessCollateralAgmLineGUID = Guid.NewGuid(),
                                                            BusinessCollateralAgmTableGUID = tmp_CreateBusCollateral.BusinessCollateralAgmTableGUID,
                                                            LineNum = i + 1,
                                                            CreditAppReqBusinessCollateralGUID = s.CreditAppReqBusinessCollateralGUID,
                                                            BusinessCollateralTypeGUID = s.BusinessCollateralTypeGUID,
                                                            BusinessCollateralSubTypeGUID = s.BusinessCollateralSubTypeGUID,
                                                            Description = s.Description,
                                                            BusinessCollateralValue = s.BusinessCollateralValue,
                                                            BuyerTableGUID = s.BuyerTableGUID,
                                                            BuyerName = s.BuyerName,
                                                            BuyerTaxIdentificationId = s.BuyerTaxIdentificationId,
                                                            BankGroupGUID = s.BankGroupGUID,
                                                            BankTypeGUID = s.BankTypeGUID,
                                                            AccountNumber = s.AccountNumber,
                                                            RefAgreementId = s.RefAgreementId,
                                                            RefAgreementDate = s.RefAgreementDate,
                                                            PreferentialCreditorNumber = s.PreferentialCreditorNumber,
                                                            Lessee = s.Lessee,
                                                            Lessor = s.Lessor,
                                                            RegistrationPlateNumber = s.RegistrationPlateNumber,
                                                            MachineNumber = s.MachineNumber,
                                                            MachineRegisteredStatus = s.MachineRegisteredStatus,
                                                            ChassisNumber = s.ChassisNumber,
                                                            RegisteredPlace = s.RegisteredPlace,
                                                            GuaranteeAmount = s.GuaranteeAmount,
                                                            Quantity = s.Quantity,
                                                            Unit = s.Unit,
                                                            ProjectName = s.ProjectName,
                                                            TitleDeedNumber = s.TitleDeedNumber,
                                                            TitleDeedSubDistrict = s.TitleDeedSubDistrict,
                                                            TitleDeedDistrict = s.TitleDeedDistrict,
                                                            TitleDeedProvince = s.TitleDeedProvince,
                                                            CapitalValuation = s.CapitalValuation,
                                                            ValuationCommittee = s.ValuationCommittee,
                                                            DateOfValuation = s.DateOfValuation,
                                                            Ownership = s.Ownership,
                                                            CompanyGUID = vwModel.CompanyGUID.StringToGuid()
                                                        }).ToList();
                    }

                    // SharedMethod30:CopyAgreementInfo
                    IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                    CopyAgreementInfoView copyAgreementInfoView = agreementTableInfoService.CopyAgreementInfo(BusCollateralTable.BusinessCollateralAgmTableGUID, tmp_CreateBusCollateral.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement, (int)RefType.BusinessCollateralAgreement);

                    //Create Agreement joint venture transactions
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IEnumerable<JointVentureTrans> copy_Join = jointVentureTransService.CopyJointVentureTrans(BusCollateralTable.BusinessCollateralAgmTableGUID, tmp_CreateBusCollateral.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement, (int)RefType.BusinessCollateralAgreement);

                    //Create Consortium transactions
                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    IEnumerable<ConsortiumTrans> Copy_Consortium = consortiumTransService.CopyConsortiumTrans(BusCollateralTable.BusinessCollateralAgmTableGUID, tmp_CreateBusCollateral.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement, (int)RefType.BusinessCollateralAgreement);

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(tmp_CreateBusCollateral.FirstToList());
                        if(copyAgreementInfoView != null)
                        {
                            this.BulkInsert(new List<AgreementTableInfo> { copyAgreementInfoView.AgreementTableInfo });
                            if (copyAgreementInfoView.AgreementTableInfoText != null)
                            {
                                this.BulkInsert(new List<AgreementTableInfoText>() { copyAgreementInfoView.AgreementTableInfoText });
                            }
                        }
                        if (Copy_Consortium.Count() > 0)
                        {
                            this.BulkInsert(Copy_Consortium.ToList());
                        }
                        this.BulkInsert(copy_Join.ToList());
                        if (busCollateralLine_CreateList.Count > 0)
                        {
                            this.BulkInsert(busCollateralLine_CreateList);
                        }
                        UnitOfWork.Commit(transaction);
                    }

                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.90019", new string[] { (AgreementDocType.Addendum).GetAttrCode() , "LABEL.BUSINESS_COLLATERAL_AGREEMENT", BusCollateralTable.InternalBusinessCollateralAgmId });
                    genBusCollateralAgmAddendumView.Notification = success;

                    return genBusCollateralAgmAddendumView;
                }
                else
                {
                    return genBusCollateralAgmAddendumView;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CancelBusinessCollateralAgmResultView CreateCancelBusinessCollateralAgm(CancelBusinessCollateralAgmView vwModel)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                vwModel = accessLevelService.AssignOwnerBU(vwModel);
                CancelBusinessCollateralAgmResultView cancelBusinessCollateralAgmResultView = new CancelBusinessCollateralAgmResultView();
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var BusCollateralTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(vwModel.CancelBusinessCollateralAgmGUID.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var documentStatus_BusCollateralTable = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(BusCollateralTable.DocumentStatusGUID);
                bool Check_MenuVelidation = GetGenerateNoticeOfCancellationValidation(vwModel.CancelBusinessCollateralAgmGUID);
                if (Check_MenuVelidation)
                {
                    //Create Business collateral agreement table
                    var documentStatus_tmp = (from documentStatus in db.Set<DocumentStatus>()
                                              where Convert.ToInt32(documentStatus.StatusId) == (int)(BusinessCollateralAgreementStatus.Draft)
                                              select documentStatus
                                         ).FirstOrDefault();
                    var internalBusinessCollateralAGM_number = GetInternalBusinessCollateralAgmNumberSeqCode(BusCollateralTable) ?? vwModel.NoticeOfCancellationInternalBusineeCollateralId;
                    IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                    BusinessCollateralAgmTable tmp_CreateBusCollateral = new BusinessCollateralAgmTable()
                    {
                        BusinessCollateralAgmTableGUID = Guid.NewGuid(),
                        InternalBusinessCollateralAgmId = internalBusinessCollateralAGM_number,
                        BusinessCollateralAgmId = null,
                        SigningDate = null,
                        AgreementExtension = 0,
                        DocumentStatusGUID = documentStatus_tmp.DocumentStatusGUID,
                        AgreementDocType = (int)AgreementDocType.NoticeOfCancellation,
                        Description = vwModel.Description,
                        AgreementDate = vwModel.CancelDate.StringToDate(),
                        DocumentReasonGUID = vwModel.DocumentReasonGUID.StringToGuid(),
                        CustomerTableGUID = BusCollateralTable.CustomerTableGUID,
                        CustomerName = BusCollateralTable.CustomerName,
                        CustomerAltName = BusCollateralTable.CustomerAltName,
                        ProductType = BusCollateralTable.ProductType,
                        CreditAppRequestTableGUID = BusCollateralTable.CreditAppRequestTableGUID,
                        CreditAppTableGUID = BusCollateralTable.CreditAppTableGUID,
                        CreditLimitTypeGUID = BusCollateralTable.CreditLimitTypeGUID,
                        RefBusinessCollateralAgmTableGUID = BusCollateralTable.BusinessCollateralAgmTableGUID,
                        CompanyGUID = BusCollateralTable.CompanyGUID,
                        Remark = vwModel.ReasonRemark
                    };

                    // SharedMethod30:CopyAgreementInfo
                    IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                    CopyAgreementInfoView copyAgreementInfoView = agreementTableInfoService.CopyAgreementInfo(BusCollateralTable.BusinessCollateralAgmTableGUID, tmp_CreateBusCollateral.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement, (int)RefType.BusinessCollateralAgreement);

                    //Create Agreement joint venture transactions
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IEnumerable<JointVentureTrans> copy_Join = jointVentureTransService.CopyJointVentureTrans(BusCollateralTable.BusinessCollateralAgmTableGUID, tmp_CreateBusCollateral.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement, (int)RefType.BusinessCollateralAgreement);

                    //Create Consortium transactions
                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    IEnumerable<ConsortiumTrans> Copy_Consortium = consortiumTransService.CopyConsortiumTrans(BusCollateralTable.BusinessCollateralAgmTableGUID, tmp_CreateBusCollateral.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement, (int)RefType.BusinessCollateralAgreement);


                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(tmp_CreateBusCollateral.FirstToList());
                        if (copyAgreementInfoView != null)
                        {
                            this.BulkInsert(new List<AgreementTableInfo> { copyAgreementInfoView.AgreementTableInfo });
                            if (copyAgreementInfoView.AgreementTableInfoText != null)
                            {
                                this.BulkInsert(new List<AgreementTableInfoText>() { copyAgreementInfoView.AgreementTableInfoText });
                            }
                        }
                        if (Copy_Consortium.Count() > 0)
                        {
                            this.BulkInsert(Copy_Consortium.ToList());
                        }
                        this.BulkInsert(copy_Join.ToList());
                        UnitOfWork.Commit(transaction);
                    }

                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.90019", new string[] { (AgreementDocType.NoticeOfCancellation).GetAttrCode(), "LABEL.BUSINESS_COLLATERAL_AGREEMENT", BusCollateralTable.InternalBusinessCollateralAgmId });
                    cancelBusinessCollateralAgmResultView.Notification = success;

                    return cancelBusinessCollateralAgmResultView;
                }
                else
                {
                    return cancelBusinessCollateralAgmResultView;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public QBusinessCollateralAgreement GetBusinessCollateralAgmBookmarkValues(Guid refGUID)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var businessCollateralAgmBookmarkValue = businessCollateralAgmTableRepo.GetBookmarkDocumentBusinessCollateral(refGUID);
                return businessCollateralAgmBookmarkValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkBusinessCollateralAgmTable GetBookmarkDocumentBusinessCollateralTable(Guid refGUID)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var businessCollateralAgmBookmarkValue = businessCollateralAgmTableRepo.GetBookmarkDocumentBusinessCollateralTable(refGUID);
                return businessCollateralAgmBookmarkValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        public BookmarkBusinessCollateralAgreementExpenceDetail GetBookmarkDocumentBusinessCollateralTableDetail(Guid refGUID)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var businessCollateralAgmBookmarkValue = businessCollateralAgmTableRepo.GetBookmarkDocumentBusinessCollateralAgreementExpenceDetail(refGUID);
                return businessCollateralAgmBookmarkValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkCancelBusinessCollateralAgreement GetBookmarkDocumentCancelBusinessCollateralAgreement(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var businessCollateralAgmBookmarkValue = businessCollateralAgmTableRepo.GetBookmarkDocumentCancelBusinessCollateralAgreement(refGUID, bookmarkDocumentTransGUID);
                return businessCollateralAgmBookmarkValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Migration
        public void CreateBusinessCollateralAgmTableCollection(IEnumerable<BusinessCollateralAgmTableItemView> businessCollateralAgmTableItemViews)
        {
            try
            {
                if (businessCollateralAgmTableItemViews.Any())
                {
                    IBusinessCollateralAgmTableRepo assignmentAgreementSettleRepo = new BusinessCollateralAgmTableRepo(db);
                    List<BusinessCollateralAgmTable> businessCollateralAgmTableItems = businessCollateralAgmTableItemViews.ToBusinessCollateralAgmTable().ToList();
                    assignmentAgreementSettleRepo.ValidateAdd(businessCollateralAgmTableItems);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(businessCollateralAgmTableItems, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateBusinessCollateralAgmLineCollection(IEnumerable<BusinessCollateralAgmLineItemView> businessCollateralAgmTableItemViews, bool ignoreValidation = false)
        {
            try
            {
                if (businessCollateralAgmTableItemViews.Any())
                {
                    IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                    List<BusinessCollateralAgmLine> businessCollateralAgmLineItems = businessCollateralAgmTableItemViews.ToBusinessCollateralAgmLine().ToList();
                    if(!ignoreValidation)
                    {
                        businessCollateralAgmLineRepo.ValidateAdd(businessCollateralAgmLineItems);
                    }
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(businessCollateralAgmLineItems, true, !ignoreValidation);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        #endregion Migration
        #region copy boookmarkducument
        public IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search)
        {

            try
            {
                search.GetParentCondition(BookmarkDocumentCondition.BookmarkDocumentRefType);
                List<SearchCondition> searchCondition = SearchConditionService.GetBookmarkDocumentTemplateCondition(((int)BookmarkDocumentRefType.BusinessCollateralAgreement).ToString());
                search.Conditions.AddRange(searchCondition);
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return sysDropDownService.GetDropDownItemBookmarkDocumentTemplateTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetVisibleGenerateMenu(string id)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);


                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(id.StringToGuid());
                if (businessCollateralAgmTable != null) 
                {
                    IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                    var documentStatusModel = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(businessCollateralAgmTable.DocumentStatusGUID);
                    if (businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.New && documentStatusModel.StatusId == ((int)BusinessCollateralAgreementStatus.Signed).ToString())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
            
        }

        public CopyBusinessCollateralAgmLine GetCopyBusinessCollateralAgmLineById(string id)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var businessCollateralAgmTableModel = businessCollateralAgmTableRepo.GetByIdvw(id.StringToGuid());
                var refBusinessCollateralAgmTableModel = businessCollateralAgmTableRepo.GetByIdvw(businessCollateralAgmTableModel.RefBusinessCollateralAgmTableGUID.StringToGuid());
                CopyBusinessCollateralAgmLine model = new CopyBusinessCollateralAgmLine();
                if (refBusinessCollateralAgmTableModel != null)
                {
                    model.InternalBusineeCollateralId = refBusinessCollateralAgmTableModel.InternalBusinessCollateralAgmId;

                }
                model.CopyBusinessCollateralAgmLineGUID = businessCollateralAgmTableModel.BusinessCollateralAgmTableGUID;
                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CopyBusinessCollateralAgmLine UpdateCopyBusinessCollateralAgmLine(CopyBusinessCollateralAgmLine vmodel)
        {
            try
            {
                CopyBusinessCollateralAgmLine result = new CopyBusinessCollateralAgmLine();
                NotificationResponse success = new NotificationResponse();
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                var businessCollateralAgmTableModel = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(vmodel.CopyBusinessCollateralAgmLineGUID.StringToGuid());
                var businessCollateralAgmLineOldList = businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(vmodel.CopyBusinessCollateralAgmLineGUID.StringToGuid());
                IEnumerable<BusinessCollateralAgmLine> businessCollateralAgmLineNewList = null;
                IEnumerable<BusinessCollateralAgmTableItemViewMap> businessCollateralAgmLineNewListAddendum = null;
                IEnumerable<BusinessCollateralAgmLine> businessCollateralAgmLineByCancelled = null;
                List<BusinessCollateralAgmLine> businessCollateralAgmLineAddendumTocreate = null;

                if (businessCollateralAgmTableModel.RefBusinessCollateralAgmTableGUID != null)
                {
                    businessCollateralAgmLineNewList = businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking((Guid)businessCollateralAgmTableModel.RefBusinessCollateralAgmTableGUID).Where(wh=>wh.Cancelled == false);
                    businessCollateralAgmLineByCancelled = businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByCancelledNoTracking(false);
                    businessCollateralAgmLineNewListAddendum = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableNoTracking().Where(wh => wh.RefBusinessCollateralAgmTableGUID == businessCollateralAgmTableModel.RefBusinessCollateralAgmTableGUID && wh.DocumentStatus_StatusId == ((int)BusinessCollateralAgreementStatus.Signed).ToString()&& wh.AgreementDocType == (int)AgreementDocType.Addendum);
                    businessCollateralAgmLineAddendumTocreate = GetbusinessCollateralAgmLineAddendumTocreate(businessCollateralAgmLineByCancelled, businessCollateralAgmLineNewListAddendum);
                }

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                if (!businessCollateralAgmLineNewList.Any() && !businessCollateralAgmLineAddendumTocreate.Any())
                {
                    ex.AddData("ERROR.90003");
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                if (businessCollateralAgmLineNewList.Count() > 0)
                {
                    List<BusinessCollateralAgmLine> createList = new List<BusinessCollateralAgmLine>();
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        try
                        {
                            int index = 1;
                            BulkDelete(businessCollateralAgmLineOldList.ToList());
                            businessCollateralAgmLineNewList.ToList().ForEach((item )=>
                            {
                                BusinessCollateralAgmLine createItem = new BusinessCollateralAgmLine()
                                {
                                    BusinessCollateralAgmLineGUID = Guid.NewGuid(),
                                    OriginalBusinessCollateralAgreementTableGUID = item.BusinessCollateralAgmTableGUID,
                                    OriginalBusinessCollateralAgreementLineGUID = item.BusinessCollateralAgmLineGUID,
                                    BusinessCollateralAgmTableGUID = businessCollateralAgmTableModel.BusinessCollateralAgmTableGUID,
                                    LineNum = index ,
                                    CreditAppReqBusinessCollateralGUID = item.CreditAppReqBusinessCollateralGUID,
                                    BusinessCollateralTypeGUID = item.BusinessCollateralTypeGUID,
                                    BusinessCollateralSubTypeGUID = item.BusinessCollateralSubTypeGUID,
                                    Product = item.Product,
                                    BusinessCollateralValue = item.BusinessCollateralValue,
                                    Description = item.Description,
                                    Cancelled = item.Cancelled,
                                    BuyerTableGUID = item.BuyerTableGUID,
                                    BuyerName = item.BuyerName,
                                    BuyerTaxIdentificationId = item.BuyerTaxIdentificationId,
                                    BankGroupGUID = item.BankGroupGUID,
                                    BankTypeGUID = item.BankTypeGUID,
                                    AccountNumber = item.AccountNumber,
                                    RefAgreementId = item.RefAgreementId,
                                    RefAgreementDate = item.RefAgreementDate,
                                    PreferentialCreditorNumber = item.PreferentialCreditorNumber,
                                    Lessee = item.Lessee,
                                    Lessor = item.Lessor,
                                    RegistrationPlateNumber = item.RegistrationPlateNumber,
                                    ChassisNumber = item.ChassisNumber,
                                    MachineNumber = item.MachineNumber,
                                    MachineRegisteredStatus = item.MachineRegisteredStatus,
                                    RegisteredPlace = item.RegisteredPlace,
                                    Quantity = item.Quantity,
                                    Unit = item.Unit,
                                    GuaranteeAmount = item.GuaranteeAmount,
                                    ProjectName = item.ProjectName,
                                    TitleDeedNumber = item.TitleDeedNumber,
                                    TitleDeedSubDistrict = item.TitleDeedSubDistrict,
                                    TitleDeedDistrict = item.TitleDeedDistrict,
                                    TitleDeedProvince = item.TitleDeedProvince,
                                    DateOfValuation = item.DateOfValuation,
                                    CapitalValuation = item.CapitalValuation,
                                    ValuationCommittee = item.ValuationCommittee,
                                    Ownership = item.Ownership,
                                    CompanyGUID = item.CompanyGUID,                                    
                                };
                                createList.Add(createItem);
                                index++;
                            });
                            if(businessCollateralAgmLineAddendumTocreate.Count() > 0)
                            {
                                businessCollateralAgmLineAddendumTocreate.ToList().ForEach((item) =>
                                {
                                    BusinessCollateralAgmLine createItem = new BusinessCollateralAgmLine()
                                    {
                                        BusinessCollateralAgmLineGUID = Guid.NewGuid(),
                                        OriginalBusinessCollateralAgreementTableGUID = item.BusinessCollateralAgmTableGUID,
                                        OriginalBusinessCollateralAgreementLineGUID = item.BusinessCollateralAgmLineGUID,
                                        BusinessCollateralAgmTableGUID = businessCollateralAgmTableModel.BusinessCollateralAgmTableGUID,
                                        LineNum = index,
                                        CreditAppReqBusinessCollateralGUID = item.CreditAppReqBusinessCollateralGUID,
                                        BusinessCollateralTypeGUID = item.BusinessCollateralTypeGUID,
                                        BusinessCollateralSubTypeGUID = item.BusinessCollateralSubTypeGUID,
                                        Product = item.Product,
                                        BusinessCollateralValue = item.BusinessCollateralValue,
                                        Description = item.Description,
                                        Cancelled = item.Cancelled,
                                        BuyerTableGUID = item.BuyerTableGUID,
                                        BuyerName = item.BuyerName,
                                        BuyerTaxIdentificationId = item.BuyerTaxIdentificationId,
                                        BankGroupGUID = item.BankGroupGUID,
                                        BankTypeGUID = item.BankTypeGUID,
                                        AccountNumber = item.AccountNumber,
                                        RefAgreementId = item.RefAgreementId,
                                        RefAgreementDate = item.RefAgreementDate,
                                        PreferentialCreditorNumber = item.PreferentialCreditorNumber,
                                        Lessee = item.Lessee,
                                        Lessor = item.Lessor,
                                        RegistrationPlateNumber = item.RegistrationPlateNumber,
                                        ChassisNumber = item.ChassisNumber,
                                        MachineNumber = item.MachineNumber,
                                        MachineRegisteredStatus = item.MachineRegisteredStatus,
                                        RegisteredPlace = item.RegisteredPlace,
                                        Quantity = item.Quantity,
                                        Unit = item.Unit,
                                        GuaranteeAmount = item.GuaranteeAmount,
                                        ProjectName = item.ProjectName,
                                        TitleDeedNumber = item.TitleDeedNumber,
                                        TitleDeedSubDistrict = item.TitleDeedSubDistrict,
                                        TitleDeedDistrict = item.TitleDeedDistrict,
                                        TitleDeedProvince = item.TitleDeedProvince,
                                        DateOfValuation = item.DateOfValuation,
                                        CapitalValuation = item.CapitalValuation,
                                        ValuationCommittee = item.ValuationCommittee,
                                        Ownership = item.Ownership,
                                        CompanyGUID = item.CompanyGUID,
                                    };
                                    createList.Add(createItem);
                                    index++;
                                });
                            }
                            BulkInsert(createList);
                            UnitOfWork.Commit(transaction);
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            throw SmartAppUtil.AddStackTrace(e);
                        }
                    }
                }
                success.AddData("SUCCESS.90001", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT_LINE" });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private List<BusinessCollateralAgmLine> GetbusinessCollateralAgmLineAddendumTocreate(IEnumerable<BusinessCollateralAgmLine> businessCollateralAgmLineByCancelled, IEnumerable<BusinessCollateralAgmTableItemViewMap> businessCollateralAgmLineNewListAddendum)
        {
            try
            {
                List<BusinessCollateralAgmLine> modelList = new List<BusinessCollateralAgmLine>();
                foreach (var adendum in businessCollateralAgmLineNewListAddendum.ToList())
                {
                    foreach(var agmLine in businessCollateralAgmLineByCancelled.ToList())
                    {
                        if(adendum.BusinessCollateralAgmTableGUID == agmLine.BusinessCollateralAgmTableGUID)
                        {
                            modelList.Add(agmLine);
                        }
                    }
                }
                return modelList;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool ValidateCopyBusinessCollateralAgmLine(string id)
        {
            try
            {
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                var businessCollateralAgmLineList = businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(id.StringToGuid());
                if (businessCollateralAgmLineList.Count() != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AccessModeView GetAccessModeBookmarkDocumentTransByBusinessCollateralAgmTable(string businessCollateralAgmTableId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(businessCollateralAgmTableId.StringToGuid());

                string _BusinessCollateralAgmTableStatus = GetStatusIdByBusinessCollateralAgmTable(businessCollateralAgmTableId);
                bool isLessThanSigned = (Convert.ToInt32(_BusinessCollateralAgmTableStatus) < (int)BusinessCollateralAgreementStatus.Signed);

                bool newAgreementDocType = businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.New;

                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = isLessThanSigned;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}