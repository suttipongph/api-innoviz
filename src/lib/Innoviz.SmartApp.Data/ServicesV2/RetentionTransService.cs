using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IRetentionTransService
	{

		RetentionTransItemView GetRetentionTransById(string id);
		RetentionTransItemView CreateRetentionTrans(RetentionTransItemView retentionTransView);
		RetentionTrans CreateRetentionTrans(RetentionTrans retentionTrans);
		RetentionTransItemView UpdateRetentionTrans(RetentionTransItemView retentionTransView);
		bool DeleteRetentionTrans(string id);

		#region Shared method
		decimal GetRetentionAccumByCreditAppTableGUID(Guid creditAppTableGUID);
		SearchResult<RetentionOutstandingView> GetRetentionOutstandingByCustomer(SearchParameter search);
		#endregion
	}
    public class RetentionTransService : SmartAppService, IRetentionTransService
	{
		public RetentionTransService(SmartAppDbContext context) : base(context) { }
		public RetentionTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public RetentionTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RetentionTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public RetentionTransService() : base() { }

		public RetentionTransItemView GetRetentionTransById(string id)
		{
			try
			{
				IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
				return retentionTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionTransItemView CreateRetentionTrans(RetentionTransItemView retentionTransView)
		{
			try
			{
				RetentionTrans retentionTrans = retentionTransView.ToRetentionTrans();
				retentionTrans = CreateRetentionTrans(retentionTrans);
				UnitOfWork.Commit();
				return retentionTrans.ToRetentionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionTrans CreateRetentionTrans(RetentionTrans retentionTrans)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				retentionTrans = accessLevelService.AssignOwnerBU(retentionTrans);
				IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
				retentionTrans = retentionTransRepo.CreateRetentionTrans(retentionTrans);
				base.LogTransactionCreate<RetentionTrans>(retentionTrans);
				return retentionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionTransItemView UpdateRetentionTrans(RetentionTransItemView retentionTransView)
		{
			try
			{
				IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
				RetentionTrans inputRetentionTrans = retentionTransView.ToRetentionTrans();
				RetentionTrans dbRetentionTrans = retentionTransRepo.Find(inputRetentionTrans.RetentionTransGUID);
				dbRetentionTrans = retentionTransRepo.UpdateRetentionTrans(dbRetentionTrans, inputRetentionTrans);
				base.LogTransactionUpdate<RetentionTrans>(GetOriginalValues<RetentionTrans>(dbRetentionTrans), dbRetentionTrans);
				UnitOfWork.Commit();
				return dbRetentionTrans.ToRetentionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteRetentionTrans(string item)
		{
			try
			{
				IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
				Guid retentionTransGUID = new Guid(item);
				RetentionTrans retentionTrans = retentionTransRepo.Find(retentionTransGUID);
				retentionTransRepo.Remove(retentionTrans);
				base.LogTransactionDelete<RetentionTrans>(retentionTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Shared method
		public decimal GetRetentionAccumByCreditAppTableGUID(Guid creditAppTableGUID)
        {
            try
            {
				IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
				List<RetentionTrans> retentionTrans = retentionTransRepo.GetRetentionTransByCreditAppTableGUIDNoTracking(creditAppTableGUID);
				decimal result = retentionTrans.Sum(s => s.Amount);
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public SearchResult<RetentionOutstandingView> GetRetentionOutstandingByCustomer(SearchParameter search)
        {
			try
			{
				IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
				SearchResult<RetentionOutstandingView> retentionOutstanding = retentionTransRepo.GetRetentionOutstandingByCustomer(search);

				return retentionOutstanding;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
			
        }
        #endregion
    }
}
