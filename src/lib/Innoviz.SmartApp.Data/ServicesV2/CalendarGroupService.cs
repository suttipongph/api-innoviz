using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICalendarGroupService
	{

		CalendarGroupItemView GetCalendarGroupById(string id);
		CalendarGroupItemView CreateCalendarGroup(CalendarGroupItemView calendarGroupView);
		CalendarGroupItemView UpdateCalendarGroup(CalendarGroupItemView calendarGroupView);
		bool DeleteCalendarGroup(string id);
	}
	public class CalendarGroupService : SmartAppService, ICalendarGroupService
	{
		public CalendarGroupService(SmartAppDbContext context) : base(context) { }
		public CalendarGroupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CalendarGroupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CalendarGroupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CalendarGroupService() : base() { }

		public CalendarGroupItemView GetCalendarGroupById(string id)
		{
			try
			{
				ICalendarGroupRepo calendarGroupRepo = new CalendarGroupRepo(db);
				return calendarGroupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CalendarGroupItemView CreateCalendarGroup(CalendarGroupItemView calendarGroupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				calendarGroupView = accessLevelService.AssignOwnerBU(calendarGroupView);
				ICalendarGroupRepo calendarGroupRepo = new CalendarGroupRepo(db);
				CalendarGroup calendarGroup = calendarGroupView.ToCalendarGroup();
				calendarGroup = calendarGroupRepo.CreateCalendarGroup(calendarGroup);
				base.LogTransactionCreate<CalendarGroup>(calendarGroup);
				UnitOfWork.Commit();
				return calendarGroup.ToCalendarGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CalendarGroupItemView UpdateCalendarGroup(CalendarGroupItemView calendarGroupView)
		{
			try
			{
				ICalendarGroupRepo calendarGroupRepo = new CalendarGroupRepo(db);
				CalendarGroup inputCalendarGroup = calendarGroupView.ToCalendarGroup();
				CalendarGroup dbCalendarGroup = calendarGroupRepo.Find(inputCalendarGroup.CalendarGroupGUID);
				dbCalendarGroup = calendarGroupRepo.UpdateCalendarGroup(dbCalendarGroup, inputCalendarGroup);
				base.LogTransactionUpdate<CalendarGroup>(GetOriginalValues<CalendarGroup>(dbCalendarGroup), dbCalendarGroup);
				UnitOfWork.Commit();
				return dbCalendarGroup.ToCalendarGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCalendarGroup(string item)
		{
			try
			{
				ICalendarGroupRepo calendarGroupRepo = new CalendarGroupRepo(db);
				Guid calendarGroupGUID = new Guid(item);
				CalendarGroup calendarGroup = calendarGroupRepo.Find(calendarGroupGUID);
				calendarGroupRepo.Remove(calendarGroup);
				base.LogTransactionDelete<CalendarGroup>(calendarGroup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
