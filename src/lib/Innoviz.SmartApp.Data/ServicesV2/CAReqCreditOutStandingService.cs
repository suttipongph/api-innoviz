using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICAReqCreditOutStandingService
	{

		CAReqCreditOutStandingItemView GetCAReqCreditOutStandingById(string id);
		CAReqCreditOutStandingItemView CreateCAReqCreditOutStanding(CAReqCreditOutStandingItemView caReqCreditOutStandingView);
		CAReqCreditOutStandingItemView UpdateCAReqCreditOutStanding(CAReqCreditOutStandingItemView caReqCreditOutStandingView);
		bool DeleteCAReqCreditOutStanding(string id);
	}
	public class CAReqCreditOutStandingService : SmartAppService, ICAReqCreditOutStandingService
	{
		public CAReqCreditOutStandingService(SmartAppDbContext context) : base(context) { }
		public CAReqCreditOutStandingService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CAReqCreditOutStandingService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqCreditOutStandingService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CAReqCreditOutStandingService() : base() { }

		public CAReqCreditOutStandingItemView GetCAReqCreditOutStandingById(string id)
		{
			try
			{
				ICAReqCreditOutStandingRepo caReqCreditOutStandingRepo = new CAReqCreditOutStandingRepo(db);
				return caReqCreditOutStandingRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqCreditOutStandingItemView CreateCAReqCreditOutStanding(CAReqCreditOutStandingItemView caReqCreditOutStandingView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				caReqCreditOutStandingView = accessLevelService.AssignOwnerBU(caReqCreditOutStandingView);
				ICAReqCreditOutStandingRepo caReqCreditOutStandingRepo = new CAReqCreditOutStandingRepo(db);
				CAReqCreditOutStanding caReqCreditOutStanding = caReqCreditOutStandingView.ToCAReqCreditOutStanding();
				caReqCreditOutStanding = caReqCreditOutStandingRepo.CreateCAReqCreditOutStanding(caReqCreditOutStanding);
				base.LogTransactionCreate<CAReqCreditOutStanding>(caReqCreditOutStanding);
				UnitOfWork.Commit();
				return caReqCreditOutStanding.ToCAReqCreditOutStandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqCreditOutStandingItemView UpdateCAReqCreditOutStanding(CAReqCreditOutStandingItemView caReqCreditOutStandingView)
		{
			try
			{
				ICAReqCreditOutStandingRepo caReqCreditOutStandingRepo = new CAReqCreditOutStandingRepo(db);
				CAReqCreditOutStanding inputCAReqCreditOutStanding = caReqCreditOutStandingView.ToCAReqCreditOutStanding();
				CAReqCreditOutStanding dbCAReqCreditOutStanding = caReqCreditOutStandingRepo.Find(inputCAReqCreditOutStanding.CAReqCreditOutStandingGUID);
				dbCAReqCreditOutStanding = caReqCreditOutStandingRepo.UpdateCAReqCreditOutStanding(dbCAReqCreditOutStanding, inputCAReqCreditOutStanding);
				base.LogTransactionUpdate<CAReqCreditOutStanding>(GetOriginalValues<CAReqCreditOutStanding>(dbCAReqCreditOutStanding), dbCAReqCreditOutStanding);
				UnitOfWork.Commit();
				return dbCAReqCreditOutStanding.ToCAReqCreditOutStandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCAReqCreditOutStanding(string item)
		{
			try
			{
				ICAReqCreditOutStandingRepo caReqCreditOutStandingRepo = new CAReqCreditOutStandingRepo(db);
				Guid caReqCreditOutStandingGUID = new Guid(item);
				CAReqCreditOutStanding caReqCreditOutStanding = caReqCreditOutStandingRepo.Find(caReqCreditOutStandingGUID);
				caReqCreditOutStandingRepo.Remove(caReqCreditOutStanding);
				base.LogTransactionDelete<CAReqCreditOutStanding>(caReqCreditOutStanding);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
