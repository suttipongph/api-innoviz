using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IProdUnitService
	{

		ProdUnitItemView GetProdUnitById(string id);
		ProdUnitItemView CreateProdUnit(ProdUnitItemView prodUnitView);
		ProdUnitItemView UpdateProdUnit(ProdUnitItemView prodUnitView);
		bool DeleteProdUnit(string id);
	}
	public class ProdUnitService : SmartAppService, IProdUnitService
	{
		public ProdUnitService(SmartAppDbContext context) : base(context) { }
		public ProdUnitService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ProdUnitService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProdUnitService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ProdUnitService() : base() { }

		public ProdUnitItemView GetProdUnitById(string id)
		{
			try
			{
				IProdUnitRepo prodUnitRepo = new ProdUnitRepo(db);
				return prodUnitRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProdUnitItemView CreateProdUnit(ProdUnitItemView prodUnitView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				prodUnitView = accessLevelService.AssignOwnerBU(prodUnitView);
				IProdUnitRepo prodUnitRepo = new ProdUnitRepo(db);
				ProdUnit prodUnit = prodUnitView.ToProdUnit();
				prodUnit = prodUnitRepo.CreateProdUnit(prodUnit);
				base.LogTransactionCreate<ProdUnit>(prodUnit);
				UnitOfWork.Commit();
				return prodUnit.ToProdUnitItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProdUnitItemView UpdateProdUnit(ProdUnitItemView prodUnitView)
		{
			try
			{
				IProdUnitRepo prodUnitRepo = new ProdUnitRepo(db);
				ProdUnit inputProdUnit = prodUnitView.ToProdUnit();
				ProdUnit dbProdUnit = prodUnitRepo.Find(inputProdUnit.ProdUnitGUID);
				dbProdUnit = prodUnitRepo.UpdateProdUnit(dbProdUnit, inputProdUnit);
				base.LogTransactionUpdate<ProdUnit>(GetOriginalValues<ProdUnit>(dbProdUnit), dbProdUnit);
				UnitOfWork.Commit();
				return dbProdUnit.ToProdUnitItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteProdUnit(string item)
		{
			try
			{
				IProdUnitRepo prodUnitRepo = new ProdUnitRepo(db);
				Guid prodUnitGUID = new Guid(item);
				ProdUnit prodUnit = prodUnitRepo.Find(prodUnitGUID);
				prodUnitRepo.Remove(prodUnit);
				base.LogTransactionDelete<ProdUnit>(prodUnit);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
