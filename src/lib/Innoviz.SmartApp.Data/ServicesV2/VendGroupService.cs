using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IVendGroupService
	{

		VendGroupItemView GetVendGroupById(string id);
		VendGroupItemView CreateVendGroup(VendGroupItemView vendGroupView);
		VendGroupItemView UpdateVendGroup(VendGroupItemView vendGroupView);
		bool DeleteVendGroup(string id);
	}
	public class VendGroupService : SmartAppService, IVendGroupService
	{
		public VendGroupService(SmartAppDbContext context) : base(context) { }
		public VendGroupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public VendGroupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VendGroupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public VendGroupService() : base() { }

		public VendGroupItemView GetVendGroupById(string id)
		{
			try
			{
				IVendGroupRepo vendGroupRepo = new VendGroupRepo(db);
				return vendGroupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendGroupItemView CreateVendGroup(VendGroupItemView vendGroupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				vendGroupView = accessLevelService.AssignOwnerBU(vendGroupView);
				IVendGroupRepo vendGroupRepo = new VendGroupRepo(db);
				VendGroup vendGroup = vendGroupView.ToVendGroup();
				vendGroup = vendGroupRepo.CreateVendGroup(vendGroup);
				base.LogTransactionCreate<VendGroup>(vendGroup);
				UnitOfWork.Commit();
				return vendGroup.ToVendGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendGroupItemView UpdateVendGroup(VendGroupItemView vendGroupView)
		{
			try
			{
				IVendGroupRepo vendGroupRepo = new VendGroupRepo(db);
				VendGroup inputVendGroup = vendGroupView.ToVendGroup();
				VendGroup dbVendGroup = vendGroupRepo.Find(inputVendGroup.VendGroupGUID);
				dbVendGroup = vendGroupRepo.UpdateVendGroup(dbVendGroup, inputVendGroup);
				base.LogTransactionUpdate<VendGroup>(GetOriginalValues<VendGroup>(dbVendGroup), dbVendGroup);
				UnitOfWork.Commit();
				return dbVendGroup.ToVendGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteVendGroup(string item)
		{
			try
			{
				IVendGroupRepo vendGroupRepo = new VendGroupRepo(db);
				Guid vendGroupGUID = new Guid(item);
				VendGroup vendGroup = vendGroupRepo.Find(vendGroupGUID);
				vendGroupRepo.Remove(vendGroup);
				base.LogTransactionDelete<VendGroup>(vendGroup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
