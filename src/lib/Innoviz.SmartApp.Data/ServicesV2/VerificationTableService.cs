using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IVerificationTableService
    {
        VerificationTableItemView CopyVerificationLine(VerificationTableItemView verificationTable);
        VerificationLineItemView GetVerificationLineById(string id);
        VerificationLineItemView CreateVerificationLine(VerificationLineItemView verificationLineView);
        VerificationLineItemView UpdateVerificationLine(VerificationLineItemView verificationLineView);
        bool DeleteVerificationLine(string id);
        void CreateVerificationLineCollection(List<VerificationLineItemView> verificationLineViews);
        VerificationTableItemView GetVerificationTableById(string id);
        VerificationTableItemView GetCopyVerificationTableById(string id);
        VerificationTableItemView CreateVerificationTable(VerificationTableItemView verificationTableView);
        VerificationTableItemView UpdateVerificationTable(VerificationTableItemView verificationTableView);
        bool DeleteVerificationTable(string id);
        void CreateVerificationTableCollection(List<VerificationTableItemView> verificationTableItemViews);
        bool IsManualByVerificationId(string companyId);
        bool CheckVerificationHesLineById(string id);
        bool CheckIsHassLineById(string id);
        VerificationTableItemView GetVerificationInitialData(string refGUID);
        VerificationLineItemView GetVerificationLineInitialData(string id);
        AccessModeView GetAccessModeByParentVerificationLine(string creditAppLineId);
        IEnumerable<SelectItem<VerificationTableItemView>> GetVerificationTableByVerificationTrans(SearchParameter search);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemVerificationTableStatus(SearchParameter search);
        #region Function
        public VerificationTableItemView GetUpdateVerificationStatusById(string refGUID);
        public UpdateVerificationStatusView UpdateUpdateVerificationStatus(UpdateVerificationStatusParamView functionView);
        VerificationTableItemView CreateCopyVerificationTable(VerificationTableItemView verificationTableView);
        #endregion Function
        IEnumerable<SelectItem<VerificationTableItemView>> GetVerificationTableDropDown(SearchParameter search);
    }
    public class VerificationTableService : SmartAppService, IVerificationTableService
    {
        public VerificationTableService(SmartAppDbContext context) : base(context) { }
        public VerificationTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public VerificationTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public VerificationTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public VerificationTableService() : base() { }

        public VerificationTableItemView CopyVerificationLine(VerificationTableItemView verificationTable)
        {
            int i = 0;
            IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
            IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
            VerificationTableItemView verificationTableItemView = new VerificationTableItemView();
            var verificationModel = verificationTableRepo.GetVerificationTableByIdNoTrackingByAccessLevel(verificationTable.VerificationTableGUID.StringToGuid());
            List<VerificationLine> lineListold = verificationLineRepo.GetVerificationLineByVerificationTable(verificationTable.VerificationTableGUID.StringToGuid());
            List<VerificationLine> lineListNew = verificationLineRepo.GetVerificationLineByVerificationTable(verificationTable.CopyVerificationGUID.StringToGuid());
            List<VerificationLine> lineList = new List<VerificationLine>(new VerificationLine[lineListNew.Count]);
            using (var transaction = UnitOfWork.ContextTransaction())
            {
                if(lineListold.Count > 0)
                {
                    BulkDelete(lineListold);
                }
                try
                {
                    foreach (VerificationLine item in lineListNew)
                    {
                        lineList[i] = new VerificationLine()
                        {
                            Pass = item.Pass,
                            Remark = item.Remark,
                            VerificationTableGUID = verificationTable.VerificationTableGUID.StringToGuid(),
                            VerificationTypeGUID = item.VerificationTypeGUID,
                            CompanyGUID = item.CompanyGUID,
                        };
                        i++;
                    }
                    //BulkInsert(lineListNew,true);
                    //BulkDelete(lineListOld);
                    BulkInsert(lineList, true);
                    UnitOfWork.Commit(transaction);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw SmartAppUtil.AddStackTrace(e);
                }
            }
            NotificationResponse success = new NotificationResponse();
            success.AddData("SUCCESS.90001", new string[] { "LABEL.VERIFICATION_LINE" });
            verificationTableItemView.Notification = success;
            return verificationTableItemView;
        }
        public VerificationLineItemView GetVerificationLineById(string id)
        {
            try
            {
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                return verificationLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByVerificationId(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.Verification);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool CheckVerificationHesLineById(string id)
        {
            try
            {
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                VerificationTableItemView verificationTableItemView = new VerificationTableItemView();

                List<VerificationLine> verificationLine = verificationLineRepo.GetVerificationLineByVerificationTableNoTracking(id.StringToGuid());
                if (verificationLine.Count > 0)
                {
                    return true;
                }
                else
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.90003");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool CheckIsHassLineById(string id)
        {
            try
            {
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                VerificationTableItemView verificationTableItemView = new VerificationTableItemView();

                List<VerificationLine> verificationLine = verificationLineRepo.GetVerificationLineByVerificationTableNoTracking(id.StringToGuid());
                if (verificationLine.Count > 0)
                {
                    return true;
                }
                else
                {
                    //SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    //ex.AddData("ERROR.90003");
                    //throw SmartAppUtil.AddStackTrace(ex);
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByParentVerificationLine(string creditAppLineId)
        {
            AccessModeView accessModeView = new AccessModeView();
            IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
            VerificationTableItemView verification = verificationTableRepo.GetByIdvw(creditAppLineId.StringToGuid());
            bool creditAppLineExpireAndInactive = verification.DocumentStatus_Values == EnumsDocumentProcessStatus.VerificationDocumentStatus.Draft.ToString();
            //bool getCanDelete = GetCanDeleteByCreditAppLine(creditAppLineId);
            accessModeView.CanCreate = creditAppLineExpireAndInactive;
            accessModeView.CanView = true;
            accessModeView.CanDelete = true;
            return accessModeView;
        }
        public VerificationTableItemView GetVerificationInitialData(string refGUID)
        {
            try
            {

                IDocumentService documentService = new DocumentService(db);
                DocumentStatus draft = documentService.GetDocumentStatusByStatusId((int)VerificationDocumentStatus.Draft);

                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(creditAppTable.CustomerTableGUID);

                return new VerificationTableItemView
                {
                    VerificationTableGUID = new Guid().GuidNullOrEmptyToString(),
                    VerificationDate = DateTime.Now.DateToString(),
                    CreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
                    CreditAppTable_Values = creditAppTable.CreditAppId,
                    CustomerTableGUID = customerTable.CustomerTableGUID.GuidNullToString(),
                    CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                    DocumentStatusGUID = draft.DocumentStatusGUID.GuidNullToString(),
                    DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(draft.Description)

                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationLineItemView GetVerificationLineInitialData(string id)
        {
            try
            {
                IVerificationTableRepo verificationTransRepo = new VerificationTableRepo(db);
                VerificationTableItemView verificationTableItemView = verificationTransRepo.GetByIdvw(id.StringToGuid());
                return new VerificationLineItemView
                {
                    VerificationTable_Values = SmartAppUtil.GetDropDownLabel(verificationTableItemView.VerificationId,verificationTableItemView.Description),
                    VerificationTableGUID = id,
                    Pass = true,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationLineItemView CreateVerificationLine(VerificationLineItemView verificationLineView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                verificationLineView = accessLevelService.AssignOwnerBU(verificationLineView);
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                VerificationLine verificationLine = verificationLineView.ToVerificationLine();
                verificationLine = verificationLineRepo.CreateVerificationLine(verificationLine);
                base.LogTransactionCreate<VerificationLine>(verificationLine);
                UnitOfWork.Commit();
                return verificationLine.ToVerificationLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationLineItemView UpdateVerificationLine(VerificationLineItemView verificationLineView)
        {
            try
            {
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                VerificationLine inputVerificationLine = verificationLineView.ToVerificationLine();
                VerificationLine dbVerificationLine = verificationLineRepo.Find(inputVerificationLine.VerificationLineGUID);
                dbVerificationLine = verificationLineRepo.UpdateVerificationLine(dbVerificationLine, inputVerificationLine);
                base.LogTransactionUpdate<VerificationLine>(GetOriginalValues<VerificationLine>(dbVerificationLine), dbVerificationLine);
                UnitOfWork.Commit();
                return dbVerificationLine.ToVerificationLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteVerificationLine(string item)
        {
            try
            {
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                Guid verificationLineGUID = new Guid(item);
                VerificationLine verificationLine = verificationLineRepo.Find(verificationLineGUID);
                verificationLineRepo.Remove(verificationLine);
                base.LogTransactionDelete<VerificationLine>(verificationLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationTableItemView GetVerificationTableById(string id)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                return verificationTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationTableItemView CreateVerificationTable(VerificationTableItemView verificationTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                verificationTableView = accessLevelService.AssignOwnerBU(verificationTableView);
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                VerificationTable verificationTable = verificationTableView.ToVerificationTable();
                GenVerificationNumberSeqCode(verificationTable);
                verificationTable = verificationTableRepo.CreateVerificationTable(verificationTable);
                base.LogTransactionCreate<VerificationTable>(verificationTable);
                UnitOfWork.Commit();
                return verificationTable.ToVerificationTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationTableItemView UpdateVerificationTable(VerificationTableItemView verificationTableView)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                VerificationTable inputVerificationTable = verificationTableView.ToVerificationTable();
                VerificationTable dbVerificationTable = verificationTableRepo.Find(inputVerificationTable.VerificationTableGUID);
                dbVerificationTable = verificationTableRepo.UpdateVerificationTable(dbVerificationTable, inputVerificationTable);
                base.LogTransactionUpdate<VerificationTable>(GetOriginalValues<VerificationTable>(dbVerificationTable), dbVerificationTable);
                UnitOfWork.Commit();
                return dbVerificationTable.ToVerificationTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteVerificationTable(string item)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                Guid verificationTableGUID = new Guid(item);
                VerificationTable verificationTable = verificationTableRepo.Find(verificationTableGUID);
                verificationTableRepo.Remove(verificationTable);
                base.LogTransactionDelete<VerificationTable>(verificationTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenVerificationNumberSeqCode(VerificationTable verificationTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = numberSequenceService.IsManualByReferenceId(verificationTable.CompanyGUID, ReferenceId.Verification);
                if (!isManual)
                {
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(verificationTable.CompanyGUID, ReferenceId.Verification);
                    verificationTable.VerificationId = numberSequenceService.GetNumber(verificationTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public IEnumerable<SelectItem<VerificationTableItemView>> GetVerificationTableByVerificationTrans(SearchParameter search)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
                search = search.GetParentCondition(new string[] { VerificationTableCondition.BuyerTableGUID, VerificationTableCondition.CreditAppTableGUID, VerificationTableCondition.DocumentId });

                SearchCondition searchConditionStatusApproved = SearchConditionService.GetVerificationTableByStatusIdDropDown(((int)VerificationDocumentStatus.Approved).ToString());
                search.Conditions.Add(searchConditionStatusApproved);

                string documentId = search.Conditions[2].Value;
                List<string> verificationTableGUIDs = verificationTransRepo.GetVerificationTransByNotEqualDocumentIdNoTracking(GetCurrentCompany().StringToGuid(), documentId).Select(s => s.VerificationTableGUID.GuidNullToString()).Distinct().ToList();
                search.Conditions[2] = SearchConditionService.GetVerificationTableNotEqualDropDown(verificationTableGUIDs);

                return verificationTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateVerificationTableCollection(List<VerificationTableItemView> verificationTableItemViews)
        {
            try
            {
                if (verificationTableItemViews.Any())
                {
                    IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                    List<VerificationTable> verificationTables = verificationTableItemViews.ToVerificationTable().ToList();
                    verificationTableRepo.ValidateAdd(verificationTables);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(verificationTables, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void CreateVerificationLineCollection(List<VerificationLineItemView> verificationLineItemViews)
        {
            try
            {
                if (verificationLineItemViews.Any())
                {
                    IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                    List<VerificationLine> verificationLines = verificationLineItemViews.ToVerificationLine().ToList();
                    verificationLineRepo.ValidateAdd(verificationLines);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(verificationLines, false);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemVerificationTableStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Verification);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Function
        public VerificationTableItemView CreateCopyVerificationTable(VerificationTableItemView verificationTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                verificationTableView = accessLevelService.AssignOwnerBU(verificationTableView);
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                VerificationTable verificationTable = verificationTableView.ToVerificationTable();
                GenVerificationNumberSeqCode(verificationTable);
                verificationTable = verificationTableRepo.CreateVerificationTable(verificationTable);
                base.LogTransactionCreate<VerificationTable>(verificationTable);
                UnitOfWork.Commit();
                return verificationTable.ToVerificationTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationTableItemView GetUpdateVerificationStatusById(string refGUID)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                return verificationTableRepo.GetByIdvw(refGUID.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public UpdateVerificationStatusView UpdateUpdateVerificationStatus(UpdateVerificationStatusParamView functionView)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                UpdateVerificationStatusView updateVerificationStatus = new UpdateVerificationStatusView();
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                VerificationTable inputVerificationTable = verificationTableRepo.GetVerificationTableByIdNoTracking(functionView.updateVerificationStatusGUID.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus inputDocumentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(functionView.documentStatusGUID.StringToGuid());
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                if (inputDocumentStatus.Description == VerificationDocumentStatus.Approved.ToString())
                {
                    List<VerificationLine> verificationLine = verificationLineRepo.GetVerificationLineByVerificationTableNoTracking(inputVerificationTable.VerificationTableGUID);
                    if (verificationLine.Count > 0)
                    {
                        foreach (var item in verificationLine)
                        {
                            if (item.Pass == false)
                            {
                                NotificationResponse error = new NotificationResponse();
                                ex.AddData("ERROR.90005");
                            }
                        }
                    }
                    else
                    {
                        ex.AddData("ERROR.90005");
                    }
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    inputVerificationTable.DocumentStatusGUID = functionView.documentStatusGUID.StringToGuid();
                    VerificationTable dbVerificationTable = verificationTableRepo.Find(inputVerificationTable.VerificationTableGUID);
                    dbVerificationTable = verificationTableRepo.UpdateVerificationTable(dbVerificationTable, inputVerificationTable);
                    base.LogTransactionUpdate<VerificationTable>(GetOriginalValues<VerificationTable>(dbVerificationTable), dbVerificationTable);
                    UnitOfWork.Commit();
                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.00075", new string[] { "LABEL.STATUS" });
                    updateVerificationStatus.Notification = success;
                    return updateVerificationStatus;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        public VerificationTableItemView GetCopyVerificationTableById(string id)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                var result = verificationTableRepo.GetByIdvw(id.StringToGuid());
                result.HaveLine = verificationLineRepo.GetVerificationLineCount(result.VerificationTableGUID.StringToGuid());
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Function
        public IEnumerable<SelectItem<VerificationTableItemView>> GetVerificationTableDropDown(SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search = search.GetParentCondition(new string[] { VerificationTableCondition.BuyerTableGUID, VerificationTableCondition.CreditAppTableGUID });
                List<SearchCondition> searchConditions = SearchConditionService.GetVerificationDocumentStatusCondition(((int)VerificationDocumentStatus.Approved).ToString());
                search.Conditions.AddRange(searchConditions);
                return sysDropDownService.GetVerificationTableDropDown(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
