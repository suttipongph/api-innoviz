using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICAReqRetentionOutstandingService
	{

		CAReqRetentionOutstandingItemView GetCAReqRetentionOutstandingById(string id);
		CAReqRetentionOutstandingItemView CreateCAReqRetentionOutstanding(CAReqRetentionOutstandingItemView caReqRetentionOutstandingView);
		CAReqRetentionOutstandingItemView UpdateCAReqRetentionOutstanding(CAReqRetentionOutstandingItemView caReqRetentionOutstandingView);
		bool DeleteCAReqRetentionOutstanding(string id);
	}
	public class CAReqRetentionOutstandingService : SmartAppService, ICAReqRetentionOutstandingService
	{
		public CAReqRetentionOutstandingService(SmartAppDbContext context) : base(context) { }
		public CAReqRetentionOutstandingService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CAReqRetentionOutstandingService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqRetentionOutstandingService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CAReqRetentionOutstandingService() : base() { }

		public CAReqRetentionOutstandingItemView GetCAReqRetentionOutstandingById(string id)
		{
			try
			{
				ICAReqRetentionOutstandingRepo caReqRetentionOutstandingRepo = new CAReqRetentionOutstandingRepo(db);
				return caReqRetentionOutstandingRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqRetentionOutstandingItemView CreateCAReqRetentionOutstanding(CAReqRetentionOutstandingItemView caReqRetentionOutstandingView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				caReqRetentionOutstandingView = accessLevelService.AssignOwnerBU(caReqRetentionOutstandingView);
				ICAReqRetentionOutstandingRepo caReqRetentionOutstandingRepo = new CAReqRetentionOutstandingRepo(db);
				CAReqRetentionOutstanding caReqRetentionOutstanding = caReqRetentionOutstandingView.ToCAReqRetentionOutstanding();
				caReqRetentionOutstanding = caReqRetentionOutstandingRepo.CreateCAReqRetentionOutstanding(caReqRetentionOutstanding);
				base.LogTransactionCreate<CAReqRetentionOutstanding>(caReqRetentionOutstanding);
				UnitOfWork.Commit();
				return caReqRetentionOutstanding.ToCAReqRetentionOutstandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqRetentionOutstandingItemView UpdateCAReqRetentionOutstanding(CAReqRetentionOutstandingItemView caReqRetentionOutstandingView)
		{
			try
			{
				ICAReqRetentionOutstandingRepo caReqRetentionOutstandingRepo = new CAReqRetentionOutstandingRepo(db);
				CAReqRetentionOutstanding inputCAReqRetentionOutstanding = caReqRetentionOutstandingView.ToCAReqRetentionOutstanding();
				CAReqRetentionOutstanding dbCAReqRetentionOutstanding = caReqRetentionOutstandingRepo.Find(inputCAReqRetentionOutstanding.CAReqRetentionOutstandingGUID);
				dbCAReqRetentionOutstanding = caReqRetentionOutstandingRepo.UpdateCAReqRetentionOutstanding(dbCAReqRetentionOutstanding, inputCAReqRetentionOutstanding);
				base.LogTransactionUpdate<CAReqRetentionOutstanding>(GetOriginalValues<CAReqRetentionOutstanding>(dbCAReqRetentionOutstanding), dbCAReqRetentionOutstanding);
				UnitOfWork.Commit();
				return dbCAReqRetentionOutstanding.ToCAReqRetentionOutstandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCAReqRetentionOutstanding(string item)
		{
			try
			{
				ICAReqRetentionOutstandingRepo caReqRetentionOutstandingRepo = new CAReqRetentionOutstandingRepo(db);
				Guid caReqRetentionOutstandingGUID = new Guid(item);
				CAReqRetentionOutstanding caReqRetentionOutstanding = caReqRetentionOutstandingRepo.Find(caReqRetentionOutstandingGUID);
				caReqRetentionOutstandingRepo.Remove(caReqRetentionOutstanding);
				base.LogTransactionDelete<CAReqRetentionOutstanding>(caReqRetentionOutstanding);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
