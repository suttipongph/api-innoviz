using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDepartmentService
	{

		DepartmentItemView GetDepartmentById(string id);
		DepartmentItemView CreateDepartment(DepartmentItemView departmentView);
		DepartmentItemView UpdateDepartment(DepartmentItemView departmentView);
		bool DeleteDepartment(string id);
	}
	public class DepartmentService : SmartAppService, IDepartmentService
	{
		public DepartmentService(SmartAppDbContext context) : base(context) { }
		public DepartmentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DepartmentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DepartmentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DepartmentService() : base() { }

		public DepartmentItemView GetDepartmentById(string id)
		{
			try
			{
				IDepartmentRepo departmentRepo = new DepartmentRepo(db);
				return departmentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DepartmentItemView CreateDepartment(DepartmentItemView departmentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				departmentView = accessLevelService.AssignOwnerBU(departmentView);
				IDepartmentRepo departmentRepo = new DepartmentRepo(db);
				Department department = departmentView.ToDepartment();
				department = departmentRepo.CreateDepartment(department);
				base.LogTransactionCreate<Department>(department);
				UnitOfWork.Commit();
				return department.ToDepartmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DepartmentItemView UpdateDepartment(DepartmentItemView departmentView)
		{
			try
			{
				IDepartmentRepo departmentRepo = new DepartmentRepo(db);
				Department inputDepartment = departmentView.ToDepartment();
				Department dbDepartment = departmentRepo.Find(inputDepartment.DepartmentGUID);
				dbDepartment = departmentRepo.UpdateDepartment(dbDepartment, inputDepartment);
				base.LogTransactionUpdate<Department>(GetOriginalValues<Department>(dbDepartment), dbDepartment);
				UnitOfWork.Commit();
				return dbDepartment.ToDepartmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDepartment(string item)
		{
			try
			{
				IDepartmentRepo departmentRepo = new DepartmentRepo(db);
				Guid departmentGUID = new Guid(item);
				Department department = departmentRepo.Find(departmentGUID);
				departmentRepo.Remove(department);
				base.LogTransactionDelete<Department>(department);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
