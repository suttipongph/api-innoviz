using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;


namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ICreditAppTableService
    {

        CreditAppLineItemView GetCreditAppLineById(string id);
        CreditAppLineItemView CreateCreditAppLine(CreditAppLineItemView creditAppLineView);
        CreditAppLineItemView UpdateCreditAppLine(CreditAppLineItemView creditAppLineView);
        bool DeleteCreditAppLine(string id);
        void CreateCreditAppLineCollection(List<CreditAppLineItemView> creditAppLineItemViews);
        CreditAppTableItemView GetCreditAppTableById(string id);
        CreditAppTableItemView CreateCreditAppTable(CreditAppTableItemView creditAppTableView);
        CreditAppTableItemView UpdateCreditAppTable(CreditAppTableItemView creditAppTableView);
        CreditAppTable UpdateCreditAppTable(CreditAppTable creditAppTable);
        bool DeleteCreditAppTable(string id);
        void CreateCreditAppTableCollection(List<CreditAppTableItemView> creditAppTableItemViews);
        DocumentConditionTransItemView GetDocumentConditionTransChildInitialData(string refGUID);
        IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByMainAgreement(SearchParameter search);
        IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByReceiptTempTable(SearchParameter search);
        IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByProductType(SearchParameter search);

        MemoTransItemView GetMemoTransInitialData(string refGUID);
        MemoTransItemView GetMemoTransInitialDataByVerification(string refGUID);
        IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByCreditAppRequestTable(SearchParameter search);
        PurchaseTableItemView GetPurchaseTableInitialData(string creditAppTableGUID);
        PurchaseLineItemView GetPurchaseLineInitialData(string creditAppTableGUID);
        PurchaseLineItemView GetPurchaseLineRollBillInitialData(PurchaseLineRollBillInitialDataParm input);
        WithdrawalTableItemView GetWithdrawalTableInitialData(string creditAppTableGUID);
        decimal GetCreditLimitBalanceByCreditConditionType(string creditAppTableGUID, string transDate);
        decimal GetCreditLimitBalanceByCreditAppLine(string creditAppLineGUID);
        decimal GetCreditLimitBalanceByProduct(string buyerTableGUID, ProductType productType);
        IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByCreditAppTableWithdrawal(SearchParameter search);
        decimal GetBuyerCreditLimitBalanceByProduct(string buyerTableGUID, int productType, decimal creditLimit);
        decimal GetCustomerCreditLimitBalanceByProduct(string customerTableGUID, int productType, decimal creditLimit);
        IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByCustomerRefundTable(SearchParameter search);
        #region function
        ExtendExpiryDateParamView GetExtendExpiryDateById(string id);
        ExtendExpiryDateResultView ExtendExpiryDate(ExtendExpiryDateResultView functionView);
        UpdateExpectedSigningDateView GetUpdateExpectedSigningDateById(string id);
        UpdateExpectedSigningDateView UpdateExpectedSigningDate(UpdateExpectedSigningDateView functionView);
        #endregion
        #region shared
        DateTime GetEndDateByCreditAppTableGUID(string id);
        decimal GetCustomerCreditLimit(Guid customerTableGUID, int productType, DateTime requestDate);
        decimal GetRemainingCreditLoanRequest(Guid CreditAppTableGUID);
        decimal GetCustomerBuyerOutstanding(Guid creditAppTableGUID, Guid buyerTableGUID);
        decimal GetRetentionBalanceByCreditAppTableGUID(Guid creditAppTableGUID);
        SearchResult<CALineOutstandingView> GetCALineOutstandingByCA(SearchParameter search);
        decimal GetRetentionBalanceByCreditAppTableGUID(CreditAppTable creditAppTable);
        decimal GetCustomerAllBuyerOutstanding(Guid creditAppTableGUID);
        #endregion
        IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLine(SearchParameter search);
        IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByProduct(SearchParameter search);
        IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByProductAndBuyer(SearchParameter search);
        BookmarkDocumentCreditApplication GetBookmaskDocumentCreditApplication(Guid refGUID, Guid bookmarkDocumentTransGuid);
        IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByCreditAppTable(SearchParameter search);



        void GenCreditAppNumberSeqCode(CreditAppTable creditAppTable);
        BookmarkDocumentQueryCreditApplicationSharedAndPF GetBookmarkDocumentQueryCreditApplicationSharedAndPF(Guid refGUID, Guid bookmarkDocumentTransGuid);
        BookmarkDocumentCreditApplicationConsideration GetBookmaskDocumentCreditApplicationConsideration(Guid refGUID, Guid bookmarkDocumentTransGuid);
        #region Loan Request
        AccessModeView GetAccessModeLoanRequestByCreditAppTable(string creditAppId);
        #endregion Loan Request
    }
    public class CreditAppTableService : SmartAppService, ICreditAppTableService
    {
        public CreditAppTableService(SmartAppDbContext context) : base(context) { }
        public CreditAppTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public CreditAppTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CreditAppTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public CreditAppTableService() : base() { }

        public CreditAppLineItemView GetCreditAppLineById(string id)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                return creditAppLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppLineItemView CreateCreditAppLine(CreditAppLineItemView creditAppLineView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                creditAppLineView = accessLevelService.AssignOwnerBU(creditAppLineView);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine creditAppLine = creditAppLineView.ToCreditAppLine();
                creditAppLine = creditAppLineRepo.CreateCreditAppLine(creditAppLine);
                base.LogTransactionCreate<CreditAppLine>(creditAppLine);
                UnitOfWork.Commit();
                return creditAppLine.ToCreditAppLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppLineItemView UpdateCreditAppLine(CreditAppLineItemView creditAppLineView)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine inputCreditAppLine = creditAppLineView.ToCreditAppLine();
                CreditAppLine dbCreditAppLine = creditAppLineRepo.Find(inputCreditAppLine.CreditAppLineGUID);
                dbCreditAppLine = creditAppLineRepo.UpdateCreditAppLine(dbCreditAppLine, inputCreditAppLine);
                base.LogTransactionUpdate<CreditAppLine>(GetOriginalValues<CreditAppLine>(dbCreditAppLine), dbCreditAppLine);
                UnitOfWork.Commit();
                return dbCreditAppLine.ToCreditAppLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCreditAppLine(string item)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                Guid creditAppLineGUID = new Guid(item);
                CreditAppLine creditAppLine = creditAppLineRepo.Find(creditAppLineGUID);
                creditAppLineRepo.Remove(creditAppLine);
                base.LogTransactionDelete<CreditAppLine>(creditAppLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CreditAppTableItemView GetCreditAppTableById(string id)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                return creditAppTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppTableItemView CreateCreditAppTable(CreditAppTableItemView creditAppTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                creditAppTableView = accessLevelService.AssignOwnerBU(creditAppTableView);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableView.ToCreditAppTable();
                creditAppTable = creditAppTableRepo.CreateCreditAppTable(creditAppTable);
                base.LogTransactionCreate<CreditAppTable>(creditAppTable);
                UnitOfWork.Commit();
                return creditAppTable.ToCreditAppTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppTableItemView UpdateCreditAppTable(CreditAppTableItemView creditAppTableView)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableView.ToCreditAppTable();
                creditAppTable = UpdateCreditAppTable(creditAppTable);
                UnitOfWork.Commit();
                return creditAppTable.ToCreditAppTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppTable UpdateCreditAppTable(CreditAppTable creditAppTable)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable dbCreditAppTable = creditAppTableRepo.Find(creditAppTable.CreditAppTableGUID);
                dbCreditAppTable = creditAppTableRepo.UpdateCreditAppTable(dbCreditAppTable, creditAppTable);
                base.LogTransactionUpdate<CreditAppTable>(GetOriginalValues<CreditAppTable>(dbCreditAppTable), dbCreditAppTable);
                return dbCreditAppTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCreditAppTable(string item)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                Guid creditAppTableGUID = new Guid(item);
                CreditAppTable creditAppTable = creditAppTableRepo.Find(creditAppTableGUID);
                creditAppTableRepo.Remove(creditAppTable);
                base.LogTransactionDelete<CreditAppTable>(creditAppTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByMainAgreement(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                Guid mainAgreementTableGUID = search.Conditions[search.Conditions.Count - 1].Value.StringToGuid();
                search.Conditions.RemoveAt(search.Conditions.Count - 1);
                search = search.GetParentCondition(new string[] { CreditAppTableCondition.CustomerTableGUID, CreditAppTableCondition.ProductType });
                return creditAppTableRepo.GetDropDownItemByMainAgreementTable(search, mainAgreementTableGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByCreditAppRequestTable(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                search = search.GetParentCondition(new string[] { CreditAppTableCondition.CustomerTableGUID, CreditAppTableCondition.CreditLimitTypeGUID, CreditAppTableCondition.InactiveDate });
                return creditAppTableRepo.GetDropDownItemByCreditAppRequestTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
            public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByProductType(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                search = search.GetParentCondition( new string[] { MainAgreementCondition.ProductType , MainAgreementCondition.CustomerGUID });
                return creditAppTableRepo.GetDropDownItemByProductType(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByReceiptTempTable(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                search = search.GetParentCondition(new string[] { CreditAppTableCondition.CustomerTableGUID, CreditAppTableCondition.ProductType });
                return creditAppTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTableByCustomerRefundTable(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                search = search.GetParentCondition(new string[] { CreditAppTableCondition.CustomerTableGUID, CreditAppTableCondition.ProductType });
                return creditAppTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialData(string refGUID)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                string refId = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppId;
                return serviceFeeConditionTransService.GetServiceFeeConditionTransInitialDataByCreditAppTable(refId, refGUID, RefType.CreditAppTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DocumentConditionTransItemView GetDocumentConditionTransChildInitialData(string refGUID)
        {
            try
            {
                IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                int lineNum = creditAppLineRepo.GetCreditAppLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).LineNum;
                return documentConditionTransService.GetDocumentConditionTransInitialData(lineNum.ToString(), refGUID, RefType.CreditAppLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region function
        public ExtendExpiryDateParamView GetExtendExpiryDateById(string id)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ExtendExpiryDateParamView extendExpiryDateParamView = creditAppTableRepo.GetExtendExpiryDateById(id.StringToGuid());
                extendExpiryDateParamView.ExpiryDate = GetEndDateByCreditAppTableGUID(id).DateToString();
                return extendExpiryDateParamView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool GetExtendExpiryDateValidation(ExtendExpiryDateResultView functionView)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                if (functionView.CreditLimitExpiration != (int)CreditLimitExpiration.ByBuyerAgreement)
                {
                    ex.AddData("ERROR.90023");
                }
                if (functionView.OriginalExpiryDate.StringToDate() >= functionView.ExpiryDate.StringToDate())
                {
                    ex.AddData("ERROR.90025");
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ExtendExpiryDateResultView ExtendExpiryDate(ExtendExpiryDateResultView functionView)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                NotificationResponse success = new NotificationResponse();
                if (GetExtendExpiryDateValidation(functionView))
                {
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(functionView.CreditAppTableGUID.StringToGuid());
                    creditAppTable.ExpiryDate = functionView.ExpiryDate.StringToDate();
                    creditAppTable = UpdateCreditAppTable(creditAppTable);
                    UnitOfWork.Commit();
                }
                success.AddData("SUCCESS.00075", new string[] { "LABEL.EXPIRY_DATE" });
                functionView.Notification = success;
                return functionView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public UpdateExpectedSigningDateView GetUpdateExpectedSigningDateById(string id)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                UpdateExpectedSigningDateView updateExpectedSigningDateView = creditAppTableRepo.GetUpdateExpectedSigningDateById(id.StringToGuid());
                return updateExpectedSigningDateView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool GetUpdateExpectedSigningDateValidation(UpdateExpectedSigningDateView functionView)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(functionView.CreditAppTableGUID.StringToGuid());
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                if (creditAppTable.InactiveDate != null && functionView.ExpectedAgmSigningDate.StringToDate() > creditAppTable.InactiveDate)
                {
                    ex.AddData("ERROR.90026",new string[] { "LABEL.CREDIT_APPLICATION", functionView.CustomerTable_Values });
                }
                if (functionView.ExpectedAgmSigningDate.StringToDate() > creditAppTable.ExpiryDate)
                {
                    ex.AddData("ERROR.90022", new string[] { "LABEL.CREDIT_APPLICATION", functionView.CustomerTable_Values });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public UpdateExpectedSigningDateView UpdateExpectedSigningDate(UpdateExpectedSigningDateView functionView)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                NotificationResponse success = new NotificationResponse();
                if (GetUpdateExpectedSigningDateValidation(functionView))
                {
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(functionView.CreditAppTableGUID.StringToGuid());
                    creditAppTable.ExpectedAgreementSigningDate = functionView.ExpectedAgmSigningDate.StringToDate();
                    creditAppTable = UpdateCreditAppTable(creditAppTable);
                    UnitOfWork.Commit();
                }
                success.AddData("SUCCESS.00075", new string[] { "LABEL.UPDATE_AGREEMENT_SIGNING_DATE" });
                functionView.Notification = success;
                return functionView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion       
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialDataByVerification(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                string refId = verificationTableRepo.GetVerificationTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).VerificationId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.Verification);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenCreditAppNumberSeqCode(CreditAppTable creditAppTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByCreditApp(creditAppTable.ProductType);
                if (!isManual)
                {
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetCreditAppNumberSeqGUID(creditAppTable.CompanyGUID, creditAppTable.ProductType);
                    creditAppTable.CreditAppId = numberSequenceService.GetNumber(creditAppTable.CompanyGUID, null, numberSeqTableGUID);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public bool IsManualByCreditApp(int productType)
        {
            try
            {
                Guid companyGUID = GetCurrentCompany().StringToGuid();
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetCreditAppNumberSeqGUID(companyGUID, productType);
                return numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTableItemView GetPurchaseTableInitialData(string creditAppTableGUID)
        {
            try
            {
                CreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(creditAppTableGUID.StringToGuid());

                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return purchaseTableService.GetPurchaseTableInitialDataByCreditAppTable(creditAppTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLineItemView GetPurchaseLineInitialData(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID.StringToGuid());
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(purchaseTable.CreditAppTableGUID);

                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return purchaseTableService.GetPurchaseLineInitialDataByCreditAppTable(purchaseTable, creditAppTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLineItemView GetPurchaseLineRollBillInitialData(PurchaseLineRollBillInitialDataParm input)
        {
            try
            {
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                return purchaseTableService.GetPurchaseLineRollBillInitialDataByCreditAppTable(input);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public WithdrawalTableItemView GetWithdrawalTableInitialData(string creditAppTableGUID)
        {
            try
            {
                CreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(creditAppTableGUID.StringToGuid());

                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                return withdrawalTableService.GetWithdrawalTableInitialDataByCreditAppTable(creditAppTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByCreditAppTableWithdrawal(SearchParameter search)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                search = search.GetParentCondition(new string[] { CreditAppLineCondition.CreditAppTableGUID, CreditAppLineCondition.ExpiryDate });
                DateTime expiryDate = search.Conditions[1].Value.StringToDate();
                search.Conditions.RemoveAt(1);
                return creditAppLineRepo.GetDropDownItemByWithdrawalTable(search, expiryDate);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateCreditAppTableCollection(List<CreditAppTableItemView> creditAppTableItemViews)
        {
            try
            {
                if (creditAppTableItemViews.Any())
                {
                    ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                    List<CreditAppTable> creditAppTables = creditAppTableItemViews.ToCreditAppTable().ToList();
                    creditAppTableRepo.ValidateAdd(creditAppTables);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(creditAppTables, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void CreateCreditAppLineCollection(List<CreditAppLineItemView> creditAppLineItemViews)
        {
            try
            {
                if (creditAppLineItemViews.Any())
                {
                    ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                    List<CreditAppLine> creditAppLines = creditAppLineItemViews.ToCreditAppLine().ToList();
                    creditAppLineRepo.ValidateAdd(creditAppLines);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(creditAppLines, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #region shared
        public decimal GetCustomerCreditLimit(Guid customerTableGUID, int productType, DateTime requestDate)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                return creditAppTableRepo.GetCustomerCreditLimit(customerTableGUID, productType, requestDate);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DateTime GetEndDateByCreditAppTableGUID(string id)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                return creditAppTableRepo.GetEndDateByCreditAppTableGUID(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetRemainingCreditLoanRequest(Guid CreditAppTableGUID)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.Find(CreditAppTableGUID);
                IEnumerable<CreditAppLine> creditAppLines = creditAppLineRepo.GetCreditAppLineByCreditAppIdNoTracking(CreditAppTableGUID);
                decimal totalCreditAppLne = 0;
                if (creditAppLines.Any())
                {
                    totalCreditAppLne = creditAppLines.Sum(su => su.ApprovedCreditLimitLine);
                }
                return creditAppTable.ApprovedCreditLimit - totalCreditAppLne;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetCustomerBuyerOutstanding(Guid creditAppTableGUID, Guid buyerTableGUID)
        {
            try
            {
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                IQueryable<CustTrans> custTrans = custTransRepo.GetByIsProductInvoice()
                                                               .Where(w => w.CreditAppTableGUID == creditAppTableGUID
                                                                        && w.BuyerTableGUID == buyerTableGUID
                                                                        && w.CustTransStatus == (int)CustTransStatus.Open);
                return (custTrans.Any() ? custTrans.Sum(su => su.TransAmount - su.SettleAmount) : 0);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetRetentionBalanceByCreditAppTableGUID(Guid creditAppTableGUID)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppTableGUID);
                return GetRetentionBalanceByCreditAppTableGUID(creditAppTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetRetentionBalanceByCreditAppTableGUID(CreditAppTable creditAppTable)
        {
            try
            {
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                decimal maxRetentionAmount = 0.0m;
                decimal retentionAccumAmount = 0.0m;
                if (creditAppTable != null)
                {
                    maxRetentionAmount = creditAppTable.MaxRetentionAmount;
                    retentionAccumAmount = retentionTransService.GetRetentionAccumByCreditAppTableGUID(creditAppTable.CreditAppTableGUID);
                }
                return maxRetentionAmount - retentionAccumAmount;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchResult<CALineOutstandingView> GetCALineOutstandingByCA(SearchParameter search)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                return creditAppLineRepo.GetCALineOutstandingByCA(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public List<CALineOutstandingViewMap> GetCALineOutstandingByCA(int refType, Guid refGUID, bool active = false, DateTime? asOfDate = null)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                return creditAppLineRepo.GetCALineOutstandingByCA(refType, refGUID, active, asOfDate);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLine(SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search = search.GetParentCondition(new string[] { CreditAppLineCondition.BuyerTableGUID });
                return sysDropDownService.GetDropDownItemCreditAppLine(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByProduct(SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search = search.GetParentCondition(new string[] { CreditAppLineCondition.ProductType, CreditAppLineCondition.BuyerTableGUID });
                return sysDropDownService.GetDropDownItemCreditAppLine(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByProductAndBuyer(SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search = search.GetParentCondition(new string[] { CreditAppLineCondition.ProductType, CreditAppLineCondition.BuyerTableGUID });
                return sysDropDownService.GetDropDownItemCreditAppLine(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLineByCreditAppTable(SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search = search.GetParentCondition(new string[] { CreditAppLineCondition.CreditAppTableGUID });
                return sysDropDownService.GetDropDownItemCreditAppLine(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetCustomerAllBuyerOutstanding(Guid creditAppTableGUID)
        {
            try
            {
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                IQueryable<CustTrans> custTrans = custTransRepo.GetByIsProductInvoice()
                                                               .Where(w => w.CreditAppTableGUID == creditAppTableGUID
                                                                        && w.CustTransStatus == (int)CustTransStatus.Open);
                return (custTrans.Any() ? custTrans.Sum(su => su.TransAmount - su.SettleAmount) : 0);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetCreditLimitBalanceByCreditConditionType
        public decimal GetCreditLimitBalanceByCreditConditionType(string creditAppTableGUID, string transDate)
        {
            try
            {
                decimal creditLimitBalance = 0;
                decimal approvedCreditLimit = 0;
                decimal sumCreditAppTrans = 0;

                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppTableGUID.StringToGuid());

                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);
                List<CreditLimitType> creditLimitTypes = creditLimitTypeRepo.GetCreditLimitTypeByCreditLImitConditionTypeNoTracking(creditAppTable.CompanyGUID, (int)CreditLimitConditionType.ByCustomer).ToList();

                ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
                sumCreditAppTrans = creditAppTransRepo.GetCreditAppTransByCreditAppTableNoTracking(creditAppTable.CreditAppTableGUID).Sum(t => t.CreditDeductAmount);

                switch (creditLimitType.CreditLimitConditionType)
                {
                    case (int)CreditLimitConditionType.ByCreditApplication:
                        approvedCreditLimit = creditAppTable.ApprovedCreditLimit;
                        break;
                    case (int)CreditLimitConditionType.ByCustomer:
                        List<CreditAppTable> creditAppTables = creditAppTableRepo.GetCreditAppTableByCompanyNoTracking(creditAppTable.CompanyGUID)
                                       .Where(t => t.ProductType == creditAppTable.ProductType && t.CustomerTableGUID == creditAppTable.CustomerTableGUID &&
                                                   t.ExpiryDate > transDate.StringToDate() && t.ConsortiumTableGUID == creditAppTable.ConsortiumTableGUID).ToList();
                        approvedCreditLimit = (from ca in creditAppTables
                                               join clt in creditLimitTypes on ca.CreditLimitTypeGUID equals clt.CreditLimitTypeGUID
                                               select ca).Sum(s => s.ApprovedCreditLimit);
                        break;
                    default:
                        approvedCreditLimit = 0;
                        break;
                }

                creditLimitBalance = approvedCreditLimit - sumCreditAppTrans;

                return creditLimitBalance;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetCreditLimitBalanceByCreditAppLine
        public decimal GetCreditLimitBalanceByCreditAppLine(string creditAppLineGUID)
        {
            try
            {
                decimal creditLimitBalance = 0;
                decimal approvedCreditLimit = 0;
                decimal sumCreditAppTrans = 0;

                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(creditAppLineGUID.StringToGuid());

                ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
                sumCreditAppTrans = creditAppTransRepo.GetCreditAppTransByCreditAppLineNoTracking(creditAppLine.CreditAppLineGUID).Sum(t => t.InvoiceAmount);
                approvedCreditLimit = creditAppLine.ApprovedCreditLimitLine;
                creditLimitBalance = approvedCreditLimit - sumCreditAppTrans;

                return creditLimitBalance;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetCreditLimitBalanceByProduct
        public decimal GetCreditLimitBalanceByProduct(string buyerTableGUID, ProductType productType)
        {
            try
            {
                decimal creditLimitBalance = 0;
                decimal approvedCreditLimit = 0;
                decimal sumCreditAppTrans = 0;

                IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
                BuyerCreditLimitByProduct buyerCreditLimitByProduct = buyerCreditLimitByProductRepo.GetBuyerCreditLimitByProductByBuyer(buyerTableGUID.StringToGuid())
                    .Where(t => t.ProductType == Convert.ToInt32(productType)).OrderByDescending(o => o.CreatedDateTime).FirstOrDefault();

                ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
                sumCreditAppTrans = creditAppTransRepo.GetCreditAppTransByBuyerTableNoTracking(buyerTableGUID.StringToGuid())
                    .Where(t => t.ProductType == Convert.ToInt32(productType)).Sum(s => s.InvoiceAmount);
                approvedCreditLimit = (buyerCreditLimitByProduct != null) ? buyerCreditLimitByProduct.CreditLimit : 0;

                creditLimitBalance = approvedCreditLimit - sumCreditAppTrans;

                return creditLimitBalance;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Loan Request
        public AccessModeView GetAccessModeLoanRequestByCreditAppTable(string creditAppId)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppId.StringToGuid());
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);
                AccessModeView accessModeView = new AccessModeView();
                DateTime dateNow = DateTime.Now;
                accessModeView.CanCreate = true;
                accessModeView.CanView = true;
                accessModeView.CanDelete = false;
                if (creditAppTable.InactiveDate.HasValue)
                {
                    accessModeView.CanCreate = (dateNow < creditAppTable.ExpiryDate && dateNow < creditAppTable.InactiveDate && creditLimitType.BuyerMatchingAndLoanRequest);
                }
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion  Loan Request
        #region GetBuyerCreditLimitByProduct
        public decimal GetBuyerCreditLimitBalanceByProduct(string buyerTableGUID, int productType, decimal creditLimit)
        {
            try
            {
                decimal creditLimitBalance = 0;
                decimal sumCreditAppTrans = 0;

                ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
                sumCreditAppTrans = creditAppTransRepo.GetCreditAppTransByBuyerTableNoTracking(buyerTableGUID.StringToGuid()).Where(t => t.ProductType == productType).Sum(s => s.InvoiceAmount);

                creditLimitBalance = creditLimit - sumCreditAppTrans;

                return creditLimitBalance;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetCustomerCreditLimitBalanceByProduct
        public decimal GetCustomerCreditLimitBalanceByProduct(string customerTableGUID, int productType, decimal creditLimit)
        {
            try
            {
                decimal creditLimitBalance = 0;
                decimal sumCreditAppTrans = 0;

                ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
                sumCreditAppTrans = creditAppTransRepo.GetCreditAppTransByCustomerTableNoTracking(customerTableGUID.StringToGuid()).Where(t => t.ProductType == productType).Sum(s => s.CreditDeductAmount);
                creditLimitBalance = creditLimit - sumCreditAppTrans;

                return creditLimitBalance;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetCreditOutstandingByCustomer
        public List<CreditOutstandingViewMap> GetCreditOutstandingByCustomer(Guid customerTableGUID, DateTime asOfDate)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                return creditAppTableRepo.GetCreditOutstandingByCustomer(customerTableGUID, asOfDate);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Bookmark document query 15CreditApplicationShared
        public BookmarkDocumentCreditApplication GetBookmaskDocumentCreditApplication(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                ICreditAppTableRepo CreditAppTableRepo = new CreditAppTableRepo(db);
                BookmarkDocumentCreditApplication result = CreditAppTableRepo.GetBookmaskDocumentCreditApplication(refGUID);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion
        #region Bookmark document query_18CreditApplicationConsideration
        public BookmarkDocumentCreditApplicationConsideration GetBookmaskDocumentCreditApplicationConsideration(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                ICreditAppTableRepo CreditAppTableRepo = new CreditAppTableRepo(db);
                BookmarkDocumentCreditApplicationConsideration result = CreditAppTableRepo.GetBookmarkDocumentCreditApplicationConsideration(refGUID);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion
        #region bookmarkCreditAppSharedAndPF
        public BookmarkDocumentQueryCreditApplicationSharedAndPF GetBookmarkDocumentQueryCreditApplicationSharedAndPF(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                var creditAppTableModel = creditAppTableRepo.GetBookmaskDocumentCreditApplication(refGUID);
                var creditAppRequestModel = creditAppRequestTableRepo.GetBookmarkDocumentQueryCreditApplicationPFValues(refGUID, bookmarkDocumentTransGuid);
                BookmarkDocumentQueryCreditApplicationSharedAndPF model = new BookmarkDocumentQueryCreditApplicationSharedAndPF();
                MergeModel(model, creditAppTableModel, creditAppRequestModel);
                return model;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        private void MergeModel(BookmarkDocumentQueryCreditApplicationSharedAndPF model, BookmarkDocumentCreditApplication creditAppTableModel, BookmarkDocumentQueryCreditApplicationPF creditAppRequestModel)
        {
            try
            {
                model.QCreditAppRequestTable_CreditAppRequestId = creditAppTableModel.QCreditAppRequestTable_CreditAppRequestId;
                model.QCreditAppRequestTable_OriginalCreditAppTableId = creditAppTableModel.QCreditAppRequestTable_OriginalCreditAppTableId;
                model.QCreditAppRequestTable_Status = creditAppTableModel.QCreditAppRequestTable_Status;
                model.QCreditAppRequestTable_RequestDate = creditAppTableModel.QCreditAppRequestTable_RequestDate;
                model.QCreditAppRequestTable_CustomerName = creditAppTableModel.QCreditAppRequestTable_CustomerName;
                model.QCreditAppRequestTable_CustomerId = creditAppTableModel.QCreditAppRequestTable_CustomerId;
                model.QCreditAppRequestTable_ResponsibleBy = creditAppTableModel.QCreditAppRequestTable_ResponsibleBy;
                model.QCreditAppRequestTable_KYC = creditAppTableModel.QCreditAppRequestTable_KYC;
                model.QCreditAppRequestTable_CreditScoring = creditAppTableModel.QCreditAppRequestTable_CreditScoring;
                model.QCreditAppRequestTable_Description = creditAppTableModel.QCreditAppRequestTable_Description;
                model.QCreditAppRequestTable_Purpose = creditAppTableModel.QCreditAppRequestTable_Purpose;
                model.QCreditAppRequestTable_CARemark = creditAppTableModel.QCreditAppRequestTable_CARemark;
                model.QCreditAppRequestTable_ProductType = creditAppTableModel.QCreditAppRequestTable_ProductType;
                model.QCreditAppRequestTable_ProductSubType = creditAppTableModel.QCreditAppRequestTable_ProductSubType;
                model.QCreditAppRequestTable_CreditLimitTypeId = creditAppTableModel.QCreditAppRequestTable_CreditLimitTypeId;
                model.QCreditAppRequestTable_CreditLimitExpiration = creditAppTableModel.QCreditAppRequestTable_CreditLimitExpiration;
                model.QCreditAppRequestTable_StartDate = creditAppTableModel.QCreditAppRequestTable_StartDate;
                model.QCreditAppRequestTable_ExpiryDate = creditAppTableModel.QCreditAppRequestTable_ExpiryDate;
                model.QCreditAppRequestTable_CreditLimitRequest = creditAppTableModel.QCreditAppRequestTable_CreditLimitRequest;
                model.QCreditAppRequestTable_CustomerCreditLimit = creditAppTableModel.QCreditAppRequestTable_CustomerCreditLimit;
                model.QCreditAppRequestTable_InterestType = creditAppTableModel.QCreditAppRequestTable_InterestType;
                model.QCreditAppRequestTable_InterestAdjustment = creditAppTableModel.QCreditAppRequestTable_InterestAdjustment;
                model.Variable_NewInterestRate = creditAppTableModel.Variable_NewInterestRate;
                model.QCreditAppRequestTable_MaxPurchasePct = creditAppTableModel.QCreditAppRequestTable_MaxPurchasePct;
                model.QCreditAppRequestTable_MaxRetentionPct = creditAppTableModel.QCreditAppRequestTable_MaxRetentionPct;
                model.QCreditAppRequestTable_MaxRetentionAmount = creditAppTableModel.QCreditAppRequestTable_MaxRetentionAmount;
                model.QCreditAppRequestTable_CustPDCBankName = creditAppTableModel.QCreditAppRequestTable_CustPDCBankName;
                model.QCreditAppRequestTable_CustPDCBankBranch = creditAppTableModel.QCreditAppRequestTable_CustPDCBankBranch;
                model.QCreditAppRequestTable_CustPDCBankType = creditAppTableModel.QCreditAppRequestTable_CustPDCBankType;
                model.QCreditAppRequestTable_CustPDCBankAccountName = creditAppTableModel.QCreditAppRequestTable_CustPDCBankAccountName;
                model.QCreditAppRequestTable_CreditRequestFeePct = creditAppTableModel.QCreditAppRequestTable_CreditRequestFeePct;
                model.QCreditAppRequestTable_CreditRequestFeeAmount = creditAppTableModel.QCreditAppRequestTable_CreditRequestFeeAmount;
                model.QCreditAppRequestTable_PurchaseFeePct = creditAppTableModel.QCreditAppRequestTable_PurchaseFeePct;
                model.QCreditAppRequestTable_PurchaseFeeCalculateBase = creditAppTableModel.QCreditAppRequestTable_PurchaseFeeCalculateBase;
                model.QCreditAppRequestTable_CACondition = creditAppTableModel.QCreditAppRequestTable_CACondition;
                model.QRetentionConditionTrans1_RetentionDeductionMethod = creditAppTableModel.QRetentionConditionTrans1_RetentionDeductionMethod;
                model.QRetentionConditionTrans1_RetentionCalculateBase = creditAppTableModel.QRetentionConditionTrans1_RetentionCalculateBase;
                model.QRetentionConditionTrans1_RetentionPct = creditAppTableModel.QRetentionConditionTrans1_RetentionPct;
                model.tentionConditionTrans1_RetentionAmount = creditAppTableModel.tentionConditionTrans1_RetentionAmount;
                model.QRetentionConditionTrans2_RetentionDeductionMethod = creditAppTableModel.QRetentionConditionTrans2_RetentionDeductionMethod;
                model.QRetentionConditionTrans2_RetentionCalculateBase = creditAppTableModel.QRetentionConditionTrans2_RetentionCalculateBase;
                model.QRetentionConditionTrans2_RetentionPct = creditAppTableModel.QRetentionConditionTrans2_RetentionPct;
                model.tentionConditionTrans2_RetentionAmount = creditAppTableModel.tentionConditionTrans2_RetentionAmount;
                model.QRetentionConditionTrans3_RetentionDeductionMethod = creditAppTableModel.QRetentionConditionTrans3_RetentionDeductionMethod;
                model.QRetentionConditionTrans3_RetentionCalculateBase = creditAppTableModel.QRetentionConditionTrans3_RetentionCalculateBase;
                model.QRetentionConditionTrans3_RetentionPct = creditAppTableModel.QRetentionConditionTrans3_RetentionPct;
                model.tentionConditionTrans3_RetentionAmount = creditAppTableModel.tentionConditionTrans3_RetentionAmount;
                model.QServiceFeeConditionTrans1_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans1_ServicefeeTypeId;
                model.QServiceFeeConditionTrans1_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans1_AmountBeforeTax;
                model.QServiceFeeConditionTrans1_Description = creditAppTableModel.QServiceFeeConditionTrans1_Description;
                model.QServiceFeeConditionTrans2_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans2_ServicefeeTypeId;
                model.QServiceFeeConditionTrans2_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans2_AmountBeforeTax;
                model.QServiceFeeConditionTrans2_Description = creditAppTableModel.QServiceFeeConditionTrans2_Description;
                model.QServiceFeeConditionTrans3_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans3_ServicefeeTypeId;
                model.QServiceFeeConditionTrans3_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans3_AmountBeforeTax;
                model.QServiceFeeConditionTrans3_Description = creditAppTableModel.QServiceFeeConditionTrans3_Description;
                model.QServiceFeeConditionTrans4_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans4_ServicefeeTypeId;
                model.QServiceFeeConditionTrans4_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans4_AmountBeforeTax;
                model.QServiceFeeConditionTrans4_Description = creditAppTableModel.QServiceFeeConditionTrans4_Description;
                model.QServiceFeeConditionTrans5_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans5_ServicefeeTypeId;
                model.QServiceFeeConditionTrans5_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans5_AmountBeforeTax;
                model.QServiceFeeConditionTrans5_Description = creditAppTableModel.QServiceFeeConditionTrans5_Description;
                model.QServiceFeeConditionTrans6_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans6_ServicefeeTypeId;
                model.QServiceFeeConditionTrans6_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans6_AmountBeforeTax;
                model.QServiceFeeConditionTrans6_Description = creditAppTableModel.QServiceFeeConditionTrans6_Description;
                model.QServiceFeeConditionTrans7_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans7_ServicefeeTypeId;
                model.QServiceFeeConditionTrans7_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans7_AmountBeforeTax;
                model.QServiceFeeConditionTrans7_Description = creditAppTableModel.QServiceFeeConditionTrans7_Description;
                model.QServiceFeeConditionTrans8_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans8_ServicefeeTypeId;
                model.QServiceFeeConditionTrans8_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans8_AmountBeforeTax;
                model.QServiceFeeConditionTrans8_Description = creditAppTableModel.QServiceFeeConditionTrans8_Description;
                model.QServiceFeeConditionTrans9_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans9_ServicefeeTypeId;
                model.QServiceFeeConditionTrans9_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans9_AmountBeforeTax;
                model.QServiceFeeConditionTrans9_Description = creditAppTableModel.QServiceFeeConditionTrans9_Description;
                model.QServiceFeeConditionTrans10_ServicefeeTypeId = creditAppTableModel.QServiceFeeConditionTrans10_ServicefeeTypeId;
                model.QServiceFeeConditionTrans10_AmountBeforeTax = creditAppTableModel.QServiceFeeConditionTrans10_AmountBeforeTax;
                model.QServiceFeeConditionTrans10_Description = creditAppTableModel.QServiceFeeConditionTrans10_Description;
                model.QGuarantorTrans1_RelatedPersonName = creditAppTableModel.QGuarantorTrans1_RelatedPersonName;
                model.QGuarantorTrans1_GuarantorType = creditAppTableModel.QGuarantorTrans1_GuarantorType;
                model.QGuarantorTrans2_RelatedPersonName = creditAppTableModel.QGuarantorTrans2_RelatedPersonName;
                model.QGuarantorTrans2_GuarantorType = creditAppTableModel.QGuarantorTrans2_GuarantorType;
                model.QGuarantorTrans3_RelatedPersonName = creditAppTableModel.QGuarantorTrans3_RelatedPersonName;
                model.QGuarantorTrans3_GuarantorType = creditAppTableModel.QGuarantorTrans3_GuarantorType;
                model.QGuarantorTrans4_RelatedPersonName = creditAppTableModel.QGuarantorTrans4_RelatedPersonName;
                model.QGuarantorTrans4_GuarantorType = creditAppTableModel.QGuarantorTrans4_GuarantorType;
                model.QGuarantorTrans5_RelatedPersonName = creditAppTableModel.QGuarantorTrans5_RelatedPersonName;
                model.QGuarantorTrans5_GuarantorType = creditAppTableModel.QGuarantorTrans5_GuarantorType;
                model.QGuarantorTrans6_RelatedPersonName = creditAppTableModel.QGuarantorTrans6_RelatedPersonName;
                model.QGuarantorTrans6_GuarantorType = creditAppTableModel.QGuarantorTrans6_GuarantorType;
                model.QGuarantorTrans7_RelatedPersonName = creditAppTableModel.QGuarantorTrans7_RelatedPersonName;
                model.QGuarantorTrans7_GuarantorType = creditAppTableModel.QGuarantorTrans7_GuarantorType;
                model.QGuarantorTrans8_RelatedPersonName = creditAppTableModel.QGuarantorTrans8_RelatedPersonName;
                model.QGuarantorTrans8_GuarantorType = creditAppTableModel.QGuarantorTrans8_GuarantorType;
                model.Variable_IsNew1 = creditAppTableModel.Variable_IsNew1;
                model.QCreditAppReqAssignment1_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment1_AssignmentBuyer;
                model.QCreditAppReqAssignment1_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment1_RefAgreementId;
                model.QCreditAppReqAssignment1_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment1_BuyerAgreementAmount;
                model.QCreditAppReqAssignment1_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment1_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment1_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment1_AssignmentAgreementDate;
                model.QCreditAppReqAssignment1_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment1_RemainingAmount;
                model.Variable_IsNew2 = creditAppTableModel.Variable_IsNew2;
                model.QCreditAppReqAssignment2_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment2_AssignmentBuyer;
                model.QCreditAppReqAssignment2_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment2_RefAgreementId;
                model.QCreditAppReqAssignment2_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment2_BuyerAgreementAmount;
                model.QCreditAppReqAssignment2_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment2_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment2_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment2_AssignmentAgreementDate;
                model.QCreditAppReqAssignment2_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment2_RemainingAmount;
                model.Variable_IsNew3 = creditAppTableModel.Variable_IsNew3;
                model.QCreditAppReqAssignment3_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment3_AssignmentBuyer;
                model.QCreditAppReqAssignment3_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment3_RefAgreementId;
                model.QCreditAppReqAssignment3_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment3_BuyerAgreementAmount;
                model.QCreditAppReqAssignment3_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment3_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment3_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment3_AssignmentAgreementDate;
                model.QCreditAppReqAssignment3_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment3_RemainingAmount;
                model.Variable_IsNew4 = creditAppTableModel.Variable_IsNew4;
                model.QCreditAppReqAssignment4_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment4_AssignmentBuyer;
                model.QCreditAppReqAssignment4_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment4_RefAgreementId;
                model.QCreditAppReqAssignment4_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment4_BuyerAgreementAmount;
                model.QCreditAppReqAssignment4_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment4_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment4_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment4_AssignmentAgreementDate;
                model.QCreditAppReqAssignment4_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment4_RemainingAmount;
                model.Variable_IsNew5 = creditAppTableModel.Variable_IsNew5;
                model.QCreditAppReqAssignment5_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment5_AssignmentBuyer;
                model.QCreditAppReqAssignment5_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment5_RefAgreementId;
                model.QCreditAppReqAssignment5_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment5_BuyerAgreementAmount;
                model.QCreditAppReqAssignment5_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment5_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment5_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment5_AssignmentAgreementDate;
                model.QCreditAppReqAssignment5_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment5_RemainingAmount;
                model.Variable_IsNew6 = creditAppTableModel.Variable_IsNew6;
                model.QCreditAppReqAssignment6_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment6_AssignmentBuyer;
                model.QCreditAppReqAssignment6_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment6_RefAgreementId;
                model.QCreditAppReqAssignment6_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment6_BuyerAgreementAmount;
                model.QCreditAppReqAssignment6_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment6_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment6_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment6_AssignmentAgreementDate;
                model.QCreditAppReqAssignment6_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment6_RemainingAmount;
                model.Variable_IsNew7 = creditAppTableModel.Variable_IsNew7;
                model.QCreditAppReqAssignment7_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment7_AssignmentBuyer;
                model.QCreditAppReqAssignment7_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment7_RefAgreementId;
                model.QCreditAppReqAssignment7_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment7_BuyerAgreementAmount;
                model.QCreditAppReqAssignment7_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment7_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment7_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment7_AssignmentAgreementDate;
                model.QCreditAppReqAssignment7_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment7_RemainingAmount;
                model.Variable_IsNew8 = creditAppTableModel.Variable_IsNew8;
                model.QCreditAppReqAssignment8_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment8_AssignmentBuyer;
                model.QCreditAppReqAssignment8_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment8_RefAgreementId;
                model.QCreditAppReqAssignment8_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment8_BuyerAgreementAmount;
                model.QCreditAppReqAssignment8_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment8_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment8_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment8_AssignmentAgreementDate;
                model.QCreditAppReqAssignment8_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment8_RemainingAmount;
                model.Variable_IsNew9 = creditAppTableModel.Variable_IsNew9;
                model.QCreditAppReqAssignment9_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment9_AssignmentBuyer;
                model.QCreditAppReqAssignment9_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment9_RefAgreementId;
                model.QCreditAppReqAssignment9_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment9_BuyerAgreementAmount;
                model.QCreditAppReqAssignment9_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment9_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment9_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment9_AssignmentAgreementDate;
                model.QCreditAppReqAssignment9_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment9_RemainingAmount;
                model.Variable_IsNew10 = creditAppTableModel.Variable_IsNew10;
                model.QCreditAppReqAssignment10_AssignmentBuyer = creditAppTableModel.QCreditAppReqAssignment10_AssignmentBuyer;
                model.QCreditAppReqAssignment10_RefAgreementId = creditAppTableModel.QCreditAppReqAssignment10_RefAgreementId;
                model.QCreditAppReqAssignment10_BuyerAgreementAmount = creditAppTableModel.QCreditAppReqAssignment10_BuyerAgreementAmount;
                model.QCreditAppReqAssignment10_AssignmentAgreementAmount = creditAppTableModel.QCreditAppReqAssignment10_AssignmentAgreementAmount;
                model.QCreditAppReqAssignment10_AssignmentAgreementDate = creditAppTableModel.QCreditAppReqAssignment10_AssignmentAgreementDate;
                model.QCreditAppReqAssignment10_RemainingAmount = creditAppTableModel.QCreditAppReqAssignment10_RemainingAmount;
                model.QSumCreditAppReqAssignment_SumBuyerAgreementAmount = creditAppTableModel.QSumCreditAppReqAssignment_SumBuyerAgreementAmount;
                model.QSumCreditAppReqAssignment_SumAssignmentAgreementAmount = creditAppTableModel.QSumCreditAppReqAssignment_SumAssignmentAgreementAmount;
                model.QSumCreditAppReqAssignment_SumRemainingAmount = creditAppTableModel.QSumCreditAppReqAssignment_SumRemainingAmount;
                model.QCreditAppReqBusinessCollateral1_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral1_IsNew;
                model.QCreditAppReqBusinessCollateral1_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral1_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral1_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral1_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral1_Description = creditAppTableModel.QCreditAppReqBusinessCollateral1_Description;
                model.QCreditAppReqBusinessCollateral1_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral1_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral2_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral2_IsNew;
                model.QCreditAppReqBusinessCollateral2_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral2_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral2_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral2_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral2_Description = creditAppTableModel.QCreditAppReqBusinessCollateral2_Description;
                model.QCreditAppReqBusinessCollateral2_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral2_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral3_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral3_IsNew;
                model.QCreditAppReqBusinessCollateral3_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral3_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral3_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral3_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral3_Description = creditAppTableModel.QCreditAppReqBusinessCollateral3_Description;
                model.QCreditAppReqBusinessCollateral3_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral3_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral4_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral4_IsNew;
                model.QCreditAppReqBusinessCollateral4_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral4_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral4_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral4_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral4_Description = creditAppTableModel.QCreditAppReqBusinessCollateral4_Description;
                model.QCreditAppReqBusinessCollateral4_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral4_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral5_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral5_IsNew;
                model.QCreditAppReqBusinessCollateral5_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral5_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral5_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral5_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral5_Description = creditAppTableModel.QCreditAppReqBusinessCollateral5_Description;
                model.QCreditAppReqBusinessCollateral5_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral5_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral6_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral6_IsNew;
                model.QCreditAppReqBusinessCollateral6_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral6_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral6_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral6_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral6_Description = creditAppTableModel.QCreditAppReqBusinessCollateral6_Description;
                model.QCreditAppReqBusinessCollateral6_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral6_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral7_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral7_IsNew;
                model.QCreditAppReqBusinessCollateral7_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral7_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral7_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral7_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral7_Description = creditAppTableModel.QCreditAppReqBusinessCollateral7_Description;
                model.QCreditAppReqBusinessCollateral7_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral7_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral8_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral8_IsNew;
                model.QCreditAppReqBusinessCollateral8_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral8_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral8_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral8_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral8_Description = creditAppTableModel.QCreditAppReqBusinessCollateral8_Description;
                model.QCreditAppReqBusinessCollateral8_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral8_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral9_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral9_IsNew;
                model.QCreditAppReqBusinessCollateral9_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral9_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral9_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral9_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral9_Description = creditAppTableModel.QCreditAppReqBusinessCollateral9_Description;
                model.QCreditAppReqBusinessCollateral9_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral9_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral10_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral10_IsNew;
                model.QCreditAppReqBusinessCollateral10_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral10_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral10_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral10_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral10_Description = creditAppTableModel.QCreditAppReqBusinessCollateral10_Description;
                model.QCreditAppReqBusinessCollateral10_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral10_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral11_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral11_IsNew;
                model.QCreditAppReqBusinessCollateral11_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral11_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral11_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral11_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral11_Description = creditAppTableModel.QCreditAppReqBusinessCollateral11_Description;
                model.QCreditAppReqBusinessCollateral11_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral11_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral12_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral12_IsNew;
                model.QCreditAppReqBusinessCollateral12_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral12_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral12_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral12_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral12_Description = creditAppTableModel.QCreditAppReqBusinessCollateral12_Description;
                model.QCreditAppReqBusinessCollateral12_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral12_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral13_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral13_IsNew;
                model.QCreditAppReqBusinessCollateral13_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral13_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral13_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral13_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral13_Description = creditAppTableModel.QCreditAppReqBusinessCollateral13_Description;
                model.QCreditAppReqBusinessCollateral13_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral13_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral14_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral14_IsNew;
                model.QCreditAppReqBusinessCollateral14_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral14_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral14_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral14_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral14_Description = creditAppTableModel.QCreditAppReqBusinessCollateral14_Description;
                model.QCreditAppReqBusinessCollateral14_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral14_BusinessCollateralValue;
                model.QCreditAppReqBusinessCollateral15_IsNew = creditAppTableModel.QCreditAppReqBusinessCollateral15_IsNew;
                model.QCreditAppReqBusinessCollateral15_BusinessCollateralType = creditAppTableModel.QCreditAppReqBusinessCollateral15_BusinessCollateralType;
                model.QCreditAppReqBusinessCollateral15_BusinessCollateralSubType = creditAppTableModel.QCreditAppReqBusinessCollateral15_BusinessCollateralSubType;
                model.QCreditAppReqBusinessCollateral15_Description = creditAppTableModel.QCreditAppReqBusinessCollateral15_Description;
                model.QCreditAppReqBusinessCollateral15_BusinessCollateralValue = creditAppTableModel.QCreditAppReqBusinessCollateral15_BusinessCollateralValue;
                model.QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue = creditAppTableModel.QSumCreditAppReqBusinessCollateral_SumBusinessCollateralValue;
                model.Variable_CustCompanyRegistrationID = creditAppTableModel.Variable_CustCompanyRegistrationID;
                model.QCreditAppRequestTable_RegisteredAddress = creditAppTableModel.QCreditAppRequestTable_RegisteredAddress;
                model.Variable_CustComEstablishedDate = creditAppTableModel.Variable_CustComEstablishedDate;
                model.QCreditAppRequestTable_BusinessType = creditAppTableModel.QCreditAppRequestTable_BusinessType;
                model.QCreditAppRequestTable_IntroducedBy = creditAppTableModel.QCreditAppRequestTable_IntroducedBy;
                model.QCreditAppRequestTable_Instituetion = creditAppTableModel.QCreditAppRequestTable_Instituetion;
                model.QCreditAppRequestTable_BankBranch = creditAppTableModel.QCreditAppRequestTable_BankBranch;
                model.QCreditAppRequestTable_BankType = creditAppTableModel.QCreditAppRequestTable_BankType;
                model.QCreditAppRequestTable_BankAccountName = creditAppTableModel.QCreditAppRequestTable_BankAccountName;
                model.QAuthorizedPersonTrans1_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans1_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans1Age = creditAppTableModel.Variable_AuthorizedPersonTrans1Age;
                model.QAuthorizedPersonTrans2_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans2_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans2Age = creditAppTableModel.Variable_AuthorizedPersonTrans2Age;
                model.QAuthorizedPersonTrans3_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans3_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans3Age = creditAppTableModel.Variable_AuthorizedPersonTrans3Age;
                model.QAuthorizedPersonTrans4_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans4_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans4Age = creditAppTableModel.Variable_AuthorizedPersonTrans4Age;
                model.QAuthorizedPersonTrans5_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans5_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans5Age = creditAppTableModel.Variable_AuthorizedPersonTrans5Age;
                model.QAuthorizedPersonTrans6_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans6_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans6Age = creditAppTableModel.Variable_AuthorizedPersonTrans6Age;
                model.QAuthorizedPersonTrans7_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans7_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans7Age = creditAppTableModel.Variable_AuthorizedPersonTrans7Age;
                model.QAuthorizedPersonTrans8_AuthorizedPersonName = creditAppTableModel.QAuthorizedPersonTrans8_AuthorizedPersonName;
                model.Variable_AuthorizedPersonTrans8Age = creditAppTableModel.Variable_AuthorizedPersonTrans8Age;
                model.QOwnerTrans1_ShareHolderPersonName = creditAppTableModel.QOwnerTrans1_ShareHolderPersonName;
                model.QOwnerTrans1_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans1_PropotionOfShareholderPct;
                model.QOwnerTrans2_ShareHolderPersonName = creditAppTableModel.QOwnerTrans2_ShareHolderPersonName;
                model.QOwnerTrans2_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans2_PropotionOfShareholderPct;
                model.QOwnerTrans3_ShareHolderPersonName = creditAppTableModel.QOwnerTrans3_ShareHolderPersonName;
                model.QOwnerTrans3_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans3_PropotionOfShareholderPct;
                model.QOwnerTrans4_ShareHolderPersonName = creditAppTableModel.QOwnerTrans4_ShareHolderPersonName;
                model.QOwnerTrans4_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans4_PropotionOfShareholderPct;
                model.QOwnerTrans5_ShareHolderPersonName = creditAppTableModel.QOwnerTrans5_ShareHolderPersonName;
                model.QOwnerTrans5_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans5_PropotionOfShareholderPct;
                model.QOwnerTrans6_ShareHolderPersonName = creditAppTableModel.QOwnerTrans6_ShareHolderPersonName;
                model.QOwnerTrans6_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans6_PropotionOfShareholderPct;
                model.QOwnerTrans7_ShareHolderPersonName = creditAppTableModel.QOwnerTrans7_ShareHolderPersonName;
                model.QOwnerTrans7_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans7_PropotionOfShareholderPct;
                model.QOwnerTrans8_ShareHolderPersonName = creditAppTableModel.QOwnerTrans8_ShareHolderPersonName;
                model.QOwnerTrans8_PropotionOfShareholderPct = creditAppTableModel.QOwnerTrans8_PropotionOfShareholderPct;
                //----------------------------------------------------------------------------------------------------------
                model.QCCreditAppReqLineFin1_Year = creditAppTableModel.QCreditAppReqLineFin1_Year;
                model.QCCreditAppReqLineFin1_RegisteredCapital = creditAppTableModel.QCreditAppReqLineFin1_RegisteredCapital;
                model.QCCreditAppReqLineFin1_PaidCapital = creditAppTableModel.QCreditAppReqLineFin1_PaidCapital;
                model.QCCreditAppReqLineFin1_TotalAsset = creditAppTableModel.QCreditAppReqLineFin1_TotalAsset;
                model.QCCreditAppReqLineFin1_TotalLiability = creditAppTableModel.QCreditAppReqLineFin1_TotalLiability;
                model.QCCreditAppReqLineFin1_TotalEquity = creditAppTableModel.QCreditAppReqLineFin1_TotalEquity;
                model.QCCreditAppReqLineFin1_TotalRevenue = creditAppTableModel.QCreditAppReqLineFin1_TotalRevenue;
                model.QCCreditAppReqLineFin1_TotalCOGS = creditAppTableModel.QCreditAppReqLineFin1_TotalCOGS;
                model.QCCreditAppReqLineFin1_TotalGrossProfit = creditAppTableModel.QCreditAppReqLineFin1_TotalGrossProfit;
                model.QCCreditAppReqLineFin1_TotalOperExpFirst = creditAppTableModel.QCreditAppReqLineFin1_TotalOperExpFirst;
                model.QCCreditAppReqLineFin1_TotalNetProfitFirst = creditAppTableModel.QCreditAppReqLineFin1_TotalNetProfitFirst;
                model.QCCreditAppReqLineFin2_Year = creditAppTableModel.QCreditAppReqLineFin2_Year;
                model.QCCreditAppReqLineFin2_RegisteredCapital = creditAppTableModel.QCreditAppReqLineFin2_RegisteredCapital;
                model.QCCreditAppReqLineFin2_PaidCapital = creditAppTableModel.QCreditAppReqLineFin2_PaidCapital;
                model.QCCreditAppReqLineFin2_TotalAsset = creditAppTableModel.QCreditAppReqLineFin2_TotalAsset;
                model.QCCreditAppReqLineFin2_TotalLiability = creditAppTableModel.QCreditAppReqLineFin2_TotalLiability;
                model.QCCreditAppReqLineFin2_TotalEquity = creditAppTableModel.QCreditAppReqLineFin2_TotalEquity;
                model.QCCreditAppReqLineFin2_TotalRevenue = creditAppTableModel.QCreditAppReqLineFin2_TotalRevenue;
                model.QCCreditAppReqLineFin2_TotalCOGS = creditAppTableModel.QCreditAppReqLineFin2_TotalCOGS;
                model.QCCreditAppReqLineFin2_TotalGrossProfit = creditAppTableModel.QCreditAppReqLineFin2_TotalGrossProfit;
                model.QCCreditAppReqLineFin2_TotalOperExpSecond = creditAppTableModel.QCreditAppReqLineFin2_TotalOperExpSecond;
                model.QCCreditAppReqLineFin2_TotalNetProfitSecond = creditAppTableModel.QCreditAppReqLineFin2_TotalNetProfitSecond;
                model.QCCreditAppReqLineFin3_Year = creditAppTableModel.QCreditAppReqLineFin3_Year;
                model.QCCreditAppReqLineFin3_RegisteredCapital = creditAppTableModel.QCreditAppReqLineFin3_RegisteredCapital;
                model.QCCreditAppReqLineFin3_PaidCapital = creditAppTableModel.QCreditAppReqLineFin3_PaidCapital;
                model.QCCreditAppReqLineFin3_TotalAsset = creditAppTableModel.QCreditAppReqLineFin3_TotalAsset;
                model.QCCreditAppReqLineFin3_TotalLiability = creditAppTableModel.QCreditAppReqLineFin3_TotalLiability;
                model.QCCreditAppReqLineFin3_TotalEquity = creditAppTableModel.QCreditAppReqLineFin3_TotalEquity;
                model.QCCreditAppReqLineFin3_TotalRevenue = creditAppTableModel.QCreditAppReqLineFin3_TotalRevenue;
                model.QCCreditAppReqLineFin3_TotalCOGS = creditAppTableModel.QCreditAppReqLineFin3_TotalCOGS;
                model.QCCreditAppReqLineFin3_TotalGrossProfit = creditAppTableModel.QCreditAppReqLineFin3_TotalGrossProfit;
                model.QCCreditAppReqLineFin3_TotalOperExpThird = creditAppTableModel.QCreditAppReqLineFin3_TotalOperExpThird;
                model.QCCreditAppReqLineFin3_TotalNetProfitThird = creditAppTableModel.QCreditAppReqLineFin3_TotalNetProfitThird;
                //------------------------------------------------------------------------------
                model.QCreditAppReqLineFin1_Year = creditAppRequestModel.QCreditAppReqLineFin1_Year;
                model.QCreditAppReqLineFin1_RegisteredCapital = creditAppRequestModel.QCreditAppReqLineFin1_RegisteredCapital;
                model.QCreditAppReqLineFin1_PaidCapital = creditAppRequestModel.QCreditAppReqLineFin1_PaidCapital;
                model.QCreditAppReqLineFin1_TotalAsset = creditAppRequestModel.QCreditAppReqLineFin1_TotalAsset;
                model.QCreditAppReqLineFin1_TotalLiability = creditAppRequestModel.QCreditAppReqLineFin1_TotalLiability;
                model.QCreditAppReqLineFin1_TotalEquity = creditAppRequestModel.QCreditAppReqLineFin1_TotalEquity;
                model.QCreditAppReqLineFin1_TotalRevenue = creditAppRequestModel.QCreditAppReqLineFin1_TotalRevenue;
                model.QCreditAppReqLineFin1_TotalCOGS = creditAppRequestModel.QCreditAppReqLineFin1_TotalCOGS;
                model.QCreditAppReqLineFin1_TotalGrossProfit = creditAppRequestModel.QCreditAppReqLineFin1_TotalGrossProfit;
                model.QCreditAppReqLineFin1_TotalOperExpFirst = creditAppRequestModel.QCreditAppReqLineFin1_TotalOperExpFirst;
                model.QCreditAppReqLineFin1_TotalNetProfitFirst = creditAppRequestModel.QCreditAppReqLineFin1_TotalNetProfitFirst;
                model.QCreditAppReqLineFin2_Year = creditAppRequestModel.QCreditAppReqLineFin2_Year;
                model.QCreditAppReqLineFin2_RegisteredCapital = creditAppRequestModel.QCreditAppReqLineFin2_RegisteredCapital;
                model.QCreditAppReqLineFin2_PaidCapital = creditAppRequestModel.QCreditAppReqLineFin2_PaidCapital;
                model.QCreditAppReqLineFin2_TotalAsset = creditAppRequestModel.QCreditAppReqLineFin2_TotalAsset;
                model.QCreditAppReqLineFin2_TotalLiability = creditAppRequestModel.QCreditAppReqLineFin2_TotalLiability;
                model.QCreditAppReqLineFin2_TotalEquity = creditAppRequestModel.QCreditAppReqLineFin2_TotalEquity;
                model.QCreditAppReqLineFin2_TotalRevenue = creditAppRequestModel.QCreditAppReqLineFin2_TotalRevenue;
                model.QCreditAppReqLineFin2_TotalCOGS = creditAppRequestModel.QCreditAppReqLineFin2_TotalCOGS;
                model.QCreditAppReqLineFin2_TotalGrossProfit = creditAppRequestModel.QCreditAppReqLineFin2_TotalGrossProfit;
                model.QCreditAppReqLineFin2_TotalOperExpFirst = creditAppRequestModel.QCreditAppReqLineFin2_TotalOperExpFirst;
                model.QCreditAppReqLineFin2_TotalNetProfitFirst = creditAppRequestModel.QCreditAppReqLineFin2_TotalNetProfitFirst;
                model.QCreditAppReqLineFin3_Year = creditAppRequestModel.QCreditAppReqLineFin3_Year;
                model.QCreditAppReqLineFin3_RegisteredCapital = creditAppRequestModel.QCreditAppReqLineFin3_RegisteredCapital;
                model.QCreditAppReqLineFin3_PaidCapital = creditAppRequestModel.QCreditAppReqLineFin3_PaidCapital;
                model.QCreditAppReqLineFin3_TotalAsset = creditAppRequestModel.QCreditAppReqLineFin3_TotalAsset;
                model.QCreditAppReqLineFin3_TotalLiability = creditAppRequestModel.QCreditAppReqLineFin3_TotalLiability;
                model.QCreditAppReqLineFin3_TotalEquity = creditAppRequestModel.QCreditAppReqLineFin3_TotalEquity;
                model.QCreditAppReqLineFin3_TotalRevenue = creditAppRequestModel.QCreditAppReqLineFin3_TotalRevenue;
                model.QCreditAppReqLineFin3_TotalCOGS = creditAppRequestModel.QCreditAppReqLineFin3_TotalCOGS;
                model.QCreditAppReqLineFin3_TotalGrossProfit = creditAppRequestModel.QCreditAppReqLineFin3_TotalGrossProfit;
                model.QCreditAppReqLineFin3_TotalOperExpFirst = creditAppRequestModel.QCreditAppReqLineFin3_TotalOperExpFirst;
                model.QCreditAppReqLineFin3_TotalNetProfitFirst = creditAppRequestModel.QCreditAppReqLineFin3_TotalNetProfitFirst;
                //------------------------------------------------------------------------------
                model.QCreditAppReqLineFin1_NetProfitPercent = creditAppRequestModel.QCreditAppReqLineFin1_NetProfitPercent;
                model.QCreditAppReqLineFin1_lDE = creditAppRequestModel.QCreditAppReqLineFin1_lDE;
                model.QCreditAppReqLineFin1_QuickRatio = creditAppRequestModel.QCreditAppReqLineFin1_QuickRatio;
                model.QCreditAppRequestTable_SalesAvgPerMonth = creditAppRequestModel.QCreditAppRequestTableCAPF_SalesAvgPerMonth;

                model.QCCreditAppReqLineFin1_NetProfitPercent = creditAppTableModel.QCreditAppReqLineFin1_NetProfitPercent;
                model.QCCreditAppReqLineFin1_lDE = creditAppTableModel.QCreditAppReqLineFin1_lDE;
                model.QCCreditAppReqLineFin1_QuickRatio = creditAppTableModel.QCreditAppReqLineFin1_QuickRatio;
                model.QCCreditAppReqLineFin1_IntCoverageRatio = creditAppTableModel.QCreditAppReqLineFin1_IntCoverageRatio;

                //------------------------------------------------------------------------------
                model.QTaxReportStart_TaxMonth = creditAppTableModel.QTaxReportStart_TaxMonth;
                model.QTaxReportEnd_TaxMonth = creditAppTableModel.QTaxReportEnd_TaxMonth;
                model.QTaxReportTrans1_TaxMonth = creditAppTableModel.QTaxReportTrans1_TaxMonth;
                model.QTaxReportTrans1_Amount = creditAppTableModel.QTaxReportTrans1_Amount;
                model.QTaxReportTrans2_TaxMonth = creditAppTableModel.QTaxReportTrans2_TaxMonth;
                model.QTaxReportTrans2_Amount = creditAppTableModel.QTaxReportTrans2_Amount;
                model.QTaxReportTrans3_TaxMonth = creditAppTableModel.QTaxReportTrans3_TaxMonth;
                model.QTaxReportTrans3_Amount = creditAppTableModel.QTaxReportTrans3_Amount;
                model.QTaxReportTrans4_TaxMonth = creditAppTableModel.QTaxReportTrans4_TaxMonth;
                model.QTaxReportTrans4_Amount = creditAppTableModel.QTaxReportTrans4_Amount;
                model.QTaxReportTrans5_TaxMonth = creditAppTableModel.QTaxReportTrans5_TaxMonth;
                model.QTaxReportTrans5_Amount = creditAppTableModel.QTaxReportTrans5_Amount;
                model.QTaxReportTrans6_TaxMonth = creditAppTableModel.QTaxReportTrans6_TaxMonth;
                model.QTaxReportTrans6_Amount = creditAppTableModel.QTaxReportTrans6_Amount;
                model.QTaxReportTrans7_TaxMonth = creditAppTableModel.QTaxReportTrans7_TaxMonth;
                model.QTaxReportTrans7_Amount = creditAppTableModel.QTaxReportTrans7_Amount;
                model.QTaxReportTrans8_TaxMonth = creditAppTableModel.QTaxReportTrans8_TaxMonth;
                model.QTaxReportTrans8_Amount = creditAppTableModel.QTaxReportTrans8_Amount;
                model.QTaxReportTrans9_TaxMonth = creditAppTableModel.QTaxReportTrans9_TaxMonth;
                model.QTaxReportTrans9_Amount = creditAppTableModel.QTaxReportTrans9_Amount;
                model.QTaxReportTrans10_TaxMonth = creditAppTableModel.QTaxReportTrans10_TaxMonth;
                model.QTaxReportTrans10_Amount = creditAppTableModel.QTaxReportTrans10_Amount;
                model.QTaxReportTrans11_TaxMonth = creditAppTableModel.QTaxReportTrans11_TaxMonth;
                model.QTaxReportTrans11_Amount = creditAppTableModel.QTaxReportTrans11_Amount;
                model.QTaxReportTrans12_TaxMonth = creditAppTableModel.QTaxReportTrans12_TaxMonth;
                model.QTaxReportTrans12_Amount = creditAppTableModel.QTaxReportTrans12_Amount;
                model.QTaxReportTransSum_SumTaxReport = creditAppTableModel.QTaxReportTransSum_SumTaxReport;
                model.Variable_AverageOutputVATPerMonth = creditAppTableModel.Variable_AverageOutputVATPerMonth;
                model.QCreditAppRequestTable_AsOfDate = creditAppTableModel.QCreditAppRequestTable_AsOfDate;
                model.Variable_CrditLimitAmtFirst1 = creditAppTableModel.Variable_CrditLimitAmtFirst1;
                model.Variable_AROutstandingAmtFirst = creditAppTableModel.Variable_AROutstandingAmtFirst;
                model.Variable_CreditLimitRemainingFirst1 = creditAppTableModel.Variable_CreditLimitRemainingFirst1;
                model.Variable_ReserveOutstandingFirst1 = creditAppTableModel.Variable_ReserveOutstandingFirst1;
                model.Variable_RetentionOutstandingFirst1 = creditAppTableModel.Variable_RetentionOutstandingFirst1;
                model.Variable_CrditLimitAmtSecond1 = creditAppTableModel.Variable_CrditLimitAmtSecond1;
                model.Variable_AROutstandingAmtSecond = creditAppTableModel.Variable_AROutstandingAmtSecond;
                model.Variable_CreditLimitRemainingSecond1 = creditAppTableModel.Variable_CreditLimitRemainingSecond1;
                model.Variable_ReserveOutstandingSecond1 = creditAppTableModel.Variable_ReserveOutstandingSecond1;
                model.Variable_RetentionOutstandingSecond1 = creditAppTableModel.Variable_RetentionOutstandingSecond1;
                model.Variable_CrditLimitAmtThird1 = creditAppTableModel.Variable_CrditLimitAmtThird1;
                model.Variable_AROutstandingAmtThird = creditAppTableModel.Variable_AROutstandingAmtThird;
                model.Variable_CreditLimitRemainingThird1 = creditAppTableModel.Variable_CreditLimitRemainingThird1;
                model.Variable_ReserveOutstandingThird1 = creditAppTableModel.Variable_ReserveOutstandingThird1;
                model.Variable_RetentionOutstandingThird1 = creditAppTableModel.Variable_RetentionOutstandingThird1;
                model.Variable_CrditLimitAmtFourth1 = creditAppTableModel.Variable_CrditLimitAmtFourth1;
                model.Variable_AROutstandingAmtFourth = creditAppTableModel.Variable_AROutstandingAmtFourth;
                model.Variable_CreditLimitRemainingFourth1 = creditAppTableModel.Variable_CreditLimitRemainingFourth1;
                model.Variable_ReserveOutstandingFourth1 = creditAppTableModel.Variable_ReserveOutstandingFourth1;
                model.Variable_RetentionOutstandingFourth1 = creditAppTableModel.Variable_RetentionOutstandingFourth1;
                model.Variable_CrditLimitAmtFifth1 = creditAppTableModel.Variable_CrditLimitAmtFifth1;
                model.Variable_AROutstandingAmtFifth = creditAppTableModel.Variable_AROutstandingAmtFifth;
                model.Variable_CreditLimitRemainingFifth1 = creditAppTableModel.Variable_CreditLimitRemainingFifth1;
                model.Variable_ReserveOutstandingFifth1 = creditAppTableModel.Variable_ReserveOutstandingFifth1;
                model.Variable_RetentionOutstandingFifth1 = creditAppTableModel.Variable_RetentionOutstandingFifth1;
                model.Variable_CrditLimitAmtSixth1 = creditAppTableModel.Variable_CrditLimitAmtSixth1;
                model.Variable_AROutstandingAmtSixth = creditAppTableModel.Variable_AROutstandingAmtSixth;
                model.Variable_CreditLimitRemainingSixth1 = creditAppTableModel.Variable_CreditLimitRemainingSixth1;
                model.Variable_ReserveOutstandingSixth1 = creditAppTableModel.Variable_ReserveOutstandingSixth1;
                model.Variable_RetentionOutstandingSixth1 = creditAppTableModel.Variable_RetentionOutstandingSixth1;
                model.Variable_CrditLimitAmtSeventh1 = creditAppTableModel.Variable_CrditLimitAmtSeventh1;
                model.Variable_AROutstandingAmtSeventh = creditAppTableModel.Variable_AROutstandingAmtSeventh;
                model.Variable_CreditLimitRemainingSeventh1 = creditAppTableModel.Variable_CreditLimitRemainingSeventh1;
                model.Variable_ReserveOutstandingSeventh1 = creditAppTableModel.Variable_ReserveOutstandingSeventh1;
                model.Variable_RetentionOutstandingSeventh1 = creditAppTableModel.Variable_RetentionOutstandingSeventh1;
                model.Variable_CrditLimitAmtEighth1 = creditAppTableModel.Variable_CrditLimitAmtEighth1;
                model.Variable_AROutstandingAmtEighth = creditAppTableModel.Variable_AROutstandingAmtEighth;
                model.Variable_CreditLimitRemainingEighth1 = creditAppTableModel.Variable_CreditLimitRemainingEighth1;
                model.Variable_ReserveOutstandingEighth1 = creditAppTableModel.Variable_ReserveOutstandingEighth1;
                model.Variable_RetentionOutstandingEighth1 = creditAppTableModel.Variable_RetentionOutstandingEighth1;
                model.Variable_CrditLimitAmtNineth1 = creditAppTableModel.Variable_CrditLimitAmtNineth1;
                model.Variable_AROutstandingAmtNineth = creditAppTableModel.Variable_AROutstandingAmtNineth;
                model.Variable_CreditLimitRemainingNineth1 = creditAppTableModel.Variable_CreditLimitRemainingNineth1;
                model.Variable_ReserveOutstandingNineth1 = creditAppTableModel.Variable_ReserveOutstandingNineth1;
                model.Variable_RetentionOutstandingNineth1 = creditAppTableModel.Variable_RetentionOutstandingNineth1;
                model.Variable_CrditLimitAmtTenth1 = creditAppTableModel.Variable_CrditLimitAmtTenth1;
                model.Variable_AROutstandingAmtTenth = creditAppTableModel.Variable_AROutstandingAmtTenth;
                model.Variable_CreditLimitRemainingTenth1 = creditAppTableModel.Variable_CreditLimitRemainingTenth1;
                model.Variable_ReserveOutstandingTenth1 = creditAppTableModel.Variable_ReserveOutstandingTenth1;
                model.Variable_RetentionOutstandingTenth1 = creditAppTableModel.Variable_RetentionOutstandingTenth1;
                model.QCAReqCreditOutStandingSum_SumApprovedCreditLimit = creditAppTableModel.QCAReqCreditOutStandingSum_SumApprovedCreditLimit;
                model.QCAReqCreditOutStandingSum_SumARBalance = creditAppTableModel.QCAReqCreditOutStandingSum_SumARBalance;
                model.QCAReqCreditOutStandingSum_SumCreditLimitBalance = creditAppTableModel.QCAReqCreditOutStandingSum_SumCreditLimitBalance;
                model.QCAReqCreditOutStandingSum_SumReserveToBeRefund = creditAppTableModel.QCAReqCreditOutStandingSum_SumReserveToBeRefund;
                model.QCAReqCreditOutStandingSum_SumAccumRetentionAmount = creditAppTableModel.QCAReqCreditOutStandingSum_SumAccumRetentionAmount;
                model.QCreditAppRequestTable_NCBCheckedDate = creditAppTableModel.QCreditAppRequestTable_NCBCheckedDate;
                model.QNCBTrans1_Institution = creditAppTableModel.QNCBTrans1_Institution;
                model.QNCBTrans1_LoanType = creditAppTableModel.QNCBTrans1_LoanType;
                model.QNCBTrans1_LoanLimit = creditAppTableModel.QNCBTrans1_LoanLimit;
                model.QNCBTrans2_Institution = creditAppTableModel.QNCBTrans2_Institution;
                model.QNCBTrans2_LoanType = creditAppTableModel.QNCBTrans2_LoanType;
                model.QNCBTrans2_LoanLimit = creditAppTableModel.QNCBTrans2_LoanLimit;
                model.QNCBTrans3_Institution = creditAppTableModel.QNCBTrans3_Institution;
                model.QNCBTrans3_LoanType = creditAppTableModel.QNCBTrans3_LoanType;
                model.QNCBTrans3_LoanLimit = creditAppTableModel.QNCBTrans3_LoanLimit;
                model.QNCBTrans4_Institution = creditAppTableModel.QNCBTrans4_Institution;
                model.QNCBTrans4_LoanType = creditAppTableModel.QNCBTrans4_LoanType;
                model.QNCBTrans4_LoanLimit = creditAppTableModel.QNCBTrans4_LoanLimit;
                model.QNCBTrans5_Institution = creditAppTableModel.QNCBTrans5_Institution;
                model.QNCBTrans5_LoanType = creditAppTableModel.QNCBTrans5_LoanType;
                model.QNCBTrans5_LoanLimit = creditAppTableModel.QNCBTrans5_LoanLimit;
                model.QNCBTrans6_Institution = creditAppTableModel.QNCBTrans6_Institution;
                model.QNCBTrans6_LoanType = creditAppTableModel.QNCBTrans6_LoanType;
                model.QNCBTrans6_LoanLimit = creditAppTableModel.QNCBTrans6_LoanLimit;
                model.QNCBTrans7_Institution = creditAppTableModel.QNCBTrans7_Institution;
                model.QNCBTrans7_LoanType = creditAppTableModel.QNCBTrans7_LoanType;
                model.QNCBTrans7_LoanLimit = creditAppTableModel.QNCBTrans7_LoanLimit;
                model.QNCBTrans8_Institution = creditAppTableModel.QNCBTrans8_Institution;
                model.QNCBTrans8_LoanType = creditAppTableModel.QNCBTrans8_LoanType;
                model.QNCBTrans8_LoanLimit = creditAppTableModel.QNCBTrans8_LoanLimit;
                model.QNCBTransSum_TotalLoanLimit = creditAppTableModel.QNCBTransSum_TotalLoanLimit;
                model.QCreditAppRequestTable_MarketingComment = creditAppTableModel.QCreditAppRequestTable_MarketingComment;
                model.QCreditAppRequestTable_ApproverComment = creditAppTableModel.QCreditAppRequestTable_ApproverComment;
                model.QCreditAppRequestTable_CreditComment = creditAppTableModel.QCreditAppRequestTable_CreditComment;
                model.QActionHistory1_ApproverName = creditAppTableModel.QActionHistory1_ApproverName;
                model.QActionHistory1_Comment = creditAppTableModel.QActionHistory1_Comment;
                model.QActionHistory1_CreatedDateTime = creditAppTableModel.QActionHistory1_CreatedDateTime;
                model.QActionHistory2_ApproverName = creditAppTableModel.QActionHistory2_ApproverName;
                model.QActionHistory2_Comment = creditAppTableModel.QActionHistory2_Comment;
                model.QActionHistory2_CreatedDateTime = creditAppTableModel.QActionHistory2_CreatedDateTime;
                model.QActionHistory3_ApproverName = creditAppTableModel.QActionHistory3_ApproverName;
                model.QActionHistory3_Comment = creditAppTableModel.QActionHistory3_Comment;
                model.QActionHistory3_CreatedDateTime = creditAppTableModel.QActionHistory3_CreatedDateTime;
                model.QActionHistory4_ApproverName = creditAppTableModel.QActionHistory4_ApproverName;
                model.QActionHistory4_Comment = creditAppTableModel.QActionHistory4_Comment;
                model.QActionHistory4_CreatedDateTime = creditAppTableModel.QActionHistory4_CreatedDateTime;
                model.QActionHistory5_ApproverName = creditAppTableModel.QActionHistory5_ApproverName;
                model.QActionHistory5_Comment = creditAppTableModel.QActionHistory5_Comment;
                model.QActionHistory5_CreatedDateTime = creditAppTableModel.QActionHistory5_CreatedDateTime;
                //-----------------------------------------------------------------------------------------------------------------------------------------------
                model.QCreditAppRequestTableCAPF_NumberOfDays = creditAppRequestModel.QCreditAppRequestTableCAPF_NumberOfDays;
                model.QCreditAppRequestTableCAPF_CreditTermId = creditAppRequestModel.QCreditAppRequestTableCAPF_CreditTermId;
                model.QCreditAppRequestTableCAPF_CreditAppRequestId = creditAppRequestModel.QCreditAppRequestTableCAPF_CreditAppRequestId;
                model.QCreditAppRequestTableCAPF_SalesAvgPerMonth = creditAppRequestModel.QCreditAppRequestTableCAPF_SalesAvgPerMonth;
                model.QCreditAppRequestTableCAPF_CreditLimitRequest = creditAppRequestModel.QCreditAppRequestTableCAPF_CreditLimitRequest;
                model.QCreditAppRequestLineCAPF_CreditAppRequestLineGUID = creditAppRequestModel.QCreditAppRequestLineCAPF_CreditAppRequestLineGUID;
                model.QCreditAppRequestLineCAPF_BuyerTableGUID = creditAppRequestModel.QCreditAppRequestLineCAPF_BuyerTableGUID;
                model.QCreditAppRequestLineCAPF_InvoiceAddressGUID = creditAppRequestModel.QCreditAppRequestLineCAPF_InvoiceAddressGUID;
                model.QCreditAppRequestLineCAPF_CreditLimitLineRequest = creditAppRequestModel.QCreditAppRequestLineCAPF_CreditLimitLineRequest;
                model.QCreditAppRequestLineCAPF_InvoiceAddress = creditAppRequestModel.QCreditAppRequestLineCAPF_InvoiceAddress;
                model.QCreditAppRequestLineCAPF_CreditScoring = creditAppRequestModel.QCreditAppRequestLineCAPF_CreditScoring;
                model.QCreditAppRequestLineCAPF_BlacklistStatus = creditAppRequestModel.QCreditAppRequestLineCAPF_BlacklistStatus;
                model.QBuyerTableCAPF_BuyerName = creditAppRequestModel.QBuyerTableCAPF_BuyerName;
                model.QBuyerTableCAPF_BuyerId = creditAppRequestModel.QBuyerTableCAPF_BuyerId;
                model.QBuyerTableCAPF_TaxId = creditAppRequestModel.QBuyerTableCAPF_TaxId;
                model.QCreditAppRequestLineCAPF_DateOfEstablish = creditAppRequestModel.QCreditAppRequestLineCAPF_DateOfEstablish;
                model.QBuyerTableCAPF_LineOfBusinessDesc = creditAppRequestModel.QBuyerTableCAPF_LineOfBusinessDesc;
                model.QBuyerAgreementCAPF_BuyerAgreementTableGUID = creditAppRequestModel.QBuyerAgreementCAPF_BuyerAgreementTableGUID;
                model.QBuyerAgreementCAPF_BuyerAgreementId = creditAppRequestModel.QBuyerAgreementCAPF_BuyerAgreementId;
                model.QBuyerAgreementCAPF_BuyerAgreement = creditAppRequestModel.QBuyerAgreementCAPF_BuyerAgreement;
                model.QBuyerAgreementCAPF_StartDate = creditAppRequestModel.QBuyerAgreementCAPF_StartDate;
                model.QBuyerAgreementCAPF_EndDate = creditAppRequestModel.QBuyerAgreementCAPF_EndDate;
                model.QBuyerAgreementCAPF_Remark = creditAppRequestModel.QBuyerAgreementCAPF_Remark;
                model.QBuyerAgreementCAPF_Penalty = creditAppRequestModel.QBuyerAgreementCAPF_Penalty;
                model.QBuyerAgreementLineCAPF_Period_1 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_1;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_1 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_1;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_1 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_1;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_1 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_1;
                model.QBuyerAgreementLineCAPF_Period_2 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_2;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_2 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_2;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_2 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_2;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_2 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_2;
                model.QBuyerAgreementLineCAPF_Period_3 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_3;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_3 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_3;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_3 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_3;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_3 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_3;
                model.QBuyerAgreementLineCAPF_Period_4 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_4;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_4 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_4;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_4 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_4;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_4 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_4;
                model.QBuyerAgreementLineCAPF_Period_5 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_5;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_5 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_5;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_5 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_5;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_5 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_5;
                model.QBuyerAgreementLineCAPF_Period_6 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_6;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_6 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_6;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_6 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_6;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_6 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_6;
                model.QBuyerAgreementLineCAPF_Period_7 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_7;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_7 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_7;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_7 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_7;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_7 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_7;
                model.QBuyerAgreementLineCAPF_Period_8 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_8;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_8 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_8;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_8 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_8;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_8 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_8;
                model.QBuyerAgreementLineCAPF_Period_9 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_9;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_9 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_9;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_9 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_9;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_9 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_9;
                model.QBuyerAgreementLineCAPF_Period_10 = creditAppRequestModel.QBuyerAgreementLineCAPF_Period_10;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_10 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDesc_10;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_10 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineAmount_10;
                model.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_10 = creditAppRequestModel.QBuyerAgreementLineCAPF_BuyerAgreementLineDueDate_10;
                //-----------------------------------------------------------------------------------------------------------------------
                model.QSum10BuyerAgreementLine_SumBuyerAgreementAmount = creditAppRequestModel.QSum10BuyerAgreementLine_SumBuyerAgreementAmount;
                model.QCreditAppRequestLine_BusinessSegmentDesc = creditAppRequestModel.QCreditAppRequestLine_BusinessSegmentDesc;
				model.QCreditAppRequestLine_BuyerName = creditAppRequestModel.QCreditAppRequestLine_BuyerName;
                model.QCreditAppRequestLine_InvoiceAddress = creditAppRequestModel.QCreditAppRequestLine_InvoiceAddress;
                model.QCreditAppRequestLine_LineOfBusinessDesc = creditAppRequestModel.QCreditAppRequestLine_LineOfBusinessDesc;
				model.QCreditAppRequestLine_BlacklistStatusDesc = creditAppRequestModel.QCreditAppRequestLine_BlacklistStatusDesc;
				model.QCreditAppRequestLine_TaxId = creditAppRequestModel.QCreditAppRequestLine_TaxId;
				model.QCreditAppRequestLine_DateOfEstablish = creditAppRequestModel.QCreditAppRequestLine_DateOfEstablish;
				model.QCreditAppRequestLine_CreditScoringDesc = creditAppRequestModel.QCreditAppRequestLine_CreditScoringDesc;
				//-----------------------------------------------------------------
				model.QCreditAppReqLineFin3_TotalOperExpFirst = creditAppRequestModel.QCreditAppReqLineFin3_TotalOperExpFirst;
				model.QCreditAppReqLineFin3_TotalNetProfitFirst = creditAppRequestModel.QCreditAppReqLineFin3_TotalNetProfitFirst;
                model.QSumCreditAppRequestLine_SumCreditLimitLineRequest = creditAppRequestModel.QSumCreditAppRequestLine_SumCreditLimitLineRequest;

                model.QCreditAppReqLineFin1_NetProfitPercent = creditAppRequestModel.QCreditAppReqLineFin1_NetProfitPercent;
                model.QCreditAppReqLineFin1_lDE = creditAppRequestModel.QCreditAppReqLineFin1_lDE;
                model.QCreditAppReqLineFin1_QuickRatio = creditAppRequestModel.QCreditAppReqLineFin1_QuickRatio;
                model.QCreditAppReqLineFin1_IntCoverageRatio = creditAppRequestModel.QCreditAppReqLineFin1_IntCoverageRatio;
                model.QCreditAppReqLineFin2_TotalOperExpFirst = creditAppRequestModel.QCreditAppReqLineFin2_TotalOperExpFirst;
                model.QCreditAppReqLineFin2_TotalNetProfitFirst = creditAppRequestModel.QCreditAppReqLineFin2_TotalNetProfitFirst;
                model.QSumServiceFeeConditionTrans_ServiceFeeAmt = creditAppTableModel.QSumServiceFeeConditionTrans_ServiceFeeAmt;
                model.QCAReqCreditOutStanding1_ApprovedCreditLimit = creditAppTableModel.QCAReqCreditOutStanding1_ApprovedCreditLimit;
                model.QCAReqCreditOutStanding1_ARBalance = creditAppTableModel.QCAReqCreditOutStanding1_ARBalance;
                model.QCAReqCreditOutStanding1_CreditLimitBalance = creditAppTableModel.QCAReqCreditOutStanding1_CreditLimitBalance;
                model.QCAReqCreditOutStanding1_ReserveToBeRefund = creditAppTableModel.QCAReqCreditOutStanding1_ReserveToBeRefund;
                model.QCAReqCreditOutStanding1_AccumRetentionAmount = creditAppTableModel.QCAReqCreditOutStanding1_AccumRetentionAmount;

                model.QCreditAppRequestTable_PurchaseWithdrawalCondition = creditAppTableModel.QCreditAppRequestTable_PurchaseWithdrawalCondition;
                model.QCreditAppRequestTable_AssignmentMethod = creditAppTableModel.QCreditAppRequestTable_AssignmentMethod;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
    }
}

