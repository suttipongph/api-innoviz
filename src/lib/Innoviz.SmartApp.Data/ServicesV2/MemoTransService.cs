using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IMemoTransService
	{

		MemoTransItemView GetMemoTransById(string id);
		MemoTransItemView CreateMemoTrans(MemoTransItemView memoTransView);
		MemoTransItemView UpdateMemoTrans(MemoTransItemView memoTransView);
		bool DeleteMemoTrans(string id);
		MemoTransItemView GetMemoTransInitialData(string refId, string refGUID, RefType refType);
	}
	public class MemoTransService : SmartAppService, IMemoTransService
	{
		public MemoTransService(SmartAppDbContext context) : base(context) { }
		public MemoTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public MemoTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MemoTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public MemoTransService() : base() { }

		public MemoTransItemView GetMemoTransById(string id)
		{
			try
			{
				IMemoTransRepo memoTransRepo = new MemoTransRepo(db);
				return memoTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MemoTransItemView CreateMemoTrans(MemoTransItemView memoTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				memoTransView = accessLevelService.AssignOwnerBU(memoTransView);
				IMemoTransRepo memoTransRepo = new MemoTransRepo(db);
				MemoTrans memoTrans = memoTransView.ToMemoTrans();
				memoTrans = memoTransRepo.CreateMemoTrans(memoTrans);
				base.LogTransactionCreate<MemoTrans>(memoTrans);
				UnitOfWork.Commit();
				return memoTrans.ToMemoTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MemoTransItemView UpdateMemoTrans(MemoTransItemView memoTransView)
		{
			try
			{
				IMemoTransRepo memoTransRepo = new MemoTransRepo(db);
				MemoTrans inputMemoTrans = memoTransView.ToMemoTrans();
				MemoTrans dbMemoTrans = memoTransRepo.Find(inputMemoTrans.MemoTransGUID);
				dbMemoTrans = memoTransRepo.UpdateMemoTrans(dbMemoTrans, inputMemoTrans);
				base.LogTransactionUpdate<MemoTrans>(GetOriginalValues<MemoTrans>(dbMemoTrans), dbMemoTrans);
				UnitOfWork.Commit();
				return dbMemoTrans.ToMemoTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteMemoTrans(string item)
		{
			try
			{
				IMemoTransRepo memoTransRepo = new MemoTransRepo(db);
				Guid memoTransGUID = new Guid(item);
				MemoTrans memoTrans = memoTransRepo.Find(memoTransGUID);
				memoTransRepo.Remove(memoTrans);
				base.LogTransactionDelete<MemoTrans>(memoTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MemoTransItemView GetMemoTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new MemoTransItemView
				{
					MemoTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
