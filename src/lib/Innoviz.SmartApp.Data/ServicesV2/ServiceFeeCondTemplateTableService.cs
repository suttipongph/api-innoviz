
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IServiceFeeCondTemplateTableService
	{

		ServiceFeeCondTemplateLineItemView GetServiceFeeCondTemplateLineById(string id);
		ServiceFeeCondTemplateLineItemView CreateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineView);
		ServiceFeeCondTemplateLineItemView UpdateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineView);
		bool DeleteServiceFeeCondTemplateLine(string id);
		ServiceFeeCondTemplateTableItemView GetServiceFeeCondTemplateTableById(string id);
		ServiceFeeCondTemplateTableItemView CreateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableView);
		ServiceFeeCondTemplateTableItemView UpdateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableView);
		bool DeleteServiceFeeCondTemplateTable(string id);
		IEnumerable<SelectItem<ServiceFeeCondTemplateTableItemView>> GetDropDownItemServiceFeeCondTemplateTableByProductType(SearchParameter search);
		public ServiceFeeCondTemplateLineItemView GetServiceFeeCondTemplateLineInitialData(string serviceFeecondtemplatetableGUID);
		ServiceFeeCondTemplateLineItemView GetInvoiceRevenueTypeInitialData(string invoiceRevenueTypeGUID);
		bool IsServiceFeeCondTemplateLine(string serviceFeeCondTemplateTableGUID);
	}
	public class ServiceFeeCondTemplateTableService : SmartAppService, IServiceFeeCondTemplateTableService
	{
		public ServiceFeeCondTemplateTableService(SmartAppDbContext context) : base(context) { }
		public ServiceFeeCondTemplateTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ServiceFeeCondTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ServiceFeeCondTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ServiceFeeCondTemplateTableService() : base() { }
		#region ServiceFeeCondTemplateLine
		public ServiceFeeCondTemplateLineItemView GetServiceFeeCondTemplateLineById(string id)
		{
			try
			{
				IServiceFeeCondTemplateLineRepo serviceFeeCondTemplateLineRepo = new ServiceFeeCondTemplateLineRepo(db);
				return serviceFeeCondTemplateLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateLineItemView CreateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				serviceFeeCondTemplateLineView = accessLevelService.AssignOwnerBU(serviceFeeCondTemplateLineView);
				IServiceFeeCondTemplateLineRepo serviceFeeCondTemplateLineRepo = new ServiceFeeCondTemplateLineRepo(db);
				ServiceFeeCondTemplateLine serviceFeeCondTemplateLine = serviceFeeCondTemplateLineView.ToServiceFeeCondTemplateLine();
				serviceFeeCondTemplateLine = serviceFeeCondTemplateLineRepo.CreateServiceFeeCondTemplateLine(serviceFeeCondTemplateLine);
				base.LogTransactionCreate<ServiceFeeCondTemplateLine>(serviceFeeCondTemplateLine);
				UnitOfWork.Commit();
				return serviceFeeCondTemplateLine.ToServiceFeeCondTemplateLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateLineItemView UpdateServiceFeeCondTemplateLine(ServiceFeeCondTemplateLineItemView serviceFeeCondTemplateLineView)
		{
			try
			{
				IServiceFeeCondTemplateLineRepo serviceFeeCondTemplateLineRepo = new ServiceFeeCondTemplateLineRepo(db);
				ServiceFeeCondTemplateLine inputServiceFeeCondTemplateLine = serviceFeeCondTemplateLineView.ToServiceFeeCondTemplateLine();
				ServiceFeeCondTemplateLine dbServiceFeeCondTemplateLine = serviceFeeCondTemplateLineRepo.Find(inputServiceFeeCondTemplateLine.ServiceFeeCondTemplateLineGUID);
				dbServiceFeeCondTemplateLine = serviceFeeCondTemplateLineRepo.UpdateServiceFeeCondTemplateLine(dbServiceFeeCondTemplateLine, inputServiceFeeCondTemplateLine);
				base.LogTransactionUpdate<ServiceFeeCondTemplateLine>(GetOriginalValues<ServiceFeeCondTemplateLine>(dbServiceFeeCondTemplateLine), dbServiceFeeCondTemplateLine);
				UnitOfWork.Commit();
				return dbServiceFeeCondTemplateLine.ToServiceFeeCondTemplateLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteServiceFeeCondTemplateLine(string item)
		{
			try
			{
				IServiceFeeCondTemplateLineRepo serviceFeeCondTemplateLineRepo = new ServiceFeeCondTemplateLineRepo(db);
				Guid serviceFeeCondTemplateLineGUID = new Guid(item);
				ServiceFeeCondTemplateLine serviceFeeCondTemplateLine = serviceFeeCondTemplateLineRepo.Find(serviceFeeCondTemplateLineGUID);
				serviceFeeCondTemplateLineRepo.Remove(serviceFeeCondTemplateLine);
				base.LogTransactionDelete<ServiceFeeCondTemplateLine>(serviceFeeCondTemplateLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateLineItemView GetServiceFeeCondTemplateLineInitialData(string serviceFeecondtemplatetableGUID)
		{
			try
			{
				IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
				ServiceFeeCondTemplateTable serviceFeecondtemplatetable = serviceFeeCondTemplateTableRepo.GetServiceFeeCondTemplateTableByIdNoTrackingByAccessLevel(serviceFeecondtemplatetableGUID.StringToGuid());
				

				return new ServiceFeeCondTemplateLineItemView
				{
					ServiceFeeCondTemplateLineGUID = new Guid().GuidNullToString(),
					ServiceFeeCondTemplateTableGUID = serviceFeecondtemplatetable.ServiceFeeCondTemplateTableGUID.GuidNullToString(),
					serviceFeeCondTemplateTable_Value = SmartAppUtil.GetDropDownLabel(serviceFeecondtemplatetable.ServiceFeeCondTemplateId, serviceFeecondtemplatetable.Description),
					ProductType = serviceFeecondtemplatetable.ProductType,

				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateLineItemView GetInvoiceRevenueTypeInitialData(string invoiceRevenueTypeGUID)
		{
			try
			{
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByIdNoTrackingByAccessLevel(invoiceRevenueTypeGUID.StringToGuid());

				return new ServiceFeeCondTemplateLineItemView
				{
					ServiceFeeCondTemplateLineGUID = new Guid().GuidNullToString(),
					InvoiceRevenueTypeGUID = invoiceRevenueType.InvoiceRevenueTypeGUID.GuidNullToString(),
					AmountBeforeTax = invoiceRevenueType.FeeAmount,
					Description = invoiceRevenueType.Description,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		#endregion
		#region ServiceFeeCondTemplateTable
		public ServiceFeeCondTemplateTableItemView GetServiceFeeCondTemplateTableById(string id)
		{
			try
			{
				IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
				return serviceFeeCondTemplateTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateTableItemView CreateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				serviceFeeCondTemplateTableView = accessLevelService.AssignOwnerBU(serviceFeeCondTemplateTableView);
				IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
				ServiceFeeCondTemplateTable serviceFeeCondTemplateTable = serviceFeeCondTemplateTableView.ToServiceFeeCondTemplateTable();
				serviceFeeCondTemplateTable = serviceFeeCondTemplateTableRepo.CreateServiceFeeCondTemplateTable(serviceFeeCondTemplateTable);
				base.LogTransactionCreate<ServiceFeeCondTemplateTable>(serviceFeeCondTemplateTable);
				UnitOfWork.Commit();
				return serviceFeeCondTemplateTable.ToServiceFeeCondTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeCondTemplateTableItemView UpdateServiceFeeCondTemplateTable(ServiceFeeCondTemplateTableItemView serviceFeeCondTemplateTableView)
		{
			try
			{
				IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
				ServiceFeeCondTemplateTable inputServiceFeeCondTemplateTable = serviceFeeCondTemplateTableView.ToServiceFeeCondTemplateTable();
				ServiceFeeCondTemplateTable dbServiceFeeCondTemplateTable = serviceFeeCondTemplateTableRepo.Find(inputServiceFeeCondTemplateTable.ServiceFeeCondTemplateTableGUID);
				dbServiceFeeCondTemplateTable = serviceFeeCondTemplateTableRepo.UpdateServiceFeeCondTemplateTable(dbServiceFeeCondTemplateTable, inputServiceFeeCondTemplateTable);
				base.LogTransactionUpdate<ServiceFeeCondTemplateTable>(GetOriginalValues<ServiceFeeCondTemplateTable>(dbServiceFeeCondTemplateTable), dbServiceFeeCondTemplateTable);
				UnitOfWork.Commit();
				return dbServiceFeeCondTemplateTable.ToServiceFeeCondTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteServiceFeeCondTemplateTable(string item)
		{
			try
			{
				IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
				Guid serviceFeeCondTemplateTableGUID = new Guid(item);
				ServiceFeeCondTemplateTable serviceFeeCondTemplateTable = serviceFeeCondTemplateTableRepo.Find(serviceFeeCondTemplateTableGUID);
				serviceFeeCondTemplateTableRepo.Remove(serviceFeeCondTemplateTable);
				base.LogTransactionDelete<ServiceFeeCondTemplateTable>(serviceFeeCondTemplateTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsServiceFeeCondTemplateLine(string serviceFeeCondTemplateTableGUID)
		{
			try
			{
				IServiceFeeCondTemplateLineRepo serviceFeeCondTemplateLineRepo = new ServiceFeeCondTemplateLineRepo(db);

				List<ServiceFeeCondTemplateLine> serviceFeeCondTemplateLines = serviceFeeCondTemplateLineRepo.GetServiceFeeCondTemplateLineByServiceFeeCondTemplateTable(serviceFeeCondTemplateTableGUID.StringToGuid());
				return ConditionService.IsNotZero(serviceFeeCondTemplateLines.Count());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region dropdown
		public IEnumerable<SelectItem<ServiceFeeCondTemplateTableItemView>> GetDropDownItemServiceFeeCondTemplateTableByProductType(SearchParameter search)
		{
			try
			{
				IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
				search = search.GetParentCondition(ServiceFeeCondTemplateTableCondition.ProductType);
				return serviceFeeCondTemplateTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
