using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICompanyParameterService
	{

		CompanyParameterItemView GetCompanyParameterById(string id);
		CompanyParameterItemView GetCompanyParameterByCompany(string id);
		CompanyParameterItemView CreateCompanyParameter(CompanyParameterItemView companyParameterView);
		CompanyParameterItemView UpdateCompanyParameter(CompanyParameterItemView companyParameterView);
		bool DeleteCompanyParameter(string id);
		bool GetDataCompaneParameterValidation(CompanyParameterItemView vmodel);
	}

	public class CompanyParameterService : SmartAppService, ICompanyParameterService
	{
		public CompanyParameterService(SmartAppDbContext context) : base(context) { }
		public CompanyParameterService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CompanyParameterService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CompanyParameterService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CompanyParameterService() : base() { }

		public CompanyParameterItemView GetCompanyParameterById(string id)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				return companyParameterRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyParameterItemView GetCompanyParameterByCompany(string id)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				SearchParameter searchParameter = new SearchParameter();
				return companyParameterRepo.GetByCompanyIdvw(searchParameter);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyParameterItemView CreateCompanyParameter(CompanyParameterItemView companyParameterView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				companyParameterView = accessLevelService.AssignOwnerBU(companyParameterView);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterView.ToCompanyParameter();
				companyParameter = companyParameterRepo.CreateCompanyParameter(companyParameter);
				base.LogTransactionCreate<CompanyParameter>(companyParameter);
				UnitOfWork.Commit();
				return companyParameter.ToCompanyParameterItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyParameterItemView UpdateCompanyParameter(CompanyParameterItemView companyParameterView)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter inputCompanyParameter = companyParameterView.ToCompanyParameter();
				CompanyParameter dbCompanyParameter = companyParameterRepo.Find(inputCompanyParameter.CompanyParameterGUID);
				dbCompanyParameter = companyParameterRepo.UpdateCompanyParameter(dbCompanyParameter, inputCompanyParameter);
				base.LogTransactionUpdate<CompanyParameter>(GetOriginalValues<CompanyParameter>(dbCompanyParameter), dbCompanyParameter);
				UnitOfWork.Commit();
				return dbCompanyParameter.ToCompanyParameterItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCompanyParameter(string item)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				Guid companyParameterGUID = new Guid(item);
				CompanyParameter companyParameter = companyParameterRepo.Find(companyParameterGUID);
				companyParameterRepo.Remove(companyParameter);
				base.LogTransactionDelete<CompanyParameter>(companyParameter);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public bool GetDataCompaneParameterValidation(CompanyParameterItemView vmodel)
        {
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				MethodOfPaymentItemView suspenseMethodOfPayment = new MethodOfPaymentItemView();
				InvoiceTypeItemView bondRetentionInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView ftInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView ftFeeInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView ftUnearnedInterestInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView ftRollbillUnearnedInterestInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView ftInterestInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView ftRetentionInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView ftReserveToBeRefundedInvType = new InvoiceTypeItemView(); 
				InvoiceTypeItemView ftInterestRefundInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView lcRetentionInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView pfInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView pfUnearnedInterestInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView pfInterestInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView pfInterestRefundInvType = new InvoiceTypeItemView();
				InvoiceTypeItemView pfRetentionInvType = new InvoiceTypeItemView();
				MethodOfPaymentItemView settlementMethodOfPayment = new MethodOfPaymentItemView();

				if (vmodel.SuspenseMethodOfPaymentGUID != null)
				{
					suspenseMethodOfPayment = methodOfPaymentRepo.GetByIdvw(vmodel.SuspenseMethodOfPaymentGUID.StringToGuid());
					if (suspenseMethodOfPayment.PaymentType != (int)PaymentType.SuspenseAccount)
					{
						ex.AddData("ERROR.90164", new string[] { "LABEL.PAYMENT_TYPE", "LABEL.SUSPENSE_METHOD_OF_PAYMENT_ID", "ENUM.SUSPENSE_ACCOUNT" });
					}
				}
				if (vmodel.BondRetentionInvTypeGUID != null)
				{
					bondRetentionInvType = invoiceTypeRepo.GetByIdvw(vmodel.BondRetentionInvTypeGUID.StringToGuid());
					if (bondRetentionInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.BOND_RETENTION_INV_TYPE_ID" });
					}
				}
				if (vmodel.FTInvTypeGUID != null)
				{
					ftInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTInvTypeGUID.StringToGuid());
					if (ftInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_INV_TYPE_ID" });
					}
				}
				if (vmodel.FTFeeInvTypeGUID != null)
				{
					ftFeeInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTFeeInvTypeGUID.StringToGuid());
					if (ftFeeInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_FEE_INV_TYPE_ID" });
					}
				}
				if (vmodel.FTUnearnedInterestInvTypeGUID != null)
				{
					ftUnearnedInterestInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTUnearnedInterestInvTypeGUID.StringToGuid());
					if (ftUnearnedInterestInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_UNEARNED_INTEREST_INV_TYPE_ID" });
					}
				}
				if (vmodel.FTRollbillUnearnedInterestInvTypeGUID != null)
				{
					 ftRollbillUnearnedInterestInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTRollbillUnearnedInterestInvTypeGUID.StringToGuid());
					if (ftRollbillUnearnedInterestInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_UNEARNED_INTEREST_INVOICE_TYPE_ID" });
					}
				}
				if (vmodel.FTInterestInvTypeGUID != null)
				{
					 ftInterestInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTInterestInvTypeGUID.StringToGuid());
					if (ftInterestInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_INTEREST_INV_TYPE_ID" });
					}
				}
				if (vmodel.FTRetentionInvTypeGUID != null)
				{
					 ftRetentionInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTRetentionInvTypeGUID.StringToGuid());
					if (ftRetentionInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_RETENTION_INV_TYPE_ID" });
					}
				}
				if (vmodel.FTReserveToBeRefundedInvTypeGUID != null)
				{
					 ftReserveToBeRefundedInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTReserveToBeRefundedInvTypeGUID.StringToGuid());
					if (ftReserveToBeRefundedInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_RESERVE_TO_BE_REFUNDED_INV_TYPE_ID" });
					}
				}
				if (vmodel.FTInterestRefundInvTypeGUID != null)
				{
					 ftInterestRefundInvType = invoiceTypeRepo.GetByIdvw(vmodel.FTInterestRefundInvTypeGUID.StringToGuid());
					if (ftInterestRefundInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.FT_INTEREST_REFUND_INV_TYPE_ID" });
					}
				}
				if (vmodel.LCRetentionInvTypeGUID != null)
				{
					 lcRetentionInvType = invoiceTypeRepo.GetByIdvw(vmodel.LCRetentionInvTypeGUID.StringToGuid());
					if (lcRetentionInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.LC_RETENTION_INV_TYPE_ID" });
					}
				}
				if (vmodel.PFInvTypeGUID != null)
				{
					 pfInvType = invoiceTypeRepo.GetByIdvw(vmodel.PFInvTypeGUID.StringToGuid());
					if (pfInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.PF_WITHDRAWAL_INVOICE_TYPE_ID" });
					}
				}
				if (vmodel.PFUnearnedInterestInvTypeGUID != null)
				{
					 pfUnearnedInterestInvType = invoiceTypeRepo.GetByIdvw(vmodel.PFUnearnedInterestInvTypeGUID.StringToGuid());
					if (pfUnearnedInterestInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.PF_UNEARNED_INTEREST_INVOICE_TYPE_ID" });
					}
				}

				if (vmodel.PFInterestInvTypeGUID!=null)
				{
					 pfInterestInvType = invoiceTypeRepo.GetByIdvw(vmodel.PFInterestInvTypeGUID.StringToGuid());
					if (pfInterestInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.PF_INTEREST_INVOICE_TYPE_ID" });
					}
				}
				if (vmodel.PFInterestRefundInvTypeGUID != null)
				{
					 pfInterestRefundInvType = invoiceTypeRepo.GetByIdvw(vmodel.PFInterestRefundInvTypeGUID.StringToGuid());
					if (pfInterestRefundInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.PF_INTEREST_REFUND_INVOICE_TYPE_ID" });
					}
				}
				if (vmodel.PFRetentionInvTypeGUID != null)
				{
					 pfRetentionInvType = invoiceTypeRepo.GetByIdvw(vmodel.PFRetentionInvTypeGUID.StringToGuid());
					if (pfRetentionInvType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTO_GEN_INV_REVENUE_TYPE_ID", "LABEL.PF_RETENTION_INVOICE_TYPE_ID" });
					}
				}
				if (vmodel.DaysPerYear <= 0)
				{
					ex.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.DAYS_PER_YEAR" });
					
				}
				if(vmodel.PFInterestCutDay == 0)
                {
					ex.AddData("ERROR.90057", new string[] { "LABEL.PF_INTEREST_CUT_DAY" });
				}
				if (vmodel.SettlementMethodOfPaymentGUID != null)
				{
					settlementMethodOfPayment = methodOfPaymentRepo.GetByIdvw(vmodel.SettlementMethodOfPaymentGUID.StringToGuid());
					if (settlementMethodOfPayment.PaymentType != (int)PaymentType.Ledger)
					{
						ex.AddData("ERROR.90164", new string[] { "LABEL.PAYMENT_TYPE", "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "ENUM.LEDGER" });
					}
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
    }
}
