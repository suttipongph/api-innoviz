using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IGradeClassificationService
	{

		GradeClassificationItemView GetGradeClassificationById(string id);
		GradeClassificationItemView CreateGradeClassification(GradeClassificationItemView gradeClassificationView);
		GradeClassificationItemView UpdateGradeClassification(GradeClassificationItemView gradeClassificationView);
		bool DeleteGradeClassification(string id);
	}
	public class GradeClassificationService : SmartAppService, IGradeClassificationService
	{
		public GradeClassificationService(SmartAppDbContext context) : base(context) { }
		public GradeClassificationService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public GradeClassificationService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public GradeClassificationService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public GradeClassificationService() : base() { }

		public GradeClassificationItemView GetGradeClassificationById(string id)
		{
			try
			{
				IGradeClassificationRepo gradeClassificationRepo = new GradeClassificationRepo(db);
				return gradeClassificationRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GradeClassificationItemView CreateGradeClassification(GradeClassificationItemView gradeClassificationView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				gradeClassificationView = accessLevelService.AssignOwnerBU(gradeClassificationView);
				IGradeClassificationRepo gradeClassificationRepo = new GradeClassificationRepo(db);
				GradeClassification gradeClassification = gradeClassificationView.ToGradeClassification();
				gradeClassification = gradeClassificationRepo.CreateGradeClassification(gradeClassification);
				base.LogTransactionCreate<GradeClassification>(gradeClassification);
				UnitOfWork.Commit();
				return gradeClassification.ToGradeClassificationItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GradeClassificationItemView UpdateGradeClassification(GradeClassificationItemView gradeClassificationView)
		{
			try
			{
				IGradeClassificationRepo gradeClassificationRepo = new GradeClassificationRepo(db);
				GradeClassification inputGradeClassification = gradeClassificationView.ToGradeClassification();
				GradeClassification dbGradeClassification = gradeClassificationRepo.Find(inputGradeClassification.GradeClassificationGUID);
				dbGradeClassification = gradeClassificationRepo.UpdateGradeClassification(dbGradeClassification, inputGradeClassification);
				base.LogTransactionUpdate<GradeClassification>(GetOriginalValues<GradeClassification>(dbGradeClassification), dbGradeClassification);
				UnitOfWork.Commit();
				return dbGradeClassification.ToGradeClassificationItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteGradeClassification(string item)
		{
			try
			{
				IGradeClassificationRepo gradeClassificationRepo = new GradeClassificationRepo(db);
				Guid gradeClassificationGUID = new Guid(item);
				GradeClassification gradeClassification = gradeClassificationRepo.Find(gradeClassificationGUID);
				gradeClassificationRepo.Remove(gradeClassification);
				base.LogTransactionDelete<GradeClassification>(gradeClassification);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
