using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ICustomerTableService
    {

        CustomerTableItemView GetCustomerTableById(string id);
        CustomerTableItemView CreateCustomerTable(CustomerTableItemView customerTableView);
        CustomerTableItemView UpdateCustomerTable(CustomerTableItemView customerTableView);
        GuarantorTransItemView GetGuarantorTransInitialData(string refGUID);
        bool DeleteCustomerTable(string id);
        bool IsManualByCustGroup(string custGroupGUID);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemCustomerStatus(SearchParameter search);
        CustomerTableItemView IsPassportIdDuplicate(string customerId, string passPortId);
        CustomerTableItemView IsTaxIdDuplicate(string customerId, string taxId);
        JointVentureTransItemView GetJointVentureTransInitialData(string refGUID);
        CustVisitingTransItemView GetCustVisitingTransInitialData(string refGUID);
        OwnerTransItemView GetOwnerTransInitialData(string refGUID);
        AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID);
        ContactPersonTransItemView GetContactPersonTransInitialData(string refGUID);
        ContactTransItemView GetContactTransInitialData(string refGUID);
        MemoTransItemView GetMemoTransInitialData(string refGUID);
        ProjectReferenceTransItemView GetProjectReferenceTransInitialData(string refGUID);
        AddressTransItemView GetAddressTransInitialData(string refGUID);
        FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refGUID);
        TaxReportTransItemView GetTaxReportTransInitialData(string refGUID);
        CustomerTableItemView GetCustomerTableInitialData(string companyGUID);
        int GetAge(string dateOfBirth);
        SearchResult<CreditOutstandingViewMap> GetCreditOutstandingByCustomer(Guid customerTableGUID, DateTime asOfDate);
        void CreateCustomerTableCollection(List<CustomerTableItemView> customerTableViewCollection);
        #region function
        UpdateCustomerTableStatusResultView GetUpdateCustomerTableStatusById(string id);
        UpdateCustomerTableStatusResultView UpdateCustomerTableStatus(UpdateCustomerTableStatusParamView functionView);
        UpdateCustomerTableBlacklistStatusResultView GetUpdateCustomerTableBlacklistStatusById(string id);
        UpdateCustomerTableBlacklistStatusResultView UpdateCustomerTableBlacklistStatus(UpdateCustomerTableBlacklistStatusParamView functionView);
        #endregion
        #region Dropdown
        IEnumerable<SelectItem<MessengerJobTableItemView>> GetDropDownItemMessengerJobTableByCustomer(SearchParameter search);

        #endregion
        BookmarkDocumentCustomerAmendCreditLimitView GetBookmarkDocumentCustomerAmendCreditLimitView(Guid RefGUID, Guid BookmarkDocumentTransGUID);

    }
    public class CustomerTableService : SmartAppService, ICustomerTableService
    {
        public CustomerTableService(SmartAppDbContext context) : base(context) { }
        public CustomerTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public CustomerTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CustomerTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public CustomerTableService() : base() { }

        public CustomerTableItemView GetCustomerTableById(string id)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                return customerTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustomerTableItemView CreateCustomerTable(CustomerTableItemView customerTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                //customerTableView = accessLevelService.AssignOwnerBU(customerTableView);
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                EmployeeTableItemView employeeTable = employeeTableRepo.GetByIdIgnoreAccessLevelvw(customerTableView.ResponsibleByGUID.StringToGuid());
                if(ConditionService.IsNullOrEmpty(employeeTable.SysuserTable_UserName))
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.90175", new string[] { "LABEL.RESPONSIBLE_BY" });
                    throw ex;
                }
                customerTableView.Owner = employeeTable.SysuserTable_UserName;
                customerTableView.OwnerBusinessUnitGUID = employeeTable.BusinessUnitGUID.GuidNullToString();

                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable customerTable = customerTableView.ToCustomerTable();
                GenNumberSeqCode(customerTable);
                customerTable = customerTableRepo.CreateCustomerTable(customerTable);
                base.LogTransactionCreate<CustomerTable>(customerTable);
                UnitOfWork.Commit();
                return customerTable.ToCustomerTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustomerTableItemView UpdateCustomerTable(CustomerTableItemView customerTableView)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable inputCustomerTable = customerTableView.ToCustomerTable();
                CustomerTable dbCustomerTable = customerTableRepo.Find(inputCustomerTable.CustomerTableGUID);
                dbCustomerTable = customerTableRepo.UpdateCustomerTable(dbCustomerTable, inputCustomerTable);
                base.LogTransactionUpdate<CustomerTable>(GetOriginalValues<CustomerTable>(dbCustomerTable), dbCustomerTable);
                UnitOfWork.Commit();
                return dbCustomerTable.ToCustomerTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCustomerTable(string item)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                Guid customerTableGUID = new Guid(item);
                CustomerTable customerTable = customerTableRepo.Find(customerTableGUID);
                customerTableRepo.Remove(customerTable);
                base.LogTransactionDelete<CustomerTable>(customerTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByCustGroup(string custGroupGUID)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                ICustGroupRepo custGroup = new CustGroupRepo(db);
                return numberSequenceService.IsManualByNumberSeqTableGUID(custGroup.GetNumberSeqTableByCustGroupGUID(custGroupGUID.StringToGuid()));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenNumberSeqCode(CustomerTable customerTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                ICustGroupRepo custGroup = new CustGroupRepo(db);
                bool isManual = IsManualByCustGroup(customerTable.CustGroupGUID.GuidNullToString());
                if (!isManual)
                {
                    customerTable.CustomerId = numberSequenceService.GetNumber(customerTable.CompanyGUID, null, custGroup.GetNumberSeqTableByCustGroupGUID(customerTable.CustGroupGUID));
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemCustomerStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Customer);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustomerTableItemView IsPassportIdDuplicate(string customerId, string passPortId)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                return customerTableRepo.IsPassportIdDuplicate(customerId, passPortId);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustomerTableItemView IsTaxIdDuplicate(string customerId, string taxId)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                return customerTableRepo.IsTaxIdDuplicate(customerId, taxId);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView GetGuarantorTransInitialData(string refGUID)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return guarantorTransService.GetGuarantorTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JointVentureTransItemView GetJointVentureTransInitialData(string refGUID)
        {
            try
            {
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return jointVentureTransService.GetJointVentureTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustVisitingTransItemView GetCustVisitingTransInitialData(string refGUID)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return custVisitingTransService.GetCustVisitingTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public OwnerTransItemView GetOwnerTransInitialData(string refGUID)
        {
            try
            {
                IOwnerTransService ownerTransService = new OwnerTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return ownerTransService.GetOwnerTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return authorizedPersonTransService.GetAuthorizedPersonTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ContactPersonTransItemView GetContactPersonTransInitialData(string refGUID)
        {
            try
            {
                IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return contactPersonTransService.GetContactPersonTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ContactTransItemView GetContactTransInitialData(string refGUID)
        {
            try
            {
                IContactTransService ContactTransService = new ContactTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return ContactTransService.GetContactTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProjectReferenceTransItemView GetProjectReferenceTransInitialData(string refGUID)
        {
            try
            {
                IProjectReferenceTransService projectReferenceTransService = new ProjectReferenceTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return projectReferenceTransService.GetProjectReferenceTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AddressTransItemView GetAddressTransInitialData(string refGUID)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return addressTransService.GetAddressTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refGUID)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return financialStatementTransService.GetFinancialStatementTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxReportTransItemView GetTaxReportTransInitialData(string refGUID)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                string refId = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerId;
                return taxReportTransService.GetTaxReportTransInitialData(refId, refGUID, Models.Enum.RefType.Customer);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustomerTableItemView GetCustomerTableInitialData(string companyGUID)
        {
            try
            {
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                EmployeeTableItemView responsible = employeeTableService.GetEmployeeTableByUserIdAndCompany(db.GetUserId(), companyGUID);
                var leadStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)CustomerStatus.Lead).ToString());
                return new CustomerTableItemView
                {
                    CustomerTableGUID = new Guid().GuidNullToString(),
                    RecordType = (int)RecordType.Person,
                    IdentificationType = (int)IdentificationType.TaxId,
                    ResponsibleByGUID = (responsible != null) ? responsible.EmployeeTableGUID : null,
                    DocumentStatusGUID = (leadStatus != null) ? leadStatus.DocumentStatusGUID.GuidNullToString() : null
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public int GetAge(string dateOfBirth)
        {
            try
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                if(dateOfBirth != null)
                {
                    return relatedPersonTableService.CalcAge(dateOfBirth.StringNullToDateNull());
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchResult<CreditOutstandingViewMap> GetCreditOutstandingByCustomer(Guid customerTableGUID, DateTime asOfDate)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                SearchParameter search = new SearchParameter();
                var temp1 = creditAppTableRepo.GetCreditOutstandingByCustomer(customerTableGUID, asOfDate);
                var result = new SearchResult<CreditOutstandingViewMap>();
                result = temp1.SetSearchResult<CreditOutstandingViewMap>(temp1.Count(), search);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public void CreateCustomerTableCollection(List<CustomerTableItemView> customerTableViewCollection)
        {
            try
            {
                if(customerTableViewCollection.Any())
                {
                    ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                    List<CustomerTable> customerTables = customerTableViewCollection.ToCustomerTable().ToList();
                    customerTableRepo.ValidateAdd(customerTables);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(customerTables, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        #region function
        public UpdateCustomerTableStatusResultView GetUpdateCustomerTableStatusById(string id)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                UpdateCustomerTableStatusResultView customerTableItemView = customerTableRepo.GetCustomerStatusById(id.StringToGuid());                
                return customerTableItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public UpdateCustomerTableStatusResultView UpdateCustomerTableStatus(UpdateCustomerTableStatusParamView functionView)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                NotificationResponse success = new NotificationResponse();
                CustomerTable inputCustomerTable = customerTableRepo.GetCustomerTableByIdNoTracking(functionView.CustomerTableGUID.StringToGuid());
                inputCustomerTable.DocumentStatusGUID = functionView.DocumentStatusGUID.StringToGuid();
                CustomerTable dbCustomerTable = customerTableRepo.Find(inputCustomerTable.CustomerTableGUID);
                dbCustomerTable = customerTableRepo.UpdateCustomerTable(dbCustomerTable, inputCustomerTable);
                base.LogTransactionUpdate<CustomerTable>(GetOriginalValues<CustomerTable>(dbCustomerTable), dbCustomerTable);
                UnitOfWork.Commit();
                UpdateCustomerTableStatusResultView updateCustomerResultView = new UpdateCustomerTableStatusResultView();
                success.AddData("SUCCESS.00075", new string[] { "LABEL.CUSTOMER_STATUS" });
                updateCustomerResultView.Notification = success;
                return updateCustomerResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public UpdateCustomerTableBlacklistStatusResultView GetUpdateCustomerTableBlacklistStatusById(string id)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                UpdateCustomerTableBlacklistStatusResultView customerTableItemView = customerTableRepo.GetCustomerBlacklistStatusById(id.StringToGuid());
                return customerTableItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public UpdateCustomerTableBlacklistStatusResultView UpdateCustomerTableBlacklistStatus(UpdateCustomerTableBlacklistStatusParamView functionView)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                NotificationResponse success = new NotificationResponse();
                CustomerTable inputCustomerTable = customerTableRepo.GetCustomerTableByIdNoTracking(functionView.CustomerTableGUID.StringToGuid());
                inputCustomerTable.BlacklistStatusGUID = functionView.BlacklistStatusGUID.StringToGuid();
                CustomerTable dbCustomerTable = customerTableRepo.Find(inputCustomerTable.CustomerTableGUID);
                dbCustomerTable = customerTableRepo.UpdateCustomerTable(dbCustomerTable, inputCustomerTable);
                base.LogTransactionUpdate<CustomerTable>(GetOriginalValues<CustomerTable>(dbCustomerTable), dbCustomerTable);
                UnitOfWork.Commit();
                UpdateCustomerTableBlacklistStatusResultView updateCustomerResultView = new UpdateCustomerTableBlacklistStatusResultView();
                success.AddData("SUCCESS.00075", new string[] { "LABEL.CUSTOMER_BLACKLIST_STATUS" });
                updateCustomerResultView.Notification = success;
                return updateCustomerResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Dropdown
        public IEnumerable<SelectItem<MessengerJobTableItemView>> GetDropDownItemMessengerJobTableByCustomer(SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search.GetParentCondition(MessengerJobCondition.CustomertableGUID);
                SearchCondition searchConditions = SearchConditionService.GetMessengerJobTableItemByResultCondition((int)Result.Failed);
                search.Conditions.Add(searchConditions);
                return sysDropDownService.GetDropDownItemMessengerJobTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BookmarkDocumentCustomerAmendCreditLimitView GetBookmarkDocumentCustomerAmendCreditLimitView(Guid RefGUID, Guid BookmarkDocumentTransGUID)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                return customerTableRepo.GetBookmarkDocumentCustomerAmendCreditLimitView(RefGUID, BookmarkDocumentTransGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
    }
}
