﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAttachmentService
	{

		Task<AttachmentItemView> GetAttachmentById(string id);
		AttachmentItemView CreateAttachment(AttachmentItemView attachmentView);
		AttachmentItemView UpdateAttachment(AttachmentItemView attachmentView);
		bool DeleteAttachment(string id);
		IEnumerable<SelectItem<AttachmentItemView>> GetAttachmentByDocumentType(SearchParameter search);
		string GetRefIdByRefTypeRefGUID(RefIdParm parm);
		Task<AttachmentItemView> GetAttachmentByFileName(AttachmentK2Parm parm);
	}
	public class AttachmentService : SmartAppService, IAttachmentService
	{
		public AttachmentService(SmartAppDbContext context) : base(context) { }
		public AttachmentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AttachmentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AttachmentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AttachmentService() : base() { }

		public async Task<AttachmentItemView> GetAttachmentById(string id)
		{
			try
			{
				IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
				return await attachmentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AttachmentItemView CreateAttachment(AttachmentItemView attachmentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				attachmentView = accessLevelService.AssignOwnerBU(attachmentView);
				IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
				Attachment attachment = attachmentView.ToAttachment();
				attachment = attachmentRepo.CreateAttachment(attachment);
				base.LogTransactionCreate<Attachment>(attachment);
				UnitOfWork.Commit();
				return attachment.ToAttachmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AttachmentItemView UpdateAttachment(AttachmentItemView attachmentView)
		{
			try
			{
				IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
				Attachment inputAttachment = attachmentView.ToAttachment();
				Attachment dbAttachment = attachmentRepo.Find(inputAttachment.AttachmentGUID);
				dbAttachment = attachmentRepo.UpdateAttachment(dbAttachment, inputAttachment);
				base.LogTransactionUpdate<Attachment>(GetOriginalValues<Attachment>(dbAttachment), dbAttachment);
				UnitOfWork.Commit();
				return dbAttachment.ToAttachmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAttachment(string item)
		{
			try
			{
				IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
				Guid attachmentGUID = new Guid(item);
				Attachment attachment = attachmentRepo.Find(attachmentGUID);
				attachmentRepo.Remove(attachment);
				base.LogTransactionDelete<Attachment>(attachment);
				UnitOfWork.Commit();
				attachment.EnsureFileDeleted();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AttachmentItemView>> GetAttachmentByDocumentType(SearchParameter search)
		{
			try
			{
				IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
				search = search.GetParentCondition(DocumentTemplateTypeCondition.DocumentTemplateType);
				return attachmentRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region GetRefId
        public string GetRefIdByRefTypeRefGUID(RefIdParm parm)
		{
			try
			{
				RefType refType = (RefType)parm.RefType;
				switch (refType)
				{
					case RefType.Buyer:
						IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
						BuyerTable buyerTable = buyerTableRepo.GetBuyerTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return buyerTable.BuyerId;

					case RefType.Customer:
						ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
						CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return customerTable.CustomerId;

					case RefType.CreditAppRequestTable:
						ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
						CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return creditAppRequestTable.CreditAppRequestId;

					case RefType.CreditAppRequestLine:
						ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
						CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTracking(parm.RefGUID.StringToGuid());
						return creditAppRequestLine.LineNum.ToString();

					case RefType.CreditAppTable:
						ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
						CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return creditAppTable.CreditAppId;

					case RefType.CreditAppLine:
						ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
						CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(parm.RefGUID.StringToGuid());
						return creditAppLine.LineNum.ToString();

					case RefType.AssignmentAgreement:
						IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
						AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return assignmentAgreementTable.InternalAssignmentAgreementId;

					case RefType.MessengerJob:
						IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
						MessengerJobTable messengerJobTable = messengerJobTableRepo.GetMessengerJobTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return messengerJobTable.JobId;

					case RefType.MainAgreement:
						IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
						MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return mainAgreementTable.InternalMainAgreementId;

					case RefType.GuarantorAgreement:
						IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
						GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return guarantorAgreementTable.InternalGuarantorAgreementId;

					case RefType.BusinessCollateralAgreement:
						IBusinessCollateralAgmTableRepo businessCollaterlAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
						BusinessCollateralAgmTable businessCollateralAgmTable = businessCollaterlAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return businessCollateralAgmTable.InternalBusinessCollateralAgmId;

                    case RefType.CollectionFollowUp:
                        ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
                        CollectionFollowUp collectionFollowUp = collectionFollowUpRepo.GetCollectionFollowUpByIdNoTracking(parm.RefGUID.StringToGuid());
                        return collectionFollowUp.CollectionDate.DateToString();

                    case RefType.ProjectProgress:
                        IProjectProgressTableRepo projectProgressTableRepo = new ProjectProgressTableRepo(db);
                        ProjectProgressTable projectProgressTable = projectProgressTableRepo.GetProjectProgressTableByIdNoTracking(parm.RefGUID.StringToGuid());
                        return projectProgressTable.ProjectProgressId;

                    case RefType.CustomerRefund:
                        ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
                        CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(parm.RefGUID.StringToGuid());
                        return customerRefundTable.CustomerRefundId;

                    case RefType.PurchaseTable:
						IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
						PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return purchaseTable.PurchaseId;

					case RefType.ReceiptTemp:
						IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
						ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return receiptTempTable.ReceiptTempId;

					case RefType.Verification:
						IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
						VerificationTable verificationTable = verificationTableRepo.GetVerificationTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return verificationTable.VerificationId;

					case RefType.WithdrawalTable:
						IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
						WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return withdrawalTable.WithdrawalId;
					case RefType.DocumentReturn:
						IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
						DocumentReturnTable documentReturnTable = documentReturnTableRepo.GetDocumentReturnTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return documentReturnTable.DocumentReturnId;

					case RefType.PurchaseLine:
						IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
						PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(parm.RefGUID.StringToGuid());
						return purchaseLine.LineNum.ToString();

					case RefType.FreeTextInvoice:
						IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
						FreeTextInvoiceTable freeTextInvoiceTable = freeTextInvoiceTableRepo.GetFreeTextInvoiceTableByIdNoTracking(parm.RefGUID.StringToGuid());
						return freeTextInvoiceTable.FreeTextInvoiceId;
					default: return null;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region LIT1710 API GetAttachmentByFileName
		public async Task<AttachmentItemView> GetAttachmentByFileName(AttachmentK2Parm parm)
        {
            try
            {
				IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
				AttachmentItemView attachment = attachmentRepo
					.GetByReferenceAndFileName(parm.RefType, parm.RefGUID.StringToGuid(), parm.FileName)
					.ToAttachmentItemView();
				attachment = await attachment.GetFileContent();
				return attachment;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion LIT1710 API GetAttachmentByFileName
	}
}
