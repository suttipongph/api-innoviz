﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICurrencyService
	{
		#region Currency
		CurrencyItemView GetCurrencyById(string id);
		CurrencyItemView CreateCurrency(CurrencyItemView currencyView);
		CurrencyItemView UpdateCurrency(CurrencyItemView currencyView);
		bool DeleteCurrency(string id);
		#endregion Currency
		#region ExchangeRate
		ExchangeRateItemView GetExchangeRateById(string id);
		ExchangeRateItemView CreateExchangeRate(ExchangeRateItemView exchangeRateView);
		ExchangeRateItemView UpdateExchangeRate(ExchangeRateItemView exchangeRateView);
		bool DeleteExchangeRate(string id);
		ExchangeRateItemView GetExchangeRateInitialData(string currencyGUID);

		#endregion ExchangeRate
	}
	public class CurrencyService : SmartAppService, ICurrencyService
	{
		public CurrencyService(SmartAppDbContext context) : base(context) { }
		public CurrencyService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CurrencyService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CurrencyService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CurrencyService() : base() { }

		#region Currency
		public CurrencyItemView GetCurrencyById(string id)
		{
			try
			{
				ICurrencyRepo currencyRepo = new CurrencyRepo(db);
				return currencyRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CurrencyItemView CreateCurrency(CurrencyItemView currencyView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				currencyView = accessLevelService.AssignOwnerBU(currencyView);
				ICurrencyRepo currencyRepo = new CurrencyRepo(db);
				Currency currency = currencyView.ToCurrency();
				currency = currencyRepo.CreateCurrency(currency);
				base.LogTransactionCreate<Currency>(currency);
				UnitOfWork.Commit();
				return currency.ToCurrencyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CurrencyItemView UpdateCurrency(CurrencyItemView currencyView)
		{
			try
			{
				ICurrencyRepo currencyRepo = new CurrencyRepo(db);
				Currency inputCurrency = currencyView.ToCurrency();
				Currency dbCurrency = currencyRepo.Find(inputCurrency.CurrencyGUID);
				dbCurrency = currencyRepo.UpdateCurrency(dbCurrency, inputCurrency);
				base.LogTransactionUpdate<Currency>(GetOriginalValues<Currency>(dbCurrency), dbCurrency);
				UnitOfWork.Commit();
				return dbCurrency.ToCurrencyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCurrency(string item)
		{
			try
			{
				ICurrencyRepo currencyRepo = new CurrencyRepo(db);
				Guid currencyGUID = new Guid(item);
				Currency currency = currencyRepo.Find(currencyGUID);
				currencyRepo.Remove(currency);
				base.LogTransactionDelete<Currency>(currency);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Currency
		#region ExchangeRate
		public ExchangeRateItemView GetExchangeRateById(string id)
		{
			try
			{
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				return exchangeRateRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExchangeRateItemView CreateExchangeRate(ExchangeRateItemView exchangeRateView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				exchangeRateView = accessLevelService.AssignOwnerBU(exchangeRateView);
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				ExchangeRate exchangeRate = exchangeRateView.ToExchangeRate();
				exchangeRate = exchangeRateRepo.CreateExchangeRate(exchangeRate);
				base.LogTransactionCreate<ExchangeRate>(exchangeRate);
				UnitOfWork.Commit();
				return exchangeRate.ToExchangeRateItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExchangeRateItemView UpdateExchangeRate(ExchangeRateItemView exchangeRateView)
		{
			try
			{
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				ExchangeRate inputExchangeRate = exchangeRateView.ToExchangeRate();
				ExchangeRate dbExchangeRate = exchangeRateRepo.Find(inputExchangeRate.ExchangeRateGUID);
				dbExchangeRate = exchangeRateRepo.UpdateExchangeRate(dbExchangeRate, inputExchangeRate);
				base.LogTransactionUpdate<ExchangeRate>(GetOriginalValues<ExchangeRate>(dbExchangeRate), dbExchangeRate);
				UnitOfWork.Commit();
				return dbExchangeRate.ToExchangeRateItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteExchangeRate(string item)
		{
			try
			{
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				Guid exchangeRateGUID = new Guid(item);
				ExchangeRate exchangeRate = exchangeRateRepo.Find(exchangeRateGUID);
				exchangeRateRepo.Remove(exchangeRate);
				base.LogTransactionDelete<ExchangeRate>(exchangeRate);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExchangeRateItemView GetExchangeRateInitialData(string currencyGUID)
		{
			try
			{
				ICurrencyRepo currencyRepo = new CurrencyRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				Currency currency = currencyRepo.GetCurrencyByIdNoTrackingByAccessLevel(currencyGUID.StringToGuid());
				Currency homeCurrency = currencyRepo.GetHomeCurrencyByCompanyIdNoTrackingByAccessLevel(currency.CompanyGUID);
				return new ExchangeRateItemView
				{
					ExchangeRateGUID = new Guid().GuidNullToString(),
					CurrencyGUID = currency.CurrencyGUID.GuidNullToString(),
					HomeCurrencyGUID = (homeCurrency != null) ? homeCurrency.CurrencyGUID.GuidNullToString() : null,
					Currency_Values = SmartAppUtil.GetDropDownLabel(currency.CurrencyId, currency.Name),
					HomeCurrency_Values = (homeCurrency != null) ? SmartAppUtil.GetDropDownLabel(homeCurrency.CurrencyId, homeCurrency.Name) : null,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion ExchangeRate
	}
}
