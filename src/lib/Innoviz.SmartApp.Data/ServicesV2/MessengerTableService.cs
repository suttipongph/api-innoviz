using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IMessengerTableService
	{

		MessengerTableItemView GetMessengerTableById(string id);
		MessengerTableItemView CreateMessengerTable(MessengerTableItemView messengerTableView);
		MessengerTableItemView UpdateMessengerTable(MessengerTableItemView messengerTableView);
		bool DeleteMessengerTable(string id);
	}
	public class MessengerTableService : SmartAppService, IMessengerTableService
	{
		public MessengerTableService(SmartAppDbContext context) : base(context) { }
		public MessengerTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public MessengerTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MessengerTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public MessengerTableService() : base() { }

		public MessengerTableItemView GetMessengerTableById(string id)
		{
			try
			{
				IMessengerTableRepo messengerTableRepo = new MessengerTableRepo(db);
				return messengerTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MessengerTableItemView CreateMessengerTable(MessengerTableItemView messengerTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				messengerTableView = accessLevelService.AssignOwnerBU(messengerTableView);
				IMessengerTableRepo messengerTableRepo = new MessengerTableRepo(db);
				MessengerTable messengerTable = messengerTableView.ToMessengerTable();
				messengerTable = messengerTableRepo.CreateMessengerTable(messengerTable);
				base.LogTransactionCreate<MessengerTable>(messengerTable);
				UnitOfWork.Commit();
				return messengerTable.ToMessengerTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MessengerTableItemView UpdateMessengerTable(MessengerTableItemView messengerTableView)
		{
			try
			{
				IMessengerTableRepo messengerTableRepo = new MessengerTableRepo(db);
				MessengerTable inputMessengerTable = messengerTableView.ToMessengerTable();
				MessengerTable dbMessengerTable = messengerTableRepo.Find(inputMessengerTable.MessengerTableGUID);
				dbMessengerTable = messengerTableRepo.UpdateMessengerTable(dbMessengerTable, inputMessengerTable);
				base.LogTransactionUpdate<MessengerTable>(GetOriginalValues<MessengerTable>(dbMessengerTable), dbMessengerTable);
				UnitOfWork.Commit();
				return dbMessengerTable.ToMessengerTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteMessengerTable(string item)
		{
			try
			{
				IMessengerTableRepo messengerTableRepo = new MessengerTableRepo(db);
				Guid messengerTableGUID = new Guid(item);
				MessengerTable messengerTable = messengerTableRepo.Find(messengerTableGUID);
				messengerTableRepo.Remove(messengerTable);
				base.LogTransactionDelete<MessengerTable>(messengerTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
