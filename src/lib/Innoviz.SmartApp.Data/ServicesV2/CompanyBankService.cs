using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICompanyBankService
	{

		CompanyBankItemView GetCompanyBankById(string id);
		CompanyBankItemView CreateCompanyBank(CompanyBankItemView companyBankView);
		CompanyBankItemView UpdateCompanyBank(CompanyBankItemView companyBankView);
		bool DeleteCompanyBank(string id);
	}
	public class CompanyBankService : SmartAppService, ICompanyBankService
	{
		public CompanyBankService(SmartAppDbContext context) : base(context) { }
		public CompanyBankService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CompanyBankService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CompanyBankService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CompanyBankService() : base() { }

		public CompanyBankItemView GetCompanyBankById(string id)
		{
			try
			{
				ICompanyBankRepo companyBankRepo = new CompanyBankRepo(db);
				return companyBankRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyBankItemView CreateCompanyBank(CompanyBankItemView companyBankView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				companyBankView = accessLevelService.AssignOwnerBU(companyBankView);
				ICompanyBankRepo companyBankRepo = new CompanyBankRepo(db);
				CompanyBank companyBank = companyBankView.ToCompanyBank();
				companyBank = companyBankRepo.CreateCompanyBank(companyBank);
				base.LogTransactionCreate<CompanyBank>(companyBank);
				UnitOfWork.Commit();
				return companyBank.ToCompanyBankItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyBankItemView UpdateCompanyBank(CompanyBankItemView companyBankView)
		{
			try
			{
				ICompanyBankRepo companyBankRepo = new CompanyBankRepo(db);
				CompanyBank inputCompanyBank = companyBankView.ToCompanyBank();
				CompanyBank dbCompanyBank = companyBankRepo.Find(inputCompanyBank.CompanyBankGUID);
				dbCompanyBank = companyBankRepo.UpdateCompanyBank(dbCompanyBank, inputCompanyBank);
				base.LogTransactionUpdate<CompanyBank>(GetOriginalValues<CompanyBank>(dbCompanyBank), dbCompanyBank);
				UnitOfWork.Commit();
				return dbCompanyBank.ToCompanyBankItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCompanyBank(string item)
		{
			try
			{
				ICompanyBankRepo companyBankRepo = new CompanyBankRepo(db);
				Guid companyBankGUID = new Guid(item);
				CompanyBank companyBank = companyBankRepo.Find(companyBankGUID);
				companyBankRepo.Remove(companyBank);
				base.LogTransactionDelete<CompanyBank>(companyBank);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
