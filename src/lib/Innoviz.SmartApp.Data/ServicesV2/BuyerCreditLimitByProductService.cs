using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBuyerCreditLimitByProductService
	{

		BuyerCreditLimitByProductItemView GetBuyerCreditLimitByProductById(string id);
		BuyerCreditLimitByProductItemView CreateBuyerCreditLimitByProduct(BuyerCreditLimitByProductItemView buyerCreditLimitByProductView);
		BuyerCreditLimitByProductItemView UpdateBuyerCreditLimitByProduct(BuyerCreditLimitByProductItemView buyerCreditLimitByProductView);
		bool DeleteBuyerCreditLimitByProduct(string id);
		BuyerCreditLimitByProductItemView GetCreditLimitByProductByCreditAppRequest(CreditAppRequestLineItemView item);
		void CreateBuyerCreditLimitByProductCollection(List<BuyerCreditLimitByProductItemView> buyerCreditLimitByProductItemViews);
	}
	public class BuyerCreditLimitByProductService : SmartAppService, IBuyerCreditLimitByProductService
	{
		public BuyerCreditLimitByProductService(SmartAppDbContext context) : base(context) { }
		public BuyerCreditLimitByProductService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BuyerCreditLimitByProductService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerCreditLimitByProductService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BuyerCreditLimitByProductService() : base() { }

		public BuyerCreditLimitByProductItemView GetBuyerCreditLimitByProductById(string id)
		{
			try
			{
				IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
				BuyerCreditLimitByProductItemView buyerCreditLimitByProductItem = buyerCreditLimitByProductRepo.GetByIdvw(id.StringToGuid());
				buyerCreditLimitByProductItem.CreditLimitBalance = creditAppTableService.GetBuyerCreditLimitBalanceByProduct(buyerCreditLimitByProductItem.BuyerTableGUID, buyerCreditLimitByProductItem.ProductType, buyerCreditLimitByProductItem.CreditLimit);

				return buyerCreditLimitByProductItem;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerCreditLimitByProductItemView CreateBuyerCreditLimitByProduct(BuyerCreditLimitByProductItemView buyerCreditLimitByProductView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				buyerCreditLimitByProductView = accessLevelService.AssignOwnerBU(buyerCreditLimitByProductView);
				IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
				BuyerCreditLimitByProduct buyerCreditLimitByProduct = buyerCreditLimitByProductView.ToBuyerCreditLimitByProduct();
				buyerCreditLimitByProduct = buyerCreditLimitByProductRepo.CreateBuyerCreditLimitByProduct(buyerCreditLimitByProduct);
				base.LogTransactionCreate<BuyerCreditLimitByProduct>(buyerCreditLimitByProduct);
				UnitOfWork.Commit();
				return buyerCreditLimitByProduct.ToBuyerCreditLimitByProductItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerCreditLimitByProductItemView UpdateBuyerCreditLimitByProduct(BuyerCreditLimitByProductItemView buyerCreditLimitByProductView)
		{
			try
			{
				IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
				BuyerCreditLimitByProduct inputBuyerCreditLimitByProduct = buyerCreditLimitByProductView.ToBuyerCreditLimitByProduct();
				BuyerCreditLimitByProduct dbBuyerCreditLimitByProduct = buyerCreditLimitByProductRepo.Find(inputBuyerCreditLimitByProduct.BuyerCreditLimitByProductGUID);
				dbBuyerCreditLimitByProduct = buyerCreditLimitByProductRepo.UpdateBuyerCreditLimitByProduct(dbBuyerCreditLimitByProduct, inputBuyerCreditLimitByProduct);
				base.LogTransactionUpdate<BuyerCreditLimitByProduct>(GetOriginalValues<BuyerCreditLimitByProduct>(dbBuyerCreditLimitByProduct), dbBuyerCreditLimitByProduct);
				UnitOfWork.Commit();
				return dbBuyerCreditLimitByProduct.ToBuyerCreditLimitByProductItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBuyerCreditLimitByProduct(string item)
		{
			try
			{
				IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
				Guid buyerCreditLimitByProductGUID = new Guid(item);
				BuyerCreditLimitByProduct buyerCreditLimitByProduct = buyerCreditLimitByProductRepo.Find(buyerCreditLimitByProductGUID);
				buyerCreditLimitByProductRepo.Remove(buyerCreditLimitByProduct);
				base.LogTransactionDelete<BuyerCreditLimitByProduct>(buyerCreditLimitByProduct);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerCreditLimitByProductItemView GetCreditLimitByProductByCreditAppRequest(CreditAppRequestLineItemView item)
		{
			try
			{
				Guid buyeTableGUID = item.BuyerTableGUID.StringToGuid();
				IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
				IQueryable<BuyerCreditLimitByProduct> buyerCreditLimitByProducts = buyerCreditLimitByProductRepo.GetBuyerCreditLimitByProductByBuyer(buyeTableGUID).Where(w => w.ProductType == item.CreditAppRequestTable_ProductType);
				return (buyerCreditLimitByProducts.Any()) ? buyerCreditLimitByProducts.FirstOrDefault().ToBuyerCreditLimitByProductItemView() : null;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public void CreateBuyerCreditLimitByProductCollection(List<BuyerCreditLimitByProductItemView> buyerCreditLimitByProductItemViews)
		{
			try
			{
				if (buyerCreditLimitByProductItemViews.Any())
				{
					IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
					List<BuyerCreditLimitByProduct> buyerCreditLimitByProduct = buyerCreditLimitByProductItemViews.ToBuyerCreditLimitByProduct().ToList();
					buyerCreditLimitByProductRepo.ValidateAdd(buyerCreditLimitByProduct);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(buyerCreditLimitByProduct, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}

	}
}
