using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAgreementTableInfoTextService
	{

		AgreementTableInfoTextItemView GetAgreementTableInfoTextById(string id);
		AgreementTableInfoTextItemView CreateAgreementTableInfoText(AgreementTableInfoTextItemView agreementTableInfoTextView);
		AgreementTableInfoText CreateAgreementTableInfoText(AgreementTableInfoText agreementTableInfoText);
		AgreementTableInfoTextItemView UpdateAgreementTableInfoText(AgreementTableInfoTextItemView agreementTableInfoTextView);
		AgreementTableInfoText UpdateAgreementTableInfoText(AgreementTableInfoText agreementTableInfoText);
		bool DeleteAgreementTableInfoText(string id);
	}
	public class AgreementTableInfoTextService : SmartAppService, IAgreementTableInfoTextService
	{
		public AgreementTableInfoTextService(SmartAppDbContext context) : base(context) { }
		public AgreementTableInfoTextService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AgreementTableInfoTextService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AgreementTableInfoTextService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AgreementTableInfoTextService() : base() { }

		public AgreementTableInfoTextItemView GetAgreementTableInfoTextById(string id)
		{
			try
			{
				IAgreementTableInfoTextRepo agreementTableInfoTextRepo = new AgreementTableInfoTextRepo(db);
				return agreementTableInfoTextRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoTextItemView CreateAgreementTableInfoText(AgreementTableInfoTextItemView agreementTableInfoTextView)
		{
			try
			{
				AgreementTableInfoText agreementTableInfoText = agreementTableInfoTextView.ToAgreementTableInfoText();
				agreementTableInfoText = CreateAgreementTableInfoText(agreementTableInfoText);
				UnitOfWork.Commit();
				return agreementTableInfoText.ToAgreementTableInfoTextItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoText CreateAgreementTableInfoText(AgreementTableInfoText agreementTableInfoText)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				agreementTableInfoText = accessLevelService.AssignOwnerBU(agreementTableInfoText);
				IAgreementTableInfoTextRepo agreementTableInfoTextRepo = new AgreementTableInfoTextRepo(db);
				agreementTableInfoText = agreementTableInfoTextRepo.CreateAgreementTableInfoText(agreementTableInfoText);
				base.LogTransactionCreate<AgreementTableInfoText>(agreementTableInfoText);
				return agreementTableInfoText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoTextItemView UpdateAgreementTableInfoText(AgreementTableInfoTextItemView agreementTableInfoTextView)
		{
			try
			{
				IAgreementTableInfoTextRepo agreementTableInfoTextRepo = new AgreementTableInfoTextRepo(db);
				AgreementTableInfoText inputAgreementTableInfoText = agreementTableInfoTextView.ToAgreementTableInfoText();
				AgreementTableInfoText dbAgreementTableInfoText = UpdateAgreementTableInfoText(inputAgreementTableInfoText);
				UnitOfWork.Commit();
				return dbAgreementTableInfoText.ToAgreementTableInfoTextItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoText UpdateAgreementTableInfoText(AgreementTableInfoText agreementTableInfoText)
		{
			try
			{
				IAgreementTableInfoTextRepo agreementTableInfoTextRepo = new AgreementTableInfoTextRepo(db);
				AgreementTableInfoText dbAgreementTableInfoText = agreementTableInfoTextRepo.Find(agreementTableInfoText.AgreementTableInfoTextGUID);
				dbAgreementTableInfoText = agreementTableInfoTextRepo.UpdateAgreementTableInfoText(dbAgreementTableInfoText, agreementTableInfoText);
				base.LogTransactionUpdate<AgreementTableInfoText>(GetOriginalValues<AgreementTableInfoText>(dbAgreementTableInfoText), dbAgreementTableInfoText);
				return dbAgreementTableInfoText;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAgreementTableInfoText(string item)
		{
			try
			{
				IAgreementTableInfoTextRepo agreementTableInfoTextRepo = new AgreementTableInfoTextRepo(db);
				Guid agreementTableInfoTextGUID = new Guid(item);
				AgreementTableInfoText agreementTableInfoText = agreementTableInfoTextRepo.Find(agreementTableInfoTextGUID);
				agreementTableInfoTextRepo.Remove(agreementTableInfoText);
				base.LogTransactionDelete<AgreementTableInfoText>(agreementTableInfoText);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
