using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface INumberSeqSetupByProductTypeService
	{

		NumberSeqSetupByProductTypeItemView GetNumberSeqSetupByProductTypeById(string id);
		NumberSeqSetupByProductTypeItemView CreateNumberSeqSetupByProductType(NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeView);
		NumberSeqSetupByProductTypeItemView UpdateNumberSeqSetupByProductType(NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeView);
		bool DeleteNumberSeqSetupByProductType(string id);
	}
	public class NumberSeqSetupByProductTypeService : SmartAppService, INumberSeqSetupByProductTypeService
	{
		public NumberSeqSetupByProductTypeService(SmartAppDbContext context) : base(context) { }
		public NumberSeqSetupByProductTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public NumberSeqSetupByProductTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NumberSeqSetupByProductTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public NumberSeqSetupByProductTypeService() : base() { }

		public NumberSeqSetupByProductTypeItemView GetNumberSeqSetupByProductTypeById(string id)
		{
			try
			{
				INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
				return numberSeqSetupByProductTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqSetupByProductTypeItemView CreateNumberSeqSetupByProductType(NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				numberSeqSetupByProductTypeView = accessLevelService.AssignOwnerBU(numberSeqSetupByProductTypeView);
				INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
				NumberSeqSetupByProductType numberSeqSetupByProductType = numberSeqSetupByProductTypeView.ToNumberSeqSetupByProductType();
				numberSeqSetupByProductType = numberSeqSetupByProductTypeRepo.CreateNumberSeqSetupByProductType(numberSeqSetupByProductType);
				base.LogTransactionCreate<NumberSeqSetupByProductType>(numberSeqSetupByProductType);
				UnitOfWork.Commit();
				return numberSeqSetupByProductType.ToNumberSeqSetupByProductTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqSetupByProductTypeItemView UpdateNumberSeqSetupByProductType(NumberSeqSetupByProductTypeItemView numberSeqSetupByProductTypeView)
		{
			try
			{
				INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
				NumberSeqSetupByProductType inputNumberSeqSetupByProductType = numberSeqSetupByProductTypeView.ToNumberSeqSetupByProductType();
				NumberSeqSetupByProductType dbNumberSeqSetupByProductType = numberSeqSetupByProductTypeRepo.Find(inputNumberSeqSetupByProductType.NumberSeqSetupByProductTypeGUID);
				dbNumberSeqSetupByProductType = numberSeqSetupByProductTypeRepo.UpdateNumberSeqSetupByProductType(dbNumberSeqSetupByProductType, inputNumberSeqSetupByProductType);
				base.LogTransactionUpdate<NumberSeqSetupByProductType>(GetOriginalValues<NumberSeqSetupByProductType>(dbNumberSeqSetupByProductType), dbNumberSeqSetupByProductType);
				UnitOfWork.Commit();
				return dbNumberSeqSetupByProductType.ToNumberSeqSetupByProductTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteNumberSeqSetupByProductType(string item)
		{
			try
			{
				INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
				Guid numberSeqSetupByProductTypeGUID = new Guid(item);
				NumberSeqSetupByProductType numberSeqSetupByProductType = numberSeqSetupByProductTypeRepo.Find(numberSeqSetupByProductTypeGUID);
				numberSeqSetupByProductTypeRepo.Remove(numberSeqSetupByProductType);
				base.LogTransactionDelete<NumberSeqSetupByProductType>(numberSeqSetupByProductType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
