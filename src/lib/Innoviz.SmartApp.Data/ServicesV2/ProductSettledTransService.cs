using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IProductSettledTransService
    {

        ProductSettledTransItemView GetProductSettledTransById(string id);
        ProductSettledTransItemView CreateProductSettledTrans(ProductSettledTransItemView productSettledTransView);
        ProductSettledTrans CreateProductSettledTrans(ProductSettledTrans productSettledTrans);
        ProductSettledTransItemView UpdateProductSettledTrans(ProductSettledTransItemView productSettledTransView);
        ProductSettledTrans UpdateProductSettledTrans(ProductSettledTrans productSettledTrans);
        bool DeleteProductSettledTrans(string id);
        bool DeleteProductSettledTrans(ProductSettledTrans productSettledTrans);
        #region shared
        #region RecalculateProductSettledTrans
        ProductSettledTrans RecalculateProductSettledTrans(InvoiceSettlementDetail invoiceSettlementDetail);
        public RecalProductSettledTransView GetRecalProductSettledTransById(string id);
        RecalProductSettledTransResultView RecalculateProductSettledTrans(RecalProductSettledTransView recalProductSettledTransView);

        #endregion
        #endregion
        void CreateProductSettledTransCollection(IEnumerable<ProductSettledTransItemView> ProductSettledTransItemView);
    }
    public class ProductSettledTransService : SmartAppService, IProductSettledTransService
    {
        public ProductSettledTransService(SmartAppDbContext context) : base(context) { }
        public ProductSettledTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public ProductSettledTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ProductSettledTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public ProductSettledTransService() : base() { }

        public ProductSettledTransItemView GetProductSettledTransById(string id)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                return productSettledTransRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProductSettledTransItemView CreateProductSettledTrans(ProductSettledTransItemView productSettledTransView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                productSettledTransView = accessLevelService.AssignOwnerBU(productSettledTransView);
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                ProductSettledTrans productSettledTrans = productSettledTransView.ToProductSettledTrans();
                productSettledTrans = productSettledTransRepo.CreateProductSettledTrans(productSettledTrans);
                base.LogTransactionCreate<ProductSettledTrans>(productSettledTrans);
                UnitOfWork.Commit();
                return productSettledTrans.ToProductSettledTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProductSettledTrans CreateProductSettledTrans(ProductSettledTrans productSettledTrans)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                productSettledTrans = accessLevelService.AssignOwnerBU(productSettledTrans);
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                productSettledTrans = productSettledTransRepo.CreateProductSettledTrans(productSettledTrans);
                base.LogTransactionCreate<ProductSettledTrans>(productSettledTrans);
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProductSettledTransItemView UpdateProductSettledTrans(ProductSettledTransItemView productSettledTransView)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                ProductSettledTrans inputProductSettledTrans = productSettledTransView.ToProductSettledTrans();
                ProductSettledTrans dbProductSettledTrans = productSettledTransRepo.Find(inputProductSettledTrans.ProductSettledTransGUID);
                dbProductSettledTrans = productSettledTransRepo.UpdateProductSettledTrans(dbProductSettledTrans, inputProductSettledTrans);
                base.LogTransactionUpdate<ProductSettledTrans>(GetOriginalValues<ProductSettledTrans>(dbProductSettledTrans), dbProductSettledTrans);
                UnitOfWork.Commit();
                return dbProductSettledTrans.ToProductSettledTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ProductSettledTrans UpdateProductSettledTrans(ProductSettledTrans productSettledTrans)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                ProductSettledTrans inputProductSettledTrans = productSettledTrans;
                ProductSettledTrans dbProductSettledTrans = productSettledTransRepo.Find(inputProductSettledTrans.ProductSettledTransGUID);
                dbProductSettledTrans = productSettledTransRepo.UpdateProductSettledTrans(dbProductSettledTrans, inputProductSettledTrans);
                base.LogTransactionUpdate<ProductSettledTrans>(GetOriginalValues<ProductSettledTrans>(dbProductSettledTrans), dbProductSettledTrans);
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteProductSettledTrans(string item)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                Guid productSettledTransGUID = new Guid(item);
                ProductSettledTrans productSettledTrans = productSettledTransRepo.Find(productSettledTransGUID);
                productSettledTransRepo.Remove(productSettledTrans);
                base.LogTransactionDelete<ProductSettledTrans>(productSettledTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteProductSettledTrans(ProductSettledTrans productSettledTrans)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                productSettledTransRepo.Remove(productSettledTrans);
                base.LogTransactionDelete<ProductSettledTrans>(productSettledTrans);
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region shared
        #region RecalculateProductSettledTrans
        public ProductSettledTrans RecalculateProductSettledTrans(InvoiceSettlementDetail invoiceSettlementDetail)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
                InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceSettlementDetail.InvoiceTableGUID.GetValueOrDefault());
                ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(invoiceSettlementDetail.RefGUID.Value);
                ProductSettledTrans productSettledTrans = new ProductSettledTrans();

                // R07
                DateTime? lastReceivedDate = invoiceSettlementDetail != null && invoiceSettlementDetail.InvoiceTableGUID.HasValue ? 
                                paymentHistoryService.GetPaymentHistoryLastReceivedDate(invoiceSettlementDetail.InvoiceTableGUID.Value) : null;
                
                if (RecalculateProductSettledTransValidation(invoiceTable, invoiceSettlementDetail, receiptTempTable, lastReceivedDate))
                {
                    productSettledTrans = productSettledTransRepo.GetProductSettledTransByInvoiceSettlementDetailGuid(invoiceSettlementDetail.InvoiceSettlementDetailGUID);
                    // 1
                    productSettledTrans = UpdateProductSettledTransRecalculateProductSettledTrans(productSettledTrans, invoiceTable, invoiceSettlementDetail, receiptTempTable);
                    // 2
                    productSettledTrans = UpdateProductSettledTransByRefTypeRecalculateProductSettledTrans(productSettledTrans, invoiceTable, 
                                                                                            invoiceSettlementDetail, lastReceivedDate);
                }
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool RecalculateProductSettledTransValidation(InvoiceTable invoiceTable, InvoiceSettlementDetail invoiceSettlementDetail, 
                                                            ReceiptTempTable receiptTempTable, DateTime? lastReceivedDate)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                if (!invoiceTable.ProductInvoice)
                {
                    ex.AddData("ERROR.90095", new string[] { "LABEL.RECALCULATE_PRODUCT_SETTLED_TRANS", "LABEL.PRODUCT_INVOICE" });
                }
                if (invoiceSettlementDetail.RefType != (int)RefType.ReceiptTemp)
                {
                    ex.AddData("ERROR.90091", new string[] { "LABEL.RECALCULATE_PRODUCT_SETTLED_TRANS", "LABEL.RECEIPT_TEMP_TABLE" });
                }
                if (receiptTempTable == null)
                {
                    ex.AddData("ERROR.90092", new string[] { "LABEL.RECEIPT_TEMP_TABLE" });
                }
                else
                {
                    DocumentStatus receiptTempTableStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(receiptTempTable.DocumentStatusGUID);
                    if (int.Parse(receiptTempTableStatus.StatusId) > (int)TemporaryReceiptStatus.Draft)
                    {
                        ex.AddData("ERROR.90092", new string[] { "LABEL.RECEIPT_TEMP_TABLE" });
                    }
                    // R07
                    if (invoiceTable.ProductInvoice && invoiceTable.ProductType == (int)ProductType.ProjectFinance && 
                        lastReceivedDate != null && receiptTempTable.ReceiptDate < lastReceivedDate)
                    {
                        ex.AddData("ERROR.90170", "LABEL.RECEIPT_DATE", "LABEL.COLLECTION", "LABEL.LAST_RECEIVED_DATE", 
                                    "LABEL.INVOICE_ID", invoiceTable.InvoiceId, lastReceivedDate.DateNullToString());
                    }
                }
                
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private ProductSettledTrans UpdateProductSettledTransRecalculateProductSettledTrans(ProductSettledTrans productSettledTrans, InvoiceTable invoiceTable, InvoiceSettlementDetail invoiceSettlementDetail, ReceiptTempTable receiptTempTable)
        {
            try
            {
                // 1.1
                if (productSettledTrans == null)
                {
                    productSettledTrans = new ProductSettledTrans()
                    {
                        InvoiceSettlementDetailGUID = invoiceSettlementDetail.InvoiceSettlementDetailGUID,
                        ProductType = invoiceSettlementDetail.ProductType,
                        RefType = invoiceTable.RefType,
                        RefGUID = invoiceTable.RefGUID,
                        DocumentId = invoiceTable.DocumentId,
                        SettledDate = receiptTempTable.ReceiptDate,
                        SettledInvoiceAmount = invoiceSettlementDetail.SettleInvoiceAmount,
                        CompanyGUID = invoiceTable.CompanyGUID
                    };
                }
                // 1.2
                else
                {
                    productSettledTrans.SettledInvoiceAmount = invoiceSettlementDetail.SettleInvoiceAmount;
                    productSettledTrans.ModifiedBy = db.GetUserName();
                    productSettledTrans.ModifiedDateTime = DateTime.Now;
                }
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private ProductSettledTrans UpdateProductSettledTransByRefTypeRecalculateProductSettledTrans(ProductSettledTrans productSettledTrans, 
                                                                                                    InvoiceTable invoiceTable, 
                                                                                                    InvoiceSettlementDetail invoiceSettlementDetail, 
                                                                                                    DateTime? lastReceivedDate)
        {
            try
            {
                // 2.1
                if (productSettledTrans.RefType == (int)RefType.PurchaseLine)
                {
                    IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                    PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(invoiceTable.RefGUID);
                    Guid? originalPurchaseLineGuid = purchaseLine.OriginalPurchaseLineGUID;
                    PurchaseLine originalPurchaseLine = new PurchaseLine();
                    if (originalPurchaseLineGuid != null)
                    {
                        originalPurchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(originalPurchaseLineGuid.GetValueOrDefault());
                    }
                    productSettledTrans.SettledPurchaseAmount = invoiceSettlementDetail.SettlePurchaseAmount;
                    productSettledTrans.SettledReserveAmount = invoiceSettlementDetail.SettleReserveAmount;
                    productSettledTrans.ReserveToBeRefund = (invoiceSettlementDetail.SettleAmount - invoiceSettlementDetail.PurchaseAmountBalance) <= 0 ? 0 : (invoiceSettlementDetail.SettleAmount - invoiceSettlementDetail.PurchaseAmountBalance);
                    productSettledTrans.OriginalRefGUID = originalPurchaseLineGuid == null ? invoiceTable.RefGUID : originalPurchaseLine.PurchaseLineGUID;
                    if (productSettledTrans.OriginalRefGUID == productSettledTrans.RefGUID)
                    {
                        productSettledTrans.OriginalDocumentId = invoiceTable.DocumentId;
                        productSettledTrans.InterestDate = purchaseLine.InterestDate;
                    }
                    else
                    {
                        IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                        InvoiceTable dbInvoiceTable = invoiceTableRepo.GetInvoiceTableByRefType((int)RefType.PurchaseLine, originalPurchaseLine.PurchaseLineGUID);
                        productSettledTrans.OriginalDocumentId = dbInvoiceTable.DocumentId;
                        productSettledTrans.InterestDate = originalPurchaseLine.InterestDate;
                    }

                    // R07
                    productSettledTrans.InterestDate = purchaseLine.InterestDate;

                    productSettledTrans.InterestDay = (int)(productSettledTrans.SettledDate - productSettledTrans.InterestDate).TotalDays;
                    InterestCalcAmountView interestCalcAmountView = CalcFTInterestAmountFromSettlement(purchaseLine, originalPurchaseLine, productSettledTrans, invoiceSettlementDetail);
                    productSettledTrans.InterestCalcAmount = interestCalcAmountView.InterestCalcAmount;
                    productSettledTrans.InterestDay = interestCalcAmountView.InterestDay;
                    productSettledTrans.OriginalInterestCalcAmount = productSettledTrans.InterestCalcAmount;
                }
                // 2.2
                else if (productSettledTrans.RefType == (int)RefType.WithdrawalLine)
                {
                    IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
                    IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                    IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                    ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                    CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(db.GetCompanyFilter().StringToGuid());
                    WithdrawalLine withdrawalLine = withdrawalLineRepo.GetWithdrawalLineByIdNoTracking(productSettledTrans.RefGUID.GetValueOrDefault());
                    WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalLine.WithdrawalTableGUID);
                    Guid? orgWithdrawalTableGuid = withdrawalTable.OriginalWithdrawalTableGUID;
                    decimal totalInterestPct = 0;
                    decimal interesPertDay = 0;
                    WithdrawalLine originalWithdrawalLine = new WithdrawalLine();
                    if (orgWithdrawalTableGuid != null)
                    {
                        originalWithdrawalLine = withdrawalLineRepo.GetWithdrawalLineByWithdrawalTableGuid(orgWithdrawalTableGuid.GetValueOrDefault())
                            .ToList().Find(f => f.WithdrawalLineType == (int)WithdrawalLineType.Principal);
                    }
                    productSettledTrans.OriginalRefGUID = orgWithdrawalTableGuid == null ? invoiceTable.RefGUID : originalWithdrawalLine.WithdrawalLineGUID;
                    if (productSettledTrans.OriginalRefGUID == productSettledTrans.RefGUID)
                    {
                        totalInterestPct = withdrawalTable.TotalInterestPct / 100;
                        productSettledTrans.OriginalDocumentId = invoiceTable.DocumentId;
                        productSettledTrans.InterestDate = withdrawalLine.InterestDate;
                    }
                    else
                    {
                        IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                        WithdrawalTable dbWithdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(orgWithdrawalTableGuid.GetValueOrDefault());
                        totalInterestPct = dbWithdrawalTable.TotalInterestPct / 100;
                        InvoiceTable dbInvoiceTable = invoiceTableRepo.GetInvoiceTableByRefType((int)RefType.WithdrawalLine, originalWithdrawalLine.WithdrawalLineGUID);
                        productSettledTrans.OriginalDocumentId = invoiceTable.DocumentId;
                        productSettledTrans.InterestDate = originalWithdrawalLine.InterestDate;
                    }

                    // R07
                    productSettledTrans.InterestDate = withdrawalLine.InterestDate;
                    productSettledTrans.LastReceivedDate = lastReceivedDate;
                    productSettledTrans.InterestDay = lastReceivedDate.HasValue ?
                        (int)(productSettledTrans.SettledDate - lastReceivedDate.Value).TotalDays :
                        (int)(productSettledTrans.SettledDate - productSettledTrans.InterestDate).TotalDays;
                    
                    interesPertDay = invoiceSettlementDetail.BalanceAmount * (totalInterestPct / companyParameter.DaysPerYear);
                    productSettledTrans.InterestCalcAmount = (interesPertDay * productSettledTrans.InterestDay).Round();
                    productSettledTrans.OriginalInterestCalcAmount = productSettledTrans.InterestCalcAmount;
                    if (invoiceSettlementDetail.BalanceAmount == invoiceSettlementDetail.SettleInvoiceAmount)
                    {
                        if (productSettledTrans.OriginalRefGUID == productSettledTrans.RefGUID)
                        {
                            orgWithdrawalTableGuid = withdrawalLine.WithdrawalTableGUID;
                        }
                        IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                        IEnumerable<ProductSettledTrans> dbProductSettledTrans = productSettledTransRepo.GetProductSettledTransByOriginalRefGuid(productSettledTrans.OriginalRefGUID.GetValueOrDefault());
                        IEnumerable<WithdrawalLineItemViewMap> withdrawalLineItemViewMaps = withdrawalLineRepo.GetWithdrawalLineItemViewMapByOriginalWithdrawalTableGuid(orgWithdrawalTableGuid.GetValueOrDefault());
                        withdrawalLineItemViewMaps = withdrawalLineItemViewMaps.Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Interest);
                        productSettledTrans.PDCInterestOutstanding = withdrawalTableService.CalcPDCInterestOutstanding(orgWithdrawalTableGuid.GetValueOrDefault());
                        dbProductSettledTrans = dbProductSettledTrans.Where(w => w.ProductSettledTransGUID != productSettledTrans.ProductSettledTransGUID);
                        productSettledTrans.TotalRefundAdditionalIntAmount = dbProductSettledTrans.Sum(s => s.InterestCalcAmount) + productSettledTrans.InterestCalcAmount + productSettledTrans.PDCInterestOutstanding;
                    }
                    else
                    {
                        productSettledTrans.PDCInterestOutstanding = 0;
                        productSettledTrans.TotalRefundAdditionalIntAmount = 0;
                    }

                }
                return productSettledTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private InterestCalcAmountView CalcFTInterestAmountFromSettlement(PurchaseLine purchaseLine, PurchaseLine OriginalPurchaseLine
            , ProductSettledTrans productSettledTrans, InvoiceSettlementDetail invoiceSettlementDetail)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(db.GetCompanyFilter().StringToGuid());
                decimal totalInterestPct = 0;
                decimal interesPertDay = 0;
                PurchaseTable purchaseTable = new PurchaseTable();
                if (purchaseLine.OriginalPurchaseLineGUID == null)
                {
                    purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseLine.PurchaseTableGUID);
                }
                else
                {
                    purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(OriginalPurchaseLine.PurchaseTableGUID);
                }
                if (productSettledTrans.InterestDay > 0)
                {
                    if (productSettledTrans.InterestDay >= companyParameter.FTMinSettleInterestDayMaxPct)
                    {
                        totalInterestPct = companyParameter.MaxInterestPct;
                    }
                    else
                    {
                        totalInterestPct = purchaseTable.TotalInterestPct;
                    }
                }
                else
                {
                    if (invoiceSettlementDetail.InterestCalculationMethod == (int)InterestCalculationMethod.Minimum)
                    {
                        if ((int)(productSettledTrans.SettledDate - purchaseTable.PurchaseDate).TotalDays < companyParameter.FTMinInterestDay)
                        {
                            productSettledTrans.InterestDay = (int)(purchaseTable.PurchaseDate.AddDays(companyParameter.FTMinInterestDay) - productSettledTrans.InterestDate).TotalDays;
                        }
                    }
                    totalInterestPct = purchaseTable.TotalInterestPct;
                }
                interesPertDay = productSettledTrans.SettledPurchaseAmount * (totalInterestPct / 100 / companyParameter.DaysPerYear);
                InterestCalcAmountView interestCalcAmountView = new InterestCalcAmountView()
                {
                    InterestCalcAmount = (Convert.ToDecimal(interesPertDay * productSettledTrans.InterestDay).Round()),
                    InterestDay = productSettledTrans.InterestDay
                };
                return interestCalcAmountView;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public RecalProductSettledTransView GetRecalProductSettledTransById(string id)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                ProductSettledTrans productSettledTrans = productSettledTransRepo.GetProductSettledTransByIdNoTracking(id.StringToGuid());

                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                InvoiceSettlementDetailItemView invoiceSettlementDetailItemView = invoiceSettlementDetailRepo.GetByIdvw(productSettledTrans.InvoiceSettlementDetailGUID);

                RecalProductSettledTransView result = new RecalProductSettledTransView
                {
                    ProductSettledTransGUID = productSettledTrans.ProductSettledTransGUID.GuidNullToString(),
                    DocumentId = productSettledTrans.DocumentId,
                    InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID.GuidNullToString(),
                    InvoiceTable_Values = invoiceSettlementDetailItemView.InvoiceTable_Values
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public RecalProductSettledTransResultView RecalculateProductSettledTrans(RecalProductSettledTransView recalProductSettledTransView)
        {
            try
            {
                RecalProductSettledTransResultView result = new RecalProductSettledTransResultView();

                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                InvoiceSettlementDetailItemView invoiceSettlementDetailView = invoiceSettlementDetailService.GetInvoiceSettlementDetailById(recalProductSettledTransView.InvoiceSettlementDetailGUID);

                InvoiceSettlementDetail invoiceSettlementDetail = invoiceSettlementDetailView.ToInvoiceSettlementDetail();
                ProductSettledTrans productSettledTrans = RecalculateProductSettledTrans(invoiceSettlementDetail);
                if (productSettledTrans != null)
                {
                    UpdateProductSettledTrans(productSettledTrans);
                    UnitOfWork.Commit();
                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.90015", new string[] { productSettledTrans.DocumentId });
                    result.Notification = success;
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region Migration
        public void CreateProductSettledTransCollection(IEnumerable<ProductSettledTransItemView> productSettledTransItemView)
        {
            try
            {
                if (productSettledTransItemView.Any())
                {
                    IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                    List<ProductSettledTrans> productSettledTranses = productSettledTransItemView.ToProductSettledTrans().ToList();

                    #region Validate
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    // 1
                    IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                    IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                    List<ProductSettledTrans> productSettledTransByPurchaseLine = productSettledTranses.Where(w => w.RefType == (int)RefType.PurchaseLine).ToList();
                    List<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByIdNoTracking(productSettledTransByPurchaseLine.Select(s => s.RefGUID.Value));
                    List<PurchaseLine> purchaseLinesInvoiceTableNotValue = purchaseLines.Where(w => !w.PurchaseLineInvoiceTableGUID.HasValue).ToList();
                    bool isPurchaseLineInvoiceTableNotValue = ConditionService.IsNotZero(purchaseLinesInvoiceTableNotValue.ToList().Count());
                    if (isPurchaseLineInvoiceTableNotValue)
                    {
                        List<PurchaseTable> purchaseTables = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseLinesInvoiceTableNotValue.Select(s => s.PurchaseTableGUID));
                        purchaseLinesInvoiceTableNotValue.ForEach(purchaseLine =>
                        {
                            PurchaseTable purchaseTable = purchaseTables.Where(w => w.PurchaseTableGUID == purchaseLine.PurchaseTableGUID).FirstOrDefault();
                            string messagePurchaseError = purchaseTable.PurchaseId + '#' + purchaseLine.LineNum.ToString();
                            ex.AddData("ERROR.90161", new string[] { "LABEL.PURCHASE_LINE", messagePurchaseError, "LABEL.PRODUCT_SETTLEMENT_TRANSACTIONS" });
                        });
                    }

                    //2
                    IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                    IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
                    List<ProductSettledTrans> productSettledTransByWithdrawalLine = productSettledTranses.Where(w => w.RefType == (int)RefType.WithdrawalLine).ToList();
                    List<WithdrawalLine> withdrawalLines = withdrawalLineRepo.GetWithdrawalLineByIdNoTracking(productSettledTransByWithdrawalLine.Select(s => s.RefGUID.Value));
                    List<WithdrawalLine> withdrawalLinesInvoiceTableNotValue = withdrawalLines.Where(w => !w.WithdrawalLineInvoiceTableGUID.HasValue).ToList();
                    bool isWithdrawalLineInvoiceTableNotValue = ConditionService.IsNotZero(withdrawalLinesInvoiceTableNotValue.Count());
                    if (isWithdrawalLineInvoiceTableNotValue)
                    {
                        List<WithdrawalTable> withdrawalTables = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalLinesInvoiceTableNotValue.Select(s => s.WithdrawalTableGUID));
                        withdrawalLinesInvoiceTableNotValue.ForEach(withdrawalLine =>
                        {
                            WithdrawalTable withdrawalTable = withdrawalTables.Where(w => w.WithdrawalTableGUID == withdrawalLine.WithdrawalTableGUID).FirstOrDefault();
                            ex.AddData("ERROR.90161", new string[] { "LABEL.WITHDRAWAL_LINE", withdrawalTable.WithdrawalId, "LABEL.PRODUCT_SETTLEMENT_TRANSACTIONS" });
                        });
                    }

                    if (ex.Data.Count > 0)
                    {
                        throw SmartAppUtil.AddStackTrace(ex);
                    }

                    productSettledTransRepo.ValidateAdd(productSettledTranses);
                    #endregion Validate

                    #region UpdateCustTrans
                    ICustTransRepo custTransRepo = new CustTransRepo(db);
                    List<CustTrans> updateCustTranses = new List<CustTrans>();
                    #region 1. UpdateCustTrans for PurchaseLine
                    // 1.1
                    var productSettledTransByPurchaseLineGroupRefGUID = productSettledTransByPurchaseLine.GroupBy(g => new { g.RefType, g.RefGUID }).Select(p =>
                    new
                    {
                        SumSettledInvoiceAmount = p.Sum(s => s.SettledInvoiceAmount),
                        RefGUID = p.Key.RefGUID,
                        RefType = p.Key.RefType,
                        SettledDate = p.Max(m => m.SettledDate)
                    }).ToList();

                    // 1.2
                    List<PurchaseLine> purchaseLinesByGroupRefGUID = purchaseLineRepo.GetPurchaseLineByIdNoTracking(productSettledTransByPurchaseLineGroupRefGUID.Select(s => s.RefGUID.Value));
                    List<CustTrans> custTransByPurchaseLine = custTransRepo.GetCustTransByInvoiceTableNoTracking(purchaseLinesByGroupRefGUID.Select(s => s.PurchaseLineInvoiceTableGUID.Value).ToList()).ToList();
                    custTransByPurchaseLine.Select(c =>
                    {
                        Guid purchaseLineGUID = purchaseLinesByGroupRefGUID.Where(w => w.PurchaseLineInvoiceTableGUID == c.InvoiceTableGUID).FirstOrDefault().PurchaseLineGUID;
                        var productSettledTrans = productSettledTransByPurchaseLineGroupRefGUID.Where(w => w.RefGUID == purchaseLineGUID).FirstOrDefault();
                        c.LastSettleDate = productSettledTrans.SettledDate;
                        c.SettleAmount = c.SettleAmount + productSettledTrans.SumSettledInvoiceAmount;
                        c.SettleAmountMST = c.SettleAmountMST + (productSettledTrans.SumSettledInvoiceAmount * c.ExchangeRate);
                        c.CustTransStatus = c.TransAmount == c.SettleAmount ? (int)CustTransStatus.Closed : (int)CustTransStatus.Open;
                        return c;
                    }).ToList();
                    updateCustTranses.AddRange(custTransByPurchaseLine);
                    #endregion 1. UpdateCustTrans for PurchaseLine
                    #region 2. UpdateCustTrans for WithdrawalLine
                    // 2.1
                    var productSettledTransByWithdrawalLineGroupRefGUID = productSettledTransByWithdrawalLine.GroupBy(g => new { g.RefType, g.RefGUID }).Select(p =>
                    new
                    {
                        SumSettledInvoiceAmount = p.Sum(s => s.SettledInvoiceAmount),
                        RefGUID = p.Key.RefGUID,
                        RefType = p.Key.RefType,
                        SettledDate = p.Max(m => m.SettledDate)
                    }).ToList();

                    // 2.2
                    List<WithdrawalLine> withdrawalLinesByGroupRefGUID = withdrawalLineRepo.GetWithdrawalLineByIdNoTracking(productSettledTransByWithdrawalLineGroupRefGUID.Select(s => s.RefGUID.Value));
                    List<CustTrans> custTransByWithdrawalLine = custTransRepo.GetCustTransByInvoiceTableNoTracking(withdrawalLinesByGroupRefGUID.Select(s => s.WithdrawalLineInvoiceTableGUID.Value).ToList()).ToList();
                    custTransByWithdrawalLine.Select(c =>
                    {
                        Guid withdrawalLineGUID = withdrawalLinesByGroupRefGUID.Where(w => w.WithdrawalLineInvoiceTableGUID == c.InvoiceTableGUID).FirstOrDefault().WithdrawalLineGUID;
                        var productSettledTrans = productSettledTransByWithdrawalLineGroupRefGUID.Where(w => w.RefGUID == withdrawalLineGUID).FirstOrDefault();
                        c.LastSettleDate = productSettledTrans.SettledDate;
                        c.SettleAmount = c.SettleAmount + productSettledTrans.SumSettledInvoiceAmount;
                        c.SettleAmountMST = c.SettleAmountMST + (productSettledTrans.SumSettledInvoiceAmount * c.ExchangeRate);
                        c.CustTransStatus = c.TransAmount == c.SettleAmount ? (int)CustTransStatus.Closed : (int)CustTransStatus.Open;
                        return c;
                    }).ToList();
                    updateCustTranses.AddRange(custTransByWithdrawalLine);
                    #endregion 2. UpdateCustTrans for WithdrawalLine
                    #endregion UpdateCustTrans

                    #region Create PaymentHistory
                    IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                    List<PaymentHistory> createPaymentHistories = new List<PaymentHistory>();
                    ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByReceiptTempIdNoTracking("Migrate");
                    #region 1. Create PaymentHistory 1 record by 1 ProductSettledTrans for PurchaseLine
                    List<PaymentHistory> paymentHistoriesByPurchaseLine =
                        (from productSettledTrans in productSettledTransByPurchaseLine
                         join purchaseLine in purchaseLines
                         on productSettledTrans.RefGUID equals purchaseLine.PurchaseLineGUID
                         join custTrans in custTransByPurchaseLine
                         on purchaseLine.PurchaseLineInvoiceTableGUID equals custTrans.InvoiceTableGUID
                         select new PaymentHistory
                         {
                             RefType = (int)RefType.ReceiptTemp,
                             RefGUID = receiptTempTable.ReceiptTempTableGUID,
                             PaymentDate = productSettledTrans.SettledDate,
                             ReceivedDate = productSettledTrans.SettledDate,
                             CurrencyGUID = custTrans.CurrencyGUID,
                             ExchangeRate = custTrans.ExchangeRate,
                             ReceivedFrom = (int)ReceivedFrom.Customer,
                             CustomerTableGUID = custTrans.CustomerTableGUID,
                             CreditAppTableGUID = custTrans.CreditAppTableGUID,
                             ProductType = custTrans.ProductType,
                             InvoiceTableGUID = custTrans.InvoiceTableGUID,
                             DueDate = custTrans.DueDate,
                             PaymentBaseAmount = productSettledTrans.SettledInvoiceAmount,
                             PaymentTaxAmount = 0,
                             PaymentAmount = productSettledTrans.SettledInvoiceAmount,
                             WHTAmount = 0,
                             WHTSlipReceivedByCustomer = false,
                             PaymentBaseAmountMST = productSettledTrans.SettledInvoiceAmount * custTrans.ExchangeRate,
                             PaymentTaxAmountMST = 0,
                             PaymentAmountMST = productSettledTrans.SettledInvoiceAmount * custTrans.ExchangeRate,
                             WHTAmountMST = 0,
                             PaymentTaxBaseAmount = 0,
                             PaymentTaxBaseAmountMST = 0,
                             OrigTaxTableGUID = null,
                             WithholdingTaxTableGUID = null,
                             ReceiptTableGUID = null,
                             InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID,
                             CustTransGUID = custTrans.CustTransGUID,
                             PaymentHistoryGUID = Guid.NewGuid(),
                             CompanyGUID = productSettledTrans.CompanyGUID,
                             BranchGUID = custTrans.BranchGUID
                         }).ToList();
                    createPaymentHistories.AddRange(paymentHistoriesByPurchaseLine);
                    #endregion 1. Create PaymentHistory 1 record by 1 ProductSettledTrans for PurchaseLine
                    #region 2. Create PaymentHistory 1 record by 1 ProductSettledTrans for WithdrawalLine
                    List<PaymentHistory> paymentHistoriesByWithdrawalLine =
                        (from productSettledTrans in productSettledTransByWithdrawalLine
                         join withdrawalLine in withdrawalLines
                         on productSettledTrans.RefGUID equals withdrawalLine.WithdrawalLineGUID
                         join custTrans in custTransByWithdrawalLine
                         on withdrawalLine.WithdrawalLineInvoiceTableGUID equals custTrans.InvoiceTableGUID
                         select new PaymentHistory
                         {
                             RefType = (int)RefType.ReceiptTemp,
                             RefGUID = receiptTempTable.ReceiptTempTableGUID,
                             PaymentDate = productSettledTrans.SettledDate,
                             ReceivedDate = productSettledTrans.SettledDate,
                             CurrencyGUID = custTrans.CurrencyGUID,
                             ExchangeRate = custTrans.ExchangeRate,
                             ReceivedFrom = (int)ReceivedFrom.Customer,
                             CustomerTableGUID = custTrans.CustomerTableGUID,
                             CreditAppTableGUID = custTrans.CreditAppTableGUID,
                             ProductType = custTrans.ProductType,
                             InvoiceTableGUID = custTrans.InvoiceTableGUID,
                             DueDate = custTrans.DueDate,
                             PaymentBaseAmount = productSettledTrans.SettledInvoiceAmount,
                             PaymentTaxAmount = 0,
                             PaymentAmount = productSettledTrans.SettledInvoiceAmount,
                             WHTAmount = 0,
                             WHTSlipReceivedByCustomer = false,
                             PaymentBaseAmountMST = productSettledTrans.SettledInvoiceAmount * custTrans.ExchangeRate,
                             PaymentTaxAmountMST = 0,
                             PaymentAmountMST = productSettledTrans.SettledInvoiceAmount * custTrans.ExchangeRate,
                             WHTAmountMST = 0,
                             PaymentTaxBaseAmount = 0,
                             PaymentTaxBaseAmountMST = 0,
                             OrigTaxTableGUID = null,
                             WithholdingTaxTableGUID = null,
                             ReceiptTableGUID = null,
                             InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID,
                             CustTransGUID = custTrans.CustTransGUID,
                             PaymentHistoryGUID = Guid.NewGuid(),
                             CompanyGUID = productSettledTrans.CompanyGUID,
                             BranchGUID = custTrans.BranchGUID
                         }).ToList();
                    createPaymentHistories.AddRange(paymentHistoriesByWithdrawalLine);
                    #endregion 2. Create PaymentHistory 1 record by 1 ProductSettledTrans for WithdrawalLine
                    #endregion Create PaymentHistory

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(productSettledTranses, true);
                        this.BulkUpdate(updateCustTranses);
                        this.BulkInsert(createPaymentHistories, false);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Migration

    }
}
