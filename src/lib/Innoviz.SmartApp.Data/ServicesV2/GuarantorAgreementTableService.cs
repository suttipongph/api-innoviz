using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IGuarantorAgreementTableService
    {
        GuarantorAgreementLineAffiliateItemView GetGuarantorAgreementLineAffiliateById(string id);
        AccessModeView GetAccessModeByGuarantorAgreement(string guarantorId);
        GuarantorAgreementLineAffiliateItemView CreateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliateItemView guarantorAgreementLineView);
        GuarantorAgreementLineAffiliateItemView UpdateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliateItemView guarantorAgreementLineView);
        GuarantorAgreementLineItemView GetGuarantorAgreementLineByIdFromAffiliate(string id);
        bool DeleteGuarantorAgreementLineAffiliate(string id);
        GuarantorAgreementLineItemView GetGuarantorAgreementLineById(string id);
        GuarantorAgreementLineItemView CreateGuarantorAgreementLine(GuarantorAgreementLineItemView guarantorAgreementLineView);
        GuarantorAgreementLineItemView UpdateGuarantorAgreementLine(GuarantorAgreementLineItemView guarantorAgreementLineView);
        bool DeleteGuarantorAgreementLine(string id);
        GuarantorAgreementTableItemView GetCopyGuarantorFromCAById(string id);
        GuarantorAgreementTableItemView GetGuarantorAgreementTableById(string id);
        GuarantorAgreementTableItemView CreateGuarantorAgreementTable(GuarantorAgreementTableItemView guarantorAgreementTableView);
        GuarantorAgreementTableItemView UpdateGuarantorAgreementTable(GuarantorAgreementTableItemView guarantorAgreementTableView);
        GuarantorAgreementTable UpdateGuarantorAgreementTable(GuarantorAgreementTable guarantorAgreementTable);
        bool DeleteGuarantorAgreementTable(string id);

        public GuarantorAgreementLineAffiliateItemView GetGuarantorAgreementLineAffiliateInitialData(string id);
        public GuarantorAgreementTableItemView GetGuarantorAgreementTableInitialData(string mainAgreement);
        public GuarantorAgreementLineItemView GetGuarantorAgreementLineInitialData(string id);
        public bool IsManualByGuarantorRequest(int productType);
        public bool IsManualByInternalGuarantorRequest(int productType);
        public bool ValidateIsManualGuarantorNumberSeq(int productType);
        public IEnumerable<SelectItem<GuarantorTransItemView>> GuarantorTransDropDown(SearchParameter search);
        public IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItemGuarantorAgreementTable(SearchParameter search);
        
        public IEnumerable<SelectItem<NationalityItemView>> GetNationalityDropDown(SearchParameter search);
        public IEnumerable<SelectItem<RaceItemView>> GetRaceDropDown(SearchParameter search);
        public IEnumerable<SelectItem<MainAgreementTableItemView>> GetMainAgreementTableByGuarantorDropDown(SearchParameter search);
        public IEnumerable<SelectItem<CustomerTableItemView>> GetCustomerTableByMainAgreementDropDown(SearchParameter search);
        public IEnumerable<SelectItem<CustomerTableItemView>> GetCustomerTableByGuarantorIdDropDown(SearchParameter search);
        public IEnumerable<SelectItem<CreditAppTableItemView>> GetCreditAppTableByGuarantorLineIdDropDown(SearchParameter search);
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemGuarantorAgreementTableStatus(SearchParameter search);
        public AccessModeView GetAccessModeByGuarantorAgreementTable(string id);
        public AccessModeView GetAccessModeByGuarantorAgreementTableBookmarkDocumentTrans(string id);
        public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialDataByGuarantorAgreement(string refGUID);

        public GuarantorAgreementTableItemView GetGuarantorAgreementTableExtendById(string id);
        public GuarantorAgreementLineItemView CreateCopyGuarantorFromCA(GuarantorAgreementLineItemView model);
        bool GetGenerateNoticeOfCancellationValidation(string id);
        List<int> GetVisibleManageMenu(string guarantorAgreementTableGuid);
        GuarantorAgreementTableItemView GuarantorAgreementExtend(ExtendGuarantorAgreementView vmodel);

        CrossGuarantorAgreementBookmarkView GetCrossGuarantorAgreementBookmarkView(Guid refGUID);
        //BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement(Guid refGUID , Guid bookmarkDocumentTransGuid);
        BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_ThaiPerson(Guid refGUID, Guid bookmarkDocumentTransGuid);
        BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Foreigner(Guid refGUID, Guid bookmarkDocumentTransGuid);
        BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Organization(Guid refGUID, Guid bookmarkDocumentTransGuid);
        void CreateGuarantorAgreementTableCollection(IEnumerable<GuarantorAgreementTableItemView> guarantorAgreementTableItemView);
        void CreateGuarantorAgreementLineCollection(IEnumerable<GuarantorAgreementLineItemView> guarantorAgreementLineItemView);
        void CreateGuarantorAgreementLineAffiliateCollection(IEnumerable<GuarantorAgreementLineAffiliateItemView> guarantorAgreementLineAffiliateItemView);
        AccessModeView GetAccessModeByDocumentStatus(string id);
        AccessModeView GetGuarantorAgreementLineAccessMode(string id);
        List<int> GetVisibleManageMenuByGuarantor(string mainAgreementTableGuid);
        bool GetVisibleGenerateFunction(string id);

        #region extend GuarantorAgreementTable
        GuarantorAgreementTableItemView GetExtendGuarantorAgreementTableById(string id);
        bool GetGenerateExtendGuarantorAgreementValidation(string id);
        #endregion
    }
    public class GuarantorAgreementTableService : SmartAppService, IGuarantorAgreementTableService
    {
        public GuarantorAgreementTableService(SmartAppDbContext context) : base(context) { }
        public GuarantorAgreementTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public GuarantorAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public GuarantorAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public GuarantorAgreementTableService() : base() { }
        public GuarantorAgreementLineItemView GetGuarantorAgreementLineById(string id)
        {
            try
            {
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                return guarantorAgreementLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementLineItemView CreateCopyGuarantorFromCA(GuarantorAgreementLineItemView model)
        {
            IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
            IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
            var oldLineList = guarantorAgreementLineRepo.GetLineListByGuarantorAgreement(model.GuarantorAgreementTableGUID.StringToGuid());
            string userName = db.GetUserName();
            DateTime systemDate = DateTime.Now;
            List<GuarantorAgreementLine> guarantorLineListToCreate = new List<GuarantorAgreementLine>();
            var guarantorTransList = guarantorTransRepo.GetGuarantorTransListForCopyFromCA(model.CreditAppRequestTable_creditAppRequestTableGUID, (int)RefType.CreditAppRequestTable, null, false, model.Affiliate).ToList();

            SmartAppException ex = new SmartAppException("ERROR.ERROR");
            if (!guarantorTransList.Any())
            {
                ex.AddData("ERROR.90003");
            }
            if (ex.Data.Count > 0)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }


            if (guarantorTransList.Any())
            {
                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                foreach (GuarantorTransItemView item in guarantorTransList)
                {
                    GuarantorAgreementLine guaratorAgreementLineModel = new GuarantorAgreementLine();
                    guaratorAgreementLineModel.Ordering = item.Ordering;
                    guaratorAgreementLineModel.GuarantorTransGUID = item.GuarantorTransGUID.StringToGuid();
                    guaratorAgreementLineModel.GuarantorAgreementTableGUID = model.GuarantorAgreementTableGUID.StringToGuid();
                    guaratorAgreementLineModel.Name = item.RelatedPersonTable_Name;
                    guaratorAgreementLineModel.OperatedBy = item.RelatedPersonTable_OperatedBy;
                    guaratorAgreementLineModel.TaxId = item.RelatedPersonTable_TaxId;
                    guaratorAgreementLineModel.PassportId = item.RelatedPersonTable_PassportId;
                    guaratorAgreementLineModel.WorkPermitId = item.RelatedPersonTable_WorkPermitId;
                    guaratorAgreementLineModel.DateOfIssue = item.RelatedPersonTable_DateOfIssue.StringNullToDateNull();
                    guaratorAgreementLineModel.DateOfBirth = item.RelatedPersonTable_DateOfBirth.StringNullToDateNull();
                    guaratorAgreementLineModel.Age = relatedPersonTableService.CalcAge(item.RelatedPersonTable_DateOfBirth.StringNullToDateNull());
                    guaratorAgreementLineModel.NationalityGUID = item.RelatedPersonTable_NationalityGUID.StringToGuidNull();
                    guaratorAgreementLineModel.RaceGUID = item.RelatedPersonTable_RaceGUID.StringToGuidNull();
                    guaratorAgreementLineModel.PrimaryAddress1 = item.RelatedPersonTable_Address1;
                    guaratorAgreementLineModel.PrimaryAddress2 = item.RelatedPersonTable_Address2;
                    guaratorAgreementLineModel.MaximumGuaranteeAmount = item.MaximumGuaranteeAmount;
                    guaratorAgreementLineModel.CompanyGUID = model.CompanyGUID.StringToGuid();
                    guarantorLineListToCreate.Add(guaratorAgreementLineModel);
                }
       
            }
            using (var transaction = UnitOfWork.ContextTransaction())
            {
                BulkDelete(oldLineList);
                BulkInsert(guarantorLineListToCreate, true);
                UnitOfWork.Commit(transaction);
            }
            NotificationResponse success = new NotificationResponse();
            success.AddData("SUCCESS.90001", new string[] { "LABEL.GUARANTOR_AGREEMENT" });
            model.Notification = success;
            return model;
        }
        public int CalculateAgeCorrect2(string birthDate, DateTime now)
        {
            DateTime dateTimeBirth = birthDate.StringToDateTime();
            int age = now.Year - dateTimeBirth.Year;
            if (dateTimeBirth > now.AddYears(-age))
                age--;
            return age;
        }
        public GuarantorAgreementLineItemView CreateGuarantorAgreementLine(GuarantorAgreementLineItemView guarantorAgreementLineView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                guarantorAgreementLineView = accessLevelService.AssignOwnerBU(guarantorAgreementLineView);
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                GuarantorAgreementLine guarantorAgreementLine = guarantorAgreementLineView.ToGuarantorAgreementLine();
                guarantorAgreementLine = guarantorAgreementLineRepo.CreateGuarantorAgreementLine(guarantorAgreementLine);
                base.LogTransactionCreate<GuarantorAgreementLine>(guarantorAgreementLine);
                UnitOfWork.Commit();
                return guarantorAgreementLine.ToGuarantorAgreementLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementLineItemView UpdateGuarantorAgreementLine(GuarantorAgreementLineItemView guarantorAgreementLineView)
        {
            try
            {
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                GuarantorAgreementLine inputGuarantorAgreementLine = guarantorAgreementLineView.ToGuarantorAgreementLine();
                GuarantorAgreementLine dbGuarantorAgreementLine = guarantorAgreementLineRepo.Find(inputGuarantorAgreementLine.GuarantorAgreementLineGUID);
                dbGuarantorAgreementLine = guarantorAgreementLineRepo.UpdateGuarantorAgreementLine(dbGuarantorAgreementLine, inputGuarantorAgreementLine);
                base.LogTransactionUpdate<GuarantorAgreementLine>(GetOriginalValues<GuarantorAgreementLine>(dbGuarantorAgreementLine), dbGuarantorAgreementLine);
                UnitOfWork.Commit();
                return dbGuarantorAgreementLine.ToGuarantorAgreementLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteGuarantorAgreementLine(string item)
        {
            try
            {
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                Guid guarantorAgreementLineGUID = new Guid(item);
                GuarantorAgreementLine guarantorAgreementLine = guarantorAgreementLineRepo.Find(guarantorAgreementLineGUID);
                guarantorAgreementLineRepo.Remove(guarantorAgreementLine);
                base.LogTransactionDelete<GuarantorAgreementLine>(guarantorAgreementLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<MainAgreementTableItemView>> GetMainAgreementTableByGuarantorDropDown(SearchParameter search)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { MainAgreementCondition.ProductType, MainAgreementCondition.CustomerGUID, MainAgreementCondition.AgreementDocType });
                return mainAgreementTableRepo.GetDropDownItemByGuarantor(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CustomerTableItemView>> GetCustomerTableByMainAgreementDropDown(SearchParameter search)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                search = search.GetParentCondition(CustmerTableCondition.GuarantorAgreementGUID);
                return customerTableRepo.GetDropDownItemByGuarantor(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CustomerTableItemView>> GetCustomerTableByGuarantorIdDropDown(SearchParameter search)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                search = search.GetParentCondition(CustmerTableCondition.ParentCompanyGUID);
                return customerTableRepo.GetDropDownItemByGuarantorAfi(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppTableItemView>> GetCreditAppTableByGuarantorLineIdDropDown(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo ICreditAppTableRepo = new CreditAppTableRepo(db);
                search = search.GetParentCondition(new string[] { CreditAppTableCondition.ProductType, CreditAppTableCondition.CustomerTableGUID });
                return ICreditAppTableRepo.GetCreditAppTableByGuarantorLineIdDropDown(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<NationalityItemView>> GetNationalityDropDown(SearchParameter search)
        {
            try
            {
                INationalityRepo nationalityRepo = new NationalityRepo(db);
                return nationalityRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<RaceItemView>> GetRaceDropDown(SearchParameter search)
        {
            try
            {
                IRaceRepo raceRepo = new RaceRepo(db);
                return raceRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementLineAffiliateItemView GetGuarantorAgreementLineAffiliateById(string id)
        {
            try
            {
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                return guarantorAgreementLineAffiliateRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementLineAffiliateItemView CreateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliateItemView guarantorAgreementLineAffiliateView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                guarantorAgreementLineAffiliateView = accessLevelService.AssignOwnerBU(guarantorAgreementLineAffiliateView);
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineRepo = new GuarantorAgreementLineAffiliateRepo(db);
                GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate = guarantorAgreementLineAffiliateView.ToGuarantorAgreementLineAffiliate();
                guarantorAgreementLineAffiliate = guarantorAgreementLineRepo.CreateGuarantorAgreementLineAffiliate(guarantorAgreementLineAffiliate);
                base.LogTransactionCreate<GuarantorAgreementLineAffiliate>(guarantorAgreementLineAffiliate);
                UnitOfWork.Commit();
                return guarantorAgreementLineAffiliate.ToGuarantorAgreementLineAffiliateItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementLineAffiliateItemView UpdateGuarantorAgreementLineAffiliate(GuarantorAgreementLineAffiliateItemView guarantorAgreementLineAffiliateView)
        {
            try
            {
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                GuarantorAgreementLineAffiliate inputGuarantorAgreementLineAffiliate = guarantorAgreementLineAffiliateView.ToGuarantorAgreementLineAffiliate();
                GuarantorAgreementLineAffiliate dbGuarantorAgreementLineAffiliate = guarantorAgreementLineAffiliateRepo.Find(inputGuarantorAgreementLineAffiliate.GuarantorAgreementLineAffiliateGUID);
                dbGuarantorAgreementLineAffiliate = guarantorAgreementLineAffiliateRepo.UpdateGuarantorAgreementLineAffiliate(dbGuarantorAgreementLineAffiliate, inputGuarantorAgreementLineAffiliate);
                base.LogTransactionUpdate<GuarantorAgreementLineAffiliate>(GetOriginalValues<GuarantorAgreementLineAffiliate>(dbGuarantorAgreementLineAffiliate), dbGuarantorAgreementLineAffiliate);
                UnitOfWork.Commit();
                return dbGuarantorAgreementLineAffiliate.ToGuarantorAgreementLineAffiliateItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteGuarantorAgreementLineAffiliate(string item)
        {
            try
            {
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                Guid guarantorAgreementLineAffiliateGUID = new Guid(item);
                GuarantorAgreementLineAffiliate guarantorAgreementLineAffiliate = guarantorAgreementLineAffiliateRepo.Find(guarantorAgreementLineAffiliateGUID);
                guarantorAgreementLineAffiliateRepo.Remove(guarantorAgreementLineAffiliate);
                base.LogTransactionDelete<GuarantorAgreementLineAffiliate>(guarantorAgreementLineAffiliate);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public GuarantorAgreementTableItemView GetGuarantorAgreementTableById(string id)
        {
            try
            {
                IBookmarkDocumentRepo bookmarkDocument = new BookmarkDocumentRepo(db);

                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                return guarantorAgreementTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTableItemView GetGuarantorAgreementTableExtendById(string id)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var result = guarantorAgreementTableRepo.GetByIdvw(id.StringToGuid());
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public GuarantorAgreementTableItemView GetCopyGuarantorFromCAById(string id)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                var result = guarantorAgreementTableRepo.GetByIdvw(id.StringToGuid());
                result.HaveLine = guarantorAgreementLineRepo.IsLineRelatedById(id.StringToGuid());
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTableItemView CreateGuarantorAgreementTable(GuarantorAgreementTableItemView guarantorAgreementTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);

                guarantorAgreementTableView = accessLevelService.AssignOwnerBU(guarantorAgreementTableView);
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableView.ToGuarantorAgreementTable();
                GenInternalGuarantorAgreementNumberSeqCode(guarantorAgreementTable, guarantorAgreementTableView);
                guarantorAgreementTable = guarantorAgreementTableRepo.CreateGuarantorAgreementTable(guarantorAgreementTable);
                base.LogTransactionCreate<GuarantorAgreementTable>(guarantorAgreementTable);
                UnitOfWork.Commit();
                return guarantorAgreementTable.ToGuarantorAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTableItemView UpdateGuarantorAgreementTable(GuarantorAgreementTableItemView guarantorAgreementTableView)
        {
            try
            {
                GuarantorAgreementTable inputGuarantorAgreementTable = guarantorAgreementTableView.ToGuarantorAgreementTable();
                inputGuarantorAgreementTable = UpdateGuarantorAgreementTable(inputGuarantorAgreementTable);
                UnitOfWork.Commit();
                return inputGuarantorAgreementTable.ToGuarantorAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTable UpdateGuarantorAgreementTable(GuarantorAgreementTable guarantorAgreementTable)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                GuarantorAgreementTable dbGuarantorAgreementTable = guarantorAgreementTableRepo.Find(guarantorAgreementTable.GuarantorAgreementTableGUID);
                dbGuarantorAgreementTable = guarantorAgreementTableRepo.UpdateGuarantorAgreementTable(dbGuarantorAgreementTable, guarantorAgreementTable);
                base.LogTransactionUpdate<GuarantorAgreementTable>(GetOriginalValues<GuarantorAgreementTable>(dbGuarantorAgreementTable), dbGuarantorAgreementTable);
                return dbGuarantorAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteGuarantorAgreementTable(string item)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                Guid guarantorAgreementTableGUID = new Guid(item);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.Find(guarantorAgreementTableGUID);
                guarantorAgreementTableRepo.Remove(guarantorAgreementTable);
                base.LogTransactionDelete<GuarantorAgreementTable>(guarantorAgreementTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementLineItemView GetGuarantorAgreementLineByIdFromAffiliate(string id)
        {
            try
            {
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                return guarantorAgreementLineRepo.GetByIdvwFormAffiliate(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public GuarantorAgreementLineAffiliateItemView GetGuarantorAgreementLineAffiliateInitialData(string id)
        {
            try
            {
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                var guarantorAgreementLineAffiliateModel = guarantorAgreementLineAffiliateRepo.GetGuarantorAgreementLineAffiliateInitialData(id.StringToGuid());
                var orderingModel = guarantorAgreementLineAffiliateRepo.GetOrderingFromGuaratorAgreement(id.StringToGuid());
                if (orderingModel != null)
                {
                    guarantorAgreementLineAffiliateModel.Ordering = orderingModel.Ordering+1;

                }
                else
                {
                    guarantorAgreementLineAffiliateModel.Ordering = 1;
                }

                return guarantorAgreementLineAffiliateModel;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTableItemView GetGuarantorAgreementTableInitialData(string id)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantor = new GuarantorAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Draft).ToString());
                var dataInitial = guarantor.GetGuarantorAgreementTableInitialData(id.StringToGuid());
                dataInitial.DocumentStatus_Values = documentStatus.Description;
                dataInitial.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                dataInitial.DocumentStatus_StatusId = documentStatus.StatusId;
                return dataInitial;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementLineItemView GetGuarantorAgreementLineInitialData(string id)
        {
            try
            {
                IGuarantorAgreementLineRepo guarantor = new GuarantorAgreementLineRepo(db);
                return guarantor.GetGuarantorAgreementLineInitialData(id.StringToGuid());

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByCreditAppRequest(int productType)
        {
            try
            {
                Guid companyGUID = GetCurrentCompany().StringToGuid();
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalGuarantorAgreementNumberSeqGUID(companyGUID, productType);
                return numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateIsManualGuarantorNumberSeq(int productType)
        {
            try
            {
                Guid companyGUID = GetCurrentCompany().StringToGuid();
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetGuarantorAgreementNumberSeqGUID(companyGUID, productType);
                return numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByGuarantorRequest(int productType)
        {
            try
            {
                Guid companyGUID = GetCurrentCompany().StringToGuid();
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetGuarantorAgreementNumberSeqGUID(companyGUID, productType);
                return numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByInternalGuarantorRequest(int productType)
        {
            try
            {
                Guid companyGUID = GetCurrentCompany().StringToGuid();
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalGuarantorAgreementNumberSeqGUID(companyGUID, productType);
                return numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenInternalGuarantorAgreementNumberSeqCode(GuarantorAgreementTable guarantorAgreementTable, GuarantorAgreementTableItemView guerantorAgreementItemView)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByInternalGuarantorRequest(guerantorAgreementItemView.creditApp_ProductType);
                if (!isManual)
                {
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalGuarantorAgreementNumberSeqGUID(guerantorAgreementItemView.CompanyGUID.StringToGuid(), guerantorAgreementItemView.creditApp_ProductType);
                    guarantorAgreementTable.InternalGuarantorAgreementId = numberSequenceService.GetNumber(guerantorAgreementItemView.CompanyGUID.StringToGuid(), null, numberSeqTableGUID);
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void GenGuarantorAgreementNumberSeqCode(GuarantorAgreementTable guarantorAgreementTable, GuarantorAgreementTableItemView guerantorAgreementItemView)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByGuarantorRequest(guerantorAgreementItemView.creditApp_ProductType);
                if (!isManual)
                {
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetGuarantorAgreementNumberSeqGUID(guerantorAgreementItemView.CompanyGUID.StringToGuid(), guerantorAgreementItemView.creditApp_ProductType);
                    guarantorAgreementTable.GuarantorAgreementId = numberSequenceService.GetNumber(guerantorAgreementItemView.CompanyGUID.StringToGuid(), null, numberSeqTableGUID);
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public IEnumerable<SelectItem<GuarantorTransItemView>> GuarantorTransDropDown(SearchParameter search)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                search = search.GetParentCondition(GuarantorTransTableCondition.RefGUID);
                return guarantorTransRepo.GetDropDownItemWithPerson(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByGuarantorAgreementTable(string id)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                string documentStatus = GetStatusIdByGuarantorAgreement(id);
                bool isLessThanSigned = (Convert.ToInt32(documentStatus) < (int)GuarantorAgreementStatus.Signed);
                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = isLessThanSigned;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByGuarantorAgreementTableBookmarkDocumentTrans(string id)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                string documentStatus = GetStatusIdByGuarantorAgreement(id);
                bool isLessThanSigned = (Convert.ToInt32(documentStatus) <= (int)GuarantorAgreementStatus.Signed);
                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = isLessThanSigned;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public string GetStatusIdByGuarantorAgreement(string id)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(id.StringToGuid());
                return documentStatusRepo.GetDocumentStatusByIdNoTracking((Guid)(guarantorAgreementTable.DocumentStatusGUID ?? null)).StatusId;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialDataByGuarantorAgreement(string refGUID)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                string refId = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalGuarantorAgreementId;
                return bookmarkDocumentTransService.GetBookmarkDocumentTransInitialData(refId, refGUID, Models.Enum.RefType.GuarantorAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTableItemView GetMemoTransInitialData(string id)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantor = new GuarantorAgreementTableRepo(db);
                return guarantor.GetGuarantorAgreementTableInitialData(id.StringToGuid());

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<int> GetVisibleManageMenu(string guarantorAgreementTableGuid)
        {
            try
            {
                List<int> result = new List<int>();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(guarantorAgreementTableGuid.StringToGuid());
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(guarantorAgreementTable.DocumentStatusGUID.Value);
                DocumentStatus completedBookmarkTrans = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.GuarantorAgreement, guarantorAgreementTableGuid.StringToGuid());
                if ((int)GuarantorAgreementStatus.Draft == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Post);
                }
                if ((int)GuarantorAgreementStatus.Posted == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Send);
                }
                if ((int)GuarantorAgreementStatus.Sent == int.Parse(documentStatus.StatusId) && bookmarkDocumentTrans.All(a => a.DocumentStatusGUID == completedBookmarkTrans.DocumentStatusGUID))
                {
                    result.Add((int)ManageAgreementAction.Sign);
                }
                if ((int)GuarantorAgreementStatus.Signed == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Close);
                }
                if (int.Parse(documentStatus.StatusId) < (int)GuarantorAgreementStatus.Signed)
                {
                    result.Add((int)ManageAgreementAction.Cancel);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetGenerateNoticeOfCancellationValidation(string id)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(id.StringToGuid());
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Signed).ToString());
                DocumentStatusItemView documentStatus = documentStatusRepo.GetByIdvw((Guid)guarantorAgreementTable.DocumentStatusGUID);
                int newStatus = ((int)AgreementDocType.New);
                int extendStatus = ((int)AgreementDocType.Extend);
                if (guarantorAgreementTable.AgreementDocType != newStatus)
                {
                    ex.AddData("ERROR.90029");
                }
                if (guarantorAgreementTable.AgreementDocType == extendStatus && Int32.Parse(documentStatus.StatusId) < (int)GuarantorAgreementStatus.Signed)
                {
                    ex.AddData("ERROR.90051", new string[] { SmartAppUtil.GetDropDownLabel(guarantorAgreementTable.InternalGuarantorAgreementId, guarantorAgreementTable.Description) });

                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #region extend GuarantorAgreementTable
        public GuarantorAgreementTableItemView GetExtendGuarantorAgreementTableById(string id)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var result = guarantorAgreementTableRepo.GetByIdvw(id.StringToGuid());
                result.AgreementDocType = (int)AgreementDocType.Extend;

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Signed).ToString());
                result.AgreementExtension = guarantorAgreementTableRepo.GetListExtendGuarantorAgreementByGuarantorAgreementTableGUID(id.StringToGuid())
                                                                       .Where(w => w.DocumentStatusGUID == signStatus.DocumentStatusGUID)
                                                                       .OrderByDescending(o => o.AgreementExtension).Select(s => s.AgreementExtension).FirstOrDefault() + 1;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetGenerateExtendGuarantorAgreementValidation(string id)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                List<DocumentStatus> guarantorStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.GuarantorAgreement).ToString()).ToList();
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(id.StringToGuid());

                DocumentStatus signStatus = guarantorStatus.Where(w => w.StatusId == ((int)GuarantorAgreementStatus.Signed).ToString()).FirstOrDefault();
                List<GuarantorAgreementTable> extendGuarantorAgreementTables = guarantorAgreementTableRepo.GetListExtendGuarantorAgreementByGuarantorAgreementTableGUID(guarantorAgreementTable.GuarantorAgreementTableGUID);

                var Condition3 = (from d_guarantorAgreementTable in extendGuarantorAgreementTables
                                  join d_documentStatus in guarantorStatus
                                  on d_guarantorAgreementTable.DocumentStatusGUID equals d_documentStatus.DocumentStatusGUID into ljd_documentStatus
                                  from d_documentStatus in ljd_documentStatus.DefaultIfEmpty()
                                  where Convert.ToInt32(d_documentStatus.StatusId) < (int)(GuarantorAgreementStatus.Signed)
                                  select d_guarantorAgreementTable
                              ).FirstOrDefault();

                if (guarantorAgreementTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.GUARANTOR_AGREEMENT" });
                }
                if (guarantorAgreementTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                if (Condition3 != null)
                {
                    ex.AddData("ERROR.90051", new string[] { SmartAppUtil.GetDropDownLabel(guarantorAgreementTable.InternalGuarantorAgreementId, guarantorAgreementTable.Description) });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorAgreementTableItemView GuarantorAgreementExtend(ExtendGuarantorAgreementView vmodel)
        {
            try
            {
                GuarantorAgreementTableItemView guarantorAgreementTableItemView = new GuarantorAgreementTableItemView();
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(vmodel.GuarantorAgreementTableGUID.StringToGuid());
                if (GetGenerateExtendGuarantorAgreementValidation(vmodel.GuarantorAgreementTableGUID))
                {
                    IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                    List<GuarantorAgreementLine> guarantorAgreementLines = guarantorAgreementLineRepo.GetLineListByGuarantorAgreement(guarantorAgreementTable.GuarantorAgreementTableGUID);
                    GuarantorAgreementTable extendGuarantorAgreement = GuarantorAgreementExtend_CreateGuarantorAgreementTable(vmodel, guarantorAgreementTable);
                    List<GuarantorAgreementLine> extendGuaranTorAgreementLines = GuarantorAgreementExtend_CreateGuarantorAgreementLines(extendGuarantorAgreement, guarantorAgreementLines);
                    List<GuarantorAgreementLineAffiliate> extendGuaranTorAgreementLineAffiliates = GuarantorAgreementExtend_CreateGuarantorAgreementLineAffiliate(extendGuaranTorAgreementLines, guarantorAgreementLines);

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(new List<GuarantorAgreementTable>() { extendGuarantorAgreement });
                        this.BulkInsert(extendGuaranTorAgreementLines);
                        this.BulkInsert(extendGuaranTorAgreementLineAffiliates);
                        UnitOfWork.Commit(transaction);
                    }

                    guarantorAgreementTableItemView = extendGuarantorAgreement.ToGuarantorAgreementTableItemView();
                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.90019", new string[] { (AgreementDocType.Extend).GetAttrCode(),"LABEL.GUARANTOR_AGREEMENT",guarantorAgreementTable.GuarantorAgreementId });
                    guarantorAgreementTableItemView.Notification = success; 
                }

                return guarantorAgreementTableItemView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private GuarantorAgreementTable GuarantorAgreementExtend_CreateGuarantorAgreementTable(ExtendGuarantorAgreementView vmodel, GuarantorAgreementTable guarantorAgreementTable)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus draftStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Draft).ToString());
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(guarantorAgreementTable.MainAgreementTableGUID);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(mainAgreementTable.CreditAppTableGUID);
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                ProductSubType productSubType = productSubTypeRepo.GetProductSubTypeByIdNoTracking(creditAppTable.ProductSubTypeGUID);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                bool isManual = IsManualByInternalGuarantorRequest(mainAgreementTable.ProductType);
                string internalGuarantorAgreementId = vmodel.ExtendInternalGuarantorAgreementId;
                if (!isManual)
                {
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalGuarantorAgreementNumberSeqGUID(guarantorAgreementTable.CompanyGUID, mainAgreementTable.ProductType);
                    internalGuarantorAgreementId = numberSequenceService.GetNumber(guarantorAgreementTable.CompanyGUID, null, numberSeqTableGUID);
                }

                GuarantorAgreementTable extendGuarantorAgreement = new GuarantorAgreementTable()
                {
                    GuarantorAgreementTableGUID = Guid.NewGuid(),
                    MainAgreementTableGUID = guarantorAgreementTable.MainAgreementTableGUID,
                    AgreementDocType = vmodel.AgreementDocType,
                    Description = vmodel.NewDescription,
                    AgreementDate = vmodel.NewAgreementDate.StringToDate(),
                    AgreementYear = productSubType.GuarantorAgreementYear,
                    ExpiryDate = (vmodel.NewAgreementDate.StringToDate().AddYears(productSubType.GuarantorAgreementYear).AddDays(-1)),
                    SigningDate = null,
                    DocumentStatusGUID = draftStatus.DocumentStatusGUID,
                    DocumentReasonGUID = vmodel.DocumentReasonGUID.StringToGuidNull(),
                    Affiliate = guarantorAgreementTable.Affiliate,
                    ParentCompanyGUID = guarantorAgreementTable.ParentCompanyGUID,
                    InternalGuarantorAgreementId = internalGuarantorAgreementId,
                    GuarantorAgreementId = String.Empty,
                    AgreementExtension = vmodel.AgreementExtension,
                    WitnessCustomer1 = guarantorAgreementTable.WitnessCustomer1,
                    WitnessCustomer2 = guarantorAgreementTable.WitnessCustomer2,
                    WitnessCompany1GUID = guarantorAgreementTable.WitnessCompany1GUID,
                    WitnessCompany2GUID = guarantorAgreementTable.WitnessCompany2GUID,
                    RefGuarantorAgreementTableGUID = guarantorAgreementTable.GuarantorAgreementTableGUID,
                    Remark = vmodel.ReasonRemark,
                    CompanyGUID = guarantorAgreementTable.CompanyGUID
                };

                return extendGuarantorAgreement;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<GuarantorAgreementLine> GuarantorAgreementExtend_CreateGuarantorAgreementLines(GuarantorAgreementTable extendGuarantorAgreement, List<GuarantorAgreementLine> guarantorAgreementLines)
        {
            try
            {
                List<GuarantorAgreementLine> extendGuarantorAgreementLines = guarantorAgreementLines.Select(s => new GuarantorAgreementLine
                {
                    GuarantorAgreementLineGUID = Guid.NewGuid(),
                    GuarantorAgreementTableGUID = extendGuarantorAgreement.GuarantorAgreementTableGUID,
                    Ordering = s.Ordering,
                    GuarantorTransGUID = s.GuarantorTransGUID,
                    Name = s.Name,
                    TaxId = s.TaxId,
                    PassportId = s.PassportId,
                    WorkPermitId = s.WorkPermitId,
                    DateOfIssue = s.DateOfIssue,
                    MaximumGuaranteeAmount = s.MaximumGuaranteeAmount,
                    CustomerTableGUID = s.CustomerTableGUID,
                    CreditAppTableGUID = s.CreditAppTableGUID,
                    ApprovedCreditLimit = s.ApprovedCreditLimit,
                    DateOfBirth = s.DateOfBirth,
                    Age = s.Age,
                    NationalityGUID = s.NationalityGUID,
                    RaceGUID = s.RaceGUID,
                    PrimaryAddress1 = s.PrimaryAddress1,
                    PrimaryAddress2 = s.PrimaryAddress2,
                    CompanyGUID = extendGuarantorAgreement.CompanyGUID
                }).ToList();

                return extendGuarantorAgreementLines;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<GuarantorAgreementLineAffiliate> GuarantorAgreementExtend_CreateGuarantorAgreementLineAffiliate(List<GuarantorAgreementLine> extendGuaranTorAgreementLines, List<GuarantorAgreementLine> guarantorAgreementLines)
        {
            try
            {
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                List<GuarantorAgreementLineAffiliate> guarantorAgreementLineAffiliates = guarantorAgreementLineAffiliateRepo.GetListByGuarantorAgreementLine(guarantorAgreementLines.Select(s => s.GuarantorAgreementLineGUID).ToList());
                List<GuarantorAgreementLineAffiliate> extendGuarantorAgreementLineAffiliates = (from aff in guarantorAgreementLineAffiliates
                                                                                                join line in guarantorAgreementLines
                                                                                                on aff.GuarantorAgreementLineGUID equals line.GuarantorAgreementLineGUID
                                                                                                join extendLine in extendGuaranTorAgreementLines
                                                                                                on line.Ordering equals extendLine.Ordering
                                                                                                select new GuarantorAgreementLineAffiliate
                                                                                                {
                                                                                                    GuarantorAgreementLineAffiliateGUID = Guid.NewGuid(),
                                                                                                    GuarantorAgreementLineGUID = extendLine.GuarantorAgreementLineGUID,
                                                                                                    Ordering = aff.Ordering,
                                                                                                    CustomerTableGUID = aff.CustomerTableGUID,
                                                                                                    MainAgreementTableGUID = aff.MainAgreementTableGUID,
                                                                                                    CompanyGUID = extendLine.CompanyGUID
                                                                                                }).ToList();

                return extendGuarantorAgreementLineAffiliates;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public CrossGuarantorAgreementBookmarkView GetCrossGuarantorAgreementBookmarkView(Guid refGUID)
        {
            IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
            CrossGuarantorAgreementBookmarkView crossGuarantorAgreementView = guarantorAgreementTableRepo.GetCrossGuarantorAgreementBookmarkView(refGUID);
            ISharedService sharedService = new SharedService(db);
            if (crossGuarantorAgreementView != null)
            {
                crossGuarantorAgreementView.MaxGuaranteeTxt1st_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt1st_1);
                crossGuarantorAgreementView.MaxGuaranteeTxt2nd_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt2nd_1);
                crossGuarantorAgreementView.MaxGuaranteeTxt3rd_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt3rd_1);
                crossGuarantorAgreementView.MaxGuaranteeTxt4th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt4th_1);
                crossGuarantorAgreementView.MaxGuaranteeTxt5th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt5th_1);
                crossGuarantorAgreementView.MaxGuaranteeTxt6th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt6th_1);
                crossGuarantorAgreementView.MaxGuaranteeTxt7th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt7th_1);
                crossGuarantorAgreementView.MaxGuaranteeTxt8th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.MaxGuaranteeAmt8th_1);

                crossGuarantorAgreementView.ApprovedCreditLimitTxt1st_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt1st_1);
                crossGuarantorAgreementView.ApprovedCreditLimitTxt2nd_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt2nd_1);
                crossGuarantorAgreementView.ApprovedCreditLimitTxt3rd_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt3rd_1);
                crossGuarantorAgreementView.ApprovedCreditLimitTxt4th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt4th_1);
                crossGuarantorAgreementView.ApprovedCreditLimitTxt5th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt5th_1);
                crossGuarantorAgreementView.ApprovedCreditLimitTxt6th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt6th_1);
                crossGuarantorAgreementView.ApprovedCreditLimitTxt7th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt7th_1);
                crossGuarantorAgreementView.ApprovedCreditLimitTxt8th_1 =
                    sharedService.NumberToWordsTH(crossGuarantorAgreementView.ApprovedCreditLimitAmt8th_1);

                crossGuarantorAgreementView.SumLineAffliliate1st = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate1st, "");
                crossGuarantorAgreementView.SumLineAffliliate2nd = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate2nd, "");
                crossGuarantorAgreementView.SumLineAffliliate3rd = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate3rd, "");
                crossGuarantorAgreementView.SumLineAffliliate4th = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate4th, "");
                crossGuarantorAgreementView.SumLineAffliliate5th = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate5th, "");
                crossGuarantorAgreementView.SumLineAffliliate6th = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate6th, "");
                crossGuarantorAgreementView.SumLineAffliliate7th = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate7th, "");
                crossGuarantorAgreementView.SumLineAffliliate8th = sharedService.NumberToWordsTH(crossGuarantorAgreementView.QSumLineAffliliate8th, "");

                crossGuarantorAgreementView.GuarantorAgreementDate1 = crossGuarantorAgreementView.qGuarantorAgreementDate1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorAgreementDate2 = crossGuarantorAgreementView.qGuarantorAgreementDate2 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate2.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorAgreementDate3 = crossGuarantorAgreementView.qGuarantorAgreementDate3 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate3.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorAgreementDate4 = crossGuarantorAgreementView.qGuarantorAgreementDate4 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate4.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorAgreementDate5 = crossGuarantorAgreementView.qGuarantorAgreementDate5 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate5.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorAgreementDate6 = crossGuarantorAgreementView.qGuarantorAgreementDate6 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate6.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorAgreementDate7 = crossGuarantorAgreementView.qGuarantorAgreementDate7 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate7.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorAgreementDate8 = crossGuarantorAgreementView.qGuarantorAgreementDate8 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorAgreementDate8.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.GuarantorDateregis1st_1 = crossGuarantorAgreementView.qGuarantorDateregis1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorDateregis2nd_1 = crossGuarantorAgreementView.qGuarantorDateregis2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorDateregis3rd_1 = crossGuarantorAgreementView.qGuarantorDateregis3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorDateregis4th_1 = crossGuarantorAgreementView.qGuarantorDateregis4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorDateregis5th_1 = crossGuarantorAgreementView.qGuarantorDateregis5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorDateregis6th_1 = crossGuarantorAgreementView.qGuarantorDateregis6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorDateregis7th_1 = crossGuarantorAgreementView.qGuarantorDateregis7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.GuarantorDateregis8th_1 = crossGuarantorAgreementView.qGuarantorDateregis8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qGuarantorDateregis8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of1st_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of1st_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of1st_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of2nd_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of2nd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of2nd_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of3rd_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of3rd_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of3rd_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of4th_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of4th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of4th_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of5th_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of5th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of5th_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of6th_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of6th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of6th_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of7th_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of7th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of7th_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                crossGuarantorAgreementView.RelatedContractDate1st_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate1st_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate1st_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate2nd_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate2nd_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate2nd_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate3rd_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate3rd_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate3rd_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate4th_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate4th_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate4th_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate5th_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate5th_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate5th_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate6th_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate6th_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate6th_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate7th_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate7th_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate7th_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";
                crossGuarantorAgreementView.RelatedContractDate8th_Of8th_1 = crossGuarantorAgreementView.qRelatedContractDate8th_Of8th_1 != null ? sharedService.DateToWord(crossGuarantorAgreementView.qRelatedContractDate8th_Of8th_1.Value, TextConstants.TH, TextConstants.DMY) : "";

                IRelatedPersonTableService relatedPersonTableService = new RelatedPersonTableService(db);
                crossGuarantorAgreementView.GuarantorAge1st_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth1st_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth1st_1).ToString() : "0";
                crossGuarantorAgreementView.GuarantorAge2nd_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth2nd_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth2nd_1).ToString() : "0";
                crossGuarantorAgreementView.GuarantorAge3rd_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth3rd_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth3rd_1).ToString() : "0";
                crossGuarantorAgreementView.GuarantorAge4th_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth4th_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth4th_1).ToString() : "0";
                crossGuarantorAgreementView.GuarantorAge5th_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth5th_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth5th_1).ToString() : "0";
                crossGuarantorAgreementView.GuarantorAge6th_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth6th_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth6th_1).ToString() : "0";
                crossGuarantorAgreementView.GuarantorAge7th_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth7th_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth7th_1).ToString() : "0";
                crossGuarantorAgreementView.GuarantorAge8th_1 = crossGuarantorAgreementView.qGuarantorDateOfBirth8th_1 != null ? relatedPersonTableService.CalcAge(crossGuarantorAgreementView.qGuarantorDateOfBirth8th_1).ToString() : "0";
            }

            return crossGuarantorAgreementView;
        }
        public AccessModeView GetAccessModeByGuarantorAgreement(string guarantorGUID)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var guarantorModel = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTrackingByAccessLevel(guarantorGUID.StringToGuid());
                AccessModeView accessModeView = new AccessModeView();
                bool isAffliate = guarantorModel.Affiliate;
                int agreementDoctype = guarantorModel.AgreementDocType;
                var documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking((Guid)guarantorModel.DocumentStatusGUID);
                accessModeView.CanCreate = isAffliate&&agreementDoctype==(int)AgreementDocType.New;
                accessModeView.CanView = true;
                accessModeView.CanDelete = Int32.Parse(documentStatus.StatusId)  < (int)GuarantorAgreementStatus.Signed;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_ThaiPerson(Guid refGUID , Guid bookmarkDocumentTransGuid)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                BookmaskDocumentGuarantorAgreement result = guarantorAgreementTableRepo.GetBookmaskDocumentGuarantorAgreement_ThaiPerson(refGUID, bookmarkDocumentTransGuid);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Foreigner(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                BookmaskDocumentGuarantorAgreement result = guarantorAgreementTableRepo.GetBookmaskDocumentGuarantorAgreement_Foreigner(refGUID, bookmarkDocumentTransGuid);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public BookmaskDocumentGuarantorAgreement GetBookmaskDocumentGuarantorAgreement_Organization(Guid refGUID, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                BookmaskDocumentGuarantorAgreement result = guarantorAgreementTableRepo.GetBookmaskDocumentGuarantorAgreement_Organization(refGUID, bookmarkDocumentTransGuid);
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public List<int> GetVisibleManageMenuByGuarantor(string guarantorAgreementGUID)
        {
            try
            {
                List<int> result = new List<int>();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(guarantorAgreementGUID.StringToGuid());

                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking((Guid)guarantorAgreementTable.DocumentStatusGUID);
                DocumentStatus completedBookmarkTrans = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.GuarantorAgreement, guarantorAgreementGUID.StringToGuid());
                if ((int)GuarantorAgreementStatus.Draft == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Post);
                }
                if ((int)GuarantorAgreementStatus.Posted == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Send);
                }
                if ((int)GuarantorAgreementStatus.Sent == int.Parse(documentStatus.StatusId) && bookmarkDocumentTrans.All(a => a.DocumentStatusGUID == completedBookmarkTrans.DocumentStatusGUID))
                {
                    result.Add((int)ManageAgreementAction.Sign);
                }
                if ((int)GuarantorAgreementStatus.Signed == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Close);
                }
                if (int.Parse(documentStatus.StatusId) < (int)GuarantorAgreementStatus.Signed)
                {
                    result.Add((int)ManageAgreementAction.Cancel);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemGuarantorAgreementTableStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.GuarantorAgreement);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Migration
        public void CreateGuarantorAgreementTableCollection(IEnumerable<GuarantorAgreementTableItemView> guarantorAgreementTableItemView)
        {
            try
            {
                if (guarantorAgreementTableItemView.Any())
                {
                    IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                    List<GuarantorAgreementTable> guarantorAgreementTableItems = guarantorAgreementTableItemView.ToGuarantorAgreementTable().ToList();
                    guarantorAgreementTableRepo.ValidateAdd(guarantorAgreementTableItems);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(guarantorAgreementTableItems, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateGuarantorAgreementLineCollection(IEnumerable<GuarantorAgreementLineItemView> guarantorAgreementLineItemView)
        {
            try
            {
                if (guarantorAgreementLineItemView.Any())
                {
                    IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                    List<GuarantorAgreementLine> guarantorAgreementLineItems = guarantorAgreementLineItemView.ToGuarantorAgreementLine().ToList();
                    guarantorAgreementLineRepo.ValidateAdd(guarantorAgreementLineItems);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(guarantorAgreementLineItems, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateGuarantorAgreementLineAffiliateCollection(IEnumerable<GuarantorAgreementLineAffiliateItemView> guarantorAgreementLineAffiliateItemView)
        {
            try
            {
                if (guarantorAgreementLineAffiliateItemView.Any())
                {
                    IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                    List<GuarantorAgreementLineAffiliate> guarantorAgreementLineAffiliates = guarantorAgreementLineAffiliateItemView.ToGuarantorAgreementLineAffiliate().ToList();
                    guarantorAgreementLineAffiliateRepo.ValidateAdd(guarantorAgreementLineAffiliates);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(guarantorAgreementLineAffiliates, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AccessModeView GetAccessModeByDocumentStatus(string id)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementTableItemView mainAgreementTableItemView = mainAgreementTableRepo.GetByIdvw(id.StringToGuid());
                if (Int32.Parse(mainAgreementTableItemView.DocumentStatus_StatusId) <= (int)MainAgreementStatus.Signed)
                {
                    accessModeView.CanCreate = true;
              
                }
                else
                {
                    accessModeView.CanCreate = false;
           
                }
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AccessModeView GetGuarantorAgreementLineAccessMode(string id)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                var guarantorAgreementModel = guarantorAgreementTableRepo.GetByIdvw(id.StringToGuid());
                DocumentStatus documentStatusModel = null;
                if (guarantorAgreementModel.DocumentStatusGUID != null)
                {
                    documentStatusModel = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(guarantorAgreementModel.DocumentStatusGUID.StringToGuid());
                    if(documentStatusModel != null)
                    {
                        if (Int32.Parse(documentStatusModel.StatusId) < (int)GuarantorAgreementStatus.Signed)
                        {
                            accessModeView.CanCreate = true;
                            accessModeView.CanDelete = true;
                            accessModeView.CanView = true;
                            return accessModeView;
                        }
                    }

                }
                else
                {
                    accessModeView.CanCreate = false;
                    accessModeView.CanDelete = false;
                    accessModeView.CanView = false;
                }
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItemGuarantorAgreementTable(SearchParameter search)
        {
            try
            {
                search = search.GetParentCondition(DocumentReasonCondition.RefType);
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return sysDropDownService.GetDropDownItemDocumentReason(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetVisibleGenerateFunction(string id)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                GuarantorAgreementTable guarantorAgreementTableModel = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(id.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatusModel = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking((Guid)guarantorAgreementTableModel.DocumentStatusGUID);
                if(documentStatusModel.StatusId == ((int)GuarantorAgreementStatus.Signed).ToString()&& guarantorAgreementTableModel.AgreementDocType == (int)AgreementDocType.New)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Migration
    }
}
