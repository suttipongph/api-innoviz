using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IIntercompanyInvoiceAdjustmentService
	{

		IntercompanyInvoiceAdjustmentItemView GetIntercompanyInvoiceAdjustmentById(string id);
   
		IntercompanyInvoiceTableItemView GetAdjustIntercompanyInvoiceById(string id);
		IntercompanyInvoiceAdjustmentItemView CreateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentView);
		IntercompanyInvoiceAdjustmentItemView UpdateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentView);
		bool DeleteIntercompanyInvoiceAdjustment(string id);
		IntercompanyInvoiceAdjustmentItemView UpdateAdjustIntercompanyInvoice(IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentItemView);
	}
	public class IntercompanyInvoiceAdjustmentService : SmartAppService, IIntercompanyInvoiceAdjustmentService
	{
		public IntercompanyInvoiceAdjustmentService(SmartAppDbContext context) : base(context) { }
		public IntercompanyInvoiceAdjustmentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public IntercompanyInvoiceAdjustmentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntercompanyInvoiceAdjustmentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public IntercompanyInvoiceAdjustmentService() : base() { }

		public IntercompanyInvoiceAdjustmentItemView GetIntercompanyInvoiceAdjustmentById(string id)
		{
			try
			{
				IIntercompanyInvoiceAdjustmentRepo intercompanyInvoiceAdjustmentRepo = new IntercompanyInvoiceAdjustmentRepo(db);
				return intercompanyInvoiceAdjustmentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceAdjustmentItemView CreateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				intercompanyInvoiceAdjustmentView = accessLevelService.AssignOwnerBU(intercompanyInvoiceAdjustmentView);
				IIntercompanyInvoiceAdjustmentRepo intercompanyInvoiceAdjustmentRepo = new IntercompanyInvoiceAdjustmentRepo(db);
				IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment = intercompanyInvoiceAdjustmentView.ToIntercompanyInvoiceAdjustment();
				intercompanyInvoiceAdjustment = intercompanyInvoiceAdjustmentRepo.CreateIntercompanyInvoiceAdjustment(intercompanyInvoiceAdjustment);
				base.LogTransactionCreate<IntercompanyInvoiceAdjustment>(intercompanyInvoiceAdjustment);
				UnitOfWork.Commit();
				return intercompanyInvoiceAdjustment.ToIntercompanyInvoiceAdjustmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceAdjustmentItemView UpdateIntercompanyInvoiceAdjustment(IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentView)
		{
			try
			{
				IIntercompanyInvoiceAdjustmentRepo intercompanyInvoiceAdjustmentRepo = new IntercompanyInvoiceAdjustmentRepo(db);
				IntercompanyInvoiceAdjustment inputIntercompanyInvoiceAdjustment = intercompanyInvoiceAdjustmentView.ToIntercompanyInvoiceAdjustment();
				IntercompanyInvoiceAdjustment dbIntercompanyInvoiceAdjustment = intercompanyInvoiceAdjustmentRepo.Find(inputIntercompanyInvoiceAdjustment.IntercompanyInvoiceAdjustmentGUID);
				dbIntercompanyInvoiceAdjustment = intercompanyInvoiceAdjustmentRepo.UpdateIntercompanyInvoiceAdjustment(dbIntercompanyInvoiceAdjustment, inputIntercompanyInvoiceAdjustment);
				base.LogTransactionUpdate<IntercompanyInvoiceAdjustment>(GetOriginalValues<IntercompanyInvoiceAdjustment>(dbIntercompanyInvoiceAdjustment), dbIntercompanyInvoiceAdjustment);
				UnitOfWork.Commit();
				return dbIntercompanyInvoiceAdjustment.ToIntercompanyInvoiceAdjustmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteIntercompanyInvoiceAdjustment(string item)
		{
			try
			{
				IIntercompanyInvoiceAdjustmentRepo intercompanyInvoiceAdjustmentRepo = new IntercompanyInvoiceAdjustmentRepo(db);
				Guid intercompanyInvoiceAdjustmentGUID = new Guid(item);
				IntercompanyInvoiceAdjustment intercompanyInvoiceAdjustment = intercompanyInvoiceAdjustmentRepo.Find(intercompanyInvoiceAdjustmentGUID);
				intercompanyInvoiceAdjustmentRepo.Remove(intercompanyInvoiceAdjustment);
				base.LogTransactionDelete<IntercompanyInvoiceAdjustment>(intercompanyInvoiceAdjustment);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public IntercompanyInvoiceTableItemView GetAdjustIntercompanyInvoiceById(string id)
        {
			try
			{
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				var model = intercompanyInvoiceTableRepo.GetByIdvw(id.StringToGuid());
				decimal sumSettleAmount = intercompanyInvoiceTableRepo.GetSumAmountByIntercompanyInvoiceId(model.IntercompanyInvoiceTableGUID.StringToGuid(), Convert.ToInt32(IntercompanyInvoiceSettlementStatus.Posted).ToString());
				model.OrigInvoiceAmount = model.InvoiceAmount;
				model.Balance = model.InvoiceAmount - sumSettleAmount;
				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public IntercompanyInvoiceAdjustmentItemView UpdateAdjustIntercompanyInvoice(IntercompanyInvoiceAdjustmentItemView intercompanyInvoiceAdjustmentItemView)
        {
			try
			{

				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				var balanceAdjust = intercompanyInvoiceAdjustmentItemView.Balance + intercompanyInvoiceAdjustmentItemView.Adjustment;
				var anvoiceAmount = intercompanyInvoiceAdjustmentItemView.OrigInvoiceAmount;
				if(anvoiceAmount>0&& balanceAdjust < 0)
                {
					ex.AddData("ERROR.90151", new string[] { "LABEL.BALANCE", "LABEL.ADJUSTMENT" });

				}
				if (anvoiceAmount < 0 && balanceAdjust > 0)
				{
					ex.AddData("ERROR.90152", new string[] { "LABEL.BALANCE", "LABEL.ADJUSTMENT" });

				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				IntercompanyInvoiceAdjustmentItemView model = new IntercompanyInvoiceAdjustmentItemView();
				IIntercompanyInvoiceTableRepo intercompanyInvoiceAdjustmentRepo = new IntercompanyInvoiceTableRepo(db);
				var interCompanyInvoiceModel = intercompanyInvoiceAdjustmentRepo.GetByIdvw(intercompanyInvoiceAdjustmentItemView.IntercompanyInvoiceTableGUID.StringToGuid());
				interCompanyInvoiceModel.InvoiceAmount = (intercompanyInvoiceAdjustmentItemView.OrigInvoiceAmount + intercompanyInvoiceAdjustmentItemView.Adjustment);
				IIntercompanyInvoiceTableService intercompanyInvoiceTableService = new IntercompanyInvoiceTableService(db);
				intercompanyInvoiceTableService.UpdateIntercompanyInvoiceTable(interCompanyInvoiceModel);



				model.IntercompanyInvoiceAdjustmentGUID = intercompanyInvoiceAdjustmentItemView.IntercompanyInvoiceTableGUID;
				model.Adjustment = intercompanyInvoiceAdjustmentItemView.Adjustment;
				model.DocumentReasonGUID = intercompanyInvoiceAdjustmentItemView.DocumentReasonGUID;
				model.IntercompanyInvoiceTableGUID = intercompanyInvoiceAdjustmentItemView.IntercompanyInvoiceTableGUID;
				model.CompanyGUID = intercompanyInvoiceAdjustmentItemView.CompanyGUID;
				model.OriginalAmount = intercompanyInvoiceAdjustmentItemView.OrigInvoiceAmount;


				CreateIntercompanyInvoiceAdjustment(model);
				NotificationResponse success = new NotificationResponse();
				success.AddData("SUCCESS.90020", new string[] { "LABEL.TRANSACTION_ADJUSTMENT" });
				model.Notification = success;
				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
