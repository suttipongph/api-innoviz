using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICreditAppTransService
	{

		CreditAppTransItemView GetCreditAppTransById(string id);
		CreditAppTransItemView CreateCreditAppTrans(CreditAppTransItemView creditAppTransView);
		CreditAppTrans CreateCreditAppTrans(CreditAppTrans creditAppTrans);
		CreditAppTransItemView UpdateCreditAppTrans(CreditAppTransItemView creditAppTransView);
		bool DeleteCreditAppTrans(string id);
		void CreateCreditAppTransCollection(List<CreditAppTransItemView> creditAppTransItemViews);
	}
	public class CreditAppTransService : SmartAppService, ICreditAppTransService
	{
		public CreditAppTransService(SmartAppDbContext context) : base(context) { }
		public CreditAppTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CreditAppTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CreditAppTransService() : base() { }

		public CreditAppTransItemView GetCreditAppTransById(string id)
		{
			try
			{
				ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
				return creditAppTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppTransItemView CreateCreditAppTrans(CreditAppTransItemView creditAppTransView)
		{
			try
			{
				CreditAppTrans creditAppTrans = creditAppTransView.ToCreditAppTrans();
				creditAppTrans = CreateCreditAppTrans(creditAppTrans);
				UnitOfWork.Commit();
				return creditAppTrans.ToCreditAppTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppTrans CreateCreditAppTrans(CreditAppTrans creditAppTrans)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				creditAppTrans = accessLevelService.AssignOwnerBU(creditAppTrans);
				ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
				creditAppTrans = creditAppTransRepo.CreateCreditAppTrans(creditAppTrans);
				base.LogTransactionCreate<CreditAppTrans>(creditAppTrans);
				return creditAppTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppTransItemView UpdateCreditAppTrans(CreditAppTransItemView creditAppTransView)
		{
			try
			{
				ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
				CreditAppTrans inputCreditAppTrans = creditAppTransView.ToCreditAppTrans();
				CreditAppTrans dbCreditAppTrans = creditAppTransRepo.Find(inputCreditAppTrans.CreditAppTransGUID);
				dbCreditAppTrans = creditAppTransRepo.UpdateCreditAppTrans(dbCreditAppTrans, inputCreditAppTrans);
				base.LogTransactionUpdate<CreditAppTrans>(GetOriginalValues<CreditAppTrans>(dbCreditAppTrans), dbCreditAppTrans);
				UnitOfWork.Commit();
				return dbCreditAppTrans.ToCreditAppTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCreditAppTrans(string item)
		{
			try
			{
				ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
				Guid creditAppTransGUID = new Guid(item);
				CreditAppTrans creditAppTrans = creditAppTransRepo.Find(creditAppTransGUID);
				creditAppTransRepo.Remove(creditAppTrans);
				base.LogTransactionDelete<CreditAppTrans>(creditAppTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCreditAppTransCollection(List<CreditAppTransItemView> creditAppTransItemViews)
		{
			try
			{
				if (creditAppTransItemViews.Any())
				{
					ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
					List<CreditAppTrans> creditAppTrans = creditAppTransItemViews.ToCreditAppTrans().ToList();
					creditAppTransRepo.ValidateAdd(creditAppTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(creditAppTrans, false);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}

	}
}
