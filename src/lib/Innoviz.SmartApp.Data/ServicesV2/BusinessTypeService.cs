using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBusinessTypeService
	{

		BusinessTypeItemView GetBusinessTypeById(string id);
		BusinessTypeItemView CreateBusinessType(BusinessTypeItemView businessTypeView);
		BusinessTypeItemView UpdateBusinessType(BusinessTypeItemView businessTypeView);
		bool DeleteBusinessType(string id);
	}
	public class BusinessTypeService : SmartAppService, IBusinessTypeService
	{
		public BusinessTypeService(SmartAppDbContext context) : base(context) { }
		public BusinessTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BusinessTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BusinessTypeService() : base() { }

		public BusinessTypeItemView GetBusinessTypeById(string id)
		{
			try
			{
				IBusinessTypeRepo businessTypeRepo = new BusinessTypeRepo(db);
				return businessTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessTypeItemView CreateBusinessType(BusinessTypeItemView businessTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				businessTypeView = accessLevelService.AssignOwnerBU(businessTypeView);
				IBusinessTypeRepo businessTypeRepo = new BusinessTypeRepo(db);
				BusinessType businessType = businessTypeView.ToBusinessType();
				businessType = businessTypeRepo.CreateBusinessType(businessType);
				base.LogTransactionCreate<BusinessType>(businessType);
				UnitOfWork.Commit();
				return businessType.ToBusinessTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessTypeItemView UpdateBusinessType(BusinessTypeItemView businessTypeView)
		{
			try
			{
				IBusinessTypeRepo businessTypeRepo = new BusinessTypeRepo(db);
				BusinessType inputBusinessType = businessTypeView.ToBusinessType();
				BusinessType dbBusinessType = businessTypeRepo.Find(inputBusinessType.BusinessTypeGUID);
				dbBusinessType = businessTypeRepo.UpdateBusinessType(dbBusinessType, inputBusinessType);
				base.LogTransactionUpdate<BusinessType>(GetOriginalValues<BusinessType>(dbBusinessType), dbBusinessType);
				UnitOfWork.Commit();
				return dbBusinessType.ToBusinessTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBusinessType(string item)
		{
			try
			{
				IBusinessTypeRepo businessTypeRepo = new BusinessTypeRepo(db);
				Guid businessTypeGUID = new Guid(item);
				BusinessType businessType = businessTypeRepo.Find(businessTypeGUID);
				businessTypeRepo.Remove(businessType);
				base.LogTransactionDelete<BusinessType>(businessType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
