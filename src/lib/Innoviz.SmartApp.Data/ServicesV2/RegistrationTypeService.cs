using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IRegistrationTypeService
	{

		RegistrationTypeItemView GetRegistrationTypeById(string id);
		RegistrationTypeItemView CreateRegistrationType(RegistrationTypeItemView registrationTypeView);
		RegistrationTypeItemView UpdateRegistrationType(RegistrationTypeItemView registrationTypeView);
		bool DeleteRegistrationType(string id);
	}
	public class RegistrationTypeService : SmartAppService, IRegistrationTypeService
	{
		public RegistrationTypeService(SmartAppDbContext context) : base(context) { }
		public RegistrationTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public RegistrationTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RegistrationTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public RegistrationTypeService() : base() { }

		public RegistrationTypeItemView GetRegistrationTypeById(string id)
		{
			try
			{
				IRegistrationTypeRepo registrationTypeRepo = new RegistrationTypeRepo(db);
				return registrationTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RegistrationTypeItemView CreateRegistrationType(RegistrationTypeItemView registrationTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				registrationTypeView = accessLevelService.AssignOwnerBU(registrationTypeView);
				IRegistrationTypeRepo registrationTypeRepo = new RegistrationTypeRepo(db);
				RegistrationType registrationType = registrationTypeView.ToRegistrationType();
				registrationType = registrationTypeRepo.CreateRegistrationType(registrationType);
				base.LogTransactionCreate<RegistrationType>(registrationType);
				UnitOfWork.Commit();
				return registrationType.ToRegistrationTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RegistrationTypeItemView UpdateRegistrationType(RegistrationTypeItemView registrationTypeView)
		{
			try
			{
				IRegistrationTypeRepo registrationTypeRepo = new RegistrationTypeRepo(db);
				RegistrationType inputRegistrationType = registrationTypeView.ToRegistrationType();
				RegistrationType dbRegistrationType = registrationTypeRepo.Find(inputRegistrationType.RegistrationTypeGUID);
				dbRegistrationType = registrationTypeRepo.UpdateRegistrationType(dbRegistrationType, inputRegistrationType);
				base.LogTransactionUpdate<RegistrationType>(GetOriginalValues<RegistrationType>(dbRegistrationType), dbRegistrationType);
				UnitOfWork.Commit();
				return dbRegistrationType.ToRegistrationTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteRegistrationType(string item)
		{
			try
			{
				IRegistrationTypeRepo registrationTypeRepo = new RegistrationTypeRepo(db);
				Guid registrationTypeGUID = new Guid(item);
				RegistrationType registrationType = registrationTypeRepo.Find(registrationTypeGUID);
				registrationTypeRepo.Remove(registrationType);
				base.LogTransactionDelete<RegistrationType>(registrationType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
