using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IGenderService
	{

		GenderItemView GetGenderById(string id);
		GenderItemView CreateGender(GenderItemView genderView);
		GenderItemView UpdateGender(GenderItemView genderView);
		bool DeleteGender(string id);
	}
	public class GenderService : SmartAppService, IGenderService
	{
		public GenderService(SmartAppDbContext context) : base(context) { }
		public GenderService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public GenderService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public GenderService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public GenderService() : base() { }

		public GenderItemView GetGenderById(string id)
		{
			try
			{
				IGenderRepo genderRepo = new GenderRepo(db);
				return genderRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GenderItemView CreateGender(GenderItemView genderView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				genderView = accessLevelService.AssignOwnerBU(genderView);
				IGenderRepo genderRepo = new GenderRepo(db);
				Gender gender = genderView.ToGender();
				gender = genderRepo.CreateGender(gender);
				base.LogTransactionCreate<Gender>(gender);
				UnitOfWork.Commit();
				return gender.ToGenderItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GenderItemView UpdateGender(GenderItemView genderView)
		{
			try
			{
				IGenderRepo genderRepo = new GenderRepo(db);
				Gender inputGender = genderView.ToGender();
				Gender dbGender = genderRepo.Find(inputGender.GenderGUID);
				dbGender = genderRepo.UpdateGender(dbGender, inputGender);
				base.LogTransactionUpdate<Gender>(GetOriginalValues<Gender>(dbGender), dbGender);
				UnitOfWork.Commit();
				return dbGender.ToGenderItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteGender(string item)
		{
			try
			{
				IGenderRepo genderRepo = new GenderRepo(db);
				Guid genderGUID = new Guid(item);
				Gender gender = genderRepo.Find(genderGUID);
				genderRepo.Remove(gender);
				base.LogTransactionDelete<Gender>(gender);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
