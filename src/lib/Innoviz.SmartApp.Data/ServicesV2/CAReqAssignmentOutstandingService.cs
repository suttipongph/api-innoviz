using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICAReqAssignmentOutstandingService
	{

		CAReqAssignmentOutstandingItemView GetCAReqAssignmentOutstandingById(string id);
		CAReqAssignmentOutstandingItemView CreateCAReqAssignmentOutstanding(CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingView);
		CAReqAssignmentOutstandingItemView UpdateCAReqAssignmentOutstanding(CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingView);
		bool DeleteCAReqAssignmentOutstanding(string id);
	}
	public class CAReqAssignmentOutstandingService : SmartAppService, ICAReqAssignmentOutstandingService
	{
		public CAReqAssignmentOutstandingService(SmartAppDbContext context) : base(context) { }
		public CAReqAssignmentOutstandingService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CAReqAssignmentOutstandingService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqAssignmentOutstandingService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CAReqAssignmentOutstandingService() : base() { }

		public CAReqAssignmentOutstandingItemView GetCAReqAssignmentOutstandingById(string id)
		{
			try
			{
				ICAReqAssignmentOutstandingRepo caReqAssignmentOutstandingRepo = new CAReqAssignmentOutstandingRepo(db);
				return caReqAssignmentOutstandingRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqAssignmentOutstandingItemView CreateCAReqAssignmentOutstanding(CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				caReqAssignmentOutstandingView = accessLevelService.AssignOwnerBU(caReqAssignmentOutstandingView);
				ICAReqAssignmentOutstandingRepo caReqAssignmentOutstandingRepo = new CAReqAssignmentOutstandingRepo(db);
				CAReqAssignmentOutstanding caReqAssignmentOutstanding = caReqAssignmentOutstandingView.ToCAReqAssignmentOutstanding();
				caReqAssignmentOutstanding = caReqAssignmentOutstandingRepo.CreateCAReqAssignmentOutstanding(caReqAssignmentOutstanding);
				base.LogTransactionCreate<CAReqAssignmentOutstanding>(caReqAssignmentOutstanding);
				UnitOfWork.Commit();
				return caReqAssignmentOutstanding.ToCAReqAssignmentOutstandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqAssignmentOutstandingItemView UpdateCAReqAssignmentOutstanding(CAReqAssignmentOutstandingItemView caReqAssignmentOutstandingView)
		{
			try
			{
				ICAReqAssignmentOutstandingRepo caReqAssignmentOutstandingRepo = new CAReqAssignmentOutstandingRepo(db);
				CAReqAssignmentOutstanding inputCAReqAssignmentOutstanding = caReqAssignmentOutstandingView.ToCAReqAssignmentOutstanding();
				CAReqAssignmentOutstanding dbCAReqAssignmentOutstanding = caReqAssignmentOutstandingRepo.Find(inputCAReqAssignmentOutstanding.CAReqAssignmentOutstandingGUID);
				dbCAReqAssignmentOutstanding = caReqAssignmentOutstandingRepo.UpdateCAReqAssignmentOutstanding(dbCAReqAssignmentOutstanding, inputCAReqAssignmentOutstanding);
				base.LogTransactionUpdate<CAReqAssignmentOutstanding>(GetOriginalValues<CAReqAssignmentOutstanding>(dbCAReqAssignmentOutstanding), dbCAReqAssignmentOutstanding);
				UnitOfWork.Commit();
				return dbCAReqAssignmentOutstanding.ToCAReqAssignmentOutstandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCAReqAssignmentOutstanding(string item)
		{
			try
			{
				ICAReqAssignmentOutstandingRepo caReqAssignmentOutstandingRepo = new CAReqAssignmentOutstandingRepo(db);
				Guid caReqAssignmentOutstandingGUID = new Guid(item);
				CAReqAssignmentOutstanding caReqAssignmentOutstanding = caReqAssignmentOutstandingRepo.Find(caReqAssignmentOutstandingGUID);
				caReqAssignmentOutstandingRepo.Remove(caReqAssignmentOutstanding);
				base.LogTransactionDelete<CAReqAssignmentOutstanding>(caReqAssignmentOutstanding);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
