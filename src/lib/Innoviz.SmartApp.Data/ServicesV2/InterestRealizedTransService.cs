using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IInterestRealizedTransService
	{

		InterestRealizedTransItemView GetInterestRealizedTransById(string id);
		InterestRealizedTransItemView CreateInterestRealizedTrans(InterestRealizedTransItemView interestRealizedTransView);
		InterestRealizedTransItemView UpdateInterestRealizedTrans(InterestRealizedTransItemView interestRealizedTransView);
		bool DeleteInterestRealizedTrans(string id);
		void CreateInterestRealizedTransCollection(IEnumerable<InterestRealizedTransItemView> interestRealizedTransItemViews);
	}
	public class InterestRealizedTransService : SmartAppService, IInterestRealizedTransService
	{
		public InterestRealizedTransService(SmartAppDbContext context) : base(context) { }
		public InterestRealizedTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public InterestRealizedTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InterestRealizedTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public InterestRealizedTransService() : base() { }

		public InterestRealizedTransItemView GetInterestRealizedTransById(string id)
		{
			try
			{
				IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
				return interestRealizedTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestRealizedTransItemView CreateInterestRealizedTrans(InterestRealizedTransItemView interestRealizedTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				interestRealizedTransView = accessLevelService.AssignOwnerBU(interestRealizedTransView);
				IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
				InterestRealizedTrans interestRealizedTrans = interestRealizedTransView.ToInterestRealizedTrans();
				interestRealizedTrans = interestRealizedTransRepo.CreateInterestRealizedTrans(interestRealizedTrans);
				base.LogTransactionCreate<InterestRealizedTrans>(interestRealizedTrans);
				UnitOfWork.Commit();
				return interestRealizedTrans.ToInterestRealizedTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestRealizedTransItemView UpdateInterestRealizedTrans(InterestRealizedTransItemView interestRealizedTransView)
		{
			try
			{
				IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
				InterestRealizedTrans inputInterestRealizedTrans = interestRealizedTransView.ToInterestRealizedTrans();
				InterestRealizedTrans dbInterestRealizedTrans = interestRealizedTransRepo.Find(inputInterestRealizedTrans.InterestRealizedTransGUID);
				dbInterestRealizedTrans = interestRealizedTransRepo.UpdateInterestRealizedTrans(dbInterestRealizedTrans, inputInterestRealizedTrans);
				base.LogTransactionUpdate<InterestRealizedTrans>(GetOriginalValues<InterestRealizedTrans>(dbInterestRealizedTrans), dbInterestRealizedTrans);
				UnitOfWork.Commit();
				return dbInterestRealizedTrans.ToInterestRealizedTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteInterestRealizedTrans(string item)
		{
			try
			{
				IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
				Guid interestRealizedTransGUID = new Guid(item);
				InterestRealizedTrans interestRealizedTrans = interestRealizedTransRepo.Find(interestRealizedTransGUID);
				interestRealizedTransRepo.Remove(interestRealizedTrans);
				base.LogTransactionDelete<InterestRealizedTrans>(interestRealizedTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Migration
		public void CreateInterestRealizedTransCollection(IEnumerable<InterestRealizedTransItemView> interestRealizedTransItemViews)
		{
			try
			{
				if (interestRealizedTransItemViews.Any())
				{
					IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
					List<InterestRealizedTrans> interestRealizedTranss = interestRealizedTransItemViews.ToInterestRealizedTrans().ToList();
					interestRealizedTranss.ForEach(f => f.InterestRealizedTransGUID = Guid.NewGuid());
					interestRealizedTransRepo.ValidateAdd(interestRealizedTranss);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(interestRealizedTranss);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Migration

	}
}
