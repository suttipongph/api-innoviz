using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICreditLimitTypeService
	{

		CreditLimitTypeItemView GetCreditLimitTypeById(string id);
		CreditLimitTypeItemView CreateCreditLimitType(CreditLimitTypeItemView creditLimitTypeView);
		CreditLimitTypeItemView UpdateCreditLimitType(CreditLimitTypeItemView creditLimitTypeView);
		bool DeleteCreditLimitType(string id);
		IEnumerable<SelectItem<CreditLimitTypeItemView>> GetDropDownItemCreditLimitType(SearchParameter search);
	}
	public class CreditLimitTypeService : SmartAppService, ICreditLimitTypeService
	{
		public CreditLimitTypeService(SmartAppDbContext context) : base(context) { }
		public CreditLimitTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CreditLimitTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditLimitTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CreditLimitTypeService() : base() { }

		public CreditLimitTypeItemView GetCreditLimitTypeById(string id)
		{
			try
			{
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				return creditLimitTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditLimitTypeItemView CreateCreditLimitType(CreditLimitTypeItemView creditLimitTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				creditLimitTypeView = accessLevelService.AssignOwnerBU(creditLimitTypeView);
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				CreditLimitType creditLimitType = creditLimitTypeView.ToCreditLimitType();
				creditLimitType = creditLimitTypeRepo.CreateCreditLimitType(creditLimitType);
				base.LogTransactionCreate<CreditLimitType>(creditLimitType);
				UnitOfWork.Commit();
				return creditLimitType.ToCreditLimitTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditLimitTypeItemView UpdateCreditLimitType(CreditLimitTypeItemView creditLimitTypeView)
		{
			try
			{
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				CreditLimitType inputCreditLimitType = creditLimitTypeView.ToCreditLimitType();
				CreditLimitType dbCreditLimitType = creditLimitTypeRepo.Find(inputCreditLimitType.CreditLimitTypeGUID);
				dbCreditLimitType = creditLimitTypeRepo.UpdateCreditLimitType(dbCreditLimitType, inputCreditLimitType);
				base.LogTransactionUpdate<CreditLimitType>(GetOriginalValues<CreditLimitType>(dbCreditLimitType), dbCreditLimitType);
				UnitOfWork.Commit();
				return dbCreditLimitType.ToCreditLimitTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCreditLimitType(string item)
		{
			try
			{
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				Guid creditLimitTypeGUID = new Guid(item);
				CreditLimitType creditLimitType = creditLimitTypeRepo.Find(creditLimitTypeGUID);
				creditLimitTypeRepo.Remove(creditLimitType);
				base.LogTransactionDelete<CreditLimitType>(creditLimitType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditLimitTypeItemView>> GetDropDownItemCreditLimitType(SearchParameter search)
		{
			try
			{
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				search = search.GetParentCondition(CreditLimitTypeCondition.ProductType);
				return creditLimitTypeRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
