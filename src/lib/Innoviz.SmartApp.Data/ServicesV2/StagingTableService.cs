using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IStagingTableService
	{

		StagingTableItemView GetStagingTableById(string id);
		StagingTableItemView CreateStagingTable(StagingTableItemView stagingTableView);
		StagingTableItemView UpdateStagingTable(StagingTableItemView stagingTableView);
		bool DeleteStagingTable(string id);
		GenStagingTableResult GenStagingTableByProcessTransType(GenStagingTableParameter parameter);
		SmartAppException GetSmartAppExceptionFromGenStagingTableErrorList(List<GenStagingTableErrorList> errorList);
	}
    public class StagingTableService : SmartAppService, IStagingTableService
	{
		public StagingTableService(SmartAppDbContext context) : base(context) { }
		public StagingTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public StagingTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public StagingTableService(List<DbContext> contexts, ISysTransactionLogService transactionLogService)
			: base(contexts, transactionLogService)
		{
		}
		public StagingTableService(List<DbContext> contexts, ISysTransactionLogService transactionLogService, IBatchLogService batchLogService)
			: base(contexts, transactionLogService, batchLogService)
		{
		}
		public StagingTableService() : base() { }

		public StagingTableItemView GetStagingTableById(string id)
		{
			try
			{
				IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
				return stagingTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableItemView CreateStagingTable(StagingTableItemView stagingTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				stagingTableView = accessLevelService.AssignOwnerBU(stagingTableView);
				IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
				StagingTable stagingTable = stagingTableView.ToStagingTable();
				stagingTable = stagingTableRepo.CreateStagingTable(stagingTable);
				base.LogTransactionCreate<StagingTable>(stagingTable);
				UnitOfWork.Commit();
				return stagingTable.ToStagingTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableItemView UpdateStagingTable(StagingTableItemView stagingTableView)
		{
			try
			{
				IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
				StagingTable inputStagingTable = stagingTableView.ToStagingTable();
				StagingTable dbStagingTable = stagingTableRepo.Find(inputStagingTable.StagingTableGUID);
				dbStagingTable = stagingTableRepo.UpdateStagingTable(dbStagingTable, inputStagingTable);
				base.LogTransactionUpdate<StagingTable>(GetOriginalValues<StagingTable>(dbStagingTable), dbStagingTable);
				UnitOfWork.Commit();
				return dbStagingTable.ToStagingTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteStagingTable(string item)
		{
			try
			{
				IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
				Guid stagingTableGUID = new Guid(item);
				StagingTable stagingTable = stagingTableRepo.Find(stagingTableGUID);
				stagingTableRepo.Remove(stagingTable);
				base.LogTransactionDelete<StagingTable>(stagingTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public GenStagingTableResult GenStagingTableByProcessTransType(GenStagingTableParameter parameter)
        {
			GenStagingTableResult result = new GenStagingTableResult();
			try
			{
				ICompanyRepo companyRepo = new CompanyRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IStagingTransTextRepo stagingTransTextRepo = new StagingTransTextRepo(db);
				ILedgerDimensionRepo ledgerDimensionRepo = new LedgerDimensionRepo(db);
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);

				parameter.StagingBatchId = SmartAppUtil.GenIDByDateTime(DateTime.Now);
				parameter.DefaultCompanyGUID = GetCurrentCompany().StringToGuid();
				parameter.Company = companyRepo.GetCompanyByIdNoTracking(parameter.DefaultCompanyGUID);
				parameter.CompanyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTrackingByAccessLevel(parameter.DefaultCompanyGUID);
				parameter.StagingTransText = stagingTransTextRepo.GetStagingTransTextByProcessTransTypeNoTracking(parameter.ProcessTransType);
				parameter.LedgerDimension = ledgerDimensionRepo.GetLedgerDimensionByCompanyNoTracking(parameter.DefaultCompanyGUID);
				parameter.InvoiceRevenueType = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByCompanyNoTracking(parameter.DefaultCompanyGUID);
				parameter.InvoiceType = invoiceTypeRepo.GetInvoiceTypeByCompanyNoTracking(parameter.DefaultCompanyGUID);
				parameter.MethodOfPayment = methodOfPaymentRepo.GetMethodOfPaymentByCompanyNoTracking(parameter.DefaultCompanyGUID);
				parameter.TaxTable = taxTableRepo.GetTaxTableByCompanyNoTracking(parameter.DefaultCompanyGUID);

				result.StagingBatchId = parameter.StagingBatchId;

				#region Flow 1.
				ValidateStagingSetup(parameter, result);
				#endregion Flow 1.

				if (parameter.CompanyTaxBranchId != null)
				{
					#region Flow 2.
					GenerateStagingByListProcessTransType(parameter, result);
					List<Guid> processTransGUIDs = result.StagingTable.Where(w => ConditionService.IsNotNullAndNotEmptyGUID(w.ProcessTransGUID)).GroupBy(g => g.ProcessTransGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
					List<ProcessTrans> processTransSuccess = parameter.ProcessTransFilter.Where(w => processTransGUIDs.Contains(w.ProcessTransGUID)).ToList();
					processTransSuccess.Select(up =>
					{
						up.StagingBatchId = parameter.StagingBatchId;
						up.StagingBatchStatus = (int)StagingBatchStatus.Success;
						return up;
					}).ToList();
					result.ProcessTrans.AddRange(processTransSuccess);
                    #endregion Flow 2.
                    #region Flow 3.
					ReplaceStagingTableTransText(parameter, result);
					#endregion Flow 3.
				}

				#region Flow 4.
				List<Guid> refGUIDs = result.GenStagingTableErrorList.Where(w => ConditionService.IsNotNullAndNotEmptyGUID(w.RefGUID)).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
				List<ProcessTrans> processTransError = parameter.ProcessTransFilter.Where(w => refGUIDs.Contains(w.RefGUID.GetValueOrDefault())).ToList();
				processTransError.Select(up =>
				{
					up.StagingBatchId = parameter.StagingBatchId;
					up.StagingBatchStatus = (int)StagingBatchStatus.Fail;
					return up;
				}).ToList();
				result.ProcessTrans.AddRange(processTransError);
				#endregion Flow 4.

				return result;
			}
			catch (Exception e)
			{
				SmartAppException errorFromList = GetSmartAppExceptionFromGenStagingTableErrorList(result.GenStagingTableErrorList);
				throw SmartAppUtil.AddStackTrace(e, errorFromList);
			}
		}
		private void ValidateStagingSetup(GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				IBranchRepo branchRepo = new BranchRepo(db);
				Branch branch = branchRepo.GetBranchByCompanyGUIDNoTracking(parameter.DefaultCompanyGUID);
				#region 1.1
				if (branch != null && branch.TaxBranchGUID.HasValue)
				{
					parameter.CompanyTaxBranchId = branch.BranchId;
				}
				else
				{
					result.GenStagingTableErrorList.Add(new GenStagingTableErrorList
					{
						TableName = typeof(Branch).Name,
						FieldName = BranchCondition.BranchGUID,
						Reference = "LABEL.STAGING_SETUP",
						RefValue = "{{0}}: {{1}}",
						RefValueArgs = new string[] { "LABEL.BRANCH_ID", (branch == null) ? "" : branch.BranchId },
						ProcessTransType = null,
						ErrorCode = "ERROR.90118",
						ErrorCodeArgs = new string[] { "LABEL.TAX_BRANCH_ID", "LABEL.BRANCH_ID", (branch == null) ? "" : branch.BranchId },
						RefGUID = null,
						ProductType = null
					});
				}
				#endregion
				#region 1.2
				result.GenStagingTableErrorList.AddRange(
					parameter.ProcessTransType.Where(w => !parameter.StagingTransText.Select(s => s.ProcessTransType).Contains(w))
					.Select(er => new GenStagingTableErrorList
					{
						TableName = typeof(StagingTransText).Name, 
						FieldName = StagingTransTextCondition.ProcessTransType,
						Reference = "LABEL.STAGING_SETUP",
						RefValue = "{{0}}: {{1}}",
						RefValueArgs = new string[] { "LABEL.STAGING_TRANSACTION_TEXT", ((ProcessTransType)er).GetAttrCode() },
						ProcessTransType = er,
						ErrorCode = "ERROR.90118",
						ErrorCodeArgs = new string[] { "LABEL.TRANSACTION_TEXT", "LABEL.PROCESS_TRANSACTION_TYPE", ((ProcessTransType)er).GetAttrCode() },
						RefGUID = null,
						ProductType = null
					}).ToList()
				);
				#endregion
			}
            catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingByListProcessTransType(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
				parameter.ProcessTransFilter = processTransRepo.GetProcessTransForGenerateStaging(parameter.ProcessTransType, parameter.StagingBatchStatus, parameter.RefGUID);

				parameter.ProcessTransTypeFilter = new List<int>() { (int)ProcessTransType.Invoice, (int)ProcessTransType.Payment, (int)ProcessTransType.PaymentDetail };
				if (
					!ConditionService.IsEqualZero(parameter.ProcessTransType.Where(w => parameter.ProcessTransTypeFilter.Contains(w)).Count()) &&
					!ConditionService.IsEqualZero(parameter.StagingTransText.Where(w => parameter.ProcessTransTypeFilter.Contains(w.ProcessTransType)).Count())
					)
				{
					#region 2.1
					GenerateStagingInvoicePayment(parameter, result);
					#endregion 2.1
				}

				if(
					parameter.ProcessTransType.Contains((int)ProcessTransType.IncomeRealization) &&
					!ConditionService.IsEqualZero(parameter.StagingTransText.Where(w => w.ProcessTransType == (int)ProcessTransType.IncomeRealization).Count())
					)
                {
					#region 2.2
					GenerateStagingIncomeRealization(parameter, result);
					#endregion 2.2
				}

				if (
					parameter.ProcessTransType.Contains((int)ProcessTransType.VendorPayment) &&
					!ConditionService.IsEqualZero(parameter.StagingTransText.Where(w => w.ProcessTransType == (int)ProcessTransType.VendorPayment).Count())
					)
				{
					#region 2.3
					GenerateStagingVendorPayment(parameter, result);
					#endregion 2.3
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region 2.1 Generate Staging - Invoice & Payment & PaymentDetail
		private void GenerateStagingInvoicePayment(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				#region 2.1.1
				ValidateTransTypeInvoicePayment(parameter, result);
				#endregion
				#region 2.1.2
				GenerateStagingInvoice(parameter, result);
				#endregion
				#region 2.1.3
				GenerateStagingPayment(parameter, result);
				#endregion
				#region 2.1.4
				GenerateStagingPaymentDetail(parameter, result);
				#endregion
				#region 2.1.5, 2.1.6
				var query215 = result.StagingTable.Where(w => w.ProcessTransType == (int)ProcessTransType.Invoice || w.ProcessTransType == (int)ProcessTransType.Payment || w.ProcessTransType == (int)ProcessTransType.PaymentDetail)
												  .GroupBy(g => new { g.RefType, g.RefGUID })
												  .Select(s => new { s.Key.RefType, s.Key.RefGUID, AmountMST = s.Sum(sum => sum.AmountMST) })
												  .Where(w => w.AmountMST != 0)
												  .ToList();

				if (!ConditionService.IsEqualZero(query215.Count()))
				{
					result.GenStagingTableErrorList.AddRange(query215.Select(s => new GenStagingTableErrorList
					{
						TableName = "LABEL.STAGING_TABLE",
						FieldName = StagingTableCondition.RefGUID,
						Reference = "Staging balance",
						RefValue = "Reference: {{0}} {{1}}",
						RefValueArgs = new string[] { ((RefType)s.RefType).GetAttrCode(), s.RefGUID.GuidNullToString() },
						ProcessTransType = null,
						ErrorCode = "ERROR.90138",
						ErrorCodeArgs = new string[] { ((RefType)result.StagingTable.Where(w => w.RefGUID == s.RefGUID).Select(s => s.SourceRefType).FirstOrDefault()).GetAttrCode(), 
														result.StagingTable.Where(w => w.RefGUID == s.RefGUID).Select(s => s.SourceRefId).FirstOrDefault(),
														s.AmountMST.DecimalToString()},
						RefGUID = s.RefGUID,
						ProductType = null
					}));

					result.StagingTable.RemoveAll(r => query215.Any(a => a.RefType == r.RefType && a.RefGUID == r.RefGUID));
				}
				#endregion
			}
            catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void ValidateTransTypeInvoicePayment(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
			try
			{
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);

				List<int> process2_1 = new List<int>() { (int)ProcessTransType.Invoice, (int)ProcessTransType.Payment, (int)ProcessTransType.PaymentDetail };
				parameter.ProcessTransFilter_InvoicePayment = parameter.ProcessTransFilter.Where(w => process2_1.Contains(w.ProcessTransType)).ToList();
				
				if (!ConditionService.IsEqualZero(parameter.ProcessTransFilter_InvoicePayment.Count()))
                {
					List<Guid> guidsTaxTableGUID = new List<Guid>();
					guidsTaxTableGUID.AddRange(parameter.ProcessTransFilter_InvoicePayment.Where(w => w.TaxTableGUID.HasValue).Select(s => s.TaxTableGUID.GetValueOrDefault()));
					guidsTaxTableGUID.AddRange(parameter.ProcessTransFilter_InvoicePayment.Where(w => w.OrigTaxTableGUID.HasValue).Select(s => s.TaxTableGUID.GetValueOrDefault()));
					List<TaxTable> taxTableFilter = taxTableRepo.GetTaxTableByIdNoTracking(guidsTaxTableGUID);
					List<InvoiceLine> invoiceLineFilter = invoiceLineRepo.GetInvoiceLinesByInvoiceTableGuid(parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Invoice).Select(s => s.InvoiceTableGUID.GetValueOrDefault()).ToList());

					List<Guid> processTransPaymentDetailGUID = parameter.ProcessTransFilter_InvoicePayment.Where(w => w.PaymentDetailGUID.HasValue && w.ProcessTransType == (int)ProcessTransType.PaymentDetail).GroupBy(g => g.PaymentDetailGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
					parameter.PaymentDetailFilter = paymentDetailRepo.GetPaymentDetailByIdNoTracking(processTransPaymentDetailGUID).ToList();
					List<Guid> processTransReceiptTempTableGUID = parameter.ProcessTransFilter_InvoicePayment.Where(w => w.RefGUID.HasValue && w.RefType == (int)RefType.ReceiptTemp && w.ProcessTransType == (int)ProcessTransType.Payment).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
					parameter.ReceiptTempTableFilter = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(processTransReceiptTempTableGUID).ToList();
					List<Guid> processTransCustomerRefundTableGUID = parameter.ProcessTransFilter_InvoicePayment.Where(w => w.RefGUID.HasValue && w.RefType == (int)RefType.CustomerRefund).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
					parameter.CustomerRefundTableFilter = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(processTransCustomerRefundTableGUID).ToList();
					parameter.PaymentHistoryFilter = paymentHistoryRepo.GetPaymentHistoryByIdNoTracking(parameter.ProcessTransFilter_InvoicePayment.Select(s => s.PaymentHistoryGUID.GetValueOrDefault()).ToList());
					parameter.InvoiceSettlementDetailFilter = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByIdNoTracking(parameter.PaymentHistoryFilter.Select(s => s.InvoiceSettlementDetailGUID.GetValueOrDefault()).ToList());

					#region 2.1.1.1 Validate OutputTaxLedgerAccount - Tax code
					result.GenStagingTableErrorList.AddRange(
						parameter.ProcessTransFilter_InvoicePayment
						.Where(w => w.TaxTableGUID.HasValue && w.TaxAmountMST != 0)
						.Select(s => s)
						.Join(taxTableFilter, ptf => ptf.TaxTableGUID, ttf => ttf.TaxTableGUID, (ptf, ttf) => new { ptf, ttf })
						.Where(w => ConditionService.IsNullOrEmpty(w.ttf.OutputTaxLedgerAccount))
						.GroupBy(g => new { g.ptf.ProcessTransType, g.ttf.TaxCode, g.ptf.RefGUID, g.ptf.RefType })
						.Select(er => new GenStagingTableErrorList
						{
							TableName = typeof(ProcessTrans).Name,
							FieldName = ProcessTransCondition.TaxTableGUID,
							Reference = ((ProcessTransType)er.Key.ProcessTransType).GetAttrCode(),
							RefValue = "{{0}}: {{1}}",
							RefValueArgs = new string[] { "LABEL.TAX_CODE", er.Key.TaxCode },
							ProcessTransType = er.Key.ProcessTransType,
							ErrorCode = "ERROR.90118",
							ErrorCodeArgs = new string[] { "LABEL.OUTPUT_TAX_LEDGER_ACCOUNT", "LABEL.TAX_CODE", er.Key.TaxCode },
							RefGUID = er.Key.RefGUID,
							ProductType = null
						}).ToList()
					);
					#endregion
					#region 2.1.1.2 Validate OutputTaxLedgerAccount - Original tax code
					result.GenStagingTableErrorList.AddRange(
						parameter.ProcessTransFilter_InvoicePayment
						.Where(w => w.OrigTaxTableGUID.HasValue && w.TaxAmountMST != 0)
						.Select(s => s)
						.Join(taxTableFilter, ptf => ptf.OrigTaxTableGUID, ttf => ttf.TaxTableGUID, (ptf, ttf) => new { ptf, ttf })
						.Where(w => ConditionService.IsNullOrEmpty(w.ttf.OutputTaxLedgerAccount))
						.GroupBy(g => new { g.ptf.ProcessTransType, g.ttf.TaxCode, g.ptf.RefGUID, g.ptf.RefType })
						.Select(er => new GenStagingTableErrorList
						{
							TableName = typeof(ProcessTrans).Name,
							FieldName = ProcessTransCondition.TaxTableGUID,
							Reference = ((ProcessTransType)er.Key.ProcessTransType).GetAttrCode(),
							RefValue = "{{0}}: {{1}}",
							RefValueArgs = new string[] { "LABEL.ORIGINAL_TAX_CODE", er.Key.TaxCode },
							ProcessTransType = er.Key.ProcessTransType,
							ErrorCode = "ERROR.90118",
							ErrorCodeArgs = new string[] { "LABEL.OUTPUT_TAX_LEDGER_ACCOUNT", "LABEL.ORIGINAL_TAX_CODE", er.Key.TaxCode },
							RefGUID = er.Key.RefGUID,
							ProductType = null
						}).ToList()
					);
					#endregion
					#region 2.1.1.3 Validate FeeLedgerAccount - Service fee type ID - Invoice line
					result.GenStagingTableErrorList.AddRange(
						parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Invoice)
						.Select(s => s)
						.Join(invoiceLineFilter, pt => pt.InvoiceTableGUID, il => il.InvoiceTableGUID, (pt, il) => new { pt, il })
						.Join(parameter.InvoiceRevenueType, ptil => ptil.il.InvoiceRevenueTypeGUID, irt => irt.InvoiceRevenueTypeGUID, (ptil, irt) => new { ptil, irt })
						.Where(w => ConditionService.IsNullOrEmpty(w.irt.FeeLedgerAccount))
						.GroupBy(g => new { g.ptil.pt.ProcessTransType, g.ptil.pt.RefGUID, g.ptil.pt.RefType, g.irt.RevenueTypeId })
						.Select(er => new GenStagingTableErrorList
						{
							TableName = typeof(InvoiceLine).Name,
							FieldName = InvoiceLineCondition.InvoiceRevenueTypeGUID,
							Reference = ((ProcessTransType)er.Key.ProcessTransType).GetAttrCode(),
							RefValue = "{{0}}: {{1}}",
							RefValueArgs = new string[] { "LABEL.REVENUE_TYPE_ID", er.Key.RevenueTypeId },
							ProcessTransType = er.Key.ProcessTransType,
							ErrorCode = "ERROR.90118",
							ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.REVENUE_TYPE_ID", er.Key.RevenueTypeId },
							RefGUID = er.Key.RefGUID,
							ProductType = null
						}).ToList()
					);
					#endregion
					#region 2.1.1.4 Validate FeeLedgerAccount - Service fee type ID - PaymentDetail
					var query2114 = (from pt in parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.PaymentDetail)
									 join pd in parameter.PaymentDetailFilter on pt.PaymentDetailGUID equals pd.PaymentDetailGUID
									 join ivt in parameter.InvoiceType on pd.InvoiceTypeGUID equals ivt.InvoiceTypeGUID
									 join ivrt in parameter.InvoiceRevenueType on ivt.AutoGenInvoiceRevenueTypeGUID equals ivrt.InvoiceRevenueTypeGUID
									 where ConditionService.IsNullOrEmpty(ivrt.FeeLedgerAccount)
									 group pt by new { pt.ProcessTransType, pt.RefGUID, pt.RefType, ivrt.RevenueTypeId } into g
									 select new GenStagingTableErrorList
									 {
										 TableName = typeof(PaymentDetail).Name,
										 FieldName = PaymentDetailCondition.InvoiceTypeGUID,
										 Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
										 RefValue = "{{0}}: {{1}}",
										 RefValueArgs = new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", g.Key.RevenueTypeId },
										 ProcessTransType = g.Key.ProcessTransType,
										 ErrorCode = "ERROR.90118",
										 ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", g.Key.RevenueTypeId },
										 RefGUID = g.Key.RefGUID,
										 ProductType = null
									 }).ToList();
					result.GenStagingTableErrorList.AddRange(query2114.ToList());
					#endregion
					#region 2.1.1.5 Validate FeeLedgerAccount - Service fee type ID - Suspense invoice type - Collection
					var query2115 = (from pt in parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment && w.RefType == (int)RefType.ReceiptTemp)
									 join rct in parameter.ReceiptTempTableFilter.Where(w => w.SuspenseAmount != 0) 
									 on pt.RefGUID equals rct.ReceiptTempTableGUID
									 join ivt in parameter.InvoiceType on rct.SuspenseInvoiceTypeGUID equals ivt.InvoiceTypeGUID
									 join ivrt in parameter.InvoiceRevenueType on ivt.AutoGenInvoiceRevenueTypeGUID equals ivrt.InvoiceRevenueTypeGUID
									 where ConditionService.IsNullOrEmpty(ivrt.FeeLedgerAccount)
									 group pt by new { pt.ProcessTransType, pt.RefGUID, pt.RefType, ivrt.RevenueTypeId } into g
									 select new GenStagingTableErrorList
									 {
										 TableName = typeof(ReceiptTempTable).Name,
										 FieldName = ReceiptTempTableCondition.SuspenseInvoiceTypeGUID,
										 Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
										 RefValue = "{{0}}: {{1}}",
										 RefValueArgs = new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", g.Key.RevenueTypeId },
										 ProcessTransType = g.Key.ProcessTransType,
										 ErrorCode = "ERROR.90118",
										 ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", g.Key.RevenueTypeId },
										 RefGUID = g.Key.RefGUID,
										 ProductType = null
									 }).ToList();
					result.GenStagingTableErrorList.AddRange(query2115.ToList());
					#endregion
					
					if (ConditionService.IsNullOrEmptyGUID(parameter.CompanyParameter.SettlementMethodOfPaymentGUID))
					{
						#region 2.1.1.6 Validate SettlementMethodOfPaymentGUID - CompanyParameter (ProcessTransType = Payment detail)
						var query2116 = (from pt in parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.PaymentDetail)
										 group pt by new { pt.ProcessTransType, pt.RefGUID, pt.RefType } into g
										 select new GenStagingTableErrorList
										 {
											 TableName = typeof(CompanyParameter).Name,
											 FieldName = CompanyParameterCondition.SettlementMethodOfPaymentGUID,
											 Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
											 RefValue = "{{0}}: {{1}}",
											 RefValueArgs = new string[] { "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" },
											 ProcessTransType = g.Key.ProcessTransType,
											 ErrorCode = "ERROR.90047",
											 ErrorCodeArgs = new string[] { "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" },
											 RefGUID = g.Key.RefGUID,
											 ProductType = null
										 }).ToList();
						result.GenStagingTableErrorList.AddRange(query2116.ToList());
						#endregion

						#region 2.1.1.7 Validate SettlementMethodOfPaymentGUID - CompanyParameter (ProcessTransType = Payment & CustomerRefundTable.PaymentAmount != 0)
						var query2117 = (from pt in parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment && w.RefType == (int)RefType.CustomerRefund)
										 join crft in parameter.CustomerRefundTableFilter.Where(w => w.PaymentAmount != 0)
									     on pt.RefGUID equals crft.CustomerRefundTableGUID
										 group pt by new { pt.ProcessTransType, pt.RefGUID, pt.RefType } into g
										 select new GenStagingTableErrorList
										 {
											 TableName = typeof(CompanyParameter).Name,
											 FieldName = CompanyParameterCondition.SettlementMethodOfPaymentGUID,
											 Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
											 RefValue = "{{0}}: {{1}}",
											 RefValueArgs = new string[] { "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" },
											 ProcessTransType = g.Key.ProcessTransType,
											 ErrorCode = "ERROR.90047",
											 ErrorCodeArgs = new string[] { "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" },
											 RefGUID = g.Key.RefGUID,
											 ProductType = null
										 }).ToList();
						result.GenStagingTableErrorList.AddRange(query2117.ToList());
						#endregion
					}

					#region 2.1.1.8 Validate FeeLedgerAccount - Service fee type ID - FT Retention invoice type ID - Company parameter (ProcessTransType = Payment & CustomerRefundTable.RetentionAmount != 0)
					var query2118 = (from pt in parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment && w.RefType == (int)RefType.CustomerRefund)
									 join crft in parameter.CustomerRefundTableFilter.Where(w => w.RetentionAmount != 0)
									 on pt.RefGUID equals crft.CustomerRefundTableGUID
									 select new { pt, crft }).ToList();
					if (ConditionService.IsNullOrEmptyGUID(parameter.CompanyParameter.FTRetentionInvTypeGUID))
					{
						var query2118_1 = (from list in query2118
										   group list by new { list.pt.ProcessTransType, list.pt.RefGUID, list.pt.RefType } into g
											select new GenStagingTableErrorList
											{
												TableName = typeof(CompanyParameter).Name,
												FieldName = CompanyParameterCondition.FTRetentionInvTypeGUID,
												Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
												RefValue = "{{0}}: {{1}}",
												RefValueArgs = new string[] { "LABEL.FT_RETENTION_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER" },
												ProcessTransType = g.Key.ProcessTransType,
												ErrorCode = "ERROR.90047",
												ErrorCodeArgs = new string[] { "LABEL.FT_RETENTION_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER" },
												RefGUID = g.Key.RefGUID,
												ProductType = null
											}).ToList();
						result.GenStagingTableErrorList.AddRange(query2118_1.ToList());
					}
					else
					{
						var query2118_2 = (from list in query2118
										   join ivt in parameter.InvoiceType on parameter.CompanyParameter.FTRetentionInvTypeGUID equals ivt.InvoiceTypeGUID
											join ivrt in parameter.InvoiceRevenueType on ivt.AutoGenInvoiceRevenueTypeGUID equals ivrt.InvoiceRevenueTypeGUID
											where ConditionService.IsNullOrEmpty(ivrt.FeeLedgerAccount)
											group list by new { list.pt.ProcessTransType, list.pt.RefGUID, list.pt.RefType, ivrt.RevenueTypeId } into g
											select new GenStagingTableErrorList
											{
												TableName = typeof(CompanyParameter).Name,
												FieldName = CompanyParameterCondition.FTRetentionInvTypeGUID,
												Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
												RefValue = "{{0}}: {{1}}",
												RefValueArgs = new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", g.Key.RevenueTypeId },
												ProcessTransType = g.Key.ProcessTransType,
												ErrorCode = "ERROR.90118",
												ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", g.Key.RevenueTypeId },
												RefGUID = g.Key.RefGUID,
												ProductType = null
											}).ToList();
						result.GenStagingTableErrorList.AddRange(query2118_2.ToList());
					}
					#endregion

					#region 2.1.1.9 Validate FeeLedgerAccount - Reserve service fee type ID - Company paramater (ProcessTransType = Payment & SettleReserveAmount != 0)
					var query2119 = (from pt in parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment && w.RefType == (int)RefType.ReceiptTemp)
									join pmh in parameter.PaymentHistoryFilter
									on pt.PaymentHistoryGUID equals pmh.PaymentHistoryGUID
									join isd in parameter.InvoiceSettlementDetailFilter.Where(w => w.SettleReserveAmount != 0 && w.ProductType == (int)ProductType.Factoring)
									on pmh.InvoiceSettlementDetailGUID equals isd.InvoiceSettlementDetailGUID
									select new { pt, pmh, isd }).ToList();
					if (ConditionService.IsNullOrEmptyGUID(parameter.CompanyParameter.FTReserveInvRevenueTypeGUID))
					{
						var query2119_1 = (from list in query2119
										   group list by new { list.pt.ProcessTransType, list.pt.RefGUID, list.pt.RefType } into g
										   select new GenStagingTableErrorList
										   {
											   TableName = typeof(CompanyParameter).Name,
											   FieldName = CompanyParameterCondition.FTReserveInvRevenueTypeGUID,
											   Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
											   RefValue = "{{0}}: {{1}}",
											   RefValueArgs = new string[] { "LABEL.FT_RESERVE_INV_REVENUE_TYPE_ID", "LABEL.COMPANY_PARAMETER" },
											   ProcessTransType = g.Key.ProcessTransType,
											   ErrorCode = "ERROR.90047",
											   ErrorCodeArgs = new string[] { "LABEL.FT_RESERVE_INV_REVENUE_TYPE_ID", "LABEL.COMPANY_PARAMETER" },
											   RefGUID = g.Key.RefGUID,
											   ProductType = null
										   }).ToList();
						result.GenStagingTableErrorList.AddRange(query2119_1.ToList());
					}
					else
					{
						var query2119_2 = (from list in query2119
										   join ivrt in parameter.InvoiceRevenueType on parameter.CompanyParameter.FTReserveInvRevenueTypeGUID equals ivrt.InvoiceRevenueTypeGUID
										   where ConditionService.IsNullOrEmpty(ivrt.FeeLedgerAccount)
										   group list by new { list.pt.ProcessTransType, list.pt.RefGUID, list.pt.RefType, ivrt.RevenueTypeId } into g
										   select new GenStagingTableErrorList
										   {
											   TableName = typeof(CompanyParameter).Name,
											   FieldName = CompanyParameterCondition.FTReserveInvRevenueTypeGUID,
											   Reference = ((ProcessTransType)g.Key.ProcessTransType).GetAttrCode(),
											   RefValue = "{{0}}: {{1}}",
											   RefValueArgs = new string[] { "LABEL.FT_RESERVE_INV_REVENUE_TYPE_ID", g.Key.RevenueTypeId },
											   ProcessTransType = g.Key.ProcessTransType,
											   ErrorCode = "ERROR.90118",
											   ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.FT_RESERVE_INV_REVENUE_TYPE_ID", g.Key.RevenueTypeId },
											   RefGUID = g.Key.RefGUID,
											   ProductType = null
										   }).ToList();
						result.GenStagingTableErrorList.AddRange(query2119_2.ToList());
					}
					#endregion
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingInvoice(GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				
				List<ProcessTrans> selectedProcessTrans = 
					parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Invoice &&
																		!result.GenStagingTableErrorList.Select(s => s.RefGUID).Contains(w.RefGUID)).ToList();

				List<InvoiceTable> invoiceTableFilter = invoiceTableRepo.GetInvoiceTableByIdNoTracking(selectedProcessTrans.Select(s => s.InvoiceTableGUID.GetValueOrDefault()).ToList());
				List<InvoiceLine> invoiceLineFilter = invoiceLineRepo.GetInvoiceLinesByInvoiceTableGuid(selectedProcessTrans.Select(s => s.InvoiceTableGUID.GetValueOrDefault()).ToList());
				List<CustomerTable> customerTableFilter = customerTableRepo.GetCustomerTableByIdNoTracking(selectedProcessTrans.Select(s => s.CustomerTableGUID).ToList());
				List<TaxInvoiceTable> taxInvoiceTableFilter = taxInvoiceTableRepo.GetTaxInvoiceTableByIdNoTracking(selectedProcessTrans.Where(w => w.RefTaxInvoiceGUID.HasValue).Select(s => s.RefTaxInvoiceGUID.GetValueOrDefault()).ToList());
				List<TaxTable> taxTableFilter = taxTableRepo.GetTaxTableByIdNoTracking(selectedProcessTrans.Where(w => w.TaxTableGUID.HasValue).Select(s => s.TaxTableGUID.GetValueOrDefault()).ToList());
	
				#region 2.1.2.2 Create Record in ListStagingTable (AR)
				var query2122 = (from pt in selectedProcessTrans
								 join it in invoiceTableFilter
								 on pt.InvoiceTableGUID equals it.InvoiceTableGUID
								 join ct in customerTableFilter
								 on pt.CustomerTableGUID equals ct.CustomerTableGUID
								 join stt in parameter.StagingTransText
								 on pt.ProcessTransType equals stt.ProcessTransType
								 join tit in taxInvoiceTableFilter
								 on pt.RefTaxInvoiceGUID equals tit.TaxInvoiceTableGUID into lj01
								 from tit in lj01.DefaultIfEmpty()
								 join ld1 in parameter.LedgerDimension
								 on it.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on it.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on it.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on it.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on it.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 select new { pt, it, ct, stt, tit, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2122.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.pt.ARLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.pt.AmountMST,
					DimensionCode1 = (s.it.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,  
					DimensionCode2 = (s.it.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,  
					DimensionCode3 = (s.it.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,  
					DimensionCode4 = (s.it.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,  
					DimensionCode5 = (s.it.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,  
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = (!s.pt.RefTaxInvoiceGUID.HasValue || s.tit == null) ? String.Empty : s.tit.TaxInvoiceId,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = s.ct.TaxID,
					TaxAccountId = s.ct.CustomerId,
					TaxAccountName = s.ct.Name,
					TaxBranchId = s.it.TaxBranchId,
					TaxAddress = s.it.InvoiceAddress1 + " " + s.it.InvoiceAddress2,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion

				#region 2.1.2.3 Create Record in ListStagingTable (Tax)
				var query2123 = (from pt in selectedProcessTrans
								 join tt in taxTableFilter
								 on pt.TaxTableGUID equals tt.TaxTableGUID
								 join stt in parameter.StagingTransText
								 on pt.ProcessTransType equals stt.ProcessTransType
								 join ct in customerTableFilter
								 on pt.CustomerTableGUID equals ct.CustomerTableGUID
								 join it in invoiceTableFilter
								 on pt.InvoiceTableGUID equals it.InvoiceTableGUID
								 join il in invoiceLineFilter.Where(w => w.TaxTableGUID.HasValue)
															 .GroupBy(g => g.InvoiceTableGUID)
															 .Select(s => new {
																 InvoiceTableGUID = s.Key,
																 SumTotalAmountBeforeTax = s.Sum(sum => sum.TotalAmountBeforeTax)
															 }).ToList()
								 on pt.InvoiceTableGUID equals il.InvoiceTableGUID
								 join tit in taxInvoiceTableFilter
								 on pt.RefTaxInvoiceGUID equals tit.TaxInvoiceTableGUID into lj01
								 from tit in lj01.DefaultIfEmpty()
								 join ld1 in parameter.LedgerDimension
								 on it.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on it.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on it.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on it.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on it.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 where pt.TaxTableGUID.HasValue &&
								 pt.TaxAmountMST != 0
								 select new { pt, tt, stt, tit, it, il, ct, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2123.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.tt.OutputTaxLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.pt.TaxAmountMST * (-1),
					DimensionCode1 = (s.it.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.it.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.it.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.it.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.it.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = (!s.pt.RefTaxInvoiceGUID.HasValue || s.tit == null) ? String.Empty : s.tit.TaxInvoiceId,
					TaxCode = s.tt.TaxCode,
					TaxBaseAmount = s.il.SumTotalAmountBeforeTax,
					TaxId = s.ct.TaxID,
					TaxAccountId = s.ct.CustomerId,
					TaxAccountName = s.ct.Name,
					TaxBranchId = s.it.TaxBranchId,
					TaxAddress = s.it.InvoiceAddress1 + " " + s.it.InvoiceAddress2,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion

				#region 2.1.2.4 Create Record in ListStagingTable (InvoiceLine)
				var query2124 = (from pt in selectedProcessTrans
								 join it in invoiceTableFilter
								 on pt.InvoiceTableGUID equals it.InvoiceTableGUID
								 join il in invoiceLineFilter
								 on it.InvoiceTableGUID equals il.InvoiceTableGUID
								 join irt in parameter.InvoiceRevenueType
								 on il.InvoiceRevenueTypeGUID equals irt.InvoiceRevenueTypeGUID
								 join stt in parameter.StagingTransText
								 on pt.ProcessTransType equals stt.ProcessTransType
								 join ct in customerTableFilter
								 on pt.CustomerTableGUID equals ct.CustomerTableGUID
								 join tit in taxInvoiceTableFilter
								 on pt.RefTaxInvoiceGUID equals tit.TaxInvoiceTableGUID into lj01
								 from tit in lj01.DefaultIfEmpty()
								 join ld1 in parameter.LedgerDimension
								 on il.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on il.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on il.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on il.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on il.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 select new { pt, ct, it, il, irt, stt, tit, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2124.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.irt.FeeLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = (s.il.TotalAmountBeforeTax * s.pt.ExchangeRate * (-1)).Round(),
					DimensionCode1 = (s.il.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.il.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.il.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.il.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.il.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = (!s.pt.RefTaxInvoiceGUID.HasValue || s.tit == null) ? String.Empty : s.tit.TaxInvoiceId,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = s.ct.TaxID,
					TaxAccountId = s.ct.CustomerId,
					TaxAccountName = s.ct.Name,
					TaxBranchId = s.it.TaxBranchId,
					TaxAddress = s.it.InvoiceAddress1 + " " + s.it.InvoiceAddress2,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingPayment(GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
				IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);

				List<ProcessTrans> selectedProcessTrans =
					parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment &&
																			!result.GenStagingTableErrorList.Select(s => s.RefGUID).Contains(w.RefGUID)).ToList();
				
				List<InvoiceTable> invoiceTableFilter = invoiceTableRepo.GetInvoiceTableByIdNoTracking(selectedProcessTrans.Select(s => s.InvoiceTableGUID.GetValueOrDefault()).ToList());
				List<CustomerTable> customerTableFilter = customerTableRepo.GetCustomerTableByIdNoTracking(selectedProcessTrans.Select(s => s.CustomerTableGUID).ToList());
				List<TaxInvoiceTable> taxInvoiceTableFilter = taxInvoiceTableRepo.GetTaxInvoiceTableByIdNoTracking(selectedProcessTrans.Where(w => w.RefTaxInvoiceGUID.HasValue).Select(s => s.RefTaxInvoiceGUID.GetValueOrDefault()).ToList());
				parameter.PaymentHistoryFilter = paymentHistoryRepo.GetPaymentHistoryByIdNoTracking(selectedProcessTrans.Select(s => s.PaymentHistoryGUID.GetValueOrDefault()).ToList());
				List<WithholdingTaxTable> withholdingTaxTableFilter = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(parameter.PaymentHistoryFilter.Select(s => s.WithholdingTaxTableGUID.GetValueOrDefault()).ToList());
				parameter.InvoiceSettlementDetailFilter = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByIdNoTracking(parameter.PaymentHistoryFilter.Select(s => s.InvoiceSettlementDetailGUID.GetValueOrDefault()).ToList());
				List<ProductSettledTrans> productSettledTranFilter = productSettledTransRepo.GetProductSettledTransByInvoiceSettlementDetailGuid(parameter.InvoiceSettlementDetailFilter.Select(s => s.InvoiceSettlementDetailGUID).ToList());
				List<Guid> processTransReceiptTempTableGUID = selectedProcessTrans.Where(w => w.RefGUID.HasValue && w.RefType == (int)RefType.ReceiptTemp && w.ProcessTransType == (int)ProcessTransType.Payment).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
				parameter.ReceiptTempTableFilter = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(processTransReceiptTempTableGUID).ToList();
				List<Guid> processTransCustomerRefundTableGUID = selectedProcessTrans.Where(w => w.RefGUID.HasValue && w.RefType == (int)RefType.CustomerRefund).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
				parameter.CustomerRefundTableFilter = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(processTransCustomerRefundTableGUID).ToList();

				#region 2.1.3.2 Create Record in ListStagingTable (ReceiptTempTable.SuspenseAmount != 0) 
				if (!ConditionService.IsEqualZero(parameter.ReceiptTempTableFilter.Count()))
				{
					var query21321 = (from rtt in parameter.ReceiptTempTableFilter.Where(w => w.SuspenseAmount != 0)
									  join stt in parameter.StagingTransText
									  on (int)ProcessTransType.Payment equals stt.ProcessTransType
									  join ity in parameter.InvoiceType.Where(w => w.AutoGenInvoiceRevenueTypeGUID.HasValue)
									  on rtt.SuspenseInvoiceTypeGUID equals ity.InvoiceTypeGUID into lj01
									  from ity in lj01.DefaultIfEmpty()
									  join irt in parameter.InvoiceRevenueType
									  on ity.AutoGenInvoiceRevenueTypeGUID equals irt.InvoiceRevenueTypeGUID into lj02
									  from irt in lj02.DefaultIfEmpty()
									  join ld1 in parameter.LedgerDimension
									  on rtt.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
									  from ld1 in ljld1.DefaultIfEmpty()
									  join ld2 in parameter.LedgerDimension
									  on rtt.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
									  from ld2 in ljld2.DefaultIfEmpty()
									  join ld3 in parameter.LedgerDimension
									  on rtt.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
									  from ld3 in ljld3.DefaultIfEmpty()
									  join ld4 in parameter.LedgerDimension
									  on rtt.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
									  from ld4 in ljld4.DefaultIfEmpty()
									  join ld5 in parameter.LedgerDimension
									  on rtt.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
									  from ld5 in ljld5.DefaultIfEmpty()
									  select new { stt, rtt, irt, ld1, ld2, ld3, ld4, ld5 }).ToList();

					result.StagingTable.AddRange(query21321.Select(s => new StagingTable
					{
						StagingTableGUID = Guid.NewGuid(),
						StagingBatchId = parameter.StagingBatchId,
						RefType = (int)RefType.ReceiptTemp,
						RefGUID = s.rtt.ReceiptTempTableGUID,
						SourceRefType = selectedProcessTrans.Where(w => w.RefGUID == s.rtt.ReceiptTempTableGUID).Select(s => s.SourceRefType).FirstOrDefault(),
						SourceRefId = selectedProcessTrans.Where(w => w.RefGUID == s.rtt.ReceiptTempTableGUID).Select(s => s.SourceRefId).FirstOrDefault(),
						DocumentId = s.rtt.ReceiptTempId,
						TransDate = s.rtt.TransDate,
						ProductType = s.rtt.ProductType,
						ProcessTransType = (int)ProcessTransType.Payment,
						AccountType = (int)AccountType.Ledger,
						AccountNum = s.irt.FeeLedgerAccount,
						TransText = s.stt.TransText,
						AmountMST = s.rtt.SuspenseAmount * (-1),
						DimensionCode1 = (s.rtt.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
						DimensionCode2 = (s.rtt.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
						DimensionCode3 = (s.rtt.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
						DimensionCode4 = (s.rtt.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
						DimensionCode5 = (s.rtt.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
						CompanyTaxBranchId = parameter.CompanyTaxBranchId,
						TaxInvoiceId = String.Empty,
						TaxCode = String.Empty,
						TaxBaseAmount = 0,
						TaxId = String.Empty,
						TaxAccountId = String.Empty,
						TaxAccountName = String.Empty,
						TaxBranchId = String.Empty,
						TaxAddress = String.Empty,
						VendorTaxInvoiceId = String.Empty,
						WHTCode = String.Empty,
						ProcessTransGUID = null,
						InterfaceStagingBatchId = String.Empty,
						InterfaceStatus = (int)InterfaceStatus.None,
						CompanyGUID = parameter.Company.CompanyGUID,
						CompanyId = parameter.Company.CompanyId
					}).ToList());
				}
				#endregion

				#region 2.1.3.3 Create Record in ListStagingTable (ReceiptTempPaymentDetail.MethodOfPayment >> MethodOfPayment.PaymentType != SuspenseAccount)
				List<ReceiptTempPaymDetail> receiptTempPaymDetailFilter = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByReceiptTempNoTracking(parameter.ReceiptTempTableFilter.Select(s => s.ReceiptTempTableGUID).ToList());

				var query2133 = (from rtt in parameter.ReceiptTempTableFilter
								 join rtpd in receiptTempPaymDetailFilter
								 on rtt.ReceiptTempTableGUID equals rtpd.ReceiptTempTableGUID
								 join mop in parameter.MethodOfPayment
								 on rtpd.MethodOfPaymentGUID equals mop.MethodOfPaymentGUID
								 join stt in parameter.StagingTransText
							     on (int)ProcessTransType.Payment equals stt.ProcessTransType
								 join ld1 in parameter.LedgerDimension
								 on rtt.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on rtt.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on rtt.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on rtt.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on rtt.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 where mop.PaymentType != (int)PaymentType.SuspenseAccount &&
								 rtpd.ReceiptAmount != 0
								 select new { rtt, rtpd, mop, stt, ld1, ld2, ld3, ld4, ld5 }).ToList();

				#region 2.1.3.3.1 Create Record in ListStagingTable (From ReceiptTempPaymentDetail)
				result.StagingTable.AddRange(query2133.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = (int)RefType.ReceiptTemp,
					RefGUID = s.rtt.ReceiptTempTableGUID,
					SourceRefType = selectedProcessTrans.Where(w => w.RefGUID == s.rtt.ReceiptTempTableGUID).Select(s => s.SourceRefType).FirstOrDefault(),
					SourceRefId = selectedProcessTrans.Where(w => w.RefGUID == s.rtt.ReceiptTempTableGUID).Select(s => s.SourceRefId).FirstOrDefault(),
					DocumentId = s.rtt.ReceiptTempId,
					TransDate = s.rtt.TransDate,
					ProductType = s.rtt.ProductType,
					ProcessTransType = (int)ProcessTransType.Payment,
					AccountType = s.mop.AccountType,
					AccountNum = s.mop.AccountNum,
					TransText = s.stt.TransText,
					AmountMST = s.rtpd.ReceiptAmount,
					DimensionCode1 = (s.rtt.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.rtt.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.rtt.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.rtt.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.rtt.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = null,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion
				#endregion

				#region 2.1.3.4 Create Record in ListStagingTable (AR Invoice Settle)
				var query2134 = (from pt in selectedProcessTrans
								 join it in invoiceTableFilter
								 on pt.InvoiceTableGUID equals it.InvoiceTableGUID
								 join ph in parameter.PaymentHistoryFilter
								 on pt.PaymentHistoryGUID equals ph.PaymentHistoryGUID
								 join stt in parameter.StagingTransText
								 on pt.ProcessTransType equals stt.ProcessTransType
								 join ct in customerTableFilter
								 on pt.CustomerTableGUID equals ct.CustomerTableGUID
								 join ld1 in parameter.LedgerDimension
								 on it.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on it.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on it.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on it.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on it.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 join tt in parameter.TaxTable 
								 on pt.TaxTableGUID equals tt.TaxTableGUID into lj01
								 from tt in lj01.DefaultIfEmpty()
								 join tit in taxInvoiceTableFilter
								 on pt.RefTaxInvoiceGUID equals tit.TaxInvoiceTableGUID into lj02
								 from tit in lj02.DefaultIfEmpty()
								 join origtt in parameter.TaxTable
								 on pt.OrigTaxTableGUID equals origtt.TaxTableGUID into lj03
								 from origtt in lj03.DefaultIfEmpty()
								 join wht in withholdingTaxTableFilter
								 on ph.WithholdingTaxTableGUID equals wht.WithholdingTaxTableGUID into lj04
								 from wht in lj04.DefaultIfEmpty()
								 select new { pt, it, ph, stt, ct, ld1, ld2, ld3, ld4, ld5, tt, origtt, tit, wht }).ToList();

				#region 2.1.3.4.1 Create Record in ListStagingTable (Reverse AR, Tax, WHT)
				//1. IF(ProcessTrans.AmountMST != 0)
				result.StagingTable.AddRange(query2134.Where(w => w.pt.AmountMST != 0).Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.pt.ARLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.pt.AmountMST * (-1),
					DimensionCode1 = (s.it.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.it.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.it.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.it.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.it.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				//2.1 StagingTable for TaxTableGUID
				result.StagingTable.AddRange(query2134.Where(w => w.pt.TaxAmountMST != 0 && w.pt.TaxTableGUID.HasValue && w.pt.OrigTaxTableGUID.HasValue).Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = (s.tt == null) ? String.Empty : s.tt.OutputTaxLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.pt.TaxAmountMST * (-1),
					DimensionCode1 = (s.it.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.it.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.it.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.it.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.it.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = (!s.pt.RefTaxInvoiceGUID.HasValue || s.tit == null) ? String.Empty : s.tit.TaxInvoiceId,
					TaxCode = (s.tt == null) ? null : s.tt.TaxCode,
					TaxBaseAmount = s.ph.PaymentTaxBaseAmountMST,
					TaxId = s.ct.TaxID,
					TaxAccountId = s.ct.CustomerId,
					TaxAccountName = s.ct.Name,
					TaxBranchId = s.it.TaxBranchId,
					TaxAddress = (s.tit == null) ? String.Empty : s.tit.InvoiceAddress1 + " " + s.tit.InvoiceAddress2,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				//2.2 StagingTable for OrigTaxTableGUID
				result.StagingTable.AddRange(query2134.Where(w => w.pt.TaxAmountMST != 0 && w.pt.TaxTableGUID.HasValue && w.pt.OrigTaxTableGUID.HasValue).Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = (s.origtt == null) ? String.Empty : s.origtt.OutputTaxLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.pt.TaxAmountMST,
					DimensionCode1 = (s.it.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.it.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.it.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.it.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.it.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = (!s.pt.RefTaxInvoiceGUID.HasValue || s.tit == null) ? String.Empty : s.tit.TaxInvoiceId,
					TaxCode = (s.origtt == null) ? String.Empty : s.origtt.TaxCode,
					TaxBaseAmount = s.ph.PaymentTaxBaseAmountMST,
					TaxId = s.ct.TaxID,
					TaxAccountId = s.ct.CustomerId,
					TaxAccountName = s.ct.Name,
					TaxBranchId = s.it.TaxBranchId,
					TaxAddress = (s.tit == null) ? String.Empty : s.tit.InvoiceAddress1 + " " + s.tit.InvoiceAddress2,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				//3. IF(PaymentHistory.WHTAmountMST != 0) 
				result.StagingTable.AddRange(query2134.Where(w => w.ph.WHTAmountMST != 0).Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = (s.wht == null) ? String.Empty : s.wht.LedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.ph.WHTAmountMST,
					DimensionCode1 = (s.it.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.it.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.it.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.it.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.it.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = s.ph.PaymentBaseAmountMST,
					TaxId = s.ct.TaxID,
					TaxAccountId = s.ct.CustomerId,
					TaxAccountName = s.ct.Name,
					TaxBranchId = s.it.TaxBranchId,
					TaxAddress = s.it.InvoiceAddress1 + " " + s.it.InvoiceAddress2,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = (s.wht == null) ? String.Empty : s.wht.WHTCode,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.1.3.4.1

				#region 2.1.3.4.2 IF (InvoiceTable.ProductInvoice  == True AND InvoiceTable.ProductType == ProductType.Factoring (1))
				if (invoiceTableFilter.Where(w => w.ProductInvoice == true && w.ProductType == (int)ProductType.Factoring).Any())
				{
					var query21342 = (from pt in selectedProcessTrans
									  join it in invoiceTableFilter
									  on pt.InvoiceTableGUID equals it.InvoiceTableGUID
									  join ph in parameter.PaymentHistoryFilter
									  on it.InvoiceTableGUID equals ph.InvoiceTableGUID
									  join isd in parameter.InvoiceSettlementDetailFilter
									  on ph.InvoiceSettlementDetailGUID equals isd.InvoiceSettlementDetailGUID
									  join stt in parameter.StagingTransText
									  on pt.ProcessTransType equals stt.ProcessTransType
									  join irt in parameter.InvoiceRevenueType
									  on parameter.CompanyParameter.FTReserveInvRevenueTypeGUID equals irt.InvoiceRevenueTypeGUID into lj02
									  from irt in lj02.DefaultIfEmpty()
									  join pst in productSettledTranFilter
									  on isd.InvoiceSettlementDetailGUID equals pst.InvoiceSettlementDetailGUID into lj03
									  from pst in lj03.DefaultIfEmpty()
									  join ld1 in parameter.LedgerDimension
									  on it.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
									  from ld1 in ljld1.DefaultIfEmpty()
									  join ld2 in parameter.LedgerDimension
									  on it.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
									  from ld2 in ljld2.DefaultIfEmpty()
									  join ld3 in parameter.LedgerDimension
									  on it.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
									  from ld3 in ljld3.DefaultIfEmpty()
									  join ld4 in parameter.LedgerDimension
									  on it.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
									  from ld4 in ljld4.DefaultIfEmpty()
									  join ld5 in parameter.LedgerDimension
									  on it.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
									  from ld5 in ljld5.DefaultIfEmpty()
									  where it.ProductInvoice == true &&
									  it.ProductType == (int)ProductType.Factoring &&
									  isd.SettleReserveAmount != 0
									  select new { pt, it, ph, isd, stt, irt, pst, ld1, ld2, ld3, ld4, ld5 }).ToList();

					result.StagingTable.AddRange(query21342.Select(s => new StagingTable
					{
						StagingTableGUID = Guid.NewGuid(),
						StagingBatchId = parameter.StagingBatchId,
						RefType = s.pt.RefType,
						RefGUID = s.pt.RefGUID,
						SourceRefType = s.pt.SourceRefType,
						SourceRefId = s.pt.SourceRefId,
						DocumentId = s.pt.DocumentId,
						TransDate = s.pt.TransDate,
						ProductType = s.pt.ProductType,
						ProcessTransType = s.pt.ProcessTransType,
						AccountType = (int)AccountType.Ledger,
						AccountNum = s.irt.FeeLedgerAccount,
						TransText = s.stt.TransText,
						AmountMST = (s.pst == null) ? s.isd.SettleReserveAmount : (s.isd.SettleReserveAmount - s.pst.ReserveToBeRefund),
						DimensionCode1 = (s.it.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
						DimensionCode2 = (s.it.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
						DimensionCode3 = (s.it.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
						DimensionCode4 = (s.it.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
						DimensionCode5 = (s.it.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
						CompanyTaxBranchId = parameter.CompanyTaxBranchId,
						TaxInvoiceId = String.Empty,
						TaxCode = String.Empty,
						TaxBaseAmount = 0,
						TaxId = String.Empty,
						TaxAccountId = String.Empty,
						TaxAccountName = String.Empty,
						TaxBranchId = String.Empty,
						TaxAddress = String.Empty,
						VendorTaxInvoiceId = String.Empty,
						WHTCode = String.Empty,
						ProcessTransGUID = s.pt.ProcessTransGUID,
						InterfaceStagingBatchId = String.Empty,
						InterfaceStatus = (int)InterfaceStatus.None,
						CompanyGUID = parameter.Company.CompanyGUID,
						CompanyId = parameter.Company.CompanyId
					}).ToList());
				}
				#endregion 2.1.3.4.2
				#endregion 2.1.3.4

				#region 2.1.3.5, 2.1.3.6 Create Record in ListStagingTable (CustomerRefundTable)
				GenerateStagingPayment_CustomerRefund(parameter, result, selectedProcessTrans);
				#endregion 2.1.3.5, 2.1.3.6
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingPayment_CustomerRefund(GenStagingTableParameter parameter, GenStagingTableResult result, List<ProcessTrans> selectedProcessTrans)
        {
            try
            {
				#region 2.1.3.5 Create Record in ListStagingTable (CustomerRefundTable.PaymentAmount != 0)
				var query2135 = (from crt in parameter.CustomerRefundTableFilter.Where(w => w.PaymentAmount != 0)
								  join stt in parameter.StagingTransText
								  on (int)ProcessTransType.Payment equals stt.ProcessTransType
								  join mop in parameter.MethodOfPayment
								  on parameter.CompanyParameter.SettlementMethodOfPaymentGUID equals mop.MethodOfPaymentGUID into ljmop
								  from mop in ljmop.DefaultIfEmpty()
								  join ld1 in parameter.LedgerDimension
								  on crt.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								  from ld1 in ljld1.DefaultIfEmpty()
								  join ld2 in parameter.LedgerDimension
								  on crt.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								  from ld2 in ljld2.DefaultIfEmpty()
								  join ld3 in parameter.LedgerDimension
								  on crt.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								  from ld3 in ljld3.DefaultIfEmpty()
								  join ld4 in parameter.LedgerDimension
								  on crt.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								  from ld4 in ljld4.DefaultIfEmpty()
								  join ld5 in parameter.LedgerDimension
								  on crt.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								  from ld5 in ljld5.DefaultIfEmpty()
								  select new { stt, crt, mop, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2135.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = (int)RefType.CustomerRefund,
					RefGUID = s.crt.CustomerRefundTableGUID,
					SourceRefType = (int)RefType.CustomerRefund,
					SourceRefId = s.crt.CustomerRefundId,
					DocumentId = s.crt.CustomerRefundId,
					TransDate = s.crt.TransDate,
					ProductType = s.crt.ProductType,
					ProcessTransType = (int)ProcessTransType.Payment,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.mop.AccountNum,
					TransText = s.stt.TransText,
					AmountMST = s.crt.PaymentAmount * (-1),
					DimensionCode1 = (s.crt.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.crt.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.crt.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.crt.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.crt.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = null,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.1.3.5

				#region 2.1.3.6 Create Record in ListStagingTable (CustomerRefundTable.RetentionAmount != 0)
				var query2136 = (from crt in parameter.CustomerRefundTableFilter.Where(w => w.RetentionAmount != 0)
								 join stt in parameter.StagingTransText
								 on (int)ProcessTransType.Payment equals stt.ProcessTransType
								 join ity in parameter.InvoiceType.Where(w => w.AutoGenInvoiceRevenueTypeGUID.HasValue)
								 on parameter.CompanyParameter.FTRetentionInvTypeGUID equals ity.InvoiceTypeGUID into lj01
								 from ity in lj01.DefaultIfEmpty()
								 join irt in parameter.InvoiceRevenueType
								 on ity.AutoGenInvoiceRevenueTypeGUID equals irt.InvoiceRevenueTypeGUID into lj02
								 from irt in lj02.DefaultIfEmpty()
								 join ld1 in parameter.LedgerDimension
								 on crt.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on crt.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on crt.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on crt.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on crt.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 select new { stt, crt, irt, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2136.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = (int)RefType.CustomerRefund,
					RefGUID = s.crt.CustomerRefundTableGUID,
					SourceRefType = (int)RefType.CustomerRefund,
					SourceRefId = s.crt.CustomerRefundId,
					DocumentId = s.crt.CustomerRefundId,
					TransDate = s.crt.TransDate,
					ProductType = s.crt.ProductType,
					ProcessTransType = (int)ProcessTransType.Payment,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.irt.FeeLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.crt.RetentionAmount * (-1),
					DimensionCode1 = (s.crt.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.crt.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.crt.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.crt.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.crt.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = null,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.1.3.6
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingPaymentDetail(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);

				List<ProcessTrans> selectedProcessTrans =
					parameter.ProcessTransFilter_InvoicePayment.Where(w => w.ProcessTransType == (int)ProcessTransType.PaymentDetail &&
																			!result.GenStagingTableErrorList.Select(s => s.RefGUID).Contains(w.RefGUID)).ToList();

				List<Guid> processTransPaymentDetailGUID = selectedProcessTrans.Where(w => w.PaymentDetailGUID.HasValue && w.ProcessTransType == (int)ProcessTransType.PaymentDetail).GroupBy(g => g.PaymentDetailGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
				parameter.PaymentDetailFilter = paymentDetailRepo.GetPaymentDetailByIdNoTracking(processTransPaymentDetailGUID).ToList();
				List<Guid> paymDetailGroupByRefGUID_pct = parameter.PaymentDetailFilter.Where(w => w.RefGUID.HasValue && w.RefType == (int)RefType.PurchaseTable).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
				List<PurchaseTable> purchaseTables = purchaseTableRepo.GetPurchaseTableByIdNoTracking(paymDetailGroupByRefGUID_pct).ToList();
				List<Guid> paymDetailGroupByRefGUID_wt = parameter.PaymentDetailFilter.Where(w => w.RefGUID.HasValue && w.RefType == (int)RefType.WithdrawalTable).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
				List<WithdrawalTable> withdrawalTables = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(paymDetailGroupByRefGUID_wt).ToList();
				List<Guid> paymDetailGroupByRefGUID_crt = parameter.PaymentDetailFilter.Where(w => w.RefGUID.HasValue && w.RefType == (int)RefType.CustomerRefund).GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList();
				List<CustomerRefundTable> customerRefundTables = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(paymDetailGroupByRefGUID_crt).ToList();

				#region 2.1.4.2 Create Record in ListStagingTable (PaymentDetail.PaidToType = PaidToType.Suspense (2))
				#region 2.1.4.2 - CASE RefType.PurchaseTable (45)
				List<QueryForGenStagingPaymentDetail> query2142_pct = 
					(from pt in selectedProcessTrans
						join pd in parameter.PaymentDetailFilter.Where(w => w.PaymentAmount != 0 && w.RefType == (int)RefType.PurchaseTable)
						on pt.PaymentDetailGUID equals pd.PaymentDetailGUID
						join pct in purchaseTables
						on pd.RefGUID equals pct.PurchaseTableGUID
						join ld1 in parameter.LedgerDimension
						on pct.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
						from ld1 in ljld1.DefaultIfEmpty()
						join ld2 in parameter.LedgerDimension
						on pct.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
						from ld2 in ljld2.DefaultIfEmpty()
						join ld3 in parameter.LedgerDimension
						on pct.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
						from ld3 in ljld3.DefaultIfEmpty()
						join ld4 in parameter.LedgerDimension
						on pct.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
						from ld4 in ljld4.DefaultIfEmpty()
						join ld5 in parameter.LedgerDimension
						on pct.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
						from ld5 in ljld5.DefaultIfEmpty()
						select new QueryForGenStagingPaymentDetail 
						{ 
							ProcessTrans = pt,
							PaymentDetail = pd,
							DimensionCode1 = (pct.Dimension1GUID == null) ? String.Empty : ld1.DimensionCode,
							DimensionCode2 = (pct.Dimension2GUID == null) ? String.Empty : ld2.DimensionCode,
							DimensionCode3 = (pct.Dimension3GUID == null) ? String.Empty : ld3.DimensionCode,
							DimensionCode4 = (pct.Dimension4GUID == null) ? String.Empty : ld4.DimensionCode,
							DimensionCode5 = (pct.Dimension5GUID == null) ? String.Empty : ld5.DimensionCode
						}).ToList();
				#endregion 2.1.4.2 - CASE RefType.PurchaseTable (45)
				#region 2.1.4.2 - CASE RefType.WithdrawalTable (56)
				List<QueryForGenStagingPaymentDetail> query2142_wt = 
					(from pt in selectedProcessTrans
						join pd in parameter.PaymentDetailFilter.Where(w => w.PaymentAmount != 0 && w.RefType == (int)RefType.WithdrawalTable)
						on pt.PaymentDetailGUID equals pd.PaymentDetailGUID
						join wt in withdrawalTables
						on pd.RefGUID equals wt.WithdrawalTableGUID
						join ld1 in parameter.LedgerDimension
						on wt.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
						from ld1 in ljld1.DefaultIfEmpty()
						join ld2 in parameter.LedgerDimension
						on wt.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
						from ld2 in ljld2.DefaultIfEmpty()
						join ld3 in parameter.LedgerDimension
						on wt.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
						from ld3 in ljld3.DefaultIfEmpty()
						join ld4 in parameter.LedgerDimension
						on wt.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
						from ld4 in ljld4.DefaultIfEmpty()
						join ld5 in parameter.LedgerDimension
						on wt.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
						from ld5 in ljld5.DefaultIfEmpty()
						select new QueryForGenStagingPaymentDetail
						{
							ProcessTrans = pt,
							PaymentDetail = pd,
							DimensionCode1 = (wt.Dimension1GUID == null) ? String.Empty : ld1.DimensionCode,
							DimensionCode2 = (wt.Dimension2GUID == null) ? String.Empty : ld2.DimensionCode,
							DimensionCode3 = (wt.Dimension3GUID == null) ? String.Empty : ld3.DimensionCode,
							DimensionCode4 = (wt.Dimension4GUID == null) ? String.Empty : ld4.DimensionCode,
							DimensionCode5 = (wt.Dimension5GUID == null) ? String.Empty : ld5.DimensionCode
						}).ToList();
				#endregion 2.1.4.2 - CASE RefType.WithdrawalTable (56)
				#region 2.1.4.2 - CASE RefType.CustomerRefund (58)
				List<QueryForGenStagingPaymentDetail> query2142_crt = 
					(from pt in selectedProcessTrans
						join pd in parameter.PaymentDetailFilter.Where(w => w.PaymentAmount != 0 && w.RefType == (int)RefType.CustomerRefund)
						on pt.PaymentDetailGUID equals pd.PaymentDetailGUID
						join crt in customerRefundTables
						on pd.RefGUID equals crt.CustomerRefundTableGUID 
						join ld1 in parameter.LedgerDimension
						on crt.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
						from ld1 in ljld1.DefaultIfEmpty()
						join ld2 in parameter.LedgerDimension
						on crt.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
						from ld2 in ljld2.DefaultIfEmpty()
						join ld3 in parameter.LedgerDimension
						on crt.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
						from ld3 in ljld3.DefaultIfEmpty()
						join ld4 in parameter.LedgerDimension
						on crt.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
						from ld4 in ljld4.DefaultIfEmpty()
						join ld5 in parameter.LedgerDimension
						on crt.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
						from ld5 in ljld5.DefaultIfEmpty()
						select new QueryForGenStagingPaymentDetail
						{
							ProcessTrans = pt,
							PaymentDetail = pd,
							DimensionCode1 = (crt.Dimension1GUID == null) ? String.Empty : ld1.DimensionCode,
							DimensionCode2 = (crt.Dimension2GUID == null) ? String.Empty : ld2.DimensionCode,
							DimensionCode3 = (crt.Dimension3GUID == null) ? String.Empty : ld3.DimensionCode,
							DimensionCode4 = (crt.Dimension4GUID == null) ? String.Empty : ld4.DimensionCode,
							DimensionCode5 = (crt.Dimension5GUID == null) ? String.Empty : ld5.DimensionCode
						}).ToList();
				#endregion 2.1.4.2 - CASE RefType.CustomerRefund (58)

				#region 2.1.4.2.1 Create Record in ListStagingTable (Cr. Revenue - Suspense invoice - Payment detail) 
				//CASE RefType.PurchaseTable (45)
				GenerateStagingPaymentDetail_Credit(query2142_pct, parameter, result);
				//CASE RefType.WithdrawalTable (56)
				GenerateStagingPaymentDetail_Credit(query2142_wt, parameter, result);
				//CASE RefType.CustomerRefund (58)
				GenerateStagingPaymentDetail_Credit(query2142_crt, parameter, result);
				#endregion 2.1.4.2.1

				#region 2.1.4.2.2 Create Record in ListStagingTable (Dr. Settle account - Settlement Method of payment - Paid to type = Suspense - PaymentDetail) 
				//CASE RefType.PurchaseTable (45)
				GenerateStagingPaymentDetail_Dedit(query2142_pct, parameter, result);
				//CASE RefType.WithdrawalTable (56)
				GenerateStagingPaymentDetail_Dedit(query2142_wt, parameter, result);
				//CASE RefType.CustomerRefund (58)
				GenerateStagingPaymentDetail_Dedit(query2142_crt, parameter, result);
				#endregion 2.1.4.2.2

				#endregion 2.1.4.2
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingPaymentDetail_Credit(List<QueryForGenStagingPaymentDetail> query, GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				var queryCredit = (from list in query
								   join stt in parameter.StagingTransText
								   on list.ProcessTrans.ProcessTransType equals stt.ProcessTransType
								   join ivt in parameter.InvoiceType
								   on list.PaymentDetail.InvoiceTypeGUID equals ivt.InvoiceTypeGUID
								   join ivrt in parameter.InvoiceRevenueType
								   on ivt.AutoGenInvoiceRevenueTypeGUID equals ivrt.InvoiceRevenueTypeGUID
								   select new { list, stt, ivrt }).ToList();
				result.StagingTable.AddRange(queryCredit.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.list.ProcessTrans.RefType,
					RefGUID = s.list.ProcessTrans.RefGUID,
					SourceRefType = s.list.ProcessTrans.SourceRefType,
					SourceRefId = s.list.ProcessTrans.SourceRefId,
					DocumentId = s.list.ProcessTrans.DocumentId,
					TransDate = s.list.ProcessTrans.TransDate,
					ProductType = s.list.ProcessTrans.ProductType,
					ProcessTransType = s.list.ProcessTrans.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.ivrt.FeeLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.list.PaymentDetail.PaymentAmount * (-1),
					DimensionCode1 = s.list.DimensionCode1,
					DimensionCode2 = s.list.DimensionCode2,
					DimensionCode3 = s.list.DimensionCode3,
					DimensionCode4 = s.list.DimensionCode4,
					DimensionCode5 = s.list.DimensionCode5,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.list.ProcessTrans.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingPaymentDetail_Dedit(List<QueryForGenStagingPaymentDetail> query, GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				var queryDedit = (from list in query
								  join stt in parameter.StagingTransText
								  on list.ProcessTrans.ProcessTransType equals stt.ProcessTransType
								  join mop in parameter.MethodOfPayment
								  on parameter.CompanyParameter.SettlementMethodOfPaymentGUID equals mop.MethodOfPaymentGUID
								  select new { list, stt, mop }).ToList();
				result.StagingTable.AddRange(queryDedit.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.list.ProcessTrans.RefType,
					RefGUID = s.list.ProcessTrans.RefGUID,
					SourceRefType = s.list.ProcessTrans.SourceRefType,
					SourceRefId = s.list.ProcessTrans.SourceRefId,
					DocumentId = s.list.ProcessTrans.DocumentId,
					TransDate = s.list.ProcessTrans.TransDate,
					ProductType = s.list.ProcessTrans.ProductType,
					ProcessTransType = s.list.ProcessTrans.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.mop.AccountNum,
					TransText = s.stt.TransText,
					AmountMST = s.list.PaymentDetail.PaymentAmount,
					DimensionCode1 = s.list.DimensionCode1,
					DimensionCode2 = s.list.DimensionCode2,
					DimensionCode3 = s.list.DimensionCode3,
					DimensionCode4 = s.list.DimensionCode4,
					DimensionCode5 = s.list.DimensionCode5,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.list.ProcessTrans.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion 2.1 Generate Staging - Invoice & Payment & PaymentDetail
		#region 2.2 Generate Staging - Income realization
		private void GenerateStagingIncomeRealization(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				parameter.InterestAccountbyProductType = new List<InterestAccountbyProductType>();
				#region 2.2.1
				ValidateTransTypeIncomeRealization(parameter, result);
				#endregion 2.2.1

				#region 2.2.2, 2.2.3
				GenerateStagingUnearnIntAndInterest(parameter, result);
				#endregion 2.2.2, 2.2.3
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void ValidateTransTypeIncomeRealization(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				string invoiceTypeId = "";
				#region 2.2.1.1 IF FTUnearnedInterestLedgerAccount = NULL
				parameter.UnearnedInterestLedgerAccount = (from invoiceRevenueType in parameter.InvoiceRevenueType
															join invoiceType in parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.FTUnearnedInterestInvTypeGUID)
															on invoiceRevenueType.InvoiceRevenueTypeGUID equals invoiceType.AutoGenInvoiceRevenueTypeGUID
															select invoiceRevenueType.FeeLedgerAccount).FirstOrDefault();
				if (ConditionService.IsNullOrEmpty(parameter.UnearnedInterestLedgerAccount))
				{
					invoiceTypeId = (parameter.CompanyParameter.FTUnearnedInterestInvTypeGUID == null) ? ""
											: parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.FTUnearnedInterestInvTypeGUID).Select(s => s.InvoiceTypeId).FirstOrDefault();
					result.GenStagingTableErrorList.Add(new GenStagingTableErrorList
					{
						TableName = typeof(CompanyParameter).Name,
						FieldName = CompanyParameterCondition.FTUnearnedInterestInvTypeGUID,
						Reference = "LABEL.INCOME_REALIZATION",
						RefValue = "{{0}}: {{1}}",
						RefValueArgs = new string[] { "LABEL.FT_UNEARNED_INTEREST_INV_TYPE_ID", invoiceTypeId },
						ProcessTransType = (int)ProcessTransType.IncomeRealization,
						ErrorCode = "ERROR.90118",
						ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.FT_UNEARNED_INTEREST_INV_TYPE_ID", invoiceTypeId },
						RefGUID = null,
						ProductType = ProductType.Factoring
					});
				}
				#endregion 2.2.1.1 IF FTUnearnedInterestLedgerAccount = NULL

				#region 2.2.1.2 IF FTInterestLedgerAccount = NULL
				parameter.InterestLedgerAccount = (from invoiceRevenueType in parameter.InvoiceRevenueType
													join invoiceType in parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.FTInterestInvTypeGUID)
													on invoiceRevenueType.InvoiceRevenueTypeGUID equals invoiceType.AutoGenInvoiceRevenueTypeGUID
													select invoiceRevenueType.FeeLedgerAccount).FirstOrDefault();
				if (ConditionService.IsNullOrEmpty(parameter.InterestLedgerAccount))
				{
					invoiceTypeId = (parameter.CompanyParameter.FTInterestInvTypeGUID == null) ? ""
											: parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.FTInterestInvTypeGUID).Select(s => s.InvoiceTypeId).FirstOrDefault();
					result.GenStagingTableErrorList.Add(new GenStagingTableErrorList
					{
						TableName = typeof(CompanyParameter).Name,
						FieldName = CompanyParameterCondition.FTInterestInvTypeGUID,
						Reference = "LABEL.INCOME_REALIZATION",
						RefValue = "{{0}}: {{1}}",
						RefValueArgs = new string[] { "LABEL.FT_INTEREST_INV_TYPE_ID", invoiceTypeId },
						ProcessTransType = (int)ProcessTransType.IncomeRealization,
						ErrorCode = "ERROR.90118",
						ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.FT_INTEREST_INV_TYPE_ID", invoiceTypeId },
						RefGUID = null,
						ProductType = ProductType.Factoring
					});
				}
				#endregion 2.2.1.2 IF FTInterestLedgerAccount = NULL

				#region 2.2.1.3 IF FTUnearnedInterestLedgerAccount != NULL AND FTInterestLedgerAccount != NULL >> Insert InterestAccountbyProductType
				if(ConditionService.IsNotNullAndNotEmpty(parameter.UnearnedInterestLedgerAccount) && ConditionService.IsNotNullAndNotEmpty(parameter.InterestLedgerAccount))
                {
					parameter.InterestAccountbyProductType.Add(new InterestAccountbyProductType
					{
						ProductType = (int)ProductType.Factoring,
						UnearnedInterestLedgerAccount = parameter.UnearnedInterestLedgerAccount,
						InterestLedgerAccount = parameter.InterestLedgerAccount
					});
                }
				#endregion 2.2.1.3 IF FTUnearnedInterestLedgerAccount != NULL AND FTInterestLedgerAccount != NULL >> Insert InterestAccountbyProductType

				#region 2.2.1.4 IF PFUnearnedInterestLedgerAccount = NULL
				parameter.UnearnedInterestLedgerAccount = (from invoiceRevenueType in parameter.InvoiceRevenueType
														   join invoiceType in parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.PFUnearnedInterestInvTypeGUID)
														   on invoiceRevenueType.InvoiceRevenueTypeGUID equals invoiceType.AutoGenInvoiceRevenueTypeGUID
														   select invoiceRevenueType.FeeLedgerAccount).FirstOrDefault();
				if (ConditionService.IsNullOrEmpty(parameter.UnearnedInterestLedgerAccount))
				{
					invoiceTypeId = (parameter.CompanyParameter.PFUnearnedInterestInvTypeGUID == null) ? ""
											: parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.PFUnearnedInterestInvTypeGUID).Select(s => s.InvoiceTypeId).FirstOrDefault();
					result.GenStagingTableErrorList.Add(new GenStagingTableErrorList
					{
						TableName = typeof(CompanyParameter).Name,
						FieldName = CompanyParameterCondition.PFUnearnedInterestInvTypeGUID,
						Reference = "LABEL.INCOME_REALIZATION",
						RefValue = "{{0}}: {{1}}",
						RefValueArgs = new string[] { "LABEL.PF_UNEARNED_INTEREST_INVOICE_TYPE_ID", invoiceTypeId },
						ProcessTransType = (int)ProcessTransType.IncomeRealization,
						ErrorCode = "ERROR.90118",
						ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.PF_UNEARNED_INTEREST_INVOICE_TYPE_ID", invoiceTypeId },
						RefGUID = null,
						ProductType = ProductType.ProjectFinance
					});
				}
				#endregion 2.2.1.4 IF PFUnearnedInterestLedgerAccount = NULL

				#region 2.2.1.5 IF PFInterestLedgerAccount = NULL
				parameter.InterestLedgerAccount = (from invoiceRevenueType in parameter.InvoiceRevenueType
												   join invoiceType in parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.PFInterestInvTypeGUID)
												   on invoiceRevenueType.InvoiceRevenueTypeGUID equals invoiceType.AutoGenInvoiceRevenueTypeGUID
												   select invoiceRevenueType.FeeLedgerAccount).FirstOrDefault();
				if (ConditionService.IsNullOrEmpty(parameter.InterestLedgerAccount))
				{
					invoiceTypeId = (parameter.CompanyParameter.PFInterestInvTypeGUID == null) ? ""
											: parameter.InvoiceType.Where(w => w.InvoiceTypeGUID == parameter.CompanyParameter.PFInterestInvTypeGUID).Select(s => s.InvoiceTypeId).FirstOrDefault();
					result.GenStagingTableErrorList.Add(new GenStagingTableErrorList
					{
						TableName = typeof(CompanyParameter).Name,
						FieldName = CompanyParameterCondition.PFInterestInvTypeGUID,
						Reference = "LABEL.INCOME_REALIZATION",
						RefValue = "{{0}}: {{1}}",
						RefValueArgs = new string[] { "LABEL.PF_INTEREST_INVOICE_TYPE_ID", invoiceTypeId },
						ProcessTransType = (int)ProcessTransType.IncomeRealization,
						ErrorCode = "ERROR.90118",
						ErrorCodeArgs = new string[] { "LABEL.FEE_LEDGER_ACCOUNT", "LABEL.PF_INTEREST_INVOICE_TYPE_ID", invoiceTypeId },
						RefGUID = null,
						ProductType = ProductType.ProjectFinance
					});
				}
				#endregion 2.2.1.5 IF PFInterestLedgerAccount = NULL

				#region 2.2.1.6 IF PFUnearnedInterestLedgerAccount != NULL AND PFInterestLedgerAccount != NULL >> Insert InterestAccountbyProductType
				if (ConditionService.IsNotNullAndNotEmpty(parameter.UnearnedInterestLedgerAccount) && ConditionService.IsNotNullAndNotEmpty(parameter.InterestLedgerAccount))
				{
					parameter.InterestAccountbyProductType.Add(new InterestAccountbyProductType
					{
						ProductType = (int)ProductType.ProjectFinance,
						UnearnedInterestLedgerAccount = parameter.UnearnedInterestLedgerAccount,
						InterestLedgerAccount = parameter.InterestLedgerAccount
					});
				}
				#endregion 2.2.1.6 IF FTUnearnedInterestLedgerAccount != NULL AND PFInterestLedgerAccount != NULL >> Insert InterestAccountbyProductType
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingUnearnIntAndInterest(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				#region 2.2.2
				List<int> process2_2 = new List<int>() { (int)ProcessTransType.IncomeRealization };
				IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
				List<ProcessTrans> selectedProcessTrans = parameter.ProcessTransFilter.Where(w => process2_2.Contains(w.ProcessTransType)).ToList();
				List<InterestRealizedTrans> interestRealizedTrans = interestRealizedTransRepo.GetInterestRealizedTransByProcessTransNoTracking(selectedProcessTrans.Select(s => s.ProcessTransGUID).ToList());

				var query222 = (from pt in selectedProcessTrans
								join irt in interestRealizedTrans
								on pt.ProcessTransGUID equals irt.RefProcessTransGUID
								join stt in parameter.StagingTransText
								on pt.ProcessTransType equals stt.ProcessTransType
								join intAcc in parameter.InterestAccountbyProductType
								on pt.ProductType equals intAcc.ProductType
								join ld1 in parameter.LedgerDimension
								on irt.Dimension1GUID equals ld1.LedgerDimensionGUID into ljld1
								from ld1 in ljld1.DefaultIfEmpty()
								join ld2 in parameter.LedgerDimension
								on irt.Dimension2GUID equals ld2.LedgerDimensionGUID into ljld2
								from ld2 in ljld2.DefaultIfEmpty()
								join ld3 in parameter.LedgerDimension
								on irt.Dimension3GUID equals ld3.LedgerDimensionGUID into ljld3
								from ld3 in ljld3.DefaultIfEmpty()
								join ld4 in parameter.LedgerDimension
								on irt.Dimension4GUID equals ld4.LedgerDimensionGUID into ljld4
								from ld4 in ljld4.DefaultIfEmpty()
								join ld5 in parameter.LedgerDimension
								on irt.Dimension5GUID equals ld5.LedgerDimensionGUID into ljld5
								from ld5 in ljld5.DefaultIfEmpty()
								select new { pt, irt, stt, intAcc, ld1, ld2, ld3, ld4, ld5 }).ToList();
				#endregion 2.2.2

				#region 2.2.3.1
				result.StagingTable.AddRange(query222.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.intAcc.UnearnedInterestLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.pt.AmountMST,
					DimensionCode1 = (s.irt.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.irt.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.irt.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.irt.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.irt.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.2.3.1

				#region 2.2.3.2
				result.StagingTable.AddRange(query222.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.pt.RefType,
					RefGUID = s.pt.RefGUID,
					SourceRefType = s.pt.SourceRefType,
					SourceRefId = s.pt.SourceRefId,
					DocumentId = s.pt.DocumentId,
					TransDate = s.pt.TransDate,
					ProductType = s.pt.ProductType,
					ProcessTransType = s.pt.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.intAcc.InterestLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.pt.AmountMST * (-1),
					DimensionCode1 = (s.irt.Dimension1GUID == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.irt.Dimension2GUID == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.irt.Dimension3GUID == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.irt.Dimension4GUID == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.irt.Dimension5GUID == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.pt.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.2.3.2
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion 2.2 Generate Staging - Income realization
		#region 2.3 Generate Staging - VendorPayment
		private void GenerateStagingVendorPayment(GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				#region 2.3.1
				List<int> process2_3 = new List<int>() { (int)ProcessTransType.VendorPayment };
				IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
				IVendBankRepo vendBankRepo = new VendBankRepo(db);
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);

				List<ProcessTrans> selectedProcessTrans = parameter.ProcessTransFilter.Where(w => process2_3.Contains(w.ProcessTransType)).ToList();
				List<VendorPaymentTrans> vendorPaymentTrans = vendorPaymentTransRepo.GetVendorPaymentTransByProcessTransNoTracking(selectedProcessTrans.GroupBy(g => g.RefGUID.GetValueOrDefault()).Select(s => s.Key).ToList());

				parameter.ProcessTransVendorPaymTrans = 
					(from pt in selectedProcessTrans
					join vpt in vendorPaymentTrans
					on pt.RefGUID equals vpt.VendorPaymentTransGUID
					select new ProcessTransVendorPaymTrans { 
						ProcessTrans = pt,
						VendorPaymentTrans = vpt
					}).ToList();
				parameter.VendorTable = vendorTableRepo.GetVendorTableByIdNoTracking(vendorPaymentTrans.Select(s => s.VendorTableGUID.Value).ToList());
				parameter.VendPrimaryBank = vendBankRepo.GetVendBankPrimaryByVendorNoTracking(parameter.VendorTable.Select(s => s.VendorTableGUID).ToList()).DistinctBy(d => d.VendorTableGUID).ToList();
				parameter.VendPrimaryAddressTrans = addressTransRepo.GetPrimaryAddressByReference(RefType.Vendor, parameter.VendorTable.Select(s => s.VendorTableGUID).ToList()).DistinctBy(d => d.RefGUID).ToList();

				ValidateTransTypeVendorPayment(parameter, result);
				#endregion 2.3.1

				#region 2.3.2
				//2.3.2.1
				parameter.ProcessTransVendorPaymTransNoError =
				   parameter.ProcessTransVendorPaymTrans.Where(w => !result.GenStagingTableErrorList.Select(s => s.RefGUID).Contains(w.ProcessTrans.RefGUID)).ToList();

				GenerateStagingVendorPayment_AP(parameter, result); //2.3.2.2
				GenerateStagingVendorInfo(parameter, result); //2.3.2.3
				GenerateStagingVendorPayment_Tax(parameter, result); //2.3.2.4
				GenerateStagingVendorPayment_OffsetAccount(parameter, result); //2.3.2.5
				#endregion 2.3.2

				#region 2.3.3, 2.3.4
				var query233 = result.StagingTable.Where(w => w.ProcessTransType == (int)ProcessTransType.VendorPayment)
												  .GroupBy(g => new { g.RefType, g.RefGUID })
												  .Select(s => new { s.Key.RefType, s.Key.RefGUID, AmountMST = s.Sum(sum => sum.AmountMST) })
												  .Where(w => w.AmountMST != 0)
												  .ToList();

				if (!ConditionService.IsEqualZero(query233.Count()))
				{
					result.GenStagingTableErrorList.AddRange(query233.Select(s => new GenStagingTableErrorList
					{
						TableName = "LABEL.STAGING_TABLE",
						FieldName = StagingTableCondition.RefGUID,
						Reference = "Staging balance",
						RefValue = "Reference: {{0}} {{1}}",
						RefValueArgs = new string[] { ((RefType)s.RefType).GetAttrCode(), s.RefGUID.GuidNullToString() },
						ProcessTransType = null,
						ErrorCode = "ERROR.90138",
						ErrorCodeArgs = new string[] { ((RefType)result.StagingTable.Where(w => w.RefGUID == s.RefGUID).Select(s => s.SourceRefType).FirstOrDefault()).GetAttrCode(),
														result.StagingTable.Where(w => w.RefGUID == s.RefGUID).Select(s => s.SourceRefId).FirstOrDefault(),
														s.AmountMST.DecimalToString() },
						RefGUID = s.RefGUID,
						ProductType = null
					}));

					result.StagingTable.RemoveAll(r => query233.Any(a => a.RefType == r.RefType && a.RefGUID == r.RefGUID));

					var query23412 = result.StagingTable.Where(w => w.ProcessTransType == (int)ProcessTransType.VendorPayment)
														.GroupBy(g => new { g.ProcessTransGUID })
														.Select(s => new { s.Key.ProcessTransGUID }).ToList();
					result.StagingTableVendorInfo.RemoveAll(r => !query23412.Any(a => a.ProcessTransGUID == r.ProcessTransGUID));
				}
				#endregion 2.3.3, 2.3.4
			}
            catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void ValidateTransTypeVendorPayment(GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				#region 2.3.1.1 IF InputTaxLedgerAccount = NULL
				List<GenStagingTableErrorList> errorListInputTax = 
					(from list in parameter.ProcessTransVendorPaymTrans
					join tt in parameter.TaxTable
					on list.ProcessTrans.TaxTableGUID equals tt.TaxTableGUID
					where ConditionService.IsNotNullAndNotEmptyGUID(list.ProcessTrans.TaxTableGUID)
					&& list.ProcessTrans.TaxAmountMST != 0
					&& ConditionService.IsNullOrEmpty(tt.InputTaxLedgerAccount)
					select new GenStagingTableErrorList
					{
						TableName = typeof(ProcessTrans).Name,
						FieldName = TaxTableCondition.TaxTableGUID,
						Reference = "LABEL.VENDOR_PAYMENT",
						RefValue = "{{0}}: {{1}}",
						RefValueArgs = new string[] { "LABEL.TAX_CODE", tt.TaxCode },
						ProcessTransType = (int)ProcessTransType.VendorPayment,
						ErrorCode = "ERROR.90118",
						ErrorCodeArgs = new string[] { "LABEL.INPUT_TAX_LEDGER_ACCOUNT", "LABEL.TAX_CODE", tt.TaxCode },
						RefGUID = list.ProcessTrans.RefGUID,
						ProductType = null
					}).ToList();

				if(errorListInputTax.Count() > 0)
					result.GenStagingTableErrorList.AddRange(errorListInputTax);
				#endregion 2.3.1.1 IF InputTaxLedgerAccount = NULL

				#region 2.3.1.2 IF OffsetAccount = NULL
				List<GenStagingTableErrorList> errorListOffsetAccount =
					parameter.ProcessTransVendorPaymTrans.Where(w => ConditionService.IsNullOrEmpty(w.VendorPaymentTrans.OffsetAccount))
					.Select(s =>
						new GenStagingTableErrorList
						{
							TableName = typeof(VendorPaymentTrans).Name,
							FieldName = VendorPaymentTransCondition.OffsetAccount,
							Reference = "LABEL.VENDOR_PAYMENT",
							RefValue = "{{0}}: {{1}}",
							RefValueArgs = new string[] { "LABEL.VENDOR_PAYMENT_TRANSACTION", s.ProcessTrans.DocumentId},
							ProcessTransType = (int)ProcessTransType.VendorPayment,
							ErrorCode = "ERROR.90118",
							ErrorCodeArgs = new string[] { "LABEL.OFFSET_ACCOUNT", "LABEL.VENDOR_PAYMENT_TRANSACTION", s.ProcessTrans.DocumentId },
							RefGUID = s.ProcessTrans.RefGUID,
							ProductType = null
						}).ToList();

				if (errorListOffsetAccount.Count() > 0)
					result.GenStagingTableErrorList.AddRange(errorListOffsetAccount);
				#endregion 2.3.1.2 IF OffsetAccount = NULL
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingVendorPayment_AP(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
                #region 2.3.2.2
				var query2322 = (from st in parameter.ProcessTransVendorPaymTransNoError
								join stt in parameter.StagingTransText
								on st.ProcessTrans.ProcessTransType equals stt.ProcessTransType
								join vd in parameter.VendorTable
								on st.VendorPaymentTrans.VendorTableGUID equals vd.VendorTableGUID
								join ld1 in parameter.LedgerDimension
								on st.VendorPaymentTrans.Dimension1 equals ld1.LedgerDimensionGUID into ljld1
								from ld1 in ljld1.DefaultIfEmpty()
								join ld2 in parameter.LedgerDimension
								on st.VendorPaymentTrans.Dimension2 equals ld2.LedgerDimensionGUID into ljld2
								from ld2 in ljld2.DefaultIfEmpty()
								join ld3 in parameter.LedgerDimension
								on st.VendorPaymentTrans.Dimension3 equals ld3.LedgerDimensionGUID into ljld3
								from ld3 in ljld3.DefaultIfEmpty()
								join ld4 in parameter.LedgerDimension
								on st.VendorPaymentTrans.Dimension4 equals ld4.LedgerDimensionGUID into ljld4
								from ld4 in ljld4.DefaultIfEmpty()
								join ld5 in parameter.LedgerDimension
								on st.VendorPaymentTrans.Dimension5 equals ld5.LedgerDimensionGUID into ljld5
								from ld5 in ljld5.DefaultIfEmpty()
								select new { st, stt, vd, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2322.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.st.ProcessTrans.RefType,
					RefGUID = s.st.ProcessTrans.RefGUID,
					SourceRefType = s.st.ProcessTrans.SourceRefType,
					SourceRefId = s.st.ProcessTrans.SourceRefId,
					DocumentId = s.st.ProcessTrans.DocumentId,
					TransDate = s.st.ProcessTrans.TransDate,
					ProductType = s.st.ProcessTrans.ProductType,
					ProcessTransType = s.st.ProcessTrans.ProcessTransType,
					AccountType = (int)AccountType.Vendor,
					AccountNum = s.vd.VendorId,
					TransText = s.stt.TransText,
					AmountMST = s.st.ProcessTrans.AmountMST * (-1),
					DimensionCode1 = (s.st.VendorPaymentTrans.Dimension1 == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.st.VendorPaymentTrans.Dimension2 == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.st.VendorPaymentTrans.Dimension3 == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.st.VendorPaymentTrans.Dimension4 == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.st.VendorPaymentTrans.Dimension5 == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = s.st.VendorPaymentTrans.VendorTaxInvoiceId,
					WHTCode = String.Empty,
					ProcessTransGUID = s.st.ProcessTrans.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = parameter.Company.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.3.2.2
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingVendorInfo(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				#region 2.3.2.3
				ICurrencyRepo currencyRepo = new CurrencyRepo(db);
				IVendGroupRepo vendGroupRepo = new VendGroupRepo(db);
				IBankGroupRepo bankGroupRepo = new BankGroupRepo(db);
				IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
				IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
				IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
				IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
				IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);

				List<Currency> currencies = currencyRepo.GetCurrencyByCompanyNoTracking(parameter.DefaultCompanyGUID);
				List<VendGroup> vendGroups = vendGroupRepo.GetVendGroupByCompanyNoTracking(parameter.DefaultCompanyGUID);
				List<BankGroup> bankGroups = bankGroupRepo.GetBankGroupByCompanyNoTracking(parameter.DefaultCompanyGUID);
				List<AddressPostalCode> addressPostalCodes = addressPostalCodeRepo.GetAddressPostalCodeByCompanyNoTracking(parameter.DefaultCompanyGUID);
				List<AddressCountry> addressCountries = addressCountryRepo.GetAddressCountryByCompanyNoTracking(parameter.DefaultCompanyGUID);
				List<AddressProvince> addressProvinces = addressProvinceRepo.GetAddressProvinceByCompanyNoTracking(parameter.DefaultCompanyGUID);
				List<AddressDistrict> addressDistricts = addressDistrictRepo.GetAddressDistrictByCompanyNoTracking(parameter.DefaultCompanyGUID);
				List<AddressSubDistrict> addressSubDistricts = addressSubDistrictRepo.GetAddressSubDistrictByCompanyNoTracking(parameter.DefaultCompanyGUID);

				var query2323 = (from st in parameter.ProcessTransVendorPaymTransNoError
								 join vd in parameter.VendorTable on st.VendorPaymentTrans.VendorTableGUID equals vd.VendorTableGUID into ljvd
								 from vd in ljvd.DefaultIfEmpty()
								 join cc in currencies on vd.CurrencyGUID equals cc.CurrencyGUID
								 join vg in vendGroups on vd.VendGroupGUID equals vg.VendGroupGUID
								 join vb in parameter.VendPrimaryBank on vd.VendorTableGUID equals vb.VendorTableGUID into ljvb
								 from vb in ljvb.DefaultIfEmpty()
								 join at in parameter.VendPrimaryAddressTrans on vd.VendorTableGUID equals at.RefGUID into ljat
								 from at in ljat.DefaultIfEmpty()
								 join ld1 in parameter.LedgerDimension on st.VendorPaymentTrans.Dimension1 equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension on st.VendorPaymentTrans.Dimension2 equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension on st.VendorPaymentTrans.Dimension3 equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension on st.VendorPaymentTrans.Dimension4 equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension on st.VendorPaymentTrans.Dimension5 equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 select new { st, vd, cc, vg, vb, at, ld1, ld2, ld3, ld4, ld5 }).ToList();

                result.StagingTableVendorInfo.AddRange(query2323.Select(s => new StagingTableVendorInfo
                {
                    StagingTableVendorInfoGUID = Guid.NewGuid(),
                    VendorId = s.vd.VendorId,
                    RecordType = s.vd.RecordType,
                    Name = s.vd.Name,
                    AltName = s.vd.AltName,
                    CurrencyId = s.cc.CurrencyId,
                    VendGroupId = s.vg.VendGroupId,
                    VendorTaxId = s.vd.TaxId,
                    BankAccountName = s.vb != null ? s.vb.BankAccountName : String.Empty,
                    BankAccount = s.vb != null ? s.vb.BankAccount : String.Empty,
                    BankGroupId = s.vb != null ? bankGroups.Where(w => w.BankGroupGUID == s.vb.BankGroupGUID)
															.Select(s => s.BankGroupId).FirstOrDefault() : String.Empty,
                    BankBranch = s.vb != null ? s.vb.BankBranch : String.Empty,
                    AddressName = s.at != null ? s.at.Name : String.Empty,
                    CountryId = s.at != null ? addressCountries.Where(w => w.AddressCountryGUID == s.at.AddressCountryGUID)
																.Select(s => s.CountryId).FirstOrDefault() : String.Empty,
                    PostalCode = s.at != null ? addressPostalCodes.Where(w => w.AddressPostalCodeGUID == s.at.AddressPostalCodeGUID)
																	.Select(s => s.PostalCode).FirstOrDefault() : String.Empty,
                    Address = s.at != null ? s.at.Address1 : String.Empty,
                    DistrictId = s.at != null ? addressDistricts.Where(w => w.AddressDistrictGUID == s.at.AddressDistrictGUID)
																.Select(s => s.DistrictId).FirstOrDefault() : String.Empty,
                    SubDistrictId = s.at != null ? addressSubDistricts.Where(w => w.AddressSubDistrictGUID == s.at.AddressSubDistrictGUID)
																		.Select(s => s.SubDistrictId).FirstOrDefault() : String.Empty,
                    ProvinceId = s.at != null ? addressProvinces.Where(w => w.AddressProvinceGUID == s.at.AddressProvinceGUID)
																.Select(s => s.ProvinceId).FirstOrDefault() : String.Empty,
                    TaxBranchId = s.at != null ? s.at.TaxBranchId : String.Empty,
                    VendorPaymentTransGUID = s.st.VendorPaymentTrans.VendorPaymentTransGUID,
                    ProcessTransGUID = s.st.ProcessTrans.ProcessTransGUID,
                    CompanyGUID = s.st.ProcessTrans.CompanyGUID,
                    CompanyId = parameter.Company.CompanyId
                }).ToList());
                #endregion 2.3.2.3
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingVendorPayment_Tax(GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				#region 2.3.2.4
				var query2324 = (from st in parameter.ProcessTransVendorPaymTransNoError.Where(w => ConditionService.IsNotNullAndNotEmptyGUID(w.ProcessTrans.TaxTableGUID)
																									&& w.ProcessTrans.TaxAmountMST != 0)
								 join stt in parameter.StagingTransText
								 on st.ProcessTrans.ProcessTransType equals stt.ProcessTransType
								 join tt in parameter.TaxTable
								 on st.ProcessTrans.TaxTableGUID equals tt.TaxTableGUID
								 join stvi in result.StagingTableVendorInfo
								 on st.ProcessTrans.ProcessTransGUID equals stvi.ProcessTransGUID
								 join ld1 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension1 equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension2 equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension3 equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension4 equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension5 equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 select new { st, stt, tt, stvi, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2324.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.st.ProcessTrans.RefType,
					RefGUID = s.st.ProcessTrans.RefGUID,
					SourceRefType = s.st.ProcessTrans.SourceRefType,
					SourceRefId = s.st.ProcessTrans.SourceRefId,
					DocumentId = s.st.ProcessTrans.DocumentId,
					TransDate = s.st.ProcessTrans.TransDate,
					ProductType = s.st.ProcessTrans.ProductType,
					ProcessTransType = s.st.ProcessTrans.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.tt.InputTaxLedgerAccount,
					TransText = s.stt.TransText,
					AmountMST = s.st.ProcessTrans.TaxAmountMST,
					DimensionCode1 = (s.st.VendorPaymentTrans.Dimension1 == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.st.VendorPaymentTrans.Dimension2 == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.st.VendorPaymentTrans.Dimension3 == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.st.VendorPaymentTrans.Dimension4 == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.st.VendorPaymentTrans.Dimension5 == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = s.tt.TaxCode,
					TaxBaseAmount = s.st.VendorPaymentTrans.AmountBeforeTax,
					TaxId = s.stvi.VendorTaxId,
					TaxAccountId = s.stvi.VendorId,
					TaxAccountName = s.stvi.Name,
					TaxBranchId = s.stvi.TaxBranchId,
					TaxAddress = s.stvi.Address,
					VendorTaxInvoiceId = s.st.VendorPaymentTrans.VendorTaxInvoiceId,
					WHTCode = String.Empty,
					ProcessTransGUID = s.st.ProcessTrans.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = s.st.ProcessTrans.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.3.2.4
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenerateStagingVendorPayment_OffsetAccount(GenStagingTableParameter parameter, GenStagingTableResult result)
		{
			try
			{
				#region 2.3.2.5
				var query2325 = (from st in parameter.ProcessTransVendorPaymTransNoError
								 join stt in parameter.StagingTransText
								 on st.ProcessTrans.ProcessTransType equals stt.ProcessTransType
								 join ld1 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension1 equals ld1.LedgerDimensionGUID into ljld1
								 from ld1 in ljld1.DefaultIfEmpty()
								 join ld2 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension2 equals ld2.LedgerDimensionGUID into ljld2
								 from ld2 in ljld2.DefaultIfEmpty()
								 join ld3 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension3 equals ld3.LedgerDimensionGUID into ljld3
								 from ld3 in ljld3.DefaultIfEmpty()
								 join ld4 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension4 equals ld4.LedgerDimensionGUID into ljld4
								 from ld4 in ljld4.DefaultIfEmpty()
								 join ld5 in parameter.LedgerDimension
								 on st.VendorPaymentTrans.Dimension5 equals ld5.LedgerDimensionGUID into ljld5
								 from ld5 in ljld5.DefaultIfEmpty()
								 select new { st, stt, ld1, ld2, ld3, ld4, ld5 }).ToList();

				result.StagingTable.AddRange(query2325.Select(s => new StagingTable
				{
					StagingTableGUID = Guid.NewGuid(),
					StagingBatchId = parameter.StagingBatchId,
					RefType = s.st.ProcessTrans.RefType,
					RefGUID = s.st.ProcessTrans.RefGUID,
					SourceRefType = s.st.ProcessTrans.SourceRefType,
					SourceRefId = s.st.ProcessTrans.SourceRefId,
					DocumentId = s.st.ProcessTrans.DocumentId,
					TransDate = s.st.ProcessTrans.TransDate,
					ProductType = s.st.ProcessTrans.ProductType,
					ProcessTransType = s.st.ProcessTrans.ProcessTransType,
					AccountType = (int)AccountType.Ledger,
					AccountNum = s.st.VendorPaymentTrans.OffsetAccount,
					TransText = s.stt.TransText,
					AmountMST = s.st.VendorPaymentTrans.AmountBeforeTax,
					DimensionCode1 = (s.st.VendorPaymentTrans.Dimension1 == null) ? String.Empty : s.ld1.DimensionCode,
					DimensionCode2 = (s.st.VendorPaymentTrans.Dimension2 == null) ? String.Empty : s.ld2.DimensionCode,
					DimensionCode3 = (s.st.VendorPaymentTrans.Dimension3 == null) ? String.Empty : s.ld3.DimensionCode,
					DimensionCode4 = (s.st.VendorPaymentTrans.Dimension4 == null) ? String.Empty : s.ld4.DimensionCode,
					DimensionCode5 = (s.st.VendorPaymentTrans.Dimension5 == null) ? String.Empty : s.ld5.DimensionCode,
					CompanyTaxBranchId = parameter.CompanyTaxBranchId,
					TaxInvoiceId = String.Empty,
					TaxCode = String.Empty,
					TaxBaseAmount = 0,
					TaxId = String.Empty,
					TaxAccountId = String.Empty,
					TaxAccountName = String.Empty,
					TaxBranchId = String.Empty,
					TaxAddress = String.Empty,
					VendorTaxInvoiceId = String.Empty,
					WHTCode = String.Empty,
					ProcessTransGUID = s.st.ProcessTrans.ProcessTransGUID,
					InterfaceStagingBatchId = String.Empty,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = s.st.ProcessTrans.CompanyGUID,
					CompanyId = parameter.Company.CompanyId
				}).ToList());
				#endregion 2.3.2.5
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion 2.3 Generate Staging - VendorPayment

		#region Replace Staging TransText
		private void ReplaceStagingTableTransText(GenStagingTableParameter parameter, GenStagingTableResult result)
        {
            try
            {
				var query = (from stagingTable in result.StagingTable
							 join processTrans in result.ProcessTrans
							 on stagingTable.ProcessTransGUID equals processTrans.ProcessTransGUID into ljProcessTrans
							 from processTrans in ljProcessTrans.DefaultIfEmpty()
							 select new { stagingTable, processTrans }).ToList();

				// %1 = ProductType, %2 = DocumentId, %3 = SourceRefId, %5 = VendorId
				query.Select(s =>
				{
					s.stagingTable.TransText = s.stagingTable.TransText.Replace("%1", SystemStaticData.GetTranslatedMessage(((ProductType)(s.stagingTable.ProductType)).GetAttrCode()));
					s.stagingTable.TransText = s.stagingTable.TransText.Replace("%2", s.stagingTable.DocumentId);
					s.stagingTable.TransText = s.stagingTable.TransText.Replace("%3", s.stagingTable.SourceRefId);
					s.stagingTable.TransText = s.stagingTable.ProcessTransGUID == null ? s.stagingTable.TransText.Replace("%4", String.Empty) 
																		: s.stagingTable.TransText;
					s.stagingTable.TransText = s.stagingTable.AccountType == (int)AccountType.Vendor ? s.stagingTable.TransText.Replace("%5", s.stagingTable.AccountNum)
																										: s.stagingTable.TransText.Replace("%5", String.Empty); 
					return s.stagingTable;
				}).ToList();

				// %4 = CustomerId
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				List<CustomerTable> customerTables = customerTableRepo.GetCustomerTableByIdNoTracking(result.ProcessTrans.GroupBy(g => g.CustomerTableGUID).Select(s => s.Key).ToList());
				var joinCustomer = (from q in query.Where(w => w.stagingTable.ProcessTransGUID != null)
									join customerTable in customerTables
									on q.processTrans.CustomerTableGUID equals customerTable.CustomerTableGUID
									select new { q.stagingTable, customerTable.CustomerId });

				joinCustomer.Select(s =>
				{
					s.stagingTable.TransText = s.stagingTable.TransText.Replace("%4", s.CustomerId);
					return s.stagingTable;
				}).ToList();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion Replace Staging TransText

        #region GetSmartAppExceptionFromGenStagingTableErrorList
		public SmartAppException GetSmartAppExceptionFromGenStagingTableErrorList(List<GenStagingTableErrorList> errorList)
        {
            try
            {
				if(errorList != null && errorList.Count > 0)
                {
					SmartAppException expectedError = new SmartAppException("ERROR.ERROR");
                    foreach (var error in errorList)
                    {
						BatchLogReference batchRef = new BatchLogReference(
														new TranslateModel { Code = error.Reference },
														new TranslateModel { Code = error.RefValue, Parameters = error.RefValueArgs });
						batchRef.Key = error.ErrorKey.GuidNullToString();
						expectedError.AddData(batchRef, error.ErrorCode, error.ErrorCodeArgs);
                    }
					return expectedError;
                }
				return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#endregion
		#region Generate Accounting staging
		public void GenerateStagingAccounting(GenerateAccountingStagingView item)
		{
			GenStagingTableResult genStagingTableResult = new GenStagingTableResult();
			try
			{
				GenStagingTableParameter genStagingTableParameter = new GenStagingTableParameter
				{
					ProcessTransType = item.ListProcessTransType,
					StagingBatchStatus = item.StagingBatchStatus
				};
				genStagingTableResult = GenStagingTableByProcessTransType(genStagingTableParameter);
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    if (genStagingTableResult.StagingTable.Any())
                    {
						BulkInsert(genStagingTableResult.StagingTable);
                    }
					if (genStagingTableResult.StagingTableVendorInfo.Any())
					{
						BulkInsert(genStagingTableResult.StagingTableVendorInfo);
					}
					if (genStagingTableResult.ProcessTrans.Any())
					{
						BulkUpdate(genStagingTableResult.ProcessTrans);
					}
					UnitOfWork.Commit(transaction);

					if (genStagingTableResult.GenStagingTableErrorList.Count > 0)
					{
						SmartAppException expectedError = GetSmartAppExceptionFromGenStagingTableErrorList(genStagingTableResult.GenStagingTableErrorList);
						throw expectedError;
					}
					else
					{
						LogBatchSuccess(null, null, "SUCCESS.90019", new string[] { "LABEL.STATGING_TRANSACTION", "LABEL.STAGING_BATCH_ID", genStagingTableResult.StagingBatchId }, null, null);
					}
				}
			}
			catch (Exception e)
			{
				SmartAppException expectedError = GetSmartAppExceptionFromGenStagingTableErrorList(genStagingTableResult.GenStagingTableErrorList);
				throw SmartAppUtil.AddStackTrace(e, expectedError);
			}
		}
		#endregion
		#region Generate Vendor invoice staging
		public void GenerateStagingVendorInvoice(GenerateVendorInvoiceStagingView item)
		{
			GenStagingTableResult genStagingTableResult = new GenStagingTableResult();
			try
			{
				GenStagingTableParameter genStagingTableParameter = new GenStagingTableParameter
				{
					ProcessTransType = item.ListProcessTransType,
					StagingBatchStatus = item.StagingBatchStatus
				};
				genStagingTableResult = GenStagingTableByProcessTransType(genStagingTableParameter);
				using (var transaction = UnitOfWork.ContextTransaction())
				{
					if (genStagingTableResult.StagingTable.Any())
					{
						BulkInsert(genStagingTableResult.StagingTable);
					}
					if (genStagingTableResult.StagingTableVendorInfo.Any())
					{
						BulkInsert(genStagingTableResult.StagingTableVendorInfo);
					}
					if (genStagingTableResult.ProcessTrans.Any())
					{
						BulkUpdate(genStagingTableResult.ProcessTrans);
					}
					UnitOfWork.Commit(transaction);

					if (genStagingTableResult.GenStagingTableErrorList.Count > 0)
					{
						SmartAppException expectedError = GetSmartAppExceptionFromGenStagingTableErrorList(genStagingTableResult.GenStagingTableErrorList);
						throw expectedError;
					}
					else
					{
						LogBatchSuccess(null, null, "SUCCESS.90019", new string[] { "LABEL.STATGING_TRANSACTION", "LABEL.STAGING_BATCH_ID", genStagingTableResult.StagingBatchId }, null, null);
					}
				}
			}
			catch (Exception e)
			{
				SmartAppException expectedError = GetSmartAppExceptionFromGenStagingTableErrorList(genStagingTableResult.GenStagingTableErrorList);
				throw SmartAppUtil.AddStackTrace(e, expectedError);
			}
		}
		#endregion

	}
}
