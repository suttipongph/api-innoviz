using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IEmployeeTableService
	{

		EmployeeTableItemView GetEmployeeTableById(string id);
		EmployeeTableItemView CreateEmployeeTable(EmployeeTableItemView employeeTableView);
		EmployeeTableItemView UpdateEmployeeTable(EmployeeTableItemView employeeTableView);
		bool DeleteEmployeeTable(string id);
		EmployeeTableItemView GetEmployeeTableByUserIdAndCompany(string userId, string companyGUID);
		bool IsManual(string companyId);
		ContactTransItemView GetContactTransInitialData(string refGUID);
		IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableByInputCompanyDropDown(SearchParameter search);
		IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableByNotCurrentEmpDropDown(SearchParameter search);
		IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableWithUserDropDown(SearchParameter search);
		IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableWithCompanySignatureDropDown(SearchParameter search);
		#region Shared
		string GetReportToUsernamedByUserId(string userId, string companyGUID);
		#endregion Shared
		void CreateEmployeeTableCollection(List<EmployeeTableItemView> employeeTableItemViews);
		IEnumerable<SelectItem<EmployeeTableItemView>> GetActiveEmployeeTableDropDown(SearchParameter search);
	}
	public class EmployeeTableService : SmartAppService, IEmployeeTableService
	{
		public EmployeeTableService(SmartAppDbContext context) : base(context) { }
		public EmployeeTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public EmployeeTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public EmployeeTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public EmployeeTableService() : base() { }

		public EmployeeTableItemView GetEmployeeTableById(string id)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				return employeeTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public EmployeeTableItemView CreateEmployeeTable(EmployeeTableItemView employeeTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				employeeTableView = accessLevelService.AssignOwnerBU(employeeTableView);
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				EmployeeTable employeeTable = employeeTableView.ToEmployeeTable();
				ValidateOnSaveEmployeeTable(employeeTable);
				GenNumberSeqCode(employeeTable);
				employeeTable = employeeTableRepo.CreateEmployeeTable(employeeTable);
				base.LogTransactionCreate<EmployeeTable>(employeeTable);
				UnitOfWork.Commit();
				return employeeTable.ToEmployeeTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public EmployeeTableItemView UpdateEmployeeTable(EmployeeTableItemView employeeTableView)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				EmployeeTable inputEmployeeTable = employeeTableView.ToEmployeeTable();
				ValidateOnSaveEmployeeTable(inputEmployeeTable);
				EmployeeTable dbEmployeeTable = employeeTableRepo.Find(inputEmployeeTable.EmployeeTableGUID);				
				dbEmployeeTable = employeeTableRepo.UpdateEmployeeTable(dbEmployeeTable, inputEmployeeTable);
				base.LogTransactionUpdate<EmployeeTable>(GetOriginalValues<EmployeeTable>(dbEmployeeTable), dbEmployeeTable);
				UnitOfWork.Commit();
				return dbEmployeeTable.ToEmployeeTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public bool ValidateOnSaveEmployeeTable(EmployeeTable employeeTable)
        {
            try
            {
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (employeeTable.AssistMD && employeeTable.UserId == null)
                {
					ex.AddData("ERROR.90129", new string[] { "LABEL.ASSIST_MD", "LABEL.USER_ID" });
				}
				if(ex.Data.Count > 0)
                {
					throw ex;
				}
				else
                {
					return true;
                }
            }
            catch (Exception e)
            {
				throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public bool DeleteEmployeeTable(string item)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				Guid employeeTableGUID = new Guid(item);
				EmployeeTable employeeTable = employeeTableRepo.Find(employeeTableGUID);
				employeeTableRepo.Remove(employeeTable);
				base.LogTransactionDelete<EmployeeTable>(employeeTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public EmployeeTableItemView GetEmployeeTableByUserIdAndCompany(string userId, string companyGUID)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				return employeeTableRepo.GetEmployeeTableByUserIdAndCompany(userId, companyGUID);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsManual(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.Employee);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenNumberSeqCode(EmployeeTable employeeTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(employeeTable.CompanyGUID, ReferenceId.Employee);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(employeeTable.CompanyGUID, ReferenceId.Employee);
					employeeTable.EmployeeId = numberSequenceService.GetNumber(employeeTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public ContactTransItemView GetContactTransInitialData(string refGUID)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db);
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				string refId = employeeTableRepo.GetEmployeeTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).EmployeeId;
				return ContactTransService.GetContactTransInitialData(refId, refGUID, Models.Enum.RefType.Employee);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableByInputCompanyDropDown(SearchParameter search)
        {
            try
            {
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				search = search.GetParentCondition(EmployeeCondition.CompanyGUID);
				return employeeTableRepo.GetDropDownByInputCompany(search);
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

		public IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableByNotCurrentEmpDropDown(SearchParameter search)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				search = search.GetParentCondition(EmployeeCondition.EmployeeTableGUID);
				search.Conditions[0].EqualityOperator = Operators.NOT_EQUAL;
				return employeeTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableWithUserDropDown(SearchParameter search)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				return employeeTableRepo.GetDropDownItemForWF(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeTableWithCompanySignatureDropDown(SearchParameter search)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				return employeeTableRepo.GetDropDownItemWithCompanySignature(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<EmployeeTableItemView>> GetActiveEmployeeTableDropDown(SearchParameter search)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				SearchCondition searchCondition = SearchConditionService.GetEmployeeActiveCondition();
				search.Conditions.Add(searchCondition);
				return employeeTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Shared
		public string GetReportToUsernamedByUserId(string userId, string companyGUID)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				SysUserTable sysUserTable = employeeTableRepo.GetSysUserTableByReportToEmployeeTable(userId.StringToGuid(), companyGUID.StringToGuid());
				return (sysUserTable != null) ? sysUserTable.UserName : null;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Shared
		#region migration
		public void CreateEmployeeTableCollection(List<EmployeeTableItemView> employeeTableItemViews)
		{
			try
			{
				if (employeeTableItemViews.Any())
				{
					IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
					List<EmployeeTable> employeeTable = employeeTableItemViews.ToEmployeeTable().ToList();
					employeeTableRepo.ValidateAdd(employeeTable);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(employeeTable, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion
	}
}
