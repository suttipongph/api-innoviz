using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICustomerRefundTableService
	{

		CustomerRefundTableItemView GetCustomerRefundTableById(string id);
		CustomerRefundTableItemView CreateCustomerRefundTable(CustomerRefundTableItemView customerRefundTableView);
		CustomerRefundTableItemView UpdateCustomerRefundTable(CustomerRefundTableItemView customerRefundTableView);
		CustomerRefundTable UpdateCustomerRefundTable(CustomerRefundTable customerRefundTable);
		bool DeleteCustomerRefundTable(string id);
		CustomerRefundTableItemView GetCustomerRefundTableInitialData(string companyId, int suspenseInvoiceType);
		string GetReferenceIdBySuspenseInvoiceType(int suspenseInvoiceType);
		bool IsManualByCustomerRefundTable(string companyId, string referenceId);
		CustomerRefundTableItemView GetExChangeRate(CustomerRefundTableItemView input);
		AccessModeView GetCustomerRefundTableAccessMode(string customerRefundTableGUID);
		RetentionConditionTransItemView GetRetentionConditionTransByCustomerRefund(string creditAppTableGUID);
		PaymentDetailItemView GetPaymentDetailInitialData(string refGUID);
		ServiceFeeTransItemView GetServiceFeeTransInitialDataByCustomerRefundTable(string refGUID);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemCustomerRefundStatus(SearchParameter search);
		PrintCustomerReserveRefund GetPrintCustomerRefundReserveById (string refGUID);
		PrintCustomerRetentionRefund GetPrintCustomerRefundRetentionById(string refGUID);
		void UpdateCustomerRefundTableServiceFeeAmount(ServiceFeeTransItemView serviceFeeTransView);
		void UpdateCustomerRefundTableServiceFeeAmountOnRemove(ServiceFeeTrans serviceFeeTrans);
		void UpdateCustomerRefundTablePaymentAmount(PaymentDetailItemView paymentDetailView);
		void UpdateCustomerRefundTablePaymentAmountOnRemove(PaymentDetail paymentDetail);
		MemoTransItemView GetMemoTransInitialData(string refGUID);
		bool ValidateIsNotInvoiceSettlementDetail(string id);
		#region functions
		CancelCustomerRefundView GetCancelCustomerRefundTableById(string customerRefundTableGUID);
		CancelCustomerRefundResultView CancelCustomerRefundTable(CancelCustomerRefundView customerRefundTable);
		PostCustomerRefundResultView PostCustomerRefundTable(PostCustomerRefundView customerRefundTable);
		PostCustomerRefundResultView InitPostCustomerRefundTable(PostCustomerRefundView customerRefundTable);
		PostCustomerRefundView GetPostCustomerRefundTableById(string customerRefundTableGUID);
		#endregion
	}
	public class CustomerRefundTableService : SmartAppService, ICustomerRefundTableService
	{
		public CustomerRefundTableService(SmartAppDbContext context) : base(context) { }
		public CustomerRefundTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CustomerRefundTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustomerRefundTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CustomerRefundTableService() : base() { }

		public CustomerRefundTableItemView GetCustomerRefundTableById(string id)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				return customerRefundTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerRefundTableItemView CreateCustomerRefundTable(CustomerRefundTableItemView customerRefundTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				customerRefundTableView = accessLevelService.AssignOwnerBU(customerRefundTableView);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTable customerRefundTable = customerRefundTableView.ToCustomerRefundTable();
				GenCustomerRefundTableNumberSeqCode(customerRefundTable);
				customerRefundTable = customerRefundTableRepo.CreateCustomerRefundTable(customerRefundTable);
				base.LogTransactionCreate<CustomerRefundTable>(customerRefundTable);
				UnitOfWork.Commit();
				return customerRefundTable.ToCustomerRefundTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerRefundTableItemView UpdateCustomerRefundTable(CustomerRefundTableItemView customerRefundTableView)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTable inputCustomerRefundTable = customerRefundTableView.ToCustomerRefundTable();
				CustomerRefundTable dbCustomerRefundTable = customerRefundTableRepo.Find(inputCustomerRefundTable.CustomerRefundTableGUID);
				dbCustomerRefundTable = customerRefundTableRepo.UpdateCustomerRefundTable(dbCustomerRefundTable, inputCustomerRefundTable);
				base.LogTransactionUpdate<CustomerRefundTable>(GetOriginalValues<CustomerRefundTable>(dbCustomerRefundTable), dbCustomerRefundTable);
				UnitOfWork.Commit();
				return dbCustomerRefundTable.ToCustomerRefundTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerRefundTable UpdateCustomerRefundTable(CustomerRefundTable customerRefundTable)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTable inputCustomerRefundTable = customerRefundTable;
				CustomerRefundTable dbCustomerRefundTable = customerRefundTableRepo.Find(inputCustomerRefundTable.CustomerRefundTableGUID);
				dbCustomerRefundTable = customerRefundTableRepo.UpdateCustomerRefundTable(dbCustomerRefundTable, inputCustomerRefundTable);
				base.LogTransactionUpdate<CustomerRefundTable>(GetOriginalValues<CustomerRefundTable>(dbCustomerRefundTable), dbCustomerRefundTable);
				return customerRefundTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCustomerRefundTable(string item)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				Guid customerRefundTableGUID = new Guid(item);
				CustomerRefundTable customerRefundTable = customerRefundTableRepo.Find(customerRefundTableGUID);
				customerRefundTableRepo.Remove(customerRefundTable);
				base.LogTransactionDelete<CustomerRefundTable>(customerRefundTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerRefundTableItemView GetCustomerRefundTableInitialData(string companyId, int suspenseInvoiceType)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTrackingByAccessLevel(db.GetCompanyFilter().StringToGuid());
				EmployeeTable employeeTable = companyParameter.OperReportSignatureGUID.HasValue ? employeeTableRepo.GetEmployeeTableByIdNoTracking(companyParameter.OperReportSignatureGUID.Value) : new EmployeeTable();

				IExchangeRateService exchangeRateService = new ExchangeRateService(db);

				decimal exchangeRate = 0;

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(CustomerRefundStatus.Draft).ToString());

				if (companyParameter.HomeCurrencyGUID != null)
				{
					exchangeRate = exchangeRateService.GetExchangeRate(companyParameter.HomeCurrencyGUID.Value, DateTime.Now);
				}

				return new CustomerRefundTableItemView
				{
					CustomerRefundTableGUID = new Guid().GuidNullToString(),
					TransDate = DateTime.Now.DateToString(),
					CurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString(),
					ExchangeRate = exchangeRate,
					DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
					ProductType = suspenseInvoiceType == (int)SuspenseInvoiceType.ToBeRefunded ? (int)ProductType.Factoring : (int)ProductType.None,
					SuspenseInvoiceType = suspenseInvoiceType,
					DocumentStatus_StatusId = documentStatus.StatusId,
					OperReportSignatureGUID = !employeeTable.InActive ? companyParameter.OperReportSignatureGUID.GuidNullToString() : null
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public string GetReferenceIdBySuspenseInvoiceType(int suspenseInvoiceType)
		{
			try
			{
				string referenceId = null;
				switch (suspenseInvoiceType)
				{
					case (int)SuspenseInvoiceType.SuspenseAccount:
						referenceId = ReferenceId.SuspenseRefund;
						break;
					case (int)SuspenseInvoiceType.ToBeRefunded:
						referenceId = ReferenceId.ReserveRefund;
						break;
					case (int)SuspenseInvoiceType.Retention:
						referenceId = ReferenceId.RetentionRefund;
						break;
				}
				return referenceId;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#region NumberSeq
		public bool IsManualByCustomerRefundTable(string companyId, string referenceId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), referenceId);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenCustomerRefundTableNumberSeqCode(CustomerRefundTable customerRefundTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				string referenceId = GetReferenceIdBySuspenseInvoiceType(customerRefundTable.SuspenseInvoiceType);
				bool isManual = IsManualByCustomerRefundTable(customerRefundTable.CompanyGUID.GuidNullToString(), referenceId);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(customerRefundTable.CompanyGUID, referenceId);
					customerRefundTable.CustomerRefundId = numberSequenceService.GetNumber(customerRefundTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion
		public CustomerRefundTableItemView GetExChangeRate(CustomerRefundTableItemView input)
		{
			try
			{
				IExchangeRateService exchangeRateService = new ExchangeRateService(db);
				decimal exchangeRate = 0;
				if (input.CurrencyGUID != null)
				{
					exchangeRate = exchangeRateService.GetExchangeRate(input.CurrencyGUID.StringToGuid(), input.TransDate.StringToDate());
				}

				return new CustomerRefundTableItemView
				{
					CustomerRefundTableGUID = new Guid().GuidNullToString(),
					ExchangeRate = exchangeRate
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AccessModeView GetCustomerRefundTableAccessMode(string customerRefundTableGUID)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(customerRefundTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(customerRefundTable.DocumentStatusGUID.Value);

				bool condition = (documentStatus.StatusId == Convert.ToInt32(CustomerRefundStatus.Draft).ToString());

				return new AccessModeView
				{
					CanCreate = condition,
					CanDelete = condition,
					CanView = condition,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionTransItemView GetRetentionConditionTransByCustomerRefund(string creditAppTableGUID)
		{
			try
			{
				IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
				IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
				RetentionConditionTrans retentionConditionTrans = retentionConditionTransService.GetRetentionConditionTransByReferenceAndMethod(creditAppTableGUID.StringToGuid(), (int)RetentionDeductionMethod.ReserveRefund).FirstOrDefault();
				if (retentionConditionTrans != null)
				{
					return retentionConditionTrans.ToRetentionConditionTransItemView();
				}
				else
				{
					return null;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public PaymentDetailItemView GetPaymentDetailInitialData(string refGUID)
		{
			try
			{
				IPaymentDetailService paymentDetailService = new PaymentDetailService(db);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				string refId = customerRefundTableRepo.GetCustomerRefundTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerRefundId;
				return paymentDetailService.GetPaymentDetailInitialData(refId, refGUID, Models.Enum.RefType.CustomerRefund);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeTransItemView GetServiceFeeTransInitialDataByCustomerRefundTable(string refGUID)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				string refId = customerRefundTableRepo.GetCustomerRefundTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerRefundId;
				return serviceFeeTransService.GetServiceFeeTransInitialDataByCustomerRefundTable(refId, refGUID, Models.Enum.RefType.CustomerRefund);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemCustomerRefundStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.CustomerRefund);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool ValidateIsNotInvoiceSettlementDetail(string id)
		{
			try
			{
				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
				List<InvoiceSettlementDetail> invoiceSettlementDetails = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByReferanceNoTracking(id.StringToGuid(), (int)RefType.CustomerRefund).Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.ToBeRefunded).ToList();
				return ConditionService.IsEqualZero(invoiceSettlementDetails.Count());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region function
		#region cancel customerRefundTable
		public CancelCustomerRefundView GetCancelCustomerRefundTableById(string customerRefundTableGUID)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTableItemView customerRefundTable = customerRefundTableRepo.GetByIdvw(customerRefundTableGUID.StringToGuid());

				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(customerRefundTable.CustomerTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(customerRefundTable.DocumentStatusGUID.StringToGuid());

				CancelCustomerRefundView cancelCustomerRefundTableView = new CancelCustomerRefundView
				{
					CustomerRefundTableGUID = customerRefundTable.CustomerRefundTableGUID.GuidNullToString(),
					CustomerRefundTable_Values = SmartAppUtil.GetDropDownLabel(customerRefundTable.CustomerRefundId, customerRefundTable.Description),
					CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					DocumentReasonGUID = customerRefundTable.DocumentReasonGUID,
					DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.StatusId, documentStatus.Description),
					NetAmount = customerRefundTable.NetAmount,
					PaymentAmount = customerRefundTable.PaymentAmount,
					RetentionAmount = customerRefundTable.RetentionAmount,
					ServiceFeeAmount = customerRefundTable.ServiceFeeAmount,
					SettleAmount = customerRefundTable.SettleAmount,
					SuspenseAmount = customerRefundTable.SuspenseAmount,
					TransDate = customerRefundTable.TransDate
				};
				return cancelCustomerRefundTableView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetCancelCustomerRefundTableValidation(string customerRefundTableGUID)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(customerRefundTableGUID.StringToGuid());

				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus customerRefundTableStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CustomerRefundStatus.Draft).ToString());

				if (customerRefundTable.DocumentStatusGUID != customerRefundTableStatusDraft.DocumentStatusGUID)
				{
					ex.AddData("ERROR.90012", new string[] { "LABEL.CUSTOMER_REFUND" });
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CancelCustomerRefundResultView CancelCustomerRefundTable(CancelCustomerRefundView customerRefundTable)
		{
			try
			{
				// validate
				GetCancelCustomerRefundTableValidation(customerRefundTable.CustomerRefundTableGUID);
				CancelCustomerRefundResultView result = new CancelCustomerRefundResultView();
				NotificationResponse success = new NotificationResponse();
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTableItemView customerRefundTableItemView = customerRefundTableRepo.GetByIdvw(customerRefundTable.CustomerRefundTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus customerRefundTableStatusCancel = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CustomerRefundStatus.Cancelled).ToString());

				customerRefundTableItemView.DocumentStatusGUID = customerRefundTableStatusCancel.DocumentStatusGUID.GuidNullToString();
				customerRefundTableItemView.DocumentReasonGUID = customerRefundTable.DocumentReasonGUID.GuidNullToString();

				UpdateCustomerRefundTable(customerRefundTableItemView);

				success.AddData("SUCCESS.90008", new string[] { "LABEL.CUSTOMER_REFUND", SmartAppUtil.GetDropDownLabel(customerRefundTableItemView.CustomerRefundId, customerRefundTableItemView.Description) });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region post customerRefundTable
		public PostCustomerRefundView GetPostCustomerRefundTableById(string customerRefundTableGUID)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTableItemView customerRefundTable = customerRefundTableRepo.GetByIdvw(customerRefundTableGUID.StringToGuid());

				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(customerRefundTable.CustomerTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(customerRefundTable.DocumentStatusGUID.StringToGuid());

				PostCustomerRefundView postCustomerRefundTableView = new PostCustomerRefundView
				{
					CustomerRefundTableGUID = customerRefundTable.CustomerRefundTableGUID.GuidNullToString(),
					CustomerRefundTable_Values = SmartAppUtil.GetDropDownLabel(customerRefundTable.CustomerRefundId, customerRefundTable.Description),
					CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.StatusId, documentStatus.Description),
					NetAmount = customerRefundTable.NetAmount,
					PaymentAmount = customerRefundTable.PaymentAmount,
					RetentionAmount = customerRefundTable.RetentionAmount,
					ServiceFeeAmount = customerRefundTable.ServiceFeeAmount,
					SettleAmount = customerRefundTable.SettleAmount,
					SuspenseAmount = customerRefundTable.SuspenseAmount,
					TransDate = customerRefundTable.TransDate,
				};
				return postCustomerRefundTableView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetPostCustomerRefundTableValidation(string customerRefundTableGUID)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(customerRefundTableGUID.StringToGuid());

				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
				List<InvoiceSettlementDetail> invoiceSettlementDetailList = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByReferanceNoTracking(customerRefundTable.CustomerRefundTableGUID, Convert.ToInt32(RefType.CustomerRefund)).ToList();

				List<InvoiceSettlementDetail> invoiceSettlementDetailNoneList = invoiceSettlementDetailList
					.Where(t => t.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.None)).ToList();

				List<InvoiceSettlementDetail> invoiceSettlementDetailSuspenseList = invoiceSettlementDetailList
					.Where(t => t.SuspenseInvoiceType != Convert.ToInt32(SuspenseInvoiceType.None)).ToList();

				ICustTransRepo custTransRepo = new CustTransRepo(db);
				var custtransList = custTransRepo.GetCustTransByCompanyNoTracking(customerRefundTable.CompanyGUID)
					.GroupBy(g => g.InvoiceTableGUID).Select(t => new { invoiceTableGUID = t.Key, sumBalance = t.Sum(u => u.TransAmount - u.SettleAmount) }).ToList();

				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);

				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				CreditAppTable creditAppTable = (customerRefundTable.CreditAppTableGUID != null) ? creditAppTableRepo.GetCreditAppTableByIdNoTracking(customerRefundTable.CreditAppTableGUID.Value) : null;

				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				#region condition 1
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus customerRefundTableStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CustomerRefundStatus.Draft).ToString());
				if (customerRefundTable.DocumentStatusGUID != customerRefundTableStatusDraft.DocumentStatusGUID)
				{
					ex.AddData("ERROR.90012", new string[] { "LABEL.CUSTOMER_REFUND" });
				}
				#endregion

				#region condition 2
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				IEnumerable<PaymentDetail> paymentDetailList = paymentDetailRepo.GetPaymentDetailByReferanceNoTracking(customerRefundTable.CustomerRefundTableGUID, Convert.ToInt32(RefType.CustomerRefund));
				if (paymentDetailList.Count() > 0)
				{
					if (paymentDetailList.Sum(s => s.PaymentAmount) != customerRefundTable.PaymentAmount)
					{
						ex.AddData("ERROR.90040", new string[] { "LABEL.PAYMENT_AMOUNT" });
					}
				}
				else
				{
					if (customerRefundTable.PaymentAmount != 0)
					{
						ex.AddData("ERROR.90040", new string[] { "LABEL.PAYMENT_AMOUNT" });
					}
				}
				#endregion

				#region condition 3
				decimal condition3Var = customerRefundTable.SuspenseAmount - (customerRefundTable.ServiceFeeAmount + customerRefundTable.SettleAmount + customerRefundTable.RetentionAmount);
				if (customerRefundTable.PaymentAmount != condition3Var)
				{
					ex.AddData("ERROR.90086", new string[] { "LABEL.SUSPENSE_AMOUNT", "LABEL.SERVICE_FEE_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.RETENTION_AMOUNT" });
				}
				#endregion

				#region condition 4 
				if (invoiceSettlementDetailNoneList.Count > 0)
				{
					var condition4_result = from invoiceSettlementDetail in invoiceSettlementDetailNoneList
											join custTrans in custtransList
											on invoiceSettlementDetail.InvoiceTableGUID equals custTrans.invoiceTableGUID
											select new
											{
												invoiceTableGUID = invoiceSettlementDetail.InvoiceTableGUID,
												balanceAmount = invoiceSettlementDetail.BalanceAmount,
												sumBalance = custTrans.sumBalance,
											};

					if (condition4_result.Count() > 0)
					{
						foreach (var item in condition4_result)
						{
							if (item.balanceAmount != item.sumBalance)
							{
								InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(item.invoiceTableGUID.Value);
								ex.AddData("ERROR.90046", new string[] { "LABEL.INVOICE_SETTLEMENT_DETAIL", invoiceTable.InvoiceId, "LABEL.POST_CUSTOMER_REFUND" });
							}
						}
					}
				}
				#endregion

				#region condition 5
				if (invoiceSettlementDetailList.Count > 0)
				{
					var condition4_result = from invoiceSettlementDetail in invoiceSettlementDetailSuspenseList
											join custTrans in custtransList
											on invoiceSettlementDetail.InvoiceTableGUID equals custTrans.invoiceTableGUID
											select new
											{
												invoiceTableGUID = invoiceSettlementDetail.InvoiceTableGUID,
												balanceAmount = invoiceSettlementDetail.BalanceAmount,
												sumBalance = custTrans.sumBalance,
											};

					if (condition4_result.Count() > 0)
					{
						foreach (var item in condition4_result)
						{
							if (item.balanceAmount != item.sumBalance)
							{
								InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(item.invoiceTableGUID.Value);
								ex.AddData("ERROR.90046", new string[] { "LABEL.SUSPENSE_SETTLEMENT_DETAIL", invoiceTable.InvoiceId, "LABEL.POST_CUSTOMER_REFUND" });
							}
						}
					}
				}
				#endregion

				#region condition 6
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(customerRefundTable.CompanyGUID);
				if (invoiceSettlementDetailNoneList.Count() > 0 && companyParameter.SettlementMethodOfPaymentGUID == null)
				{
					ex.AddData("ERROR.90047", new string[] { "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" });
				}
				#endregion

				#region condition 7
				ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
				bool isPeriodSatusOpen = ledgerFiscalService.IsPeriodStatusOpen(customerRefundTable.CompanyGUID, customerRefundTable.TransDate);

				if (isPeriodSatusOpen == false)
				{
					ex.AddData("ERROR.90048", new string[] { customerRefundTable.TransDate.DateToString() });
				}

				#endregion

				#region condition 8
				if (customerRefundTable.ExchangeRate <= 0)
				{
					ex.AddData("ERROR.90169", new string[] { "LABEL.EXCHANGE_RATE", "LABEL.CUSTOMER_REFUND" });
				}
				#endregion

				#region condition 9
				if (invoiceSettlementDetailNoneList.Sum(s => s.SettleAmount) != customerRefundTable.SettleAmount)
				{
					ex.AddData("ERROR.90124", new string[] { "LABEL.SETTLE_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.INVOICE_SETTLEMENT" });
				}
				#endregion

				#region condition 10
				if (invoiceSettlementDetailSuspenseList.Sum(s => s.SettleAmount) * (-1) != customerRefundTable.SuspenseAmount)
				{
					ex.AddData("ERROR.90124", new string[] { "LABEL.SUSPENSE_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.SUSPENSE_SETTLEMENT" });
				}
				#endregion

				#region condition 10
				IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
				List<ServiceFeeTrans> serviceFeeTransList = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.CustomerRefund, customerRefundTable.CustomerRefundTableGUID).ToList();
				if (serviceFeeTransList.Sum(s => s.SettleAmount) != customerRefundTable.ServiceFeeAmount)
				{
					ex.AddData("ERROR.90124", new string[] { "LABEL.SETTLED_FEE_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.SERVICE_FEE_TRANSACTIONS" });
				}
				#endregion



				if (customerRefundTable.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.ToBeRefunded))
				{
					#region	Validate_ToBeRefunded
					if (customerRefundTable.CreditAppTableGUID != null)
					{
						if (creditAppTable.MaxRetentionAmount != 0)
						{
							IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
							decimal sumRetentionAmount = retentionTransRepo.GetRetentionTransByCreditAppTableGUIDNoTracking(creditAppTable.CreditAppTableGUID).Sum(s => s.Amount);

							if ((creditAppTable.MaxRetentionAmount - sumRetentionAmount) < customerRefundTable.RetentionAmount)
							{
								ex.AddData("ERROR.90048", new string[] { customerRefundTable.TransDate.DateToString() });
							}
						}
					}


					List<InvoiceSettlementDetail> invoiceSettlementDetailTobeRefundList = invoiceSettlementDetailRepo
							.GetInvoiceSettlementDetailByReferanceNoTracking(customerRefundTable.CustomerRefundTableGUID, Convert.ToInt32(RefType.CustomerRefund))
							.Where(t => t.SuspenseInvoiceType != Convert.ToInt32(SuspenseInvoiceType.ToBeRefunded)).ToList();

					if (invoiceSettlementDetailTobeRefundList.Count() > 0)
					{
						List<InvoiceTable> invoiceTableToBeRefundList = invoiceTableRepo.GetInvoiceTableByCustTransOpenStatus(customerRefundTable.CompanyGUID)
							.Where(t => t.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.ToBeRefunded)).ToList();
						List<InvoiceTable> condition8_result = (from invoiceTableToBeRefund in invoiceTableToBeRefundList
																join invoiceSettlementDetailTobeRefund in invoiceSettlementDetailTobeRefundList
																on invoiceTableToBeRefund.RefGUID equals invoiceSettlementDetailTobeRefund.InvoiceTableGUID
																select invoiceTableToBeRefund).ToList();
						if (condition8_result.Count() > 0)
						{
							ex.AddData("ERROR.90090");
						}
					}

					if (customerRefundTable.RetentionAmount != 0)
					{
						#region condition 9
						if (companyParameter.FTRetentionInvTypeGUID == null)
						{
							ex.AddData("ERROR.90047", new string[] { "LABEL.FT_RETENTION_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER" });

						}
						#endregion
						else
						{
							#region condition 10
							IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
							InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParameter.FTRetentionInvTypeGUID.Value);
							if (invoiceType.AutoGenInvoiceRevenueTypeGUID == null)
							{
								ex.AddData("ERROR.90047", new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.INVOICE_TYPE" });

							}
							#endregion

							#region condition 11
							if (invoiceType.SuspenseInvoiceType != Convert.ToInt32(SuspenseInvoiceType.Retention))
							{
								ex.AddData("ERROR.90114", new string[] { "LABEL.COMPANY_PARAMETER", "LABEL.SUSPENSE_INVOICE_TYPE", "ENUM.RETENTION" });

							}
							#endregion
						}
						if (customerRefundTable.CreditAppTableGUID != null)
						{
							if (creditAppTable.MaxRetentionAmount != 0)
							{
								ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
								decimal retensionBalance = creditAppTableService.GetRetentionBalanceByCreditAppTableGUID(customerRefundTable.CreditAppTableGUID.Value);

								if (retensionBalance < customerRefundTable.RetentionAmount)
								{
									ex.AddData("ERROR.90104");
								}
							}
						}
					}
					#endregion
				}

				if (customerRefundTable.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.ToBeRefunded) || customerRefundTable.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.Retention)) {
					#region condition 13
					if (!invoiceSettlementDetailList.Exists(t => t.SuspenseInvoiceType == customerRefundTable.SuspenseInvoiceType))
					{
						ex.AddData("ERROR.90097", new string[] { "LABEL.SUSPENSE_SETTLEMENT_DETAIL", "LABEL.SUSPENSE_INVOICE_TYPE", ((SuspenseInvoiceType)customerRefundTable.SuspenseInvoiceType).GetAttrCode() });
					}
					#endregion
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public PostCustomerRefundResultView InitPostCustomerRefundTable(PostCustomerRefundView customerRefundTable)
		{
			try
			{
				// validate
				GetPostCustomerRefundTableValidation(customerRefundTable.CustomerRefundTableGUID);

				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTable dbCustomerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(customerRefundTable.CustomerRefundTableGUID.StringToGuid());

				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
				List<ServiceFeeTrans> serviceFeeTransList = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.CustomerRefund, dbCustomerRefundTable.CustomerRefundTableGUID).ToList();
				List<ServiceFeeTrans> serviceFeeTrans_SettleAmount = serviceFeeTransList.Where(t => t.SettleAmount > 0).ToList();

				IInvoiceService invoiceTableService = new InvoiceService(db);
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);

				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				List<PaymentDetail> paymentDetailsList = paymentDetailRepo.GetPaymentDetailByReferanceNoTracking(dbCustomerRefundTable.CustomerRefundTableGUID, Convert.ToInt32(RefType.CustomerRefund)).ToList();

				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(dbCustomerRefundTable.CompanyGUID);

				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				MethodOfPayment methodOfPaymentByCompanyParameter = (companyParameter.SettlementMethodOfPaymentGUID != null) ? methodOfPaymentRepo.GetMethodOfPaymentByIdNoTracking(companyParameter.SettlementMethodOfPaymentGUID.Value) : null; ;

				IProcessTransService processTransService = new ProcessTransService(db);
				IVendorPaymentTransService vendorPaymentTransService = new VendorPaymentTransService(db);

				IRetentionTransService retentionTransService = new RetentionTransService(db);
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
				// create list
				List<InvoiceTable> invoiceTable_CreateList = new List<InvoiceTable>();
				List<InvoiceLine> invoiceLine_CreateList = new List<InvoiceLine>();
				List<InvoiceSettlementDetail> invoiceSettlementDetail_CreateList = new List<InvoiceSettlementDetail>();
				List<VendorPaymentTrans> vendorPaymentTrans_CreateList = new List<VendorPaymentTrans>();
				List<ProcessTrans> processTrans_CreateList = new List<ProcessTrans>();
				List<RetentionTrans> retentionTrans_CreateList = new List<RetentionTrans>();

				List<CustTrans> custTranse_CreateList = new List<CustTrans>();
				List<CreditAppTrans> creditAppTranse_CreateList = new List<CreditAppTrans>();
				List<TaxInvoiceTable> taxInvoiceTable_CreateList = new List<TaxInvoiceTable>();
				List<TaxInvoiceLine> taxInvoiceLine_CreateList = new List<TaxInvoiceLine>();

				List<ReceiptTable> receiptTable_CreateList = new List<ReceiptTable>();
				List<ReceiptLine> receiptLine_CreateList = new List<ReceiptLine>();
				List<PaymentHistory> paymentHistory_CreateList = new List<PaymentHistory>();
				List<IntercompanyInvoiceTable> intercompanyInvoiceTable_CreateList = new List<IntercompanyInvoiceTable>();

				// update list
				List<InvoiceSettlementDetail> invoiceSettlementDetail_UpdateList = new List<InvoiceSettlementDetail>();
				List<CustTrans> custTrans_UpdateList = new List<CustTrans>();

				// PostSettleList
				List<InvoiceSettlementDetail> invoiceSettlementDetails_PostSettle = new List<InvoiceSettlementDetail>();
				List<InvoiceTable> invoiceTable_PostSettle = new List<InvoiceTable>();
				List<InvoiceLine> invoiceLine_PostSettle = new List<InvoiceLine>();
				List<CustTrans> custTrans_PostSettle = new List<CustTrans>();

				#region	1 Create Invoice from ServiceFeeTrans
				if (serviceFeeTransList.Count() > 0)
				{
					GenInvoiceFromServiceFeeTransParamView genInvoiceFromServiceFeeTransParamView = new GenInvoiceFromServiceFeeTransParamView()
					{
						ServiceFeeTrans = serviceFeeTransList.OrderBy(o => o.ServiceFeeTransGUID).ToList(),
						IssuedDate = dbCustomerRefundTable.TransDate,
						CustomerTableGUID = dbCustomerRefundTable.CustomerTableGUID,
						ProductType = (ProductType)dbCustomerRefundTable.ProductType,
						DocumentId = dbCustomerRefundTable.CustomerRefundId,
						CreditAppTableGUID = dbCustomerRefundTable.CreditAppTableGUID
					};
					GenInvoiceFromServiceFeeTransResultView genInvoiceFromServiceFeeTransResultView = serviceFeeTransService.GenInvoiceFromServiceFeeTrans(genInvoiceFromServiceFeeTransParamView);
					PostInvoiceResultView postInvoiceResultView = invoiceTableService.PostInvoice(genInvoiceFromServiceFeeTransResultView.InvoiceTable, genInvoiceFromServiceFeeTransResultView.InvoiceLine, RefType.CustomerRefund, dbCustomerRefundTable.CustomerRefundTableGUID);
					invoiceTable_CreateList.AddRange(postInvoiceResultView.InvoiceTables);
					invoiceLine_CreateList.AddRange(postInvoiceResultView.InvoiceLines);
					custTrans_PostSettle.AddRange(postInvoiceResultView.CustTranses);

					// PostSettle
					invoiceTable_PostSettle.AddRange(postInvoiceResultView.InvoiceTables);
					invoiceLine_PostSettle.AddRange(postInvoiceResultView.InvoiceLines);

					processTrans_CreateList.AddRange(postInvoiceResultView.ProcessTranses);
					custTranse_CreateList.AddRange(postInvoiceResultView.CustTranses);
					creditAppTranse_CreateList.AddRange(postInvoiceResultView.CreditAppTranses);
					taxInvoiceTable_CreateList.AddRange(postInvoiceResultView.TaxInvoiceTables);
					taxInvoiceLine_CreateList.AddRange(postInvoiceResultView.TaxInvoiceLines);
					retentionTrans_CreateList.AddRange(postInvoiceResultView.RetentionTranses);
					intercompanyInvoiceTable_CreateList.AddRange(genInvoiceFromServiceFeeTransResultView.IntercompanyInvoiceTable);
				}
				#endregion

				#region 2 Create InvoiceSettlementDetail from ServiceFeeTrans 
				if (serviceFeeTrans_SettleAmount.Count > 0)
				{
					List<InvoiceSettlementDetail> GenInvoiceSettlementDetailFromServiceFeeTransResult = invoiceSettlementDetailService.GenInvoiceSettlementDetailFromServiceFeeTrans(
					serviceFeeTrans_SettleAmount,
					invoiceTable_CreateList,
					dbCustomerRefundTable.CustomerRefundId,
					RefType.CustomerRefund,
					dbCustomerRefundTable.CustomerRefundTableGUID);

					invoiceSettlementDetail_CreateList.AddRange(GenInvoiceSettlementDetailFromServiceFeeTransResult);
					invoiceSettlementDetails_PostSettle.AddRange(GenInvoiceSettlementDetailFromServiceFeeTransResult);
				}
				#endregion

				#region 3 Create Invoice from PaymentDetail Suspense
				IEnumerable<PaymentDetail> paymentDetailsSuspenseList = paymentDetailsList.Where(t => t.PaidToType == Convert.ToInt32(PaidToType.Suspense) && t.PaymentAmount != 0);

				if (paymentDetailsSuspenseList.Count() > 0)
				{
					foreach (PaymentDetail item in paymentDetailsSuspenseList)
					{
						Guid? setCreditAppTableGUID = (item.SuspenseTransfer == false) ? dbCustomerRefundTable.CreditAppTableGUID : null;
						InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(item.InvoiceTypeGUID.Value);
						Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView
						{
							RefType = RefType.PaymentDetail,
							RefGUID = item.PaymentDetailGUID,
							IssuedDate = dbCustomerRefundTable.TransDate,
							DueDate = dbCustomerRefundTable.TransDate,
							CustomerTableGUID = item.CustomerTableGUID.Value,
							ProductType = (ProductType)invoiceType.ProductType,
							InvoiceTypeGUID = item.InvoiceTypeGUID.Value,
							AutoGenInvoiceRevenueType = true,
							InvoiceAmount = item.PaymentAmount * (-1),
							IncludeTax = true,
							CompanyGUID = dbCustomerRefundTable.CompanyGUID,
							InvoiceRevenueTypeGUID = null,
							DocumentId = dbCustomerRefundTable.CustomerRefundId,
							CreditAppTableGUID = (dbCustomerRefundTable.CreditAppTableGUID == null) ? null : setCreditAppTableGUID,
							BuyerTableGUID = null,
							Dimension1GUID = dbCustomerRefundTable.Dimension1GUID,
							Dimension2GUID = dbCustomerRefundTable.Dimension2GUID,
							Dimension3GUID = dbCustomerRefundTable.Dimension3GUID,
							Dimension4GUID = dbCustomerRefundTable.Dimension4GUID,
							Dimension5GUID = dbCustomerRefundTable.Dimension5GUID,
							MethodOfPaymentGUID = null,
							BuyerInvoiceTableGUID = null,
							BuyerAgreementTableGUID = null
						};

						GenInvoiceResultView genInvoiceResultView = invoiceTableService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
						PostInvoiceResultView postInvoiceResultView = invoiceTableService.PostInvoice(new List<InvoiceTable> { genInvoiceResultView.InvoiceTable }, new List<InvoiceLine> { genInvoiceResultView.InvoiceLine }, RefType.CustomerRefund, dbCustomerRefundTable.CustomerRefundTableGUID);

						invoiceTable_CreateList.AddRange(postInvoiceResultView.InvoiceTables);
						invoiceLine_CreateList.AddRange(postInvoiceResultView.InvoiceLines);

						processTrans_CreateList.AddRange(postInvoiceResultView.ProcessTranses);
						custTranse_CreateList.AddRange(postInvoiceResultView.CustTranses);
						creditAppTranse_CreateList.AddRange(postInvoiceResultView.CreditAppTranses);
						taxInvoiceTable_CreateList.AddRange(postInvoiceResultView.TaxInvoiceTables);
						taxInvoiceLine_CreateList.AddRange(postInvoiceResultView.TaxInvoiceLines);
						retentionTrans_CreateList.AddRange(postInvoiceResultView.RetentionTranses);
					}
					// R02
					List<ProcessTrans> processTrans_3 = processTransService.GenProcessTransFromSuspensePaymentDetail(paymentDetailsSuspenseList.ToList(), (ProductType)dbCustomerRefundTable.ProductType, dbCustomerRefundTable.CustomerRefundId, dbCustomerRefundTable.TransDate);
					processTrans_CreateList.AddRange(processTrans_3);
				}

				#endregion

				#region 4 create vendorpayment from PaymentDetail
				IEnumerable<PaymentDetail> paymentDetailsCustomerVendorList = paymentDetailsList.Where(t => t.VendorTableGUID != null && (t.PaidToType == Convert.ToInt32(PaidToType.Customer) || t.PaidToType == Convert.ToInt32(PaidToType.Vendor)));

				if (paymentDetailsCustomerVendorList.Count() > 0)
				{
					Guid comfirmedStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(VendorPaymentStatus.Confirmed).ToString()).DocumentStatusGUID;
					foreach (PaymentDetail item in paymentDetailsCustomerVendorList)
					{
						VendorPaymentTrans vendorPaymentTrans = new VendorPaymentTrans
						{
							CreditAppTableGUID = dbCustomerRefundTable.CreditAppTableGUID,
							VendorTableGUID = item.VendorTableGUID,
							PaymentDate = dbCustomerRefundTable.TransDate,
							OffsetAccount = (methodOfPaymentByCompanyParameter != null) ? methodOfPaymentByCompanyParameter.AccountNum.ToString() : null,
							TaxTableGUID = null,
							AmountBeforeTax = item.PaymentAmount,
							TaxAmount = 0,
							TotalAmount = item.PaymentAmount,
							DocumentStatusGUID = comfirmedStatus,
							Dimension1 = dbCustomerRefundTable.Dimension1GUID,
							Dimension2 = dbCustomerRefundTable.Dimension2GUID,
							Dimension3 = dbCustomerRefundTable.Dimension3GUID,
							Dimension4 = dbCustomerRefundTable.Dimension4GUID,
							Dimension5 = dbCustomerRefundTable.Dimension5GUID,
							RefType = (int)RefType.CustomerRefund,
							RefGUID = dbCustomerRefundTable.CustomerRefundTableGUID,
							CompanyGUID = item.CompanyGUID,
							Owner = dbCustomerRefundTable.Owner,
							OwnerBusinessUnitGUID = dbCustomerRefundTable.OwnerBusinessUnitGUID,
							VendorPaymentTransGUID = Guid.NewGuid()
						};

						vendorPaymentTrans_CreateList.Add(vendorPaymentTrans);
						ProcessTrans processTrans = processTransService.GenProcessTransFromVendorPaymentTrans(vendorPaymentTrans, dbCustomerRefundTable.CustomerTableGUID, (ProductType)dbCustomerRefundTable.ProductType, dbCustomerRefundTable.CustomerRefundId);
						processTrans_CreateList.Add(processTrans);
					}
				}
				#endregion

				#region 5 update customer refund
				DocumentStatus postedStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(CustomerRefundStatus.Posted).ToString());
				dbCustomerRefundTable.DocumentStatusGUID = postedStatus.DocumentStatusGUID;
				#endregion

				#region 6 create invoice from CustomerRefund
				if (dbCustomerRefundTable.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.ToBeRefunded) && dbCustomerRefundTable.RetentionAmount != 0)
				{
					Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView
					{
						RefType = RefType.CustomerRefund,
						RefGUID = dbCustomerRefundTable.CustomerRefundTableGUID,
						IssuedDate = dbCustomerRefundTable.TransDate,
						DueDate = dbCustomerRefundTable.TransDate,
						CustomerTableGUID = dbCustomerRefundTable.CustomerTableGUID,
						ProductType = (ProductType)dbCustomerRefundTable.ProductType,
						InvoiceTypeGUID = companyParameter.FTRetentionInvTypeGUID.Value,
						AutoGenInvoiceRevenueType = true,
						InvoiceAmount = dbCustomerRefundTable.RetentionAmount * (-1),
						IncludeTax = true,
						CompanyGUID = dbCustomerRefundTable.CompanyGUID,
						InvoiceRevenueTypeGUID = null,
						DocumentId = dbCustomerRefundTable.CustomerRefundId,
						CreditAppTableGUID = dbCustomerRefundTable.CreditAppTableGUID,
						BuyerTableGUID = null,
						Dimension1GUID = dbCustomerRefundTable.Dimension1GUID,
						Dimension2GUID = dbCustomerRefundTable.Dimension2GUID,
						Dimension3GUID = dbCustomerRefundTable.Dimension3GUID,
						Dimension4GUID = dbCustomerRefundTable.Dimension4GUID,
						Dimension5GUID = dbCustomerRefundTable.Dimension5GUID,
						MethodOfPaymentGUID = null,
						BuyerInvoiceTableGUID = null,
						BuyerAgreementTableGUID = null
					};

					GenInvoiceResultView genInvoiceResultView = invoiceTableService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
					PostInvoiceResultView postInvoiceResultView = invoiceTableService.PostInvoice(new List<InvoiceTable> { genInvoiceResultView.InvoiceTable }, new List<InvoiceLine> { genInvoiceResultView.InvoiceLine }, RefType.CustomerRefund, dbCustomerRefundTable.CustomerRefundTableGUID);

					invoiceTable_CreateList.AddRange(postInvoiceResultView.InvoiceTables);
					invoiceLine_CreateList.AddRange(postInvoiceResultView.InvoiceLines);

					processTrans_CreateList.AddRange(postInvoiceResultView.ProcessTranses);
					custTranse_CreateList.AddRange(postInvoiceResultView.CustTranses);
					creditAppTranse_CreateList.AddRange(postInvoiceResultView.CreditAppTranses);
					taxInvoiceTable_CreateList.AddRange(postInvoiceResultView.TaxInvoiceTables);
					taxInvoiceLine_CreateList.AddRange(postInvoiceResultView.TaxInvoiceLines);
					retentionTrans_CreateList.AddRange(postInvoiceResultView.RetentionTranses);
				}
				#endregion

				#region 7 create retensionTrans from invoiceSettleMentDetail

				if (dbCustomerRefundTable.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.Retention))
				{
					List<InvoiceSettlementDetail> invoiceSettlementDetailsRetensionList = invoiceSettlementDetailRepo
						.GetInvoiceSettlementDetailByReferanceNoTracking(dbCustomerRefundTable.CustomerRefundTableGUID, Convert.ToInt32(RefType.CustomerRefund))
						.Where(t => t.SuspenseInvoiceType == Convert.ToInt32(SuspenseInvoiceType.Retention) && t.SettleAmount != 0).ToList();

					if (invoiceSettlementDetailsRetensionList.Count > 0)
					{
						foreach (InvoiceSettlementDetail item in invoiceSettlementDetailsRetensionList)
						{
							InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(item.InvoiceTableGUID.GetValueOrDefault());
							BuyerAgreementTable buyerAgreement = null;
							if (invoiceTable != null)
								buyerAgreement = invoiceTable.BuyerAgreementTableGUID.HasValue ? buyerAgreementTableRepo.GetBuyerAgreementTableByIdNoTracking(invoiceTable.BuyerAgreementTableGUID.Value) : null;
							
							RetentionTrans retentionTrans = new RetentionTrans
							{
								CustomerTableGUID = invoiceTable.CustomerTableGUID,
								CreditAppTableGUID = invoiceTable.CreditAppTableGUID.Value,
								ProductType = invoiceTable.ProductType,
								TransDate = dbCustomerRefundTable.TransDate,
								Amount = item.SettleAmount,
								BuyerTableGUID = buyerAgreement?.BuyerTableGUID,
								BuyerAgreementTableGUID = invoiceTable.BuyerAgreementTableGUID,
								RefType = Convert.ToInt32(RefType.CustomerRefund),
								RefGUID = dbCustomerRefundTable.CustomerRefundTableGUID,
								DocumentId = dbCustomerRefundTable.CustomerRefundId,
								CompanyGUID = item.CompanyGUID,
								RetentionTransGUID = Guid.NewGuid()
							};
							retentionTrans_CreateList.Add(retentionTrans);

							#region 8 update InvoiceSettlement 
							if (invoiceTable.CreditAppTableGUID != null)
							{
								item.RetentionAmountAccum = retentionTransService.GetRetentionAccumByCreditAppTableGUID(invoiceTable.CreditAppTableGUID.Value);
								invoiceSettlementDetail_UpdateList.Add(item);
							}
							#endregion
						}

					}
				}

				#endregion

				#region 9 post settlement
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);

				List<InvoiceSettlementDetail> invoiceSettlementDetailsByCustomerRefund = invoiceSettlementDetailRepo
						.GetInvoiceSettlementDetailByReferanceNoTracking(dbCustomerRefundTable.CustomerRefundTableGUID, Convert.ToInt32(RefType.CustomerRefund)).ToList();

				List<InvoiceTable> invoiceTablesByInvoiceSettleMentDeatial = (from it in db.Set<InvoiceTable>()
																			  join isd in db.Set<InvoiceSettlementDetail>()
																			  on it.InvoiceTableGUID equals isd.InvoiceTableGUID
																			  where isd.RefType == Convert.ToInt32(RefType.CustomerRefund) && isd.RefGUID == dbCustomerRefundTable.CustomerRefundTableGUID
																			  select it).ToList();

				List<InvoiceLine> invoiceLinesByInvoiceSettleMentDeatial = (from il in db.Set<InvoiceLine>()
																			join isd in db.Set<InvoiceSettlementDetail>()
																			on il.InvoiceTableGUID equals isd.InvoiceTableGUID
																			where isd.RefType == Convert.ToInt32(RefType.CustomerRefund) && isd.RefGUID == dbCustomerRefundTable.CustomerRefundTableGUID
																			select il).ToList();

				List<CustTrans> custTranssByInvoiceSettleMentDeatial = (from ct in db.Set<CustTrans>()
																		join isd in db.Set<InvoiceSettlementDetail>()
																				on ct.InvoiceTableGUID equals isd.InvoiceTableGUID
																		where isd.RefType == Convert.ToInt32(RefType.CustomerRefund) && isd.RefGUID == dbCustomerRefundTable.CustomerRefundTableGUID
																		select ct).ToList();

				invoiceSettlementDetails_PostSettle.AddRange(invoiceSettlementDetailsByCustomerRefund);
				invoiceTable_PostSettle.AddRange(invoiceTablesByInvoiceSettleMentDeatial);
				invoiceLine_PostSettle.AddRange(invoiceLinesByInvoiceSettleMentDeatial);
				custTrans_PostSettle.AddRange(custTranssByInvoiceSettleMentDeatial);

				ViewPostSettlementParameter viewPostSettlementParameter = new ViewPostSettlementParameter
				{
					RefType = RefType.CustomerRefund,
					InvoiceSettlementDetails = invoiceSettlementDetails_PostSettle,
					InvoiceTables = invoiceTable_PostSettle,
					InvoiceLines = invoiceLine_PostSettle,
					CustTrans = custTrans_PostSettle,
					ReceiptTempTable = null,
					CustomerRefundTable = dbCustomerRefundTable,
					SourceRefId = dbCustomerRefundTable.CustomerRefundId,
					SourceRefType = RefType.CustomerRefund,
				};
				ViewPostSettlementResult viewPostSettlementResult = receiptTempTableService.PostSettlement(viewPostSettlementParameter);
				if (viewPostSettlementResult.UpdateCustTrans != null) custTrans_UpdateList.AddRange(viewPostSettlementResult.UpdateCustTrans.Where(t => !custTranse_CreateList.Any(j => j.CustTransGUID == t.CustTransGUID)).ToList());
				if (viewPostSettlementResult.CreateCreditAppTrans != null) creditAppTranse_CreateList.AddRange(viewPostSettlementResult.CreateCreditAppTrans);
				if (viewPostSettlementResult.CreateReceiptTable != null) receiptTable_CreateList.AddRange(viewPostSettlementResult.CreateReceiptTable);
				if (viewPostSettlementResult.CreateReceiptLine != null) receiptLine_CreateList.AddRange(viewPostSettlementResult.CreateReceiptLine);
				if (viewPostSettlementResult.CreateTaxInvoiceTable != null) taxInvoiceTable_CreateList.AddRange(viewPostSettlementResult.CreateTaxInvoiceTable);
				if (viewPostSettlementResult.CreateTaxInvoiceLine != null) taxInvoiceLine_CreateList.AddRange(viewPostSettlementResult.CreateTaxInvoiceLine);
				if (viewPostSettlementResult.CreatePaymentHistory != null) paymentHistory_CreateList.AddRange(viewPostSettlementResult.CreatePaymentHistory);
				if (viewPostSettlementResult.CreateProcessTrans != null) processTrans_CreateList.AddRange(viewPostSettlementResult.CreateProcessTrans);
				#endregion

				PostCustomerRefundResultView final_result = new PostCustomerRefundResultView
				{
					CustomerRefundTable = dbCustomerRefundTable,
					InvoiceTable = invoiceTable_CreateList.OrderBy(o => o.InvoiceId).ToList(),
					InvoiceLine = (from invoiceLine in invoiceLine_CreateList
								   join invoiceTable in invoiceTable_CreateList
								   on invoiceLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
								   from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
								   select new
								   {
									   invoiceLine,
									   invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
								   }).OrderBy(o => o.invoiceId).Select(s => s.invoiceLine).ToList(),
					InvoiceSettlementDetail = (from invoiceSettlementDetail in invoiceSettlementDetail_CreateList
											   join invoiceTable in invoiceTable_CreateList
											   on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
											   from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
											   select new
											   {
												   invoiceSettlementDetail,
												   invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : invoiceSettlementDetail.InvoiceTableGUID.ToString()
											   }).OrderBy(o => o.invoiceId).Select(s => s.invoiceSettlementDetail).ToList(),
					VendorPaymentTrans = vendorPaymentTrans_CreateList.OrderBy(o => o.VendorTableGUID).ToList(),
					ProcessTrans = (from processTrans in processTrans_CreateList
									join invoiceTable in invoiceTable_CreateList
									on new { processTrans.RefType, InvoiceTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.Invoice, invoiceTable.InvoiceTableGUID } into ljInvoiceTableProcessTrans
									from invoiceTable in ljInvoiceTableProcessTrans.DefaultIfEmpty()
									join vendorPaymentTrans in vendorPaymentTrans_CreateList
									on new { processTrans.RefType, VendorPaymentTransGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.VendorPaymentTrans, vendorPaymentTrans.VendorPaymentTransGUID } into ljVendorPaymentTransProcessTrans
									from vendorPaymentTrans in ljVendorPaymentTransProcessTrans.DefaultIfEmpty()
									select new
									{
										processTrans,
										processTrans.RefType,
										RefId = invoiceTable != null ? invoiceTable.InvoiceId : (vendorPaymentTrans != null ? vendorPaymentTrans.VendorTableGUID.ToString() : string.Empty)
									}).OrderBy(o => o.RefType).ThenBy(t => t.RefId).ThenBy(t => t.processTrans.DocumentId).Select(s => s.processTrans).ToList(),
					RetentionTrans = retentionTrans_CreateList,
					CustTrans = (from custTrans in custTranse_CreateList
								 join invoiceTable in invoiceTable_CreateList
								 on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
								 from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
								 select new
								 {
									 custTrans,
									 invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
								 }).OrderBy(o => o.invoiceId).Select(s => s.custTrans).ToList(),
					CreditAppTrans = creditAppTranse_CreateList,
					TaxInvoiceLine = taxInvoiceLine_CreateList.OrderBy(t => t.UnitPrice).ThenBy(t => t.TotalAmountBeforeTax).ThenBy(t => t.TaxAmount).ThenBy(t => t.TotalAmount).ToList(),
					TaxInvoiceTable = taxInvoiceTable_CreateList.OrderBy(t => t.TaxInvoiceId).ToList(),
					PaymentHistory = paymentHistory_CreateList.OrderBy(t => t.PaymentBaseAmount).ToList(),
					ReceiptTable = receiptTable_CreateList.OrderBy(t => t.ReceiptId).ToList(),
					ReceiptLine = receiptLine_CreateList.OrderBy(t => t.InvoiceAmount).ThenBy(t => t.TaxAmount).ToList(),
					IntercompanyInvoiceTable = intercompanyInvoiceTable_CreateList,
					InvoiceSettlementDetail_Updated = invoiceSettlementDetail_UpdateList,
					CustTrans_Update = custTrans_UpdateList
				};

				return final_result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public PostCustomerRefundResultView PostCustomerRefundTable(PostCustomerRefundView customerRefundTable)
		{
			try
			{
				PostCustomerRefundResultView postCustomerRefundResult = InitPostCustomerRefundTable(customerRefundTable);

				#region check result count
				bool hasInvoiceTable = postCustomerRefundResult.InvoiceTable.Count > 0;
				bool hasInvoiceLine = postCustomerRefundResult.InvoiceLine.Count > 0;
				bool hasProcessTrans = postCustomerRefundResult.ProcessTrans.Count > 0;
				bool hasCustTrans = postCustomerRefundResult.CustTrans.Count > 0;
				bool hasCreditAppTrans = postCustomerRefundResult.CreditAppTrans.Count > 0;
				bool hasTaxInvoiceTable = postCustomerRefundResult.TaxInvoiceTable.Count > 0;
				bool hasTaxInvoiceLine = postCustomerRefundResult.TaxInvoiceLine.Count > 0;
				bool hasRetentionTrans = postCustomerRefundResult.RetentionTrans.Count > 0;
				bool hasVendorPaymentTrans = postCustomerRefundResult.VendorPaymentTrans.Count > 0;
				bool hasInvoiceSettlementDetailTrans = postCustomerRefundResult.InvoiceSettlementDetail.Count > 0;
				bool hasIntercompanyInvoiceTable = postCustomerRefundResult.IntercompanyInvoiceTable.Count > 0;


				bool hasReceiptTable = postCustomerRefundResult.ReceiptTable.Count > 0;
				bool hasReceiptLine = postCustomerRefundResult.ReceiptLine.Count > 0;
				bool hasPaymentHistory = postCustomerRefundResult.PaymentHistory.Count > 0;

				bool hasInvoiceSettlementDetailTrans_Updated = postCustomerRefundResult.InvoiceSettlementDetail_Updated.Count > 0;
				bool hasCustTrans_Updated = postCustomerRefundResult.CustTrans_Update.Count > 0;

				#endregion

				UpdateCustomerRefundTable(postCustomerRefundResult.CustomerRefundTable);

				if (hasInvoiceTable || hasInvoiceLine || hasProcessTrans || hasCustTrans ||
					hasCreditAppTrans || hasTaxInvoiceTable || hasTaxInvoiceLine || hasRetentionTrans || hasVendorPaymentTrans ||
					hasInvoiceSettlementDetailTrans || hasReceiptTable || hasReceiptLine || hasPaymentHistory || hasIntercompanyInvoiceTable || hasInvoiceSettlementDetailTrans_Updated || hasCustTrans_Updated)
				{
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						this.BulkInsert(postCustomerRefundResult.InvoiceTable);
						this.BulkInsert(postCustomerRefundResult.InvoiceLine);
						this.BulkInsert(postCustomerRefundResult.CustTrans);
						this.BulkInsert(postCustomerRefundResult.CreditAppTrans);
						this.BulkInsert(postCustomerRefundResult.TaxInvoiceTable);
						this.BulkInsert(postCustomerRefundResult.TaxInvoiceLine);
						this.BulkInsert(postCustomerRefundResult.RetentionTrans);
						this.BulkInsert(postCustomerRefundResult.VendorPaymentTrans);
						this.BulkInsert(postCustomerRefundResult.InvoiceSettlementDetail);
						this.BulkInsert(postCustomerRefundResult.IntercompanyInvoiceTable);

						this.BulkInsert(postCustomerRefundResult.ReceiptTable);
						this.BulkInsert(postCustomerRefundResult.ReceiptLine);
						this.BulkInsert(postCustomerRefundResult.PaymentHistory);
						this.BulkInsert(postCustomerRefundResult.ProcessTrans);

						this.BulkUpdate(postCustomerRefundResult.InvoiceSettlementDetail_Updated);
						this.BulkUpdate(postCustomerRefundResult.CustTrans_Update);

						UnitOfWork.Commit(transaction);
					}
				}
				else
				{
					UnitOfWork.Commit();
				}

				NotificationResponse success = new NotificationResponse();
				success.AddData("SUCCESS.90004", new string[] { "LABEL.CUSTOMER_REFUND", SmartAppUtil.GetDropDownLabel(postCustomerRefundResult.CustomerRefundTable.CustomerRefundId,
																									postCustomerRefundResult.CustomerRefundTable.Description) });
				postCustomerRefundResult.Notification = success;
				return postCustomerRefundResult;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion

		#endregion

		public PrintCustomerReserveRefund GetPrintCustomerRefundReserveById(string refGUID)
		{
			try
			{
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTableItemView customerRefundItem = customerRefundTableRepo.GetByIdvw(refGUID.StringToGuid());
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTableItem = customerTableRepo.GetByIdvw(customerRefundItem.CustomerTableGUID.StringToGuid());
				DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatusItemView documentStatusItemView = documentStatusRepo.GetByIdvw(customerRefundItem.DocumentStatusGUID.StringToGuid());
				PrintCustomerReserveRefund printCustomerReserveRefund = new PrintCustomerReserveRefund
				{
					printCustomerRefundReserveGUID = customerRefundItem.CustomerRefundTableGUID,
					customerId = SmartAppUtil.GetDropDownLabel(customerTableItem.CustomerId,customerTableItem.Name),
					customerRefundId = customerRefundItem.CustomerRefundId,
					documentStatus = documentStatusItemView.Description,
					netAmount = customerRefundItem.NetAmount,
					paymentAmount = customerRefundItem.PaymentAmount,
					retentionAmount = customerRefundItem.RetentionAmount,
					serviceFeeAmount = customerRefundItem.ServiceFeeAmount,
					settleAmount = customerRefundItem.SettleAmount,
					suspenseAmount = customerRefundItem.SuspenseAmount,
					transDate = customerRefundItem.TransDate,
					CustomerRefundTableGUID = customerRefundItem.CustomerRefundTableGUID
				};
				return printCustomerReserveRefund;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public PrintCustomerRetentionRefund GetPrintCustomerRefundRetentionById(string refGUID)
		{
            try 
			{ 
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				CustomerRefundTableItemView customerRefundItem = customerRefundTableRepo.GetByIdvw(refGUID.StringToGuid());
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTableItem = customerTableRepo.GetByIdvw(customerRefundItem.CustomerTableGUID.StringToGuid());
				DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatusItemView documentStatusItemView = documentStatusRepo.GetByIdvw(customerRefundItem.DocumentStatusGUID.StringToGuid());

				PrintCustomerRetentionRefund printCustomerRetentionRefund = new PrintCustomerRetentionRefund
				{
					printCustomerRefundRetentionGUID = customerRefundItem.CustomerRefundTableGUID,
					customerId = SmartAppUtil.GetDropDownLabel(customerTableItem.CustomerId, customerTableItem.Name),
					customerRefundId = customerRefundItem.CustomerRefundId,
					documentStatus = documentStatusItemView.Description,
					netAmount = customerRefundItem.NetAmount,
					paymentAmount = customerRefundItem.PaymentAmount,
					retentionAmount = customerRefundItem.RetentionAmount,
					serviceFeeAmount = customerRefundItem.ServiceFeeAmount,
					settleAmount = customerRefundItem.SettleAmount,
					suspenseAmount = customerRefundItem.SuspenseAmount,
					transDate = customerRefundItem.TransDate,
					CustomerRefundTableGUID = customerRefundItem.CustomerRefundTableGUID
				};
				return printCustomerRetentionRefund;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustomerRefundTableServiceFeeAmount(ServiceFeeTransItemView serviceFeeTransView)
		{
			try
			{
				if (serviceFeeTransView.RefType == (int)RefType.CustomerRefund)
				{
					IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
					ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
					CustomerRefundTable dbCustomerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(serviceFeeTransView.RefGUID.StringToGuid());
					IEnumerable<ServiceFeeTrans> serviceFeeTranses = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking((RefType)serviceFeeTransView.RefType, dbCustomerRefundTable.CustomerRefundTableGUID)
																						.Where(w => w.ServiceFeeTransGUID != serviceFeeTransView.ServiceFeeTransGUID.StringToGuid());
					if (serviceFeeTranses.Count() > 0)
					{
						dbCustomerRefundTable.ServiceFeeAmount = serviceFeeTranses.Sum(s => s.SettleAmount) + serviceFeeTransView.SettleAmount;
					}
					else
					{
						dbCustomerRefundTable.ServiceFeeAmount = serviceFeeTransView.SettleAmount;
					}
					UpdateCustomerRefundTable(dbCustomerRefundTable);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustomerRefundTableServiceFeeAmountOnRemove(ServiceFeeTrans serviceFeeTrans)
		{
			try
			{
				if (serviceFeeTrans.RefType == (int)RefType.CustomerRefund)
				{
					IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
					ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
					CustomerRefundTable dbCustomerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(serviceFeeTrans.RefGUID);
					dbCustomerRefundTable.ServiceFeeAmount -= serviceFeeTrans.SettleAmount;
					UpdateCustomerRefundTable(dbCustomerRefundTable);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustomerRefundTablePaymentAmount(PaymentDetailItemView paymentDetailView)
		{
			try
			{
				if (paymentDetailView.RefType == (int)RefType.CustomerRefund)
				{
					IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
					ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
					CustomerRefundTable dbCustomerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(paymentDetailView.RefGUID.StringToGuid());
					IEnumerable<PaymentDetail> paymentDetails = paymentDetailRepo.GetPaymentDetailByReferanceNoTracking(dbCustomerRefundTable.CustomerRefundTableGUID, paymentDetailView.RefType)
																						.Where(w => w.PaymentDetailGUID != paymentDetailView.PaymentDetailGUID.StringToGuid());
					if (paymentDetails.Count() > 0)
					{
						dbCustomerRefundTable.PaymentAmount = paymentDetails.Sum(s => s.PaymentAmount) + paymentDetailView.PaymentAmount;
					}
					else
					{
						dbCustomerRefundTable.PaymentAmount = paymentDetailView.PaymentAmount;
					}
					UpdateCustomerRefundTable(dbCustomerRefundTable);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCustomerRefundTablePaymentAmountOnRemove(PaymentDetail paymentDetail)
		{
			try
			{
				if (paymentDetail.RefType == (int)RefType.CustomerRefund)
				{
					IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
					ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
					CustomerRefundTable dbCustomerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(paymentDetail.RefGUID.Value);
					dbCustomerRefundTable.PaymentAmount -= paymentDetail.PaymentAmount;
					UpdateCustomerRefundTable(dbCustomerRefundTable);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MemoTransItemView GetMemoTransInitialData(string refGUID)
		{
			try
			{
				IMemoTransService memoTransService = new MemoTransService(db);
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
				string refId = customerRefundTableRepo.GetCustomerRefundTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CustomerRefundId;
				return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.CustomerRefund);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
