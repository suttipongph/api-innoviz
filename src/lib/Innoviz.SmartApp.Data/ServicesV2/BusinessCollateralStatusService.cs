using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBusinessCollateralStatusService
	{

		BusinessCollateralStatusItemView GetBusinessCollateralStatusById(string id);
		BusinessCollateralStatusItemView CreateBusinessCollateralStatus(BusinessCollateralStatusItemView businessCollateralStatusView);
		BusinessCollateralStatusItemView UpdateBusinessCollateralStatus(BusinessCollateralStatusItemView businessCollateralStatusView);
		bool DeleteBusinessCollateralStatus(string id);
	}
	public class BusinessCollateralStatusService : SmartAppService, IBusinessCollateralStatusService
	{
		public BusinessCollateralStatusService(SmartAppDbContext context) : base(context) { }
		public BusinessCollateralStatusService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BusinessCollateralStatusService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessCollateralStatusService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BusinessCollateralStatusService() : base() { }

		public BusinessCollateralStatusItemView GetBusinessCollateralStatusById(string id)
		{
			try
			{
				IBusinessCollateralStatusRepo businessCollateralStatusRepo = new BusinessCollateralStatusRepo(db);
				return businessCollateralStatusRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralStatusItemView CreateBusinessCollateralStatus(BusinessCollateralStatusItemView businessCollateralStatusView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				businessCollateralStatusView = accessLevelService.AssignOwnerBU(businessCollateralStatusView);
				IBusinessCollateralStatusRepo businessCollateralStatusRepo = new BusinessCollateralStatusRepo(db);
				BusinessCollateralStatus businessCollateralStatus = businessCollateralStatusView.ToBusinessCollateralStatus();
				businessCollateralStatus = businessCollateralStatusRepo.CreateBusinessCollateralStatus(businessCollateralStatus);
				base.LogTransactionCreate<BusinessCollateralStatus>(businessCollateralStatus);
				UnitOfWork.Commit();
				return businessCollateralStatus.ToBusinessCollateralStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralStatusItemView UpdateBusinessCollateralStatus(BusinessCollateralStatusItemView businessCollateralStatusView)
		{
			try
			{
				IBusinessCollateralStatusRepo businessCollateralStatusRepo = new BusinessCollateralStatusRepo(db);
				BusinessCollateralStatus inputBusinessCollateralStatus = businessCollateralStatusView.ToBusinessCollateralStatus();
				BusinessCollateralStatus dbBusinessCollateralStatus = businessCollateralStatusRepo.Find(inputBusinessCollateralStatus.BusinessCollateralStatusGUID);
				dbBusinessCollateralStatus = businessCollateralStatusRepo.UpdateBusinessCollateralStatus(dbBusinessCollateralStatus, inputBusinessCollateralStatus);
				base.LogTransactionUpdate<BusinessCollateralStatus>(GetOriginalValues<BusinessCollateralStatus>(dbBusinessCollateralStatus), dbBusinessCollateralStatus);
				UnitOfWork.Commit();
				return dbBusinessCollateralStatus.ToBusinessCollateralStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBusinessCollateralStatus(string item)
		{
			try
			{
				IBusinessCollateralStatusRepo businessCollateralStatusRepo = new BusinessCollateralStatusRepo(db);
				Guid businessCollateralStatusGUID = new Guid(item);
				BusinessCollateralStatus businessCollateralStatus = businessCollateralStatusRepo.Find(businessCollateralStatusGUID);
				businessCollateralStatusRepo.Remove(businessCollateralStatus);
				base.LogTransactionDelete<BusinessCollateralStatus>(businessCollateralStatus);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
