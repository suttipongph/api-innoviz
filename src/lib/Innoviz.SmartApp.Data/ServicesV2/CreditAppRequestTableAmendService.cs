using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ICreditAppRequestTableAmendService
    {

        AuthorizedPersonTransItemView GetAuthorizedPersonTransById(string id);
        AuthorizedPersonTransItemView CreateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView);
        AuthorizedPersonTransItemView UpdateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView);
        bool DeleteAuthorizedPersonTrans(string id);
        CreditAppRequestTableAmendItemView GetCreditAppRequestTableAmendById(string id);
        CreditAppRequestTableAmendItemView CreateCreditAppRequestTableAmend(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView);
        CreditAppRequestTableAmendItemView UpdateCreditAppRequestTableAmend(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView);
        bool DeleteCreditAppRequestTableAmend(string id);
        CreditAppRequestTableAmendItemView GetCreditAppRequestTableAmnendInitialData(string creditAppId);
        GuarantorTransItemView GetGuarantorTransById(string id);
        GuarantorTransItemView CreateGuarantorTrans(GuarantorTransItemView guarantorTransView);
        GuarantorTransItemView UpdateGuarantorTrans(GuarantorTransItemView guarantorTransView);
        bool DeleteGuarantorTrans(string id);
        MemoTransItemView GetMemoTransInitialData(string refGUID);
        #region Amend CA
        decimal GetInterestTypeValueByCreditAppRequestAmend(CreditAppRequestTableAmendItemView creditAppRequestTableAmnedItem);
        AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID);
        GuarantorTransItemView GetGuarantorTransInitialData(string refGUID);
        CreditAppRequestTableAmendItemView GetCreditAppRequestTableAmendUnboundForAmendRate(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView);
        CreditAppRequestTableItemView UpdateCreditAppRequestTableByAmendCA(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView);
        void CreateAuthorizedPersonTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView);
        void DeleteAuthorizedPersonTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView);
        void CreateGuarantorTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView);
        void DeleteGuarantorTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView);
        decimal GetCustomerCreditLimit(CreditAppRequestTableAmendItemView item);
        #endregion Amend CA
    }
    public class CreditAppRequestTableAmendService : SmartAppService, ICreditAppRequestTableAmendService
    {
        public CreditAppRequestTableAmendService(SmartAppDbContext context) : base(context) { }
        public CreditAppRequestTableAmendService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public CreditAppRequestTableAmendService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CreditAppRequestTableAmendService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public CreditAppRequestTableAmendService() : base() { }

        public AuthorizedPersonTransItemView GetAuthorizedPersonTransById(string id)
        {
            try
            {
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                return authorizedPersonTransRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AuthorizedPersonTransItemView CreateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                authorizedPersonTransView = accessLevelService.AssignOwnerBU(authorizedPersonTransView);
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                AuthorizedPersonTrans authorizedPersonTrans = authorizedPersonTransView.ToAuthorizedPersonTrans();
                authorizedPersonTrans = authorizedPersonTransRepo.CreateAuthorizedPersonTrans(authorizedPersonTrans);
                base.LogTransactionCreate<AuthorizedPersonTrans>(authorizedPersonTrans);
                UnitOfWork.Commit();
                return authorizedPersonTrans.ToAuthorizedPersonTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AuthorizedPersonTransItemView UpdateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView)
        {
            try
            {
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                AuthorizedPersonTrans inputAuthorizedPersonTrans = authorizedPersonTransView.ToAuthorizedPersonTrans();
                AuthorizedPersonTrans dbAuthorizedPersonTrans = authorizedPersonTransRepo.Find(inputAuthorizedPersonTrans.AuthorizedPersonTransGUID);
                dbAuthorizedPersonTrans = authorizedPersonTransRepo.UpdateAuthorizedPersonTrans(dbAuthorizedPersonTrans, inputAuthorizedPersonTrans);
                base.LogTransactionUpdate<AuthorizedPersonTrans>(GetOriginalValues<AuthorizedPersonTrans>(dbAuthorizedPersonTrans), dbAuthorizedPersonTrans);
                UnitOfWork.Commit();
                return dbAuthorizedPersonTrans.ToAuthorizedPersonTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteAuthorizedPersonTrans(string item)
        {
            try
            {
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                Guid authorizedPersonTransGUID = new Guid(item);
                AuthorizedPersonTrans authorizedPersonTrans = authorizedPersonTransRepo.Find(authorizedPersonTransGUID);
                authorizedPersonTransRepo.Remove(authorizedPersonTrans);
                base.LogTransactionDelete<AuthorizedPersonTrans>(authorizedPersonTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CreditAppRequestTableAmendItemView GetCreditAppRequestTableAmendById(string id)
        {
            try
            {
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                return creditAppRequestTableAmendRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableAmendItemView CreateCreditAppRequestTableAmend(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                creditAppRequestTableAmendView = accessLevelService.AssignOwnerBU(creditAppRequestTableAmendView);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.Find(creditAppRequestTableAmendView.RefCreditAppTableGUID.StringToGuid());
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                ValidateExpired(creditAppRequestTableAmendView);
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendView.ToCreditAppRequestTableAmend();
                ValidateCARequest(creditAppRequestTableAmend, creditAppTable);
                CreditAppRequestTable creditAppRequestTable = SaveDataOnCreate(creditAppRequestTableAmendView, creditAppRequestTableAmend, creditAppTable);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                creditAppRequestTable.CreditAppRequestTableGUID = Guid.NewGuid();
                creditAppRequestTableAmend.CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID;
                creditAppRequestTableAmend.CreditAppRequestTableAmendGUID = Guid.NewGuid();

                string userName = db.GetUserName();
                IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
                List<CAReqRetentionOutstanding> caReqRetentionOutstanding = new List<CAReqRetentionOutstanding>();
                List<RetentionOutstandingView> retentionOutstandingViews = retentionTransRepo.GetRetentionOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID);
                if (retentionOutstandingViews.Any())
                {
                    retentionOutstandingViews.ForEach(f =>
                    {
                        caReqRetentionOutstanding.Add(new CAReqRetentionOutstanding
                        {
                            CAReqRetentionOutstandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID,
                            ProductType = f.ProductType,
                            MaximumRetention = f.MaximumRetention,
                            AccumRetentionAmount = f.AccumRetentionAmount,
                            RemainingAmount = f.RemainingAmount,
                            CreditAppTableGUID = f.CreditAppTableGUID,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            CompanyGUID = f.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID,
                        });
                    });
                }
                List<CAReqCreditOutStanding> caReqCreditOutStanding = new List<CAReqCreditOutStanding>();
                List<CreditOutstandingViewMap> creditOustandingView = creditAppTableRepo.GetCreditOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID, creditAppRequestTable.RequestDate);
                if (creditOustandingView.Any())
                {
                    creditOustandingView.ForEach(f =>
                    {
                        caReqCreditOutStanding.Add(new CAReqCreditOutStanding
                        {
                            CAReqCreditOutStandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID,
                            ProductType = f.ProductType,
                            CreditLimitTypeGUID = f.CreditLimitTypeGUID,
                            AccumRetentionAmount = f.AccumRetentionAmount,
                            AsOfDate = f.AsOfDate,
                            CreditAppTableGUID = f.CreditAppTableGUID,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            ApprovedCreditLimit = f.ApprovedCreditLimit,
                            CreditLimitBalance = f.CreditLimitBalance,
                            ARBalance = f.ARBalance,
                            ReserveToBeRefund = f.ReserveToBeRefund,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID,
                        });
                    });
                }
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                List<CAReqAssignmentOutstanding> cAReqAssignmentOutstandings = new List<CAReqAssignmentOutstanding>();
                List<AssignmentAgreementOutstandingView> assignmentAgreementOutstandingViews = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID);
                if (assignmentAgreementOutstandingViews.Any())
                {
                    assignmentAgreementOutstandingViews.ForEach(f =>
                    {
                        cAReqAssignmentOutstandings.Add(new CAReqAssignmentOutstanding
                        {
                            CAReqAssignmentOutstandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID.StringToGuid(),
                            AssignmentAgreementTableGUID = f.AssignmentAgreementTableGUID.StringToGuid(),
                            BuyerTableGUID = f.BuyerTableGUID.StringToGuid(),
                            AssignmentAgreementAmount = f.AssignmentAgreementAmount,
                            SettleAmount = f.SettledAmount,
                            RemainingAmount = f.RemainingAmount,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID,
                        });
                    });
                }
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                List<CAReqBuyerCreditOutstanding> cAReqBuyerCreditOutstandings = new List<CAReqBuyerCreditOutstanding>();
                List<CALineOutstandingViewMap> caLineOutstandingViewMap = creditAppLineRepo.GetCALineOutstandingByCA((int)RefType.CreditAppTable, creditAppRequestTableAmendView.RefCreditAppTableGUID.StringToGuid(), true, creditAppRequestTable.RequestDate);
                if (caLineOutstandingViewMap.Any())
                {
                    caLineOutstandingViewMap.ForEach(f =>
                    {
                        cAReqBuyerCreditOutstandings.Add(new CAReqBuyerCreditOutstanding
                        {
                            CAReqBuyerCreditOutstandingGUID = Guid.NewGuid(),
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            CreditAppTableGUID = f.CreditAppTableGUID,
                            CreditAppLineGUID = f.CreditAppLineGUID,
                            LineNum = f.LineNum,
                            BuyerTableGUID = f.BuyerTableGUID,
                            Status = f.Status,
                            ApprovedCreditLimitLine = f.ApprovedCreditLimitLine,
                            ARBalance = f.ARBalance,
                            MaxPurchasePct = f.MaxPurchasePct,
                            AssignmentMethodGUID = f.AssignmentMethodGUID,
                            BillingResponsibleByGUID = f.BillingResponsibleByGUID,
                            MethodOfPaymentGUID = f.MethodOfPaymentGUID,
                            ProductType = f.ProductType,
                            CompanyGUID = f.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID,
                        });
                    });
                }

                #region Create
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        BulkInsert(creditAppRequestTable.FirstToList());
                        BulkInsert(creditAppRequestTableAmend.FirstToList());
                        if (caReqRetentionOutstanding.Any())
                        {
                            BulkInsert(caReqRetentionOutstanding);
                        }
                        if (caReqCreditOutStanding.Any())
                        {
                            BulkInsert(caReqCreditOutStanding);
                        }
                        if (cAReqAssignmentOutstandings.Any())
                        {
                            BulkInsert(cAReqAssignmentOutstandings);
                        }
                        if (cAReqBuyerCreditOutstandings.Any())
                        {
                            BulkInsert(cAReqBuyerCreditOutstandings);
                        }
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
                #endregion

                return creditAppRequestTableAmend.ToCreditAppRequestTableAmendItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableAmendItemView UpdateCreditAppRequestTableAmend(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView)
        {
            try
            {
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                ValidateExpired(creditAppRequestTableAmendView);
                CreditAppRequestTableAmend inputCreditAppRequestTableAmend = creditAppRequestTableAmendView.ToCreditAppRequestTableAmend();
                CreditAppRequestTableAmend dbCreditAppRequestTableAmend = creditAppRequestTableAmendRepo.Find(inputCreditAppRequestTableAmend.CreditAppRequestTableAmendGUID);

                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable inputCreditAppRequestTable = SaveDataOnView(creditAppRequestTableAmendView);
                creditAppRequestTableService.UpdateCreditAppRequestTable(inputCreditAppRequestTable);
                dbCreditAppRequestTableAmend = creditAppRequestTableAmendRepo.UpdateCreditAppRequestTableAmend(dbCreditAppRequestTableAmend, inputCreditAppRequestTableAmend);
                base.LogTransactionUpdate<CreditAppRequestTableAmend>(GetOriginalValues<CreditAppRequestTableAmend>(dbCreditAppRequestTableAmend), dbCreditAppRequestTableAmend);
                UnitOfWork.Commit();
                return dbCreditAppRequestTableAmend.ToCreditAppRequestTableAmendItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableAmend UpdateCreditAppRequestTableAmend(CreditAppRequestTableAmend creditAppRequestTableAmend)
        {
            try
            {
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                CreditAppRequestTableAmend inputCreditAppRequestTableAmend = creditAppRequestTableAmend;
                CreditAppRequestTableAmend dbCreditAppRequestTableAmend = creditAppRequestTableAmendRepo.Find(inputCreditAppRequestTableAmend.CreditAppRequestTableAmendGUID);
                dbCreditAppRequestTableAmend = creditAppRequestTableAmendRepo.UpdateCreditAppRequestTableAmend(dbCreditAppRequestTableAmend, inputCreditAppRequestTableAmend);
                base.LogTransactionUpdate<CreditAppRequestTableAmend>(GetOriginalValues<CreditAppRequestTableAmend>(dbCreditAppRequestTableAmend), dbCreditAppRequestTableAmend);
                return dbCreditAppRequestTableAmend;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCreditAppRequestTableAmend(string item)
        {
            try
            {
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                Guid creditAppRequestTableAmendGUID = new Guid(item);
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.Find(creditAppRequestTableAmendGUID);
                creditAppRequestTableAmendRepo.Remove(creditAppRequestTableAmend);
                base.LogTransactionDelete<CreditAppRequestTableAmend>(creditAppRequestTableAmend);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableAmendItemView GetCreditAppRequestTableAmnendInitialData(string creditAppId)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(creditAppId.StringToGuid());
                IDocumentService documentService = new DocumentService(db);
                DocumentStatus draft = documentService.GetDocumentStatusByStatusId((int)CreditAppRequestStatus.Draft);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(creditAppTable.CustomerTableGUID);
                DateTime dateNow = DateTime.Now;
                int creditAppRequestType = 0;
                if (creditAppTable.InactiveDate.HasValue)
                {
                    creditAppRequestType = dateNow >= creditAppTable.InactiveDate ? (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit : 0;
                }
                return new CreditAppRequestTableAmendItemView
                {
                    CreditAppRequestTableAmendGUID = new Guid().GuidNullToString(),
                    CreditAppRequestTable_RequestDate = dateNow.DateToString(),
                    CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                    CreditAppRequestTable_CustomerCreditLimit = creditAppTableRepo.GetCustomerCreditLimit(creditAppTable.CustomerTableGUID, creditAppTable.ProductType, creditAppTable.ExpiryDate),
                    RefCreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
                    DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(draft.Description),
                    DocumentStatus_StatusId = draft.StatusId,
                    CreditAppRequestTable_DocumentStatusGUID = draft.DocumentStatusGUID.GuidNullToString(),
                    RefCreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
                    CreditAppRequestTable_ExpiryDate = creditAppTable.ExpiryDate.DateToString(),
                    CreditAppRequestTable_BlacklistStatusGUID = customerTable.BlacklistStatusGUID.GuidNullToString(),
                    CreditAppRequestTable_KYCGUID = customerTable.KYCSetupGUID.GuidNullToString(),
                    CreditAppRequestTable_CreditScoringGUID = customerTable.CreditScoringGUID.GuidNullToString(),
                    CreditAppRequestTable_CreditAppRequestType = creditAppRequestType,
                    CreditAppTable_InactiveDate = creditAppTable.InactiveDate.DateNullToString(),
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public GuarantorTransItemView GetGuarantorTransById(string id)
        {
            try
            {
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                return guarantorTransRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView CreateGuarantorTrans(GuarantorTransItemView guarantorTransView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                guarantorTransView = accessLevelService.AssignOwnerBU(guarantorTransView);
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                GuarantorTrans guarantorTrans = guarantorTransView.ToGuarantorTrans();
                guarantorTrans = guarantorTransRepo.CreateGuarantorTrans(guarantorTrans);
                base.LogTransactionCreate<GuarantorTrans>(guarantorTrans);
                UnitOfWork.Commit();
                return guarantorTrans.ToGuarantorTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView UpdateGuarantorTrans(GuarantorTransItemView guarantorTransView)
        {
            try
            {
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                GuarantorTrans inputGuarantorTrans = guarantorTransView.ToGuarantorTrans();
                GuarantorTrans dbGuarantorTrans = guarantorTransRepo.Find(inputGuarantorTrans.GuarantorTransGUID);
                dbGuarantorTrans = guarantorTransRepo.UpdateGuarantorTrans(dbGuarantorTrans, inputGuarantorTrans);
                base.LogTransactionUpdate<GuarantorTrans>(GetOriginalValues<GuarantorTrans>(dbGuarantorTrans), dbGuarantorTrans);
                UnitOfWork.Commit();
                return dbGuarantorTrans.ToGuarantorTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteGuarantorTrans(string item)
        {
            try
            {
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                Guid guarantorTransGUID = new Guid(item);
                GuarantorTrans guarantorTrans = guarantorTransRepo.Find(guarantorTransGUID);
                guarantorTransRepo.Remove(guarantorTrans);
                base.LogTransactionDelete<GuarantorTrans>(guarantorTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTable SaveDataOnCreate(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView, CreditAppRequestTableAmend creditAppRequestTableAmend, CreditAppTable creditAppTable)
        {
            try
            {
                #region Initial CreditAppRequestTable 
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                Guid creditAppTableGUID = creditAppRequestTableAmendView.RefCreditAppTableGUID.StringToGuid();
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.CopyByCreditAppRequestTableAmend(creditAppTableGUID);
                creditAppRequestTableService.GenCreditAppRequestNumberSeqCode(creditAppRequestTable);
                creditAppRequestTable.DocumentStatusGUID = creditAppRequestTableAmendView.CreditAppRequestTable_DocumentStatusGUID.StringToGuid();
                creditAppRequestTable.Description = creditAppRequestTableAmendView.CreditAppRequestTable_Description;
                creditAppRequestTable.CreditAppRequestType = creditAppRequestTableAmendView.CreditAppRequestTable_CreditAppRequestType;
                creditAppRequestTable.RequestDate = creditAppRequestTableAmendView.CreditAppRequestTable_RequestDate.StringToDate();
                creditAppRequestTable.Remark = creditAppRequestTableAmendView.CreditAppRequestTable_Remark;
                creditAppRequestTable.CustomerCreditLimit = creditAppRequestTableAmendView.CreditAppRequestTable_CustomerCreditLimit;
                creditAppRequestTable.CACondition = creditAppRequestTableAmendView.CreditAppRequestTable_CACondition;
                creditAppRequestTable.BlacklistStatusGUID = creditAppRequestTableAmendView.CreditAppRequestTable_BlacklistStatusGUID.StringToGuidNull();
                creditAppRequestTable.KYCGUID = creditAppRequestTableAmendView.CreditAppRequestTable_KYCGUID.StringToGuidNull();
                creditAppRequestTable.CreditScoringGUID = creditAppRequestTableAmendView.CreditAppRequestTable_CreditScoringGUID.StringToGuidNull();
                #endregion
                #region Initial CreditAppRequestTable 
                creditAppRequestTableAmend.OriginalCreditLimit = creditAppTable.ApprovedCreditLimit;
                creditAppRequestTableAmend.OriginalInterestTypeGUID = creditAppTable.InterestTypeGUID;
                creditAppRequestTableAmend.OriginalInterestAdjustmentPct = creditAppTable.InterestAdjustment;
                creditAppRequestTableAmend.OriginalTotalInterestPct = creditAppTable.TotalInterestPct;
                creditAppRequestTableAmend.OriginalMaxRetentionPct = creditAppTable.MaxRetentionPct;
                creditAppRequestTableAmend.OriginalMaxRetentionAmount = creditAppTable.MaxRetentionAmount;
                creditAppRequestTableAmend.OriginalMaxPurchasePct = creditAppTable.MaxPurchasePct;
                creditAppRequestTableAmend.OriginalPurchaseFeePct = creditAppTable.PurchaseFeePct;
                creditAppRequestTableAmend.OriginalPurchaseFeeCalculateBase = creditAppTable.PurchaseFeeCalculateBase;
                creditAppRequestTableAmend.OriginalCreditLimitRequestFeeAmount = creditAppRequestTable.CreditRequestFeeAmount;
                return creditAppRequestTable;
                #endregion
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTable SaveDataOnView(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView)
        {
            try
            {
                #region Initial CreditAppRequestTable 
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(creditAppRequestTableAmendView.CreditAppRequestTableAmendGUID.StringToGuid());
                bool isActiveAmendCustomerCreditLimit = (creditAppRequestTableAmendView.CreditAppRequestTable_CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit)
                    ? true : false;
                creditAppRequestTable.RequestDate = creditAppRequestTableAmendView.CreditAppRequestTable_RequestDate.StringToDate();
                creditAppRequestTable.Description = creditAppRequestTableAmendView.CreditAppRequestTable_Description;
                creditAppRequestTable.Remark = creditAppRequestTableAmendView.CreditAppRequestTable_Remark;
                creditAppRequestTable.CreditRequestFeeAmount = creditAppRequestTableAmendView.CreditAppRequestTable_NewCreditLimitRequestFeeAmount;
                creditAppRequestTable.InterestTypeGUID = creditAppRequestTableAmendView.CreditAppRequestTable_NewInterestTypeGUID.StringToGuid();
                creditAppRequestTable.InterestAdjustment = creditAppRequestTableAmendView.CreditAppRequestTable_NewInterestAdjustmentPct;
                creditAppRequestTable.CreditRequestFeePct = creditAppRequestTableAmendView.CreditAppRequestTable_NewCreditLimitRequestFeePct;
                creditAppRequestTable.MaxPurchasePct = creditAppRequestTableAmendView.CreditAppRequestTable_NewMaxPurchasePct;
                creditAppRequestTable.PurchaseFeePct = creditAppRequestTableAmendView.CreditAppRequestTable_NewPurchaseFeePct;
                creditAppRequestTable.PurchaseFeeCalculateBase = creditAppRequestTableAmendView.CreditAppRequestTable_NewPurchaseFeeCalculateBase;
                creditAppRequestTable.MarketingComment = creditAppRequestTableAmendView.CreditAppRequestTable_MarketingComment;
                creditAppRequestTable.CreditComment = creditAppRequestTableAmendView.CreditAppRequestTable_CreditComment;
                creditAppRequestTable.ApproverComment = creditAppRequestTableAmendView.CreditAppRequestTable_ApproverComment;
                creditAppRequestTable.ApprovedCreditLimitRequest = creditAppRequestTableAmendView.CreditAppRequestTable_ApprovedCreditLimitRequest;
                creditAppRequestTable.ApprovedDate = creditAppRequestTableAmendView.CreditAppRequestTable_ApprovedDate.StringNullToDateNull();
                creditAppRequestTable.MaxRetentionPct = (isActiveAmendCustomerCreditLimit)
                    ? creditAppRequestTableAmendView.OriginalMaxRetentionPct
                    : creditAppRequestTableAmendView.CreditAppRequestTable_NewMaxRetentionPct;
                creditAppRequestTable.MaxRetentionAmount = (isActiveAmendCustomerCreditLimit)
                    ? creditAppRequestTableAmendView.CreditAppRequestTable_MaxRetentionAmount
                    : creditAppRequestTableAmendView.CreditAppRequestTable_NewMaxRetentionAmount;
                creditAppRequestTable.CreditLimitRequest = (isActiveAmendCustomerCreditLimit)
                    ? creditAppRequestTable.CreditLimitRequest
                    : creditAppRequestTableAmendView.CreditAppRequestTable_NewCreditLimit;
                creditAppRequestTable.CACondition = creditAppRequestTableAmendView.CreditAppRequestTable_CACondition;
                creditAppRequestTable.BlacklistStatusGUID = creditAppRequestTableAmendView.CreditAppRequestTable_BlacklistStatusGUID.StringToGuidNull();
                creditAppRequestTable.KYCGUID = creditAppRequestTableAmendView.CreditAppRequestTable_KYCGUID.StringToGuidNull();
                creditAppRequestTable.CreditScoringGUID = creditAppRequestTableAmendView.CreditAppRequestTable_CreditScoringGUID.StringToGuidNull();
                creditAppRequestTable.SigningCondition = creditAppRequestTableAmendView.CreditAppRequestTable_SigningCondition;
                creditAppRequestTable.FinancialCheckedBy = creditAppRequestTableAmendView.CreditAppRequestTable_FinancialCheckedBy;
                creditAppRequestTable.FinancialCreditCheckedDate = creditAppRequestTableAmendView.CreditAppRequestTable_FinancialCreditCheckedDate.StringNullToDateNull();
                creditAppRequestTable.NCBCheckedBy = creditAppRequestTableAmendView.CreditAppRequestTable_NCBCheckedBy;
                creditAppRequestTable.NCBCheckedDate = creditAppRequestTableAmendView.CreditAppRequestTable_NCBCheckedDate.StringNullToDateNull();
                creditAppRequestTable.CreditLimitRequest = creditAppRequestTableAmendView.CreditAppRequestTable_NewCreditLimit;
                return creditAppRequestTable;
                #endregion
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public void ValidateExpired(CreditAppRequestTableAmendItemView item)
        {
            try
            {
                if (item.CreditAppRequestTable_RequestDate.StringNullToDateNull() >= item.CreditAppRequestTable_ExpiryDate.StringNullToDateNull())
                {
                    SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                    smartAppException.AddData("ERROR.90038");
                    throw smartAppException;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateCARequest(CreditAppRequestTableAmend item, CreditAppTable creditAppTable)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = new CreditAppRequestTable();
                var existCreditAppRequest = creditAppRequestTableRepo.GetExistByCAReqAmend(item, creditAppRequestTable).Any();

                if (existCreditAppRequest)
                {
                    SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                    smartAppException.AddData("ERROR.90004", new string[] { "LABEL.AMEND_CREDIT_APPLICATION", SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) });
                    throw smartAppException;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetInterestTypeValueByCreditAppRequestAmend(CreditAppRequestTableAmendItemView creditAppRequestTableAmnedItem)
        {
            try
            {
                IInterestTypeService interestTypeService = new InterestTypeService(db);
                return interestTypeService.GetInterestTypeValue(creditAppRequestTableAmnedItem.CreditAppRequestTable_NewInterestTypeGUID.StringToGuid(), creditAppRequestTableAmnedItem.CreditAppRequestTable_RequestDate.StringToDate());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(refGUID.StringToGuid());
                return authorizedPersonTransService.GetAuthorizedPersonTransInitialData(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString(), Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView GetGuarantorTransInitialData(string refGUID)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(refGUID.StringToGuid());
                return guarantorTransService.GetGuarantorTransInitialData(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString(), Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableAmendItemView GetCreditAppRequestTableAmendUnboundForAmendRate(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                IInterestTypeService interestTypeService = new InterestTypeService(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo
                .GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID.StringToGuid());
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID.StringToGuid());
                creditAppRequestTableAmend.AmendRate = true;
                UpdateCreditAppRequestTableAmend(creditAppRequestTableAmend);
                UnitOfWork.Commit();
                CreditAppRequestTableAmendItemView new_CreditAppRequestTableAmendItemView = new CreditAppRequestTableAmendItemView();
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewCreditLimitRequestFeePct = creditAppRequestTable.CreditRequestFeePct;
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewMaxRetentionPct = creditAppRequestTable.MaxRetentionPct;
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewMaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount;
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewMaxPurchasePct = creditAppRequestTable.MaxPurchasePct;
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewPurchaseFeePct = creditAppRequestTable.PurchaseFeePct;
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewPurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase;
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewInterestTypeGUID = creditAppRequestTable.InterestTypeGUID.GuidNullToString();
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewInterestAdjustmentPct = creditAppRequestTable.InterestAdjustment;
                new_CreditAppRequestTableAmendItemView.CreditAppRequestTable_NewTotalInterestPct = creditAppRequestTable.InterestAdjustment +
                    interestTypeService.GetInterestTypeValue(creditAppRequestTable.InterestTypeGUID, creditAppRequestTableAmendItemView.CreditAppRequestTable_RequestDate.StringToDate());
                return new_CreditAppRequestTableAmendItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView UpdateCreditAppRequestTableByAmendCA(CreditAppRequestTableAmendItemView creditAppRequestTableAmendView)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                IInterestTypeService interestTypeService = new InterestTypeService(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo
                .GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(creditAppRequestTableAmendView.CreditAppRequestTableAmendGUID.StringToGuid());
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(creditAppRequestTableAmendView.CreditAppRequestTableAmendGUID.StringToGuid());
                creditAppRequestTableAmend.AmendRate = false;
                UpdateCreditAppRequestTableAmend(creditAppRequestTableAmend);
                creditAppRequestTable.InterestTypeGUID = creditAppRequestTableAmendView.OriginalInterestTypeGUID.StringToGuid();
                creditAppRequestTable.InterestAdjustment = creditAppRequestTableAmendView.OriginalInterestAdjustmentPct;
               // creditAppRequestTable.CreditRequestFeePct = creditAppRequestTableAmendView.OriginalCreditLimitRequestFeePct;
                creditAppRequestTable.MaxRetentionPct = creditAppRequestTableAmendView.OriginalMaxRetentionPct;
                creditAppRequestTable.MaxRetentionAmount = creditAppRequestTableAmendView.OriginalMaxRetentionAmount;
                creditAppRequestTable.MaxPurchasePct = creditAppRequestTableAmendView.OriginalMaxPurchasePct;
                creditAppRequestTable.PurchaseFeePct = creditAppRequestTableAmendView.OriginalPurchaseFeePct;
                creditAppRequestTable.CreditLimitRequest = creditAppRequestTableAmendView.CreditAppRequestTable_NewCreditLimit;
                creditAppRequestTable.PurchaseFeeCalculateBase = creditAppRequestTableAmendView.OriginalPurchaseFeeCalculateBase;
                creditAppRequestTableService.UpdateCreditAppRequestTable(creditAppRequestTable);
                UnitOfWork.Commit();
                CreditAppRequestTableItemView creditAppRequestTableItemView = creditAppRequestTable.ToCreditAppRequestTableItemView();
                creditAppRequestTableItemView.TotalInterestPct = creditAppRequestTable.InterestAdjustment +
                    interestTypeService.GetInterestTypeValue(creditAppRequestTable.InterestTypeGUID, creditAppRequestTableAmendView.CreditAppRequestTable_RequestDate.StringToDate());
                return creditAppRequestTableItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateAuthorizedPersonTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                List<AuthorizedPersonTrans> authorizedPersonTrans = authorizedPersonTransRepo.GetAuthorizedPersonTransByReference(creditAppRequestTableAmendItemView.RefCreditAppTableGUID.StringToGuid(), (int)RefType.CreditAppTable).Where(w => w.InActive == false).ToList();
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID.StringToGuid());
                creditAppRequestTableAmend.AmendAuthorizedPerson = true;
                string userName = db.GetUserName();
                DateTime systemDate = DateTime.Now;

                if (authorizedPersonTrans.Any())
                {
                    authorizedPersonTrans.ToList().ForEach(f =>
                    {
                        f.RefAuthorizedPersonTransGUID = f.AuthorizedPersonTransGUID;
                        f.AuthorizedPersonTransGUID = Guid.NewGuid();
                        f.RefType = (int)RefType.CreditAppRequestTable;
                        f.RefGUID = creditAppRequestTableAmendItemView.CreditAppRequestTableGUID.StringToGuid();
                        f.CreatedBy = userName;
                        f.ModifiedBy = userName;
                        f.CreatedDateTime = systemDate;
                        f.ModifiedDateTime = systemDate;
                        f.Owner = userName;
                        f.OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID;
                    });
                }
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        try
                        {
                            if (authorizedPersonTrans.Any())
                            {
                                authorizedPersonTransRepo.ValidateAdd(authorizedPersonTrans);
                                BulkInsert(authorizedPersonTrans);
                            }
                            BulkUpdate(creditAppRequestTableAmend.FirstToList());
                            UnitOfWork.Commit(transaction);
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            throw SmartAppUtil.AddStackTrace(e);
                        }
                    }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void DeleteAuthorizedPersonTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                List<AuthorizedPersonTrans> authorizedPersonTranses = authorizedPersonTransRepo.GetAuthorizedPersonTransByReference(creditAppRequestTableAmendItemView.CreditAppRequestTableGUID.StringToGuid(), (int)RefType.CreditAppRequestTable).ToList();
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID.StringToGuid());
                creditAppRequestTableAmend.AmendAuthorizedPerson = false;
                string userName = db.GetUserName();
                DateTime systemDate = DateTime.Now;
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        if (authorizedPersonTranses.Any())
                        {
                            authorizedPersonTransRepo.ValidateRemove(authorizedPersonTranses);
                            BulkDelete(authorizedPersonTranses);
                        }
                        BulkUpdate(creditAppRequestTableAmend.FirstToList());
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateGuarantorTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                List<GuarantorTrans> guarantorTrans = guarantorTransRepo.GetGuarantorTransByReference(creditAppRequestTableAmendItemView.RefCreditAppTableGUID.StringToGuid(), (int)RefType.CreditAppTable).Where(w => w.InActive == false).ToList();
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID.StringToGuid());
                creditAppRequestTableAmend.AmendGuarantor = true;
                string userName = db.GetUserName();
                DateTime systemDate = DateTime.Now;
                if (guarantorTrans.Any())
                {
                    guarantorTrans.ToList().ForEach(f =>
                    {
                        f.RefGuarantorTransGUID = f.GuarantorTransGUID;
                        f.GuarantorTransGUID = Guid.NewGuid();
                        f.RefType = (int)RefType.CreditAppRequestTable;
                        f.RefGUID = creditAppRequestTableAmendItemView.CreditAppRequestTableGUID.StringToGuid();
                        f.CreatedBy = userName;
                        f.ModifiedBy = userName;
                        f.CreatedDateTime = systemDate;
                        f.ModifiedDateTime = systemDate;
                        f.Owner = userName;
                        f.OwnerBusinessUnitGUID = creditAppRequestTableAmend.OwnerBusinessUnitGUID;
                    });
                }
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        try
                        {
                            if (guarantorTrans.Any())
                            {

                                guarantorTransRepo.ValidateAdd(guarantorTrans);
                                BulkInsert(guarantorTrans);
                            }
                            BulkUpdate(creditAppRequestTableAmend.FirstToList());
                            UnitOfWork.Commit(transaction);
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            throw SmartAppUtil.AddStackTrace(e);
                        }
                    }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void DeleteGuarantorTrans(CreditAppRequestTableAmendItemView creditAppRequestTableAmendItemView)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                List<GuarantorTrans> guarantorTranses = guarantorTransRepo.GetGuarantorTransByReference(creditAppRequestTableAmendItemView.CreditAppRequestTableGUID.StringToGuid(), (int)RefType.CreditAppRequestTable).ToList();
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByIdNoTrackingByAccessLevel(creditAppRequestTableAmendItemView.CreditAppRequestTableAmendGUID.StringToGuid());
                creditAppRequestTableAmend.AmendGuarantor = false;
                string userName = db.GetUserName();
                DateTime systemDate = DateTime.Now;
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        if (guarantorTranses.Any())
                        {

                            guarantorTransRepo.ValidateRemove(guarantorTranses);
                            BulkDelete(guarantorTranses);
                        }
                        BulkUpdate(creditAppRequestTableAmend.FirstToList());
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetCustomerCreditLimit(CreditAppRequestTableAmendItemView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.Find(item.RefCreditAppTableGUID.StringToGuid());
                return creditAppTableRepo.GetCustomerCreditLimit(item.CreditAppRequestTable_CustomerTableGUID.StringToGuid(), creditAppTable.ProductType, item.CreditAppRequestTable_RequestDate.StringToDate());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
