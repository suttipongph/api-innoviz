using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IGuarantorTypeService
	{

		GuarantorTypeItemView GetGuarantorTypeById(string id);
		GuarantorTypeItemView CreateGuarantorType(GuarantorTypeItemView guarantorTypeView);
		GuarantorTypeItemView UpdateGuarantorType(GuarantorTypeItemView guarantorTypeView);
		bool DeleteGuarantorType(string id);
	}
	public class GuarantorTypeService : SmartAppService, IGuarantorTypeService
	{
		public GuarantorTypeService(SmartAppDbContext context) : base(context) { }
		public GuarantorTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public GuarantorTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public GuarantorTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public GuarantorTypeService() : base() { }

		public GuarantorTypeItemView GetGuarantorTypeById(string id)
		{
			try
			{
				IGuarantorTypeRepo guarantorTypeRepo = new GuarantorTypeRepo(db);
				return guarantorTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GuarantorTypeItemView CreateGuarantorType(GuarantorTypeItemView guarantorTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				guarantorTypeView = accessLevelService.AssignOwnerBU(guarantorTypeView);
				IGuarantorTypeRepo guarantorTypeRepo = new GuarantorTypeRepo(db);
				GuarantorType guarantorType = guarantorTypeView.ToGuarantorType();
				guarantorType = guarantorTypeRepo.CreateGuarantorType(guarantorType);
				base.LogTransactionCreate<GuarantorType>(guarantorType);
				UnitOfWork.Commit();
				return guarantorType.ToGuarantorTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GuarantorTypeItemView UpdateGuarantorType(GuarantorTypeItemView guarantorTypeView)
		{
			try
			{
				IGuarantorTypeRepo guarantorTypeRepo = new GuarantorTypeRepo(db);
				GuarantorType inputGuarantorType = guarantorTypeView.ToGuarantorType();
				GuarantorType dbGuarantorType = guarantorTypeRepo.Find(inputGuarantorType.GuarantorTypeGUID);
				dbGuarantorType = guarantorTypeRepo.UpdateGuarantorType(dbGuarantorType, inputGuarantorType);
				base.LogTransactionUpdate<GuarantorType>(GetOriginalValues<GuarantorType>(dbGuarantorType), dbGuarantorType);
				UnitOfWork.Commit();
				return dbGuarantorType.ToGuarantorTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteGuarantorType(string item)
		{
			try
			{
				IGuarantorTypeRepo guarantorTypeRepo = new GuarantorTypeRepo(db);
				Guid guarantorTypeGUID = new Guid(item);
				GuarantorType guarantorType = guarantorTypeRepo.Find(guarantorTypeGUID);
				guarantorTypeRepo.Remove(guarantorType);
				base.LogTransactionDelete<GuarantorType>(guarantorType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
