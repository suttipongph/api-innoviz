using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBuyerInvoiceTableService
	{

		BuyerInvoiceTableItemView GetBuyerInvoiceTableById(string id);
		BuyerInvoiceTableItemView CreateBuyerInvoiceTable(BuyerInvoiceTableItemView buyerInvoiceTableView);
		BuyerInvoiceTableItemView UpdateBuyerInvoiceTable(BuyerInvoiceTableItemView buyerInvoiceTableView);
		bool DeleteBuyerInvoiceTable(string id);
		BuyerInvoiceTableItemView GetBuyerInvoiceInitialData(string creditAppLine);
		AccessModeView GetAccessModeByCreditAppLine(string creditAppLineId);
		IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItemByCreditAppLine(SearchParameter search);
		void CreateBuyerInvoiceTableCollection(List<BuyerInvoiceTableItemView> buyerInvoiceTableItemViews);
	}
	public class BuyerInvoiceTableService : SmartAppService, IBuyerInvoiceTableService
	{
		public BuyerInvoiceTableService(SmartAppDbContext context) : base(context) { }
		public BuyerInvoiceTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BuyerInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BuyerInvoiceTableService() : base() { }

		public BuyerInvoiceTableItemView GetBuyerInvoiceTableById(string id)
		{
			try
			{
				IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
				return buyerInvoiceTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerInvoiceTableItemView CreateBuyerInvoiceTable(BuyerInvoiceTableItemView buyerInvoiceTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				buyerInvoiceTableView = accessLevelService.AssignOwnerBU(buyerInvoiceTableView);
				IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
				BuyerInvoiceTable buyerInvoiceTable = buyerInvoiceTableView.ToBuyerInvoiceTable();
				buyerInvoiceTable = buyerInvoiceTableRepo.CreateBuyerInvoiceTable(buyerInvoiceTable);
				base.LogTransactionCreate<BuyerInvoiceTable>(buyerInvoiceTable);
				UnitOfWork.Commit();
				return buyerInvoiceTable.ToBuyerInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerInvoiceTableItemView UpdateBuyerInvoiceTable(BuyerInvoiceTableItemView buyerInvoiceTableView)
		{
			try
			{
				IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
				BuyerInvoiceTable inputBuyerInvoiceTable = buyerInvoiceTableView.ToBuyerInvoiceTable();
				BuyerInvoiceTable dbBuyerInvoiceTable = buyerInvoiceTableRepo.Find(inputBuyerInvoiceTable.BuyerInvoiceTableGUID);
				dbBuyerInvoiceTable = buyerInvoiceTableRepo.UpdateBuyerInvoiceTable(dbBuyerInvoiceTable, inputBuyerInvoiceTable);
				base.LogTransactionUpdate<BuyerInvoiceTable>(GetOriginalValues<BuyerInvoiceTable>(dbBuyerInvoiceTable), dbBuyerInvoiceTable);
				UnitOfWork.Commit();
				return dbBuyerInvoiceTable.ToBuyerInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBuyerInvoiceTable(string item)
		{
			try
			{
				IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
				Guid buyerInvoiceTableGUID = new Guid(item);
				BuyerInvoiceTable buyerInvoiceTable = buyerInvoiceTableRepo.Find(buyerInvoiceTableGUID);
				buyerInvoiceTableRepo.Remove(buyerInvoiceTable);
				base.LogTransactionDelete<BuyerInvoiceTable>(buyerInvoiceTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public BuyerInvoiceTableItemView GetBuyerInvoiceInitialData(string creditAppLineGUID)
        {
            try
            {
				ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
				CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTrackingByAccessLevel(creditAppLineGUID.StringToGuid());
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(creditAppLine.CreditAppTableGUID);
                IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
                BuyerTable buyerTable = buyerTableRepo.GetBuyerTableByIdNoTrackingByAccessLevel(creditAppLine.BuyerTableGUID);


                return new BuyerInvoiceTableItemView
                {
                    BuyerInvoiceTableGUID = new Guid().GuidNullToString(),
                    BuyerId = SmartAppUtil.GetDropDownLabel(buyerTable.BuyerId, buyerTable.BuyerName),
					CreditAppLineGUID = creditAppLine.CreditAppLineGUID.GuidNullToString(),
                    CreditAppId = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
					LineNum = creditAppLine.LineNum,
					CreditAppLine_BuyerTableGUID = creditAppLine.BuyerTableGUID.GuidNullToString(),
					CreditAppTable_CustomerTableGUID = creditAppTable.CustomerTableGUID.GuidNullToString()
				};
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AccessModeView GetAccessModeByCreditAppLine(string creditAppLineId)
        {
            AccessModeView accessModeView = new AccessModeView();
            bool creditAppLineExpireAndInactive = GetExprieAndInactiveByCreditAppLine(creditAppLineId);
            //bool getCanDelete = GetCanDeleteByCreditAppLine(creditAppLineId);
            accessModeView.CanCreate = creditAppLineExpireAndInactive;
            accessModeView.CanView = creditAppLineExpireAndInactive;
            accessModeView.CanDelete = true;
            return accessModeView;
        }
        public bool GetExprieAndInactiveByCreditAppLine(string creditAppLineId)
		{
			try
			{
				ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
				CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(creditAppLineId.StringToGuid());
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppLine.CreditAppTableGUID);
				DateTime expireDate = creditAppTable.ExpiryDate;
				DateTime? inActiveDate = creditAppTable.InactiveDate;
				DateTime currentDate = DateTime.Now;
				bool canAccessMode = false ;
				if (currentDate < expireDate) {
					if (inActiveDate == null) {
						return canAccessMode = true;
					} else if (currentDate <= inActiveDate.Value) {
						return canAccessMode = true;
					}
					else {
						return canAccessMode = false;
					}
                }
                else
                {
					return canAccessMode = false;
				}
				
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItemByCreditAppLine(SearchParameter search)
		{
			try
			{
				IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
				search = search.GetParentCondition(BuyerInvoiceTableCondition.CreditAppLineGUID);
				return buyerInvoiceTableRepo.GetDropDownItemByPurchaseLine(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateBuyerInvoiceTableCollection(List<BuyerInvoiceTableItemView> buyerInvoiceTableItemViews)
		{
			try
			{
				if (buyerInvoiceTableItemViews.Any())
				{
					IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
					List<BuyerInvoiceTable> buyerInvoiceTables = buyerInvoiceTableItemViews.ToBuyerInvoiceTable().ToList();
					buyerInvoiceTableRepo.ValidateAdd(buyerInvoiceTables);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(buyerInvoiceTables, false);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
	}
}
