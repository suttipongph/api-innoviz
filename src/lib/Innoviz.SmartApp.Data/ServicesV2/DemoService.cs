﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDemoService
	{
		IEnumerable<SelectItem<DocumentProcessItemView>> GetDocumentProcessDropdown(SearchParameter search);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusByProcess(SearchParameter search);
		IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeFilterActiveStatus(SearchParameter search);
		Task DemoImportExcel();
		Task<WorkflowResultView> CallFunctionThenActionK2(DemoFunctionParm demoFunctionParm);
		ReplaceBookmarkDemoView GetDemoBookmarkValues(Guid refGUID);
		Task<List<FileInformation>> DemoReplaceBookmark(List<Guid> guids, Guid refGUID);
		DemoItemView GetTimeTest(DemoItemView view);
		DemoItemView SetTimeTest(DemoItemView view);
	}
	public class DemoService : SmartAppService, IDemoService
	{
		public DemoService(SmartAppDbContext context) : base(context) { }
		public DemoService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DemoService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DemoService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DemoService(List<DbContext> contexts, ISysTransactionLogService transactionLogService, IBatchLogService batchLogService)
			: base(contexts, transactionLogService, batchLogService)
		{
			
		}
		public DemoService() : base() { }

		public IEnumerable<SelectItem<DocumentProcessItemView>> GetDocumentProcessDropdown(SearchParameter search)
		{
			try
			{
				IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
				return documentProcessRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusByProcess(SearchParameter search)
		{
			try
			{
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				search = search.GetParentCondition(DocumentProcessCondition.DocumentProcessGUID);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<EmployeeTableItemView>> GetEmployeeFilterActiveStatus(SearchParameter search)
		{
			try
			{
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				SearchCondition searchCondition = SearchConditionService.GetEmployeeActiveCondition();
				search.Conditions.Add(searchCondition);
				return employeeTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DemoBulkInsert()
		{
			try
			{
				IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);

				List<BusinessUnit> newItems = new List<BusinessUnit>();
				for (int i = 0; i < 10; i++)
				{
					BusinessUnit item = new BusinessUnit
					{
						//BusinessUnitGUID = Guid.NewGuid(),
						BusinessUnitId = "UNIT[" + i + "]",
						CompanyGUID = GetCurrentCompany().StringToGuid(),
						Description = "unit " + i,
					};
					newItems.Add(item);
				}

                #region using repo.Create, Update, Remove
                // prepare value for update (some business logic...)
                var businessunit = businessUnitRepo.GetBusinessUnitByCompanyNoTracking(GetCurrentCompany().StringToGuid()).FirstOrDefault();
				var employeeTable = employeeTableRepo.GetEmployeeTableByUserIdAndCompany(GetUserId(), GetCurrentCompany()).ToEmployeeTable();

				// more business logic ...
				
				// for repo.Create, repo.Update, repo.Delete => use base.LogTransaction after the call
				var dbBusinessUnit = businessUnitRepo.Find(businessunit.BusinessUnitGUID);
				businessUnitRepo.UpdateBusinessUnitVoid(dbBusinessUnit, businessunit);
				base.LogTransactionUpdate<BusinessUnit>(GetOriginalValues<BusinessUnit>(dbBusinessUnit), businessunit);

				var dbEmployeeTable = employeeTableRepo.Find(employeeTable.EmployeeTableGUID);
				employeeTableRepo.UpdateEmployeeTableVoid(dbEmployeeTable, employeeTable);
				base.LogTransactionUpdate<EmployeeTable>(GetOriginalValues<EmployeeTable>(dbEmployeeTable), employeeTable);

				// for Create => also call AssignOwnerBU
				var newEmployee = new EmployeeTable();
				// business...
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				newEmployee = accessLevelService.AssignOwnerBU(newEmployee);
				employeeTableRepo.CreateEmployeeTable(employeeTable);
				base.LogTransactionCreate<EmployeeTable>(newEmployee);

				//UnitOfWork.Commit();
                #endregion

                using (var transaction = UnitOfWork.ContextTransaction())
				{
					// normal bulk update (check duplicate)
					//this.BulkUpdate(all);

					// bulk update skip check duplicate
					//this.BulkUpdate(all, false);

					// BulkInsert with auto-generated PK (guid)
					//this.BulkInsert(newItems, true);

					// normal BulkInsert (PK is assigned)
					//this.BulkInsert(newItmes);

					// bulk insert skip check duplicate
					this.BulkInsert(newItems, false, false);

					//this.BulkDelete(newItems);

					UnitOfWork.Commit(transaction);
				}
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public async Task DemoImportExcel()
        {
            try
            {
				// prepare fileUpload
				FileInformation fileInfo1 = new FileInformation { Path = @"D:\Projects\Project_Docs\LIT\test\importFile01.xlsx" };
				fileInfo1 = await fileInfo1.GetFileContent();
				FileInformation fileInfo2 = new FileInformation { Path = @"D:\Projects\Project_Docs\LIT\test\importFile02.xlsx" };
				fileInfo2 = await fileInfo2.GetFileContent();
				FileInformation fileInfo3 = new FileInformation { Path = @"D:\Projects\Project_Docs\LIT\test\importFile03.xlsx" };
				fileInfo3 = await fileInfo3.GetFileContent();
				FileUpload fileUpload = new FileUpload { FileInfos = new List<FileInformation>() { fileInfo1, fileInfo2, fileInfo3 } };
				
				// get excel data
				FileHelper fileHelper = new FileHelper();
				List<ImportDemoViewMap> result = fileHelper.MapInputExcelFileToSingleEntityType<ImportDemoViewMap>(fileUpload, ImportDemoViewMap.GetMapper());

				// call business service
				// demoService.DoSomeBusinessThings(result);
				string jsonString = JToken.Parse(JsonConvert.SerializeObject(result)).ToString(Formatting.Indented);
				Console.WriteLine(jsonString);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	
		public async Task<WorkflowResultView>CallFunctionThenActionK2(DemoFunctionParm demoFunctionParm)
        {
            try
            {
				// call function...
				object fromFunction = null; // logic

				// ...

				// call function done, call action K2
				IK2Service k2Service = new K2Service(db, transactionLogService);
				// add result from function to datafield
				var listDataField = k2Service.GetDataFieldFromParam<object>(fromFunction);
				demoFunctionParm.Workflow.DataFields.AddRange(listDataField);

				WorkflowResultView result = await k2Service.ActionWorkflow(demoFunctionParm.Workflow);
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public ReplaceBookmarkDemoView GetDemoBookmarkValues(Guid refGUID)
        {
            try
            {
				// select from database
				ReplaceBookmarkDemoView result = new ReplaceBookmarkDemoView
				{
					DemoDate = "26/04/2021",
					DemoId = "DEMO0001"
				};
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public async Task<List<FileInformation>> DemoReplaceBookmark(List<Guid> guids, Guid refGUID)
        {
            try
            {
				IDocumentTemplateTableRepo templateRepo = new DocumentTemplateTableRepo(db);
				IEnumerable<GenBookmarkDocumentFileParamViewMap> parm = (await templateRepo.GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(guids))
																				.Select(s => new GenBookmarkDocumentFileParamViewMap
																				{
																					Base64Data = s.Base64Data,
																					DocumentTemplateType = s.DocumentTemplateType,
																					RefGUID = refGUID
																				});
				IBookmarkDocumentTransService bookmarkTransService = new BookmarkDocumentTransService(db);
				return bookmarkTransService.GenBookmarkDocumentFile(parm);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

		public DemoItemView GetTimeTest(DemoItemView view)
        {
            try
            {
				IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
				MessengerJobTable messengerJobTable = messengerJobTableRepo.GetMessengerJobTableByIdNoTracking("61ED5CE6-7140-44EC-B573-E40A3F044FBD".StringToGuid());
				//view.DemoDate = messengerJobTable.JobTime.DateTimeToTimeString();
				view.DemoDate = "15/05/2020";
				return view;
			}
            catch (Exception e)
            {
                throw e;
            }
        }
		public DemoItemView SetTimeTest(DemoItemView view)
		{
			try
			{
				IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
				MessengerJobTable messengerJobTable = messengerJobTableRepo.Find("61ED5CE6-7140-44EC-B573-E40A3F044FBD".StringToGuid());
				DateTime date = view.DemoDate.TimeStringToDateTime();
				messengerJobTable.JobStartTime = date;
				UnitOfWork.Commit();
				view.DemoDate = messengerJobTable.JobStartTime.DateTimeNullToTimeString();
				return view;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		public void AddException(/*string input*/)
        {
			//SmartAppException ex = new SmartAppException("ERROR.ERROR");
			List<GenStagingTableErrorList> errorlist = new List<GenStagingTableErrorList>();
			try
			{
				//ex.AddData("Validating something and is not passing.");
				
				//MakeExpectedError(errorlist);
				//MakeUnexpectedError();

				//if(ex.MessageList.Count > 1)
                //{
				//	throw ex;
                //}
				if(errorlist.Count > 0)
                {
					SmartAppException expectedError = GetSmartAppExceptionFromGenStagingTableErrorList(errorlist);
					throw expectedError;
                }
				else
                {
					LogBatchSuccess("Success message", null, null);
                }
            }
            catch (Exception e)
            {
				//var x = ex;
				//var y = input;
				var z = GetSmartAppExceptionFromGenStagingTableErrorList(errorlist);

				throw SmartAppUtil.AddStackTrace(e, z);
            }
        }
		private void MakeExpectedError(List<GenStagingTableErrorList> errorlist)
        {
            try
            {
				//SmartAppException ex = new SmartAppException("ERROR.ERROR");
				//ex.AddData("Error occurred '{{0}}'.", "DemoService.MakeExpectedError");
				//throw ex;
				errorlist.Add(new GenStagingTableErrorList
				{
					TableName = typeof(Branch).Name,
					FieldName = BranchCondition.BranchGUID,
					Reference = "LABEL.STAGING_SETUP",
					RefValue = "{{0}}: {{1}}",
					RefValueArgs = new string[] { "LABEL.BRANCH_ID", "Test branch" },
					ProcessTransType = null,
					ErrorCode = "ERROR.90118",
					ErrorCodeArgs = new string[] { "LABEL.TAX_BRANCH_ID", "LABEL.BRANCH_ID", "Test branch" },
					RefGUID = null,
					ProductType = null
				});
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private void MakeUnexpectedError()
        {
            try
            {
				int x = 0, y = 10, z = 0;
				z = y / x;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private SmartAppException GetSmartAppExceptionFromGenStagingTableErrorList(List<GenStagingTableErrorList> errorList)
		{
			try
			{
				if (errorList != null && errorList.Count > 0)
				{
					SmartAppException expectedError = new SmartAppException("ERROR.ERROR");
					foreach (var error in errorList)
					{
						BatchLogReference batchRef = new BatchLogReference(
														new TranslateModel { Code = error.Reference },
														new TranslateModel { Code = error.RefValue, Parameters = error.RefValueArgs });
						batchRef.Key = error.ErrorKey.GuidNullToString();
						expectedError.AddData(batchRef, error.ErrorCode, error.ErrorCodeArgs);
					}
					return expectedError;
				}
				return null;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
