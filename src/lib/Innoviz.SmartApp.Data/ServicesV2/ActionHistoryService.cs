﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IActionHistoryService
	{

		ActionHistoryItemView GetActionHistoryById(string id);
		ActionHistoryItemView CreateActionHistory(ActionHistoryItemView actionHistoryView);
		ActionHistoryItemView UpdateActionHistory(ActionHistoryItemView actionHistoryView);
		bool DeleteActionHistory(string id);
		IEnumerable<ActionHistory> CreateErrorActionHistories(ActionHistory actionHistory, Exception exception);
		IEnumerable<ActionHistory> CreateErrorActionHistories(string refGUID, string workflowName,
															string activityName, string serialNumber,
															string companyGUID, string owner, string ownerBusinessUnitGUID,
															Exception exception);
		IEnumerable<ActionHistoryListView> GetActionHistoryByRefGUID(string refGUID);
		ActionHistoryItemView CreateActionHistoryStartWorkflow(WorkflowInstance workflow, string activityName = null, string actionName = null);
		ActionHistoryItemView CreateActionHistoryActionWorkflow(WorkflowInstance workflow);
		ActionHistoryItemView GetActionHistoryForK2(ActionHistoryK2Parm parm);
	}
	public class ActionHistoryService : SmartAppService, IActionHistoryService
	{
		public ActionHistoryService(SmartAppDbContext context) : base(context) { }
		public ActionHistoryService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ActionHistoryService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ActionHistoryService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ActionHistoryService() : base() { }

		public ActionHistoryItemView GetActionHistoryById(string id)
		{
			try
			{
				IActionHistoryRepo actionHistoryRepo = new ActionHistoryRepo(db);
				return actionHistoryRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ActionHistoryItemView CreateActionHistory(ActionHistoryItemView actionHistoryView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				actionHistoryView = accessLevelService.AssignOwnerBU(actionHistoryView);
				IActionHistoryRepo actionHistoryRepo = new ActionHistoryRepo(db);
				ActionHistory actionHistory = actionHistoryView.ToActionHistory();
				actionHistory = actionHistoryRepo.CreateActionHistory(actionHistory);
				base.LogTransactionCreate<ActionHistory>(actionHistory);
				UnitOfWork.Commit();
				return actionHistory.ToActionHistoryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ActionHistoryItemView UpdateActionHistory(ActionHistoryItemView actionHistoryView)
		{
			try
			{
				IActionHistoryRepo actionHistoryRepo = new ActionHistoryRepo(db);
				ActionHistory inputActionHistory = actionHistoryView.ToActionHistory();
				ActionHistory dbActionHistory = actionHistoryRepo.Find(inputActionHistory.ActionHistoryGUID);
				dbActionHistory = actionHistoryRepo.UpdateActionHistory(dbActionHistory, inputActionHistory);
				base.LogTransactionUpdate<ActionHistory>(GetOriginalValues<ActionHistory>(dbActionHistory), dbActionHistory);
				UnitOfWork.Commit();
				return dbActionHistory.ToActionHistoryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteActionHistory(string item)
		{
			try
			{
				IActionHistoryRepo actionHistoryRepo = new ActionHistoryRepo(db);
				Guid actionHistoryGUID = new Guid(item);
				ActionHistory actionHistory = actionHistoryRepo.Find(actionHistoryGUID);
				actionHistoryRepo.Remove(actionHistory);
				base.LogTransactionDelete<ActionHistory>(actionHistory);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<ActionHistory> CreateErrorActionHistories(string refGUID, string workflowName, 
																	string activityName, string serialNumber,
																	string companyGUID, string owner, string ownerBusinessUnitGUID,
																	Exception exception)
        {
            try
            {
				ActionHistory actionHistory = new ActionHistory
				{
					RefGUID = refGUID.StringToGuid(),
					WorkflowName = workflowName,
					ActivityName = activityName,
					SerialNo = serialNumber,
					CompanyGUID = companyGUID.StringToGuid(),
					Owner = owner,
					OwnerBusinessUnitGUID = ownerBusinessUnitGUID.StringToGuid()
				};
				return CreateErrorActionHistories(actionHistory, exception);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public IEnumerable<ActionHistory> CreateErrorActionHistories(ActionHistory actionHistory, Exception exception)
        {
            try
            {
				if(exception != null)
                {
					List<string> messageList = (List<string>)exception.Data[ExceptionDataKey.MessageList];
					if(messageList != null && messageList.Count() > 0)
                    {
						var result = messageList.Where(w => w != "Error").Select(s => new ActionHistory
						{
							ActionHistoryGUID = Guid.NewGuid(),
							ActionName = "Error",
							ActivityName = actionHistory.ActivityName,
							WorkflowName = actionHistory.WorkflowName,
							SerialNo = actionHistory.SerialNo,
							RefGUID = actionHistory.RefGUID,
							Comment = s,
							CompanyGUID = actionHistory.CompanyGUID,
							Owner = actionHistory.Owner,
							OwnerBusinessUnitGUID = actionHistory.OwnerBusinessUnitGUID
						}).ToList();

						using(var transaction = UnitOfWork.ContextTransaction())
                        {
							this.BulkInsert(result);
							UnitOfWork.Commit(transaction);
						}
						return result;
                    }
                }
				return null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public IEnumerable<ActionHistoryListView> GetActionHistoryByRefGUID(string refGUID)
        {
            try
            {
				IActionHistoryRepo actionHistoryRepo = new ActionHistoryRepo(db);
				var result = actionHistoryRepo.GetActionHistoryByRefGUID(refGUID.StringToGuid());
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public ActionHistoryItemView CreateActionHistoryStartWorkflow(WorkflowInstance workflow, string activityName = null, string actionName = null)
        {
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (workflow == null)
				{
					ex.AddData("Missing parameters for start workflow action history.");
					throw ex;
				}
				string startflowActivityName = DataTypeHandler.GetConfigurationValue("ActionHistoryK2:StartFlowActivityName");
				string startflowActionName = DataTypeHandler.GetConfigurationValue("ActionHistoryK2:StartFlowActionName");

				if (string.IsNullOrWhiteSpace(startflowActivityName))
				{
					ex.AddData("Missing Startflow Activity name.");
					throw ex;
				}
				if (string.IsNullOrWhiteSpace(startflowActionName))
				{
					ex.AddData("Missing Startflow Action name.");
					throw ex;
				}

				var dataFields = workflow.DataFields;
				if (dataFields != null && dataFields.Count() > 0)
                {
					string parmDocGUID = dataFields.Where(w => w.Name.ToUpper() == K2DataField.ParmDocGUID.ToUpper()).FirstOrDefault()?.Value;
					if(string.IsNullOrWhiteSpace(parmDocGUID))
                    {
						ex.AddData("Missing ParmDocGUID.");
						throw ex;
                    }
					ActionHistoryItemView actionHistory = new ActionHistoryItemView
					{
						ActionName = (actionName != null) ? actionName : startflowActionName,
						ActivityName = (activityName != null) ? activityName : startflowActivityName,
						WorkflowName = workflow.Name,
						Comment = null,
						SerialNo = null,
						RefGUID = parmDocGUID,
						Owner = GetUserName(),
						CompanyGUID = GetCurrentCompany(),
					};
					return CreateActionHistory(actionHistory);
                }
				else
                {
					ex.AddData("Missing DataFields.");
					throw ex;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public ActionHistoryItemView CreateActionHistoryActionWorkflow(WorkflowInstance workflow)
        {
            try
            {
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (workflow == null)
				{
					ex.AddData("Missing parameters for action workflow action history.");
					throw ex;
				}
				
				var dataFields = workflow.DataFields;
				if (dataFields != null && dataFields.Count() > 0)
				{
					string parmDocGUID = dataFields.Where(w => w.Name.ToUpper() == K2DataField.ParmDocGUID.ToUpper()).FirstOrDefault()?.Value;
					if (string.IsNullOrWhiteSpace(parmDocGUID))
					{
						ex.AddData("Missing ParmDocGUID.");
						throw ex;
					}
					ActionHistoryItemView actionHistory = new ActionHistoryItemView
					{
						ActionName = workflow.Action,
						ActivityName = workflow.ActInstDestDisplayName,
						WorkflowName = workflow.Name,
						Comment = workflow.Comments.FirstOrDefault()?.Message,
						SerialNo = workflow.SerialNumber,
						RefGUID = parmDocGUID,
						Owner = GetUserName(),
						CompanyGUID = GetCurrentCompany(),
					};
					return CreateActionHistory(actionHistory);
				}
				else
				{
					ex.AddData("Missing DataFields.");
					throw ex;
				}
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public ActionHistoryItemView GetActionHistoryForK2(ActionHistoryK2Parm parm)
        {
            try
            {
				IActionHistoryRepo actionHistoryRepo = new ActionHistoryRepo(db);
				var result = actionHistoryRepo.GetActionHistoryForK2(parm);
				return result;
            }
            catch (Exception e)
            {
				throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}
