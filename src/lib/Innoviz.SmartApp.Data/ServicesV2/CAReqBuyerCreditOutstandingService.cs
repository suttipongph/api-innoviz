using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICAReqBuyerCreditOutstandingService
	{

		CAReqBuyerCreditOutstandingItemView GetCAReqBuyerCreditOutstandingById(string id);
		CAReqBuyerCreditOutstandingItemView CreateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingView);
		CAReqBuyerCreditOutstandingItemView UpdateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingView);
		bool DeleteCAReqBuyerCreditOutstanding(string id);
	}
	public class CAReqBuyerCreditOutstandingService : SmartAppService, ICAReqBuyerCreditOutstandingService
	{
		public CAReqBuyerCreditOutstandingService(SmartAppDbContext context) : base(context) { }
		public CAReqBuyerCreditOutstandingService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CAReqBuyerCreditOutstandingService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CAReqBuyerCreditOutstandingService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CAReqBuyerCreditOutstandingService() : base() { }

		public CAReqBuyerCreditOutstandingItemView GetCAReqBuyerCreditOutstandingById(string id)
		{
			try
			{
				ICAReqBuyerCreditOutstandingRepo caReqBuyerCreditOutstandingRepo = new CAReqBuyerCreditOutstandingRepo(db);
				return caReqBuyerCreditOutstandingRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqBuyerCreditOutstandingItemView CreateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				caReqBuyerCreditOutstandingView = accessLevelService.AssignOwnerBU(caReqBuyerCreditOutstandingView);
				ICAReqBuyerCreditOutstandingRepo caReqBuyerCreditOutstandingRepo = new CAReqBuyerCreditOutstandingRepo(db);
				CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding = caReqBuyerCreditOutstandingView.ToCAReqBuyerCreditOutstanding();
				caReqBuyerCreditOutstanding = caReqBuyerCreditOutstandingRepo.CreateCAReqBuyerCreditOutstanding(caReqBuyerCreditOutstanding);
				base.LogTransactionCreate<CAReqBuyerCreditOutstanding>(caReqBuyerCreditOutstanding);
				UnitOfWork.Commit();
				return caReqBuyerCreditOutstanding.ToCAReqBuyerCreditOutstandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CAReqBuyerCreditOutstandingItemView UpdateCAReqBuyerCreditOutstanding(CAReqBuyerCreditOutstandingItemView caReqBuyerCreditOutstandingView)
		{
			try
			{
				ICAReqBuyerCreditOutstandingRepo caReqBuyerCreditOutstandingRepo = new CAReqBuyerCreditOutstandingRepo(db);
				CAReqBuyerCreditOutstanding inputCAReqBuyerCreditOutstanding = caReqBuyerCreditOutstandingView.ToCAReqBuyerCreditOutstanding();
				CAReqBuyerCreditOutstanding dbCAReqBuyerCreditOutstanding = caReqBuyerCreditOutstandingRepo.Find(inputCAReqBuyerCreditOutstanding.CAReqBuyerCreditOutstandingGUID);
				dbCAReqBuyerCreditOutstanding = caReqBuyerCreditOutstandingRepo.UpdateCAReqBuyerCreditOutstanding(dbCAReqBuyerCreditOutstanding, inputCAReqBuyerCreditOutstanding);
				base.LogTransactionUpdate<CAReqBuyerCreditOutstanding>(GetOriginalValues<CAReqBuyerCreditOutstanding>(dbCAReqBuyerCreditOutstanding), dbCAReqBuyerCreditOutstanding);
				UnitOfWork.Commit();
				return dbCAReqBuyerCreditOutstanding.ToCAReqBuyerCreditOutstandingItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCAReqBuyerCreditOutstanding(string item)
		{
			try
			{
				ICAReqBuyerCreditOutstandingRepo caReqBuyerCreditOutstandingRepo = new CAReqBuyerCreditOutstandingRepo(db);
				Guid caReqBuyerCreditOutstandingGUID = new Guid(item);
				CAReqBuyerCreditOutstanding caReqBuyerCreditOutstanding = caReqBuyerCreditOutstandingRepo.Find(caReqBuyerCreditOutstandingGUID);
				caReqBuyerCreditOutstandingRepo.Remove(caReqBuyerCreditOutstanding);
				base.LogTransactionDelete<CAReqBuyerCreditOutstanding>(caReqBuyerCreditOutstanding);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
