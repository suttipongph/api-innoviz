using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IWithholdingTaxGroupService
	{

		WithholdingTaxGroupItemView GetWithholdingTaxGroupById(string id);
		WithholdingTaxGroupItemView CreateWithholdingTaxGroup(WithholdingTaxGroupItemView withholdingTaxGroupView);
		WithholdingTaxGroupItemView UpdateWithholdingTaxGroup(WithholdingTaxGroupItemView withholdingTaxGroupView);
		bool DeleteWithholdingTaxGroup(string id);
	}
	public class WithholdingTaxGroupService : SmartAppService, IWithholdingTaxGroupService
	{
		public WithholdingTaxGroupService(SmartAppDbContext context) : base(context) { }
		public WithholdingTaxGroupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public WithholdingTaxGroupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public WithholdingTaxGroupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public WithholdingTaxGroupService() : base() { }

		public WithholdingTaxGroupItemView GetWithholdingTaxGroupById(string id)
		{
			try
			{
				IWithholdingTaxGroupRepo withholdingTaxGroupRepo = new WithholdingTaxGroupRepo(db);
				return withholdingTaxGroupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithholdingTaxGroupItemView CreateWithholdingTaxGroup(WithholdingTaxGroupItemView withholdingTaxGroupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				withholdingTaxGroupView = accessLevelService.AssignOwnerBU(withholdingTaxGroupView);
				IWithholdingTaxGroupRepo withholdingTaxGroupRepo = new WithholdingTaxGroupRepo(db);
				WithholdingTaxGroup withholdingTaxGroup = withholdingTaxGroupView.ToWithholdingTaxGroup();
				withholdingTaxGroup = withholdingTaxGroupRepo.CreateWithholdingTaxGroup(withholdingTaxGroup);
				base.LogTransactionCreate<WithholdingTaxGroup>(withholdingTaxGroup);
				UnitOfWork.Commit();
				return withholdingTaxGroup.ToWithholdingTaxGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithholdingTaxGroupItemView UpdateWithholdingTaxGroup(WithholdingTaxGroupItemView withholdingTaxGroupView)
		{
			try
			{
				IWithholdingTaxGroupRepo withholdingTaxGroupRepo = new WithholdingTaxGroupRepo(db);
				WithholdingTaxGroup inputWithholdingTaxGroup = withholdingTaxGroupView.ToWithholdingTaxGroup();
				WithholdingTaxGroup dbWithholdingTaxGroup = withholdingTaxGroupRepo.Find(inputWithholdingTaxGroup.WithholdingTaxGroupGUID);
				dbWithholdingTaxGroup = withholdingTaxGroupRepo.UpdateWithholdingTaxGroup(dbWithholdingTaxGroup, inputWithholdingTaxGroup);
				base.LogTransactionUpdate<WithholdingTaxGroup>(GetOriginalValues<WithholdingTaxGroup>(dbWithholdingTaxGroup), dbWithholdingTaxGroup);
				UnitOfWork.Commit();
				return dbWithholdingTaxGroup.ToWithholdingTaxGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteWithholdingTaxGroup(string item)
		{
			try
			{
				IWithholdingTaxGroupRepo withholdingTaxGroupRepo = new WithholdingTaxGroupRepo(db);
				Guid withholdingTaxGroupGUID = new Guid(item);
				WithholdingTaxGroup withholdingTaxGroup = withholdingTaxGroupRepo.Find(withholdingTaxGroupGUID);
				withholdingTaxGroupRepo.Remove(withholdingTaxGroup);
				base.LogTransactionDelete<WithholdingTaxGroup>(withholdingTaxGroup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
