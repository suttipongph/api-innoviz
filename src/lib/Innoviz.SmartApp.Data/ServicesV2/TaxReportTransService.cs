using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ITaxReportTransService
	{

		TaxReportTransItemView GetTaxReportTransById(string id);
		TaxReportTransItemView CreateTaxReportTrans(TaxReportTransItemView taxReportTransView);
		TaxReportTransItemView UpdateTaxReportTrans(TaxReportTransItemView taxReportTransView);
		bool DeleteTaxReportTrans(string id);
		TaxReportTransItemView GetTaxReportTransInitialData(string refId, string refGUID, RefType refType);
		void CreateTaxReportTransCollection(List<TaxReportTransItemView> taxReportTransItemViews);
		#region function
		CopyTaxReportTransView GetCopyTaxReportTransByRefType(RefTypeModel reftype);
		bool GetCopyTaxReportTransByRefTypeValidation(CopyTaxReportTransView param);
		CopyTaxReportTransView CopyTaxReportTrans(CopyTaxReportTransView paramView);
        ImportTaxReportResultView CreateImportTaxReport(ImportTaxReportView importTaxReportView);
        ImportTaxReportView GetImportTaxReportByRefType(string id, int refType);
		#endregion
	}
    public class TaxReportTransService : SmartAppService, ITaxReportTransService
	{
		public TaxReportTransService(SmartAppDbContext context) : base(context) { }
		public TaxReportTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public TaxReportTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TaxReportTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public TaxReportTransService() : base() { }

		public TaxReportTransItemView GetTaxReportTransById(string id)
		{
			try
			{
				ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
				return taxReportTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxReportTransItemView CreateTaxReportTrans(TaxReportTransItemView taxReportTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				taxReportTransView = accessLevelService.AssignOwnerBU(taxReportTransView);
				ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
				TaxReportTrans taxReportTrans = taxReportTransView.ToTaxReportTrans();
				taxReportTrans = taxReportTransRepo.CreateTaxReportTrans(taxReportTrans);
				base.LogTransactionCreate<TaxReportTrans>(taxReportTrans);
				UnitOfWork.Commit();
				return taxReportTrans.ToTaxReportTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxReportTransItemView UpdateTaxReportTrans(TaxReportTransItemView taxReportTransView)
		{
			try
			{
				ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
				TaxReportTrans inputTaxReportTrans = taxReportTransView.ToTaxReportTrans();
				TaxReportTrans dbTaxReportTrans = taxReportTransRepo.Find(inputTaxReportTrans.TaxReportTransGUID);
				dbTaxReportTrans = taxReportTransRepo.UpdateTaxReportTrans(dbTaxReportTrans, inputTaxReportTrans);
				base.LogTransactionUpdate<TaxReportTrans>(GetOriginalValues<TaxReportTrans>(dbTaxReportTrans), dbTaxReportTrans);
				UnitOfWork.Commit();
				return dbTaxReportTrans.ToTaxReportTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteTaxReportTrans(string item)
		{
			try
			{
				ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
				Guid taxReportTransGUID = new Guid(item);
				TaxReportTrans taxReportTrans = taxReportTransRepo.Find(taxReportTransGUID);
				taxReportTransRepo.Remove(taxReportTrans);
				base.LogTransactionDelete<TaxReportTrans>(taxReportTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxReportTransItemView GetTaxReportTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new TaxReportTransItemView
				{
					TaxReportTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public ImportTaxReportView GetImportTaxReportByRefType(string id, int refType)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                ImportTaxReportView model = new ImportTaxReportView();
                switch (refType)
                {
                    case (int)RefType.Customer:
                        CustomerTableItemView item = customerTableRepo.GetByIdvw(id.StringToGuid());
                        model.RefId = item.CustomerId;
                        model.Name = item.Name;
                        model.RefType = (int)RefType.Customer;
                        model.RefGUID = item.CustomerTableGUID;
                        break;
                }
                return model;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ImportTaxReportResultView CreateImportTaxReport(ImportTaxReportView importTaxReportView)
        {
            try
            {
                FileUpload fileUpload = new FileUpload
                    {FileInfos = new List<FileInformation>() {importTaxReportView.FileInfo}};
                FileHelper fileHelper = new FileHelper();
                IEnumerable<ImportTaxReportView> importTaxReportViews =
                    fileHelper.MapInputExcelFileToSingleEntityType<ImportTaxReportView>(fileUpload,
                        ImportTaxReportView.GetMapper());

                if (importTaxReportViews.Any())
                {
                    GetImportTaxReportValidation(importTaxReportViews);
                    List<TaxReportTrans> taxReportTrans = new List<TaxReportTrans>();
                    foreach (var item in importTaxReportViews)
                    {
						int month = 0;
						foreach (MonthNames e in System.Enum.GetValues(typeof(MonthNames)))
						{
							if (e.GetAttrCode().ToUpper() == item.Month.ToUpper())
							{
								month = e.GetAttrValue();
								break;
							}
						}
						taxReportTrans.Add(new TaxReportTrans()
                        {
                            CompanyGUID = db.GetCompanyFilter().StringToGuid(),
                            RefGUID = importTaxReportView.RefGUID.StringToGuid(),
                            RefType = Convert.ToInt32(importTaxReportView.RefType),
                            Year = Convert.ToInt32(item.Year),
							Month = month,
                            Description = item.Description,
                            Amount = Convert.ToDecimal(item.Amount)
                        });
                    }

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(taxReportTrans.ToList(), true);
                        UnitOfWork.Commit(transaction);
                    }
                }

                NotificationResponse success = new NotificationResponse();
                ImportTaxReportResultView importTaxReportResultView = new ImportTaxReportResultView();
                success.AddData("SUCCESS.90017", new string[] {"LABEL.TAX_REPORT_TRANSACTION"});
                importTaxReportResultView.Notification = success;

                return importTaxReportResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

		private bool GetImportTaxReportValidation(IEnumerable<ImportTaxReportView> taxReportItemViews)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				List<ImportTaxReportView> taxReportItemViewList = taxReportItemViews.ToList();
				foreach (var item in taxReportItemViewList)
				{
					int index = taxReportItemViewList.IndexOf(item) + 1;
					if (string.IsNullOrEmpty(item.Year) || string.IsNullOrEmpty(item.Month) || string.IsNullOrEmpty(item.Amount) || string.IsNullOrEmpty(item.Description))
					{
						ex.AddData("ERROR.90163", index);
					}
					if (item.Year != null && Convert.ToInt32(item.Year) > 9999)
					{
						ex.AddData("ERROR.90144", index, new string[] { "LABEL.YEAR" });
					}
					if (item.Year != null && Convert.ToInt32(item.Year) <= 0)
					{
						ex.AddData("ERROR.GREATER_THAN_ZERO", index, new string[] { "LABEL.YEAR" });
					}
					bool isMonthValid = false;
					foreach (MonthNames e in System.Enum.GetValues(typeof(MonthNames)))
					{
						if( e.GetAttrCode().ToUpper() == item.Month.ToUpper())
                        {
							isMonthValid = true;
							break;
						}
                    }
                    if (!isMonthValid)
                    {
						ex.AddData("ERROR.STRING_INCORRECT", index);
					}
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateTaxReportTransCollection(List<TaxReportTransItemView> taxReportTransItemViews)
		{
			try
			{
				if (taxReportTransItemViews.Any())
				{
					ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
					List<TaxReportTrans> taxReportTrans = taxReportTransItemViews.ToTaxReportTrans().ToList();
					taxReportTransRepo.ValidateAdd(taxReportTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(taxReportTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#region function
		public CopyTaxReportTransView GetCopyTaxReportTransByRefType(RefTypeModel reftype)
        {
            try
            {
				CopyTaxReportTransView result = new CopyTaxReportTransView();
				if (reftype.RefType == (int)RefType.CreditAppRequestTable)
                {
					ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
					CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(reftype.RefGUID);
					result.RefType = (int)RefType.CreditAppRequestTable;
					result.RefGUID = reftype.RefGUID.ToString();
					result.CustomerTableGUID = creditAppRequestTable.CustomerTableGUID.ToString();
				}
				return result;
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetCopyTaxReportTransByRefTypeValidation(CopyTaxReportTransView param)
		{
			try
			{
				ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(db.GetCompanyFilter().StringToGuid());
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				var crediappRepTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(param.RefGUID.StringToGuid());
				var hasList = taxReportTransRepo.GetCopyTaxReportTransByRefTypes(crediappRepTable.CreditAppRequestTableGUID, (int)RefType.CreditAppRequestTable);

				if (param.Month < 1)
				{
					ex.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.COPY_TAX_REPORT_TRANS" });
				}
				if (param.Month > companyParameter.MaxCopyTaxReportNumOfMonth)
				{
					ex.AddData("ERROR.90031", new string[] { param.Month.ToString(), companyParameter.MaxCopyTaxReportNumOfMonth.ToString() });
				}
				IEnumerable<TaxReportTrans> cloneTaxReportTranses = taxReportTransRepo.GetCopyTaxReportTransByRefTypes(param.CustomerTableGUID.StringToGuid(), (int)RefType.Customer);

				if (cloneTaxReportTranses.Count() > 0)
				{
					TaxReportTrans customerTaxReportTrans = cloneTaxReportTranses.OrderByDescending(o => o.Year).ThenByDescending(o => o.Month).FirstOrDefault();

					int startYear = DateTime.Now.Year;
					int startMonth = DateTime.Now.Month;
					DateTime date = new DateTime(customerTaxReportTrans.Year, customerTaxReportTrans.Month, 1);
					date = date.AddMonths((param.Month * -1));
					startYear = date.Year;
					startMonth = date.Month;

					cloneTaxReportTranses = cloneTaxReportTranses.Where(w => w.Year >= startYear || (w.Year == startYear && w.Month > startMonth));

					if(cloneTaxReportTranses.Count() == 0)
                    {
						ex.AddData("ERROR.90003");
					}
				}
				else
                {
					ex.AddData("ERROR.90003");
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				if (hasList.Count() > 0)
				{
					ex.AddData("CONFIRM.90002", new string[] { "LABEL.COPY_TAX_REPORT_TRANS" });
					return false;
				}
				IEnumerable<TaxReportTrans> taxReportTrans = taxReportTransRepo.GetCopyTaxReportTransByRefTypes(param.RefGUID.StringToGuid(), param.RefType);
				return taxReportTrans.Any() ? false : true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CopyTaxReportTransView CopyTaxReportTrans(CopyTaxReportTransView paramView)
		{
			try
			{
				ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
				CopyTaxReportTransView result = new CopyTaxReportTransView();
				NotificationResponse success = new NotificationResponse();
				int startYear = DateTime.Now.Year;
				int startMonth = DateTime.Now.Month;
				IEnumerable<TaxReportTrans> cloneTaxReportTranses = taxReportTransRepo.GetCopyTaxReportTransByRefTypes(paramView.CustomerTableGUID.StringToGuid(), (int)RefType.Customer);
				IEnumerable<TaxReportTrans> removeTaxReportTranses = taxReportTransRepo.GetCopyTaxReportTransByRefTypes(paramView.RefGUID.StringToGuid(), paramView.RefType);

				TaxReportTrans customerTaxReportTrans = cloneTaxReportTranses.OrderByDescending(o => o.Year).ThenByDescending(o => o.Month).FirstOrDefault();
				if (cloneTaxReportTranses.Count() > 0)
				{
					DateTime date = new DateTime(customerTaxReportTrans.Year, customerTaxReportTrans.Month, 1);
					date = date.AddMonths((paramView.Month * -1));
					startYear = date.Year;
					startMonth = date.Month;

					List<TaxReportTrans> createList = new List<TaxReportTrans>();
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						try
						{
							BulkDelete(removeTaxReportTranses.ToList());
							cloneTaxReportTranses = cloneTaxReportTranses.Where(w => w.Year > startYear || (w.Year == startYear && w.Month > startMonth));
							cloneTaxReportTranses.ToList().ForEach(item =>
							{
								TaxReportTrans createItem = new TaxReportTrans()
								{
									TaxReportTransGUID = Guid.NewGuid(),
									Year = item.Year,
									Month = item.Month,
									Description = item.Description,
									Amount = item.Amount,
									RefType = paramView.RefType,
									RefGUID = paramView.RefGUID.StringToGuid(),
									CompanyGUID = db.GetCompanyFilter().StringToGuid()
								};
								createList.Add(createItem);
							});
							BulkInsert(createList);
							UnitOfWork.Commit(transaction);
						}
						catch (Exception e)
						{
							transaction.Rollback();
							throw SmartAppUtil.AddStackTrace(e);
						}
					}
				}
				success.AddData("SUCCESS.90002", new string[] { paramView.ResultLabel });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
