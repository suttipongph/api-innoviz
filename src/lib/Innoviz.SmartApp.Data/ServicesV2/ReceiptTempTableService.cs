using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IReceiptTempTableService
	{

		ReceiptTempPaymDetailItemView GetReceiptTempPaymDetailById(string id);
		ReceiptTempPaymDetailItemView CreateReceiptTempPaymDetail(ReceiptTempPaymDetailItemView receiptTempPaymDetailView);
		ReceiptTempPaymDetailItemView UpdateReceiptTempPaymDetail(ReceiptTempPaymDetailItemView receiptTempPaymDetailView);
		ReceiptTempPaymDetail UpdateReceiptTempPaymDetail(ReceiptTempPaymDetail receiptTempPaymDetail);
		bool DeleteReceiptTempPaymDetail(string id);
		ReceiptTempTableItemView GetReceiptTempTableById(string id);
		ReceiptTempTableItemView CreateReceiptTempTable(ReceiptTempTableItemView receiptTempTableView);
		ReceiptTempTable CreateReceiptTempTable(ReceiptTempTable receiptTempTable);
		ReceiptTempTableItemView UpdateReceiptTempTable(ReceiptTempTableItemView receiptTempTableView);
		ReceiptTempTable UpdateReceiptTempTable(ReceiptTempTable receiptTempTable);
		bool DeleteReceiptTempTable(string id);
		bool IsManualByReceiptTempTable(string companyId);
		bool IsInvoiceSettlementOrPaymentDetailCreated(string receiptTempTableGUID);
		ReceiptTempTableItemView GetReceiptTempTableInitialData(string companyGUID);
		ReceiptTempPaymDetailItemView GetReceiptTempPaymDetailInitialData(string receiptTempTableGUID);
		ReceiptTempTableItemView GetExChangeRate(ReceiptTempTableItemView input);
		AccessModeView GetReceiptTempTableAccessMode(string receiptTempTableGUID);

		CancelReceiptTempView GetCancelReceiptTempById(string receiptTempTableGUID);
		CancelReceiptTempResultView CancelReceiptTemp(CancelReceiptTempView receiptTempTable);
		ServiceFeeTransItemView GetServiceFeeTransInitialDataByReceiptTempTable(string refGUID);
		void UpdateReceiptTempTableSettleFeeAmount(ServiceFeeTransItemView serviceFeeTransView);
		void UpdateReceiptTempTableSettleFeeAmountOnRemove(ServiceFeeTrans serviceFeeTrans);
		PrintReceiptTemp GetPrintReceiptTempTableById(string receiptTempTableGUID);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemReceiptTempDocumentStatus(SearchParameter search);

		#region function
		#region Post Settlement
		ViewPostSettlementResult PostSettlement(ViewPostSettlementParameter parameter);
		#endregion
		#region Post ReceiptTemp
		PostReceiptTempResultView PostReceiptTemp(PostReceiptTempParamView parm);
		PostReceiptTempResultViewMap InitPostReceiptTemp(Guid receiptTempTableGUID);
		PostReceiptTempResultViewMap InitPostReceiptTemp(ReceiptTempTable receiptTempTable);
		PostReceiptTempParamView GetPostReceiptTempById(string receiptTempTableGUID);
		#endregion
		#endregion

	}
	public class ReceiptTempTableService : SmartAppService, IReceiptTempTableService
	{
		public ReceiptTempTableService(SmartAppDbContext context) : base(context) { }
		public ReceiptTempTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ReceiptTempTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ReceiptTempTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ReceiptTempTableService() : base() { }

		public ReceiptTempPaymDetailItemView GetReceiptTempPaymDetailById(string id)
		{
			try
			{
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				return receiptTempPaymDetailRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempPaymDetailItemView CreateReceiptTempPaymDetail(ReceiptTempPaymDetailItemView receiptTempPaymDetailView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				receiptTempPaymDetailView = accessLevelService.AssignOwnerBU(receiptTempPaymDetailView);
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				ReceiptTempPaymDetail receiptTempPaymDetail = receiptTempPaymDetailView.ToReceiptTempPaymDetail();
				receiptTempPaymDetail = receiptTempPaymDetailRepo.CreateReceiptTempPaymDetail(receiptTempPaymDetail);
				UpdateReceiptTempTableReceiptAmount(receiptTempPaymDetailView);
				base.LogTransactionCreate<ReceiptTempPaymDetail>(receiptTempPaymDetail);
				UnitOfWork.Commit();
				return receiptTempPaymDetail.ToReceiptTempPaymDetailItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempPaymDetailItemView UpdateReceiptTempPaymDetail(ReceiptTempPaymDetailItemView receiptTempPaymDetailView)
		{
			try
			{
				ReceiptTempPaymDetail inputReceiptTempPaymDetail = receiptTempPaymDetailView.ToReceiptTempPaymDetail();
				ReceiptTempPaymDetail dbReceiptTempPaymDetail = UpdateReceiptTempPaymDetail(inputReceiptTempPaymDetail);
				UpdateReceiptTempTableReceiptAmount(receiptTempPaymDetailView);
				UnitOfWork.Commit();
				return dbReceiptTempPaymDetail.ToReceiptTempPaymDetailItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempPaymDetail UpdateReceiptTempPaymDetail(ReceiptTempPaymDetail receiptTempPaymDetail)
		{
			try
			{
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				ReceiptTempPaymDetail inputReceiptTempPaymDetail = receiptTempPaymDetail;
				ReceiptTempPaymDetail dbReceiptTempPaymDetail = receiptTempPaymDetailRepo.Find(inputReceiptTempPaymDetail.ReceiptTempPaymDetailGUID);
				dbReceiptTempPaymDetail = receiptTempPaymDetailRepo.UpdateReceiptTempPaymDetail(dbReceiptTempPaymDetail, inputReceiptTempPaymDetail);
				base.LogTransactionUpdate<ReceiptTempPaymDetail>(GetOriginalValues<ReceiptTempPaymDetail>(dbReceiptTempPaymDetail), dbReceiptTempPaymDetail);
				return receiptTempPaymDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTempTableReceiptAmount(ReceiptTempPaymDetailItemView receiptTempPaymDetailView)
		{
			try
			{
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTable dbReceiptTempTable = receiptTempTableRepo.Find(receiptTempPaymDetailView.ReceiptTempTableGUID.StringToGuid());
				IEnumerable<ReceiptTempPaymDetail> receiptTempPaymDetails = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByReceiptTempNoTracking(dbReceiptTempTable.ReceiptTempTableGUID).Where(t => t.ReceiptTempPaymDetailGUID != receiptTempPaymDetailView.ReceiptTempPaymDetailGUID.StringToGuid());
			    dbReceiptTempTable.ReceiptAmount = receiptTempPaymDetails.Sum(s => s.ReceiptAmount) + receiptTempPaymDetailView.ReceiptAmount;
				receiptTempTableRepo.UpdateReceiptTempTableVoid(dbReceiptTempTable, dbReceiptTempTable);
				base.LogTransactionUpdate<ReceiptTempTable>(GetOriginalValues<ReceiptTempTable>(dbReceiptTempTable), dbReceiptTempTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTempTableReceiptAmountOnRemove(ReceiptTempPaymDetail receiptTempPaymDetail)
		{
			try
			{
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTable dbReceiptTempTable = receiptTempTableRepo.Find(receiptTempPaymDetail.ReceiptTempTableGUID);
				dbReceiptTempTable.ReceiptAmount -= receiptTempPaymDetail.ReceiptAmount;
				base.LogTransactionUpdate<ReceiptTempTable>(GetOriginalValues<ReceiptTempTable>(dbReceiptTempTable), dbReceiptTempTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteReceiptTempPaymDetail(string item)
		{
			try
			{
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				Guid receiptTempPaymDetailGUID = new Guid(item);
				ReceiptTempPaymDetail receiptTempPaymDetail = receiptTempPaymDetailRepo.Find(receiptTempPaymDetailGUID);
				receiptTempPaymDetailRepo.Remove(receiptTempPaymDetail);
				UpdateReceiptTempTableReceiptAmountOnRemove(receiptTempPaymDetail);
				base.LogTransactionDelete<ReceiptTempPaymDetail>(receiptTempPaymDetail);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ReceiptTempTableItemView GetReceiptTempTableById(string id)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				return receiptTempTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempTableItemView CreateReceiptTempTable(ReceiptTempTableItemView receiptTempTableView)
		{
			try
			{
				ReceiptTempTable receiptTempTable = receiptTempTableView.ToReceiptTempTable();
				GenReceiptTempTableNumberSeqCode(receiptTempTable);
				receiptTempTable = CreateReceiptTempTable(receiptTempTable);
				UnitOfWork.Commit();
				return receiptTempTable.ToReceiptTempTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempTable CreateReceiptTempTable(ReceiptTempTable receiptTempTable)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				receiptTempTable = accessLevelService.AssignOwnerBU(receiptTempTable);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				receiptTempTable = receiptTempTableRepo.CreateReceiptTempTable(receiptTempTable);
				base.LogTransactionCreate<ReceiptTempTable>(receiptTempTable);
				return receiptTempTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		public ReceiptTempTableItemView UpdateReceiptTempTable(ReceiptTempTableItemView receiptTempTableView)
		{
			try
			{
				ReceiptTempTable inputReceiptTempTable = receiptTempTableView.ToReceiptTempTable();
				UpdateReceiptTempTableReceiptAmount(inputReceiptTempTable);
				ReceiptTempTable dbReceiptTempTable = UpdateReceiptTempTable(inputReceiptTempTable);
				UnitOfWork.Commit();
				return dbReceiptTempTable.ToReceiptTempTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempTable UpdateReceiptTempTable(ReceiptTempTable receiptTempTable)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTable inputReceiptTempTable = receiptTempTable;
				ReceiptTempTable dbReceiptTempTable = receiptTempTableRepo.Find(inputReceiptTempTable.ReceiptTempTableGUID);
				dbReceiptTempTable = receiptTempTableRepo.UpdateReceiptTempTable(dbReceiptTempTable, inputReceiptTempTable);
				base.LogTransactionUpdate<ReceiptTempTable>(GetOriginalValues<ReceiptTempTable>(dbReceiptTempTable), dbReceiptTempTable);
				return receiptTempTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteReceiptTempTable(string item)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				Guid receiptTempTableGUID = new Guid(item);
				ReceiptTempTable receiptTempTable = receiptTempTableRepo.Find(receiptTempTableGUID);
				receiptTempTableRepo.Remove(receiptTempTable);
				base.LogTransactionDelete<ReceiptTempTable>(receiptTempTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTempTableReceiptAmount(ReceiptTempTable receiptTempTable)
		{
			try
			{
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				IEnumerable<ReceiptTempPaymDetail> receiptTempPaymDetails = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByReceiptTempNoTracking(receiptTempTable.ReceiptTempTableGUID);
				if (receiptTempPaymDetails.Count() > 0)
				{
					receiptTempTable.ReceiptAmount = receiptTempPaymDetails.Sum(s => s.ReceiptAmount);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region NumberSeq
		public bool IsManualByReceiptTempTable(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.ReceiptTemp);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenReceiptTempTableNumberSeqCode(ReceiptTempTable receiptTempTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = IsManualByReceiptTempTable(receiptTempTable.CompanyGUID.GuidNullToString());
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(receiptTempTable.CompanyGUID, ReferenceId.ReceiptTemp);
					receiptTempTable.ReceiptTempId = numberSequenceService.GetNumber(receiptTempTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion

		public bool IsInvoiceSettlementOrPaymentDetailCreated(string receiptTempTableGUID)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(receiptTempTableGUID.StringToGuid());

				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				IEnumerable<ReceiptTempPaymDetail> receiptTempPaymDetailList = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByReceiptTempNoTracking(receiptTempTable.ReceiptTempTableGUID);

				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
				IEnumerable<InvoiceSettlementDetail> invoiceSettlementDetailList = invoiceSettlementDetailRepo.GetByReference(RefType.ReceiptTemp, receiptTempTable.ReceiptTempTableGUID);

				if (receiptTempPaymDetailList.Count() > 0 || invoiceSettlementDetailList.Count() > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTempTableItemView GetReceiptTempTableInitialData(string companyGUID)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTrackingByAccessLevel(companyGUID.StringToGuid());

				IExchangeRateService exchangeRateService = new ExchangeRateService(db);

				decimal exchangeRate = 0;

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(TemporaryReceiptStatus.Draft).ToString());

				if (companyParameter.HomeCurrencyGUID != null)
				{
					exchangeRate = exchangeRateService.GetExchangeRate(companyParameter.HomeCurrencyGUID.Value, DateTime.Now);
				}

				return new ReceiptTempTableItemView
				{
					ReceiptTempTableGUID = new Guid().GuidNullToString(),
					ReceiptDate = DateTime.Now.DateToString(),
					TransDate = DateTime.Now.DateToString(),
					CurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString(),
					ExchangeRate = exchangeRate,
					DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
					DocumentStatus_StatusId = documentStatus.StatusId,
					DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description)
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ReceiptTempPaymDetailItemView GetReceiptTempPaymDetailInitialData(string receiptTempTableGUID)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(receiptTempTableGUID.StringToGuid());
				return new ReceiptTempPaymDetailItemView
				{
					ReceiptTempPaymDetailGUID = Guid.Empty.ToString(),
					ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID.GuidNullToString(),
					ReceiptTempTable_BuyerTableGUID = receiptTempTable.BuyerTableGUID.GuidNullToString(),
					ReceiptTempTable_CustomerTableGUID = receiptTempTable.CustomerTableGUID.GuidNullToString(),
					ReceiptTempTable_ReceivedFrom = receiptTempTable.ReceivedFrom,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ReceiptTempTableItemView GetExChangeRate(ReceiptTempTableItemView input)
		{
			try
			{
				IExchangeRateService exchangeRateService = new ExchangeRateService(db);
				decimal exchangeRate = 0;
				if (input.CurrencyGUID != null)
				{
					exchangeRate = exchangeRateService.GetExchangeRate(input.CurrencyGUID.StringToGuid(), input.TransDate.StringToDate());
				}

				return new ReceiptTempTableItemView
				{
					ReceiptTempTableGUID = new Guid().GuidNullToString(),
					ExchangeRate = exchangeRate
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AccessModeView GetReceiptTempTableAccessMode(string receiptTempTableGUID)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(receiptTempTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(receiptTempTable.DocumentStatusGUID);

				bool condition = (documentStatus.StatusId == Convert.ToInt32(TemporaryReceiptStatus.Draft).ToString());

				return new AccessModeView
				{
					CanCreate = condition,
					CanDelete = condition,
					CanView = condition,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeTransItemView GetServiceFeeTransInitialDataByReceiptTempTable(string refGUID)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				string refId = receiptTempTableRepo.GetReceiptTempTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).ReceiptTempId;
				return serviceFeeTransService.GetServiceFeeTransInitialDataByReceiptTempTable(refId, refGUID, Models.Enum.RefType.ReceiptTemp);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTempTableSettleFeeAmount(ServiceFeeTransItemView serviceFeeTransView)
		{
			try
			{
				if (serviceFeeTransView.RefType == (int)RefType.ReceiptTemp)
				{
					IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
					IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
					ReceiptTempTable dbReceiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(serviceFeeTransView.RefGUID.StringToGuid());
					IEnumerable<ServiceFeeTrans> serviceFeeTranses = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking((RefType)serviceFeeTransView.RefType, dbReceiptTempTable.ReceiptTempTableGUID)
																						.Where(w => w.ServiceFeeTransGUID != serviceFeeTransView.ServiceFeeTransGUID.StringToGuid());
					if (serviceFeeTranses.Count() > 0)
					{
						dbReceiptTempTable.SettleFeeAmount = serviceFeeTranses.Sum(s => s.SettleAmount) + serviceFeeTransView.SettleAmount;
					}
					else
					{
						dbReceiptTempTable.SettleFeeAmount = serviceFeeTransView.SettleAmount;
					}
					UpdateReceiptTempTable(dbReceiptTempTable);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateReceiptTempTableSettleFeeAmountOnRemove(ServiceFeeTrans serviceFeeTrans)
		{
			try
			{
				if (serviceFeeTrans.RefType == (int)RefType.ReceiptTemp)
				{
					IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
					IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
					ReceiptTempTable dbReceiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(serviceFeeTrans.RefGUID);
					dbReceiptTempTable.SettleFeeAmount -= serviceFeeTrans.SettleAmount;
					UpdateReceiptTempTable(dbReceiptTempTable);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemReceiptTempDocumentStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.TemporaryReceipt);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region function
		#region cancel receiptTemp
		public CancelReceiptTempView GetCancelReceiptTempById(string receiptTempTableGUID)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTableItemView receiptTempTable = receiptTempTableRepo.GetByIdvwByCancelReceiptTemp(receiptTempTableGUID.StringToGuid());

				CancelReceiptTempView cancelReceiptTempView = new CancelReceiptTempView
				{
					ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID.GuidNullToString(),
					DocumentReasonGUID = receiptTempTable.DocumentReasonGUID,
					ReceiptDate = receiptTempTable.ReceiptDate,
					TransDate = receiptTempTable.ReceiptDate,
					ReceiptAmount = receiptTempTable.ReceiptAmount,
					SettleAmount = receiptTempTable.SettleAmount,
					SettleFeeAmount = receiptTempTable.SettleAmount,
					SuspenseAmount = receiptTempTable.SuspenseAmount,
					ProductType = receiptTempTable.ProductType,
					ReceivedFrom = receiptTempTable.ReceivedFrom,
					BuyerTable_Values = receiptTempTable.BuyerTable_Values,
					CreditAppTable_Values = receiptTempTable.CreditAppTable_Values,
					CustomerTable_Values = receiptTempTable.CustomerTable_Values,
					ReceiptTempTable_Values = SmartAppUtil.GetDropDownLabel(receiptTempTable.ReceiptTempId, receiptTempTable.ReceiptDate),
					SuspenseInvoiceType_Values = receiptTempTable.SuspenseInvoiceType_Values
				};
				return cancelReceiptTempView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetCancelReceiptTempValidation(string receiptTempTableGUID)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(receiptTempTableGUID.StringToGuid());

				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus receiptTempStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)TemporaryReceiptStatus.Draft).ToString());

				if (receiptTempTable.DocumentStatusGUID != receiptTempStatusDraft.DocumentStatusGUID)
				{
					ex.AddData("ERROR.90012", new string[] { "LABEL.COLLECTION" });
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CancelReceiptTempResultView CancelReceiptTemp(CancelReceiptTempView receiptTempTable)
		{
			try
			{
				// validate
				GetCancelReceiptTempValidation(receiptTempTable.ReceiptTempTableGUID);
				CancelReceiptTempResultView result = new CancelReceiptTempResultView();
				NotificationResponse success = new NotificationResponse();
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTableItemView receiptTempTableItemView = receiptTempTableRepo.GetByIdvw(receiptTempTable.ReceiptTempTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus receiptTempStatusCancel = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)TemporaryReceiptStatus.Cancelled).ToString());

				receiptTempTableItemView.DocumentStatusGUID = receiptTempStatusCancel.DocumentStatusGUID.GuidNullToString();
				receiptTempTableItemView.DocumentReasonGUID = receiptTempTable.DocumentReasonGUID.GuidNullToString();

				UpdateReceiptTempTable(receiptTempTableItemView);

				success.AddData("SUCCESS.90008", new string[] { "LABEL.COLLECTION", SmartAppUtil.GetDropDownLabel(receiptTempTableItemView.ReceiptTempId, receiptTempTableItemView.ReceiptDate.ToString()) });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion

		#region Shared Method Post Settlement
		public ViewPostSettlementResult PostSettlement(ViewPostSettlementParameter parameter)
		{
			try
			{
				ViewPostSettlementResult result = new ViewPostSettlementResult();

				if (ValidatePostSettlement(parameter))
				{
					UpdateCustTrans_Settle(parameter, result);
					GenCreditAppTrans_Settle(parameter, result);
					GenReceiptTable_Line(parameter, result);
					GenTaxInvoice_Payment(parameter, result);
					GenPaymentHistory(parameter, result);
					GenProcessTrans_Payment(parameter, result);
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private bool ValidatePostSettlement(ViewPostSettlementParameter viewPostSettlementParameter)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				#region 1
				if (viewPostSettlementParameter.RefType == RefType.ReceiptTemp && viewPostSettlementParameter.ReceiptTempTable == null)
					ex.AddData("ERROR.90125", new string[] { "LABEL.POST_SETTLEMENT", "LABEL.RECEIPT_TEMP_TABLE", "LABEL.CUSTOMER_REFUND_TABLE" });
				#endregion
				#region 2
				if (viewPostSettlementParameter.RefType == RefType.CustomerRefund && viewPostSettlementParameter.CustomerRefundTable == null)
					ex.AddData("ERROR.90125", new string[] { "LABEL.POST_SETTLEMENT", "LABEL.RECEIPT_TEMP_TABLE", "LABEL.CUSTOMER_REFUND_TABLE" });
				#endregion
				#region 3
				var validate3 = viewPostSettlementParameter.InvoiceSettlementDetails.Where(w => !viewPostSettlementParameter.InvoiceTables.Select(s => s.InvoiceTableGUID).ToList().Contains(w.InvoiceTableGUID.Value)).ToList().FirstOrDefault();
				if (validate3 != null)
					ex.AddData("ERROR.90142", new string[] { "LABEL.DOCUMENT_ID", validate3.DocumentId, "LABEL.INVOICE" });
				#endregion
				#region 4
				var validate4 = viewPostSettlementParameter.InvoiceSettlementDetails.Where(w => !viewPostSettlementParameter.InvoiceLines.Select(s => s.InvoiceTableGUID).ToList().Contains(w.InvoiceTableGUID.Value)).ToList().FirstOrDefault();
				if (validate4 != null)
					ex.AddData("ERROR.90142", new string[] { "LABEL.DOCUMENT_ID", validate4.DocumentId, "LABEL.INVOICE_LINE" });
				#endregion
				#region 5
				var validate5 = viewPostSettlementParameter.InvoiceSettlementDetails.Where(w => !viewPostSettlementParameter.CustTrans.Select(s => s.InvoiceTableGUID).ToList().Contains(w.InvoiceTableGUID.Value)).ToList().FirstOrDefault();
				if (validate5 != null)
					ex.AddData("ERROR.90142", new string[] { "LABEL.DOCUMENT_ID", validate5.DocumentId, "LABEL.CUSTOMER_TRANSACTION" });
				#endregion
				#region 6
				var validate6 = (from isd in viewPostSettlementParameter.InvoiceSettlementDetails
								 join it in viewPostSettlementParameter.InvoiceTables
								 on isd.InvoiceTableGUID equals it.InvoiceTableGUID
								 join ct in viewPostSettlementParameter.CustTrans
								 on it.InvoiceTableGUID equals ct.InvoiceTableGUID
								 where isd.BalanceAmount != (ct.TransAmount - ct.SettleAmount)
								 select it).ToList().FirstOrDefault();
				if (validate6 != null)
					ex.AddData("ERROR.90046", new string[] { "LABEL.INVOICE_SETTLEMENT_DETAIL", validate6.InvoiceId, "LABEL.POST_SETTLEMENT" });
				#endregion
				#region 7
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				List<TaxTable> taxTables = taxTableRepo.GetTaxTableByIdNoTracking(viewPostSettlementParameter.InvoiceLines.Where(w => w.TaxTableGUID.HasValue).Select(s => s.TaxTableGUID.Value).ToList());
				var validate7 = (from isd in viewPostSettlementParameter.InvoiceSettlementDetails
								 join it in viewPostSettlementParameter.InvoiceTables
								 on isd.InvoiceTableGUID equals it.InvoiceTableGUID
								 join il in viewPostSettlementParameter.InvoiceLines
								 on isd.InvoiceTableGUID equals il.InvoiceTableGUID
								 join tt in taxTables
								 on il.TaxTableGUID equals tt.TaxTableGUID into lj1
								 from tt in lj1.DefaultIfEmpty()
								 where isd.SettleTaxAmount != 0 && 
								 isd.SuspenseInvoiceType == (int)SuspenseInvoiceType.None && 
								 ((!il.TaxTableGUID.HasValue) ||
								 (tt != null && !tt.PaymentTaxTableGUID.HasValue))
								 select it).ToList().FirstOrDefault();
				if (validate7 != null)
					ex.AddData("ERROR.90120", new string[] { "LABEL.TAX_CODE", "LABEL.PAYMENT_TAX_CODE", validate7.InvoiceId });
				#endregion
				#region 8
				var validate8 = (from isd in viewPostSettlementParameter.InvoiceSettlementDetails
								 join it in viewPostSettlementParameter.InvoiceTables
								 on isd.InvoiceTableGUID equals it.InvoiceTableGUID
								 join il in viewPostSettlementParameter.InvoiceLines
								 on isd.InvoiceTableGUID equals il.InvoiceTableGUID
								 where isd.WHTAmount != 0 &&
								 !il.WithholdingTaxTableGUID.HasValue
								 select it).ToList().FirstOrDefault();
				if (validate8 != null)
					ex.AddData("ERROR.90141", new string[] { "LABEL.WITHHOLDING_TAX_CODE", validate8.InvoiceId });
				#endregion

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void UpdateCustTrans_Settle(ViewPostSettlementParameter parameter, ViewPostSettlementResult result)
		{
			try
			{
				result.UpdateCustTrans = parameter.CustTrans;
				if (parameter.RefType == RefType.ReceiptTemp)
				{
					result.UpdateCustTrans.Join(parameter.InvoiceSettlementDetails, ct => ct.InvoiceTableGUID, isd => isd.InvoiceTableGUID, (ct, isd) => new { ct, isd })
						.Select(u =>
						{
							u.ct.LastSettleDate = parameter.ReceiptTempTable.TransDate;
							u.ct.SettleAmount = u.ct.SettleAmount + u.isd.SettleInvoiceAmount;
							u.ct.SettleAmountMST = (u.ct.SettleAmountMST + (u.isd.SettleInvoiceAmount * parameter.ReceiptTempTable.ExchangeRate)).Round();
							u.ct.CustTransStatus = (u.ct.SettleAmount == u.ct.TransAmount) ? (int)CustTransStatus.Closed : (int)CustTransStatus.Open;
							return u;
						}).ToList();
				}
				if (parameter.RefType == RefType.CustomerRefund)
				{
					result.UpdateCustTrans.Join(parameter.InvoiceSettlementDetails, ct => ct.InvoiceTableGUID, isd => isd.InvoiceTableGUID, (ct, isd) => new { ct, isd })
						.Select(u =>
						{
							u.ct.LastSettleDate = parameter.CustomerRefundTable.TransDate;
							u.ct.SettleAmount = u.ct.SettleAmount + u.isd.SettleInvoiceAmount;
							u.ct.SettleAmountMST = (u.ct.SettleAmountMST + (u.isd.SettleInvoiceAmount * parameter.CustomerRefundTable.ExchangeRate)).Round();
							u.ct.CustTransStatus = (u.ct.SettleAmount == u.ct.TransAmount) ? (int)CustTransStatus.Closed : (int)CustTransStatus.Open;
							return u;
						}).ToList();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenCreditAppTrans_Settle(ViewPostSettlementParameter parameter, ViewPostSettlementResult result)
		{
			try
			{
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);

				Guid companyGUID = parameter.InvoiceTables.Select(s => s.CompanyGUID).FirstOrDefault();
				List<Guid> creditAppTableGUIDs = parameter.InvoiceTables.Where(w => w.CreditAppTableGUID.HasValue).Select(s => s.CreditAppTableGUID.Value).ToList();
				List<CreditAppTable> dbCreditAppTables = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppTableGUIDs);
				List<CreditLimitType> dbCreditLimitTypes = creditLimitTypeRepo.GetCreditLimitTypeByCompanyNoTracking(companyGUID).ToList();

				var query = (from it in parameter.InvoiceTables
							 join cat in dbCreditAppTables
							 on it.CreditAppTableGUID equals cat.CreditAppTableGUID
							 join clt in dbCreditLimitTypes
							 on cat.CreditLimitTypeGUID equals clt.CreditLimitTypeGUID
							 join isd in parameter.InvoiceSettlementDetails
							 on it.InvoiceTableGUID equals isd.InvoiceTableGUID
							 where it.ProductInvoice == true &&
							 clt.Revolving == true &&
							 (it.RefType == (int)RefType.WithdrawalLine || it.RefType == (int)RefType.PurchaseLine)
							 select new {
								 InvoiceTable = it,
								 InvoiceSettlementDetail = isd
							 }).ToList();

				if (!ConditionService.IsEqualZero(query.Count()))
				{
					result.CreateCreditAppTrans = query.Select(s => new CreditAppTrans
					{
						CreditAppTransGUID = Guid.NewGuid(),
						CreditAppTableGUID = s.InvoiceTable.CreditAppTableGUID.GetValueOrDefault(),
						ProductType = s.InvoiceTable.ProductType,
						CustomerTableGUID = s.InvoiceTable.CustomerTableGUID,
						BuyerTableGUID = s.InvoiceTable.BuyerTableGUID,
						TransDate = parameter.ReceiptTempTable.TransDate,
						DocumentId = parameter.ReceiptTempTable.ReceiptTempId,
						CompanyGUID = s.InvoiceTable.CompanyGUID,
						RefType = (int)RefType.InvoiceSettlementDetail,
						RefGUID = s.InvoiceSettlementDetail.InvoiceSettlementDetailGUID,
						InvoiceAmount = s.InvoiceSettlementDetail.SettleInvoiceAmount * (-1),
						CreditDeductAmount = (s.InvoiceTable.RefType == (int)RefType.WithdrawalLine) ?
												(s.InvoiceSettlementDetail.SettleInvoiceAmount * (-1)) :
												(s.InvoiceSettlementDetail.SettlePurchaseAmount * (-1)),
						CreditAppLineGUID = (s.InvoiceTable.RefType == (int)RefType.WithdrawalLine) ?
												withdrawalTableRepo.GetWithdrawalTableByInvoiceTableRefGuid(s.InvoiceTable.RefGUID).CreditAppLineGUID :
												purchaseLineRepo.GetPurchaseLineByIdNoTracking(s.InvoiceTable.RefGUID).CreditAppLineGUID
					}).ToList();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenReceiptTable_Line(ViewPostSettlementParameter parameter, ViewPostSettlementResult result)
		{
			try
			{
				INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				
				Guid receiptTablePosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ReceiptStatus.Posted).ToString()).DocumentStatusGUID;
				var query = (from it in parameter.InvoiceTables
							 join isd in parameter.InvoiceSettlementDetails
							 on it.InvoiceTableGUID equals isd.InvoiceTableGUID
							 where isd.SuspenseInvoiceType == (int)SuspenseInvoiceType.None
							 select new
							 {
								 InvoiceTable = it,
								 InvoiceSettlementDetail = isd
							 }).ToList();

				if (!ConditionService.IsEqualZero(query.Count()))
				{
					var getReceiptNumberSeqGUID = query
						.GroupBy(g => new { g.InvoiceTable.CompanyGUID, g.InvoiceTable.BranchGUID, g.InvoiceTable.InvoiceTableGUID, g.InvoiceTable.ProductType })
						.Select(s => new
						{
							s.Key.CompanyGUID,
							s.Key.BranchGUID,
							s.Key.ProductType,
							s.Key.InvoiceTableGUID,
							ReceiptNumberSeqGUID = numberSeqSetupByProductTypeRepo.GetReceiptNumberSeqGUID(s.Key.CompanyGUID, s.Key.ProductType),
							Manual = numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqSetupByProductTypeRepo.GetReceiptNumberSeqGUID(s.Key.CompanyGUID, s.Key.ProductType)),
							ReceiptTableGUID = Guid.NewGuid()
						}).ToList();

					if (!ConditionService.IsEqualZero(getReceiptNumberSeqGUID.Where(w => w.Manual == true).Count()))
					{
						SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
						smartAppException.AddData("ERROR.90050", new string[] { "LABEL.RECEIPT_ID" });
						throw smartAppException;
					}

					List<NumberSequences> numberSequences = getReceiptNumberSeqGUID.Select(s => new NumberSequences
					{
						Key = s.InvoiceTableGUID,
						NumberSeqTableGUID = s.ReceiptNumberSeqGUID,
						CompanyGUID = s.CompanyGUID,
						BranchGUID = s.BranchGUID
					}).ToList();
					numberSequences = numberSequenceService.GetNumber(numberSequences);

					List<Guid> creditAppTableGUIDs = parameter.InvoiceTables.Where(w => w.CreditAppTableGUID.HasValue).Select(s => s.CreditAppTableGUID.Value).ToList();
					List<CreditAppTable> dbCreditAppTables = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppTableGUIDs);
					List<Guid> receiptAddressGUIDs = dbCreditAppTables.Where(w => w.ReceiptAddressGUID.HasValue).Select(s => s.ReceiptAddressGUID.Value).ToList();
					List<AddressTrans> dbAddressTrans_CreditAppTableGUIDNotNull = addressTransRepo.GetAddressTransByIdNoTracking(receiptAddressGUIDs);
					List<AddressTrans> dbAddressTrans_CreditAppTableGUIDNull = addressTransRepo.GetPrimaryAddressByReference(RefType.Customer, query.Select(s => s.InvoiceTable.CustomerTableGUID).ToList());

					#region ReceiptTable
					Guid refGuid = Guid.Empty;
					DateTime receiptDate = DateTime.Now;
					DateTime transDate = DateTime.Now;
					Guid? currencyGUID = null;
					decimal exchangeRate = 0;
					if (parameter.RefType == RefType.ReceiptTemp)
					{
						refGuid = parameter.ReceiptTempTable.ReceiptTempTableGUID;
						receiptDate = parameter.ReceiptTempTable.ReceiptDate;
						transDate = parameter.ReceiptTempTable.TransDate;
						currencyGUID = parameter.ReceiptTempTable.CurrencyGUID;
						exchangeRate = parameter.ReceiptTempTable.ExchangeRate;
					}
					else if (parameter.RefType == RefType.CustomerRefund)
					{
						refGuid = parameter.CustomerRefundTable.CustomerRefundTableGUID;
						receiptDate = parameter.CustomerRefundTable.TransDate;
						transDate = parameter.CustomerRefundTable.TransDate;
						currencyGUID = parameter.CustomerRefundTable.CurrencyGUID;
						exchangeRate = parameter.CustomerRefundTable.ExchangeRate;
					}

					result.CreateReceiptTable = (from it in query.Select(s => s.InvoiceTable)
												 join isd in query.Select(s => s.InvoiceSettlementDetail)
												 on it.InvoiceTableGUID equals isd.InvoiceTableGUID
												 join cat in dbCreditAppTables
												 on it.CreditAppTableGUID equals cat.CreditAppTableGUID into lj1
												 from cat in lj1.DefaultIfEmpty()
												 join adt_notnull in dbAddressTrans_CreditAppTableGUIDNotNull
												 on ((cat == null) ? Guid.Empty : cat.ReceiptAddressGUID) equals adt_notnull.AddressTransGUID into lj2
												 from adt_notnull in lj2.DefaultIfEmpty()
												 join adt_null in dbAddressTrans_CreditAppTableGUIDNull
												 on it.CustomerTableGUID equals adt_null.RefGUID into lj3
												 from adt_null in lj3.DefaultIfEmpty()
												 select new ReceiptTable
												 {
													 ReceiptTableGUID = getReceiptNumberSeqGUID.Where(w => w.InvoiceTableGUID == it.InvoiceTableGUID).Select(s => s.ReceiptTableGUID).FirstOrDefault(),
													 RefType = (int)parameter.RefType,
													 RefGUID = refGuid,
													 ReceiptDate = receiptDate,
													 TransDate = transDate,
													 CurrencyGUID = currencyGUID,
													 ExchangeRate = exchangeRate,
													 ReceiptId = numberSequences.Where(w => w.Key == it.InvoiceTableGUID).Select(s => s.GeneratedId).FirstOrDefault(),
													 CustomerTableGUID = it.CustomerTableGUID,
													 MethodOfPaymentGUID = null,
													 DocumentStatusGUID = receiptTablePosted,
													 ReceiptAddress1 = it.CreditAppTableGUID.HasValue ? (adt_notnull == null ? null : adt_notnull.Address1) : (adt_null == null ? null : adt_null.Address1),
													 ReceiptAddress2 = it.CreditAppTableGUID.HasValue ? (adt_notnull == null ? null : adt_notnull.Address2) : (adt_null == null ? null : adt_null.Address2),
													 Remark = null,
													 SettleBaseAmount = isd.SettleInvoiceAmount - isd.SettleTaxAmount,
													 SettleTaxAmount = isd.SettleTaxAmount,
													 SettleAmount = isd.SettleInvoiceAmount,
													 SettleBaseAmountMST = ((isd.SettleInvoiceAmount - isd.SettleTaxAmount) * exchangeRate).Round(),
													 SettleTaxAmountMST = (isd.SettleTaxAmount * exchangeRate).Round(),
													 SettleAmountMST = (isd.SettleInvoiceAmount * exchangeRate).Round(),
													 OverUnderAmount = 0,
													 ChequeDate = null,
													 ChequeNo = null,
													 ChequeBankGroupGUID = null,
													 ChequeBranch = null,
													 InvoiceSettlementDetailGUID = isd.InvoiceSettlementDetailGUID,
													 CompanyGUID = it.CompanyGUID,
													 BranchGUID = it.BranchGUID
												 }).ToList();
					#endregion
					#region ReceiptLine
					result.CreateReceiptLine = (from it in query.Select(s => s.InvoiceTable)
												join isd in query.Select(s => s.InvoiceSettlementDetail)
												on it.InvoiceTableGUID equals isd.InvoiceTableGUID
												join il_tax in parameter.InvoiceLines.Where(w => w.TaxTableGUID.HasValue)
																				 .GroupBy(g => g.InvoiceTableGUID)
																				 .Select(s => new { InvoiceTableGUID = s.Key, TaxTableGUID = s.FirstOrDefault().TaxTableGUID.Value })
												on it.InvoiceTableGUID equals il_tax.InvoiceTableGUID into lj1
												from il_tax in lj1.DefaultIfEmpty()
												join il_wht in parameter.InvoiceLines.Where(w => w.WithholdingTaxTableGUID.HasValue)
																					 .GroupBy(g => g.InvoiceTableGUID)
																					 .Select(s => new { InvoiceTableGUID = s.Key, WithholdingTaxTableGUID = s.FirstOrDefault().WithholdingTaxTableGUID.Value })
												on it.InvoiceTableGUID equals il_wht.InvoiceTableGUID into lj2
												from il_wht in lj2.DefaultIfEmpty()
												select new ReceiptLine
												{
													ReceiptLineGUID = Guid.NewGuid(),
													ReceiptTableGUID = getReceiptNumberSeqGUID.Where(w => w.InvoiceTableGUID == it.InvoiceTableGUID).Select(s => s.ReceiptTableGUID).FirstOrDefault(),
													LineNum = 1,
													InvoiceTableGUID = it.InvoiceTableGUID,
													DueDate = it.DueDate,
													InvoiceAmount = it.InvoiceAmount,
													TaxAmount = it.TaxAmount,
													TaxTableGUID = il_tax?.TaxTableGUID,
													WithholdingTaxTableGUID = il_wht?.WithholdingTaxTableGUID,
													SettleBaseAmount = isd.SettleInvoiceAmount - isd.SettleTaxAmount,
													SettleTaxAmount = isd.SettleTaxAmount,
													SettleAmount = isd.SettleInvoiceAmount,
													WHTAmount = isd.WHTAmount,
													SettleBaseAmountMST = ((isd.SettleInvoiceAmount - isd.SettleTaxAmount) * exchangeRate).Round(),
													SettleTaxAmountMST = (isd.SettleTaxAmount * exchangeRate).Round(),
													SettleAmountMST = (isd.SettleInvoiceAmount * exchangeRate).Round(),
													CompanyGUID = it.CompanyGUID,
													BranchGUID = it.BranchGUID
												}).ToList();
                    #endregion
                }
				else
                {
					result.CreateReceiptTable = new List<ReceiptTable>();
					result.CreateReceiptLine = new List<ReceiptLine>();
                }
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenTaxInvoice_Payment(ViewPostSettlementParameter parameter, ViewPostSettlementResult result)
        {
            try
            {
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);

				Guid taxInvoiceStatusPosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)TaxInvoiceStatus.Posted).ToString()).DocumentStatusGUID;

				List<Guid> taxTableGUIDs = parameter.InvoiceLines.Where(w => w.TaxTableGUID.HasValue).GroupBy(g => g.TaxTableGUID.Value).Select(s => s.Key).ToList();
				List<TaxTable> taxTables = taxTableRepo.GetTaxTableByIdNoTracking(taxTableGUIDs);

				var query = (from it in parameter.InvoiceTables
							 join isd in parameter.InvoiceSettlementDetails
							 on it.InvoiceTableGUID equals isd.InvoiceTableGUID
							 join il in parameter.InvoiceLines
							 on it.InvoiceTableGUID equals il.InvoiceTableGUID
							 join tt in taxTables
							 on il.TaxTableGUID equals tt.TaxTableGUID
							 where isd.SettleTaxAmount != 0 &&
							 it.SuspenseInvoiceType == (int)SuspenseInvoiceType.None &&
							 il.TaxTableGUID.HasValue &&
							 tt.PaymentTaxTableGUID.HasValue
							 select new
							 {
								 InvoiceTable = it,
								 InvoiceLine = il,
								 InvoiceSettlementDetail = isd,
								 TaxTable = tt
							 }).ToList();

				if (!ConditionService.IsEqualZero(query.Count()))
				{
					var getTaxInvoiceNumberSeqGUID = query
						.GroupBy(g => new { g.InvoiceTable.CompanyGUID, g.InvoiceTable.BranchGUID, g.InvoiceTable.InvoiceTableGUID, g.InvoiceTable.ProductType })
						.Select(s => new
						{
							s.Key.CompanyGUID,
							s.Key.BranchGUID,
							s.Key.ProductType,
							s.Key.InvoiceTableGUID,
							TaxInvoiceNumberSeqGUID = numberSeqSetupByProductTypeRepo.GetTaxInvoiceNumberSeqGUID(s.Key.CompanyGUID, s.Key.ProductType),
							Manual = numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqSetupByProductTypeRepo.GetTaxInvoiceNumberSeqGUID(s.Key.CompanyGUID, s.Key.ProductType)),
							TaxInvoiceTableGUID = Guid.NewGuid()
						}).ToList();

					if (!ConditionService.IsEqualZero(getTaxInvoiceNumberSeqGUID.Where(w => w.Manual == true).Count()))
					{
						SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
						smartAppException.AddData("ERROR.90050", new string[] { "LABEL.TAX_INVOICE_ID" });
						throw smartAppException;
					}

					List<NumberSequences> numberSequences = getTaxInvoiceNumberSeqGUID.Select(s => new NumberSequences
					{
						Key = s.InvoiceTableGUID,
						NumberSeqTableGUID = s.TaxInvoiceNumberSeqGUID,
						CompanyGUID = s.CompanyGUID,
						BranchGUID = s.BranchGUID
					}).ToList();
					numberSequences = numberSequenceService.GetNumber(numberSequences);

					#region TaxInvoiceTable
					var query41 = query.Select(s => new {
						s.InvoiceTable.InvoiceTableGUID,
						s.InvoiceLine.TotalAmountBeforeTax,
						s.InvoiceLine.TaxAmount,
						s.InvoiceSettlementDetail.SettleTaxAmount,
						SettleAmountBeforeTax = s.InvoiceSettlementDetail.SettleInvoiceAmount - s.InvoiceSettlementDetail.SettleTaxAmount
					}).GroupBy(g => new { g.InvoiceTableGUID, g.SettleTaxAmount, g.SettleAmountBeforeTax }).Select(s => new
					{
						s.Key.InvoiceTableGUID,
						TotalAmountbeforeTax_forTax = s.Sum(sum => sum.TotalAmountBeforeTax).Round(),
						TotalTaxAmount = s.Sum(sum => sum.TaxAmount).Round(),
						PaymentBaseAmount_forTax = (((s.Key.SettleTaxAmount * s.Sum(sum => sum.TotalAmountBeforeTax).Round()) / s.Sum(sum => sum.TaxAmount).Round()).Round() > s.Key.SettleAmountBeforeTax)
														? s.Key.SettleAmountBeforeTax
														: ((s.Key.SettleTaxAmount * s.Sum(sum => sum.TotalAmountBeforeTax).Round()) / s.Sum(sum => sum.TaxAmount).Round()).Round()
					}).ToList();

					DateTime issuedDate = DateTime.Now;
					DateTime dueDate = DateTime.Now;
					Guid? currencyGUID = null;
					decimal exchangeRate = 0;
					if (parameter.RefType == RefType.ReceiptTemp)
					{
						issuedDate = parameter.ReceiptTempTable.TransDate;
						dueDate = parameter.ReceiptTempTable.TransDate;
						currencyGUID = parameter.ReceiptTempTable.CurrencyGUID;
						exchangeRate = parameter.ReceiptTempTable.ExchangeRate;
					}
					else if (parameter.RefType == RefType.CustomerRefund)
					{
						issuedDate = parameter.CustomerRefundTable.TransDate;
						dueDate = parameter.CustomerRefundTable.TransDate;
						currencyGUID = parameter.CustomerRefundTable.CurrencyGUID;
						exchangeRate = parameter.CustomerRefundTable.ExchangeRate;
					}

					result.CreateTaxInvoiceTable = (from it in query.GroupBy(g => g.InvoiceTable).Select(s => s.Key)
													join q41 in query41
													on it.InvoiceTableGUID equals q41.InvoiceTableGUID
													join isd in query.GroupBy(g => g.InvoiceSettlementDetail).Select(s => s.Key)
													on it.InvoiceTableGUID equals isd.InvoiceTableGUID
													join rt in result.CreateReceiptTable
													on isd.InvoiceSettlementDetailGUID equals rt.InvoiceSettlementDetailGUID
													select new TaxInvoiceTable
													{
														TaxInvoiceTableGUID = getTaxInvoiceNumberSeqGUID.Where(w => w.InvoiceTableGUID == it.InvoiceTableGUID).Select(s => s.TaxInvoiceTableGUID).FirstOrDefault(),
														IssuedDate = issuedDate,
														DueDate = dueDate,
														CurrencyGUID = currencyGUID.Value,
														ExchangeRate = exchangeRate,
														TaxInvoiceId = numberSequences.Where(w => w.Key == it.InvoiceTableGUID).Select(s => s.GeneratedId).FirstOrDefault(),
														CustomerTableGUID = it.CustomerTableGUID,
														CustomerName = it.CustomerName,
														InvoiceAmountBeforeTax = q41.PaymentBaseAmount_forTax,
														TaxAmount = isd.SettleTaxAmount,
														InvoiceAmount = q41.PaymentBaseAmount_forTax + isd.SettleTaxAmount,
														InvoiceAmountBeforeTaxMST = (q41.PaymentBaseAmount_forTax * exchangeRate).Round(),
														TaxAmountMST = (isd.SettleTaxAmount * exchangeRate).Round(),
														InvoiceAmountMST = ((q41.PaymentBaseAmount_forTax + isd.SettleTaxAmount) * exchangeRate).Round(),
														Remark = null,
														InvoiceAddress1 = it.InvoiceAddress1,
														InvoiceAddress2 = it.InvoiceAddress2,
														MailingInvoiceAddress1 = it.MailingInvoiceAddress1,
														MailingInvoiceAddress2 = it.MailingInvoiceAddress2,
														OrigTaxInvoiceAmount = 0,
														CNReasonGUID = null,
														DocumentStatusGUID = taxInvoiceStatusPosted,
														InvoiceTypeGUID = it.InvoiceTypeGUID,
														Dimension5GUID = it.Dimension5GUID,
														Dimension4GUID = it.Dimension4GUID,
														Dimension3GUID = it.Dimension3GUID,
														Dimension2GUID = it.Dimension2GUID,
														Dimension1GUID = it.Dimension1GUID,
														MethodOfPaymentGUID = null,
														CompanyGUID = it.CompanyGUID,
														BranchGUID = it.BranchGUID,
														TaxBranchId = it.TaxBranchId,
														TaxBranchName = it.TaxBranchName,
														RefTaxInvoiceGUID = null,
														OrigTaxInvoiceId = null,
														InvoiceTableGUID = isd.InvoiceTableGUID.Value,
														DocumentId = it.DocumentId,
														TaxInvoiceRefType = (int)TaxInvoiceRefType.Receipt,
														TaxInvoiceRefGUID = rt.ReceiptTableGUID
													}).ToList();
					#endregion
					#region TaxInvoiceLine
					result.CreateTaxInvoiceLine = (from il in query.Where(w => w.InvoiceLine.TaxAmount != 0).Select(s => s.InvoiceLine)
												   join q41 in query41
												   on il.InvoiceTableGUID equals q41.InvoiceTableGUID
												   join it in query.GroupBy(g => g.InvoiceTable).Select(s => s.Key)
												   on il.InvoiceTableGUID equals it.InvoiceTableGUID
												   join isd in query.GroupBy(g => g.InvoiceSettlementDetail).Select(s => s.Key)
												   on it.InvoiceTableGUID equals isd.InvoiceTableGUID
												   join tt in query.GroupBy(g => g.TaxTable).Select(s => s.Key)
												   on il.TaxTableGUID equals tt.TaxTableGUID
												   select new TaxInvoiceLine
												   {
													   TaxInvoiceLineGUID = Guid.NewGuid(),
													   TaxInvoiceTableGUID = getTaxInvoiceNumberSeqGUID.Where(w => w.InvoiceTableGUID == il.InvoiceTableGUID).Select(s => s.TaxInvoiceTableGUID).FirstOrDefault(),
													   LineNum = il.LineNum,
													   InvoiceText = il.InvoiceText,
													   Qty = il.Qty,
													   UnitPrice = ((il.TotalAmountBeforeTax / q41.TotalAmountbeforeTax_forTax) * q41.PaymentBaseAmount_forTax).Round(),
													   TotalAmountBeforeTax = ((il.TotalAmountBeforeTax / q41.TotalAmountbeforeTax_forTax) * q41.PaymentBaseAmount_forTax).Round(),
													   TaxAmount = ((il.TaxAmount / it.TaxAmount) * isd.SettleTaxAmount).Round(),
													   TotalAmount = ((il.TotalAmountBeforeTax / q41.TotalAmountbeforeTax_forTax) * q41.PaymentBaseAmount_forTax).Round() +
																	 ((il.TaxAmount / it.TaxAmount) * isd.SettleTaxAmount).Round(),
													   InvoiceRevenueTypeGUID = il.InvoiceRevenueTypeGUID,
													   Dimension1GUID = il.Dimension1GUID,
													   Dimension2GUID = il.Dimension2GUID,
													   Dimension3GUID = il.Dimension3GUID,
													   Dimension4GUID = il.Dimension4GUID,
													   Dimension5GUID = il.Dimension5GUID,
													   ProdUnitGUID = il.ProdUnitGUID,
													   TaxTableGUID = tt.PaymentTaxTableGUID.Value,
													   CompanyGUID = il.CompanyGUID,
													   BranchGUID = il.BranchGUID
												   }).ToList();
                    #endregion
                }
                else
                {
                    result.CreateTaxInvoiceTable = new List<TaxInvoiceTable>();
                    result.CreateTaxInvoiceLine = new List<TaxInvoiceLine>();
                }
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenPaymentHistory(ViewPostSettlementParameter parameter, ViewPostSettlementResult result)
        {
            try
            {
				RefType? refType = null;
				Guid? refGUID = null;
				DateTime paymentDate = DateTime.Now;
				Guid? currencyGUID = null;
				decimal exchangeRate = 0;
				int receivedFrom = 0;
				// R05
				DateTime receivedDate = DateTime.Now;

				if (parameter.RefType == RefType.ReceiptTemp)
				{
					refType = parameter.RefType;
					refGUID = parameter.ReceiptTempTable.ReceiptTempTableGUID;
					paymentDate = parameter.ReceiptTempTable.TransDate;
					currencyGUID = parameter.ReceiptTempTable.CurrencyGUID;
					exchangeRate = parameter.ReceiptTempTable.ExchangeRate;
					receivedFrom = parameter.ReceiptTempTable.ReceivedFrom;
					// R05
					receivedDate = parameter.ReceiptTempTable.ReceiptDate;
				}
				else if (parameter.RefType == RefType.CustomerRefund)
				{
					refType = parameter.RefType;
					refGUID = parameter.CustomerRefundTable.CustomerRefundTableGUID;
					paymentDate = parameter.CustomerRefundTable.TransDate;
					currencyGUID = parameter.CustomerRefundTable.CurrencyGUID;
					exchangeRate = parameter.CustomerRefundTable.ExchangeRate;
					receivedFrom = (int)ReceivedFrom.Customer;
					// R05
					receivedDate = parameter.CustomerRefundTable.TransDate;
				}

				result.CreatePaymentHistory = (from ct in parameter.CustTrans
											   join isd in parameter.InvoiceSettlementDetails
											   on ct.InvoiceTableGUID equals isd.InvoiceTableGUID
											   join rt in result.CreateReceiptTable
											   on isd.InvoiceSettlementDetailGUID equals rt.InvoiceSettlementDetailGUID into lj01
											   from rt in lj01.DefaultIfEmpty()
											   join tit in result.CreateTaxInvoiceTable
											   on ct.InvoiceTableGUID equals tit.InvoiceTableGUID into lj02
											   from tit in lj02.DefaultIfEmpty()
											   join il_tax in parameter.InvoiceLines.Where(w => w.TaxTableGUID.HasValue)
																				    .GroupBy(g=> g.InvoiceTableGUID)
																				    .Select(s => new { InvoiceTableGUID = s.Key, TaxTableGUID = s.Select(se => se.TaxTableGUID).FirstOrDefault() })
											   on ct.InvoiceTableGUID equals il_tax.InvoiceTableGUID into lj03
											   from il_tax in lj03.DefaultIfEmpty()
											   join il_wht in parameter.InvoiceLines.Where(w => w.WithholdingTaxTableGUID.HasValue)
																					.GroupBy(g => g.InvoiceTableGUID)
																					.Select(s => new { InvoiceTableGUID = s.Key, WithholdingTaxTableGUID = s.Select(se => se.WithholdingTaxTableGUID).FirstOrDefault() })
											   on ct.InvoiceTableGUID equals il_wht.InvoiceTableGUID into lj04
											   from il_wht in lj04.DefaultIfEmpty()
											   select new PaymentHistory
											   {
												   PaymentHistoryGUID = Guid.NewGuid(),
												   RefType = (int)refType,
												   RefGUID = refGUID,
												   PaymentDate = paymentDate,
												   CurrencyGUID = currencyGUID.Value,
												   ExchangeRate = exchangeRate,
												   ReceivedFrom = receivedFrom,
												   CustomerTableGUID = ct.CustomerTableGUID,
												   CreditAppTableGUID = ct.CreditAppTableGUID,
												   ProductType = ct.ProductType,
												   InvoiceTableGUID = ct.InvoiceTableGUID,
												   DueDate = ct.DueDate,
												   PaymentBaseAmount = isd.SettleInvoiceAmount - isd.SettleTaxAmount,
												   PaymentTaxAmount = isd.SettleTaxAmount,
												   PaymentAmount = isd.SettleInvoiceAmount,
												   WHTAmount = isd.WHTAmount,
												   WHTSlipReceivedByCustomer = isd.WHTSlipReceivedByCustomer,
												   PaymentBaseAmountMST = ((isd.SettleInvoiceAmount - isd.SettleTaxAmount) * exchangeRate).Round(),
												   PaymentTaxAmountMST = (isd.SettleTaxAmount * exchangeRate).Round(),
												   PaymentAmountMST = (isd.SettleInvoiceAmount * exchangeRate).Round(),
												   WHTAmountMST = (isd.WHTAmount * exchangeRate).Round(),
												   PaymentTaxBaseAmount = (tit == null) ? 0 : tit.InvoiceAmountBeforeTax,
												   PaymentTaxBaseAmountMST = (tit == null) ? 0 : (tit.InvoiceAmountBeforeTax * exchangeRate).Round(),
												   OrigTaxTableGUID = il_tax?.TaxTableGUID,
												   WithholdingTaxTableGUID = il_wht?.WithholdingTaxTableGUID,
												   ReceiptTableGUID = rt?.ReceiptTableGUID,
												   InvoiceSettlementDetailGUID = isd.InvoiceSettlementDetailGUID,
												   CustTransGUID = ct.CustTransGUID,
												   CompanyGUID = ct.CompanyGUID,
												   BranchGUID = ct.BranchGUID,
												   // R05
												   ReceivedDate = receivedDate
											   }).ToList();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private void GenProcessTrans_Payment(ViewPostSettlementParameter parameter, ViewPostSettlementResult result)
        {
			try
			{
				result.CreateProcessTrans = new List<ProcessTrans>();
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				List<InvoiceType> invoiceTypes = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(parameter.InvoiceTables.Select(s => s.InvoiceTypeGUID).ToList()).ToList();
				var query = (from it in parameter.InvoiceTables
							 join isd in parameter.InvoiceSettlementDetails
							 on it.InvoiceTableGUID equals isd.InvoiceTableGUID
							 join ph in result.CreatePaymentHistory
							 on isd.InvoiceSettlementDetailGUID equals ph.InvoiceSettlementDetailGUID
							 join its in invoiceTypes
							 on it.InvoiceTypeGUID equals its.InvoiceTypeGUID
							 select new { it, isd, ph , its }).ToList();

				var taxInvoice = (from tit in result.CreateTaxInvoiceTable
								  join til in result.CreateTaxInvoiceLine
								  on tit.TaxInvoiceTableGUID equals til.TaxInvoiceTableGUID
								  group new
								  {
									  tit.InvoiceTableGUID,
									  tit.TaxInvoiceTableGUID,
									  til.TaxTableGUID,
									  til.LineNum
								  } by new { tit.InvoiceTableGUID, tit.TaxInvoiceTableGUID } into g
								  select new
								  {
									  g.Key.InvoiceTableGUID,
									  g.Key.TaxInvoiceTableGUID,
									  TaxTableGUID = g.OrderBy(o => o.LineNum).Select(s => s.TaxTableGUID).FirstOrDefault()
								  }).ToList();

				if (!ConditionService.IsEqualZero(query.Count()))
				{
					result.CreateProcessTrans = (from q in query
												 join tit in taxInvoice
												 on q.isd.InvoiceTableGUID equals tit.InvoiceTableGUID into lj01
												 from tit in lj01.DefaultIfEmpty()
												 select new ProcessTrans
												 {
													 ProcessTransGUID = Guid.NewGuid(),
													 TransDate = q.ph.PaymentDate,
													 CreditAppTableGUID = q.ph.CreditAppTableGUID,
													 CustomerTableGUID = q.ph.CustomerTableGUID,
													 Revert = false,
													 ProcessTransType = (int)ProcessTransType.Payment,
													 Amount = q.ph.PaymentAmount,
													 AmountMST = q.ph.PaymentAmountMST,
													 ExchangeRate = q.ph.ExchangeRate,
													 TaxAmount = q.ph.PaymentTaxAmount,
													 TaxAmountMST = q.ph.PaymentTaxAmountMST,
													 ARLedgerAccount = q.its.ARLedgerAccount,
													 ProductType = q.ph.ProductType,
													 CurrencyGUID = q.ph.CurrencyGUID,
													 DocumentReasonGUID = null,
													 OrigTaxTableGUID = q.ph.OrigTaxTableGUID,
													 RefTaxInvoiceGUID = tit?.TaxInvoiceTableGUID,
													 TaxTableGUID = tit?.TaxTableGUID,
													 RefType = q.ph.RefType,
													 RefGUID = q.ph.RefGUID,
													 InvoiceTableGUID = q.ph.InvoiceTableGUID,
													 DocumentId = q.it.DocumentId,
													 RefProcessTransGUID = null,
													 StagingBatchId = null,
													 StagingBatchStatus = (int)StagingBatchStatus.None,
													 PaymentHistoryGUID = q.ph.PaymentHistoryGUID,
													 CompanyGUID = q.ph.CompanyGUID,
													 #region Method044_PostSettlement_R04
													 SourceRefType = (int)parameter.SourceRefType,
													 SourceRefId = parameter.SourceRefId
                                                     #endregion
                                                 }).ToList();
				}
				else if (parameter.RefType == RefType.ReceiptTemp)
				{
					result.CreateProcessTrans.Add(new ProcessTrans
					{
						ProcessTransGUID = Guid.NewGuid(),
						TransDate = parameter.ReceiptTempTable.TransDate,
						CreditAppTableGUID = parameter.ReceiptTempTable.CreditAppTableGUID,
						CustomerTableGUID = parameter.ReceiptTempTable.CustomerTableGUID,
						Revert = false,
						ProcessTransType = (int)ProcessTransType.Payment,
						Amount = parameter.ReceiptTempTable.ReceiptAmount,
						ExchangeRate = parameter.ReceiptTempTable.ExchangeRate,
						AmountMST = (parameter.ReceiptTempTable.ReceiptAmount * parameter.ReceiptTempTable.ExchangeRate).Round(),
						TaxAmount = 0,
						TaxAmountMST = 0,
						ARLedgerAccount = string.Empty,
						ProductType = parameter.ReceiptTempTable.ProductType,
						CurrencyGUID = parameter.ReceiptTempTable.CurrencyGUID.Value,
						DocumentReasonGUID = null,
						OrigTaxTableGUID = null,
						RefTaxInvoiceGUID = null,
						TaxTableGUID = null,
						RefType = (int)parameter.RefType,
						RefGUID = parameter.ReceiptTempTable.ReceiptTempTableGUID,
						InvoiceTableGUID = null,
						DocumentId = null,
						RefProcessTransGUID = null,
						StagingBatchId = null,
						StagingBatchStatus = (int)StagingBatchStatus.None,
						PaymentHistoryGUID = null,
						CompanyGUID = parameter.ReceiptTempTable.CompanyGUID,
						#region Method044_PostSettlement_R04
						SourceRefType = (int)parameter.SourceRefType,
						SourceRefId = parameter.SourceRefId
						#endregion
					});
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region Post ReceiptTemp
		public PostReceiptTempParamView GetPostReceiptTempById(string receiptTempTableGUID)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTableItemView receiptTempTable = receiptTempTableRepo.GetByIdvwByCancelReceiptTemp(receiptTempTableGUID.StringToGuid());

				PostReceiptTempParamView cancelReceiptTempView = new PostReceiptTempParamView
				{
					ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID,
					ReceiptTempTable_Values = SmartAppUtil.GetDropDownLabel(receiptTempTable.ReceiptTempId, receiptTempTable.ReceiptDate),
					ReceiptDate = receiptTempTable.ReceiptDate,
					TransDate = receiptTempTable.TransDate,
					ProductType = receiptTempTable.ProductType,
					ReceivedFrom = receiptTempTable.ReceivedFrom,
					CustomerTable_Values = receiptTempTable.CustomerTable_Values,
					BuyerTable_Values = receiptTempTable.BuyerTable_Values,
					ReceiptAmount = receiptTempTable.ReceiptAmount,
					SettleFeeAmount = receiptTempTable.SettleFeeAmount,
					SettleAmount = receiptTempTable.SettleAmount,
					SuspenseInvoiceType_Values = receiptTempTable.SuspenseInvoiceType_Values,
					SuspenseAmount = receiptTempTable.SuspenseAmount,
					CreditAppTable_Values = receiptTempTable.CreditAppTable_Values,
				};
				return cancelReceiptTempView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostReceiptTempResultView PostReceiptTemp(PostReceiptTempParamView parm)
		{
			try
			{
				IBuyerReceiptTableService buyerReceiptTableService = new BuyerReceiptTableService(db);
				PostReceiptTempResultView result = new PostReceiptTempResultView();
				PostReceiptTempResultViewMap postReceiptTempResult = InitPostReceiptTemp(parm.ReceiptTempTableGUID.StringToGuid());

				#region check result count
				bool hasBuyerReceiptTable = postReceiptTempResult.BuyerReceiptTable != null;
				#endregion

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					this.BulkUpdate(new List<ReceiptTempTable>() { postReceiptTempResult.ReceiptTempTable });
					this.BulkUpdate(postReceiptTempResult.CustTransUpdate);
					// R07
					this.BulkUpdate(postReceiptTempResult.ProductSettledTrans);

					this.BulkInsert(postReceiptTempResult.NewReceiptTempTable);
					this.BulkInsert(postReceiptTempResult.ReceiptTempPaymDetail);

					this.BulkInsert(postReceiptTempResult.InvoiceTable);
					this.BulkInsert(postReceiptTempResult.InvoiceLine);
						
					this.BulkInsert(postReceiptTempResult.InvoiceSettlementDetail);
						
					this.BulkInsert(postReceiptTempResult.ReceiptTable);
					this.BulkInsert(postReceiptTempResult.ReceiptLine);

					this.BulkInsert(postReceiptTempResult.PaymentHistory);

					this.BulkInsert(postReceiptTempResult.TaxInvoiceTable);
					this.BulkInsert(postReceiptTempResult.TaxInvoiceLine);

					this.BulkInsert(postReceiptTempResult.ProcessTrans);
					this.BulkInsert(postReceiptTempResult.CustTransCreate);
					this.BulkInsert(postReceiptTempResult.CreditAppTrans);
					this.BulkInsert(postReceiptTempResult.RetentionTrans);
					this.BulkUpdate(postReceiptTempResult.InterestRealizedTrans);
					this.BulkInsert(postReceiptTempResult.AssignmentAgreementSettle);
					if (hasBuyerReceiptTable)
					{
						this.BulkInsert(new List<BuyerReceiptTable>() { postReceiptTempResult.BuyerReceiptTable });
					}
					this.BulkInsert(postReceiptTempResult.IntercompanyInvoiceTable);

					UnitOfWork.Commit(transaction);
				}
				
				NotificationResponse success = new NotificationResponse();
				success.AddData("SUCCESS.90004", new string[] { "LABEL.COLLECTION", SmartAppUtil.GetDropDownLabel(postReceiptTempResult.ReceiptTempTable.ReceiptTempId,
																									postReceiptTempResult.ReceiptTempTable.ReceiptDate.DateToString()) });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostReceiptTempResultViewMap InitPostReceiptTemp(Guid receiptTempTableGUID)
		{
			try
			{
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				var receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(receiptTempTableGUID);
				return InitPostReceiptTemp(receiptTempTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostReceiptTempResultViewMap InitPostReceiptTemp(ReceiptTempTable receiptTempTable)
		{
			try
			{
				PostReceiptTempResultViewMap result = new PostReceiptTempResultViewMap();
				PostReceiptTempParamViewMap parm = PreparePostReceiptTempParam(receiptTempTable);

				result.NewReceiptTempTable = new List<ReceiptTempTable>();
				result.InvoiceTable = new List<InvoiceTable>();
				result.InvoiceLine = new List<InvoiceLine>();
				result.ProcessTrans = new List<ProcessTrans>();
				result.CustTransCreate = new List<CustTrans>();
				result.CustTransUpdate = new List<CustTrans>();
				result.CreditAppTrans = new List<CreditAppTrans>();
				result.TaxInvoiceTable = new List<TaxInvoiceTable>();
				result.TaxInvoiceLine = new List<TaxInvoiceLine>();
				result.RetentionTrans = new List<RetentionTrans>();
				result.InterestRealizedTrans = new List<InterestRealizedTrans>();
				result.AssignmentAgreementSettle = new List<AssignmentAgreementSettle>();
				result.ReceiptTempPaymDetail = new List<ReceiptTempPaymDetail>();
				result.InvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
				result.ReceiptTable = new List<ReceiptTable>();
				result.ReceiptLine = new List<ReceiptLine>();
				result.PaymentHistory = new List<PaymentHistory>();
				result.IntercompanyInvoiceTable = new List<IntercompanyInvoiceTable>();
				// R07
				result.ProductSettledTrans = new List<ProductSettledTrans>();

				if (ValidatePostReceiptTemp(parm))
				{
					IInvoiceService invoiceService = new InvoiceService(db);
					IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
					IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
					IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
					IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);

					ICustTransRepo custTransRepo = new CustTransRepo(db);
					INumberSequenceService numberSequenceService = new NumberSequenceService(db);

					IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);

					List<InvoiceTable> invoiceTableList = new List<InvoiceTable>();
					List<InvoiceLine> invoiceLineList = new List<InvoiceLine>();
					List<InvoiceTable> reservedRefundInvoiceTableList = new List<InvoiceTable>();
					List<InvoiceLine> reservedRefundInvoiceLineList = new List<InvoiceLine>();
					List<InvoiceTable> serviceFeeTransInvoiceTableList = new List<InvoiceTable>();
					List<InvoiceLine> serviceFeeTransInvoiceLineList = new List<InvoiceLine>();
					List<InterestRealizedTrans> interestRealizedTransList = new List<InterestRealizedTrans>();
					List<ReceiptTempTable> newReceiptTempTable = new List<ReceiptTempTable>();
					List<ReceiptTempPaymDetail> receiptTempPaymDetailList = new List<ReceiptTempPaymDetail>();
					List<InvoiceSettlementDetail> invoiceSettlementDetailList = new List<InvoiceSettlementDetail>();
					List<SettlePDCInterestOutstandingPostReceiptTempView> settlePDCParmList = new List<SettlePDCInterestOutstandingPostReceiptTempView>();
					List<AssignmentAgreementSettle> assignmentAgreementSettleList = new List<AssignmentAgreementSettle>();

					GenInvoiceFromServiceFeeTransResultView serviceFeeTransInvoiceResult = null;
					List<InvoiceSettlementDetail> serviceFeeTransInvSettleDetail = new List<InvoiceSettlementDetail>();

					List<InvoiceTable> callerInvoiceTable = parm.InvoiceTable;
					List<InvoiceLine> callerInvoiceline =
						invoiceLineRepo.GetInvoiceLinesByInvoiceTableGuid(callerInvoiceTable.Select(s => s.InvoiceTableGUID));
					List<CustTrans> callerCustTrans = parm.CustTrans;

					Guid receiptTempPostedStatusGUID = parm.DocumentStatus.Where(w => w.StatusId == ((int)TemporaryReceiptStatus.Posted).ToString())
																									.FirstOrDefault().DocumentStatusGUID;
					#region 1 settle ProductSettledTrans
					if (parm.ReceiptTempTable.ProductType == (int)ProductType.Factoring ||
						parm.ReceiptTempTable.ProductType == (int)ProductType.ProjectFinance)
					{
						var parmInvSettlementDetails = parm.InvoiceSettlementDetail.Where(w => w.BalanceAmount == w.SettleInvoiceAmount);
						var parmInvoiceTables =
							callerInvoiceTable.Where(w => parmInvSettlementDetails.Select(s => s.InvoiceTableGUID.Value).Contains(w.InvoiceTableGUID));
						var parmProdSettleTransInvSettleDetailInvoice =
							(from productSettledTrans in parm.ProductSettledTrans
							 join invoiceSettlementDetail in parmInvSettlementDetails
							 on productSettledTrans.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID
							 join invoiceTable in parmInvoiceTables
							 on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID
							 select new { productSettledTrans, invoiceSettlementDetail, invoiceTable });
						#region 1.3.2 create invoice from productSettledTrans.ReserveRefund > 0
						if (parm.ProductSettledTrans.Any(a => a.ReserveToBeRefund > 0))
						{
							var reserveRefundGreaterThanZero = parm.ProductSettledTrans.Where(w => w.ReserveToBeRefund > 0);
							foreach (var productSettledTrans in reserveRefundGreaterThanZero)
							{
								var parmProdSettleTrans = parmProdSettleTransInvSettleDetailInvoice
															.Where(w => w.productSettledTrans.ProductSettledTransGUID == productSettledTrans.ProductSettledTransGUID)
															.FirstOrDefault();
								var reserveRefundInvoiceResult = CreateInvoice1LineSpecificForPostReceiptTemp(parm.ReceiptTempTable,
																								parm.CompanyParameter.FTReserveToBeRefundedInvTypeGUID.Value,
																								(productSettledTrans.ReserveToBeRefund * (-1)),
																								parmProdSettleTrans.invoiceTable,
																								parmProdSettleTrans.invoiceSettlementDetail,
																								true);
								reservedRefundInvoiceTableList.Add(reserveRefundInvoiceResult.InvoiceTable);
								reservedRefundInvoiceLineList.Add(reserveRefundInvoiceResult.InvoiceLine);
							}
							invoiceTableList.AddRange(reservedRefundInvoiceTableList);
							invoiceLineList.AddRange(reservedRefundInvoiceLineList);
						}
						#endregion
						// 1.4 SumInterestCalcAmount
						var sumInterestCalcAmount = parm.SumInterestCalcAmount.Select(s => new
						{
							OriginalRefGUID = s.Key,
							SumInterestCaclAmount = s.Value
						});

						#region 1.5 !purchaseLine.CloseForAdditionalPurchase
						if (parm.ReceiptTempTable.ProductType == (int)ProductType.Factoring)
						{
							var purchaseLineSumIntCalcAmountProdSettledTrans =
							(from parmProdSettleTrans in parmProdSettleTransInvSettleDetailInvoice
							 join sumInterestCalc in sumInterestCalcAmount
							 on parmProdSettleTrans.productSettledTrans.OriginalRefGUID equals sumInterestCalc.OriginalRefGUID
							 join purchaseLine in parm.PurchaseLine
							 on parmProdSettleTrans.productSettledTrans.RefGUID equals purchaseLine.PurchaseLineGUID
							 select new
							 {
								 SumInterestCalcAmount = sumInterestCalc.SumInterestCaclAmount,
								 parmProdSettleTrans.productSettledTrans,
								 parmProdSettleTrans.invoiceTable,
								 parmProdSettleTrans.invoiceSettlementDetail,
								 purchaseLine.ClosedForAdditionalPurchase
							 });
							#region 1.5.1 create invoice from SumInterestCalcAmount
							if (purchaseLineSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount != 0 && !a.ClosedForAdditionalPurchase))
							{
								var notClosedForAdditionalPurchase = purchaseLineSumIntCalcAmountProdSettledTrans
																		.Where(a => a.SumInterestCalcAmount != 0 && !a.ClosedForAdditionalPurchase);

								foreach (var parmPurchLineSumIntProd in notClosedForAdditionalPurchase)
								{
									var ftSumInterestCalcInvoiceType = parmPurchLineSumIntProd.SumInterestCalcAmount < 0 ?
											parm.CompanyParameter.FTInterestRefundInvTypeGUID.Value : parm.CompanyParameter.FTInterestInvTypeGUID.Value;
									var ftSumInterestCalcInvoiceResult = CreateInvoice1LineSpecificForPostReceiptTemp(parm.ReceiptTempTable,
																										ftSumInterestCalcInvoiceType,
																										parmPurchLineSumIntProd.SumInterestCalcAmount,
																										parmPurchLineSumIntProd.invoiceTable,
																										parmPurchLineSumIntProd.invoiceSettlementDetail,
																										true);
									invoiceTableList.Add(ftSumInterestCalcInvoiceResult.InvoiceTable);
									invoiceLineList.Add(ftSumInterestCalcInvoiceResult.InvoiceLine);

									#region R07 1.5.1.4 update ProductSettledTrans_FT
									parmPurchLineSumIntProd.productSettledTrans.PostedInterestAmount = parmPurchLineSumIntProd.SumInterestCalcAmount;
									result.ProductSettledTrans.Add(parmPurchLineSumIntProd.productSettledTrans);
									#endregion
								}

							}
							#endregion
							#region 1.5.2 update InterestRealizedTrans_FT

							var interestRealizedTransFT =
								interestRealizedTransRepo.GetInterestRealizedTransForPostReceiptTempNoTracking(ProductType.Factoring,
																parm.PurchaseLine.Where(w => !w.ClosedForAdditionalPurchase &&
																							(parm.ProductSettledTrans.Where(w => w.RefGUID.HasValue).Select(s => s.RefGUID.Value))
																							.Contains(w.PurchaseLineGUID))
																				.Select(s => s.PurchaseLineGUID));

							var updateInterestRealizedTransFT =
								(from productSettledTrans in parm.ProductSettledTrans.Where(w => w.RefGUID.HasValue)
								 join invoiceSettlementDetail in parmInvSettlementDetails
								 on productSettledTrans.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID
								 join interestRealizedTrans in interestRealizedTransFT
								 on productSettledTrans.RefGUID.Value equals interestRealizedTrans.RefGUID
								 where interestRealizedTrans.AccountingDate > productSettledTrans.SettledDate
								 select new { interestRealizedTrans, productSettledTrans.SettledDate })
								 .Select(s =>
								 {
									 s.interestRealizedTrans.AccountingDate = s.SettledDate;
									 return s.interestRealizedTrans;
								 });
							interestRealizedTransList.AddRange(updateInterestRealizedTransFT);
							#endregion
						}

						#endregion

						#region 1.6 !withdrawalLine.ClosedForTermExtension
						if (parm.ReceiptTempTable.ProductType == (int)ProductType.ProjectFinance)
						{
							var withdrawalSumIntCalcAmountProdSettledTrans =
							(from parmProdSettleTrans in parmProdSettleTransInvSettleDetailInvoice
							 join sumInterestCalc in sumInterestCalcAmount
							 on parmProdSettleTrans.productSettledTrans.OriginalRefGUID equals sumInterestCalc.OriginalRefGUID
							 join withdrawalLine in parm.WithdrawalLine
							 on parmProdSettleTrans.productSettledTrans.RefGUID equals withdrawalLine.WithdrawalLineGUID
							 select new
							 {
								 SumInterestCalcAmount = sumInterestCalc.SumInterestCaclAmount,
								 parmProdSettleTrans.productSettledTrans,
								 parmProdSettleTrans.invoiceTable,
								 parmProdSettleTrans.invoiceSettlementDetail,
								 withdrawalLine.ClosedForTermExtension
							 });
							var pdcInterestOutstanding = parm.CustTransCalcPDCInterestOutstanding.Select(s => new
							{
								WithdrawalTableGUID = s.Key,
								CustTransPDC = s.Value
							});
							if (withdrawalSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount != 0 && !a.ClosedForTermExtension))
							{
								var notClosedForTermExtension = withdrawalSumIntCalcAmountProdSettledTrans
																		.Where(a => a.SumInterestCalcAmount != 0 && !a.ClosedForTermExtension);

								// invoicetable for CustTransPDC
								List<InvoiceTable> invoiceTablePDCs = new List<InvoiceTable>();
								List<InvoiceLine> invoiceLinePDCs = new List<InvoiceLine>();
								if (notClosedForTermExtension.Any(a => a.SumInterestCalcAmount < 0 &&
																	 a.productSettledTrans.PDCInterestOutstanding > 0 &&
																	 a.productSettledTrans.TotalRefundAdditionalIntAmount <= 0))
								{
									var settlePDCProdSettleTrans = notClosedForTermExtension.Where(a => a.SumInterestCalcAmount < 0 &&
																	a.productSettledTrans.PDCInterestOutstanding > 0 &&
																	a.productSettledTrans.TotalRefundAdditionalIntAmount <= 0);
									var productSettledTransOrigRefGUIDs = settlePDCProdSettleTrans.Where(w => w.productSettledTrans.OriginalRefGUID.HasValue)
																						.Select(s => s.productSettledTrans.OriginalRefGUID.Value).Distinct();
									var withdrawalTables = parm.WithdrawalLine.Where(w => productSettledTransOrigRefGUIDs.Contains(w.WithdrawalLineGUID))
																				.Select(s => s.WithdrawalTableGUID).Distinct();
									pdcInterestOutstanding = pdcInterestOutstanding.Where(w => withdrawalTables.Contains(w.WithdrawalTableGUID));
									invoiceTablePDCs = invoiceTableRepo.GetInvoiceTableByIdNoTracking(pdcInterestOutstanding.SelectMany(sm =>
																											sm.CustTransPDC.Select(s => s.InvoiceTableGUID)));
									invoiceLinePDCs = invoiceLineRepo.GetInvoiceLinesByInvoiceTableGuid(invoiceLinePDCs.Select(s => s.InvoiceTableGUID));
								}

								foreach (var parmWithdrawLineSumIntProd in notClosedForTermExtension)
								{

									#region 1.6.1 create invoice from SumInterestCalcAmount
									var pfSumInterestCalcInvoiceType = parmWithdrawLineSumIntProd.SumInterestCalcAmount < 0 ?
											parm.CompanyParameter.PFInterestRefundInvTypeGUID.Value : parm.CompanyParameter.PFInterestInvTypeGUID.Value;
									var pfSumInterestCalcInvoiceResult = CreateInvoice1LineSpecificForPostReceiptTemp(parm.ReceiptTempTable,
																													pfSumInterestCalcInvoiceType,
																													parmWithdrawLineSumIntProd.SumInterestCalcAmount,
																													parmWithdrawLineSumIntProd.invoiceTable,
																													parmWithdrawLineSumIntProd.invoiceSettlementDetail,
																													false);
									invoiceTableList.Add(pfSumInterestCalcInvoiceResult.InvoiceTable);
									invoiceLineList.Add(pfSumInterestCalcInvoiceResult.InvoiceLine);

									#region R07 1.6.1.4 update ProductSettledTrans_PF
									parmWithdrawLineSumIntProd.productSettledTrans.PostedInterestAmount = parmWithdrawLineSumIntProd.SumInterestCalcAmount;
									result.ProductSettledTrans.Add(parmWithdrawLineSumIntProd.productSettledTrans);
									#endregion
									
									#endregion
									#region 1.6.2 settle pdc interest outstanding
									if (parmWithdrawLineSumIntProd.SumInterestCalcAmount < 0 &&
										parmWithdrawLineSumIntProd.productSettledTrans.PDCInterestOutstanding > 0 &&
										parmWithdrawLineSumIntProd.productSettledTrans.TotalRefundAdditionalIntAmount <= 0)
									{
										SettlePDCInterestOutstandingPostReceiptTempView pdcSettleParm = new SettlePDCInterestOutstandingPostReceiptTempView();
										List<InvoiceSettlementDetail> pdcInvSettleDetail = new List<InvoiceSettlementDetail>();

										pdcSettleParm.InvoiceTableSumIntCalcAmount = pfSumInterestCalcInvoiceResult.InvoiceTable;
										pdcSettleParm.InvoiceLineSumIntCalcAmount = pfSumInterestCalcInvoiceResult.InvoiceLine;

										#region 1.6.2.1 create pdc receiptTemp
										ReceiptTempTable pdcReceiptTempTable = CreatePDCReceiptTempForPostReceiptTemp(parm.ReceiptTempTable,
																													receiptTempPostedStatusGUID,
																													parmWithdrawLineSumIntProd.productSettledTrans);
										pdcSettleParm.ReceiptTempPDC = pdcReceiptTempTable;
										#endregion
										#region 1.6.2.2 create invoiceSettlementDetail from pdc invoice
										var withdrawalTable = parm.WithdrawalLine.Where(w => parmWithdrawLineSumIntProd.productSettledTrans.OriginalRefGUID == w.WithdrawalLineGUID)
																				.Select(s => s.WithdrawalTableGUID).FirstOrDefault();
										var custTransInvoicePDC = pdcInterestOutstanding.Where(w => w.WithdrawalTableGUID == withdrawalTable)
																						.SelectMany(ss => ss.CustTransPDC);
										var invoiceTables = invoiceTablePDCs.Where(w => custTransInvoicePDC.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)).ToList();
										var invoiceLines = invoiceLinePDCs.Where(w => invoiceTables.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)).ToList();
										var invoiceSettlementDetailPDCResult =
											invoiceSettlementDetailService.GenInvoiceSettlementDetailFromInvoice(invoiceTables, RefType.ReceiptTemp,
																													pdcReceiptTempTable.ReceiptTempTableGUID);
										pdcInvSettleDetail.AddRange(invoiceSettlementDetailPDCResult);
										pdcSettleParm.InvoiceTablePDC = invoiceTables;
										pdcSettleParm.InvoiceLinePDC = invoiceLines;
										pdcSettleParm.CustTransPDC = custTransInvoicePDC.ToList();
										#endregion
										#region 1.6.2.3 create pdc receiptTempPaymDetail
										ReceiptTempPaymDetail pdcReceiptTempPaymDetail = new ReceiptTempPaymDetail
										{
											ReceiptTempTableGUID = pdcReceiptTempTable.ReceiptTempTableGUID,
											MethodOfPaymentGUID = parm.CompanyParameter.SuspenseMethodOfPaymentGUID.Value,
											ReceiptAmount = parmWithdrawLineSumIntProd.productSettledTrans.PDCInterestOutstanding,
											ChequeTableGUID = null,
											ChequeBankGroupGUID = null,
											TransferReference = string.Empty,
											ReceiptTempPaymDetailGUID = Guid.NewGuid(),
											CompanyGUID = parm.ReceiptTempTable.CompanyGUID
										};
										receiptTempPaymDetailList.Add(pdcReceiptTempPaymDetail);
										InvoiceSettlementDetail pdcInvSettleDetailReceiptTmpPaymDetail =
											CreatePDCInvoiceSettlementDetailReceiptTempPaymDetailForPostReceiptTemp(pdcSettleParm.InvoiceTableSumIntCalcAmount,
																													parmWithdrawLineSumIntProd.productSettledTrans,
																													pdcReceiptTempTable.ReceiptTempTableGUID,
																													pdcReceiptTempPaymDetail.ReceiptTempPaymDetailGUID);
										pdcInvSettleDetail.Add(pdcInvSettleDetailReceiptTmpPaymDetail);
										#endregion
										pdcSettleParm.InvoiceSettlementDetailPDC = pdcInvSettleDetail;

										settlePDCParmList.Add(pdcSettleParm);
									}
									#endregion
									
								}
							}
							#region 1.6.3 update InterestRealizedTrans PF
							var interestRealizedTransPF =
								interestRealizedTransRepo.GetInterestRealizedTransForPostReceiptTempNoTracking(ProductType.ProjectFinance,
																parm.WithdrawalLine.Where(w => !w.ClosedForTermExtension &&
																							(parm.ProductSettledTrans.Where(w => w.RefGUID.HasValue).Select(s => s.RefGUID.Value))
																							.Contains(w.WithdrawalLineGUID))
																					.Select(s => s.WithdrawalLineGUID));

							var updateInterestRealizedTransPF =
								(from productSettledTrans in parm.ProductSettledTrans.Where(w => w.RefGUID.HasValue)
								 join invoiceSettlementDetail in parmInvSettlementDetails
								 on productSettledTrans.InvoiceSettlementDetailGUID equals invoiceSettlementDetail.InvoiceSettlementDetailGUID
								 join interestRealizedTrans in interestRealizedTransPF
								 on productSettledTrans.RefGUID.Value equals interestRealizedTrans.RefGUID
								 where interestRealizedTrans.AccountingDate > productSettledTrans.SettledDate
								 select new { interestRealizedTrans, productSettledTrans.SettledDate })
								 .Select(s =>
								 {
									 s.interestRealizedTrans.AccountingDate = s.SettledDate;
									 return s.interestRealizedTrans;
								 });
							interestRealizedTransList.AddRange(updateInterestRealizedTransPF);
							#endregion
						}
						#endregion
					}
					#endregion
					#region 2.1 create invoice from suspense amount
					if (parm.ReceiptTempTable.SuspenseAmount != 0 && parm.ReceiptTempTable.SuspenseInvoiceTypeGUID.HasValue)
					{
						var suspenseAmountInvoiceResult = CreateInvoice1LineSpecificForPostReceiptTemp(parm.ReceiptTempTable,
																											parm.ReceiptTempTable.SuspenseInvoiceTypeGUID.Value,
																											(parm.ReceiptTempTable.SuspenseAmount * (-1)),
																											null,
																											null,
																											false);
						invoiceTableList.Add(suspenseAmountInvoiceResult.InvoiceTable);
						invoiceLineList.Add(suspenseAmountInvoiceResult.InvoiceLine);

					}
					#endregion

					if (parm.ServiceFeeTrans.Count > 0)
					{
						#region 3.1 create invoice from serviceFeeTrans
						GenInvoiceFromServiceFeeTransParamView serviceFeeTransInvoiceParam = new GenInvoiceFromServiceFeeTransParamView
						{
							ServiceFeeTrans = parm.ServiceFeeTrans,
							IssuedDate = parm.ReceiptTempTable.TransDate,
							CustomerTableGUID = parm.ReceiptTempTable.CustomerTableGUID,
							ProductType = (ProductType)parm.ReceiptTempTable.ProductType,
							DocumentId = parm.ReceiptTempTable.ReceiptTempId
						};

						serviceFeeTransInvoiceResult = serviceFeeTransService.GenInvoiceFromServiceFeeTrans(serviceFeeTransInvoiceParam);
						serviceFeeTransInvoiceTableList.AddRange(serviceFeeTransInvoiceResult.InvoiceTable);
						serviceFeeTransInvoiceLineList.AddRange(serviceFeeTransInvoiceResult.InvoiceLine);
						invoiceTableList.AddRange(serviceFeeTransInvoiceTableList);
						invoiceLineList.AddRange(serviceFeeTransInvoiceLineList);

						// R02
						result.IntercompanyInvoiceTable.AddRange(serviceFeeTransInvoiceResult.IntercompanyInvoiceTable);
						#endregion
						#region 4 create invoiceSettlementDetail from serviceFeeTrans
						if (parm.ServiceFeeTrans.Any(a => a.SettleAmount > 0))
						{
							serviceFeeTransInvSettleDetail =
								invoiceSettlementDetailService.GenInvoiceSettlementDetailFromServiceFeeTrans(parm.ServiceFeeTrans.Where(w => w.SettleAmount > 0).ToList(),
																											serviceFeeTransInvoiceResult.InvoiceTable, parm.ReceiptTempTable.ReceiptTempId,
																											RefType.ReceiptTemp, parm.ReceiptTempTable.ReceiptTempTableGUID);
							invoiceSettlementDetailList.AddRange(serviceFeeTransInvSettleDetail);
						}
						#endregion
					}

					#region 5 create assignmentAgreementSettle
					if (parm.InvoiceSettlementDetail.Any(a => a.SettleAssignmentAmount > 0 && a.AssignmentAgreementTableGUID.HasValue))
					{
						assignmentAgreementSettleList =
							CreateAssignmentAgreementSettleForPostReceiptTemp(parm.ReceiptTempTable,
																			parm.InvoiceSettlementDetail.Where(a => a.SettleAssignmentAmount > 0 &&
																													a.AssignmentAgreementTableGUID.HasValue));
					}
					#endregion
					#region 6 create buyerReceiptTable
					if (parm.ReceiptTempTable.ReceivedFrom == (int)ReceivedFrom.Buyer && !parm.ReceiptTempTable.BuyerReceiptTableGUID.HasValue)
					{
						ReceiptTempPaymDetail receiptTempPaymDetail = parm.ReceiptTempPaymDetail.OrderByDescending(o => o.ReceiptAmount).FirstOrDefault();
						result.BuyerReceiptTable = CreateBuyerReceiptTableForPostReceiptTemp(parm.ReceiptTempTable, receiptTempPaymDetail);
					}
					#endregion

					#region post invoice 1.3.3, 1.5.1.3, 1.6.1.3, 2.2, 3.2
					var postInvoiceResult = invoiceService.PostInvoice(invoiceTableList, invoiceLineList, RefType.ReceiptTemp, parm.ReceiptTempTable.ReceiptTempTableGUID);
					#endregion
					#region generate numberseq (for 1.6.2.1, 6)
					List<NumberSequences> numberSequences = new List<NumberSequences>();
					if (parm.ReceiptTempNumberSeqTable != null && newReceiptTempTable.Count > 0)
					{
						var receiptTempNumberSeqs = newReceiptTempTable.Select(s => new NumberSequences
						{
							Key = s.ReceiptTempTableGUID,
							BranchGUID = null,
							CompanyGUID = parm.ReceiptTempTable.CompanyGUID,
							NumberSeqTableGUID = parm.ReceiptTempNumberSeqTable.NumberSeqTableGUID
						});
						numberSequences.AddRange(receiptTempNumberSeqs);
					}
					if (parm.BuyerReceiptNumberSeqTable != null && result.BuyerReceiptTable != null)
					{
						numberSequences.Add(new NumberSequences
						{
							Key = result.BuyerReceiptTable.BuyerReceiptTableGUID,
							BranchGUID = null,
							CompanyGUID = parm.ReceiptTempTable.CompanyGUID,
							NumberSeqTableGUID = parm.BuyerReceiptNumberSeqTable.NumberSeqTableGUID
						});
					}
					if (numberSequences.Count > 0)
					{
						numberSequences = numberSequenceService.GetNumber(numberSequences);
						if (newReceiptTempTable.Count > 0)
						{
							foreach (var receiptTemp in newReceiptTempTable)
							{
								receiptTemp.ReceiptTempId =
									numberSequences.Where(w => w.Key == receiptTemp.ReceiptTempTableGUID).FirstOrDefault().GeneratedId;
							}
						}
						if (result.BuyerReceiptTable != null)
						{
							result.BuyerReceiptTable.BuyerReceiptId =
								numberSequences.Where(w => w.Key == result.BuyerReceiptTable.BuyerReceiptTableGUID).FirstOrDefault().GeneratedId;
						}
					}

					#endregion

					#region post settlement 
					#region 1.6.2.4
					if (settlePDCParmList.Count > 0)
					{
                        foreach (var item in settlePDCParmList)
                        {
                            #region prepare settle PDC posttlement parameter
                            List<InvoiceTable> settlePDCInvoiceTable = new List<InvoiceTable>();
							List<InvoiceLine> settlePDCInvoiceLine = new List<InvoiceLine>();
							List<CustTrans> settlePDCCustTrans = new List<CustTrans>();
							if (item.InvoiceTableSumIntCalcAmount != null)
                            {
								var invoiceTableFromPostInvoice = 
									postInvoiceResult.InvoiceTables.Where(w => w.InvoiceTableGUID == item.InvoiceTableSumIntCalcAmount.InvoiceTableGUID)
																	.FirstOrDefault();
								settlePDCInvoiceTable.Add(item.InvoiceTableSumIntCalcAmount);
								var invoiceLineFromPostInvoice =
									postInvoiceResult.InvoiceLines.Where(w => w.InvoiceTableGUID == invoiceTableFromPostInvoice.InvoiceTableGUID);
								settlePDCInvoiceLine.AddRange(invoiceLineFromPostInvoice);
								var custTransFromPostInvoice =
									postInvoiceResult.CustTranses.Where(w => w.InvoiceTableGUID == invoiceTableFromPostInvoice.InvoiceTableGUID);
								settlePDCCustTrans.AddRange(custTransFromPostInvoice);
                            }
							settlePDCInvoiceTable.AddRange(item.InvoiceTablePDC);
							settlePDCInvoiceLine.AddRange(item.InvoiceLinePDC);
							settlePDCCustTrans.AddRange(item.CustTransPDC);
                            #endregion
                            ViewPostSettlementParameter settlePDCPostSettlementParam = new ViewPostSettlementParameter
							{
								RefType = RefType.ReceiptTemp,
								InvoiceSettlementDetails = item.InvoiceSettlementDetailPDC,
								InvoiceTables = settlePDCInvoiceTable,
								InvoiceLines = settlePDCInvoiceLine,
								CustTrans = settlePDCCustTrans,
								ReceiptTempTable = item.ReceiptTempPDC,
								SourceRefId = item.ReceiptTempPDC.ReceiptTempId,
								SourceRefType = RefType.ReceiptTemp
							};
							var settlePDCPostSettlementResult = PostSettlement(settlePDCPostSettlementParam);
							#region assign values from settle PDC post settlement result
							if (settlePDCPostSettlementResult.UpdateCustTrans != null && settlePDCPostSettlementResult.UpdateCustTrans.Count > 0)
							{
								if (item.InvoiceTableSumIntCalcAmount != null)
                                {
									result.CustTransCreate.AddRange(settlePDCPostSettlementResult.UpdateCustTrans
																	.Where(w => w.InvoiceTableGUID == item.InvoiceTableSumIntCalcAmount.InvoiceTableGUID));
									result.CustTransUpdate.AddRange(settlePDCPostSettlementResult.UpdateCustTrans
																	.Where(w => w.InvoiceTableGUID != item.InvoiceTableSumIntCalcAmount.InvoiceTableGUID));
								}
								else
                                {
									result.CustTransUpdate.AddRange(settlePDCPostSettlementResult.UpdateCustTrans);
                                }
							}
							if (settlePDCPostSettlementResult.CreateCreditAppTrans != null && settlePDCPostSettlementResult.CreateCreditAppTrans.Count > 0)
								result.CreditAppTrans.AddRange(settlePDCPostSettlementResult.CreateCreditAppTrans);
							if (settlePDCPostSettlementResult.CreateReceiptTable != null && settlePDCPostSettlementResult.CreateReceiptTable.Count > 0)
								result.ReceiptTable.AddRange(settlePDCPostSettlementResult.CreateReceiptTable);
							if (settlePDCPostSettlementResult.CreateReceiptLine != null && settlePDCPostSettlementResult.CreateReceiptLine.Count > 0)
								result.ReceiptLine.AddRange(settlePDCPostSettlementResult.CreateReceiptLine);
							if (settlePDCPostSettlementResult.CreateTaxInvoiceTable != null && settlePDCPostSettlementResult.CreateTaxInvoiceTable.Count > 0)
								result.TaxInvoiceTable.AddRange(settlePDCPostSettlementResult.CreateTaxInvoiceTable);
							if (settlePDCPostSettlementResult.CreateTaxInvoiceLine != null && settlePDCPostSettlementResult.CreateTaxInvoiceLine.Count > 0)
								result.TaxInvoiceLine.AddRange(settlePDCPostSettlementResult.CreateTaxInvoiceLine);
							if (settlePDCPostSettlementResult.CreatePaymentHistory != null && settlePDCPostSettlementResult.CreatePaymentHistory.Count > 0)
								result.PaymentHistory.AddRange(settlePDCPostSettlementResult.CreatePaymentHistory);
							if (settlePDCPostSettlementResult.CreateProcessTrans != null && settlePDCPostSettlementResult.CreateProcessTrans.Count > 0)
								result.ProcessTrans.AddRange(settlePDCPostSettlementResult.CreateProcessTrans);
							#endregion
							invoiceSettlementDetailList.AddRange(item.InvoiceSettlementDetailPDC);
						}

                    }
					#endregion
					#region 7.1
					#region prepare post settlement parameters
					List<InvoiceSettlementDetail> postSettlementInvSettleDetailParm = new List<InvoiceSettlementDetail>();
					List<InvoiceTable> postSettlementInvoiceTableParm = new List<InvoiceTable>();
					List<InvoiceLine> postSettlementInvoiceLineParm = new List<InvoiceLine>();
					List<CustTrans> postSettlementCustTransParm = new List<CustTrans>();

					postSettlementInvSettleDetailParm.AddRange(serviceFeeTransInvSettleDetail);
					postSettlementInvSettleDetailParm.AddRange(parm.InvoiceSettlementDetail);

					if(serviceFeeTransInvoiceTableList.Count > 0)
                    {
						var postInvoiceServiceFeeInvoiceTable =
							postInvoiceResult.InvoiceTables.Where(w => serviceFeeTransInvoiceTableList.Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID));
						postSettlementInvoiceTableParm.AddRange(postInvoiceServiceFeeInvoiceTable);

						var postInvoiceServiceFeeInvoiceLine =
							postInvoiceResult.InvoiceLines.Where(w => serviceFeeTransInvoiceLineList.Any(a => a.InvoiceLineGUID == w.InvoiceLineGUID));
						postSettlementInvoiceLineParm.AddRange(postInvoiceServiceFeeInvoiceLine);

						var postInvoiceServiceFeeCustTrans =
							postInvoiceResult.CustTranses.Where(w => serviceFeeTransInvoiceTableList.Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID));
						postSettlementCustTransParm.AddRange(postInvoiceServiceFeeCustTrans);
					}
					postSettlementInvoiceTableParm.AddRange(callerInvoiceTable);
					postSettlementInvoiceLineParm.AddRange(callerInvoiceline);
					postSettlementCustTransParm.AddRange(callerCustTrans);
					#endregion
					ViewPostSettlementParameter postSettlementParameter = new ViewPostSettlementParameter
					{
						RefType = RefType.ReceiptTemp,
						InvoiceSettlementDetails = postSettlementInvSettleDetailParm,
						InvoiceTables = postSettlementInvoiceTableParm,
						InvoiceLines = postSettlementInvoiceLineParm,
						CustTrans = postSettlementCustTransParm,
						ReceiptTempTable = parm.ReceiptTempTable,
						SourceRefType = RefType.ReceiptTemp,
						SourceRefId = parm.ReceiptTempTable.ReceiptTempId
					};
					var postSettlementResult = PostSettlement(postSettlementParameter);
                    #endregion
                    #endregion
                    #region 8 & assign results
                    parm.ReceiptTempTable.DocumentStatusGUID = receiptTempPostedStatusGUID;
					result.ReceiptTempTable = parm.ReceiptTempTable;

					result.InvoiceTable.AddRange(postInvoiceResult.InvoiceTables);
					result.InvoiceLine.AddRange(postInvoiceResult.InvoiceLines);
					if(serviceFeeTransInvoiceTableList.Count > 0)
                    {
						var custTransNotPostSettlement =
							postInvoiceResult.CustTranses.Where(w => !serviceFeeTransInvoiceTableList.Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID));
						result.CustTransCreate.AddRange(custTransNotPostSettlement);
                    }
					else
                    {
						result.CustTransCreate.AddRange(postInvoiceResult.CustTranses);
					}

					result.CreditAppTrans.AddRange(postInvoiceResult.CreditAppTranses);
					result.ProcessTrans.AddRange(postInvoiceResult.ProcessTranses);
					result.TaxInvoiceTable.AddRange(postInvoiceResult.TaxInvoiceTables);
					result.TaxInvoiceLine.AddRange(postInvoiceResult.TaxInvoiceLines);
					result.RetentionTrans.AddRange(postInvoiceResult.RetentionTranses);

					result.NewReceiptTempTable.AddRange(newReceiptTempTable);
					result.ReceiptTempPaymDetail.AddRange(receiptTempPaymDetailList);
					result.AssignmentAgreementSettle.AddRange(assignmentAgreementSettleList);
					result.InvoiceSettlementDetail.AddRange(invoiceSettlementDetailList);

					result.InterestRealizedTrans.AddRange(interestRealizedTransList);

					#region assign values from post settlement 7.1
					if (postSettlementResult.UpdateCustTrans != null && postSettlementResult.UpdateCustTrans.Count > 0)
					{
						result.CustTransCreate.AddRange(postSettlementResult.UpdateCustTrans
															.Where(w => !callerCustTrans.Any(a => a.CustTransGUID == w.CustTransGUID)));
						result.CustTransUpdate.AddRange(postSettlementResult.UpdateCustTrans
																.Where(w => callerCustTrans.Any(a => a.CustTransGUID == w.CustTransGUID)));
					}
					if (postSettlementResult.CreateCreditAppTrans != null && postSettlementResult.CreateCreditAppTrans.Count > 0)
						result.CreditAppTrans.AddRange(postSettlementResult.CreateCreditAppTrans);
					if (postSettlementResult.CreateReceiptTable != null && postSettlementResult.CreateReceiptTable.Count > 0)
						result.ReceiptTable.AddRange(postSettlementResult.CreateReceiptTable);
					if (postSettlementResult.CreateReceiptLine != null && postSettlementResult.CreateReceiptLine.Count > 0)
						result.ReceiptLine.AddRange(postSettlementResult.CreateReceiptLine);
					if (postSettlementResult.CreateTaxInvoiceTable != null && postSettlementResult.CreateTaxInvoiceTable.Count > 0)
						result.TaxInvoiceTable.AddRange(postSettlementResult.CreateTaxInvoiceTable);
					if (postSettlementResult.CreateTaxInvoiceLine != null && postSettlementResult.CreateTaxInvoiceLine.Count > 0)
						result.TaxInvoiceLine.AddRange(postSettlementResult.CreateTaxInvoiceLine);
					if (postSettlementResult.CreatePaymentHistory != null && postSettlementResult.CreatePaymentHistory.Count > 0)
						result.PaymentHistory.AddRange(postSettlementResult.CreatePaymentHistory);
					if (postSettlementResult.CreateProcessTrans != null && postSettlementResult.CreateProcessTrans.Count > 0)
						result.ProcessTrans.AddRange(postSettlementResult.CreateProcessTrans);
                    #endregion
                    #endregion
                }

                return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private PostReceiptTempParamViewMap PreparePostReceiptTempParam(ReceiptTempTable receiptTempTable)
		{
			try
			{
				PostReceiptTempParamViewMap parm = new PostReceiptTempParamViewMap();
				parm.ReceiptTempTable = receiptTempTable;

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
				IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
				IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);

				List<string> statusIds = new List<string>();
				List<Guid> invoiceTypeGuids = new List<Guid>();

				statusIds.Add(((int)TemporaryReceiptStatus.Posted).ToString());
				statusIds.Add(((int)TemporaryReceiptStatus.Draft).ToString());

				parm.DocumentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(statusIds.ToArray()).ToList();

				var companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(receiptTempTable.CompanyGUID);
				parm.CompanyParameter = companyParameter;

				parm.ReceiptTempPaymDetail = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByReceiptTempNoTracking(receiptTempTable.ReceiptTempTableGUID).ToList();

				parm.ServiceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.ReceiptTemp, receiptTempTable.ReceiptTempTableGUID).ToList();

				parm.InvoiceSettlementDetail = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByReferanceNoTracking(receiptTempTable.ReceiptTempTableGUID, (int)RefType.ReceiptTemp).ToList();

				if (receiptTempTable.ProductType == (int)ProductType.Factoring ||
					receiptTempTable.ProductType == (int)ProductType.ProjectFinance)
				{
					// 1.1
					var productSettledTransList = productSettledTransRepo.GetProductSettledTransByInvoiceSettlementDetailGuid(parm.InvoiceSettlementDetail
																																.Where(w => w.BalanceAmount == w.SettleInvoiceAmount)
																																.Select(s => s.InvoiceSettlementDetailGUID));
					parm.ProductSettledTrans = productSettledTransList;

					// sumInterestCalcAmount
					var productSettledTransOrigRefGUIDs = productSettledTransList.Where(w => w.OriginalRefGUID.HasValue).Select(s => s.OriginalRefGUID.Value).Distinct();
					var productSettledTransForSumInterestCalcAmount =
							productSettledTransRepo.GetProductSettledTransByOriginalRefGuidAndReceiptTempStatus(productSettledTransOrigRefGUIDs, TemporaryReceiptStatus.Posted);

					// R03
					productSettledTransForSumInterestCalcAmount.AddRange(productSettledTransList);

					var parmSumInterestCalcAmount =
						productSettledTransForSumInterestCalcAmount.GroupBy(g => g.OriginalRefGUID)
									.Select(s => new { OriginalRefGUID = s.Key.Value, SumInterestCalcAmount = s.Sum(sum => sum.InterestCalcAmount) });

					parm.SumInterestCalcAmount = parmSumInterestCalcAmount.ToDictionary(key => key.OriginalRefGUID, val => val.SumInterestCalcAmount);

					if (receiptTempTable.ProductType == (int)ProductType.ProjectFinance)
					{
						IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
						List<Guid> withdrawalLineGuids = new List<Guid>();
						withdrawalLineGuids.AddRange(productSettledTransList.Where(w => w.RefGUID.HasValue).Select(s => s.RefGUID.Value));
						withdrawalLineGuids.AddRange(productSettledTransList.Where(w => w.OriginalRefGUID.HasValue).Select(s => s.OriginalRefGUID.Value));
						parm.WithdrawalLine = withdrawalLineRepo.GetWithdrawalLineByIdNoTracking(withdrawalLineGuids.Distinct());

						// PDC custTrans
						parm.CustTransCalcPDCInterestOutstanding = new Dictionary<Guid, List<CustTrans>>();
						var withdrawalTables = parm.WithdrawalLine.Where(w => productSettledTransOrigRefGUIDs.Contains(w.WithdrawalLineGUID)).Select(s => s.WithdrawalTableGUID).Distinct();
						foreach (var withdrawalTableGuid in withdrawalTables)
						{
							var calcPDCCustTrans = custTransRepo.GetCustTransCalcPDCInterestOutstanding(withdrawalTableGuid);
							parm.CustTransCalcPDCInterestOutstanding.Add(withdrawalTableGuid, calcPDCCustTrans.ToList());
						}
						// receiptTempTableNumberSeq
						var withdrawalSumIntCalcAmountProdSettledTrans =
						(from productSettledTrans in productSettledTransList
						 join sumInterestCalc in parmSumInterestCalcAmount
						 on productSettledTrans.OriginalRefGUID equals sumInterestCalc.OriginalRefGUID
						 join withdrawalLine in parm.WithdrawalLine
						 on productSettledTrans.RefGUID equals withdrawalLine.WithdrawalLineGUID
						 select new
						 {
							 SumInterestCalcAmount = sumInterestCalc.SumInterestCalcAmount,
							 productSettledTrans.PDCInterestOutstanding,
							 productSettledTrans.TotalRefundAdditionalIntAmount,
							 withdrawalLine.ClosedForTermExtension
						 });
						if (withdrawalSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount < 0 &&
																		 a.PDCInterestOutstanding > 0 &&
																		 a.TotalRefundAdditionalIntAmount <= 0 &&
																		 !a.ClosedForTermExtension))
						{
							NumberSeqTable receiptTempNumberSeqTable =
								numberSeqTableRepo.GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(parm.ReceiptTempTable.CompanyGUID, ReferenceId.ReceiptTemp);
							parm.ReceiptTempNumberSeqTable = receiptTempNumberSeqTable;
						}
					}
					if (receiptTempTable.ProductType == (int)ProductType.Factoring)
					{
						IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
						parm.PurchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(productSettledTransList.Where(w => w.RefGUID.HasValue).Select(s => s.RefGUID.Value));
					}
				}

				// get invoiceType
				if (companyParameter.FTReserveToBeRefundedInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTReserveToBeRefundedInvTypeGUID.Value);
				if (companyParameter.FTInterestRefundInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTInterestRefundInvTypeGUID.Value);
				if (companyParameter.FTInterestInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTInterestInvTypeGUID.Value);
				if (companyParameter.PFInterestRefundInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.PFInterestRefundInvTypeGUID.Value);
				if (companyParameter.PFInterestInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.PFInterestInvTypeGUID.Value);

				parm.InvoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTypeGuids);

				// get custTrans
				parm.CustTrans = custTransRepo.GetCustTransByInvoiceTableNoTracking(
										parm.InvoiceSettlementDetail.Where(w => w.InvoiceTableGUID.HasValue)
										.Select(s => s.InvoiceTableGUID.Value)
										.ToList()).ToList();
				parm.InvoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(
										parm.InvoiceSettlementDetail.Where(w => w.InvoiceTableGUID.HasValue)
										.Select(s => s.InvoiceTableGUID.Value));

				// buyerReceiptTableNumberSeq
				if (receiptTempTable.ReceivedFrom == (int)ReceivedFrom.Buyer && !receiptTempTable.BuyerReceiptTableGUID.HasValue)
				{
					NumberSeqTable buyerReceiptNumberSeqTable =
						numberSeqTableRepo.GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(parm.ReceiptTempTable.CompanyGUID, ReferenceId.BuyerReceipt);
					parm.BuyerReceiptNumberSeqTable = buyerReceiptNumberSeqTable;
				}
				return parm;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region validation
		private bool ValidatePostReceiptTemp(PostReceiptTempParamViewMap parm)
		{
			try
			{
				IPaymentHistoryService paymentHistoryService = new PaymentHistoryService(db);
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);

				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				var receiptTempDraft = parm.DocumentStatus.Where(w => w.StatusId == ((int)TemporaryReceiptStatus.Draft).ToString()).FirstOrDefault();
				if (parm.ReceiptTempTable.DocumentStatusGUID != receiptTempDraft.DocumentStatusGUID)
				{
					ex.AddData("ERROR.90012", "LABEL.COLLECTION");
				}
				if (parm.ReceiptTempPaymDetail.Sum(s => s.ReceiptAmount) != parm.ReceiptTempTable.ReceiptAmount)
				{
					ex.AddData("ERROR.90124", "LABEL.RECEIPT_AMOUNT", "LABEL.RECEIPT_AMOUNT", "LABEL.COLLECTION_PAYMENT_DETAIL");
				}
				if (parm.ServiceFeeTrans.Sum(s => s.SettleAmount) != parm.ReceiptTempTable.SettleFeeAmount)
				{
					ex.AddData("ERROR.90124", "LABEL.SETTLE_FEE_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.SERVICE_FEE_TRANSACTIONS");
				}
				if (parm.ReceiptTempTable.ReceiptAmount != (parm.ReceiptTempTable.SettleFeeAmount +
														parm.ReceiptTempTable.SettleAmount +
														parm.ReceiptTempTable.SuspenseAmount))
				{
					ex.AddData("ERROR.90123", "LABEL.SETTLE_FEE_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.SUSPENSE_AMOUNT", "LABEL.RECEIPT_AMOUNT");
				}

				// 5-6
				var checkBalanceAmount =
					(from invSettle in parm.InvoiceSettlementDetail.Where(w => (w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None ||
																				w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount))
					 join custTrans in parm.CustTrans
					 on invSettle.InvoiceTableGUID equals custTrans.InvoiceTableGUID
					 select new
					 {
						 invSettle.BalanceAmount,
						 invSettle.SuspenseInvoiceType,
						 RemainingAmount = custTrans.TransAmount - custTrans.SettleAmount,
						 custTrans.InvoiceTableGUID
					 }).Where(w => w.BalanceAmount != w.RemainingAmount);

				if (checkBalanceAmount.Count() > 0)
				{
					var invoiceTables = parm.InvoiceTable.Where(w => (checkBalanceAmount.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)));
					var invoiceIds =
						(from checkBal in checkBalanceAmount
						 join invoiceTable in invoiceTables
						 on checkBal.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID
						 select new { checkBal.SuspenseInvoiceType, invoiceTable.InvoiceId }).OrderBy(o => o.SuspenseInvoiceType);
					foreach (var inv in invoiceIds)
					{
						if (inv.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
						{
							ex.AddData("ERROR.90046", "LABEL.INVOICE_SETTLEMENT_DETAIL", inv.InvoiceId, "LABEL.POST");
						}
						else
						{
							ex.AddData("ERROR.90046", "LABEL.SUSPENSE_SETTLEMENT_DETAIL", inv.InvoiceId, "LABEL.POST");
						}
					}
				}
				// 7
				if (parm.ReceiptTempPaymDetail.Count == 0 && parm.InvoiceSettlementDetail.Count == 0 && parm.ServiceFeeTrans.Count == 0)
				{
					ex.AddData("ERROR.90125", "LABEL.POST", "LABEL.SETTLE_AMOUNT", "LABEL.COLLECTION_PAYMENT_DETAIL");
				}

				// R06
				// 8
				if (parm.ReceiptTempTable.ExchangeRate <= 0)
                {
					ex.AddData("ERROR.90169", "LABEL.EXCHANGE_RATE", "LABEL.COLLECTION");
                }
				// 9
				var projectFinanceInvoiceTables = parm.InvoiceTable.Where(w => w.ProductInvoice == true && 
																					w.ProductType == (int)ProductType.ProjectFinance);
				if(projectFinanceInvoiceTables.Count() > 0)
                {
					var paymentHistoryList = paymentHistoryRepo
												.GetPaymentHistoryForLastPaymentDateNoTracking(projectFinanceInvoiceTables.Select(s => s.InvoiceTableGUID));
                    foreach (var invoiceTable in projectFinanceInvoiceTables)
                    {
						var lastReceivedDate =
							paymentHistoryService.GetPaymentHistoryLastReceivedDate(paymentHistoryList
																					.Where(w => w.InvoiceTableGUID == invoiceTable.InvoiceTableGUID)
																					.ToList());
						if(lastReceivedDate.HasValue && parm.ReceiptTempTable.ReceiptDate < lastReceivedDate)
                        {
							ex.AddData("ERROR.90170", "LABEL.RECEIPT_DATE", "LABEL.COLLECTION", "LABEL.LAST_RECEIVED_DATE",
										"LABEL.INVOICE_ID", invoiceTable.InvoiceId, lastReceivedDate.DateNullToString());
						}
                    }
                }
				// R08 
				// 10
				if (parm.InvoiceSettlementDetail.Where(w => (w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)).Select(s => s.SettleAmount).Sum() != parm.ReceiptTempTable.SettleAmount)
				{
					ex.AddData("ERROR.90124", "LABEL.SETTLE_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.INVOICE_SETTLEMENT");
				}
				// 11
				var groupSuspenseByRef = parm.InvoiceSettlementDetail.Where(w => (w.SuspenseInvoiceType != (int)SuspenseInvoiceType.None))
																		.GroupBy(g => g.RefReceiptTempPaymDetailGUID).Select(s => new {
																			RefReceiptTempPaymDetailGUID = s.Key,
																			SumSettleAmount = s.Sum(s => s.SettleAmount) * (-1)});
				var v = (from g in groupSuspenseByRef
						 join paym in parm.ReceiptTempPaymDetail
						 on g.RefReceiptTempPaymDetailGUID equals paym.ReceiptTempPaymDetailGUID
						 where g.SumSettleAmount != paym.ReceiptAmount
						 select g);
				if (v.Any())
				{
					ex.AddData("ERROR.90124", "LABEL.RECEIPT_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.SUSPENSE_SETTLEMENT");
				}

				// settle productSettledTrans validation
				if (parm.ReceiptTempTable.ProductType == (int)ProductType.Factoring || parm.ReceiptTempTable.ProductType == (int)ProductType.ProjectFinance)
				{
					ex = ValidateSettleProductSettledTransPostReceiptTemp(ex, parm);
				}
				// validate BuyerReceiptTable numberSeq
				if (parm.ReceiptTempTable.ReceivedFrom == (int)ReceivedFrom.Buyer && !parm.ReceiptTempTable.BuyerReceiptTableGUID.HasValue)
				{
					if (parm.BuyerReceiptNumberSeqTable == null || parm.BuyerReceiptNumberSeqTable.Manual)
					{
						ex.AddData("ERROR.90050", "LABEL.BUYER_RECEIPT_ID");
					}
				}

				if (ex.MessageList.Count > 1)
				{
					throw ex;
				}
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private SmartAppException ValidateSettleProductSettledTransPostReceiptTemp(SmartAppException ex, PostReceiptTempParamViewMap parm)
		{
			try
			{
				var sumInterestCalcAmount = parm.SumInterestCalcAmount.Select(s => new
				{
					OriginalRefGUID = s.Key,
					SumInterestCalcAmount = s.Value
				});

				// 1.3
				if (parm.ProductSettledTrans.Any(a => a.ReserveToBeRefund > 0))
				{
					if (parm.CompanyParameter.FTReserveToBeRefundedInvTypeGUID.HasValue)
					{
						var ftReserveToBeRefundedInvType = parm.InvoiceType.Where(w => w.InvoiceTypeGUID == parm.CompanyParameter.FTReserveToBeRefundedInvTypeGUID.Value).FirstOrDefault();
						if (!ftReserveToBeRefundedInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
						{
							ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_RESERVE_TO_BE_REFUNDED_INV_TYPE_ID");
						}
					}
					else
					{
						ex.AddData("ERROR.90047", "LABEL.FT_RESERVE_TO_BE_REFUNDED_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER");
					}
				}
				#region ProductType = Factoring
				if (parm.ReceiptTempTable.ProductType == (int)ProductType.Factoring)
				{
					// 1.5
					var purchaseLineSumIntCalcAmountProdSettledTrans =
							(from productSettledTrans in parm.ProductSettledTrans
							 join sumInterestCalc in sumInterestCalcAmount
							 on productSettledTrans.OriginalRefGUID equals sumInterestCalc.OriginalRefGUID
							 join purchaseLine in parm.PurchaseLine
							 on productSettledTrans.RefGUID equals purchaseLine.PurchaseLineGUID
							 select new
							 {
								 SumInterestCalcAmount = sumInterestCalc.SumInterestCalcAmount,
								 productSettledTrans.PDCInterestOutstanding,
								 productSettledTrans.TotalRefundAdditionalIntAmount,
								 purchaseLine.ClosedForAdditionalPurchase
							 });
					if (purchaseLineSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount < 0 && !a.ClosedForAdditionalPurchase))
					{
						if (parm.CompanyParameter.FTInterestRefundInvTypeGUID.HasValue)
						{
							var ftInterestRefundInvType = parm.InvoiceType.Where(w => w.InvoiceTypeGUID == parm.CompanyParameter.FTInterestRefundInvTypeGUID.Value).FirstOrDefault();
							if (!ftInterestRefundInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
							{
								ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.PF_INTEREST_REFUND_INVOICE_TYPE_ID");
							}
						}
						else
						{
							ex.AddData("ERROR.90047", "LABEL.PF_INTEREST_REFUND_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
						}
					}
					if (purchaseLineSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount > 0 && !a.ClosedForAdditionalPurchase))
					{
						if (parm.CompanyParameter.FTInterestInvTypeGUID.HasValue)
						{
							var ftInterestInvType = parm.InvoiceType.Where(w => w.InvoiceTypeGUID == parm.CompanyParameter.FTInterestInvTypeGUID.Value).FirstOrDefault();
							if (!ftInterestInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
							{
								ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_INTEREST_INV_TYPE_ID");
							}
						}
						else
						{
							ex.AddData("ERROR.90047", "LABEL.FT_INTEREST_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER");
						}
					}
				}

				#endregion
				#region ProductType = ProjectFinance
				if (parm.ReceiptTempTable.ProductType == (int)ProductType.ProjectFinance)
				{
					// 1.2
					IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
					var pdcInterestOutstanding = parm.CustTransCalcPDCInterestOutstanding.Select(s => new
					{
						WithdrawalTableGUID = s.Key,
						PDCInterestOutstanding = withdrawalTableService.CalcPDCInterestOutstanding(s.Value)
					});
					var comparePDCInterestOutstanding =
						(from productSettledTrans in parm.ProductSettledTrans
						 join withdrawalLine in parm.WithdrawalLine
						 on productSettledTrans.OriginalRefGUID equals withdrawalLine.WithdrawalLineGUID
						 join pdc in pdcInterestOutstanding
						 on withdrawalLine.WithdrawalTableGUID equals pdc.WithdrawalTableGUID
						 select new
						 {
							 VariablePDCInterestOutstanding = pdc.PDCInterestOutstanding,
							 PDCInterestOutstanding = productSettledTrans.PDCInterestOutstanding,
							 InvoiceSettlementDetailGUID = productSettledTrans.InvoiceSettlementDetailGUID
						 });
					if (comparePDCInterestOutstanding.Any(w => w.VariablePDCInterestOutstanding != w.PDCInterestOutstanding))
					{
						ex.AddData("ERROR.90106", "LABEL.PDC_INTEREST_OUTSTANDING", "LABEL.POST");
					}

					#region validate 1.6.1
					var withdrawalSumIntCalcAmountProdSettledTrans =
						(from productSettledTrans in parm.ProductSettledTrans
						 join sumInterestCalc in sumInterestCalcAmount
						 on productSettledTrans.OriginalRefGUID equals sumInterestCalc.OriginalRefGUID
						 join withdrawalLine in parm.WithdrawalLine
						 on productSettledTrans.RefGUID equals withdrawalLine.WithdrawalLineGUID
						 select new
						 {
							 SumInterestCalcAmount = sumInterestCalc.SumInterestCalcAmount,
							 productSettledTrans.PDCInterestOutstanding,
							 productSettledTrans.TotalRefundAdditionalIntAmount,
							 withdrawalLine.ClosedForTermExtension
						 });
					if (withdrawalSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount < 0 && !a.ClosedForTermExtension))
					{
						if (parm.CompanyParameter.PFInterestRefundInvTypeGUID.HasValue)
						{
							var pfInterestRefundInvType = parm.InvoiceType.Where(w => w.InvoiceTypeGUID == parm.CompanyParameter.PFInterestRefundInvTypeGUID.Value).FirstOrDefault();
							if (!pfInterestRefundInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
							{
								ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.PF_INTEREST_REFUND_INVOICE_TYPE_ID");
							}
						}
						else
						{
							ex.AddData("ERROR.90047", "LABEL.PF_INTEREST_REFUND_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
						}
					}
					if (withdrawalSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount > 0 && !a.ClosedForTermExtension))
					{
						if (parm.CompanyParameter.PFInterestInvTypeGUID.HasValue)
						{
							var pfInterestInvType = parm.InvoiceType.Where(w => w.InvoiceTypeGUID == parm.CompanyParameter.PFInterestInvTypeGUID.Value).FirstOrDefault();
							if (!pfInterestInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
							{
								ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.PF_INTEREST_INVOICE_TYPE_ID");
							}
						}
						else
						{
							ex.AddData("ERROR.90047", "LABEL.PF_INTEREST_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
						}
					}
					if (withdrawalSumIntCalcAmountProdSettledTrans.Any(a => a.SumInterestCalcAmount < 0 &&
																		 a.PDCInterestOutstanding > 0 &&
																		 a.TotalRefundAdditionalIntAmount <= 0 &&
																		 !a.ClosedForTermExtension))
					{
						if (!parm.CompanyParameter.SuspenseMethodOfPaymentGUID.HasValue)
						{
							ex.AddData("ERROR.90047", "LABEL.SUSPENSE_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER");
						}
						// 1.6.2.1
						if (parm.ReceiptTempNumberSeqTable == null || parm.ReceiptTempNumberSeqTable.Manual)
						{
							ex.AddData("ERROR.00576", parm.ReceiptTempNumberSeqTable.NumberSeqCode);
						}
					}
					#endregion
				}
				#endregion

				return ex;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		private GenInvoiceResultView CreateInvoice1LineSpecificForPostReceiptTemp(ReceiptTempTable receiptTempTable, Guid invoiceTypeGuid,
																					decimal invoiceAmount, InvoiceTable invoiceTable,
																					InvoiceSettlementDetail invoiceSettlementDetail,
																					bool refTypeBuyerInvoiceFromInvoice)
		{
			try
			{
				IInvoiceService invoiceService = new InvoiceService(db);
				Invoice1LineSpecificParamView sumInterestCalcInvoiceParam = new Invoice1LineSpecificParamView
				{
					RefType = refTypeBuyerInvoiceFromInvoice ? (RefType)invoiceTable.RefType : RefType.ReceiptTemp,
					RefGUID = refTypeBuyerInvoiceFromInvoice ? invoiceTable.RefGUID : receiptTempTable.ReceiptTempTableGUID,
					IssuedDate = receiptTempTable.TransDate,
					DueDate = receiptTempTable.TransDate,
					CustomerTableGUID = receiptTempTable.CustomerTableGUID,
					ProductType = (ProductType)receiptTempTable.ProductType,
					InvoiceTypeGUID = invoiceTypeGuid,
					AutoGenInvoiceRevenueType = true,
					InvoiceAmount = invoiceAmount,
					IncludeTax = true,
					CompanyGUID = receiptTempTable.CompanyGUID,
					InvoiceRevenueTypeGUID = null,
					DocumentId = invoiceTable != null ? invoiceTable.DocumentId : receiptTempTable.ReceiptTempId,
					CreditAppTableGUID = invoiceTable != null ? invoiceTable.CreditAppTableGUID : receiptTempTable.CreditAppTableGUID,
					BuyerTableGUID = null,
					Dimension1GUID = receiptTempTable.Dimension1GUID,
					Dimension2GUID = receiptTempTable.Dimension2GUID,
					Dimension3GUID = receiptTempTable.Dimension3GUID,
					Dimension4GUID = receiptTempTable.Dimension4GUID,
					Dimension5GUID = receiptTempTable.Dimension5GUID,
					MethodOfPaymentGUID = null,
					BuyerInvoiceTableGUID = refTypeBuyerInvoiceFromInvoice ? invoiceTable.BuyerInvoiceTableGUID : null,
					BuyerAgreementTableGUID = invoiceTable != null ? invoiceTable.BuyerAgreementTableGUID : null,
					ReceiptTempTableGUID = invoiceSettlementDetail != null ? invoiceSettlementDetail.RefGUID : null
				};
				var result = invoiceService.GenInvoice1LineSpecific(sumInterestCalcInvoiceParam);
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private ReceiptTempTable CreatePDCReceiptTempForPostReceiptTemp(ReceiptTempTable receiptTempTable, Guid receiptTempPostedStatusGUID,
																		ProductSettledTrans productSettledTrans)
		{
			try
			{
				ReceiptTempTable pdcReceiptTempTable = new ReceiptTempTable
				{
					ReceiptDate = receiptTempTable.TransDate,
					TransDate = receiptTempTable.TransDate,
					ProductType = receiptTempTable.ProductType,
					ReceivedFrom = (int)ReceivedFrom.Customer,
					CustomerTableGUID = receiptTempTable.CustomerTableGUID,
					BuyerTableGUID = null,
					BuyerReceiptTableGUID = null,
					DocumentStatusGUID = receiptTempPostedStatusGUID,
					DocumentReasonGUID = null,
					Remark = string.Empty,
					ReceiptAmount = productSettledTrans.PDCInterestOutstanding,
					SettleFeeAmount = 0,
					SettleAmount = productSettledTrans.PDCInterestOutstanding,
					CurrencyGUID = receiptTempTable.CurrencyGUID,
					ExchangeRate = receiptTempTable.ExchangeRate,
					SuspenseInvoiceTypeGUID = null,
					SuspenseAmount = 0,
					CreditAppTableGUID = null,
					ReceiptTempRefType = (int)ReceiptTempRefType.Product,
					RefReceiptTempGUID = null,
					MainReceiptTempGUID = receiptTempTable.ReceiptTempTableGUID,
					Dimension1GUID = receiptTempTable.Dimension1GUID,
					Dimension2GUID = receiptTempTable.Dimension2GUID,
					Dimension3GUID = receiptTempTable.Dimension3GUID,
					Dimension4GUID = receiptTempTable.Dimension4GUID,
					Dimension5GUID = receiptTempTable.Dimension5GUID,
					CompanyGUID = receiptTempTable.CompanyGUID,
					ReceiptTempTableGUID = Guid.NewGuid()
				};
				return pdcReceiptTempTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private InvoiceSettlementDetail CreatePDCInvoiceSettlementDetailReceiptTempPaymDetailForPostReceiptTemp(InvoiceTable invoiceTable, ProductSettledTrans productSettledTrans,
																												Guid receiptTempTableGuid, Guid receiptTempPaymDetailGuid)
		{
			try
			{
				InvoiceSettlementDetail pdcInvSettleDetailReceiptTmpPaymDetail = new InvoiceSettlementDetail
				{
					ProductType = invoiceTable.ProductType,
					DocumentId = invoiceTable.DocumentId,
					InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
					InvoiceDueDate = invoiceTable.DueDate,
					InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID,
					SuspenseInvoiceType = invoiceTable.SuspenseInvoiceType,
					InvoiceAmount = invoiceTable.InvoiceAmount,
					BalanceAmount = invoiceTable.InvoiceAmount,
					SettleAmount = productSettledTrans.PDCInterestOutstanding * (-1),
					WHTAmount = 0,
					SettleInvoiceAmount = productSettledTrans.PDCInterestOutstanding * (-1),
					SettleTaxAmount = 0,
					WHTSlipReceivedByCustomer = false,
					WHTSlipReceivedByBuyer = false,
					BuyerAgreementTableGUID = invoiceTable.BuyerAgreementTableGUID,
					AssignmentAgreementTableGUID = null,
					SettleAssignmentAmount = 0,
					PurchaseAmount = 0,
					PurchaseAmountBalance = 0,
					SettlePurchaseAmount = 0,
					SettleReserveAmount = 0,
					InterestCalculationMethod = (int)InterestCalculationMethod.Minimum,
					RetentionAmountAccum = 0,
					RefType = (int)RefType.ReceiptTemp,
					RefGUID = receiptTempTableGuid,
					RefReceiptTempPaymDetailGUID = receiptTempPaymDetailGuid,
					InvoiceSettlementDetailGUID = Guid.NewGuid(),
					CompanyGUID = invoiceTable.CompanyGUID
				};
				return pdcInvSettleDetailReceiptTmpPaymDetail;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private List<AssignmentAgreementSettle> CreateAssignmentAgreementSettleForPostReceiptTemp(ReceiptTempTable receiptTempTable,
																							IEnumerable<InvoiceSettlementDetail> invoiceSettlementDetail)
		{
			try
			{
				List<AssignmentAgreementSettle> result = invoiceSettlementDetail.Select(s => new AssignmentAgreementSettle
				{
					RefType = (int)RefType.ReceiptTemp,
					RefGUID = receiptTempTable.ReceiptTempTableGUID,
					RefAssignmentAgreementSettleGUID = null,
					AssignmentAgreementTableGUID = s.AssignmentAgreementTableGUID.Value,
					SettledDate = receiptTempTable.TransDate,
					SettledAmount = s.SettleAssignmentAmount,
					BuyerAgreementTableGUID = s.BuyerAgreementTableGUID,
					DocumentReasonGUID = null,
					InvoiceTableGUID = s.InvoiceTableGUID,
					DocumentId = s.DocumentId,
					CompanyGUID = receiptTempTable.CompanyGUID,
					AssignmentAgreementSettleGUID = Guid.NewGuid(),
				}).ToList();
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private BuyerReceiptTable CreateBuyerReceiptTableForPostReceiptTemp(ReceiptTempTable receiptTempTable,
																			ReceiptTempPaymDetail receiptTempPaymDetail)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
				Guid buyerTableGuid = receiptTempTable.BuyerTableGUID.HasValue ? receiptTempTable.BuyerTableGUID.Value : Guid.Empty;
				AddressTrans buyerReceiptAddress = addressTransRepo.GetPrimaryAddressByReference(RefType.Buyer, buyerTableGuid);

				ChequeTable chequeTable = receiptTempPaymDetail != null && receiptTempPaymDetail.ChequeTableGUID.HasValue ?
					chequeTableRepo.GetChequeTableByIdNoTracking(receiptTempPaymDetail.ChequeTableGUID.Value) : null;

				BuyerReceiptTable result = new BuyerReceiptTable
				{
					BuyerReceiptDate = receiptTempTable.ReceiptDate,
					BuyerTableGUID = buyerTableGuid,
					CustomerTableGUID = receiptTempTable.CustomerTableGUID,
					BuyerReceiptAmount = receiptTempTable.ReceiptAmount,
					Cancel = false,
					BuyerReceiptAddress = buyerReceiptAddress != null ?
						string.Concat(buyerReceiptAddress.Address1, " ", buyerReceiptAddress.Address2).Trim() : string.Empty,
					AssignmentAgreementTableGUID = null,
					BuyerAgreementTableGUID = null,
					Remark = string.Empty,
					ShowRemarkOnly = false,
					MethodOfPaymentGUID = receiptTempPaymDetail != null ? (Guid?)receiptTempPaymDetail.MethodOfPaymentGUID : null,
					ChequeNo = chequeTable != null ? chequeTable.ChequeNo : null,
					ChequeDate = chequeTable != null ? (DateTime?)chequeTable.ChequeDate : null,
					RefType = (int)RefType.ReceiptTemp,
					RefGUID = receiptTempTable.ReceiptTempTableGUID,
					BuyerReceiptTableGUID = Guid.NewGuid(),
					CompanyGUID = receiptTempTable.CompanyGUID
				};
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Report
		public PrintReceiptTemp GetPrintReceiptTempTableById(string receiptTempTableGUID)
        {
            try
            {
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				ReceiptTempTableItemView receiptTempTable = receiptTempTableRepo.GetByIdvw(receiptTempTableGUID.StringToGuid());

				PrintReceiptTemp printReceiptTemp = new PrintReceiptTemp
				{
					ReceiptTempTableGUID = receiptTempTable.ReceiptTempTableGUID,
					ReceiptTempId = receiptTempTable.ReceiptTempId,
					ReceiptDate = receiptTempTable.ReceiptDate,
					TransDate = receiptTempTable.TransDate,
					ProductType = receiptTempTable.ProductType,
					ReceivedFrom = receiptTempTable.ReceivedFrom,
					CustomerId =receiptTempTable.CustomerTable_Values,
					BuyerId = receiptTempTable.BuyerTable_Values,
					ReceiptAmount = receiptTempTable.ReceiptAmount,
					SuspenseInvoiceTypeGUID = receiptTempTable.SuspenseInvoiceTypeGUID,
					SuspenseInvoiceType_Values = receiptTempTable.SuspenseInvoiceType_Values,
					SettleFeeAmount = receiptTempTable.SettleFeeAmount,
					SettleAmount = receiptTempTable.SettleAmount,
					SuspenseAmount = receiptTempTable.SuspenseAmount,
					CreditAppTableGUID = receiptTempTable.CreditAppTableGUID,
					CreditAppTable_Values = receiptTempTable.CreditAppTable_Values
				};
				return printReceiptTemp;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
        #endregion
    }
}
