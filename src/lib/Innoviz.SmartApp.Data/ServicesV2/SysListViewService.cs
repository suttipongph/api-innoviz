using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ISysListViewService
    {
        SearchResult<AddressDistrictListView> GetAddressDistrictListvw(SearchParameter search);
        SearchResult<RelatedPersonTableListView> GetRelatedPersonTableListvw(SearchParameter search);
        SearchResult<AssignmentMethodListView> GetAssignmentMethodListvw(SearchParameter search);
        SearchResult<AssignmentAgreementTableListView> GetAssignmentAgreementTableListvw(SearchParameter search);
        SearchResult<AuthorizedPersonTypeListView> GetAuthorizedPersonTypeListvw(SearchParameter search);
        SearchResult<BankGroupListView> GetBankGroupListvw(SearchParameter search);
        SearchResult<BankTypeListView> GetBankTypeListvw(SearchParameter search);
        SearchResult<BillingResponsibleByListView> GetBillingResponsibleByListvw(SearchParameter search);
        SearchResult<BlacklistStatusListView> GetBlacklistStatusListvw(SearchParameter search);
        SearchResult<BusinessCollateralStatusListView> GetBusinessCollateralStatusListvw(SearchParameter search);
        SearchResult<BusinessCollateralSubTypeListView> GetBusinessCollateralSubTypeListvw(SearchParameter search);
        SearchResult<BusinessCollateralTypeListView> GetBusinessCollateralTypeListvw(SearchParameter search);
        SearchResult<BusinessSegmentListView> GetBusinessSegmentListvw(SearchParameter search);
        SearchResult<BusinessSizeListView> GetBusinessSizeListvw(SearchParameter search);
        SearchResult<BusinessTypeListView> GetBusinessTypeListvw(SearchParameter search);
        SearchResult<CompanyBankListView> GetCompanyBankListvw(SearchParameter search);
        SearchResult<CreditLimitTypeListView> GetCreditLimitTypeListvw(SearchParameter search);
        SearchResult<CreditTypeListView> GetCreditTypeListvw(SearchParameter search);
        SearchResult<CreditScoringListView> GetCreditScoringListvw(SearchParameter search);
        SearchResult<CustGroupListView> GetCustGroupListvw(SearchParameter search);
        SearchResult<DepartmentListView> GetDepartmentListvw(SearchParameter search);
        SearchResult<DocumentTypeListView> GetDocumentTypeListvw(SearchParameter search);
        SearchResult<GradeClassificationListView> GetGradeClassificationListvw(SearchParameter search);
        SearchResult<ProductSubTypeListView> GetProductSubTypeListvw(SearchParameter search);
        SearchResult<RegistrationTypeListView> GetRegistrationTypeListvw(SearchParameter search);
        SearchResult<InterestTypeListView> GetInterestTypeListvw(SearchParameter search);
        SearchResult<InterestTypeValueListView> GetInterestTypeValueListvw(SearchParameter search);
        SearchResult<GuarantorTypeListView> GetGuarantorTypeListvw(SearchParameter search);
        SearchResult<IntercompanyListView> GetIntercompanyListvw(SearchParameter search);
        SearchResult<NationalityListView> GetNationalityListvw(SearchParameter search);
        SearchResult<KYCSetupListView> GetKYCSetupListvw(SearchParameter search);
        SearchResult<RaceListView> GetRaceListvw(SearchParameter search);
        SearchResult<VerificationTypeListView> GetVerificationTypeListvw(SearchParameter search);
        SearchResult<CreditTermListView> GetCreditTermListvw(SearchParameter search);
        SearchResult<BuyerTableListView> GetBuyerTableListvw(SearchParameter search);
        SearchResult<ContactPersonTransListView> GetContactPersonTransListvw(SearchParameter search);
        SearchResult<PropertyTypeListView> GetPropertyTypeListvw(SearchParameter search);
        SearchResult<VendorTableListView> GetVendorTableListvw(SearchParameter search);
        SearchResult<CustomerTableListView> GetCustomerTableListvw(SearchParameter search);
        SearchResult<ExposureGroupListView> GetExposureGroupListvw(SearchParameter search);
        SearchResult<GuarantorTransListView> GetGuarantorTransListvw(SearchParameter search);
        SearchResult<JointVentureTransListView> GetJointVentureTransListvw(SearchParameter search);
        SearchResult<VendGroupListView> GetVendGroupListvw(SearchParameter search);
        SearchResult<MemoTransListView> GetMemoTransListvw(SearchParameter search);
        SearchResult<DocumentStatusListView> GetDocumentStatusListvw(SearchParameter search);
        SearchResult<DocumentReasonListView> GetDocumentReasonListvw(SearchParameter search);
        SearchResult<DocumentProcessListView> GetDocumentProcessListvw(SearchParameter search);
        SearchResult<BuyerCreditLimitByProductListView> GetBuyerCreditLimitByProductListvw(SearchParameter search);
        SearchResult<LineOfBusinessListView> GetLineOfBusinessListvw(SearchParameter search);
        SearchResult<CustVisitingTransListView> GetCustVisitingTransListvw(SearchParameter search);
        SearchResult<ContactTransListView> GetContactTransListvw(SearchParameter search);
        SearchResult<MaritalStatusListView> GetMaritalStatusListvw(SearchParameter search);
        SearchResult<OwnerTransListView> GetOwnerTransListvw(SearchParameter search);
        SearchResult<GenderListView> GetGenderListvw(SearchParameter search);
        SearchResult<OccupationListView> GetOccupationListvw(SearchParameter search);
        SearchResult<AddressCountryListView> GetAddressCountryListvw(SearchParameter search);
        SearchResult<AddressPostalCodeListView> GetAddressPostalCodeListvw(SearchParameter search);
        SearchResult<AddressProvinceListView> GetAddressProvinceListvw(SearchParameter search);
        SearchResult<AuthorizedPersonTransListView> GetAuthorizedPersonTransListvw(SearchParameter search);
        SearchResult<AddressSubDistrictListView> GetAddressSubDistrictListvw(SearchParameter search);
        SearchResult<TaxTableListView> GetTaxTableListvw(SearchParameter search);
        SearchResult<TaxValueListView> GetTaxValueListvw(SearchParameter search);
        SearchResult<CurrencyListView> GetCurrencyListvw(SearchParameter search);
        SearchResult<ExchangeRateListView> GetExchangeRateListvw(SearchParameter search);
        SearchResult<WithholdingTaxGroupListView> GetWithholdingTaxGroupListvw(SearchParameter search);
        SearchResult<IntroducedByListView> GetIntroducedByListvw(SearchParameter search);
        SearchResult<ProjectReferenceTransListView> GetProjectReferenceTransListvw(SearchParameter search);
        SearchResult<TerritoryListView> GetTerritoryListvw(SearchParameter search);
        SearchResult<BuyerAgreementLineListView> GetBuyerAgreementLineListvw(SearchParameter search);
        SearchResult<BuyerAgreementTableListView> GetBuyerAgreementTableListvw(SearchParameter search);
        SearchResult<NCBAccountStatusListView> GetNCBAccountStatusListvw(SearchParameter search);
        SearchResult<CustomerCreditLimitByProductListView> GetCustomerCreditLimitByProductListvw(SearchParameter search);
        SearchResult<EmployeeTableListView> GetEmployeeTableListvw(SearchParameter search);
        SearchResult<EmplTeamListView> GetEmplTeamListvw(SearchParameter search);
        SearchResult<LedgerDimensionListView> GetLedgerDimensionListvw(SearchParameter search);
        SearchResult<RetentionConditionSetupListView> GetRetentionConditionSetupListvw(SearchParameter search);
        SearchResult<NumberSeqTableListView> GetNumberSeqTableListvw(SearchParameter search);
        SearchResult<NumberSeqSegmentListView> GetNumberSeqSegmentListvw(SearchParameter search);

        SearchResult<MethodOfPaymentListView> GetMethodOfPaymentListvw(SearchParameter search);
        SearchResult<AddressTransListView> GetAddressTransListvw(SearchParameter search);
        SearchResult<OwnershipListView> GetOwnershipListvw(SearchParameter search);
        SearchResult<ParentCompanyListView> GetParentCompanyListvw(SearchParameter search);
        SearchResult<BookmarkDocumentListView> GetBookmarkDocumentListvw(SearchParameter search);
        SearchResult<ServiceFeeCondTemplateLineListView> GetServiceFeeCondTemplateLineListvw(SearchParameter search);
        SearchResult<ServiceFeeCondTemplateTableListView> GetServiceFeeCondTemplateTableListvw(SearchParameter search);
        SearchResult<InvoiceRevenueTypeListView> GetInvoiceRevenueTypeListvw(SearchParameter search);
        SearchResult<DocumentConditionTransListView> GetDocumentConditionTransListvw(SearchParameter search);
        SearchResult<CreditAppRequestLineListView> GetCreditAppRequestLineListvw(SearchParameter search);
        SearchResult<CreditAppRequestTableListView> GetCreditAppRequestTableListvw(SearchParameter search);
        SearchResult<CreditAppRequestTableListView> GetReviewCreditAppRequestListvw(SearchParameter search);
        SearchResult<CreditAppRequestTableListView> GetClosingCreditAppRequestListvw(SearchParameter search);
        SearchResult<ConsortiumTableListView> GetConsortiumTableListvw(SearchParameter search);
        SearchResult<ConsortiumLineListView> GetConsortiumLineListvw(SearchParameter search);
        SearchResult<FinancialStatementTransListView> GetFinancialStatementTransListvw(SearchParameter search);
        SearchResult<TaxReportTransListView> GetTaxReportTransListvw(SearchParameter search);
        SearchResult<WithholdingTaxTableListView> GetWithholdingTaxTableListvw(SearchParameter search);
        SearchResult<WithholdingTaxValueListView> GetWithholdingTaxValueListvw(SearchParameter search);
        SearchResult<CompanyListView> GetCompanyListvw(SearchParameter search);
        SearchResult<CustBankListView> GetCustBankListvw(SearchParameter search);
        SearchResult<DocumentConditionTemplateLineListView> GetDocumentConditionTemplateLineListvw(SearchParameter search);
        SearchResult<DocumentConditionTemplateTableListView> GetDocumentConditionTemplateTableListvw(SearchParameter search);
        SearchResult<VendBankListView> GetVendBankListvw(SearchParameter search);
        SearchResult<BusinessUnitListView> GetBusinessUnitListvw(SearchParameter search);
        SearchResult<ExposureGroupByProductListView> GetExposureGroupByProductListvw(SearchParameter search);
        SearchResult<NumberSeqParameterListView> GetNumberSeqParameterListvw(SearchParameter search);
        SearchResult<NumberSeqSetupByProductTypeListView> GetNumberSeqSetupByProductTypeListvw(SearchParameter search);
        SearchResult<CalendarGroupListView> GetCalendarGroupListvw(SearchParameter search);
        SearchResult<NCBTransListView> GetNCBTransListvw(SearchParameter search);
        SearchResult<ServiceFeeConditionTransListView> GetServiceFeeConditionTransListvw(SearchParameter search);
        SearchResult<CalendarNonWorkingDateListView> GetCalendarNonWorkingDateListvw(SearchParameter search);
        SearchResult<RetentionConditionTransListView> GetRetentionConditionTransListvw(SearchParameter search);
        SearchResult<CreditAppTableListView> GetCreditAppTableListvw(SearchParameter search);
        SearchResult<CreditAppLineListView> GetCreditAppLineListvw(SearchParameter search);
        SearchResult<DocumentTemplateTableListView> GetDocumentTemplateTableListvw(SearchParameter search);
        SearchResult<BuyerAgreementTransListView> GetBuyerAgreementTransListvw(SearchParameter search);
        SearchResult<FinancialCreditTransListView> GetFinancialCreditTransListvw(SearchParameter search);
        SearchResult<CompanyParameterListView> GetCompanyParameterListvw(SearchParameter search);
        SearchResult<CompanySignatureListView> GetCompanySignatureListvw(SearchParameter search);
        SearchResult<CustBusinessCollateralListView> GetCustBusinessCollateralListvw(SearchParameter search);
        SearchResult<MainAgreementTableListView> GetMainAgreementTableListvw(SearchParameter search);
        SearchResult<BookmarkDocumentTemplateLineListView> GetBookmarkDocumentTemplateLineListvw(SearchParameter search);
        SearchResult<BookmarkDocumentTemplateTableListView> GetBookmarkDocumentTemplateTableListvw(SearchParameter search);
        SearchResult<InvoiceNumberSeqSetupListView> GetInvoiceNumberSeqSetupListvw(SearchParameter search);
        SearchResult<LedgerFiscalYearListView> GetLedgerFiscalYearListvw(SearchParameter search);
        SearchResult<LedgerFiscalPeriodListView> GetLedgerFiscalPeriodListvw(SearchParameter search);
        SearchResult<ProdUnitListView> GetProdUnitListListvw(SearchParameter search);
        SearchResult<CreditAppReqBusinessCollateralListView> GetCreditAppReqBusinessCollateralListvw(SearchParameter search);
        SearchResult<BookmarkDocumentTransListView> GetBookmarkDocumentTransListvw(SearchParameter search);
        SearchResult<CreditAppRequestTableAmendListView> GetCreditAppRequestTableAmendListvw(SearchParameter search);
        SearchResult<AgreementTableInfoListView> GetAgreementTableInfoListvw(SearchParameter search);
        SearchResult<AssignmentAgreementLineListView> GetAssignmentAgreementLineListvw(SearchParameter search);
        SearchResult<InvoiceTypeListView> GetInvoiceTypeListvw(SearchParameter search);
        SearchResult<BuyerInvoiceTableListView> GetBuyerInvoiceTableListvw(SearchParameter search);
        SearchResult<PurchaseLineListView> GetPurchaseLineListvw(SearchParameter search);
        SearchResult<PurchaseTableListView> GetPurchaseTableListvw(SearchParameter search);
        SearchResult<ConsortiumTransListView> GetConsortiumTransListvw(SearchParameter search);
        SearchResult<SysUserTableListView> GetSysUserTableListvw(SearchParameter search);

        SearchResult<ChequeTableListView> GetChequeTableListvw(SearchParameter search);

        SearchResult<ChequeTableListView> GetChequeTableListvwByLine(SearchParameter search);
        SearchResult<GuarantorAgreementTableListView> GetGuarantorAgreementTableListvw(SearchParameter search);
        SearchResult<GuarantorAgreementLineListView> GetGuarantorAgreementLineListvw(SearchParameter search);
        SearchResult<GuarantorAgreementLineAffiliateListView> GetGuarantorAgreementLineAffiliateListvw(SearchParameter search);
        SearchResult<BusinessCollateralAgmTableListView> GetBusinessCollateralAgmTableListvw(SearchParameter search);
        SearchResult<BusinessCollateralAgmLineListView> GetBusinessCollateralAgmLineListvw(SearchParameter search);
        SearchResult<JobTypeListView> GetJobTypeListvw(SearchParameter search);
        SearchResult<AttachmentListView> GetAttachmentListvw(SearchParameter search);
        SearchResult<InvoiceSettlementDetailListView> GetInvoiceSettlementDetailListvw(SearchParameter search);
        SearchResult<ServiceFeeTransListView> GetServiceFeeTransListvw(SearchParameter search);
        SearchResult<AssignmentAgreementSettleListView> GetAssignmentAgreementSettleListvw(SearchParameter search);
        SearchResult<VerificationLineListView> GetVerificationLineListvw(SearchParameter search);
        SearchResult<VerificationTableListView> GetVerificationTableListvw(SearchParameter search);
        SearchResult<VendorPaymentTransListView> GetVendorPaymentTransListvw(SearchParameter search);
        SearchResult<CustTransListView> GetCustTransListvw(SearchParameter search);
        SearchResult<PaymentDetailListView> GetPaymentDetailListvw(SearchParameter search);
        SearchResult<ProcessTransListView> GetProcessTransactionListvw(SearchParameter search);
        SearchResult<InvoiceTableListView> GetInvoiceTableListvw(SearchParameter search);
        SearchResult<InvoiceLineListView> GetInvoiceLineListvw(SearchParameter search);
        SearchResult<CreditAppTransListView> GetCreditApplicationTransListvw(SearchParameter search);
        SearchResult<InquiryPurchaseLineOutstandingListView> GetInquiryPurchaseLineOutstandingListvw(SearchParameter search);
        SearchResult<VerificationTransListView> GetVerificationTransListvw(SearchParameter search);
        SearchResult<CreditAppRequestLineAmendListView> GetCreditAppRequestLineAmendListvw(SearchParameter search);
        SearchResult<CollectionFollowUpListView> GetCollectionFollowUpListvw(SearchParameter search);
        SearchResult<MessengerTableListView> GetMessengerTableListvw(SearchParameter search);
        SearchResult<BranchListView> GetBranchListvw(SearchParameter search);
        SearchResult<ReceiptTableListView> GetReceiptTableListvw(SearchParameter search);
        SearchResult<ReceiptLineListView> GetReceiptLineListvw(SearchParameter search);
        SearchResult<InterestRealizedTransListView> GetInterestRealizedTransListvw(SearchParameter search);
        SearchResult<TaxInvoiceTableListView> GetTaxInvoiceTableListvw(SearchParameter search);
        SearchResult<TaxInvoiceLineListView> GetTaxInvoiceLineListvw(SearchParameter search);
        SearchResult<MessengerJobTableListView> GetMessengerJobTableListvw(SearchParameter search);
        SearchResult<ListPurchaseLineOutstandingListView> GetListPurchaseLineOutstandingListvw(SearchParameter search);
        SearchResult<BuyerReceiptTableListView> GetBuyerRecepiptTableListvw(SearchParameter search);
        SearchResult<WithdrawalTableListView> GetWithdrawalTableListvw(SearchParameter search);
        SearchResult<WithdrawalLineListView> GetWithdrawalLineListvw(SearchParameter search);
        SearchResult<InquiryWithdrawalLineOutstandListView> GetInquiryWithdrawalLineOutstandListvw(SearchParameter search);
        SearchResult<RetentionTransListView> GetRetentionTransListvw(SearchParameter search);
        SearchResult<PaymentHistoryListView> GetPaymentHistoryListvw(SearchParameter search);
        SearchResult<JobChequeListView> GetJobChequeListvw(SearchParameter search);
        SearchResult<ProjectProgressTableListView> GetProjectProgressListvw(SearchParameter search);
        SearchResult<InvoiceOutstandingListView> GetInvoiceOutstandingListvw(SearchParameter search);
        SearchResult<InvoiceOutstandingListView> GetSuspenseOutstandingListByCompany(SearchParameter search);
        SearchResult<CAReqRetentionOutstandingListView> GetRetentionOutstandingListvw(SearchParameter search);
        SearchResult<AddressTransListView> GetAdressTransListvw(SearchParameter search);
        SearchResult<ReceiptTempTableListView> GetReceiptTempTableListvw(SearchParameter search);
        SearchResult<ReceiptTempPaymDetailListView> GetReceiptTempPaymDetailListvw(SearchParameter search);
        SearchResult<CAReqAssignmentOutstandingListView> GetAssignmentOutstandingListvw(SearchParameter search);
        SearchResult<SysRoleTableListView> GetSysRoleTableListvw(SearchParameter search);
        SearchResult<CAReqCreditOutStandingListView> GetCreditOutStandingListvw(SearchParameter search);
        SearchResult<ProductSettledTransListView> GetProductSettledTransListvw(SearchParameter search);
        SearchResult<ProductSettledTransListView> GetProductSettledTransListvwByWithdrawal(SearchParameter search);
        SearchResult<MigrationTableListView> GetMigrationTableListvw(SearchParameter search);
        SearchResult<MigrationLogTableView> GetMigrationLogTableListvw(SearchParameter search);
        SearchResult<CustomerRefundTableListView> GetCustomerRefundTableListvw(SearchParameter search);
        SearchResult<InquiryRollbillPurchaseLineListView> GetInquiryRollBillPurchaseLineListvw(SearchParameter search);
        SearchResult<CreditAppReqAssignmentListView> GetCreditAppReqAssignmentListvw(SearchParameter search);
        SearchResult<StagingTableListView> GetStagingTableListvw(SearchParameter search);
        SearchResult<DocumentReturnMethodListView> GetDocumentReturnMethodListvw(SearchParameter search);
        SearchResult<StagingTableVendorInfoListView> GetStagingTableVendorInfoListvw(SearchParameter search);
        SearchResult<StagingTransTextListView> GetStagingTransTextListvw(SearchParameter search);
        SearchResult<DocumentReturnLineListView> GetDocumentReturnLineListvw(SearchParameter search);
        SearchResult<DocumentReturnTableListView> GetDocumentReturnTableListvw(SearchParameter search);
        SearchResult<CAReqBuyerCreditOutstandingListView> GetCAReqBuyerCreditOutstandingListvw(SearchParameter search);
        SearchResult<CreditOutstandingView> GetInquiryCreditOutstandingListByCustvw(SearchParameter search);
        SearchResult<IntercompanyInvoiceTableListView> GetIntercompanyInvoiceTableListvw(SearchParameter search);
        SearchResult<FreeTextInvoiceTableListView> GetFreeTextInvoiceTableListvw(SearchParameter search);
        SearchResult<IntercompanyInvoiceAdjustmentListView> GetIntercompanyInvoiceAdjustmentListvw(SearchParameter search);
        SearchResult<CALineOutstandingView> GetGetCALineOutstandingByCA(SearchParameter search);
        SearchResult<StagingTableIntercoInvSettleListView> GetStagingTableIntercoInvSettleListvw(SearchParameter search);
        SearchResult<IntercompanyInvoiceSettlementListView> GetIntercompanyInvoiceSettlementListvw(SearchParameter search);
  
    }
    public class SysListViewService : SmartAppService, ISysListViewService
    {
        public SysListViewService(SmartAppDbContext context) : base(context) { }
        public SysListViewService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public SysListViewService() : base() { }


        #region NumberSeqParameter
        public SearchResult<NumberSeqParameterListView> GetNumberSeqParameterListvw(SearchParameter search)
        {
            try
            {
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                return numberSeqParameterRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region NumberSeqSegment
        public SearchResult<NumberSeqSegmentListView> GetNumberSeqSegmentListvw(SearchParameter search)
        {
            try
            {
                INumberSeqSegmentRepo numberSeqSegmentRepo = new NumberSeqSegmentRepo(db);
                return numberSeqSegmentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region NumberSeqTable
        public SearchResult<NumberSeqTableListView> GetNumberSeqTableListvw(SearchParameter search)
        {
            try
            {
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                return numberSeqTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AddressProvince
        public SearchResult<AddressProvinceListView> GetAddressProvinceListvw(SearchParameter search)
        {
            try
            {
                IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
                return addressProvinceRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AddressDistrict
        public SearchResult<AddressDistrictListView> GetAddressDistrictListvw(SearchParameter search)
        {
            try
            {
                IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
                return addressDistrictRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AddressSubDistrict
        public SearchResult<AddressSubDistrictListView> GetAddressSubDistrictListvw(SearchParameter search)
        {
            try
            {
                IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
                return addressSubDistrictRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Currency
        public SearchResult<CurrencyListView> GetCurrencyListvw(SearchParameter search)
        {
            try
            {
                ICurrencyRepo currencyRepo = new CurrencyRepo(db);
                return currencyRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ExchangeRate
        public SearchResult<ExchangeRateListView> GetExchangeRateListvw(SearchParameter search)
        {
            try
            {
                IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
                return exchangeRateRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region RelatedPersonTable
        public SearchResult<LineOfBusinessListView> GetLineOfBusinessListvw(SearchParameter search)
        {
            try
            {
                ILineOfBusinessRepo lineOfBusinessRepo = new LineOfBusinessRepo(db);
                return lineOfBusinessRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region RelatedPersonTable
        public SearchResult<RelatedPersonTableListView> GetRelatedPersonTableListvw(SearchParameter search)
        {
            try
            {
                IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
                return relatedPersonTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AssignmentMethod
        public SearchResult<AssignmentMethodListView> GetAssignmentMethodListvw(SearchParameter search)
        {
            try
            {
                IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
                return assignmentMethodRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AssignmentAgreementTable
        public SearchResult<AssignmentAgreementTableListView> GetAssignmentAgreementTableListvw(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                return assignmentAgreementTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion        
        #region AuthorizedPersonType
        public SearchResult<AuthorizedPersonTypeListView> GetAuthorizedPersonTypeListvw(SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTypeRepo authorizedPersonTypeRepo = new AuthorizedPersonTypeRepo(db);
                return authorizedPersonTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BankGroup
        public SearchResult<BankGroupListView> GetBankGroupListvw(SearchParameter search)
        {
            try
            {
                IBankGroupRepo bankGroupRepo = new BankGroupRepo(db);
                return bankGroupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BankType
        public SearchResult<BankTypeListView> GetBankTypeListvw(SearchParameter search)
        {
            try
            {
                IBankTypeRepo bankTypeRepo = new BankTypeRepo(db);
                return bankTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BillingResponsibleBy
        public SearchResult<BillingResponsibleByListView> GetBillingResponsibleByListvw(SearchParameter search)
        {
            try
            {
                IBillingResponsibleByRepo billingResponsibleByRepo = new BillingResponsibleByRepo(db);
                return billingResponsibleByRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BlacklistStatus
        public SearchResult<BlacklistStatusListView> GetBlacklistStatusListvw(SearchParameter search)
        {
            try
            {
                IBlacklistStatusRepo blacklistStatusRepo = new BlacklistStatusRepo(db);
                return blacklistStatusRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BookmarkDocument
        public SearchResult<BookmarkDocumentListView> GetBookmarkDocumentListvw(SearchParameter search)
        {
            try
            {
                IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
                return bookmarkDocumentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BusinessCollateralStatus
        public SearchResult<BusinessCollateralStatusListView> GetBusinessCollateralStatusListvw(SearchParameter search)
        {
            try
            {
                IBusinessCollateralStatusRepo businessCollateralStatusRepo = new BusinessCollateralStatusRepo(db);
                return businessCollateralStatusRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BusinessCollateralSubType
        public SearchResult<BusinessCollateralSubTypeListView> GetBusinessCollateralSubTypeListvw(SearchParameter search)
        {
            try
            {
                IBusinessCollateralSubTypeRepo businessCollateralSubTypeRepo = new BusinessCollateralSubTypeRepo(db);
                return businessCollateralSubTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BusinessCollateralType
        public SearchResult<BusinessCollateralTypeListView> GetBusinessCollateralTypeListvw(SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeRepo businessCollateralTypeRepo = new BusinessCollateralTypeRepo(db);
                return businessCollateralTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BusinessSegment
        public SearchResult<BusinessSegmentListView> GetBusinessSegmentListvw(SearchParameter search)
        {
            try
            {
                IBusinessSegmentRepo businessSegmentRepo = new BusinessSegmentRepo(db);
                return businessSegmentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion		
        #region BusinessSize
        public SearchResult<BusinessSizeListView> GetBusinessSizeListvw(SearchParameter search)
        {
            try
            {
                IBusinessSizeRepo businessSizeRepo = new BusinessSizeRepo(db);
                return businessSizeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BusinessType
        public SearchResult<BusinessTypeListView> GetBusinessTypeListvw(SearchParameter search)
        {
            try
            {
                IBusinessTypeRepo businessTypeRepo = new BusinessTypeRepo(db);
                return businessTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CompanyBank
        public SearchResult<CompanyBankListView> GetCompanyBankListvw(SearchParameter search)
        {
            try
            {
                ICompanyBankRepo companyBankRepo = new CompanyBankRepo(db);
                return companyBankRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditLimitType
        public SearchResult<CreditLimitTypeListView> GetCreditLimitTypeListvw(SearchParameter search)
        {
            try
            {
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                return creditLimitTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditType
        public SearchResult<CreditTypeListView> GetCreditTypeListvw(SearchParameter search)
        {
            try
            {
                ICreditTypeRepo creditTypeRepo = new CreditTypeRepo(db);
                return creditTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditScoring
        public SearchResult<CreditScoringListView> GetCreditScoringListvw(SearchParameter search)
        {
            try
            {
                ICreditScoringRepo creditScoringRepo = new CreditScoringRepo(db);
                return creditScoringRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CustGroup
        public SearchResult<CustGroupListView> GetCustGroupListvw(SearchParameter search)
        {
            try
            {
                ICustGroupRepo custGroupRepo = new CustGroupRepo(db);
                return custGroupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Department
        public SearchResult<DepartmentListView> GetDepartmentListvw(SearchParameter search)
        {
            try
            {
                IDepartmentRepo departmentRepo = new DepartmentRepo(db);
                return departmentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentType
        public SearchResult<DocumentTypeListView> GetDocumentTypeListvw(SearchParameter search)
        {
            try
            {
                IDocumentTypeRepo documentTypeRepo = new DocumentTypeRepo(db);
                return documentTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentReason
        public SearchResult<DocumentReasonListView> GetDocumentReasonListvw(SearchParameter search)
        {
            try
            {
                IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
                return documentReasonRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region GradeClassification
        public SearchResult<GradeClassificationListView> GetGradeClassificationListvw(SearchParameter search)
        {
            try
            {
                IGradeClassificationRepo gradeClassificationRepo = new GradeClassificationRepo(db);
                return gradeClassificationRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region GuarantorType
        public SearchResult<GuarantorTypeListView> GetGuarantorTypeListvw(SearchParameter search)
        {
            try
            {
                IGuarantorTypeRepo guarantorTypeRepo = new GuarantorTypeRepo(db);
                return guarantorTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AddressCountry
        public SearchResult<AddressCountryListView> GetAddressCountryListvw(SearchParameter search)
        {
            try
            {
                IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
                return addressCountryRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AddressPostalCode
        public SearchResult<AddressPostalCodeListView> GetAddressPostalCodeListvw(SearchParameter search)
        {
            try
            {
                IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
                return addressPostalCodeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Intercompany
        public SearchResult<IntercompanyListView> GetIntercompanyListvw(SearchParameter search)
        {
            try
            {
                IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
                return intercompanyRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Nationality
        public SearchResult<NationalityListView> GetNationalityListvw(SearchParameter search)
        {
            try
            {
                INationalityRepo nationalityRepo = new NationalityRepo(db);
                return nationalityRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region KYCSetup
        public SearchResult<KYCSetupListView> GetKYCSetupListvw(SearchParameter search)
        {
            try
            {
                IKYCSetupRepo kycSetupRepo = new KYCSetupRepo(db);
                return kycSetupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ProductSubType
        public SearchResult<ProductSubTypeListView> GetProductSubTypeListvw(SearchParameter search)
        {
            try
            {
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                return productSubTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Race
        public SearchResult<RaceListView> GetRaceListvw(SearchParameter search)
        {
            try
            {
                IRaceRepo raceRepo = new RaceRepo(db);
                return raceRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region RegistrationType
        public SearchResult<RegistrationTypeListView> GetRegistrationTypeListvw(SearchParameter search)
        {
            try
            {
                IRegistrationTypeRepo registrationTypeRepo = new RegistrationTypeRepo(db);
                return registrationTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InterestType
        public SearchResult<InterestTypeListView> GetInterestTypeListvw(SearchParameter search)
        {
            try
            {
                IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
                return interestTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion InterestType
        #region TaxValue
        public SearchResult<IntroducedByListView> GetIntroducedByListvw(SearchParameter search)
        {
            try
            {
                IIntroducedByRepo introducedByRepo = new IntroducedByRepo(db);
                return introducedByRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region VerificationType
        public SearchResult<VerificationTypeListView> GetVerificationTypeListvw(SearchParameter search)
        {
            try
            {
                IVerificationTypeRepo verificationTypeRepo = new VerificationTypeRepo(db);
                return verificationTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InterestTypeValue
        public SearchResult<InterestTypeValueListView> GetInterestTypeValueListvw(SearchParameter search)
        {
            try
            {
                IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
                return interestTypeValueRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditTerm
        public SearchResult<CreditTermListView> GetCreditTermListvw(SearchParameter search)
        {
            try
            {
                ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
                return creditTermRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BuyerTable
        public SearchResult<BuyerTableListView> GetBuyerTableListvw(SearchParameter search)
        {
            try
            {
                IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
                return buyerTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ContactPersonTrans
        public SearchResult<ContactPersonTransListView> GetContactPersonTransListvw(SearchParameter search)
        {
            try
            {
                IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
                return contactPersonTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region PropertyType
        public SearchResult<PropertyTypeListView> GetPropertyTypeListvw(SearchParameter search)
        {
            try
            {
                IPropertyTypeRepo propertyTypeRepo = new PropertyTypeRepo(db);
                return propertyTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region VendorTable
        public SearchResult<VendorTableListView> GetVendorTableListvw(SearchParameter search)
        {
            try
            {
                IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
                return vendorTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CustomerTable
        public SearchResult<CustomerTableListView> GetCustomerTableListvw(SearchParameter search)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                return customerTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ExposureGroup
        public SearchResult<ExposureGroupListView> GetExposureGroupListvw(SearchParameter search)
        {
            try
            {
                IExposureGroupRepo exposureGroupRepo = new ExposureGroupRepo(db);
                return exposureGroupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region VendGroup
        public SearchResult<VendGroupListView> GetVendGroupListvw(SearchParameter search)
        {
            try
            {
                IVendGroupRepo vendGroupRepo = new VendGroupRepo(db);
                return vendGroupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region GuarantorTrans
        public SearchResult<GuarantorTransListView> GetGuarantorTransListvw(SearchParameter search)
        {
            try
            {
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                return guarantorTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region JointVentureTrans
        public SearchResult<JointVentureTransListView> GetJointVentureTransListvw(SearchParameter search)
        {
            try
            {
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                return jointVentureTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region MemoTrans
        public SearchResult<MemoTransListView> GetMemoTransListvw(SearchParameter search)
        {
            try
            {
                IMemoTransRepo memoTransRepo = new MemoTransRepo(db);

                return memoTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentStatus
        public SearchResult<DocumentStatusListView> GetDocumentStatusListvw(SearchParameter search)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                return documentStatusRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentProcess
        public SearchResult<DocumentProcessListView> GetDocumentProcessListvw(SearchParameter search)
        {
            try
            {
                IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
                return documentProcessRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CustVisitingTrans
        public SearchResult<CustVisitingTransListView> GetCustVisitingTransListvw(SearchParameter search)
        {
            try
            {
                ICustVisitingTransRepo jointVentureTransRepo = new CustVisitingTransRepo(db);
                return jointVentureTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ContactTrans
        public SearchResult<ContactTransListView> GetContactTransListvw(SearchParameter search)
        {
            try
            {
                IContactTransRepo contactTransRepo = new ContactTransRepo(db);
                return contactTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region MaritalStatus
        public SearchResult<MaritalStatusListView> GetMaritalStatusListvw(SearchParameter search)
        {
            try
            {
                IMaritalStatusRepo maritalStatusRepo = new MaritalStatusRepo(db);
                return maritalStatusRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region OwnerTrans
        public SearchResult<OwnerTransListView> GetOwnerTransListvw(SearchParameter search)
        {
            try
            {
                IOwnerTransRepo jointVentureTransRepo = new OwnerTransRepo(db);
                return jointVentureTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Gender
        public SearchResult<GenderListView> GetGenderListvw(SearchParameter search)
        {
            try
            {
                IGenderRepo genderRepo = new GenderRepo(db);
                return genderRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Occupation
        public SearchResult<OccupationListView> GetOccupationListvw(SearchParameter search)
        {
            try
            {
                IOccupationRepo occupationRepo = new OccupationRepo(db);
                return occupationRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AuthorizedPersonTrans
        public SearchResult<AuthorizedPersonTransListView> GetAuthorizedPersonTransListvw(SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                return authorizedPersonTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region TaxTable
        public SearchResult<TaxTableListView> GetTaxTableListvw(SearchParameter search)
        {
            try
            {
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                return taxTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region TaxValue
        public SearchResult<TaxValueListView> GetTaxValueListvw(SearchParameter search)
        {
            try
            {
                ITaxValueRepo taxValueRepo = new TaxValueRepo(db);
                return taxValueRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Territory
        public SearchResult<TerritoryListView> GetTerritoryListvw(SearchParameter search)
        {
            try
            {
                ITerritoryRepo territoryRepo = new TerritoryRepo(db);
                return territoryRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BuyerCreditLimitByProduct
        public SearchResult<BuyerCreditLimitByProductListView> GetBuyerCreditLimitByProductListvw(SearchParameter search)
        {
            try
            {
                IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                SearchResult<BuyerCreditLimitByProductListView> buyerCreditLimitByProductList = buyerCreditLimitByProductRepo.GetListvw(search);
                buyerCreditLimitByProductList.Results.ForEach(item => item.CreditLimitBalance = creditAppTableService.GetBuyerCreditLimitBalanceByProduct(item.BuyerTableGUID, item.ProductType, item.CreditLimit));

                return buyerCreditLimitByProductList;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region WithholdingTaxGroup
        public SearchResult<WithholdingTaxGroupListView> GetWithholdingTaxGroupListvw(SearchParameter search)
        {
            try
            {
                IWithholdingTaxGroupRepo withholdingTaxGroupRepo = new WithholdingTaxGroupRepo(db);
                return withholdingTaxGroupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ProjectReferenceTrans
        public SearchResult<ProjectReferenceTransListView> GetProjectReferenceTransListvw(SearchParameter search)
        {
            try
            {
                IProjectReferenceTransRepo projectReferenceTransRepo = new ProjectReferenceTransRepo(db);
                return projectReferenceTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BuyerAgreementLine
        public SearchResult<BuyerAgreementLineListView> GetBuyerAgreementLineListvw(SearchParameter search)
        {
            try
            {
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                return buyerAgreementLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region NCBAccountStatus
        public SearchResult<NCBAccountStatusListView> GetNCBAccountStatusListvw(SearchParameter search)
        {
            try
            {
                INCBAccountStatusRepo nCBAccountStatusRepo = new NCBAccountStatusRepo(db);
                return nCBAccountStatusRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BuyerAgreementTable
        public SearchResult<BuyerAgreementTableListView> GetBuyerAgreementTableListvw(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                return buyerAgreementTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CustomerCreditLimitByProduct
        public SearchResult<CustomerCreditLimitByProductListView> GetCustomerCreditLimitByProductListvw(SearchParameter search)
        {
            try
            {
                ICustomerCreditLimitByProductRepo customerCreditLimitByProductRepo = new CustomerCreditLimitByProductRepo(db);
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                SearchResult<CustomerCreditLimitByProductListView> customerCreditLimitByProductList = customerCreditLimitByProductRepo.GetListvw(search);
                customerCreditLimitByProductList.Results.ForEach(item => item.CreditLimitBalance = creditAppTableService.GetCustomerCreditLimitBalanceByProduct(item.CustomerTableGUID, item.ProductType, item.CreditLimit));

                return customerCreditLimitByProductList;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region LedgerDimension
        public SearchResult<LedgerDimensionListView> GetLedgerDimensionListvw(SearchParameter search)
        {
            try
            {
                ILedgerDimensionRepo ledgerDimensionRepo = new LedgerDimensionRepo(db);
                return ledgerDimensionRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentConditionTemplateLine
        public SearchResult<DocumentConditionTemplateLineListView> GetDocumentConditionTemplateLineListvw(SearchParameter search)
        {
            try
            {
                IDocumentConditionTemplateLineRepo documentConditionTemplateLineRepo = new DocumentConditionTemplateLineRepo(db);
                return documentConditionTemplateLineRepo.GetListvw(search);

            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region RetentionConditionSetup
        public SearchResult<RetentionConditionSetupListView> GetRetentionConditionSetupListvw(SearchParameter search)
        {
            try
            {
                IRetentionConditionSetupRepo retentionConditionSetupRepo = new RetentionConditionSetupRepo(db);
                return retentionConditionSetupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AddressTrans
        public SearchResult<AddressTransListView> GetAddressTransListvw(SearchParameter search)
        {
            try
            {
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                return addressTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region MethodOfPayment
        public SearchResult<MethodOfPaymentListView> GetMethodOfPaymentListvw(SearchParameter search)
        {
            try
            {
                IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
                return methodOfPaymentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Ownership
        public SearchResult<OwnershipListView> GetOwnershipListvw(SearchParameter search)
        {
            try
            {
                IOwnershipRepo ownershipRepo = new OwnershipRepo(db);
                return ownershipRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ParentCompany
        public SearchResult<ParentCompanyListView> GetParentCompanyListvw(SearchParameter search)
        {
            try
            {
                IParentCompanyRepo parentCompanyRepo = new ParentCompanyRepo(db);
                return parentCompanyRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region EmployeeTable
        public SearchResult<EmployeeTableListView> GetEmployeeTableListvw(SearchParameter search)
        {
            try
            {
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                return employeeTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region EmplTeam
        public SearchResult<EmplTeamListView> GetEmplTeamListvw(SearchParameter search)
        {
            try
            {
                IEmplTeamRepo emplTeamRepo = new EmplTeamRepo(db);
                return emplTeamRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ServiceFeeCondTemplateLine
        public SearchResult<ServiceFeeCondTemplateLineListView> GetServiceFeeCondTemplateLineListvw(SearchParameter search)
        {
            try
            {
                IServiceFeeCondTemplateLineRepo serviceFeeCondTemplateLineRepo = new ServiceFeeCondTemplateLineRepo(db);
                return serviceFeeCondTemplateLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ConsortiumTable
        public SearchResult<ConsortiumTableListView> GetConsortiumTableListvw(SearchParameter search)
        {
            try
            {
                IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
                return consortiumTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ServiceFeeCondTemplateTable
        public SearchResult<ServiceFeeCondTemplateTableListView> GetServiceFeeCondTemplateTableListvw(SearchParameter search)
        {
            try
            {
                IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
                return serviceFeeCondTemplateTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ConsortiumLine
        public SearchResult<ConsortiumLineListView> GetConsortiumLineListvw(SearchParameter search)
        {
            try
            {
                IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
                return consortiumLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InvoiceRevenueType
        public SearchResult<InvoiceRevenueTypeListView> GetInvoiceRevenueTypeListvw(SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
                return invoiceRevenueTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InvoiceLine
        public SearchResult<InvoiceLineListView> GetInvoiceLineListvw(SearchParameter search)
        {
            try
            {
                IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
                return invoiceLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentConditionTrans
        public SearchResult<DocumentConditionTransListView> GetDocumentConditionTransListvw(SearchParameter search)
        {
            try
            {
                IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
                return documentConditionTransRepo.GetListvw(search);

            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditAppRequestLine
        public SearchResult<CreditAppRequestLineListView> GetCreditAppRequestLineListvw(SearchParameter search)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                return creditAppRequestLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region FinancialStatementTrans
        public SearchResult<FinancialStatementTransListView> GetFinancialStatementTransListvw(SearchParameter search)
        {
            try
            {
                IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
                return financialStatementTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditAppRequestTable
        public SearchResult<CreditAppRequestTableListView> GetCreditAppRequestTableListvw(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<CreditAppRequestTableListView> GetReviewCreditAppRequestListvw(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetListReview(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<CreditAppRequestTableListView> GetClosingCreditAppRequestListvw(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetListClosing(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region WithholdingTaxTable
        public SearchResult<WithholdingTaxTableListView> GetWithholdingTaxTableListvw(SearchParameter search)
        {
            try
            {
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                return withholdingTaxTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region WithholdingTaxValue
        public SearchResult<WithholdingTaxValueListView> GetWithholdingTaxValueListvw(SearchParameter search)
        {
            try
            {
                IWithholdingTaxValueRepo withholdingTaxValueRepo = new WithholdingTaxValueRepo(db);
                return withholdingTaxValueRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region TaxReportTrans
        public SearchResult<TaxReportTransListView> GetTaxReportTransListvw(SearchParameter search)
        {
            try
            {
                ITaxReportTransRepo taxReportTransRepo = new TaxReportTransRepo(db);
                return taxReportTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region Company
        public SearchResult<CompanyListView> GetCompanyListvw(SearchParameter search)
        {
            try
            {
                ICompanyRepo companyRepo = new CompanyRepo(db);
                return companyRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CustBank
        public SearchResult<CustBankListView> GetCustBankListvw(SearchParameter search)
        {
            try
            {
                ICustBankRepo custBankRepo = new CustBankRepo(db);
                return custBankRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentConditionTemplateTable
        public SearchResult<DocumentConditionTemplateTableListView> GetDocumentConditionTemplateTableListvw(SearchParameter search)
        {
            try
            {
                IDocumentConditionTemplateTableRepo documentConditionTemplateTableRepo = new DocumentConditionTemplateTableRepo(db);
                return documentConditionTemplateTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region VendBank
        public SearchResult<VendBankListView> GetVendBankListvw(SearchParameter search)
        {
            try
            {
                IVendBankRepo vendBankRepo = new VendBankRepo(db);
                return vendBankRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BusinessUnit
        public SearchResult<BusinessUnitListView> GetBusinessUnitListvw(SearchParameter search)
        {
            try
            {
                IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
                return businessUnitRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ExposureGroupByProduct
        public SearchResult<ExposureGroupByProductListView> GetExposureGroupByProductListvw(SearchParameter search)
        {
            try
            {
                IExposureGroupByProductRepo exposureGroupByProductRepo = new ExposureGroupByProductRepo(db);
                return exposureGroupByProductRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region NCBTrans
        public SearchResult<NCBTransListView> GetNCBTransListvw(SearchParameter search)
        {
            try
            {
                INCBTransRepo ncbTransRepo = new NCBTransRepo(db);
                return ncbTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region NumberSeqSetupByProductType
        public SearchResult<NumberSeqSetupByProductTypeListView> GetNumberSeqSetupByProductTypeListvw(SearchParameter search)
        {
            try
            {
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                return numberSeqSetupByProductTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CalendarGroup
        public SearchResult<CalendarGroupListView> GetCalendarGroupListvw(SearchParameter search)
        {
            try
            {
                ICalendarGroupRepo calendarGroupRepo = new CalendarGroupRepo(db);
                return calendarGroupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ServiceFeeConditionTrans
        public SearchResult<ServiceFeeConditionTransListView> GetServiceFeeConditionTransListvw(SearchParameter search)
        {
            try
            {
                IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
                return serviceFeeConditionTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CalendarNonWorkingDate
        public SearchResult<CalendarNonWorkingDateListView> GetCalendarNonWorkingDateListvw(SearchParameter search)
        {
            try
            {
                ICalendarNonWorkingDateRepo calendarNonWorkingDateRepo = new CalendarNonWorkingDateRepo(db);
                return calendarNonWorkingDateRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region RetentionConditionTrans
        public SearchResult<RetentionConditionTransListView> GetRetentionConditionTransListvw(SearchParameter search)
        {
            try
            {
                IRetentionConditionTransRepo RetentionConditionTransRepo = new RetentionConditionTransRepo(db);
                return RetentionConditionTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditAppTable
        public SearchResult<CreditAppTableListView> GetCreditAppTableListvw(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                return creditAppTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentTemplateTable
        public SearchResult<DocumentTemplateTableListView> GetDocumentTemplateTableListvw(SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
                return documentTemplateTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditAppLine
        public SearchResult<CreditAppLineListView> GetCreditAppLineListvw(SearchParameter search)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                return creditAppLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BuyerAgreementTrans
        public SearchResult<BuyerAgreementTransListView> GetBuyerAgreementTransListvw(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTransRepo BuyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                return BuyerAgreementTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region FinancialCreditTrans
        public SearchResult<FinancialCreditTransListView> GetFinancialCreditTransListvw(SearchParameter search)
        {
            try
            {
                IFinancialCreditTransRepo FinancialCreditTransRepo = new FinancialCreditTransRepo(db);
                return FinancialCreditTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CompanyParameter
        public SearchResult<CompanyParameterListView> GetCompanyParameterListvw(SearchParameter search)
        {
            try
            {
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                return companyParameterRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BookmarkDocumentTemplateLine
        public SearchResult<BookmarkDocumentTemplateLineListView> GetBookmarkDocumentTemplateLineListvw(SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTemplateLineRepo bookmarkDocumentTemplateLineRepo = new BookmarkDocumentTemplateLineRepo(db);
                return bookmarkDocumentTemplateLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BookmarkDocumentTemplateTable
        public SearchResult<BookmarkDocumentTemplateTableListView> GetBookmarkDocumentTemplateTableListvw(SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTemplateTableRepo bookmarkDocumentTemplateTableRepo = new BookmarkDocumentTemplateTableRepo(db);
                return bookmarkDocumentTemplateTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CompanySignature
        public SearchResult<CompanySignatureListView> GetCompanySignatureListvw(SearchParameter search)
        {
            try
            {
                ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
                return companySignatureRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CustBusinessCollateral
        public SearchResult<CustBusinessCollateralListView> GetCustBusinessCollateralListvw(SearchParameter search)
        {
            try
            {
                ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
                return custBusinessCollateralRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region MainAgreementTable
        public SearchResult<MainAgreementTableListView> GetMainAgreementTableListvw(SearchParameter search)
        {
            try
            {
                IMainAgreementTableRepo MainAgreementTableRepo = new MainAgreementTableRepo(db);
                return MainAgreementTableRepo.GetListvw(search);

            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditAppReqBusinessCollateral
        public SearchResult<CreditAppReqBusinessCollateralListView> GetCreditAppReqBusinessCollateralListvw(SearchParameter search)
        {
            try
            {
                ICreditAppReqBusinessCollateralRepo creditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                return creditAppReqBusinessCollateralRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InvoiceNumberSeqSetup
        public SearchResult<InvoiceNumberSeqSetupListView> GetInvoiceNumberSeqSetupListvw(SearchParameter search)
        {
            try
            {
                IInvoiceNumberSeqSetupRepo invoiceNumberSeqSetupRepo = new InvoiceNumberSeqSetupRepo(db);
                return invoiceNumberSeqSetupRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region LedgerFiscalYear
        public SearchResult<LedgerFiscalYearListView> GetLedgerFiscalYearListvw(SearchParameter search)
        {
            try
            {
                ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
                return ledgerFiscalYearRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region LedgerFiscalPeriod
        public SearchResult<LedgerFiscalPeriodListView> GetLedgerFiscalPeriodListvw(SearchParameter search)
        {
            try
            {
                ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
                return ledgerFiscalPeriodRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ProdUnit
        public SearchResult<ProdUnitListView> GetProdUnitListListvw(SearchParameter search)
        {
            try
            {
                IProdUnitRepo prodUnitRepo = new ProdUnitRepo(db);
                return prodUnitRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BookmarkDocumentTrans
        public SearchResult<BookmarkDocumentTransListView> GetBookmarkDocumentTransListvw(SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                return bookmarkDocumentTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AgreementTableInfo
        public SearchResult<AgreementTableInfoListView> GetAgreementTableInfoListvw(SearchParameter search)
        {
            try
            {
                IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
                return agreementTableInfoRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AssignmentAgreementLine
        public SearchResult<AssignmentAgreementLineListView> GetAssignmentAgreementLineListvw(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                return assignmentAgreementLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InvoiceType
        public SearchResult<InvoiceTypeListView> GetInvoiceTypeListvw(SearchParameter search)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                return invoiceTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BuyerInvoiceTable
        public SearchResult<BuyerInvoiceTableListView> GetBuyerInvoiceTableListvw(SearchParameter search)
        {
            try
            {
                IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
                return buyerInvoiceTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region PurchaseLine
        public SearchResult<PurchaseLineListView> GetPurchaseLineListvw(SearchParameter search)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region PurchaseTable
        public SearchResult<PurchaseTableListView> GetPurchaseTableListvw(SearchParameter search)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                return purchaseTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ConsortiumTrans
        public SearchResult<ConsortiumTransListView> GetConsortiumTransListvw(SearchParameter search)
        {
            try
            {
                IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
                return consortiumTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditAppRequestTableAmend
        public SearchResult<CreditAppRequestTableAmendListView> GetCreditAppRequestTableAmendListvw(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                return creditAppRequestTableAmendRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region VerificationLine
        public SearchResult<VerificationLineListView> GetVerificationLineListvw(SearchParameter search)
        {
            try
            {
                IVerificationLineRepo verificationLineRepo = new VerificationLineRepo(db);
                return verificationLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ProcessTransaction
        public SearchResult<ProcessTransListView> GetProcessTransactionListvw(SearchParameter search)
        {
            try
            {
                IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
                return processTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region VerificationTable
        public SearchResult<VerificationTableListView> GetVerificationTableListvw(SearchParameter search)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                return verificationTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        #region SysUserTable
        public SearchResult<SysUserTableListView> GetSysUserTableListvw(SearchParameter search)
        {
            try
            {
                ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
                return sysUserTableRepo.GetListvw(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region ChequeTable
        public SearchResult<ChequeTableListView> GetChequeTableListvw(SearchParameter search)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                return chequeTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<ChequeTableListView> GetChequeTableListvwByLine(SearchParameter search)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                return chequeTableRepo.GetListvwByLine(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region GuarantorAgreementTable
        public SearchResult<GuarantorAgreementTableListView> GetGuarantorAgreementTableListvw(SearchParameter search)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                return guarantorAgreementTableRepo.GetListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion 
        #region BusinessCollateralAgmTable
        public SearchResult<BusinessCollateralAgmTableListView> GetBusinessCollateralAgmTableListvw(SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                return businessCollateralAgmTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BusinessCollateralAgmLine
        public SearchResult<BusinessCollateralAgmLineListView> GetBusinessCollateralAgmLineListvw(SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                return businessCollateralAgmLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region JobType
        public SearchResult<JobTypeListView> GetJobTypeListvw(SearchParameter search)
        {
            try
            {
                IJobTypeRepo jobTypeRepo = new JobTypeRepo(db);
                return jobTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }


        #endregion
        #region
        public SearchResult<CustTransListView> GetCustTransListvw(SearchParameter search)
        {
            try
            {
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                return custTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InvoiceSettlementDetail
        public SearchResult<InvoiceSettlementDetailListView> GetInvoiceSettlementDetailListvw(SearchParameter search)
        {
            try
            {
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                return invoiceSettlementDetailRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InvoiceTable
        public SearchResult<InvoiceTableListView> GetInvoiceTableListvw(SearchParameter search)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                return invoiceTableRepo.GetListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #region GuarantorAgreementLine
        public SearchResult<GuarantorAgreementLineListView> GetGuarantorAgreementLineListvw(SearchParameter search)
        {
            try
            {
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                var result = guarantorAgreementLineRepo.GetListvw(search);
                foreach (GuarantorAgreementLineListView item in result.Results)
                {
                    item.IsAffiliateCreated = guarantorAgreementLineAffiliateRepo.GetByIdvwByGuarator(item.GuarantorAgreementLineGUID);
                }
                return result;
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion

        #endregion GuarantorAgreementLine

        #region ServiceFeeTrans
        public SearchResult<ServiceFeeTransListView> GetServiceFeeTransListvw(SearchParameter search)
        {
            try
            {
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                return serviceFeeTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        #region CreditApplicationTransaction
        public SearchResult<CreditAppTransListView> GetCreditApplicationTransListvw(SearchParameter search)
        {
            try
            {
                ICreditAppTransRepo creditAppTransRepo = new CreditAppTransRepo(db);
                return creditAppTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #region GuarantorAgreementLineAffiliate
        public SearchResult<GuarantorAgreementLineAffiliateListView> GetGuarantorAgreementLineAffiliateListvw(SearchParameter search)
        {
            try
            {
                IGuarantorAgreementLineAffiliateRepo guarantorAgreementLineAffiliateRepo = new GuarantorAgreementLineAffiliateRepo(db);
                return guarantorAgreementLineAffiliateRepo.GetListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion

        #endregion GuarantorAgreementLineAffiliate

        #region Attachment
        public SearchResult<AttachmentListView> GetAttachmentListvw(SearchParameter search)
        {
            try
            {
                IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
                return attachmentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        #region VendorPaymentTrans
        public SearchResult<VendorPaymentTransListView> GetVendorPaymentTransListvw(SearchParameter search)
        {
            try
            {
                IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
                return vendorPaymentTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region AssignmentAgreementSettle
        public SearchResult<AssignmentAgreementSettleListView> GetAssignmentAgreementSettleListvw(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
                return assignmentAgreementSettleRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region PaymentDetail
        public SearchResult<PaymentDetailListView> GetPaymentDetailListvw(SearchParameter search)
        {
            try
            {
                IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
                return paymentDetailRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region PaymentDetail
        public SearchResult<InquiryPurchaseLineOutstandingListView> GetInquiryPurchaseLineOutstandingListvw(SearchParameter search)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetInquiryPurchaseLineOutstandingListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region VerificationTrans
        public SearchResult<VerificationTransListView> GetVerificationTransListvw(SearchParameter search)
        {
            try
            {
                IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
                return verificationTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CreditAppRequestLineAmend
        public SearchResult<CreditAppRequestLineAmendListView> GetCreditAppRequestLineAmendListvw(SearchParameter search)
        {
            try
            {
                ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
                return creditAppRequestLineAmendRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion        
        #region Interest Realization Transaction
        public SearchResult<InterestRealizedTransListView> GetInterestRealizedTransListvw(SearchParameter search)
        {
            try
            {
                IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
                return interestRealizedTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion Interest Realization Transaction
        #region CollectionFollowUp
        public SearchResult<CollectionFollowUpListView> GetCollectionFollowUpListvw(SearchParameter search)
        {
            try
            {
                ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
                return collectionFollowUpRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region MessengerTableList
        public SearchResult<MessengerTableListView> GetMessengerTableListvw(SearchParameter search)
        {
            try
            {
                IMessengerTableRepo messengerTableRepo = new MessengerTableRepo(db);
                return messengerTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }


        #endregion
        #region receipt
        public SearchResult<ReceiptTableListView> GetReceiptTableListvw(SearchParameter search)
        {
            try
            {
                IReceiptTableRepo receiptTableRepo = new ReceiptTableRepo(db);
                return receiptTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public SearchResult<ReceiptLineListView> GetReceiptLineListvw(SearchParameter search)
        {

            try
            {
                IReceiptLineRepo receiptLineRepoRepo = new ReceiptLineRepo(db);
                return receiptLineRepoRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        #endregion

        #region Branch
        public SearchResult<BranchListView> GetBranchListvw(SearchParameter search)
        {
            try
            {
                IBranchRepo branchRepo = new BranchRepo(db);
                return branchRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion

        #region TaxInvoiceTable
        public SearchResult<TaxInvoiceTableListView> GetTaxInvoiceTableListvw(SearchParameter search)
        {
            try
            {
                ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
                return taxInvoiceTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<TaxInvoiceLineListView> GetTaxInvoiceLineListvw(SearchParameter search)
        {
            try
            {
                ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
                return taxInvoiceLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion Interest Realization Transaction



        #region Messenger JOB
        public SearchResult<MessengerJobTableListView> GetMessengerJobTableListvw(SearchParameter search)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                return messengerJobTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ListPurchaseLineOutstanding
        public SearchResult<ListPurchaseLineOutstandingListView> GetListPurchaseLineOutstandingListvw(SearchParameter search)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetListPurchaseLineOutstandingListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }


        #endregion

        #region ListWithdrawalTable
        public SearchResult<WithdrawalTableListView> GetWithdrawalTableListvw(SearchParameter search)
        {
            try
            {
                IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                return withdrawalTableRepo.GetListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ListWithdrawalLine
        public SearchResult<WithdrawalLineListView> GetWithdrawalLineListvw(SearchParameter search)
        {
            try
            {
                IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
                return withdrawalLineRepo.GetListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ListInquiryWithdrawalLineOutstand
        public SearchResult<InquiryWithdrawalLineOutstandListView> GetInquiryWithdrawalLineOutstandListvw(SearchParameter search)
        {
            try
            {
                IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
                return withdrawalLineRepo.GetInquiryWithdrawalLineOutstandListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }


        #endregion
        #region ListPaymentHistory
        public SearchResult<PaymentHistoryListView> GetPaymentHistoryListvw(SearchParameter search)
        {
            try
            {
                IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
                return paymentHistoryRepo.GetListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }


        #endregion

        #region JobChequeListvw
        public SearchResult<JobChequeListView> GetJobChequeListvw(SearchParameter search)
        {
            try
            {
                IJobChequeRepo jobChequeRepo = new JobChequeRepo(db);
                return jobChequeRepo.GetListvw(search);
            }

            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }


        #endregion
        #region RetentionTrans
        public SearchResult<RetentionTransListView> GetRetentionTransListvw(SearchParameter search)
        {
            try
            {
                IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
                return retentionTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region BuyerRecepiptTable
        public SearchResult<BuyerReceiptTableListView> GetBuyerRecepiptTableListvw(SearchParameter search)
        {

            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                return buyerReceiptTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ProjectProgress
        public SearchResult<ProjectProgressTableListView> GetProjectProgressListvw(SearchParameter search)
        {

            try
            {
                IProjectProgressTableRepo projectProgressTableRepo = new ProjectProgressTableRepo(db);
                return projectProgressTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region InvoiceSettlementDetail
        public SearchResult<InvoiceOutstandingListView> GetInvoiceOutstandingListvw(SearchParameter search)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                return invoiceTableRepo.GetListOutstandingvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<InvoiceOutstandingListView> GetSuspenseOutstandingListByCompany(SearchParameter search)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                // return invoiceTableRepo.GetListOutstandingvw(search);
                return null;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region RetentionOutstanding
        public SearchResult<CAReqRetentionOutstandingListView> GetRetentionOutstandingListvw(SearchParameter search)
        {
            try
            {
                CAReqRetentionOutstandingRepo cAReqRetentionOutstandingRepo = new CAReqRetentionOutstandingRepo(db);
                return cAReqRetentionOutstandingRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion RetentionOutstanding
        #region AssignmentOutstanding
        public SearchResult<CAReqAssignmentOutstandingListView> GetAssignmentOutstandingListvw(SearchParameter search)
        {
            try
            {
                CAReqAssignmentOutstandingRepo cAReqAssignmentOutstandingRepo = new CAReqAssignmentOutstandingRepo(db);
                return cAReqAssignmentOutstandingRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion AssignmentOutstanding
        #region AddressTrans
        public SearchResult<AddressTransListView> GetAdressTransListvw(SearchParameter search)
        {
            try
            {
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                return addressTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion AddressTrans
        #region ReceiptTempTable
        public SearchResult<ReceiptTempTableListView> GetReceiptTempTableListvw(SearchParameter search)
        {
            try
            {
                IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                return receiptTempTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region ReceiptTempPaymDetail
        public SearchResult<ReceiptTempPaymDetailListView> GetReceiptTempPaymDetailListvw(SearchParameter search)
        {
            try
            {
                IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
                return receiptTempPaymDetailRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region SysRoleTable
        public SearchResult<SysRoleTableListView> GetSysRoleTableListvw(SearchParameter search)
        {
            try
            {
                ISysRoleTableRepo sysRoleTableRepo = new SysRoleTableRepo(db);
                return sysRoleTableRepo.GetListvw(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion 
        #region CreditOutStanding
        public SearchResult<CAReqCreditOutStandingListView> GetCreditOutStandingListvw(SearchParameter search)
        {
            try
            {
                CAReqCreditOutStandingRepo cAReqCreditOutStandingRepo = new CAReqCreditOutStandingRepo(db);
                return cAReqCreditOutStandingRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion CreditOutStanding
        #region ProductSettledTrans
        public SearchResult<ProductSettledTransListView> GetProductSettledTransListvw(SearchParameter search)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                return productSettledTransRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<ProductSettledTransListView> GetProductSettledTransListvwByWithdrawal(SearchParameter search)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                return productSettledTransRepo.GetListvwByWithdrawal(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion ProductSettledTrans

        #region Migration
        public SearchResult<MigrationTableListView> GetMigrationTableListvw(SearchParameter search)
        {
            try
            {
                IMigrationTableRepo migrationTableRepo = new MigrationTableRepo(db);
                return migrationTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.ThrowDBException(ex);
            }
        }
        public SearchResult<MigrationLogTableView> GetMigrationLogTableListvw(SearchParameter search)
        {
            try
            {
                IMigrationTableService migrationTableService = new MigrationTableService(db);
                return migrationTableService.GetListByInquiryMigrationName(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.ThrowDBException(ex);
            }
        }
        #endregion
        #region CustomerRefundTable
        public SearchResult<CustomerRefundTableListView> GetCustomerRefundTableListvw(SearchParameter search)
        {
            try
            {
                ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
                return customerRefundTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion CustomerRefundTable

        #region InquiryRollBillPurchaseLineListvw
        public SearchResult<InquiryRollbillPurchaseLineListView> GetInquiryRollBillPurchaseLineListvw(SearchParameter search)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetInquiryRollBillListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion InquiryRollBillPurchaseLineListvw
        #region CreditAppReqAssignment
        public SearchResult<CreditAppReqAssignmentListView> GetCreditAppReqAssignmentListvw(SearchParameter search)
        {
            try
            {
                ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
                return creditAppReqAssignmentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion CreditAppReqAssignment
        #region Account staging
        public SearchResult<StagingTableListView> GetStagingTableListvw(SearchParameter search)
        {
            try
            {
                IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
                return stagingTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentReturnMethod
        public SearchResult<DocumentReturnMethodListView> GetDocumentReturnMethodListvw(SearchParameter search)
        {
            try
            {
                IDocumentReturnMethodRepo documentReturnMethodRepo = new DocumentReturnMethodRepo(db);
                return documentReturnMethodRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region StagingTableVendorInfo
        public SearchResult<StagingTableVendorInfoListView> GetStagingTableVendorInfoListvw(SearchParameter search)
        {
            try
            {
                IStagingTableVendorInfoRepo stagingTableVendorInfoRepo = new StagingTableVendorInfoRepo(db);
                return stagingTableVendorInfoRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region StagingTransText
        public SearchResult<StagingTransTextListView> GetStagingTransTextListvw(SearchParameter search)
        {
            try
            {
                IStagingTransTextRepo stagingTransTextRepo = new StagingTransTextRepo(db);
                return stagingTransTextRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentReturnLine
        public SearchResult<DocumentReturnLineListView> GetDocumentReturnLineListvw(SearchParameter search)
        {
            try
            {
                IDocumentReturnLineRepo documentReturnLineRepo = new DocumentReturnLineRepo(db);
                return documentReturnLineRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region DocumentReturnTable
        public SearchResult<DocumentReturnTableListView> GetDocumentReturnTableListvw(SearchParameter search)
        {
            try
            {
                IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
                return documentReturnTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region CAReqBuyerCreditOutstanding
        public SearchResult<CAReqBuyerCreditOutstandingListView> GetCAReqBuyerCreditOutstandingListvw(SearchParameter search)
        {
            try
            {
                ICAReqBuyerCreditOutstandingRepo documentTypeRepo = new CAReqBuyerCreditOutstandingRepo(db);
                return documentTypeRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion CAReqBuyerCreditOutstanding
        #region Customer Credit Outstanding
        public SearchResult<CreditOutstandingView> GetInquiryCreditOutstandingListByCustvw(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                search.Conditions[4].ColumnName = "ExpiryDate";
                return creditAppTableRepo.GetCreditOutstandingByCustomervw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public SearchResult<CALineOutstandingView> GetGetCALineOutstandingByCA(SearchParameter search)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                search.Conditions[4].ColumnName = "ExpiryDate";
                return creditAppLineRepo.GetCALineOutstandingByCA2(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #endregion
        #region IntercompanyInvoiceTable
        public SearchResult<IntercompanyInvoiceTableListView> GetIntercompanyInvoiceTableListvw(SearchParameter search)
        {
            try
            {
                IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
                return intercompanyInvoiceTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #region FreeTextInvoiceTable
        public SearchResult<FreeTextInvoiceTableListView> GetFreeTextInvoiceTableListvw(SearchParameter search)
        {
            try
            {
                IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
                return freeTextInvoiceTableRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        # region IntercompanyInvoiceAdjustment
        public SearchResult<IntercompanyInvoiceAdjustmentListView> GetIntercompanyInvoiceAdjustmentListvw(SearchParameter search)
        {
            try
            {
                IIntercompanyInvoiceAdjustmentRepo intercompanyInvoiceAdjustmentRepo = new IntercompanyInvoiceAdjustmentRepo(db);
                return intercompanyInvoiceAdjustmentRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
       
        #region IntercompanyInvoiceSettlement
        public SearchResult<IntercompanyInvoiceSettlementListView> GetIntercompanyInvoiceSettlementListvw(SearchParameter search)
        {
            try
            {
                IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
                return intercompanyInvoiceSettlementRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        # region StagingTableIntercoInvSettleListvw
        public SearchResult<StagingTableIntercoInvSettleListView> GetStagingTableIntercoInvSettleListvw(SearchParameter search)
        {
            try
            {
                IStagingTableIntercoInvSettleRepo stagingTableIntercoInvSettleRepo = new StagingTableIntercoInvSettleRepo(db);
                return stagingTableIntercoInvSettleRepo.GetListvw(search);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
    }
}