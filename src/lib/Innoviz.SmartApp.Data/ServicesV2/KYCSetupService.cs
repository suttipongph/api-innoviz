using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IKYCSetupService
	{

		KYCSetupItemView GetKYCSetupById(string id);
		KYCSetupItemView CreateKYCSetup(KYCSetupItemView kycSetupView);
		KYCSetupItemView UpdateKYCSetup(KYCSetupItemView kycSetupView);
		bool DeleteKYCSetup(string id);
	}
	public class KYCSetupService : SmartAppService, IKYCSetupService
	{
		public KYCSetupService(SmartAppDbContext context) : base(context) { }
		public KYCSetupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public KYCSetupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public KYCSetupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public KYCSetupService() : base() { }

		public KYCSetupItemView GetKYCSetupById(string id)
		{
			try
			{
				IKYCSetupRepo kycSetupRepo = new KYCSetupRepo(db);
				return kycSetupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public KYCSetupItemView CreateKYCSetup(KYCSetupItemView kycSetupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				kycSetupView = accessLevelService.AssignOwnerBU(kycSetupView);
				IKYCSetupRepo kycSetupRepo = new KYCSetupRepo(db);
				KYCSetup kycSetup = kycSetupView.ToKYCSetup();
				kycSetup = kycSetupRepo.CreateKYCSetup(kycSetup);
				base.LogTransactionCreate<KYCSetup>(kycSetup);
				UnitOfWork.Commit();
				return kycSetup.ToKYCSetupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public KYCSetupItemView UpdateKYCSetup(KYCSetupItemView kycSetupView)
		{
			try
			{
				IKYCSetupRepo kycSetupRepo = new KYCSetupRepo(db);
				KYCSetup inputKYCSetup = kycSetupView.ToKYCSetup();
				KYCSetup dbKYCSetup = kycSetupRepo.Find(inputKYCSetup.KYCSetupGUID);
				dbKYCSetup = kycSetupRepo.UpdateKYCSetup(dbKYCSetup, inputKYCSetup);
				base.LogTransactionUpdate<KYCSetup>(GetOriginalValues<KYCSetup>(dbKYCSetup), dbKYCSetup);
				UnitOfWork.Commit();
				return dbKYCSetup.ToKYCSetupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteKYCSetup(string item)
		{
			try
			{
				IKYCSetupRepo kycSetupRepo = new KYCSetupRepo(db);
				Guid kycSetupGUID = new Guid(item);
				KYCSetup kycSetup = kycSetupRepo.Find(kycSetupGUID);
				kycSetupRepo.Remove(kycSetup);
				base.LogTransactionDelete<KYCSetup>(kycSetup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
