using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICustTransService
	{

		CustTransItemView GetCustTransById(string id);
		CustTransItemView CreateCustTrans(CustTransItemView custTransView);
		CustTrans CreateCustTrans(CustTrans custTrans);
		CustTransItemView UpdateCustTrans(CustTransItemView custTransView);
		bool DeleteCustTrans(string id);
		#region Shared
		decimal GetAllCustomerBuyerOutstanding(int productType, Guid buyerTableGUID);
        #endregion Shared
    }
    public class CustTransService : SmartAppService, ICustTransService
	{
		public CustTransService(SmartAppDbContext context) : base(context) { }
		public CustTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CustTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CustTransService() : base() { }

		public CustTransItemView GetCustTransById(string id)
		{
			try
			{
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				return custTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustTransItemView CreateCustTrans(CustTransItemView custTransView)
		{
			try
			{
				CustTrans custTrans = custTransView.ToCustTrans();
				custTrans = CreateCustTrans(custTrans);
				UnitOfWork.Commit();
				return custTrans.ToCustTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustTrans CreateCustTrans(CustTrans custTrans)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				custTrans = accessLevelService.AssignOwnerBU(custTrans);
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				custTrans = custTransRepo.CreateCustTrans(custTrans);
				base.LogTransactionCreate<CustTrans>(custTrans);
				return custTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustTransItemView UpdateCustTrans(CustTransItemView custTransView)
		{
			try
			{
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				CustTrans inputCustTrans = custTransView.ToCustTrans();
				CustTrans dbCustTrans = custTransRepo.Find(inputCustTrans.CustTransGUID);
				dbCustTrans = custTransRepo.UpdateCustTrans(dbCustTrans, inputCustTrans);
				base.LogTransactionUpdate<CustTrans>(GetOriginalValues<CustTrans>(dbCustTrans), dbCustTrans);
				UnitOfWork.Commit();
				return dbCustTrans.ToCustTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCustTrans(string item)
		{
			try
			{
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				Guid custTransGUID = new Guid(item);
				CustTrans custTrans = custTransRepo.Find(custTransGUID);
				custTransRepo.Remove(custTrans);
				base.LogTransactionDelete<CustTrans>(custTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Shared
		public decimal GetAllCustomerBuyerOutstanding(int productType, Guid buyerTableGUID)
		{
			try
			{
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				IQueryable<CustTrans> custTrans = custTransRepo.GetByIsProductInvoice()
															   .Where(w => w.ProductType == productType
															            && w.BuyerTableGUID == buyerTableGUID
																		&& w.CustTransStatus == (int)CustTransStatus.Open);
				return (custTrans.Any() ? custTrans.Sum(su => su.TransAmount - su.SettleAmount) : 0);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Shared
	}
}
