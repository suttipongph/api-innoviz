using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IPaymentHistoryService
	{

		PaymentHistoryItemView GetPaymentHistoryById(string id);
		PaymentHistoryItemView CreatePaymentHistory(PaymentHistoryItemView paymentHistoryView);
		PaymentHistoryItemView UpdatePaymentHistory(PaymentHistoryItemView paymentHistoryView);
		bool DeletePaymentHistory(string id);
		FileInformation RenderReportServiceFeeFactRealization(RptReportServiceFeeFactRealizationReportView rptServiceFeeFactRealizationReportView);
		DateTime? GetPaymentHistoryLastReceivedDate(Guid InvoiceTableGUID);
		DateTime? GetPaymentHistoryLastReceivedDate(List<PaymentHistory> paymentHistoryList);
	}
	public class PaymentHistoryService : SmartAppService, IPaymentHistoryService
	{
		public PaymentHistoryService(SmartAppDbContext context) : base(context) { }
		public PaymentHistoryService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public PaymentHistoryService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public PaymentHistoryService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public PaymentHistoryService() : base() { }

		public PaymentHistoryItemView GetPaymentHistoryById(string id)
		{
			try
			{
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
				return paymentHistoryRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PaymentHistoryItemView CreatePaymentHistory(PaymentHistoryItemView paymentHistoryView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				paymentHistoryView = accessLevelService.AssignOwnerBU(paymentHistoryView);
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
				PaymentHistory paymentHistory = paymentHistoryView.ToPaymentHistory();
				paymentHistory = paymentHistoryRepo.CreatePaymentHistory(paymentHistory);
				base.LogTransactionCreate<PaymentHistory>(paymentHistory);
				UnitOfWork.Commit();
				return paymentHistory.ToPaymentHistoryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PaymentHistoryItemView UpdatePaymentHistory(PaymentHistoryItemView paymentHistoryView)
		{
			try
			{
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
				PaymentHistory inputPaymentHistory = paymentHistoryView.ToPaymentHistory();
				PaymentHistory dbPaymentHistory = paymentHistoryRepo.Find(inputPaymentHistory.PaymentHistoryGUID);
				dbPaymentHistory = paymentHistoryRepo.UpdatePaymentHistory(dbPaymentHistory, inputPaymentHistory);
				base.LogTransactionUpdate<PaymentHistory>(GetOriginalValues<PaymentHistory>(dbPaymentHistory), dbPaymentHistory);
				UnitOfWork.Commit();
				return dbPaymentHistory.ToPaymentHistoryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeletePaymentHistory(string item)
		{
			try
			{
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
				Guid paymentHistoryGUID = new Guid(item);
				PaymentHistory paymentHistory = paymentHistoryRepo.Find(paymentHistoryGUID);
				paymentHistoryRepo.Remove(paymentHistory);
				base.LogTransactionDelete<PaymentHistory>(paymentHistory);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FileInformation RenderReportServiceFeeFactRealization(RptReportServiceFeeFactRealizationReportView rptServiceFeeFactRealizationReportView)
		{
			try
			{
				#region Validate
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				// 1. To collection date < From collection date
				if (ConditionService.IsFromDateGreaterThanToDate(rptServiceFeeFactRealizationReportView.FromCollectionDate, rptServiceFeeFactRealizationReportView.ToCollectionDate))
				{
						ex.AddData("ERROR.90165", new string[] { "LABEL.TO_COLLECTION_DATE", "LABEL.FROM_COLLECTION_DATE" });
				}
				#endregion Validate

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					IReportViewerService reportViewerService = new ReportViewerService();
					return reportViewerService.RenderReport(rptServiceFeeFactRealizationReportView);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DateTime? GetPaymentHistoryLastReceivedDate(Guid invoiceTableGUID)
        {
            try
            {
				IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
				var paymentHistoryList = paymentHistoryRepo.GetPaymentHistoryForLastPaymentDateNoTracking(invoiceTableGUID);
				return GetPaymentHistoryLastReceivedDate(paymentHistoryList);
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public DateTime? GetPaymentHistoryLastReceivedDate(List<PaymentHistory> paymentHistoryList)
        {
            try
            {
				if(paymentHistoryList != null && paymentHistoryList.Count > 0)
                {
					// validate: get LastReceiptDate by InvoiceTableGUID
					if(paymentHistoryList.GroupBy(g => g.InvoiceTableGUID).Count() > 1)
                    {
						throw new SmartAppException("Error GetPaymentHistoryLastReceivedDate: Only 1 value of PaymentHistory.InvoiceTableGUID is allowed.");
                    }
					else if(paymentHistoryList.Any(a => a.Cancel == true))
                    {
						throw new SmartAppException("Error GetPaymentHistoryLastReceivedDate: Wrong PaymentHistory condition.");
					}
					else
                    {
						return paymentHistoryList.Max(m => m.ReceivedDate);
                    }
                }
				else
                {
					return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}
