using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface INumberSequenceService
	{

		NumberSeqParameterItemView GetNumberSeqParameterById(string id);
		NumberSeqParameterItemView CreateNumberSeqParameter(NumberSeqParameterItemView numberSeqParameterView);
		NumberSeqParameterItemView UpdateNumberSeqParameter(NumberSeqParameterItemView numberSeqParameterView);
		bool DeleteNumberSeqParameter(string id);
		NumberSeqSegmentItemView GetNumberSeqSegmentById(string id);
		NumberSeqSegmentItemView CreateNumberSeqSegment(NumberSeqSegmentItemView numberSeqSegmentView);
		NumberSeqSegmentItemView UpdateNumberSeqSegment(NumberSeqSegmentItemView numberSeqSegmentView);
		bool DeleteNumberSeqSegment(string id);
		NumberSeqTableItemView GetNumberSeqTableById(string id);
		NumberSeqTableItemView CreateNumberSeqTable(NumberSeqTableItemView numberSeqTableView);
		NumberSeqTableItemView UpdateNumberSeqTable(NumberSeqTableItemView numberSeqTableView);
		bool DeleteNumberSeqTable(string id);
		bool IsManualByNumberSeqTableGUID(Guid numberSeqTableGUID);
		bool IsManualByReferenceId(Guid companyGUID, string referenceId);
		string GetNumber(Guid companyGUID, Guid? branchGUID, Guid numberSeqTableGUID, bool ignoreValidation = false);
		List<NumberSequences> GetNumber(List<NumberSequences> items);
		NumberSeqSegmentItemView GetNumberSeqSegmentInitialData(string id);
		bool IsManualByProductType(Guid numberSeqTableGUID);
	}
	public class NumberSequenceService : SmartAppService, INumberSequenceService
	{
		public NumberSequenceService(SmartAppDbContext context) : base(context) { }
		public NumberSequenceService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public NumberSequenceService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NumberSequenceService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public NumberSequenceService() : base() { }

		public NumberSeqParameterItemView GetNumberSeqParameterById(string id)
		{
			try
			{
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				return numberSeqParameterRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqParameterItemView CreateNumberSeqParameter(NumberSeqParameterItemView numberSeqParameterView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				numberSeqParameterView = accessLevelService.AssignOwnerBU(numberSeqParameterView);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqParameter numberSeqParameter = numberSeqParameterView.ToNumberSeqParameter();
				numberSeqParameter = numberSeqParameterRepo.CreateNumberSeqParameter(numberSeqParameter);
				base.LogTransactionCreate<NumberSeqParameter>(numberSeqParameter);
				UnitOfWork.Commit();
				return numberSeqParameter.ToNumberSeqParameterItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqParameterItemView UpdateNumberSeqParameter(NumberSeqParameterItemView numberSeqParameterView)
		{
			try
			{
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqParameter inputNumberSeqParameter = numberSeqParameterView.ToNumberSeqParameter();
				NumberSeqParameter dbNumberSeqParameter = numberSeqParameterRepo.Find(inputNumberSeqParameter.NumberSeqParameterGUID);
				dbNumberSeqParameter = numberSeqParameterRepo.UpdateNumberSeqParameter(dbNumberSeqParameter, inputNumberSeqParameter);
				base.LogTransactionUpdate<NumberSeqParameter>(GetOriginalValues<NumberSeqParameter>(dbNumberSeqParameter), dbNumberSeqParameter);
				UnitOfWork.Commit();
				return dbNumberSeqParameter.ToNumberSeqParameterItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteNumberSeqParameter(string item)
		{
			try
			{
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				Guid numberSeqParameterGUID = new Guid(item);
				NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.Find(numberSeqParameterGUID);
				numberSeqParameterRepo.Remove(numberSeqParameter);
				base.LogTransactionDelete<NumberSeqParameter>(numberSeqParameter);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public NumberSeqSegmentItemView GetNumberSeqSegmentById(string id)
		{
			try
			{
				INumberSeqSegmentRepo numberSeqSegmentRepo = new NumberSeqSegmentRepo(db);
				return numberSeqSegmentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqSegmentItemView CreateNumberSeqSegment(NumberSeqSegmentItemView numberSeqSegmentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				numberSeqSegmentView = accessLevelService.AssignOwnerBU(numberSeqSegmentView);
				INumberSeqSegmentRepo numberSeqSegmentRepo = new NumberSeqSegmentRepo(db);
				NumberSeqSegment numberSeqSegment = numberSeqSegmentView.ToNumberSeqSegment();
				numberSeqSegment = numberSeqSegmentRepo.CreateNumberSeqSegment(numberSeqSegment);
				base.LogTransactionCreate<NumberSeqSegment>(numberSeqSegment);
				UnitOfWork.Commit();
				return numberSeqSegment.ToNumberSeqSegmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqSegmentItemView UpdateNumberSeqSegment(NumberSeqSegmentItemView numberSeqSegmentView)
		{
			try
			{
				INumberSeqSegmentRepo numberSeqSegmentRepo = new NumberSeqSegmentRepo(db);
				NumberSeqSegment inputNumberSeqSegment = numberSeqSegmentView.ToNumberSeqSegment();
				NumberSeqSegment dbNumberSeqSegment = numberSeqSegmentRepo.Find(inputNumberSeqSegment.NumberSeqSegmentGUID);
				dbNumberSeqSegment = numberSeqSegmentRepo.UpdateNumberSeqSegment(dbNumberSeqSegment, inputNumberSeqSegment);
				base.LogTransactionUpdate<NumberSeqSegment>(GetOriginalValues<NumberSeqSegment>(dbNumberSeqSegment), dbNumberSeqSegment);
				UnitOfWork.Commit();
				return dbNumberSeqSegment.ToNumberSeqSegmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteNumberSeqSegment(string item)
		{
			try
			{
				INumberSeqSegmentRepo numberSeqSegmentRepo = new NumberSeqSegmentRepo(db);
				Guid numberSeqSegmentGUID = new Guid(item);
				NumberSeqSegment numberSeqSegment = numberSeqSegmentRepo.Find(numberSeqSegmentGUID);
				numberSeqSegmentRepo.Remove(numberSeqSegment);
				base.LogTransactionDelete<NumberSeqSegment>(numberSeqSegment);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public NumberSeqTableItemView GetNumberSeqTableById(string id)
		{
			try
			{
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				return numberSeqTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqTableItemView CreateNumberSeqTable(NumberSeqTableItemView numberSeqTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				numberSeqTableView = accessLevelService.AssignOwnerBU(numberSeqTableView);
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				NumberSeqTable numberSeqTable = numberSeqTableView.ToNumberSeqTable();
				numberSeqTable = numberSeqTableRepo.CreateNumberSeqTable(numberSeqTable);
				base.LogTransactionCreate<NumberSeqTable>(numberSeqTable);
				UnitOfWork.Commit();
				return numberSeqTable.ToNumberSeqTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NumberSeqTableItemView UpdateNumberSeqTable(NumberSeqTableItemView numberSeqTableView)
		{
			try
			{
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				NumberSeqTable inputNumberSeqTable = numberSeqTableView.ToNumberSeqTable();
				NumberSeqTable dbNumberSeqTable = numberSeqTableRepo.Find(inputNumberSeqTable.NumberSeqTableGUID);
				dbNumberSeqTable = numberSeqTableRepo.UpdateNumberSeqTable(dbNumberSeqTable, inputNumberSeqTable);
				base.LogTransactionUpdate<NumberSeqTable>(GetOriginalValues<NumberSeqTable>(dbNumberSeqTable), dbNumberSeqTable);
				UnitOfWork.Commit();
				return dbNumberSeqTable.ToNumberSeqTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteNumberSeqTable(string item)
		{
			try
			{
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				Guid numberSeqTableGUID = new Guid(item);
				NumberSeqTable numberSeqTable = numberSeqTableRepo.Find(numberSeqTableGUID);
				numberSeqTableRepo.Remove(numberSeqTable);
				base.LogTransactionDelete<NumberSeqTable>(numberSeqTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsManualByNumberSeqTableGUID(Guid numberSeqTableGUID)
		{
			try
			{
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				NumberSeqTable numberSeqTable = numberSeqTableRepo.GetByNumberSeqTableGUID(numberSeqTableGUID);
				return numberSeqTable.Manual;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsManualByReferenceId(Guid companyGUID, string referenceId)
		{
			try
			{
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(companyGUID, referenceId);
				NumberSeqTable numberSeqTable = numberSeqTableRepo.GetByNumberSeqTableGUID(numberSeqParameter.NumberSeqTableGUID.HasValue ? numberSeqParameter.NumberSeqTableGUID.Value : (Guid?)null);

				return numberSeqTable.Manual;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsManualByProductType(Guid numberSeqTableGUID)
		{
			try
			{
				Guid companyGUID = GetCurrentCompany().StringToGuid();
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
				NumberSeqTable numberSeqTable = numberSeqTableRepo.GetByNumberSeqTableGUID(numberSeqTableGUID);
				return numberSeqTable.Manual;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #region gen number sequence
        public string GetNumber(Guid companyGUID, Guid? branchGUID, Guid numberSeqTableGUID, bool ignoreValidation = false)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				string generateId = "";

				var query = db.Set<NumberSeqTable>().Where(a => a.NumberSeqTableGUID == numberSeqTableGUID)
					.Join(db.Set<NumberSeqSegment>(), ns => ns.NumberSeqTableGUID, nss => nss.NumberSeqTableGUID,
					(ns, nss) => new
					{
						NumberSeqTable = ns,
						NumberSeqSegment = nss,
					});

				if (query.Count() == 0)
				{
					ex.AddData("ERROR.NUMBER_SEQ_FAILD");
					throw SmartAppUtil.AddStackTrace(ex);
				}

				if (ConditionService.IsNotZero(query.Where(w => w.NumberSeqTable.Next > w.NumberSeqTable.Largest).Count()))
				{
					ex.AddData("ERROR.00832", new object[] { query.Select(s => s.NumberSeqTable.NumberSeqCode).FirstOrDefault() });
					throw SmartAppUtil.AddStackTrace(ex);
				}

				if (query.Select(s => s.NumberSeqTable.Manual).First() == true && !ignoreValidation)
				{
					generateId = "";
				}
				else
				{
					ICompanyRepo companyRepo = new CompanyRepo(db);
					IBranchRepo branchRepo = new BranchRepo(db);
					string companyId = companyRepo.GetCompanyByIdNoTracking(companyGUID).CompanyId;
					string branchId = (branchGUID.HasValue && branchGUID != Guid.Empty) ? branchRepo.GetBranchByIdNoTracking(branchGUID.Value).BranchId : "";

					int constant = Convert.ToInt32(NumberSeqSegmentType.Constant);
					int number = Convert.ToInt32(NumberSeqSegmentType.Number);
					int variable1 = Convert.ToInt32(NumberSeqSegmentType.Variable1);
					int variable2 = Convert.ToInt32(NumberSeqSegmentType.Variable2);
					int variable3 = Convert.ToInt32(NumberSeqSegmentType.Variable3);
					int companyVariable = Convert.ToInt32(NumberSeqSegmentType.CompanyVariable);
					string numberSeqVariable = db.Set<CompanyParameter>()
							.Where(a => a.CompanyGUID == companyGUID)
							.Select(s => s.NumberSeqVariable)
							.FirstOrDefault();

					foreach (var item in query.OrderBy(o => o.NumberSeqSegment.Ordering))
					{
						if (item.NumberSeqSegment.SegmentType == constant)
							generateId = generateId + item.NumberSeqSegment.SegmentValue;

						if (item.NumberSeqSegment.SegmentType == number)
						{
							int next = (item.NumberSeqTable.Next);
							int largestL = item.NumberSeqTable.Largest.ToString().Length;
							int nextL = next.ToString().Length;
							generateId = generateId + string.Concat(Enumerable.Repeat("0", largestL - nextL)) + next.ToString();
						}

						if (item.NumberSeqSegment.SegmentType == variable1)
							generateId = generateId + companyId;

						if (item.NumberSeqSegment.SegmentType == variable2)
							generateId = generateId + branchId;

						if (item.NumberSeqSegment.SegmentType == companyVariable)
						{
							generateId = generateId + numberSeqVariable;
						}
					}

					using (NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db))
					{
						var item = query.First().NumberSeqTable;
						item.Next = item.Next + 1;

						// 0024908: remove check AccessLevel for gen number sequence
						//numberSeqTableRepo.CheckAccessLevel(item);
						
						numberSeqTableRepo.UpdateSystemFields(item);

						base.LogTransactionUpdate<NumberSeqTable>(GetOriginalValues<NumberSeqTable>(item), item);
					}

				}
				return generateId;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}

		}
		public List<NumberSequences> GetNumber(List<NumberSequences> items)
		{
			try
			{

				List<Guid> numberSeqTableGUID = new List<Guid>();
				NumberSeqTable numberSeqTable = new NumberSeqTable();
				numberSeqTableGUID.AddRange(items.Select(s => s.NumberSeqTableGUID).Distinct());

				if (ValidateGetNumber(numberSeqTableGUID))
				{
					var _numberSequence = items.Join(db.Set<NumberSeqTable>().Where(w => w.Manual == false),
									   item => item.NumberSeqTableGUID,
					nst => nst.NumberSeqTableGUID,
					(item, nst) => new
					{
						item.Key,
						item.NumberSeqTableGUID,
						item.CompanyGUID,
						item.BranchGUID,
						Next = (int)nst.Next - 1,
						nst.Largest
					})
					.GroupBy(g => new { g.NumberSeqTableGUID })
					.SelectMany(s => s.Select((j, i) => new
					{
						j.Key,
						j.NumberSeqTableGUID,
						j.CompanyGUID,
						j.BranchGUID,
						j.Next,
						j.Largest,
						Sequence = i + 1
					}));

					items = (from item in _numberSequence
							 join nss in db.Set<NumberSeqSegment>().OrderBy(o => o.NumberSeqTableGUID).ThenBy(o => o.Ordering)
							 on item.NumberSeqTableGUID equals nss.NumberSeqTableGUID
							 join c in db.Set<Company>()
							 on item.CompanyGUID equals c.CompanyGUID into ljc
							 from c in ljc.DefaultIfEmpty()
							 join b in db.Set<Branch>()
							 on item.BranchGUID equals b.BranchGUID into ljb
							 from b in ljb.DefaultIfEmpty()
							 join cp in db.Set<CompanyParameter>()
							 on item.CompanyGUID equals cp.CompanyGUID into ljcp
							 from cp in ljcp.DefaultIfEmpty()
							 select new
							 {
								 item.Key,
								 item.NumberSeqTableGUID,
								 item.Next,
								 LargestL = item.Largest.ToString().Length,
								 item.Sequence,
								 nss.Ordering,
								 nss.SegmentType,
								 nss.SegmentValue,
								 c?.CompanyId,
								 b?.BranchId,
								 cp?.NumberSeqVariable,
							 })
							.GroupBy(g => new { g.Key, g.NumberSeqTableGUID })
							.Select(s => new NumberSequences
							{
								Key = s.Key.Key,
								NumberSeqTableGUID = s.Key.NumberSeqTableGUID,
								GeneratedId = string.Join("", s.Select(i => (
									(i.SegmentType == (int)NumberSeqSegmentType.Constant) ? i.SegmentValue :
									(i.SegmentType == (int)NumberSeqSegmentType.Number) ? string.Concat(Enumerable.Repeat("0", i.LargestL - (i.Next + i.Sequence).ToString().Length)) + (i.Next + i.Sequence).ToString() :
									(i.SegmentType == (int)NumberSeqSegmentType.Variable1) ? i.CompanyId :
									(i.SegmentType == (int)NumberSeqSegmentType.Variable2) ? i.BranchId :
									(i.SegmentType == (int)NumberSeqSegmentType.CompanyVariable) ? i.NumberSeqVariable : "")))
							}).ToList();

					using (NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db))
					{
						var queryUpdate = _numberSequence.GroupBy(g => g.NumberSeqTableGUID).Select(s => new { NumberSeqTableGUID = s.Key, Next = s.Max(m => m.Sequence) });
						foreach (var item in queryUpdate)
						{
							numberSeqTable = numberSeqTableRepo.Find(item.NumberSeqTableGUID);
							numberSeqTable.Next = numberSeqTable.Next + item.Next;
							numberSeqTableRepo.UpdateSystemFields(numberSeqTable);
							base.LogTransactionUpdate<NumberSeqTable>(GetOriginalValues<NumberSeqTable>(numberSeqTable), numberSeqTable);
						}
					}

				}

				return items;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public bool ValidateGetNumber(List<Guid> guids)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                var query = (from guid in guids
                             join nss in db.Set<NumberSeqSegment>()
                             on guid equals nss.NumberSeqTableGUID into lj
                             from nss in lj.DefaultIfEmpty()
                             where nss == null
                             select guid);

                if (ConditionService.IsNotZero(query.Count()))
                {
                    ex.AddData("ERROR.NUMBER_SEQ_FAILD");
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                var validateNext = from guid in guids
                                   join nst in db.Set<NumberSeqTable>()
                                   on guid equals nst.NumberSeqTableGUID
                                   join nss in db.Set<NumberSeqSegment>()
                                   on nst.NumberSeqTableGUID equals nss.NumberSeqTableGUID
                                   select new
                                   {
                                       NumberSeqTable = nst,
                                       NumberSeqSegment = nss,
                                   };

                if (ConditionService.IsNotZero(validateNext.Where(w => w.NumberSeqTable.Next > w.NumberSeqTable.Largest).Count()))
                {
                    foreach (var item in validateNext.Where(w => w.NumberSeqTable.Next > w.NumberSeqTable.Largest))
                    {
                        ex.AddData("ERROR.00832", new object[] { item.NumberSeqTable.NumberSeqCode });
                    }
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public NumberSeqSegmentItemView GetNumberSeqSegmentInitialData(string id)
		{
			try
			{
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				INumberSeqSegmentRepo numberSeqSegmentRepo = new NumberSeqSegmentRepo(db);
				NumberSeqTable numberSeqTable = numberSeqTableRepo.GetNumberSeqTableIdNoTrackingByAccessLevel(id.StringToGuid());
				List<NumberSeqSegment> numberSeqSegment = numberSeqSegmentRepo.GetNumberSeqSegmentByNumberSeqTableIdNoTracking(id.StringToGuid()).ToList();

				return new NumberSeqSegmentItemView
				{
					NumberSeqSegmentGUID = new Guid().GuidNullToString(),
					NumberSeqTableGUID = numberSeqTable.NumberSeqTableGUID.GuidNullToString(),
					NumberSeqTable_Values = SmartAppUtil.GetDropDownLabel(numberSeqTable.NumberSeqCode, numberSeqTable.Description),
					Ordering = numberSeqSegment.Any() ? numberSeqSegment.Max(m => m.Ordering) + 1 : 1
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
	}
}
