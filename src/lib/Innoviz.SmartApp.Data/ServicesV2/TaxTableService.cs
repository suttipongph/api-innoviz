﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ITaxTableService
    {
        TaxInvoiceLineItemView GetTaxInvoiceLineById(string id);
        TaxInvoiceLineItemView CreateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView);
        TaxInvoiceLineItemView UpdateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView);
        bool DeleteTaxInvoiceLine(string id);
        TaxTableItemView GetTaxTableById(string id);
        TaxTableItemView CreateTaxTable(TaxTableItemView taxTableView);
        TaxTableItemView UpdateTaxTable(TaxTableItemView taxTableView);
        bool DeleteTaxTable(string id);
        TaxValueItemView GetTaxValueById(string id);
        TaxValueItemView CreateTaxValue(TaxValueItemView taxValueView);
        TaxValueItemView UpdateTaxValue(TaxValueItemView taxValueView);
        bool DeleteTaxValue(string id);
        TaxValueItemView GetTaxValueInitialData(string taxTableGUID);
        #region shared
        decimal GetTaxValue(Guid taxTableGuid, DateTime asOfDate, bool isThrow = true);
        #endregion

    }
    public class TaxTableService : SmartAppService, ITaxTableService
    {
        public TaxTableService(SmartAppDbContext context) : base(context) { }
        public TaxTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public TaxTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public TaxTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public TaxTableService() : base() { }
        public TaxInvoiceLineItemView GetTaxInvoiceLineById(string id)
        {
            try
            {
                ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
                return taxInvoiceLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxInvoiceLineItemView CreateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                taxInvoiceLineView = accessLevelService.AssignOwnerBU(taxInvoiceLineView);
                ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
                TaxInvoiceLine taxInvoiceLine = taxInvoiceLineView.ToTaxInvoiceLine();
                taxInvoiceLine = taxInvoiceLineRepo.CreateTaxInvoiceLine(taxInvoiceLine);
                base.LogTransactionCreate<TaxInvoiceLine>(taxInvoiceLine);
                UnitOfWork.Commit();
                return taxInvoiceLine.ToTaxInvoiceLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxInvoiceLineItemView UpdateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView)
        {
            try
            {
                ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
                TaxInvoiceLine inputTaxInvoiceLine = taxInvoiceLineView.ToTaxInvoiceLine();
                TaxInvoiceLine dbTaxInvoiceLine = taxInvoiceLineRepo.Find(inputTaxInvoiceLine.TaxInvoiceLineGUID);
                dbTaxInvoiceLine = taxInvoiceLineRepo.UpdateTaxInvoiceLine(dbTaxInvoiceLine, inputTaxInvoiceLine);
                base.LogTransactionUpdate<TaxInvoiceLine>(GetOriginalValues<TaxInvoiceLine>(dbTaxInvoiceLine), dbTaxInvoiceLine);
                UnitOfWork.Commit();
                return dbTaxInvoiceLine.ToTaxInvoiceLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteTaxInvoiceLine(string item)
        {
            try
            {
                ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
                Guid taxInvoiceLineGUID = new Guid(item);
                TaxInvoiceLine taxInvoiceLine = taxInvoiceLineRepo.Find(taxInvoiceLineGUID);
                taxInvoiceLineRepo.Remove(taxInvoiceLine);
                base.LogTransactionDelete<TaxInvoiceLine>(taxInvoiceLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public TaxTableItemView GetTaxTableById(string id)
        {
            try
            {
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                return taxTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxTableItemView CreateTaxTable(TaxTableItemView taxTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                taxTableView = accessLevelService.AssignOwnerBU(taxTableView);
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                TaxTable taxTable = taxTableView.ToTaxTable();
                taxTable = taxTableRepo.CreateTaxTable(taxTable);
                base.LogTransactionCreate<TaxTable>(taxTable);
                UnitOfWork.Commit();
                return taxTable.ToTaxTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxTableItemView UpdateTaxTable(TaxTableItemView taxTableView)
        {
            try
            {
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                TaxTable inputTaxTable = taxTableView.ToTaxTable();
                TaxTable dbTaxTable = taxTableRepo.Find(inputTaxTable.TaxTableGUID);
                dbTaxTable = taxTableRepo.UpdateTaxTable(dbTaxTable, inputTaxTable);
                base.LogTransactionUpdate<TaxTable>(GetOriginalValues<TaxTable>(dbTaxTable), dbTaxTable);
                UnitOfWork.Commit();
                return dbTaxTable.ToTaxTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteTaxTable(string item)
        {
            try
            {
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                Guid taxTableGUID = new Guid(item);
                TaxTable taxTable = taxTableRepo.Find(taxTableGUID);
                taxTableRepo.Remove(taxTable);
                base.LogTransactionDelete<TaxTable>(taxTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public TaxValueItemView GetTaxValueById(string id)
        {
            try
            {
                ITaxValueRepo taxValueRepo = new TaxValueRepo(db);
                return taxValueRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxValueItemView CreateTaxValue(TaxValueItemView taxValueView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                taxValueView = accessLevelService.AssignOwnerBU(taxValueView);
                ITaxValueRepo taxValueRepo = new TaxValueRepo(db);
                TaxValue taxValue = taxValueView.ToTaxValue();
                taxValue = taxValueRepo.CreateTaxValue(taxValue);
                base.LogTransactionCreate<TaxValue>(taxValue);
                UnitOfWork.Commit();
                return taxValue.ToTaxValueItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxValueItemView UpdateTaxValue(TaxValueItemView taxValueView)
        {
            try
            {
                ITaxValueRepo taxValueRepo = new TaxValueRepo(db);
                TaxValue inputTaxValue = taxValueView.ToTaxValue();
                TaxValue dbTaxValue = taxValueRepo.Find(inputTaxValue.TaxValueGUID);
                dbTaxValue = taxValueRepo.UpdateTaxValue(dbTaxValue, inputTaxValue);
                base.LogTransactionUpdate<TaxValue>(GetOriginalValues<TaxValue>(dbTaxValue), dbTaxValue);
                UnitOfWork.Commit();
                return dbTaxValue.ToTaxValueItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteTaxValue(string item)
        {
            try
            {
                ITaxValueRepo taxValueRepo = new TaxValueRepo(db);
                Guid taxValueGUID = new Guid(item);
                TaxValue taxValue = taxValueRepo.Find(taxValueGUID);
                taxValueRepo.Remove(taxValue);
                base.LogTransactionDelete<TaxValue>(taxValue);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxValueItemView GetTaxValueInitialData(string taxTableGUID)
        {
            try
            {
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                TaxTable taxTable = taxTableRepo.GetTaxTableByIdNoTracking(taxTableGUID.StringToGuid());
                return new TaxValueItemView
                {
                    TaxValueGUID = new Guid().GuidNullToString(),
                    TaxTableGUID = taxTableGUID,
                    TaxTable_Values = SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description),
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region shared
        public decimal GetTaxValue(Guid taxTableGuid, DateTime asOfDate, bool isThrow = true)
        {
            try
            {
                ITaxValueRepo taxValueRepo = new TaxValueRepo(db);
                TaxValue taxValue = taxValueRepo.GetByTaxTableGUIDAndDateNoTracking(taxTableGuid, asOfDate, isThrow);
                return taxValue.Value;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        #endregion
    }
}
