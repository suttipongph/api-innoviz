using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IIntercompanyInvoiceTableService
	{

		IntercompanyInvoiceTableItemView GetIntercompanyInvoiceTableById(string id);
		IntercompanyInvoiceTableItemView GetIntercompanyById(string id);
		IntercompanyInvoiceTableItemView CreateIntercompanyInvoiceTable(IntercompanyInvoiceTableItemView intercompanyInvoiceTableView);
		IntercompanyInvoiceTableItemView UpdateIntercompanyInvoiceTable(IntercompanyInvoiceTableItemView intercompanyInvoiceTableView);
		bool DeleteIntercompanyInvoiceTable(string id);
		bool GetValidateAdjustFunction(string id);
		#region Migration
		void CreateIntercompanyInvoiceTableCollection(IEnumerable<IntercompanyInvoiceTableItemView> intercompanyInvoiceTableItemViews);
		#endregion Migration
	}
	public class IntercompanyInvoiceTableService : SmartAppService, IIntercompanyInvoiceTableService
	{
		public IntercompanyInvoiceTableService(SmartAppDbContext context) : base(context) { }
		public IntercompanyInvoiceTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public IntercompanyInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntercompanyInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public IntercompanyInvoiceTableService() : base() { }

		public IntercompanyInvoiceTableItemView GetIntercompanyInvoiceTableById(string id)
		{
			try
			{
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				return intercompanyInvoiceTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceTableItemView CreateIntercompanyInvoiceTable(IntercompanyInvoiceTableItemView intercompanyInvoiceTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				intercompanyInvoiceTableView = accessLevelService.AssignOwnerBU(intercompanyInvoiceTableView);
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				IntercompanyInvoiceTable intercompanyInvoiceTable = intercompanyInvoiceTableView.ToIntercompanyInvoiceTable();
				intercompanyInvoiceTable = intercompanyInvoiceTableRepo.CreateIntercompanyInvoiceTable(intercompanyInvoiceTable);
				base.LogTransactionCreate<IntercompanyInvoiceTable>(intercompanyInvoiceTable);
				UnitOfWork.Commit();
				return intercompanyInvoiceTable.ToIntercompanyInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceTableItemView UpdateIntercompanyInvoiceTable(IntercompanyInvoiceTableItemView intercompanyInvoiceTableView)
		{
			try
			{
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				IntercompanyInvoiceTable inputIntercompanyInvoiceTable = intercompanyInvoiceTableView.ToIntercompanyInvoiceTable();
				IntercompanyInvoiceTable dbIntercompanyInvoiceTable = intercompanyInvoiceTableRepo.Find(inputIntercompanyInvoiceTable.IntercompanyInvoiceTableGUID);
				dbIntercompanyInvoiceTable = intercompanyInvoiceTableRepo.UpdateIntercompanyInvoiceTable(dbIntercompanyInvoiceTable, inputIntercompanyInvoiceTable);
				base.LogTransactionUpdate<IntercompanyInvoiceTable>(GetOriginalValues<IntercompanyInvoiceTable>(dbIntercompanyInvoiceTable), dbIntercompanyInvoiceTable);
				UnitOfWork.Commit();
				return dbIntercompanyInvoiceTable.ToIntercompanyInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteIntercompanyInvoiceTable(string item)
		{
			try
			{
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				Guid intercompanyInvoiceTableGUID = new Guid(item);
				IntercompanyInvoiceTable intercompanyInvoiceTable = intercompanyInvoiceTableRepo.Find(intercompanyInvoiceTableGUID);
				intercompanyInvoiceTableRepo.Remove(intercompanyInvoiceTable);
				base.LogTransactionDelete<IntercompanyInvoiceTable>(intercompanyInvoiceTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceTableItemView GetIntercompanyById(string id)
		{
			try
			{
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				return intercompanyInvoiceTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public bool GetValidateAdjustFunction(string id)
        {
			try
			{

				IntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				var intercompanyInvoiceSettlementList = intercompanyInvoiceSettlementRepo.GetIntercompanyInvoiceBy(id.StringToGuid(), ((int)IntercompanyInvoiceSettlementStatus.Draft).ToString());
				if(intercompanyInvoiceSettlementList > 0)
                {
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90150", new string[] { "LABEL.INTERCOMPANY_INVOICE_SETTLEMENT" });
					throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
					return true;

				}


			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Migration
		public void CreateIntercompanyInvoiceTableCollection(IEnumerable<IntercompanyInvoiceTableItemView> intercompanyInvoiceTableItemViews)
		{
			try
			{
				if (intercompanyInvoiceTableItemViews.Any())
				{
					IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
					List<IntercompanyInvoiceTable> intercompanyInvoiceTables = intercompanyInvoiceTableItemViews.ToIntercompanyInvoiceTable().ToList();
					intercompanyInvoiceTables.ForEach(f => f.IntercompanyInvoiceTableGUID = Guid.NewGuid());
					intercompanyInvoiceTableRepo.ValidateAdd(intercompanyInvoiceTables);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(intercompanyInvoiceTables);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Migration

	}
}
