using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBusinessSegmentService
	{

		BusinessSegmentItemView GetBusinessSegmentById(string id);
		BusinessSegmentItemView CreateBusinessSegment(BusinessSegmentItemView businessSegmentView);
		BusinessSegmentItemView UpdateBusinessSegment(BusinessSegmentItemView businessSegmentView);
		bool DeleteBusinessSegment(string id);
	}
	public class BusinessSegmentService : SmartAppService, IBusinessSegmentService
	{
		public BusinessSegmentService(SmartAppDbContext context) : base(context) { }
		public BusinessSegmentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BusinessSegmentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessSegmentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BusinessSegmentService() : base() { }

		public BusinessSegmentItemView GetBusinessSegmentById(string id)
		{
			try
			{
				IBusinessSegmentRepo businessSegmentRepo = new BusinessSegmentRepo(db);
				return businessSegmentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessSegmentItemView CreateBusinessSegment(BusinessSegmentItemView businessSegmentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				businessSegmentView = accessLevelService.AssignOwnerBU(businessSegmentView);
				IBusinessSegmentRepo businessSegmentRepo = new BusinessSegmentRepo(db);
				BusinessSegment businessSegment = businessSegmentView.ToBusinessSegment();
				businessSegment = businessSegmentRepo.CreateBusinessSegment(businessSegment);
				base.LogTransactionCreate<BusinessSegment>(businessSegment);
				UnitOfWork.Commit();
				return businessSegment.ToBusinessSegmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessSegmentItemView UpdateBusinessSegment(BusinessSegmentItemView businessSegmentView)
		{
			try
			{
				IBusinessSegmentRepo businessSegmentRepo = new BusinessSegmentRepo(db);
				BusinessSegment inputBusinessSegment = businessSegmentView.ToBusinessSegment();
				BusinessSegment dbBusinessSegment = businessSegmentRepo.Find(inputBusinessSegment.BusinessSegmentGUID);
				dbBusinessSegment = businessSegmentRepo.UpdateBusinessSegment(dbBusinessSegment, inputBusinessSegment);
				base.LogTransactionUpdate<BusinessSegment>(GetOriginalValues<BusinessSegment>(dbBusinessSegment), dbBusinessSegment);
				UnitOfWork.Commit();
				return dbBusinessSegment.ToBusinessSegmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBusinessSegment(string item)
		{
			try
			{
				IBusinessSegmentRepo businessSegmentRepo = new BusinessSegmentRepo(db);
				Guid businessSegmentGUID = new Guid(item);
				BusinessSegment businessSegment = businessSegmentRepo.Find(businessSegmentGUID);
				businessSegmentRepo.Remove(businessSegment);
				base.LogTransactionDelete<BusinessSegment>(businessSegment);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
