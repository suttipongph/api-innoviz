using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IVerificationTypeService
	{

		VerificationTypeItemView GetVerificationTypeById(string id);
		VerificationTypeItemView CreateVerificationType(VerificationTypeItemView verificationTypeView);
		VerificationTypeItemView UpdateVerificationType(VerificationTypeItemView verificationTypeView);
		bool DeleteVerificationType(string id);
	}
	public class VerificationTypeService : SmartAppService, IVerificationTypeService
	{
		public VerificationTypeService(SmartAppDbContext context) : base(context) { }
		public VerificationTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public VerificationTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VerificationTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public VerificationTypeService() : base() { }

		public VerificationTypeItemView GetVerificationTypeById(string id)
		{
			try
			{
				IVerificationTypeRepo verificationTypeRepo = new VerificationTypeRepo(db);
				return verificationTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VerificationTypeItemView CreateVerificationType(VerificationTypeItemView verificationTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				verificationTypeView = accessLevelService.AssignOwnerBU(verificationTypeView);
				IVerificationTypeRepo verificationTypeRepo = new VerificationTypeRepo(db);
				VerificationType verificationType = verificationTypeView.ToVerificationType();
				verificationType = verificationTypeRepo.CreateVerificationType(verificationType);
				base.LogTransactionCreate<VerificationType>(verificationType);
				UnitOfWork.Commit();
				return verificationType.ToVerificationTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VerificationTypeItemView UpdateVerificationType(VerificationTypeItemView verificationTypeView)
		{
			try
			{
				IVerificationTypeRepo verificationTypeRepo = new VerificationTypeRepo(db);
				VerificationType inputVerificationType = verificationTypeView.ToVerificationType();
				VerificationType dbVerificationType = verificationTypeRepo.Find(inputVerificationType.VerificationTypeGUID);
				dbVerificationType = verificationTypeRepo.UpdateVerificationType(dbVerificationType, inputVerificationType);
				base.LogTransactionUpdate<VerificationType>(GetOriginalValues<VerificationType>(dbVerificationType), dbVerificationType);
				UnitOfWork.Commit();
				return dbVerificationType.ToVerificationTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteVerificationType(string item)
		{
			try
			{
				IVerificationTypeRepo verificationTypeRepo = new VerificationTypeRepo(db);
				Guid verificationTypeGUID = new Guid(item);
				VerificationType verificationType = verificationTypeRepo.Find(verificationTypeGUID);
				verificationTypeRepo.Remove(verificationType);
				base.LogTransactionDelete<VerificationType>(verificationType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
