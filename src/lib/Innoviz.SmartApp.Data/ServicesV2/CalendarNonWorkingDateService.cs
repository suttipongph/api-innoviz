using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ICalendarNonWorkingDateService
    {

        CalendarNonWorkingDateItemView GetCalendarNonWorkingDateById(string id);
        CalendarNonWorkingDateItemView CreateCalendarNonWorkingDate(CalendarNonWorkingDateItemView calendarNonWorkingDateView);
        CalendarNonWorkingDateItemView UpdateCalendarNonWorkingDate(CalendarNonWorkingDateItemView calendarNonWorkingDateView);
        bool DeleteCalendarNonWorkingDate(string id);
        #region shared
        DateTime FindWorkingDate(DateTime date, Guid calendarGroupGuid);
        #endregion
    }
    public class CalendarNonWorkingDateService : SmartAppService, ICalendarNonWorkingDateService
    {
        public CalendarNonWorkingDateService(SmartAppDbContext context) : base(context) { }
        public CalendarNonWorkingDateService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public CalendarNonWorkingDateService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CalendarNonWorkingDateService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public CalendarNonWorkingDateService() : base() { }

        public CalendarNonWorkingDateItemView GetCalendarNonWorkingDateById(string id)
        {
            try
            {
                ICalendarNonWorkingDateRepo calendarNonWorkingDateRepo = new CalendarNonWorkingDateRepo(db);
                return calendarNonWorkingDateRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CalendarNonWorkingDateItemView CreateCalendarNonWorkingDate(CalendarNonWorkingDateItemView calendarNonWorkingDateView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                calendarNonWorkingDateView = accessLevelService.AssignOwnerBU(calendarNonWorkingDateView);
                ICalendarNonWorkingDateRepo calendarNonWorkingDateRepo = new CalendarNonWorkingDateRepo(db);
                CalendarNonWorkingDate calendarNonWorkingDate = calendarNonWorkingDateView.ToCalendarNonWorkingDate();
                calendarNonWorkingDate = calendarNonWorkingDateRepo.CreateCalendarNonWorkingDate(calendarNonWorkingDate);
                base.LogTransactionCreate<CalendarNonWorkingDate>(calendarNonWorkingDate);
                UnitOfWork.Commit();
                return calendarNonWorkingDate.ToCalendarNonWorkingDateItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CalendarNonWorkingDateItemView UpdateCalendarNonWorkingDate(CalendarNonWorkingDateItemView calendarNonWorkingDateView)
        {
            try
            {
                ICalendarNonWorkingDateRepo calendarNonWorkingDateRepo = new CalendarNonWorkingDateRepo(db);
                CalendarNonWorkingDate inputCalendarNonWorkingDate = calendarNonWorkingDateView.ToCalendarNonWorkingDate();
                CalendarNonWorkingDate dbCalendarNonWorkingDate = calendarNonWorkingDateRepo.Find(inputCalendarNonWorkingDate.CalendarNonWorkingDateGUID);
                dbCalendarNonWorkingDate = calendarNonWorkingDateRepo.UpdateCalendarNonWorkingDate(dbCalendarNonWorkingDate, inputCalendarNonWorkingDate);
                base.LogTransactionUpdate<CalendarNonWorkingDate>(GetOriginalValues<CalendarNonWorkingDate>(dbCalendarNonWorkingDate), dbCalendarNonWorkingDate);
                UnitOfWork.Commit();
                return dbCalendarNonWorkingDate.ToCalendarNonWorkingDateItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCalendarNonWorkingDate(string item)
        {
            try
            {
                ICalendarNonWorkingDateRepo calendarNonWorkingDateRepo = new CalendarNonWorkingDateRepo(db);
                Guid calendarNonWorkingDateGUID = new Guid(item);
                CalendarNonWorkingDate calendarNonWorkingDate = calendarNonWorkingDateRepo.Find(calendarNonWorkingDateGUID);
                calendarNonWorkingDateRepo.Remove(calendarNonWorkingDate);
                base.LogTransactionDelete<CalendarNonWorkingDate>(calendarNonWorkingDate);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region shared
        public DateTime FindWorkingDate(DateTime date, Guid calendarGroupGuid)
        {
            try
            {
                ICalendarNonWorkingDateRepo calendarNonWorkingDateRepo = new CalendarNonWorkingDateRepo(db);
                ICalendarGroupRepo calendarGroupRepo = new CalendarGroupRepo(db);
                CalendarNonWorkingDate calendarNonWorkingDate = new CalendarNonWorkingDate();
                CalendarGroup calendarGroup = new CalendarGroup();
                while (true)
                {
                    calendarNonWorkingDate = calendarNonWorkingDateRepo.GetCalendarNonWorkingDateByDate(calendarGroupGuid, date);
                    if (calendarNonWorkingDate != null)
                    {
                        date = date.AddDays(1);
                    }
                    else if (date.DayOfWeek == DayOfWeek.Saturday)
                    {
                        calendarGroup = calendarGroupRepo.GetCalendarGroupByIdNoTracking(calendarGroupGuid);
                        if (calendarGroup == null)
                        {
                            SmartAppException ex = new SmartAppException("ERROR.ERROR");
                            ex.AddData("ERROR.90037", new string[] { "LABEL.CALENDAR_GROUP_ID" });
                            throw SmartAppUtil.AddStackTrace(ex);
                        }
                        if (calendarGroup.WorkingDay == (int)WorkingDay.MonToFri)
                        {
                            date = date.AddDays(2);
                        }
                        else
                        {
                            return date;
                        }
                    }
                    else if (date.DayOfWeek == DayOfWeek.Sunday)
                    {
                        calendarGroup = calendarGroupRepo.GetCalendarGroupByIdNoTracking(calendarGroupGuid);
                        if (calendarGroup == null)
                        {
                            SmartAppException ex = new SmartAppException("ERROR.ERROR");
                            ex.AddData("ERROR.90037", new string[] { "LABEL.CALENDAR_GROUP_ID" });
                            throw SmartAppUtil.AddStackTrace(ex);
                        }
                        if (calendarGroup.WorkingDay != (int)WorkingDay.MonToSun)
                        {
                            date = date.AddDays(1);
                        }
                        else
                        {
                            return date;
                        }
                    }
                    else
                    {
                        return date;
                    }
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        #endregion
    }
}
