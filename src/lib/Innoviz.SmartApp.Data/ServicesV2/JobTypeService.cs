using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IJobTypeService
	{

		JobTypeItemView GetJobTypeById(string id);
		JobTypeItemView CreateJobType(JobTypeItemView jobTypeView);
		JobTypeItemView UpdateJobType(JobTypeItemView jobTypeView);
		bool DeleteJobType(string id);
	}
	public class JobTypeService : SmartAppService, IJobTypeService
	{
		public JobTypeService(SmartAppDbContext context) : base(context) { }
		public JobTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public JobTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public JobTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public JobTypeService() : base() { }

		public JobTypeItemView GetJobTypeById(string id)
		{
			try
			{
				IJobTypeRepo jobTypeRepo = new JobTypeRepo(db);
				return jobTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public JobTypeItemView CreateJobType(JobTypeItemView jobTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				jobTypeView = accessLevelService.AssignOwnerBU(jobTypeView);
				IJobTypeRepo jobTypeRepo = new JobTypeRepo(db);
				JobType jobType = jobTypeView.ToJobType();
				jobType = jobTypeRepo.CreateJobType(jobType);
				base.LogTransactionCreate<JobType>(jobType);
				UnitOfWork.Commit();
				return jobType.ToJobTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public JobTypeItemView UpdateJobType(JobTypeItemView jobTypeView)
		{
			try
			{
				IJobTypeRepo jobTypeRepo = new JobTypeRepo(db);
				JobType inputJobType = jobTypeView.ToJobType();
				JobType dbJobType = jobTypeRepo.Find(inputJobType.JobTypeGUID);
				dbJobType = jobTypeRepo.UpdateJobType(dbJobType, inputJobType);
				base.LogTransactionUpdate<JobType>(GetOriginalValues<JobType>(dbJobType), dbJobType);
				UnitOfWork.Commit();
				return dbJobType.ToJobTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteJobType(string item)
		{
			try
			{
				IJobTypeRepo jobTypeRepo = new JobTypeRepo(db);
				Guid jobTypeGUID = new Guid(item);
				JobType jobType = jobTypeRepo.Find(jobTypeGUID);
				jobTypeRepo.Remove(jobType);
				base.LogTransactionDelete<JobType>(jobType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
