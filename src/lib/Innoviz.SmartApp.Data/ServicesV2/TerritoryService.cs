using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ITerritoryService
	{

		TerritoryItemView GetTerritoryById(string id);
		TerritoryItemView CreateTerritory(TerritoryItemView territoryView);
		TerritoryItemView UpdateTerritory(TerritoryItemView territoryView);
		bool DeleteTerritory(string id);
	}
	public class TerritoryService : SmartAppService, ITerritoryService
	{
		public TerritoryService(SmartAppDbContext context) : base(context) { }
		public TerritoryService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public TerritoryService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TerritoryService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public TerritoryService() : base() { }

		public TerritoryItemView GetTerritoryById(string id)
		{
			try
			{
				ITerritoryRepo territoryRepo = new TerritoryRepo(db);
				return territoryRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TerritoryItemView CreateTerritory(TerritoryItemView territoryView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				territoryView = accessLevelService.AssignOwnerBU(territoryView);
				ITerritoryRepo territoryRepo = new TerritoryRepo(db);
				Territory territory = territoryView.ToTerritory();
				territory = territoryRepo.CreateTerritory(territory);
				base.LogTransactionCreate<Territory>(territory);
				UnitOfWork.Commit();
				return territory.ToTerritoryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TerritoryItemView UpdateTerritory(TerritoryItemView territoryView)
		{
			try
			{
				ITerritoryRepo territoryRepo = new TerritoryRepo(db);
				Territory inputTerritory = territoryView.ToTerritory();
				Territory dbTerritory = territoryRepo.Find(inputTerritory.TerritoryGUID);
				dbTerritory = territoryRepo.UpdateTerritory(dbTerritory, inputTerritory);
				base.LogTransactionUpdate<Territory>(GetOriginalValues<Territory>(dbTerritory), dbTerritory);
				UnitOfWork.Commit();
				return dbTerritory.ToTerritoryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteTerritory(string item)
		{
			try
			{
				ITerritoryRepo territoryRepo = new TerritoryRepo(db);
				Guid territoryGUID = new Guid(item);
				Territory territory = territoryRepo.Find(territoryGUID);
				territoryRepo.Remove(territory);
				base.LogTransactionDelete<Territory>(territory);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
