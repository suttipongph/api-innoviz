using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDocumentConditionTemplateTableService
	{

		DocumentConditionTemplateLineItemView GetDocumentConditionTemplateLineById(string id);
		DocumentConditionTemplateLineItemView CreateDocumentConditionTemplateLine(DocumentConditionTemplateLineItemView documentConditionTemplateLineView);
		DocumentConditionTemplateLineItemView UpdateDocumentConditionTemplateLine(DocumentConditionTemplateLineItemView documentConditionTemplateLineView);
		bool DeleteDocumentConditionTemplateLine(string id);
		DocumentConditionTemplateTableItemView GetDocumentConditionTemplateTableById(string id);
		DocumentConditionTemplateTableItemView CreateDocumentConditionTemplateTable(DocumentConditionTemplateTableItemView documentConditionTemplateTableView);
		DocumentConditionTemplateTableItemView UpdateDocumentConditionTemplateTable(DocumentConditionTemplateTableItemView documentConditionTemplateTableView);
		bool DeleteDocumentConditionTemplateTable(string id);
		bool IsManualByDocumentConditionTemplateTable();
		public DocumentConditionTemplateLineItemView GetDocumentconditiontemplatelineInitialdata(string id);
		#region function
		CopyDocumentConditionTemplateResultView CopyDocumentConditionTemplate(CopyDocumentConditionTemplateParamView copyDocumentConditionTemplateParamView);
		void DeleteCopyDocumentConditionTemplateByDocType(DocConVerifyTypeModel doctype);
		#endregion
	}
    public class DocumentConditionTemplateTableService : SmartAppService, IDocumentConditionTemplateTableService
	{
		public DocumentConditionTemplateTableService(SmartAppDbContext context) : base(context) { }
		public DocumentConditionTemplateTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DocumentConditionTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentConditionTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DocumentConditionTemplateTableService() : base() { }

		#region DocumentConditionTemplateLine
		public DocumentConditionTemplateLineItemView GetDocumentConditionTemplateLineById(string id)
		{
			try
			{
				IDocumentConditionTemplateLineRepo documentConditionTemplateLineRepo = new DocumentConditionTemplateLineRepo(db);
				return documentConditionTemplateLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTemplateLineItemView CreateDocumentConditionTemplateLine(DocumentConditionTemplateLineItemView documentConditionTemplateLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentConditionTemplateLineView = accessLevelService.AssignOwnerBU(documentConditionTemplateLineView);
				IDocumentConditionTemplateLineRepo documentConditionTemplateLineRepo = new DocumentConditionTemplateLineRepo(db);
				DocumentConditionTemplateLine documentConditionTemplateLine = documentConditionTemplateLineView.ToDocumentConditionTemplateLine();
				documentConditionTemplateLine = documentConditionTemplateLineRepo.CreateDocumentConditionTemplateLine(documentConditionTemplateLine);
				base.LogTransactionCreate<DocumentConditionTemplateLine>(documentConditionTemplateLine);
				UnitOfWork.Commit();
				return documentConditionTemplateLine.ToDocumentConditionTemplateLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTemplateLineItemView UpdateDocumentConditionTemplateLine(DocumentConditionTemplateLineItemView documentConditionTemplateLineView)
		{
			try
			{
				IDocumentConditionTemplateLineRepo documentConditionTemplateLineRepo = new DocumentConditionTemplateLineRepo(db);
				DocumentConditionTemplateLine inputDocumentConditionTemplateLine = documentConditionTemplateLineView.ToDocumentConditionTemplateLine();
				DocumentConditionTemplateLine dbDocumentConditionTemplateLine = documentConditionTemplateLineRepo.Find(inputDocumentConditionTemplateLine.DocumentConditionTemplateLineGUID);
				dbDocumentConditionTemplateLine = documentConditionTemplateLineRepo.UpdateDocumentConditionTemplateLine(dbDocumentConditionTemplateLine, inputDocumentConditionTemplateLine);
				base.LogTransactionUpdate<DocumentConditionTemplateLine>(GetOriginalValues<DocumentConditionTemplateLine>(dbDocumentConditionTemplateLine), dbDocumentConditionTemplateLine);
				UnitOfWork.Commit();
				return dbDocumentConditionTemplateLine.ToDocumentConditionTemplateLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentConditionTemplateLine(string item)
		{
			try
			{
				IDocumentConditionTemplateLineRepo documentConditionTemplateLineRepo = new DocumentConditionTemplateLineRepo(db);
				Guid documentConditionTemplateLineGUID = new Guid(item);
				DocumentConditionTemplateLine documentConditionTemplateLine = documentConditionTemplateLineRepo.Find(documentConditionTemplateLineGUID);
				documentConditionTemplateLineRepo.Remove(documentConditionTemplateLine);
				base.LogTransactionDelete<DocumentConditionTemplateLine>(documentConditionTemplateLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public DocumentConditionTemplateLineItemView GetDocumentconditiontemplatelineInitialdata(string documentConditionTemplateTableGUID)
        {
            try
            {
                IDocumentConditionTemplateTableRepo documentconditiontemplatetablerepo = new DocumentConditionTemplateTableRepo(db);
				DocumentConditionTemplateTable documentConditionTemplateTable = documentconditiontemplatetablerepo.GetDocumentConditionTemplateTableNoTrackingByAccessLevel(documentConditionTemplateTableGUID.StringToGuid());

				return new DocumentConditionTemplateLineItemView
				{
					DocumentConditionTemplateLineGUID = new Guid().GuidNullToString(),
					DocumentConditionTemplateTableGUID = (documentConditionTemplateTable != null) ? documentConditionTemplateTable.DocumentConditionTemplateTableGUID.GuidNullToString() : null,
					DocumentType_Values = SmartAppUtil.GetDropDownLabel(documentConditionTemplateTable.DocumentConditionTemplateTableId, documentConditionTemplateTable.Description)
				};
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetDocumentConditionTemplateTable
        public DocumentConditionTemplateTableItemView GetDocumentConditionTemplateTableById(string id)
		{
			try
			{
				IDocumentConditionTemplateTableRepo documentConditionTemplateTableRepo = new DocumentConditionTemplateTableRepo(db);
				return documentConditionTemplateTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTemplateTableItemView CreateDocumentConditionTemplateTable(DocumentConditionTemplateTableItemView documentConditionTemplateTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentConditionTemplateTableView = accessLevelService.AssignOwnerBU(documentConditionTemplateTableView);
				IDocumentConditionTemplateTableRepo documentConditionTemplateTableRepo = new DocumentConditionTemplateTableRepo(db);
				DocumentConditionTemplateTable documentConditionTemplateTable = documentConditionTemplateTableView.ToDocumentConditionTemplateTable();
				GenDocumentConditionTemplateTableNumberSeqCode(documentConditionTemplateTable);
				documentConditionTemplateTable = documentConditionTemplateTableRepo.CreateDocumentConditionTemplateTable(documentConditionTemplateTable);
				base.LogTransactionCreate<DocumentConditionTemplateTable>(documentConditionTemplateTable);
				UnitOfWork.Commit();
				return documentConditionTemplateTable.ToDocumentConditionTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public DocumentConditionTemplateTableItemView UpdateDocumentConditionTemplateTable(DocumentConditionTemplateTableItemView documentConditionTemplateTableView)
		{
			try
			{
				IDocumentConditionTemplateTableRepo documentConditionTemplateTableRepo = new DocumentConditionTemplateTableRepo(db);
				DocumentConditionTemplateTable inputDocumentConditionTemplateTable = documentConditionTemplateTableView.ToDocumentConditionTemplateTable();
				DocumentConditionTemplateTable dbDocumentConditionTemplateTable = documentConditionTemplateTableRepo.Find(inputDocumentConditionTemplateTable.DocumentConditionTemplateTableGUID);
				dbDocumentConditionTemplateTable = documentConditionTemplateTableRepo.UpdateDocumentConditionTemplateTable(dbDocumentConditionTemplateTable, inputDocumentConditionTemplateTable);
				base.LogTransactionUpdate<DocumentConditionTemplateTable>(GetOriginalValues<DocumentConditionTemplateTable>(dbDocumentConditionTemplateTable), dbDocumentConditionTemplateTable);
				UnitOfWork.Commit();
				return dbDocumentConditionTemplateTable.ToDocumentConditionTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentConditionTemplateTable(string item)
		{
			try
			{
				IDocumentConditionTemplateTableRepo documentConditionTemplateTableRepo = new DocumentConditionTemplateTableRepo(db);
				Guid documentConditionTemplateTableGUID = new Guid(item);
				DocumentConditionTemplateTable documentConditionTemplateTable = documentConditionTemplateTableRepo.Find(documentConditionTemplateTableGUID);
				documentConditionTemplateTableRepo.Remove(documentConditionTemplateTable);
				base.LogTransactionDelete<DocumentConditionTemplateTable>(documentConditionTemplateTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenDocumentConditionTemplateTableNumberSeqCode(DocumentConditionTemplateTable documentConditionTemplateTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(documentConditionTemplateTable.CompanyGUID , ReferenceId.DocumentConditionTemplateTable);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(documentConditionTemplateTable.CompanyGUID, ReferenceId.DocumentConditionTemplateTable);
					documentConditionTemplateTable.DocumentConditionTemplateTableId = numberSequenceService.GetNumber(documentConditionTemplateTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public bool IsManualByDocumentConditionTemplateTable()
		{
			try
			{
				Guid companyId = GetCurrentCompany().StringToGuid();
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId( companyId, ReferenceId.DocumentConditionTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region function
		public CopyDocumentConditionTemplateResultView CopyDocumentConditionTemplate(CopyDocumentConditionTemplateParamView paramView)
		{
			try
			{
				IDocumentConditionTemplateLineRepo documentConditionTemplateLineRepo = new DocumentConditionTemplateLineRepo(db);
				NotificationResponse success = new NotificationResponse();
				CopyDocumentConditionTemplateResultView result = new CopyDocumentConditionTemplateResultView();
				DocConVerifyTypeModel docType = new DocConVerifyTypeModel() { RefGUID = paramView.RefGUID.StringToGuid(), DocConVerifyType = paramView.DocConVerifyType };
				
				IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
				List<DocumentConditionTrans> documentConditionTrans = documentConditionTransService.GetDocumentTransByDocType(docType).ToList();

				IEnumerable< DocumentConditionTemplateLine> documentConditionTemplateLines = 
					documentConditionTemplateLineRepo.GetDocumentConditionTemplateLineByDocumentConditionTemplateTableGUID(paramView.DocumentConditionTemplateTableGUID.StringToGuid());
				List<DocumentConditionTrans> documentConditionTranses = new List<DocumentConditionTrans>();
				documentConditionTemplateLines.ToList().ForEach(item =>
				{
					DocumentConditionTrans documentConditionTrans = new DocumentConditionTrans()
					{
						Mandatory = item.Mandatory,
						DocumentTypeGUID = item.DocumentTypeGUID,
						DocConVerifyType = paramView.DocConVerifyType,
						RefType = paramView.RefType,
						RefGUID = paramView.RefGUID.StringToGuid(),	
						CompanyGUID = item.CompanyGUID
					};
					documentConditionTranses.Add(documentConditionTrans);
				});
				using (var transaction = UnitOfWork.ContextTransaction())
				{
					this.BulkDelete(documentConditionTrans);
					this.BulkInsert(documentConditionTranses, true);
					UnitOfWork.Commit(transaction);
				}
				success.AddData("SUCCESS.90003", new string[] { paramView.CallerLabel, "LABEL.DOCUMENT_CONDITION_TRANSACTION", documentConditionTemplateLines.Count().ToString() });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}		
		public void DeleteCopyDocumentConditionTemplateByDocType(DocConVerifyTypeModel doctype)
        {			
            try
            {
				IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
				IEnumerable<DocumentConditionTrans> documentConditionTrans = documentConditionTransService.GetDocumentTransByDocType(doctype);
				documentConditionTrans.ToList().ForEach(item =>
				{
					documentConditionTransService.DeleteDocumentConditionTrans(item.DocumentConditionTransGUID.ToString());
				});

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		#endregion
	}
}
