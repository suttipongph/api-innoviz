using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICreditTypeService
	{

		CreditTypeItemView GetCreditTypeById(string id);
		CreditTypeItemView CreateCreditType(CreditTypeItemView creditTypeView);
		CreditTypeItemView UpdateCreditType(CreditTypeItemView creditTypeView);
		bool DeleteCreditType(string id);
	}
	public class CreditTypeService : SmartAppService, ICreditTypeService
	{
		public CreditTypeService(SmartAppDbContext context) : base(context) { }
		public CreditTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CreditTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CreditTypeService() : base() { }

		public CreditTypeItemView GetCreditTypeById(string id)
		{
			try
			{
				ICreditTypeRepo creditTypeRepo = new CreditTypeRepo(db);
				return creditTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditTypeItemView CreateCreditType(CreditTypeItemView creditTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				creditTypeView = accessLevelService.AssignOwnerBU(creditTypeView);
				ICreditTypeRepo creditTypeRepo = new CreditTypeRepo(db);
				CreditType creditType = creditTypeView.ToCreditType();
				creditType = creditTypeRepo.CreateCreditType(creditType);
				base.LogTransactionCreate<CreditType>(creditType);
				UnitOfWork.Commit();
				return creditType.ToCreditTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditTypeItemView UpdateCreditType(CreditTypeItemView creditTypeView)
		{
			try
			{
				ICreditTypeRepo creditTypeRepo = new CreditTypeRepo(db);
				CreditType inputCreditType = creditTypeView.ToCreditType();
				CreditType dbCreditType = creditTypeRepo.Find(inputCreditType.CreditTypeGUID);
				dbCreditType = creditTypeRepo.UpdateCreditType(dbCreditType, inputCreditType);
				base.LogTransactionUpdate<CreditType>(GetOriginalValues<CreditType>(dbCreditType), dbCreditType);
				UnitOfWork.Commit();
				return dbCreditType.ToCreditTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCreditType(string item)
		{
			try
			{
				ICreditTypeRepo creditTypeRepo = new CreditTypeRepo(db);
				Guid creditTypeGUID = new Guid(item);
				CreditType creditType = creditTypeRepo.Find(creditTypeGUID);
				creditTypeRepo.Remove(creditType);
				base.LogTransactionDelete<CreditType>(creditType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
