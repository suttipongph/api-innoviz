using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IIntercompanyInvoiceSettlementService
	{

		IntercompanyInvoiceSettlementItemView GetIntercompanyInvoiceSettlementById(string id);
		IntercompanyInvoiceSettlementItemView CreateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementView);
		IntercompanyInvoiceSettlementItemView UpdateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementView);
		IntercompanyInvoiceSettlementItemView GetIntercompanyInvoiceSettlementInitialData(string id);

		bool DeleteIntercompanyInvoiceSettlement(string id);
		AccessModeView GetAccessModeByIntercompanyInvoiceTable(string refId);

		#region Function
		#region post intercompany invoice settlement
		PostIntercompanyInvSettlementView GetPostIntercompanyInvSettlementById(string intercompanyInvoiceSettlementGUID);
		PostIntercompanyInvSettlementViewMap InitPostIntercompanyInvSettlement(Guid intercompanyInvoiceSettlementGUID);
		PostIntercompanyInvSettlementResultView PostIntercompanyInvSettlement(PostIntercompanyInvSettlementView view);
		bool GetPostIntercompanyInvSettlementValidation(Guid intercompanyInvoiceSettlementGUID);
		#endregion post free text invoice
		#region cancel intercompany invoice settlement
		CancelIntercompanyInvSettlementView GetCancelIntercompanyInvSettlementById(string intercompanyInvoiceSettlementGUID);
		CancelIntercompanyInvSettlementViewMap InitCancelIntercompanyInvSettlement(CancelIntercompanyInvSettlementView cancelIntercompanyInvSettlementView);
		CancelIntercompanyInvSettlementResultView CancelIntercompanyInvSettlement(CancelIntercompanyInvSettlementView view);
		bool GetCancelIntercompanyInvSettlementValidation(CancelIntercompanyInvSettlementView cancelIntercompanyInvSettlementView);
		bool GetCancelIntercompanyInvSettlementMenuValidation(IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView);
		#endregion cancel free text invoice
		#endregion Function
	}
	public class IntercompanyInvoiceSettlementService : SmartAppService, IIntercompanyInvoiceSettlementService
	{
		public IntercompanyInvoiceSettlementService(SmartAppDbContext context) : base(context) { }
		public IntercompanyInvoiceSettlementService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public IntercompanyInvoiceSettlementService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntercompanyInvoiceSettlementService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public IntercompanyInvoiceSettlementService() : base() { }

		public IntercompanyInvoiceSettlementItemView GetIntercompanyInvoiceSettlementById(string id)
		{
			try
			{
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				return intercompanyInvoiceSettlementRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceSettlementItemView CreateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementView)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				intercompanyInvoiceSettlementView = accessLevelService.AssignOwnerBU(intercompanyInvoiceSettlementView);
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IntercompanyInvoiceSettlement intercompanyInvoiceSettlement = intercompanyInvoiceSettlementView.ToIntercompanyInvoiceSettlement();
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				var intercompanyInvoiceTable = intercompanyInvoiceTableRepo.GetByIdvw(intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID);
				if(intercompanyInvoiceTable.InvoiceAmount> 0 && intercompanyInvoiceSettlement.SettleInvoiceAmount > intercompanyInvoiceTable.Balance)
                {
					ex.AddData("ERROR.90031", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT", "LABEL.BALANCE" });
				}
				if (intercompanyInvoiceTable.InvoiceAmount < 0 && intercompanyInvoiceSettlement.SettleInvoiceAmount < intercompanyInvoiceTable.Balance)
				{
					ex.AddData("ERROR.90031", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT", "LABEL.BALANCE" });
				}
				if (intercompanyInvoiceSettlement.SettleInvoiceAmount == 0)
				{
					ex.AddData("ERROR.90049", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT" });
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				intercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.CreateIntercompanyInvoiceSettlement(intercompanyInvoiceSettlement);
				base.LogTransactionCreate<IntercompanyInvoiceSettlement>(intercompanyInvoiceSettlement);
				UnitOfWork.Commit();
				return intercompanyInvoiceSettlement.ToIntercompanyInvoiceSettlementItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceSettlementItemView UpdateIntercompanyInvoiceSettlement(IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementView)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IntercompanyInvoiceSettlement inputIntercompanyInvoiceSettlement = intercompanyInvoiceSettlementView.ToIntercompanyInvoiceSettlement();
				IntercompanyInvoiceSettlement dbIntercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.Find(inputIntercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID);
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				var intercompanyInvoiceTable = intercompanyInvoiceTableRepo.GetByIdvw(inputIntercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID);
				if (intercompanyInvoiceTable.InvoiceAmount > 0 && inputIntercompanyInvoiceSettlement.SettleInvoiceAmount > intercompanyInvoiceTable.Balance)
				{
					ex.AddData("ERROR.90031", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT", "LABEL.BALANCE" });
				}
				if (intercompanyInvoiceTable.InvoiceAmount < 0 && inputIntercompanyInvoiceSettlement.SettleInvoiceAmount < intercompanyInvoiceTable.Balance)
				{
					ex.AddData("ERROR.90031", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT", "LABEL.BALANCE" });
				}
				if (inputIntercompanyInvoiceSettlement.SettleInvoiceAmount == 0)
				{
					ex.AddData("ERROR.90049", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT" });
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}





				dbIntercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.UpdateIntercompanyInvoiceSettlement(dbIntercompanyInvoiceSettlement, inputIntercompanyInvoiceSettlement);
				base.LogTransactionUpdate<IntercompanyInvoiceSettlement>(GetOriginalValues<IntercompanyInvoiceSettlement>(dbIntercompanyInvoiceSettlement), dbIntercompanyInvoiceSettlement);
				UnitOfWork.Commit();
				return dbIntercompanyInvoiceSettlement.ToIntercompanyInvoiceSettlementItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteIntercompanyInvoiceSettlement(string item)
		{
			try
			{
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				Guid intercompanyInvoiceSettlementGUID = new Guid(item);
				IntercompanyInvoiceSettlement intercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.Find(intercompanyInvoiceSettlementGUID);
				intercompanyInvoiceSettlementRepo.Remove(intercompanyInvoiceSettlement);
				base.LogTransactionDelete<IntercompanyInvoiceSettlement>(intercompanyInvoiceSettlement);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyInvoiceSettlementItemView GetIntercompanyInvoiceSettlementInitialData(string id)
		{
			try
			{				
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				var documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)IntercompanyInvoiceSettlementStatus.Draft).ToString());
				IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView = new IntercompanyInvoiceSettlementItemView();
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				var intercompanyInvoiceTable = intercompanyInvoiceTableRepo.GetByIdvw(id.StringToGuid());
				intercompanyInvoiceSettlementItemView.IntercompanyInvoice_Values = SmartAppUtil.GetDropDownLabel(intercompanyInvoiceTable.IntercompanyInvoiceId, intercompanyInvoiceTable.IssuedDate);
				intercompanyInvoiceSettlementItemView.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
				intercompanyInvoiceSettlementItemView.IntercompanyInvoiceTableGUID = intercompanyInvoiceTable.IntercompanyInvoiceTableGUID;
				intercompanyInvoiceSettlementItemView.TransDate = DateTime.Now.DateToString();
				intercompanyInvoiceSettlementItemView.DocumentStatus_Values = documentStatus.Description;
			
				intercompanyInvoiceSettlementItemView.DocumentStatus_StatusId = documentStatus.StatusId;
				intercompanyInvoiceSettlementItemView.CNReasonGUID = intercompanyInvoiceTable.CNReasonGUID;
				intercompanyInvoiceSettlementItemView.CNReason_Values = intercompanyInvoiceTable.CNReason_Values;
				intercompanyInvoiceSettlementItemView.OrigInvoiceId = intercompanyInvoiceTable.OrigInvoiceId;
				intercompanyInvoiceSettlementItemView.OrigInvoiceAmount = intercompanyInvoiceTable.OrigInvoiceAmount;
				intercompanyInvoiceSettlementItemView.OrigTaxInvoiceId = intercompanyInvoiceTable.OrigTaxInvoiceId;
				intercompanyInvoiceSettlementItemView.OrigTaxInvoiceAmount = intercompanyInvoiceTable.OrigTaxInvoiceAmount;
				intercompanyInvoiceSettlementItemView.IntercomapnyInvoice_IssuedDate = intercompanyInvoiceTable.IssuedDate;
				intercompanyInvoiceSettlementItemView.IntercomapnyInvoice_InvoiceAmount = intercompanyInvoiceTable.InvoiceAmount;
				return intercompanyInvoiceSettlementItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}


        public AccessModeView GetAccessModeByIntercompanyInvoiceTable(string refId)
        {
            try {
				AccessModeView accessModeView = new AccessModeView();
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				decimal intercompanyInvoiceTable = intercompanyInvoiceTableRepo.GetByIdvw(refId.StringToGuid()).InvoiceAmount;
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				var postedStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(IntercompanyInvoiceSettlementStatus.Posted).ToString());
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				decimal sumSettleInvoiceAmount = intercompanyInvoiceSettlementRepo.GetSumSettleInvoiceAmount(refId.StringToGuid(), postedStatus.DocumentStatusGUID);
			    bool isResult = (intercompanyInvoiceTable != sumSettleInvoiceAmount); 
				accessModeView.CanCreate = isResult;
				return accessModeView;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region function
		#region post intercompanyInvoiceSettlement
		public PostIntercompanyInvSettlementView GetPostIntercompanyInvSettlementById(string intercompanyInvoiceSettlementGUID)
		{
			try
			{
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView = intercompanyInvoiceSettlementRepo.GetByIdvw(intercompanyInvoiceSettlementGUID.StringToGuid());
				PostIntercompanyInvSettlementView postIntercompanyInvSettlementView = new PostIntercompanyInvSettlementView
				{
					IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementGUID,
					DocumentStatus_Values = intercompanyInvoiceSettlementItemView.DocumentStatus_Values,
					IntercompanyInvoice_Values = intercompanyInvoiceSettlementItemView.IntercompanyInvoice_Values,
					MethodOfPayment_Values = intercompanyInvoiceSettlementItemView.MethodOfPayment_Values,
					SettleInvoiceAmount = intercompanyInvoiceSettlementItemView.SettleInvoiceAmount,
					TransDate = intercompanyInvoiceSettlementItemView.TransDate
				};
				return postIntercompanyInvSettlementView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostIntercompanyInvSettlementResultView PostIntercompanyInvSettlement(PostIntercompanyInvSettlementView view)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				PostIntercompanyInvSettlementResultView result = new PostIntercompanyInvSettlementResultView();
				NotificationResponse success = new NotificationResponse();
				PostIntercompanyInvSettlementViewMap postIntercompanyInvSettlement = InitPostIntercompanyInvSettlement(view.IntercompanyInvoiceSettlementGUID.StringToGuid());

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					#region Create staging table invoiceco inv settle
					// 1. Call method : Generate IntercompanySettlementStaging
					this.BulkInsert(postIntercompanyInvSettlement.StagingTableIntercoInvSettle.FirstToList(), false);
					#endregion Insert data from post invoice

					#region Update IntercompanyInvoiceSettlement
					// 1. Call method : Generate IntercompanySettlementStaging
					// 2. Update Intercompany invoice settlement status = Posted
					this.BulkUpdate(postIntercompanyInvSettlement.IntercompanyInvoiceSettlement.FirstToList());
					#endregion Update IntercompanyInvoiceSettlement

					UnitOfWork.Commit(transaction);
				}

				success.AddData("SUCCESS.90004", new string[] { "LABEL.INTERCOMPANY_INVOICE_SETTLEMENT", postIntercompanyInvSettlement.IntercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostIntercompanyInvSettlementViewMap InitPostIntercompanyInvSettlement(Guid intercompanyInvoiceSettlementGUID)
		{
			try
			{
				IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db);
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				StagingTableIntercoInvSettle createStagingTableIntercoInvSettle = new StagingTableIntercoInvSettle();
				PostIntercompanyInvSettlementResultView result = new PostIntercompanyInvSettlementResultView();
				NotificationResponse success = new NotificationResponse();
				string intercompanyInvoiceSettlementId = null;

				#region Validate
				GetPostIntercompanyInvSettlementValidation(intercompanyInvoiceSettlementGUID);
				#endregion Validate

				#region 1. Call method : Generate IntercompanySettlementStaging
				IntercompanyInvoiceSettlement intercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.GetIntercompanyInvoiceSettlementByIdNoTracking(intercompanyInvoiceSettlementGUID);
				GenIntercompanySettlementStagingView genIntercompanySettlementStagingView = stagingTableIntercoInvSettleService.GenIntercompanySettlementStaging(intercompanyInvoiceSettlement);
				intercompanyInvoiceSettlementId = genIntercompanySettlementStagingView.IntercompanyInvoiceSettlementId;
				createStagingTableIntercoInvSettle = genIntercompanySettlementStagingView.StagingTableIntercoInvSettle;
				#endregion 1. Call method : Generate IntercompanySettlementStaging

				#region 2. Update Intercompany invoice settlement status = Posted
				DocumentStatus intercompanyInvoiceSettlementStatusPosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)IntercompanyInvoiceSettlementStatus.Posted).ToString());
				intercompanyInvoiceSettlement.DocumentStatusGUID = intercompanyInvoiceSettlementStatusPosted.DocumentStatusGUID;
				intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlementId;
				intercompanyInvoiceSettlement.ModifiedBy = db.GetUserName();
				intercompanyInvoiceSettlement.ModifiedDateTime = DateTime.Now;
				#endregion 2. Update Intercompany invoice settlement status = Posted

				#region Setup data from post intercompanyInvoiceSettlement
				PostIntercompanyInvSettlementViewMap postIntercompanyInvSettlement = new PostIntercompanyInvSettlementViewMap
				{
					IntercompanyInvoiceSettlement = intercompanyInvoiceSettlement,
					StagingTableIntercoInvSettle = createStagingTableIntercoInvSettle
				};
				#endregion Setup data from post intercompanyInvoiceSettlement

				return postIntercompanyInvSettlement;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetPostIntercompanyInvSettlementValidation(Guid intercompanyInvoiceSettlementGUID)
		{
			try
			{
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IntercompanyInvoiceSettlement intercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.GetIntercompanyInvoiceSettlementByIdNoTracking(intercompanyInvoiceSettlementGUID);
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				#region Validate
				// Validate No.1 && Validate No.2
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				IntercompanyInvoiceTable intercompanyInvoiceTable = intercompanyInvoiceTableRepo.GetIntercompanyInvoiceTableByIdNoTracking(intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID);
				DocumentStatus intercompanyInvoiceSettlementStatusPosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)IntercompanyInvoiceSettlementStatus.Posted).ToString());
				decimal sumSettleInvoiceAmount = intercompanyInvoiceSettlementRepo.GetSumSettleInvoiceAmount(intercompanyInvoiceTable.IntercompanyInvoiceTableGUID, intercompanyInvoiceSettlementStatusPosted.DocumentStatusGUID);
				decimal balance = intercompanyInvoiceTable.InvoiceAmount - sumSettleInvoiceAmount;

				// Validate No.3
				ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
				bool isPeriodStatusOpen = ledgerFiscalService.IsPeriodStatusOpen(intercompanyInvoiceSettlement.CompanyGUID, intercompanyInvoiceSettlement.TransDate);

				if (intercompanyInvoiceTable.InvoiceAmount > 0 && intercompanyInvoiceSettlement.SettleInvoiceAmount > balance) // Validate No.1
                {
                    ex.AddData("ERROR.90031", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT",	"LABEL.BALANCE" });
                }
                if (intercompanyInvoiceTable.InvoiceAmount < 0 && intercompanyInvoiceSettlement.SettleInvoiceAmount < balance) // Validate No.2
				{
					ex.AddData("ERROR.90131", new string[] { "LABEL.SETTLE_INVOICE_AMOUNT", "LABEL.BALANCE" });
				}
                if (!isPeriodStatusOpen) // Validate No.3
                {
                    ex.AddData("ERROR.90048", new string[] { intercompanyInvoiceSettlement.TransDate.DateToString() });
                }
                #endregion Validate

                if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region cancel intercompanyInvoiceSettlement
		public CancelIntercompanyInvSettlementView GetCancelIntercompanyInvSettlementById(string intercompanyInvoiceSettlementGUID)
		{
			try
			{
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView = intercompanyInvoiceSettlementRepo.GetByIdvw(intercompanyInvoiceSettlementGUID.StringToGuid());
				CancelIntercompanyInvSettlementView cancelIntercompanyInvSettlementView = new CancelIntercompanyInvSettlementView
				{
					CancelDate = intercompanyInvoiceSettlementItemView.TransDate,
					DocumentStatus_Values = intercompanyInvoiceSettlementItemView.DocumentStatus_Values,
					IntercompanyInvoice_Values = intercompanyInvoiceSettlementItemView.IntercompanyInvoice_Values,
					IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementGUID,
					IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementId,
					MethodOfPayment_Values = intercompanyInvoiceSettlementItemView.MethodOfPayment_Values,
					SettleAmount = intercompanyInvoiceSettlementItemView.SettleAmount,
					SettleWHTAmount = intercompanyInvoiceSettlementItemView.SettleWHTAmount,
					SettleInvoiceAmount = intercompanyInvoiceSettlementItemView.SettleInvoiceAmount,
				};
				return cancelIntercompanyInvSettlementView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CancelIntercompanyInvSettlementResultView CancelIntercompanyInvSettlement(CancelIntercompanyInvSettlementView view)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				CancelIntercompanyInvSettlementResultView result = new CancelIntercompanyInvSettlementResultView();
				NotificationResponse success = new NotificationResponse();
				CancelIntercompanyInvSettlementViewMap cancelIntercompanyInvSettlement = InitCancelIntercompanyInvSettlement(view);

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					this.BulkInsert(cancelIntercompanyInvSettlement.CreateIntercompanyInvoiceSettlement.FirstToList(), false);
					this.BulkInsert(cancelIntercompanyInvSettlement.StagingTableIntercoInvSettle.FirstToList(), false);
					this.BulkUpdate(cancelIntercompanyInvSettlement.UpdateIntercompanyInvoiceSettlement.FirstToList());

					UnitOfWork.Commit(transaction);
				}

				success.AddData("SUCCESS.90008", new string[] { "LABEL.INTERCOMPANY_INVOICE_SETTLEMENT", cancelIntercompanyInvSettlement.UpdateIntercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CancelIntercompanyInvSettlementViewMap InitCancelIntercompanyInvSettlement(CancelIntercompanyInvSettlementView cancelIntercompanyInvSettlementView)
		{
			try
			{
				IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db);
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				StagingTableIntercoInvSettle createStagingTableIntercoInvSettle = new StagingTableIntercoInvSettle();
				CancelIntercompanyInvSettlementResultView result = new CancelIntercompanyInvSettlementResultView();
				NotificationResponse success = new NotificationResponse();
				string intercompanyInvoiceSettlementId = null;

				#region Validate
				GetCancelIntercompanyInvSettlementValidation(cancelIntercompanyInvSettlementView);
				#endregion Validate

				#region 1. Create Intercompany invoice settlement status Cancelled 
				IntercompanyInvoiceSettlement intercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.GetIntercompanyInvoiceSettlementByIdNoTracking(cancelIntercompanyInvSettlementView.IntercompanyInvoiceSettlementGUID.StringToGuid());
				DocumentStatus intercompanyInvoiceSettlementStatusCancelled = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)IntercompanyInvoiceSettlementStatus.Cancelled).ToString());
				IntercompanyInvoiceSettlement createIntercompanyInvoiceSettlement = new IntercompanyInvoiceSettlement
				{
					IntercompanyInvoiceTableGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID,
					TransDate = cancelIntercompanyInvSettlementView.CancelDate.StringToDate(),
					DocumentStatusGUID = intercompanyInvoiceSettlementStatusCancelled.DocumentStatusGUID,
					WHTSlipReceivedByCustomer = intercompanyInvoiceSettlement.WHTSlipReceivedByCustomer,
					SettleAmount = intercompanyInvoiceSettlement.SettleAmount * (-1),
					SettleWHTAmount = intercompanyInvoiceSettlement.SettleWHTAmount * (-1),
					SettleInvoiceAmount = intercompanyInvoiceSettlement.SettleInvoiceAmount * (-1),
					MethodOfPaymentGUID = intercompanyInvoiceSettlement.MethodOfPaymentGUID,
					RefIntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID,
					CNReasonGUID = cancelIntercompanyInvSettlementView.CNReasonGUID.StringToGuid(),
					OrigInvoiceId = cancelIntercompanyInvSettlementView.OrigInvoiceId,
					OrigInvoiceAmount = cancelIntercompanyInvSettlementView.OrigInvoiceAmount,
					OrigTaxInvoiceId = cancelIntercompanyInvSettlementView.OrigTaxInvoiceId,
					OrigTaxInvoiceAmount = cancelIntercompanyInvSettlementView.OrigTaxInvoiceAmount,
					
					IntercompanyInvoiceSettlementGUID = Guid.NewGuid(),
					CompanyGUID = intercompanyInvoiceSettlement.CompanyGUID
				};
				#endregion 1. Create Intercompany invoice settlement status Cancelled 

				#region 2. Call method : Generate IntercompanySettlementStaging
				GenIntercompanySettlementStagingView genIntercompanySettlementStagingView = stagingTableIntercoInvSettleService.GenIntercompanySettlementStaging(createIntercompanyInvoiceSettlement);
				intercompanyInvoiceSettlementId = genIntercompanySettlementStagingView.IntercompanyInvoiceSettlementId;
				createStagingTableIntercoInvSettle = genIntercompanySettlementStagingView.StagingTableIntercoInvSettle;
				#endregion 2. Call method : Generate IntercompanySettlementStaging

				#region 3. Update IntercompanyInvoiceSettlementId from No.1
				createIntercompanyInvoiceSettlement.IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlementId;
				#endregion 3. Update IntercompanyInvoiceSettlementId from No.1

				#region 4. Update Intercompany invoice settlement status = Canceled
				intercompanyInvoiceSettlement.DocumentStatusGUID = intercompanyInvoiceSettlementStatusCancelled.DocumentStatusGUID;
				intercompanyInvoiceSettlement.RefIntercompanyInvoiceSettlementGUID = createIntercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID;
				intercompanyInvoiceSettlement.ModifiedBy = db.GetUserName();
				intercompanyInvoiceSettlement.ModifiedDateTime = DateTime.Now;
				#endregion 4. Update Intercompany invoice settlement status = Canceled

				#region Setup data from cancel intercompanyInvoiceSettlement
				CancelIntercompanyInvSettlementViewMap cancelIntercompanyInvSettlement = new CancelIntercompanyInvSettlementViewMap
				{
					CreateIntercompanyInvoiceSettlement = createIntercompanyInvoiceSettlement,
					UpdateIntercompanyInvoiceSettlement = intercompanyInvoiceSettlement,
					StagingTableIntercoInvSettle = createStagingTableIntercoInvSettle
				};
				#endregion Setup data from cancel intercompanyInvoiceSettlement

				return cancelIntercompanyInvSettlement;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetCancelIntercompanyInvSettlementValidation(CancelIntercompanyInvSettlementView cancelIntercompanyInvSettlementView)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				#region Validate
				// Validate No.1
				ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
				bool isPeriodStatusOpen = ledgerFiscalService.IsPeriodStatusOpen(GetCurrentCompany().StringToGuid(), cancelIntercompanyInvSettlementView.CancelDate.StringToDate());

				if (!isPeriodStatusOpen) // Validate No.1
				{
					ex.AddData("ERROR.90048", new string[] { cancelIntercompanyInvSettlementView.CancelDate });
				}
				#endregion Validate

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetCancelIntercompanyInvSettlementMenuValidation(IntercompanyInvoiceSettlementItemView intercompanyInvoiceSettlementItemView)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				#region Validate
				// Validate No.1
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				IntercompanyInvoiceSettlement intercompanyInvoiceSettlement = intercompanyInvoiceSettlementRepo.GetIntercompanyInvoiceSettlementByIdNoTracking(intercompanyInvoiceSettlementItemView.IntercompanyInvoiceSettlementGUID.StringToGuid());
				DocumentStatus intercompanyInvoiceSettlementStatusPosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)IntercompanyInvoiceSettlementStatus.Posted).ToString());
				
				if (intercompanyInvoiceSettlement.DocumentStatusGUID != intercompanyInvoiceSettlementStatusPosted.DocumentStatusGUID || intercompanyInvoiceSettlement.RefIntercompanyInvoiceSettlementGUID != null) // Validate No.1
				{
					ex.AddData("ERROR.90166", new string[] { "LABEL.INTERCOMPANY_INVOICE_SETTLEMENT", "LABEL.REFERENCE_INTERCOMPANY_INVOICE_SETTLEMENT_ID" });
				}
				#endregion Validate

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion function
	}
}
