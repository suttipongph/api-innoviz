using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDocumentReasonService
	{

		DocumentReasonItemView GetDocumentReasonById(string id);
		DocumentReasonItemView CreateDocumentReason(DocumentReasonItemView documentReasonView);
		DocumentReasonItemView UpdateDocumentReason(DocumentReasonItemView documentReasonView);
		IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItemDocumentReason(SearchParameter search);
		bool DeleteDocumentReason(string id);
	}
	public class DocumentReasonService : SmartAppService, IDocumentReasonService
	{
		public DocumentReasonService(SmartAppDbContext context) : base(context) { }
		public DocumentReasonService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DocumentReasonService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentReasonService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DocumentReasonService() : base() { }

		public DocumentReasonItemView GetDocumentReasonById(string id)
		{
			try
			{
				IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
				return documentReasonRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReasonItemView CreateDocumentReason(DocumentReasonItemView documentReasonView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentReasonView = accessLevelService.AssignOwnerBU(documentReasonView);
				IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
				DocumentReason documentReason = documentReasonView.ToDocumentReason();
				documentReason = documentReasonRepo.CreateDocumentReason(documentReason);
				base.LogTransactionCreate<DocumentReason>(documentReason);
				UnitOfWork.Commit();
				return documentReason.ToDocumentReasonItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReasonItemView UpdateDocumentReason(DocumentReasonItemView documentReasonView)
		{
			try
			{
				IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
				DocumentReason inputDocumentReason = documentReasonView.ToDocumentReason();
				DocumentReason dbDocumentReason = documentReasonRepo.Find(inputDocumentReason.DocumentReasonGUID);
				dbDocumentReason = documentReasonRepo.UpdateDocumentReason(dbDocumentReason, inputDocumentReason);
				base.LogTransactionUpdate<DocumentReason>(GetOriginalValues<DocumentReason>(dbDocumentReason), dbDocumentReason);
				UnitOfWork.Commit();
				return dbDocumentReason.ToDocumentReasonItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentReason(string item)
		{
			try
			{
				IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
				Guid documentReasonGUID = new Guid(item);
				DocumentReason documentReason = documentReasonRepo.Find(documentReasonGUID);
				documentReasonRepo.Remove(documentReason);
				base.LogTransactionDelete<DocumentReason>(documentReason);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItemDocumentReason(SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				search.GetParentCondition(DocumentReasonCondition.RefType);
				return sysDropDownService.GetDropDownItemDocumentReason(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
