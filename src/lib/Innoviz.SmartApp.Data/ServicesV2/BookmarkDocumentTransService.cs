using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBookmarkDocumentTransService
	{

		BookmarkDocumentTransItemView GetBookmarkDocumentTransById(string id);
		BookmarkDocumentTransItemView CreateBookmarkDocumentTrans(BookmarkDocumentTransItemView bookmarkDocumentTransView);
		BookmarkDocumentTransItemView UpdateBookmarkDocumentTrans(BookmarkDocumentTransItemView bookmarkDocumentTransView);
		bool DeleteBookmarkDocumentTrans(string id);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemBookMarkDocumentStatus(SearchParameter search);
		BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refId, string refGUID, RefType refType);
		bool GetPrintBookDocTransValidation(BookmarkDocumentTransItemView bookmarkDocumentTransItemView);
		bool GetPrintSetBookDocTransValidation(PrintSetBookDocTransParm printSetBookDocTransParm);
		#region Shared
		FileInformation GenBookmarkDocumentFile(GenBookmarkDocumentFileParamViewMap template);
		List<FileInformation> GenBookmarkDocumentFile(IEnumerable<GenBookmarkDocumentFileParamViewMap> templates);
		FileInformation GenBookmarkDocumentFile<T>(GenBookmarkDocumentFileParamViewMap template, T data, string fileName = null);
		Task<FileInformation> GetDocumentTemplateByDocumentTemplateTableGUID(PrintBookDocTransParm printBookDocTransParm);
		Task<List<FileInformation>> GetDocumentTemplateByRefGUID(PrintSetBookDocTransParm printSetBookDocTransParm);
		#endregion
	}
	public class BookmarkDocumentTransService : SmartAppService, IBookmarkDocumentTransService
	{
		public BookmarkDocumentTransService(SmartAppDbContext context) : base(context) { }
		public BookmarkDocumentTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BookmarkDocumentTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BookmarkDocumentTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BookmarkDocumentTransService() : base() { }

		public BookmarkDocumentTransItemView GetBookmarkDocumentTransById(string id)
		{
			try
			{
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				return bookmarkDocumentTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTransItemView CreateBookmarkDocumentTrans(BookmarkDocumentTransItemView bookmarkDocumentTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				bookmarkDocumentTransView = accessLevelService.AssignOwnerBU(bookmarkDocumentTransView);
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				BookmarkDocumentTrans bookmarkDocumentTrans = bookmarkDocumentTransView.ToBookmarkDocumentTrans();
				bookmarkDocumentTrans = bookmarkDocumentTransRepo.CreateBookmarkDocumentTrans(bookmarkDocumentTrans);
				base.LogTransactionCreate<BookmarkDocumentTrans>(bookmarkDocumentTrans);
				UnitOfWork.Commit();
				return bookmarkDocumentTrans.ToBookmarkDocumentTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTransItemView UpdateBookmarkDocumentTrans(BookmarkDocumentTransItemView bookmarkDocumentTransView)
		{
			try
			{
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				BookmarkDocumentTrans inputBookmarkDocumentTrans = bookmarkDocumentTransView.ToBookmarkDocumentTrans();
				BookmarkDocumentTrans dbBookmarkDocumentTrans = bookmarkDocumentTransRepo.Find(inputBookmarkDocumentTrans.BookmarkDocumentTransGUID);
				dbBookmarkDocumentTrans = bookmarkDocumentTransRepo.UpdateBookmarkDocumentTrans(dbBookmarkDocumentTrans, inputBookmarkDocumentTrans);
				base.LogTransactionUpdate<BookmarkDocumentTrans>(GetOriginalValues<BookmarkDocumentTrans>(dbBookmarkDocumentTrans), dbBookmarkDocumentTrans);
				UnitOfWork.Commit();
				return dbBookmarkDocumentTrans.ToBookmarkDocumentTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBookmarkDocumentTrans(string item)
		{
			try
			{
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				Guid bookmarkDocumentTransGUID = new Guid(item);
				BookmarkDocumentTrans bookmarkDocumentTrans = bookmarkDocumentTransRepo.Find(bookmarkDocumentTransGUID);
				bookmarkDocumentTransRepo.Remove(bookmarkDocumentTrans);
				base.LogTransactionDelete<BookmarkDocumentTrans>(bookmarkDocumentTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemBookMarkDocumentStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.BookmarkDocument);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(Convert.ToInt32(BookMarkDocumentStatus.Draft).ToString());
				return new BookmarkDocumentTransItemView
				{
					BookmarkDocumentTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
					DocumentStatus_StatusId = documentStatus.StatusId
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetPrintBookDocTransValidation(BookmarkDocumentTransItemView bookmarkDocumentTransItemView)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				string[] statusUseInProcess = new string[]
				{	
					((int)BookMarkDocumentStatus.Draft).ToString(), 
					((int)BookMarkDocumentStatus.InProgress).ToString(), 
					((int)BookMarkDocumentStatus.Completed).ToString() 
				};
				List<Guid> statusUseInProcessGUID = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(statusUseInProcess).Select(s=> s.DocumentStatusGUID).ToList();
				BookmarkDocumentTrans bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByIdNoTracking(bookmarkDocumentTransItemView.BookmarkDocumentTransGUID.StringToGuid());

				if (!statusUseInProcessGUID.Contains(bookmarkDocumentTrans.DocumentStatusGUID))
				{
					ex.AddData("ERROR.90117");
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetPrintSetBookDocTransValidation(PrintSetBookDocTransParm printSetBookDocTransParm)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				DocumentStatus completedBookmark = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
				IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTransByRefType = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType(printSetBookDocTransParm.RefType, printSetBookDocTransParm.RefGUID.StringToGuid());
				bool isNotBookmarkDocumentTransStatusCompleted = ConditionService.IsEqualZero(bookmarkDocumentTransByRefType.Where(w => w.DocumentStatusGUID == completedBookmark.DocumentStatusGUID).Count());

				if (isNotBookmarkDocumentTransStatusCompleted)
				{
					ex.AddData("ERROR.90117");
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Shared
		public FileInformation GenBookmarkDocumentFile(GenBookmarkDocumentFileParamViewMap template)
		{
			try
			{
				IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
				IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
				IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
				IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
				ICustomerTableService customerTableService = new CustomerTableService(db);
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppTableService creditAppTableService = new CreditAppTableService(db);

				DocumentTemplateType inputDocType = (DocumentTemplateType)template.DocumentTemplateType;

				switch (inputDocType)
				{
					case DocumentTemplateType.MainAgreementJv:
                        GenMainAgreementBookmarkSharedJVView mainAgreementBookmarkSharedJvView = mainAgreementTableService.GetMainAgreementBookmarkSharedJVView(template.RefGUID, template.BookmarkDocumentTransGUID);
                        return GenBookmarkDocumentFile(template, mainAgreementBookmarkSharedJvView, null);

					case DocumentTemplateType.MainAgreementExpenseNotice:
						GenMainAgreementExpenceDetailView modelGenMainAgreementExpenceDetailView = mainAgreementTableService.GenMainAgreementExpenceDetailView(template.RefGUID);
						return GenBookmarkDocumentFile(template, modelGenMainAgreementExpenceDetailView, null);
					
					case DocumentTemplateType.MainAgreementShared:
                        GenMainAgreementBookmarkSharedView modelMainAgreementBookmarkSharedView = mainAgreementTableService.GetMainAgreementBookmarkShared(template.RefGUID, template.BookmarkDocumentTransGUID);
                        return GenBookmarkDocumentFile(template, modelMainAgreementBookmarkSharedView, null);
					
					case DocumentTemplateType.MainAgreementPf:
						MainAgreementPFBookmarkView modelMainAgreementPFBookmarkView = mainAgreementTableService.GetMainAgreementPFAndSharedBookmarkValue(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, modelMainAgreementPFBookmarkView, null);

					case DocumentTemplateType.MainAgreementConsortium:
						GenMainAgreementConsortiumBookmarkView modelMainAgreementConsortiumBookmarkView = mainAgreementTableService.GetMainAgreementConsortiumBookmarkView(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, modelMainAgreementConsortiumBookmarkView, null);

					case DocumentTemplateType.AssignmentAgreement:
						AssignmentAgreementBookmarkView modelAssignmentAgreementBookmarkView = assignmentAgreementTableService.GetAssignmentAgreementBookmarkValue(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, modelAssignmentAgreementBookmarkView, null);

					case DocumentTemplateType.CancelAssignmentAgreement:
						CancelAssignmentAgreementView cancelAssModel = assignmentAgreementTableService
							.GetCancelAssignmentAgreementValue(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, cancelAssModel, null);
					case DocumentTemplateType.GuarantorAgreement_ThaiPerson:
						BookmaskDocumentGuarantorAgreement bookmaskDocumentGuarantorAgreement_ThaiPerson = guarantorAgreementTableService.GetBookmaskDocumentGuarantorAgreement_ThaiPerson(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmaskDocumentGuarantorAgreement_ThaiPerson, null);

					case DocumentTemplateType.CrossGuarantorAgreement:
						CrossGuarantorAgreementBookmarkView crossGuantor = guarantorAgreementTableService
							.GetCrossGuarantorAgreementBookmarkView(template.RefGUID);
						return GenBookmarkDocumentFile(template, crossGuantor, null);

					case DocumentTemplateType.BusinessCollateralAgreement:
						BookmarkBusinessCollateralAgmTable bookmarkBusinessCollateralAgmTable = businessCollateralAgmTableService.GetBookmarkDocumentBusinessCollateralTable(template.RefGUID);
						return GenBookmarkDocumentFile(template, bookmarkBusinessCollateralAgmTable, null); 
					
					case DocumentTemplateType.BusCollateralAgmExpenseNotice:

						BookmarkBusinessCollateralAgreementExpenceDetail bookmarkBusinessCollateralAgmExpenceDetail = 
							businessCollateralAgmTableService.GetBookmarkDocumentBusinessCollateralTableDetail(template.RefGUID);
						return GenBookmarkDocumentFile(template, bookmarkBusinessCollateralAgmExpenceDetail, null);
					case DocumentTemplateType.ExtendNotesBusCollateralAgm:
						QBusinessCollateralAgreement businessCollateralAgreement = businessCollateralAgmTableService.GetBusinessCollateralAgmBookmarkValues(template.RefGUID);
						return GenBookmarkDocumentFile(template, businessCollateralAgreement, null);

					case DocumentTemplateType.LetterReqCancelBusCollateralAgm:
						BookmarkCancelBusinessCollateralAgreement bookmarkCancelBusinessCollateralAgreement = 
							businessCollateralAgmTableService.GetBookmarkDocumentCancelBusinessCollateralAgreement(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmarkCancelBusinessCollateralAgreement, null);
					
					case DocumentTemplateType.AddendumMainAgreement:
						AddendumMainAgreementBookmarkView addendumMainAgreementBookmarkView = 
							mainAgreementTableService.GetAddendumMainAgreementBookmarkView(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, addendumMainAgreementBookmarkView, null);
					
					case DocumentTemplateType.CustomerAmendCredit_limit:
						BookmarkDocumentCustomerAmendCreditLimitView bookmarkDocumentCustomerAmendCreditLimitView = 
							customerTableService.GetBookmarkDocumentCustomerAmendCreditLimitView(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template,bookmarkDocumentCustomerAmendCreditLimitView,null);
					
					case DocumentTemplateType.CustomerAmendCredit_information:
						BookmarkDocumentQueryCusotmerAmendInformation bookmarkDocumentQueryCusotmerAmendInformation = 
							creditAppRequestTableService.GetBookmarkDocumentQueryCustomerAmendInfo(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmarkDocumentQueryCusotmerAmendInformation, null);
					
					case DocumentTemplateType.CustomerCreditApplication_shared:
						BookmarkDocumentCreditApplication bookmarkDocumentCreditApplication = 
							creditAppTableService.GetBookmaskDocumentCreditApplication(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmarkDocumentCreditApplication, null);
					
					case DocumentTemplateType.CustomerCreditApplication_pf:
						BookmarkDocumentQueryCreditApplicationSharedAndPF bookmarkDocumentQueryCreditApplicationSharedAndPF = 
							creditAppTableService.GetBookmarkDocumentQueryCreditApplicationSharedAndPF(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmarkDocumentQueryCreditApplicationSharedAndPF, null);

					case DocumentTemplateType.BuyerAmendCreditLimit:
						QueryBuyerAmendCreditLimit queryBuyerAmendCreditLimit = creditAppRequestTableService.GetBookmarkDocumentQueryBuyerAmendCreditLimit(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, queryBuyerAmendCreditLimit, null);

					case DocumentTemplateType.BuyerAmendCreditInformation:
						BookmarkDocumentQueryBuyerAmendInformation bookmarkDocumentQueryBuyerAmendInformation = creditAppRequestTableService.GetBookmarkDocumentQueryBuyerAmendInfo(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmarkDocumentQueryBuyerAmendInformation, null);
                    case DocumentTemplateType.CreditApplicationConsideration:
						BookmarkDocumentCreditApplicationConsideration bookmarkDocumentCreditApplicationConsideration = creditAppTableService.GetBookmaskDocumentCreditApplicationConsideration(template.RefGUID, template.BookmarkDocumentTransGUID);
                        return GenBookmarkDocumentFile(template, bookmarkDocumentCreditApplicationConsideration, null);
					case DocumentTemplateType.BuyerReviewCreditLimit:
						BuyerReviewCreditLimitBookmarkView buyerReviewCreditLimitBookmarkView = creditAppRequestTableService.GetBuyerReviewCreditLimitBookmarkValue(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, buyerReviewCreditLimitBookmarkView, null);
					case DocumentTemplateType.BuyerCreditApplication_Private:
					case DocumentTemplateType.BuyerCreditApplication_Government:
						GenBookmarkBuyerCreditApplicationView bookmarkBuyerCreditApplicationView = creditAppRequestTableService.GenBookmarkBuyerCreditAppValue(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmarkBuyerCreditApplicationView, null);
					case DocumentTemplateType.CreditLimitClosing:
						CreditLimitClosingBookmarkView creditLimitClosingBookmarkView = creditAppRequestTableService.GetCreditLimitClosingBookmarkValue(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, creditLimitClosingBookmarkView, null);
					case DocumentTemplateType.GuarantorAgreement_Foreigner:
						BookmaskDocumentGuarantorAgreement bookmaskDocumentGuarantorAgreement_Foreigner = guarantorAgreementTableService.GetBookmaskDocumentGuarantorAgreement_Foreigner(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmaskDocumentGuarantorAgreement_Foreigner, null);
					case DocumentTemplateType.GuarantorAgreement_Organization:
						BookmaskDocumentGuarantorAgreement bookmaskDocumentGuarantorAgreement_Organization = guarantorAgreementTableService.GetBookmaskDocumentGuarantorAgreement_Organization(template.RefGUID, template.BookmarkDocumentTransGUID);
						return GenBookmarkDocumentFile(template, bookmaskDocumentGuarantorAgreement_Organization, null);
					default:
						//IDemoService demoService = new DemoService(db);
						//ReplaceBookmarkDemoView model = demoService.GetDemoBookmarkValues(template.RefGUID);
						//return GenBookmarkDocumentFile(template, model, null);

						return null;
                }

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<FileInformation> GenBookmarkDocumentFile(IEnumerable<GenBookmarkDocumentFileParamViewMap> templates)
		{
			try
			{
				if (templates != null && templates.Count() > 0)
				{
					List<FileInformation> result = templates.Select(s =>
					{
						return GenBookmarkDocumentFile(s);
					})
					.ToList();
					return result;
				}
				else
				{
					throw new Exception("DocumentTemplates not found.");
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FileInformation GenBookmarkDocumentFile<T>(GenBookmarkDocumentFileParamViewMap template, T data, string fileName = null)
        {
            try
            {
				if(template != null && !string.IsNullOrWhiteSpace(template.Base64Data))
                {
					BookmarkHelper helper = new BookmarkHelper();
					var bookmarkMapping = data.GetBookmarkMapperKeyValues();
					return helper.ReplaceBookmark(template.Base64Data, bookmarkMapping, fileName);
				}
				else
                {
					throw new Exception("DocumentTemplate cannot be null.");
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public async Task<FileInformation> GetDocumentTemplateByDocumentTemplateTableGUID(PrintBookDocTransParm printBookDocTransParm)
		{
			try
			{
				IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
				DocumentTemplateTableItemViewMap documentTemplateTable = (await documentTemplateTableRepo.GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(printBookDocTransParm.DocumentTemplateTableGUID.StringToGuid()));
				GenBookmarkDocumentFileParamViewMap template = new GenBookmarkDocumentFileParamViewMap
				{
					Base64Data = documentTemplateTable.Base64Data,
					DocumentTemplateType = documentTemplateTable.DocumentTemplateType,
					RefGUID = printBookDocTransParm.RefGUID.StringToGuid(),
					BookmarkDocumentTransGUID = printBookDocTransParm.BookmarkDocumentTransGUID.StringToGuid()
				};
				return GenBookmarkDocumentFile(template);
				}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public async Task<List<FileInformation>> GetDocumentTemplateByRefGUID(PrintSetBookDocTransParm printSetBookDocTransParm)
		{
			try
			{
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
				DocumentStatus rejectedBookmark = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Rejected).ToString());
				var documentTemplateTableList = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType(printSetBookDocTransParm.RefType, printSetBookDocTransParm.RefGUID.StringToGuid()).Where(w => w.DocumentStatusGUID != rejectedBookmark.DocumentStatusGUID).Select(s => new { s.DocumentTemplateTableGUID, s.BookmarkDocumentTransGUID});
				IEnumerable<GenBookmarkDocumentFileParamViewMap> templates = (await documentTemplateTableRepo.GetDocumentTemplateTableItemViewMapByDocumentTemplateTableGUID(documentTemplateTableList.Select(s => s.DocumentTemplateTableGUID).ToList()))
																				.Join(documentTemplateTableList.ToList()
																				, documentTemplateTable => documentTemplateTable.DocumentTemplateTableGUID
																				, documentTemplateTableList => documentTemplateTableList.DocumentTemplateTableGUID
																				, (documentTemplateTable, documentTemplateTableList) => new { documentTemplateTable, documentTemplateTableList.BookmarkDocumentTransGUID })
																				.Select(s => new GenBookmarkDocumentFileParamViewMap
																				{
																					Base64Data = s.documentTemplateTable.Base64Data,
																					DocumentTemplateType = s.documentTemplateTable.DocumentTemplateType,
																					RefGUID = printSetBookDocTransParm.RefGUID.StringToGuid(),
																					BookmarkDocumentTransGUID = s.BookmarkDocumentTransGUID
																				});
				return GenBookmarkDocumentFile(templates);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
