using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAgingReportSetupService
	{
		ReportAgingView GetReportAgingInitialDataByAgingFactoring(string companyGUID);
		ReportAgingView GetReportAgingInitialDataByAgingProjectFinance(string companyGUID);
	}
	public class AgingReportSetupService : SmartAppService, IAgingReportSetupService
	{
		public AgingReportSetupService(SmartAppDbContext context) : base(context) { }
		public AgingReportSetupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AgingReportSetupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AgingReportSetupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AgingReportSetupService() : base() { }
		public ReportAgingView GetReportAgingInitialDataByAgingFactoring(string companyGUID)
		{
            try
			{
				IAgingReportSetupRepo agingReportSetupRepo = new AgingReportSetupRepo(db);
				AgingReportSetupView agingReportSetupView = agingReportSetupRepo.GetAgingReportSetupDescription(companyGUID.StringToGuid());
				ReportAgingView reportAgingView = new ReportAgingView()
				{
					ProductType = (int)ProductType.Factoring,
					SuspenseInvoiceType = (int)SuspenseInvoiceType.None,
					RefType = new int[] { (int)RefType.PurchaseTable, (int)RefType.PurchaseLine },
					AgingReportSetup_Description_LineNum1 = agingReportSetupView.Description1,
					AgingReportSetup_Description_LineNum2 = agingReportSetupView.Description2,
					AgingReportSetup_Description_LineNum3 = agingReportSetupView.Description3,
					AgingReportSetup_Description_LineNum4 = agingReportSetupView.Description4,
					AgingReportSetup_Description_LineNum5 = agingReportSetupView.Description5,
					AgingReportSetup_Description_LineNum6 = agingReportSetupView.Description6
				};
				return reportAgingView;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
		}
		public ReportAgingView GetReportAgingInitialDataByAgingProjectFinance(string companyGUID)
		{
			try
			{
				IAgingReportSetupRepo agingReportSetupRepo = new AgingReportSetupRepo(db);
				AgingReportSetupView agingReportSetupView = agingReportSetupRepo.GetAgingReportSetupDescription(companyGUID.StringToGuid());
				ReportAgingView reportAgingView = new ReportAgingView()
				{
					ProductType = (int)ProductType.ProjectFinance,
					SuspenseInvoiceType = (int)SuspenseInvoiceType.None,
					RefType = new int[] { (int)RefType.WithdrawalTable, (int)RefType.WithdrawalLine },
					AgingReportSetup_Description_LineNum1 = agingReportSetupView.Description1,
					AgingReportSetup_Description_LineNum2 = agingReportSetupView.Description2,
					AgingReportSetup_Description_LineNum3 = agingReportSetupView.Description3,
					AgingReportSetup_Description_LineNum4 = agingReportSetupView.Description4,
					AgingReportSetup_Description_LineNum5 = agingReportSetupView.Description5,
					AgingReportSetup_Description_LineNum6 = agingReportSetupView.Description6
				};
				return reportAgingView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
