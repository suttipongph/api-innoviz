using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBusinessCollateralTypeService
	{

		BusinessCollateralSubTypeItemView GetBusinessCollateralSubTypeById(string id);
		BusinessCollateralSubTypeItemView CreateBusinessCollateralSubType(BusinessCollateralSubTypeItemView businessCollateralSubTypeView);
		BusinessCollateralSubTypeItemView UpdateBusinessCollateralSubType(BusinessCollateralSubTypeItemView businessCollateralSubTypeView);
		bool DeleteBusinessCollateralSubType(string id);
		BusinessCollateralTypeItemView GetBusinessCollateralTypeById(string id);
		BusinessCollateralTypeItemView CreateBusinessCollateralType(BusinessCollateralTypeItemView businessCollateralTypeView);
		BusinessCollateralTypeItemView UpdateBusinessCollateralType(BusinessCollateralTypeItemView businessCollateralTypeView);
		bool DeleteBusinessCollateralType(string id);
		BusinessCollateralSubTypeItemView GetBusinessCollateralSubTypeInitialdata(string businessCollateralTypeGUID);
		IEnumerable<SelectItem<BusinessCollateralSubTypeItemView>> GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(SearchParameter search);
	}
	public class BusinessCollateralTypeService : SmartAppService, IBusinessCollateralTypeService
	{
		public BusinessCollateralTypeService(SmartAppDbContext context) : base(context) { }
		public BusinessCollateralTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BusinessCollateralTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessCollateralTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BusinessCollateralTypeService() : base() { }

		public BusinessCollateralSubTypeItemView GetBusinessCollateralSubTypeById(string id)
		{
			try
			{
				IBusinessCollateralSubTypeRepo businessCollateralSubTypeRepo = new BusinessCollateralSubTypeRepo(db);
				return businessCollateralSubTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralSubTypeItemView CreateBusinessCollateralSubType(BusinessCollateralSubTypeItemView businessCollateralSubTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				businessCollateralSubTypeView = accessLevelService.AssignOwnerBU(businessCollateralSubTypeView);
				IBusinessCollateralSubTypeRepo businessCollateralSubTypeRepo = new BusinessCollateralSubTypeRepo(db);
				BusinessCollateralSubType businessCollateralSubType = businessCollateralSubTypeView.ToBusinessCollateralSubType();
				businessCollateralSubType = businessCollateralSubTypeRepo.CreateBusinessCollateralSubType(businessCollateralSubType);
				base.LogTransactionCreate<BusinessCollateralSubType>(businessCollateralSubType);
				UnitOfWork.Commit();
				return businessCollateralSubType.ToBusinessCollateralSubTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralSubTypeItemView UpdateBusinessCollateralSubType(BusinessCollateralSubTypeItemView businessCollateralSubTypeView)
		{
			try
			{
				IBusinessCollateralSubTypeRepo businessCollateralSubTypeRepo = new BusinessCollateralSubTypeRepo(db);
				BusinessCollateralSubType inputBusinessCollateralSubType = businessCollateralSubTypeView.ToBusinessCollateralSubType();
				BusinessCollateralSubType dbBusinessCollateralSubType = businessCollateralSubTypeRepo.Find(inputBusinessCollateralSubType.BusinessCollateralSubTypeGUID);
				dbBusinessCollateralSubType = businessCollateralSubTypeRepo.UpdateBusinessCollateralSubType(dbBusinessCollateralSubType, inputBusinessCollateralSubType);
				base.LogTransactionUpdate<BusinessCollateralSubType>(GetOriginalValues<BusinessCollateralSubType>(dbBusinessCollateralSubType), dbBusinessCollateralSubType);
				UnitOfWork.Commit();
				return dbBusinessCollateralSubType.ToBusinessCollateralSubTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBusinessCollateralSubType(string item)
		{
			try
			{
				IBusinessCollateralSubTypeRepo businessCollateralSubTypeRepo = new BusinessCollateralSubTypeRepo(db);
				Guid businessCollateralSubTypeGUID = new Guid(item);
				BusinessCollateralSubType businessCollateralSubType = businessCollateralSubTypeRepo.Find(businessCollateralSubTypeGUID);
				businessCollateralSubTypeRepo.Remove(businessCollateralSubType);
				base.LogTransactionDelete<BusinessCollateralSubType>(businessCollateralSubType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public BusinessCollateralTypeItemView GetBusinessCollateralTypeById(string id)
		{
			try
			{
				IBusinessCollateralTypeRepo businessCollateralTypeRepo = new BusinessCollateralTypeRepo(db);
				return businessCollateralTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralTypeItemView CreateBusinessCollateralType(BusinessCollateralTypeItemView businessCollateralTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				businessCollateralTypeView = accessLevelService.AssignOwnerBU(businessCollateralTypeView);
				IBusinessCollateralTypeRepo businessCollateralTypeRepo = new BusinessCollateralTypeRepo(db);
				BusinessCollateralType businessCollateralType = businessCollateralTypeView.ToBusinessCollateralType();
				businessCollateralType = businessCollateralTypeRepo.CreateBusinessCollateralType(businessCollateralType);
				base.LogTransactionCreate<BusinessCollateralType>(businessCollateralType);
				UnitOfWork.Commit();
				return businessCollateralType.ToBusinessCollateralTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessCollateralTypeItemView UpdateBusinessCollateralType(BusinessCollateralTypeItemView businessCollateralTypeView)
		{
			try
			{
				IBusinessCollateralTypeRepo businessCollateralTypeRepo = new BusinessCollateralTypeRepo(db);
				BusinessCollateralType inputBusinessCollateralType = businessCollateralTypeView.ToBusinessCollateralType();
				BusinessCollateralType dbBusinessCollateralType = businessCollateralTypeRepo.Find(inputBusinessCollateralType.BusinessCollateralTypeGUID);
				dbBusinessCollateralType = businessCollateralTypeRepo.UpdateBusinessCollateralType(dbBusinessCollateralType, inputBusinessCollateralType);
				base.LogTransactionUpdate<BusinessCollateralType>(GetOriginalValues<BusinessCollateralType>(dbBusinessCollateralType), dbBusinessCollateralType);
				UnitOfWork.Commit();
				return dbBusinessCollateralType.ToBusinessCollateralTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBusinessCollateralType(string item)
		{
			try
			{
				IBusinessCollateralTypeRepo businessCollateralTypeRepo = new BusinessCollateralTypeRepo(db);
				Guid businessCollateralTypeGUID = new Guid(item);
				BusinessCollateralType businessCollateralType = businessCollateralTypeRepo.Find(businessCollateralTypeGUID);
				businessCollateralTypeRepo.Remove(businessCollateralType);
				base.LogTransactionDelete<BusinessCollateralType>(businessCollateralType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public BusinessCollateralSubTypeItemView GetBusinessCollateralSubTypeInitialdata(string businessCollateralTypeGUID)
		{
			try
			{
				IBusinessCollateralTypeRepo businessCollateralTypeRepo = new BusinessCollateralTypeRepo(db);
				BusinessCollateralType businessCollateralType  = businessCollateralTypeRepo.GetBusinessCollateralTypeNoTrackingByAccessLevel(businessCollateralTypeGUID.StringToGuid());

				return new BusinessCollateralSubTypeItemView
				{
					BusinessCollateralSubTypeGUID = new Guid().GuidNullToString(),
					BusinessCollateralTypeGUID = (businessCollateralType != null) ? businessCollateralType.BusinessCollateralTypeGUID.GuidNullToString() : null,
					BusinessCollateralType_Values = SmartAppUtil.GetDropDownLabel(businessCollateralType.BusinessCollateralTypeId,businessCollateralType.Description)
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		public IEnumerable<SelectItem<BusinessCollateralSubTypeItemView>> GetDropDownItemBusinessCollateralSubTypeByBusinessCollateralType(SearchParameter search)
		{
			try
			{
				IBusinessCollateralSubTypeRepo businessCollateralSubTypeRepo = new BusinessCollateralSubTypeRepo(db);
				IDocumentService documentService = new DocumentService(db);
				search = search.GetParentCondition(BusinessCollateralSubTypeCondition.BusinessCollateralTypeGUID);
				return businessCollateralSubTypeRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
