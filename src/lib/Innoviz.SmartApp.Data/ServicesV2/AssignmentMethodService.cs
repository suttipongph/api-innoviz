using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAssignmentMethodService
	{

		AssignmentMethodItemView GetAssignmentMethodById(string id);
		AssignmentMethodItemView CreateAssignmentMethod(AssignmentMethodItemView assignmentMethodView);
		AssignmentMethodItemView UpdateAssignmentMethod(AssignmentMethodItemView assignmentMethodView);
		bool DeleteAssignmentMethod(string id);
	}
	public class AssignmentMethodService : SmartAppService, IAssignmentMethodService
	{
		public AssignmentMethodService(SmartAppDbContext context) : base(context) { }
		public AssignmentMethodService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AssignmentMethodService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AssignmentMethodService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AssignmentMethodService() : base() { }

		public AssignmentMethodItemView GetAssignmentMethodById(string id)
		{
			try
			{
				IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
				return assignmentMethodRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AssignmentMethodItemView CreateAssignmentMethod(AssignmentMethodItemView assignmentMethodView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				assignmentMethodView = accessLevelService.AssignOwnerBU(assignmentMethodView);
				IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
				AssignmentMethod assignmentMethod = assignmentMethodView.ToAssignmentMethod();
				assignmentMethod = assignmentMethodRepo.CreateAssignmentMethod(assignmentMethod);
				base.LogTransactionCreate<AssignmentMethod>(assignmentMethod);
				UnitOfWork.Commit();
				return assignmentMethod.ToAssignmentMethodItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AssignmentMethodItemView UpdateAssignmentMethod(AssignmentMethodItemView assignmentMethodView)
		{
			try
			{
				IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
				AssignmentMethod inputAssignmentMethod = assignmentMethodView.ToAssignmentMethod();
				AssignmentMethod dbAssignmentMethod = assignmentMethodRepo.Find(inputAssignmentMethod.AssignmentMethodGUID);
				dbAssignmentMethod = assignmentMethodRepo.UpdateAssignmentMethod(dbAssignmentMethod, inputAssignmentMethod);
				base.LogTransactionUpdate<AssignmentMethod>(GetOriginalValues<AssignmentMethod>(dbAssignmentMethod), dbAssignmentMethod);
				UnitOfWork.Commit();
				return dbAssignmentMethod.ToAssignmentMethodItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAssignmentMethod(string item)
		{
			try
			{
				IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
				Guid assignmentMethodGUID = new Guid(item);
				AssignmentMethod assignmentMethod = assignmentMethodRepo.Find(assignmentMethodGUID);
				assignmentMethodRepo.Remove(assignmentMethod);
				base.LogTransactionDelete<AssignmentMethod>(assignmentMethod);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
