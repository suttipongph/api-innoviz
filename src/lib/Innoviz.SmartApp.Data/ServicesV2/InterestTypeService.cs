using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IInterestTypeService
	{

		InterestTypeItemView GetInterestTypeById(string id);
		InterestTypeItemView CreateInterestType(InterestTypeItemView interestTypeView);
		InterestTypeItemView UpdateInterestType(InterestTypeItemView interestTypeView);
		bool DeleteInterestType(string id);
		InterestTypeValueItemView GetInterestTypeValueById(string id);
		InterestTypeValueItemView CreateInterestTypeValue(InterestTypeValueItemView interestTypeValueView);
		InterestTypeValueItemView UpdateInterestTypeValue(InterestTypeValueItemView interestTypeValueView);
		bool DeleteInterestTypeValue(string id);
		InterestTypeValueItemView GetInterestTypeValueInitialData(string interestTypeGUID);
		decimal GetInterestTypeValue(Guid interestTypeGuid, DateTime asOfDate, bool isThrow = true);
	}
	public class InterestTypeService : SmartAppService, IInterestTypeService
	{
		public InterestTypeService(SmartAppDbContext context) : base(context) { }
		public InterestTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public InterestTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InterestTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public InterestTypeService() : base() { }

		#region InterestType
		public InterestTypeItemView GetInterestTypeById(string id)
		{
			try
			{
				IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
				return interestTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestTypeItemView CreateInterestType(InterestTypeItemView interestTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				interestTypeView = accessLevelService.AssignOwnerBU(interestTypeView);
				IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
				InterestType interestType = interestTypeView.ToInterestType();
				interestType = interestTypeRepo.CreateInterestType(interestType);
				base.LogTransactionCreate<InterestType>(interestType);
				UnitOfWork.Commit();
				return interestType.ToInterestTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestTypeItemView UpdateInterestType(InterestTypeItemView interestTypeView)
		{
			try
			{
				IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
				InterestType inputInterestType = interestTypeView.ToInterestType();
				InterestType dbInterestType = interestTypeRepo.Find(inputInterestType.InterestTypeGUID);
				dbInterestType = interestTypeRepo.UpdateInterestType(dbInterestType, inputInterestType);
				base.LogTransactionUpdate<InterestType>(GetOriginalValues<InterestType>(dbInterestType), dbInterestType);
				UnitOfWork.Commit();
				return dbInterestType.ToInterestTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteInterestType(string item)
		{
			try
			{
				IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
				Guid interestTypeGUID = new Guid(item);
				InterestType interestType = interestTypeRepo.Find(interestTypeGUID);
				interestTypeRepo.Remove(interestType);
				base.LogTransactionDelete<InterestType>(interestType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InterestType
		#region InterestTypeValue
		public InterestTypeValueItemView GetInterestTypeValueById(string id)
		{
			try
			{
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				return interestTypeValueRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestTypeValueItemView CreateInterestTypeValue(InterestTypeValueItemView interestTypeValueView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				interestTypeValueView = accessLevelService.AssignOwnerBU(interestTypeValueView);
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				InterestTypeValue interestTypeValue = interestTypeValueView.ToInterestTypeValue();
				interestTypeValue = interestTypeValueRepo.CreateInterestTypeValue(interestTypeValue);
				base.LogTransactionCreate<InterestTypeValue>(interestTypeValue);
				UnitOfWork.Commit();
				return interestTypeValue.ToInterestTypeValueItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestTypeValueItemView UpdateInterestTypeValue(InterestTypeValueItemView interestTypeValueView)
		{
			try
			{
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				InterestTypeValue inputInterestTypeValue = interestTypeValueView.ToInterestTypeValue();
				InterestTypeValue dbInterestTypeValue = interestTypeValueRepo.Find(inputInterestTypeValue.InterestTypeValueGUID);
				dbInterestTypeValue = interestTypeValueRepo.UpdateInterestTypeValue(dbInterestTypeValue, inputInterestTypeValue);
				base.LogTransactionUpdate<InterestTypeValue>(GetOriginalValues<InterestTypeValue>(dbInterestTypeValue), dbInterestTypeValue);
				UnitOfWork.Commit();
				return dbInterestTypeValue.ToInterestTypeValueItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteInterestTypeValue(string item)
		{
			try
			{
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				Guid interestTypeValueGUID = new Guid(item);
				InterestTypeValue interestTypeValue = interestTypeValueRepo.Find(interestTypeValueGUID);
				interestTypeValueRepo.Remove(interestTypeValue);
				base.LogTransactionDelete<InterestTypeValue>(interestTypeValue);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InterestTypeValueItemView GetInterestTypeValueInitialData(string interestTypeGUID)
		{
			try
			{
				IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
				InterestType interestType = interestTypeRepo.GetInterestTypeByIdNoTracking(interestTypeGUID.StringToGuid());
				return new InterestTypeValueItemView
				{
					InterestTypeValueGUID = new Guid().GuidNullToString(),
					InterestTypeGUID = interestTypeGUID,
					InterestType_Values = SmartAppUtil.GetDropDownLabel(interestType.InterestTypeId, interestType.Description),
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public decimal GetInterestTypeValue(Guid interestTypeGuid, DateTime asOfDate, bool isThrow = true)
		{
			try
			{
				IInterestTypeValueRepo interestTypeValueRepo = new InterestTypeValueRepo(db);
				InterestTypeValue interestTypeValue = interestTypeValueRepo.GetByInterestTypeDateNoTracking(interestTypeGuid, asOfDate, isThrow);
				return interestTypeValue.Value;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InterestTypeValue
	}
}
