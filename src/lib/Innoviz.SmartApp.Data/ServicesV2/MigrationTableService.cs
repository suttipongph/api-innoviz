﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IMigrationTableService
    {
        MigrationTableItemView GetMigrationTableById(string id);
        MigrationTableItemView ExecuteMigration(MigrationTableItemView migrationTableView);
        bool GenerateDummyRecored(string companyGUID);
        SearchResult<MigrationLogTableView> GetListByInquiryMigrationName(SearchParameter search);
        bool InitializeMigrationTable(List<MigrationTable> list);
        bool InitializeMigrationTable(string jsonContent, string jsonContent_CUSTOM, string username);
        GenMigrationTransactionResultView GenMigrationTransaction(string companyGUID );
        bool ValidateUpdateChequeReference();
        UpdateChequeReferenceResultView UpdateChequeReference();
        GenPULineInvoiceResultView GenPULineInvoice(string companyGUID);
        bool ValidateGenPULineInvoice(string companyGUID);
        GenWDLineInvoiceResultView GenWDLineInvoice(string companyGUID);
        bool ValidateGenWDLineInvoice(string companyGUID);
    }

    public class MigrationTableService : SmartAppService, IMigrationTableService
    {
        public MigrationTableService(SmartAppDbContext context) : base(context) { }
        public MigrationTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public MigrationTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public MigrationTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public MigrationTableService() : base() { }

        public MigrationTableItemView GetMigrationTableById(string id)
        {
            try
            {
                IMigrationTableRepo migrationTableRepo = new MigrationTableRepo(db);
                return migrationTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public MigrationTableItemView ExecuteMigration(MigrationTableItemView migrationTableView)
        {
            try
            {
                IMigrationTableRepo migrationTableRepo = new MigrationTableRepo(db);
                MigrationTable migrationTable = migrationTableView.ToMigrationTable();
                migrationTableRepo.Update(migrationTable);
                migrationTableRepo.ExecuteSSISPackage(migrationTable.PackageName);
                UnitOfWork.Commit();
                return migrationTable.ToMigrationTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        public SearchResult<MigrationLogTableView> GetListByInquiryMigrationName(SearchParameter search)
        {
            try
            {
                MigrationLogParm input = search.GetSearchToModel<MigrationLogParm>();
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                if (input.MigrationFromDate == null || input.MigrationToDate == null)
                {
                    ex.AddData("ERROR.MANDATORY_FIELD");
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                if (input.MigrationToDate.StringToDate() < input.MigrationFromDate.StringToDate())
                {
                    ex.AddData("ERROR.00475");
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                IMigrationLogTableRepo migrationLogTableRepo = new MigrationLogTableRepo(db);
                return migrationLogTableRepo.GetListvw(search, input);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #region Generate Application Dummy Recored
        public bool GenerateDummyRecored(string companyGUID)
        {
            try
            {
                IMigrationTableRepo migrationTableRepo = new MigrationTableRepo(db);
                migrationTableRepo.GenerateDummyRecord(companyGUID);
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion
        public bool InitializeMigrationTable(List<MigrationTable> list)
        {
            try
            {
                // use UseTempDB when CREATE TABLE permission denied
                var bulkConfig = new BulkConfig { UseTempDB = true };

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    List<MigrationTable> oldData = db.Set<MigrationTable>().ToList();
                    db.BulkDelete(oldData);
                    db.BulkInsert(list);
                    UnitOfWork.Commit(transaction);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InitializeMigrationTable(string jsonContent, string jsonContent_CUSTOM, string username)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing MigrationTable...");
                List<MigrationTable> migrationTables = JsonConvert.DeserializeObject<List<MigrationTable>>(jsonContent);
                List<MigrationTable> migrationTables_CUSTOM = JsonConvert.DeserializeObject<List<MigrationTable>>(jsonContent_CUSTOM);
                
                migrationTables = migrationTables.Select(s =>
                {
                    s.PackageName = "0" + s.PackageName;
                    s.GroupName = "0" + s.GroupName;
                    return s;
                })
                .ToList();
                migrationTables_CUSTOM = migrationTables_CUSTOM.Select(s =>
                {
                    s.PackageName = "1" + s.PackageName;
                    s.GroupName = "1" + s.GroupName;
                    return s;
                })
                .ToList();
                migrationTables = migrationTables
                        .Concat(migrationTables_CUSTOM)
                        .ToList().GroupBy(g => g.MigrateName)
                        .Select(g => new MigrationTable
                        {
                            MigrateName = g.Key,
                            PackageName = g.Max(p => p.PackageName),
                            GroupName = g.Max(p => p.GroupName),
                            GroupOrder = g.Max(p => p.GroupOrder),
                            MigrateOrder = g.Max(p => p.MigrateOrder)
                        }).ToList();
                migrationTables = migrationTables.Select(s =>
                {
                    s.MigrationTableGUID = Guid.NewGuid();
                    s.PackageName = s.PackageName.Substring(1);
                    s.GroupName = s.GroupName.Substring(1);
                    s.MigrateResult = null;
                    s.CreatedBy = username;
                    s.CreatedDateTime = DateTime.Now;
                    s.ModifiedBy = username;
                    s.ModifiedDateTime = DateTime.Now;
                    return s;
                })
                .ToList();

                // validate
                SmartAppException ex = new SmartAppException("Error Migration.json");
                var groupByPackageName = migrationTables.GroupBy(g => new { g.GroupName, g.PackageName }).Select(s => new { s.Key.GroupName, s.Key.PackageName, Group = s });
                if (groupByPackageName.Any(a => a.Group.Count() > 1))
                {
                    var groups = groupByPackageName.Where(w => w.Group.Count() > 1).Select(s => string.Concat(s.GroupName, ".", s.PackageName));
                    ex.AddData("Duplicate MigrationTable.PackageName: " + string.Join(", ", groups));
                    throw ex;
                }

                return InitializeMigrationTable(migrationTables);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenMigrationTransactionResultView InitGenMiGrationTransaction(Guid companyGUID)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                BusinessUnit businessUnit = (from bu in db.Set<BusinessUnit>()
                                             where bu.ParentBusinessUnitGUID == null
                                             select bu).FirstOrDefault();

                Company company = (from cp in db.Set<Company>()
                                   where cp.CompanyGUID == companyGUID
                                   select cp).FirstOrDefault();
                Branch branch = (from b in db.Set<Branch>()
                                   select b).FirstOrDefault();
                CompanyParameter companyParameter = (from cp in db.Set<CompanyParameter>()
                                                    where cp.CompanyGUID == company.CompanyGUID
                                                     select cp).FirstOrDefault();

                // share

                ProductSubType productSubTypeZeroProductType = (from pst in db.Set<ProductSubType>()
                                                                where pst.ProductType == 0
                                                                select pst).FirstOrDefault();
                CreditLimitType creditLimitTypeZeroProductType = (from clt in db.Set<CreditLimitType>()
                                                                  where clt.ProductType == 0
                                                                  select clt).FirstOrDefault();
                CreditAppRequestTable creditAppRequestTableByMigrate = (from cart in db.Set<CreditAppRequestTable>()
                                                                          where cart.CreditAppRequestId == "Migrate"
                                                                          select cart).FirstOrDefault();
                CreditAppTable creditAppTableByMigrate = (from ca in db.Set<CreditAppTable>()
                                                                        where ca.CreditAppId == "Migrate"
                                                                        select ca).FirstOrDefault();
                BusinessCollateralAgmTable businessCollateralAgmTableByMigrate = (from bca in db.Set<BusinessCollateralAgmTable>()
                                                          where bca.BusinessCollateralAgmId == "Migrate"
                                                          select bca).FirstOrDefault();
                CreditAppReqBusinessCollateral creditAppReqBusinessCollateralByMigrate = (from carbc in db.Set<CreditAppReqBusinessCollateral>()
                                                                                          where carbc.RefAgreementId == "Migrate"
                                                                                          select carbc).FirstOrDefault();
                InterestType interestType = (from it in db.Set<InterestType>()
                                             select it).FirstOrDefault();
                InvoiceRevenueType invoiceRevenueType = (from irt in db.Set<InvoiceRevenueType>()
                                                         select irt).FirstOrDefault();
                JobType jobType = (from jt in db.Set<JobType>()
                                   select jt).FirstOrDefault();
                EmployeeTable employeeTable = (from et in db.Set<EmployeeTable>()
                                               select et).FirstOrDefault();
                CustomerRefundTable customerRefundTableByMigrate = (from crt in db.Set<CustomerRefundTable>()
                                                                    where crt.CustomerRefundId == "Migrate"
                                                                    select crt).FirstOrDefault();
                InvoiceType invoiceType = (from it in db.Set<InvoiceType>()
                                               select it).FirstOrDefault();

                #region ProductSubType
                ProductSubTypeItemView productSubType = new ProductSubTypeItemView
                {
                    ProductSubTypeId = "None",
                    Description = "None",
                    ProductType = 0,
                    MaxInterestFeePct = 0,
                    GuarantorAgreementYear = 0,
                    CalcInterestMethod = 0,
                    CalcInterestDayMethod = 0,
                    ProductSubTypeGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion ProductSubType

                #region CreditLimitType  
                CreditLimitTypeItemView creditLimitType = new CreditLimitTypeItemView
                {
                    CreditLimitTypeId = "None",
                    Description = "None",
                    ProductType = 0,
                    CreditLimitConditionType = 0,
                    ParentCreditLimitTypeGUID = null,
                    CreditLimitExpiration = 4,
                    CreditLimitExpiryDay = 0,
                    ValidateCreditLimitType = false,
                    Revolving = false,
                    InactiveDay = 0,
                    CloseDay = 0,
                    BuyerMatchingAndLoanRequest = false,
                    AllowAddLine = false,
                    CreditLimitRemark = null,
                    ValidateBuyerAgreement = false,
                    CreditLimitTypeGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion CreditLimitType

                #region AddressTrans
                AddressCountry addresscountry = (from ac in db.Set<AddressCountry>() select ac).FirstOrDefault();
                AddressProvince addressprovince = (from ap in db.Set<AddressProvince>() select ap).FirstOrDefault();
                AddressDistrict addressdistrict = (from ad in db.Set<AddressDistrict>() select ad).FirstOrDefault();
                AddressSubDistrict addresssubdistrict = (from asd in db.Set<AddressSubDistrict>() select asd).FirstOrDefault();
                AddressPostalCode addresspostalcode = (from apc in db.Set<AddressPostalCode>() select apc).FirstOrDefault();

                AddressTransItemView addressTrans = new AddressTransItemView
                {
                    Name = "Migrate",
                    Primary = false,
                    CurrentAddress = false,
                    Address1 = "Migration transaction",
                    Address2 = null,
                    AddressCountryGUID = addresscountry.AddressCountryGUID.GuidNullToString(),
                    AddressProvinceGUID = addressprovince.AddressProvinceGUID.GuidNullToString(),
                    AddressDistrictGUID = addressdistrict.AddressDistrictGUID.GuidNullToString(),
                    AddressSubDistrictGUID = addresssubdistrict.AddressSubDistrictGUID.GuidNullToString(),
                    AddressPostalCodeGUID = addresspostalcode.AddressPostalCodeGUID.GuidNullToString(),
                    PropertyTypeGUID = null,
                    OwnershipGUID = null,
                    AltAddress = null,
                    IsTax = false,
                    TaxBranchId = null,
                    TaxBranchName = null,
                    RefType = 0,
                    RefGUID = "00000000-0000-0000-0000-000000000000",
                    AddressTransGUID = "00000000-0000-0000-0000-000000000000",
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion AddressTrans

                #region CreditAppRequestTable
                Guid creditAppRequestTableCancelledStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CreditAppRequestStatus.Cancelled).ToString()).DocumentStatusGUID;
                CreditAppRequestTableItemView creditAppRequestTable = new CreditAppRequestTableItemView
                {
                    CreditAppRequestId = "Migrate",
                    Description = "Migration transaction",
                    ProductType = 0,
                    ProductSubTypeGUID = (productSubTypeZeroProductType != null ) ? productSubTypeZeroProductType.ProductSubTypeGUID.GuidNullToString() : productSubType.ProductSubTypeGUID,
                    CreditAppRequestType = 0,
                    CreditLimitTypeGUID = (creditLimitTypeZeroProductType != null) ? creditLimitTypeZeroProductType.CreditLimitTypeGUID.GuidNullToString() : creditLimitType.CreditLimitTypeGUID,
                    RequestDate = "01/01/1900",
                    DocumentReasonGUID = null,
                    DocumentStatusGUID = creditAppRequestTableCancelledStatus.GuidNullToString(),
                    RefCreditAppTableGUID = null,
                    Remark = null,
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    ConsortiumTableGUID = null,
                    RegisteredAddressGUID = "00000000-0000-0000-0000-000000000000",
                    SalesAvgPerMonth = 0,
                    BankAccountControlGUID = null,
                    PDCBankGUID = null,
                    CreditLimitRequest = 0,
                    CustomerCreditLimit = 0,
                    CustomerAllBuyerOutstanding = 0,
                    CreditLimitExpiration = 4,
                    StartDate = null,
                    ExpiryDate = null,
                    CreditLimitRemark = null,
                    CreditRequestFeePct = 0,
                    CreditRequestFeeAmount = 0,
                    InterestTypeGUID = interestType.InterestTypeGUID.GuidNullToString(),
                    InterestAdjustment = 0,
                    CACondition = null,
                    SigningCondition = null,
                    FinancialCreditCheckedDate = null,
                    FinancialCheckedBy = null,
                    NCBCheckedDate = null,
                    NCBCheckedBy = null,
                    BlacklistStatusGUID = null,
                    KYCGUID = null,
                    CreditScoringGUID = null,
                    BillingContactPersonGUID = null,
                    BillingAddressGUID = null,
                    ReceiptContactPersonGUID = null,
                    ReceiptAddressGUID = null,
                    InvoiceAddressGUID = null,
                    MailingReceiptAddressGUID = null,
                    MaxPurchasePct = 0,
                    MaxRetentionPct = 0,
                    MaxRetentionAmount = 0,
                    PurchaseFeePct = 0,
                    PurchaseFeeCalculateBase = 0,
                    CreditTermGUID = null,
                    ExtensionServiceFeeCondTemplateGUID = null,
                    ApplicationTableGUID = null,
                    ApprovedCreditLimitRequest = 0,
                    ApprovedDate = null,
                    MarketingComment = null,
                    CreditComment = null,
                    ApproverComment = null,
                    Dimension1GUID = null,
                    Dimension2GUID = null,
                    Dimension3GUID = null,
                    Dimension4GUID = null,
                    Dimension5GUID = null,
                    CreditAppRequestTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion CreditAppRequestTable

                #region CreditAppReqBusinessCollateral

                BusinessCollateralType businessCollateralType = (from bct in db.Set<BusinessCollateralType>()
                                                      select bct).FirstOrDefault();
                BusinessCollateralSubType businessCollateralSubType = (from bcst in db.Set<BusinessCollateralSubType>()
                                                       where bcst.BusinessCollateralTypeGUID == businessCollateralType.BusinessCollateralTypeGUID
                                                       select bcst).FirstOrDefault();

                CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateral = new CreditAppReqBusinessCollateralItemView
                {
                    IsNew = true,
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    CreditAppRequestTableGUID = (creditAppRequestTableByMigrate != null) ? creditAppRequestTableByMigrate.CreditAppRequestTableGUID.GuidNullToString() : creditAppRequestTable.CreditAppRequestTableGUID,
                    CustBusinessCollateralGUID = null,
                    BusinessCollateralTypeGUID = businessCollateralType.BusinessCollateralTypeGUID.GuidNullToString(),
                    BusinessCollateralSubTypeGUID = businessCollateralSubType.BusinessCollateralSubTypeGUID.GuidNullToString(),
                    Description = "Migration transaction",
                    BusinessCollateralValue = 0,
                    BuyerTableGUID = null,
                    BuyerName = null,
                    BuyerTaxIdentificationId = null,
                    BankGroupGUID = null,
                    BankTypeGUID = null,
                    AccountNumber = null,
                    RefAgreementId = "Migrate",
                    RefAgreementDate = null,
                    PreferentialCreditorNumber = 0,
                    Lessee = null,
                    Lessor = null,
                    RegistrationPlateNumber = null,
                    MachineNumber = null,
                    MachineRegisteredStatus = null,
                    ChassisNumber = null,
                    RegisteredPlace = null,
                    Quantity = 0,
                    Unit = null,
                    GuaranteeAmount = 0,
                    ProjectName = null,
                    TitleDeedNumber = null,
                    TitleDeedSubDistrict = null,
                    TitleDeedDistrict = null,
                    TitleDeedProvince = null,
                    DBDRegistrationId = null,
                    DBDRegistrationDescription = null,
                    DBDRegistrationDate = null,
                    DBDRegistrationAmount = 0,
                    AttachmentRemark = null,
                    CapitalValuation = 0,
                    ValuationCommittee = null,
                    DateOfValuation = null,
                    Ownership = null,
                    CreditAppReqBusinessCollateralGUID = Guid.NewGuid().GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                };
                #endregion CreditAppReqBusinessCollateral

                #region CreditAppTable   
                CreditAppTableItemView creditAppTable = new CreditAppTableItemView
                {
                    CreditAppId = "Migrate",
                    Description = "Migration transaction",
                    ProductType = 0,
                    ProductSubTypeGUID = (productSubTypeZeroProductType != null) ? productSubTypeZeroProductType.ProductSubTypeGUID.GuidNullToString() : productSubType.ProductSubTypeGUID,
                    CreditLimitTypeGUID = (creditLimitTypeZeroProductType != null) ? creditLimitTypeZeroProductType.CreditLimitTypeGUID.GuidNullToString() : creditLimitType.CreditLimitTypeGUID,
                    ApprovedDate = "01/01/1900",
                    ExpectedAgreementSigningDate = null,
                    DocumentReasonGUID = null,
                    RefCreditAppRequestTableGUID = (creditAppRequestTableByMigrate != null) ? creditAppRequestTableByMigrate.CreditAppRequestTableGUID.GuidNullToString() : creditAppRequestTable.CreditAppRequestTableGUID,
                    Remark = null,
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    ConsortiumTableGUID = null,
                    RegisteredAddressGUID = null,
                    SalesAvgPerMonth = 0,
                    BankAccountControlGUID = null,
                    PDCBankGUID = null,
                    ApprovedCreditLimit = 0,
                    CreditLimitExpiration = 4,
                    StartDate = "01/01/1900",
                    ExpiryDate = "01/01/1900",
                    CreditLimitRemark = null,
                    InactiveDate = null,
                    CreditRequestFeePct = 0,
                    InterestTypeGUID = interestType.InterestTypeGUID.GuidNullToString(),
                    InterestAdjustment = 0,
                    TotalInterestPct = 0,
                    CACondition = null,
                    BillingContactPersonGUID = null,
                    BillingAddressGUID = null,
                    ReceiptContactPersonGUID = null,
                    ReceiptAddressGUID = null,
                    InvoiceAddressGUID = null,
                    MailingReceipAddressGUID = null,
                    MaxPurchasePct = 0,
                    MaxRetentionPct = 0,
                    MaxRetentionAmount = 0,
                    PurchaseFeePct = 0,
                    PurchaseFeeCalculateBase = 0,
                    CreditTermGUID = null,
                    ExtensionServiceFeeCondTemplateGUID = null,
                    ApplicationTableGUID = null,
                    Dimension1GUID = null,
                    Dimension2GUID = null,
                    Dimension3GUID = null,
                    Dimension4GUID = null,
                    Dimension5GUID = null,
                    CreditAppTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion CreditAppTable

                #region MainAgreementTable
                Guid mainAgreementTableCancelledStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Cancelled).ToString()).DocumentStatusGUID;

                MainAgreementTableItemView mainAgreementTable = new MainAgreementTableItemView
                {
                    InternalMainAgreementId = "Migrate",
                    MainAgreementId = "Migrate",
                    AgreementDocType = 0,
                    Description = "Migration transaction",
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    CustomerName = "Migration transaction",
                    CustomerAltName = null,
                    AgreementDate = "01/01/1900",
                    DocumentStatusGUID = mainAgreementTableCancelledStatus.GuidNullToString(),
                    StartDate = null,
                    ExpiryDate = null,
                    AgreementYear = 0,
                    SigningDate = null,
                    MaxInterestPct = 0,
                    DocumentReasonGUID = null,
                    Remark = null,
                    ProductType = 0,
                    CreditAppTableGUID = (creditAppTableByMigrate != null) ? creditAppTableByMigrate.CreditAppTableGUID.GuidNullToString() : creditAppTable.CreditAppTableGUID,
                    CreditAppRequestTableGUID = (creditAppRequestTableByMigrate != null) ? creditAppRequestTableByMigrate.CreditAppRequestTableGUID.GuidNullToString() : creditAppRequestTable.CreditAppRequestTableGUID,
                    CreditLimitTypeGUID = (creditAppTableByMigrate != null) ? creditAppTableByMigrate.CreditLimitTypeGUID.GuidNullToString() : creditAppTable.CreditLimitTypeGUID,
                    ApprovedCreditLimit = 0,
                    ApprovedCreditLimitLine = 0,
                    TotalInterestPct = 0,
                    ConsortiumTableGUID = null,
                    BuyerTableGUID = null,
                    BuyerName = null,
                    BuyerAgreementDescription = null,
                    BuyerAgreementReferenceId = null,
                    WithdrawalTableGUID = null,
                    WithdrawalTable_DueDate = null,
                    RefMainAgreementTableGUID = null,
                    Agreementextension = 0,
                    MainAgreementTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion MainAgreementTable

                #region BusinessCollateralAgmTable
                Guid businessCollateralAgmTableCancelledStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Cancelled).ToString()).DocumentStatusGUID;
                BusinessCollateralAgmTableItemView businessCollateralAgmTable = new BusinessCollateralAgmTableItemView
                {
                    InternalBusinessCollateralAgmId = "Migrate",
                    BusinessCollateralAgmId = "Migrate",
                    Description = "Migration transaction",
                    AgreementDocType = 0,
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    CustomerName = "Migration transaction",
                    CustomerAltName = null,
                    AgreementDate = "01/01/1900",
                    SigningDate = null,
                    DocumentStatusGUID = businessCollateralAgmTableCancelledStatus.GuidNullToString(),
                    DocumentReasonGUID = null,
                    Remark = null,
                    ProductType = 0,
                    CreditAppRequestTableGUID = (creditAppRequestTableByMigrate != null) ? creditAppRequestTableByMigrate.CreditAppRequestTableGUID.GuidNullToString() : creditAppRequestTable.CreditAppRequestTableGUID,
                    CreditLimitTypeGUID = (creditAppRequestTableByMigrate != null) ? creditAppRequestTableByMigrate.CreditLimitTypeGUID.GuidNullToString() : creditAppRequestTable.CreditLimitTypeGUID,
                    CreditAppTableGUID = (creditAppTableByMigrate != null) ? creditAppTableByMigrate.CreditAppTableGUID.GuidNullToString() : creditAppTable.CreditAppTableGUID,
                    ConsortiumTableGUID = null,
                    DBDRegistrationId = null,
                    DBDRegistrationDescription = null,
                    DBDRegistrationDate = null,
                    DBDRegistrationAmount = 0,
                    AttachmentRemark = null,
                    RefBusinessCollateralAgmTableGUID = null,
                    AgreementExtension = 0,
                    BusinessCollateralAgmTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion BusinessCollateralAgmTable

                #region BusinessCollateralAgmLine
                BusinessCollateralAgmLineItemView businessCollateralAgmLine = new BusinessCollateralAgmLineItemView
                {
                    OriginalBusinessCollateralAgreementTableGUID = null,
                    OriginalBusinessCollateralAgreementLineGUID = null,
                    BusinessCollateralAgmTableGUID = (businessCollateralAgmTableByMigrate != null) ? businessCollateralAgmTableByMigrate.BusinessCollateralAgmTableGUID.GuidNullToString() : businessCollateralAgmTable.BusinessCollateralAgmTableGUID,
                    LineNum = 1,
                    CreditAppReqBusinessCollateralGUID = (creditAppReqBusinessCollateralByMigrate != null) ? creditAppReqBusinessCollateralByMigrate.CreditAppReqBusinessCollateralGUID.GuidNullToString() : creditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID,
                    BusinessCollateralTypeGUID = (creditAppReqBusinessCollateralByMigrate != null) ? creditAppReqBusinessCollateralByMigrate.BusinessCollateralTypeGUID.GuidNullToString() : creditAppReqBusinessCollateral.BusinessCollateralTypeGUID,
                    BusinessCollateralSubTypeGUID = (creditAppReqBusinessCollateralByMigrate != null) ? creditAppReqBusinessCollateralByMigrate.BusinessCollateralSubTypeGUID.GuidNullToString() : creditAppReqBusinessCollateral.BusinessCollateralSubTypeGUID,
                    Product = null,
                    BusinessCollateralValue = 0,
                    Description = "Migration transaction",
                    Cancelled = false,
                    BuyerTableGUID = null,
                    BuyerName = null,
                    BuyerTaxIdentificationId = null,
                    BankGroupGUID = null,
                    BankTypeGUID = null,
                    AccountNumber = null,
                    RefAgreementId = null,
                    RefAgreementDate = null,
                    PreferentialCreditorNumber = 0,
                    Lessee = null,
                    Lessor = null,
                    RegistrationPlateNumber = null,
                    ChassisNumber = null,
                    MachineNumber = null,
                    MachineRegisteredStatus = null,
                    RegisteredPlace = null,
                    Quantity = 0,
                    Unit = null,
                    GuaranteeAmount = 0,
                    ProjectName = null,
                    TitleDeedNumber = null,
                    TitleDeedSubDistrict = null,
                    TitleDeedDistrict = null,
                    TitleDeedProvince = null,
                    DateOfValuation = null,
                    CapitalValuation = 0,
                    ValuationCommittee = null,
                    Ownership = null,
                    BusinessCollateralAgmLineGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion BusinessCollateralAgmLine

                #region ServiceFeeTrans
                ServiceFeeTransItemView serviceFeeTrans = new ServiceFeeTransItemView
                {
                    Ordering = 1,
                    InvoiceRevenueTypeGUID = invoiceRevenueType.InvoiceRevenueTypeGUID.GuidNullToString(),
                    Description = "Migrate",
                    FeeAmount = 0,
                    IncludeTax = false,
                    TaxTableGUID = null,
                    TaxAmount = 0,
                    AmountBeforeTax = 0,
                    AmountIncludeTax = 0,
                    WithholdingTaxTableGUID = null,
                    WHTAmount = 0,
                    WHTSlipReceivedByCustomer = false,
                    SettleAmount = 0,
                    SettleWHTAmount = 0,
                    SettleInvoiceAmount = 0,
                    RefType = 0,
                    RefGUID = "00000000-0000-0000-0000-000000000000",
                    Dimension1GUID = null,
                    Dimension2GUID = null,
                    Dimension3GUID = null,
                    Dimension4GUID = null,
                    Dimension5GUID = null,
                    CNReasonGUID = null,
                    OrigInvoiceId = null,
                    OrigInvoiceAmount = 0,
                    OrigTaxInvoiceId = null,
                    OrigTaxInvoiceAmount = 0,
                    ServiceFeeTransGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion ServiceFeeTrans

                #region MessengerJobTable
                Guid messengerJobStatusCancelledStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MessengerJobStatus.Cancelled).ToString()).DocumentStatusGUID;
                MessengerJobTableItemView messengerJobTable = new MessengerJobTableItemView
                {
                    JobId = "Migrate",
                    JobDate = "01/01/1900",
                    JobStartTime = null,
                    JobEndTime = null,
                    ContactTo = 2,
                    CustomerTableGUID = null,
                    BuyerTableGUID = null,
                    RefMessengerJobTableGUID = null,
                    ContactName = "Migration transaction",
                    JobTypeGUID = jobType.JobTypeGUID.GuidNullToString(),
                    MessengerTableGUID = null,
                    Priority = 0,
                    JobDetail = "Migration transaction",
                    DocumentStatusGUID = messengerJobStatusCancelledStatus.GuidNullToString(),
                    RequestorGUID = employeeTable.EmployeeTableGUID.GuidNullToString(),
                    Map = false,
                    AssignmentAgreementTableGUID = null,
                    ContactPersonName = null,
                    ContactPersonPosition = null,
                    ContactPersonPhone = null,
                    ContactPersonExtension = null,
                    ContactPersonMobile = null,
                    ContactPersonDepartment = null,
                    Address1 = null,
                    Address2 = null,
                    ProductType = 0,
                    RefCreditAppLineGUID = null,
                    Result = 0,
                    FeeAmount = 0,
                    ResultRemark = null,
                    RefType = 63,
                    RefGUID = null,
                    MessengerJobTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion MessengerJobTable

                #region CustomerRefundTable
                Guid customerRefundStatusCancelledStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CustomerRefundStatus.Cancelled).ToString()).DocumentStatusGUID;

                CustomerRefundTableItemView customerRefundTable = new CustomerRefundTableItemView
                {
                    CustomerRefundId = "Migrate",
                    Description = "Migration transaction",
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    TransDate = "01/01/1900",
                    ProductType = 0,
                    CreditAppTableGUID = null,
                    SuspenseInvoiceType = 0,
                    DocumentStatusGUID = customerRefundStatusCancelledStatus.GuidNullToString(),
                    DocumentReasonGUID = null,
                    CurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString(),
                    Remark = null,
                    OperReportSignatureGUID = companyParameter.OperReportSignatureGUID.GuidNullToString(),
                    PaymentAmount = 0,
                    SuspenseAmount = 0,
                    ServiceFeeAmount = 0,
                    SettleAmount = 0,
                    ExchangeRate = 1,
                    RetentionCalculateBase = 0,
                    RetentionPct = 0,
                    RetentionAmount = 0,
                    Dimension1GUID = null,
                    Dimension2GUID = null,
                    Dimension3GUID = null,
                    Dimension4GUID = null,
                    Dimension5GUID = null,
                    CustomerRefundTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion CustomerRefundTable

                #region PaymentDetail
                PaymentDetailItemView paymentDetail = new PaymentDetailItemView
                {
                    PaidToType = 2,
                    InvoiceTypeGUID = null,
                    SuspenseTransfer = false,
                    CustomerTableGUID = null,
                    VendorTableGUID = null,
                    PaymentAmount = 0,
                    RefType = (int)RefType.CustomerRefund,
                    RefGUID = (customerRefundTableByMigrate != null) ? customerRefundTableByMigrate.CustomerRefundTableGUID.GuidNullToString() : customerRefundTable.CustomerRefundTableGUID,
                    PaymentDetailGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion PaymentDetail

                #region InvoiceTable
                Guid InvoiceTableCancelledStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)InvoiceStatus.Cancelled).ToString()).DocumentStatusGUID;

                InvoiceTableItemView invoiceTable = new InvoiceTableItemView
                {
                    InvoiceId = "Migrate",
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    CustomerName = "Migration transaction",
                    InvoiceTypeGUID = invoiceType.InvoiceTypeGUID.GuidNullToString(),
                    DocumentStatusGUID = InvoiceTableCancelledStatus.GuidNullToString(),
                    ProductType = 0,
                    ProductInvoice = false,
                    SuspenseInvoiceType = 0,
                    CreditAppTableGUID = null,
                    IssuedDate = "01/01/1900",
                    DueDate = "01/01/1900",
                    MethodOfPaymentGUID = null,
                    DocumentId = null,
                    BuyerTableGUID = null,
                    BuyerInvoiceTableGUID = null,
                    BuyerAgreementTableGUID = null,
                    Remark = null,
                    AccountingPeriod = 0,
                    MarketingPeriod = 0,
                    InvoiceAddress1 = null,
                    InvoiceAddress2 = null,
                    TaxBranchId = null,
                    TaxBranchName = null,
                    MailingInvoiceAddress1 = null,
                    MailingInvoiceAddress2 = null,
                    CurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString(),
                    ExchangeRate = 1,
                    InvoiceAmountBeforeTax = 0,
                    TaxAmount = 0,
                    InvoiceAmount = 0,
                    WHTAmount = 0,
                    InvoiceAmountBeforeTaxMST = 0,
                    TaxAmountMST = 0,
                    InvoiceAmountMST = 0,
                    WHTBaseAmount = 0,
                    CNReasonGUID = null,
                    OrigInvoiceId = null,
                    OrigInvoiceAmount = 0,
                    OrigTaxInvoiceId = null,
                    OrigTaxInvoiceAmount = 0,
                    RefTaxInvoiceGUID = null,
                    RefInvoiceGUID = null,
                    RefType = 0,
                    RefGUID = "00000000-0000-0000-0000-000000000000",
                    ReceiptTempTableGUID = null,
                    Dimension1GUID = null,
                    Dimension2GUID = null,
                    Dimension3GUID = null,
                    Dimension4GUID = null,
                    Dimension5GUID = null,
                    BranchGUID = company.DefaultBranchGUID.GuidNullToString(),
                    InvoiceTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion InvoiceTable

                #region InvoiceSettlementDetail
                InvoiceSettlementDetailItemView invoiceSettlementDetail = new InvoiceSettlementDetailItemView
                {
                    ProductType = 0,
                    DocumentId = "Migrate",
                    InvoiceTableGUID = null,
                    InvoiceDueDate = null,
                    InvoiceTypeGUID = null,
                    SuspenseInvoiceType = 0,
                    InvoiceAmount = 0,
                    BalanceAmount = 0,
                    SettleAmount = 0,
                    WHTAmount = 0,
                    WHTSlipReceivedByCustomer = false,
                    SettleInvoiceAmount = 0,
                    SettleTaxAmount = 0,
                    BuyerAgreementTableGUID = null,
                    WHTSlipReceivedByBuyer = false,
                    AssignmentAgreementTableGUID = null,
                    SettleAssignmentAmount = 0,
                    PurchaseAmount = 0,
                    PurchaseAmountBalance = 0,
                    SettlePurchaseAmount = 0,
                    SettleReserveAmount = 0,
                    InterestCalculationMethod = 0,
                    RetentionAmountAccum = 0,
                    RefType = 0,
                    RefReceiptTempPaymDetailGUID = null,
                    RefGUID = "00000000-0000-0000-0000-000000000000",
                    InvoiceSettlementDetailGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion InvoiceSettlementDetail

                #region ReceiptTempTable
                Guid receiptTempTableCancelledStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ReceiptStatus.Cancelled).ToString()).DocumentStatusGUID;

                ReceiptTempTableItemView receiptTempTable = new ReceiptTempTableItemView
                {
                    ReceiptTempId = "Migrate",
                    ReceiptDate = "01/01/1900",
                    TransDate = "01/01/1900",
                    ProductType = 0,
                    ReceivedFrom = 0,
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    BuyerTableGUID = null,
                    BuyerReceiptTableGUID = null,
                    DocumentStatusGUID = receiptTempTableCancelledStatus.GuidNullToString(),
                    DocumentReasonGUID = null,
                    CurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString(),
                    ReceiptAmount = 0,
                    SettleFeeAmount = 0,
                    SettleAmount = 0,
                    ExchangeRate = 1,
                    Remark = null,
                    SuspenseInvoiceTypeGUID = null,
                    SuspenseAmount = 0,
                    CreditAppTableGUID = null,
                    ReceiptTempRefType = 0,
                    MainReceiptTempGUID = null,
                    RefReceiptTempGUID = null,
                    Dimension1GUID = null,
                    Dimension2GUID = null,
                    Dimension3GUID = null,
                    Dimension4GUID = null,
                    Dimension5GUID = null,
                    ReceiptTempTableGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion ReceiptTempTable

                #region ProcessTrans
                ProcessTransItemView processTrans = new ProcessTransItemView
                {
                    CustomerTableGUID = "00000000-0000-0000-0000-000000000000",
                    CreditAppTableGUID = null,
                    ProductType = 0,
                    TransDate = "01/01/1900",
                    ProcessTransType = 0,
                    ARLedgerAccount = "",
                    Amount = 0,
                    AmountMST = 0,
                    TaxAmount = 0,
                    TaxAmountMST = 0,
                    CurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString(),
                    ExchangeRate = 1,
                    DocumentReasonGUID = null,
                    RefTaxInvoiceGUID = null,
                    TaxTableGUID = null,
                    OrigTaxTableGUID = null,
                    RefType = 0,
                    RefGUID = "00000000-0000-0000-0000-000000000000",
                    SourceRefType = 0,
                    SourceRefId = "Migrate",
                    InvoiceTableGUID = null,
                    DocumentId = "Migrate",
                    PaymentHistoryGUID = null,
                    PaymentDetailGUID = null,
                    Revert = false,
                    RefProcessTransGUID = null,
                    StagingBatchId = "Migrate",
                    StagingBatchStatus = 1,
                    ProcessTransGUID = Guid.NewGuid().GuidNullToString(),
                    Owner = "Migrate",
                    OwnerBusinessUnitGUID = businessUnit.BusinessUnitGUID.GuidNullToString(),
                    CompanyGUID = company.CompanyGUID.GuidNullToString(),
                };
                #endregion ProcessTrans

                return new GenMigrationTransactionResultView()
                {
                    ProductSubType = new List<ProductSubTypeItemView>() {productSubType},
                    CreditLimitType = new List<CreditLimitTypeItemView>() { creditLimitType },
                    AddressTrans = new List<AddressTransItemView>() { addressTrans },
                    CreditAppRequestTable = new List<CreditAppRequestTableItemView>() { creditAppRequestTable },
                    CreditAppReqBusinessCollateral = new List<CreditAppReqBusinessCollateralItemView>() { creditAppReqBusinessCollateral },
                    CreditAppTable = new List<CreditAppTableItemView>() { creditAppTable },
                    MainAgreementTable = new List<MainAgreementTableItemView>() { mainAgreementTable },
                    BusinessCollateralAgmTable = new List<BusinessCollateralAgmTableItemView>() { businessCollateralAgmTable },
                    BusinessCollateralAgmLine = new List<BusinessCollateralAgmLineItemView>() { businessCollateralAgmLine },
                    ServiceFeeTrans = new List<ServiceFeeTransItemView>() { serviceFeeTrans },
                    MessengerJobTable = new List<MessengerJobTableItemView>() { messengerJobTable },
                    CustomerRefundTable = new List<CustomerRefundTableItemView>() { customerRefundTable },
                    PaymentDetail = new List<PaymentDetailItemView>() { paymentDetail },
                    InvoiceTable = new List<InvoiceTableItemView>() { invoiceTable },
                    InvoiceSettlementDetail = new List<InvoiceSettlementDetailItemView>() { invoiceSettlementDetail },
                    ReceiptTempTable = new List<ReceiptTempTableItemView>() { receiptTempTable },
                    ProcessTrans = new List<ProcessTransItemView>() { processTrans },
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public GenMigrationTransactionResultView GenMigrationTransaction(string companyGUID)
        {
            try
            {
                GenMigrationTransactionResultView initGenMiGrationTransaction = InitGenMiGrationTransaction(companyGUID.StringToGuid());

                #region check result count
                bool hasProductSubType = initGenMiGrationTransaction.ProductSubType.Count > 0;
                bool hasCreditLimitType = initGenMiGrationTransaction.CreditLimitType.Count > 0;
                bool hasAddressTrans = initGenMiGrationTransaction.AddressTrans.Count > 0;
                bool hasCreditAppRequestTable = initGenMiGrationTransaction.CreditAppRequestTable.Count > 0;
                bool hasCreditAppReqBusinessCollateral = initGenMiGrationTransaction.CreditAppReqBusinessCollateral.Count > 0;
                bool hasCreditAppTable = initGenMiGrationTransaction.CreditAppTable.Count > 0;
                bool hasMainAgreementTable = initGenMiGrationTransaction.MainAgreementTable.Count > 0;
                bool hasBusinessCollateralAgmTable = initGenMiGrationTransaction.BusinessCollateralAgmTable.Count > 0;
                bool hasBusinessCollateralAgmLine = initGenMiGrationTransaction.BusinessCollateralAgmLine.Count > 0;
                bool hasServiceFeeTrans = initGenMiGrationTransaction.ServiceFeeTrans.Count > 0;
                bool hasMessengerJobTable = initGenMiGrationTransaction.MessengerJobTable.Count > 0;
                bool hasCustomerRefundTable = initGenMiGrationTransaction.CustomerRefundTable.Count > 0;
                bool hasPaymentDetail = initGenMiGrationTransaction.PaymentDetail.Count > 0;
                bool hasInvoiceTable = initGenMiGrationTransaction.InvoiceTable.Count > 0;
                bool hasInvoiceSettlementDetail = initGenMiGrationTransaction.InvoiceSettlementDetail.Count > 0;
                bool hasReceiptTempTable = initGenMiGrationTransaction.ReceiptTempTable.Count > 0;
                bool hasProcessTrans = initGenMiGrationTransaction.ProcessTrans.Count > 0;
                #endregion

                if (hasProductSubType ||
                    hasCreditLimitType ||
                    hasAddressTrans ||
                    hasCreditAppRequestTable ||
                    hasCreditAppReqBusinessCollateral ||
                    hasCreditAppTable ||
                    hasMainAgreementTable ||
                    hasBusinessCollateralAgmTable ||
                    hasBusinessCollateralAgmLine ||
                    hasServiceFeeTrans ||
                    hasMessengerJobTable ||
                    hasCustomerRefundTable ||
                    hasPaymentDetail ||
                    hasInvoiceTable ||
                    hasInvoiceSettlementDetail ||
                    hasReceiptTempTable ||
                    hasProcessTrans)
                {
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(initGenMiGrationTransaction.ProductSubType.ToProductSubType().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.CreditLimitType.ToCreditLimitType().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.AddressTrans.ToAddressTrans().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.CreditAppRequestTable.ToCreditAppRequestTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.CreditAppReqBusinessCollateral.ToCreditAppReqBusinessCollateral().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.CreditAppTable.ToCreditAppTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.MainAgreementTable.ToMainAgreementTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.BusinessCollateralAgmTable.ToBusinessCollateralAgmTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.BusinessCollateralAgmLine.ToBusinessCollateralAgmLine().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.ServiceFeeTrans.ToServiceFeeTrans().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.MessengerJobTable.ToMessengerJobTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.CustomerRefundTable.ToCustomerRefundTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.PaymentDetail.ToPaymentDetail().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.InvoiceTable.ToInvoiceTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.InvoiceSettlementDetail.ToInvoiceSettlementDetail().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.ReceiptTempTable.ToReceiptTempTable().ToList());
                        this.BulkInsert(initGenMiGrationTransaction.ProcessTrans.ToProcessTrans().ToList());
                        UnitOfWork.Commit(transaction);
                    }
                }
                else
                {
                    UnitOfWork.Commit();
                }

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.SUCCESS");
                initGenMiGrationTransaction.Notification = success;
                return initGenMiGrationTransaction;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool ValidateUpdateChequeReference()
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                List<ChequeTable> cheuqTable = (from ct in db.Set<ChequeTable>()
                                 where (ct.RefType == (int)RefType.PurchaseTable || ct.RefType == (int)RefType.WithdrawalTable) &&
                                        ct.RefGUID == null
                                 select ct).ToList();
                if(cheuqTable.Count() == 0)
                {
                    ex.AddData("ERROR.90157", "LABEL.CHEQUE");
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        public UpdateChequeReferenceResultView UpdateChequeReference()
        {
            try
            {
                UpdateChequeReferenceResultView result = new UpdateChequeReferenceResultView();

                var list1 = (from cht in db.Set<ChequeTable>()
                                           join wt in db.Set<WithdrawalLine>()
                                           on cht.ChequeTableGUID equals wt.CustomerPDCTableGUID
                                           where (cht.RefType == (int)RefType.WithdrawalTable) 
                                                  && cht.RefGUID == null
                                           select new { cht, wt.WithdrawalTableGUID }).ToList();
                foreach(var item in list1)
                {
                    if(!result.ChequeTable.Any(w => w.ChequeTableGUID == item.cht.ChequeTableGUID))
                    {
                        item.cht.RefGUID = item.WithdrawalTableGUID;
                        result.ChequeTable.Add(item.cht);
                    }
                }

                var list2 = (from cht in db.Set<ChequeTable>()
                             join wt in db.Set<WithdrawalLine>()
                             on cht.ChequeTableGUID equals wt.BuyerPDCTableGUID
                             where (cht.RefType == (int)RefType.WithdrawalTable)
                                    && cht.RefGUID == null
                             select new { cht, wt.WithdrawalTableGUID }).ToList();
                foreach (var item in list2)
                {
                    if (!result.ChequeTable.Any(w => w.ChequeTableGUID == item.cht.ChequeTableGUID))
                    {
                        item.cht.RefGUID = item.WithdrawalTableGUID;
                        result.ChequeTable.Add(item.cht);
                    }
                }

                var list3 = (from cht in db.Set<ChequeTable>()
                             join pl in db.Set<PurchaseLine>()
                             on cht.ChequeTableGUID equals pl.CustomerPDCTableGUID
                             where (cht.RefType == (int)RefType.PurchaseTable)
                                    && cht.RefGUID == null
                             select new { cht, pl.PurchaseTableGUID }).ToList();
                foreach (var item in list3)
                {
                    if (!result.ChequeTable.Any(w => w.ChequeTableGUID == item.cht.ChequeTableGUID))
                    {
                        item.cht.RefGUID = item.PurchaseTableGUID;
                        result.ChequeTable.Add(item.cht);
                    }
                }

                var list4 = (from cht in db.Set<ChequeTable>()
                             join pl in db.Set<PurchaseLine>()
                             on cht.ChequeTableGUID equals pl.BuyerPDCTableGUID
                             where (cht.RefType == (int)RefType.PurchaseTable)
                                    && cht.RefGUID == null
                             select new { cht, pl.PurchaseTableGUID }).ToList();
                foreach (var item in list4)
                {
                    if (!result.ChequeTable.Any(w => w.ChequeTableGUID == item.cht.ChequeTableGUID))
                    {
                        item.cht.RefGUID = item.PurchaseTableGUID;
                        result.ChequeTable.Add(item.cht);
                    }
                }

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkUpdate(result.ChequeTable);
                    UnitOfWork.Commit(transaction);
                }

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.90023",new string[] { "LABEL.CHEQUE", result.ChequeTable.Count().ToString() });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        #region GenPuLineInvoice
        public bool ValidateGenPULineInvoice(string companyGUID)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(companyGUID.StringToGuid());


                if (companyParameter.FTInvTypeGUID == null)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                else
                {
                    IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                    InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParameter.FTInvTypeGUID.Value);
                    if(invoiceType.AutoGenInvoiceRevenueTypeGUID == null)
                    {
                        ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_INV_TYPE_ID");

                    }
                }

                if (companyParameter.FTReserveInvRevenueTypeGUID == null)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_RESERVE_INV_REVENUE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }

                if(ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        public GenPULineInvoiceResultView GenPULineInvoice(string companyGUID)
        {
            try
            {
                IInvoiceService invoiceService = new InvoiceService(db);
                IInvoiceLineService invoiceLineService = new InvoiceLineService(db);
                IExchangeRateService exchangeRateService = new ExchangeRateService(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);

                ICompanyRepo companyRepo = new CompanyRepo(db);
                Company company = companyRepo.GetCompanyByIdNoTracking(companyGUID.StringToGuid());

                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(company.CompanyGUID);

                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParameter.FTInvTypeGUID.Value);

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                Guid purchasePostedStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Posted).ToString()).DocumentStatusGUID;
                Guid invoiceDraftStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(InvoiceStatus.Draft).ToString()).DocumentStatusGUID;

                // create list
                List<InvoiceTable> invoiceTable_CreateList = new List<InvoiceTable>();
                List<InvoiceLine> invoiceLine_CreateList = new List<InvoiceLine>();
                List<ProcessTrans> processTrans_CreateList = new List<ProcessTrans>();
                List<RetentionTrans> retentionTrans_CreateList = new List<RetentionTrans>();

                List<CustTrans> custTranse_CreateList = new List<CustTrans>();
                List<CreditAppTrans> creditAppTranse_CreateList = new List<CreditAppTrans>();
                List<TaxInvoiceTable> taxInvoiceTable_CreateList = new List<TaxInvoiceTable>();
                List<TaxInvoiceLine> taxInvoiceLine_CreateList = new List<TaxInvoiceLine>();

                List<PurchaseLine> purchaseLine_UpdateList = new List<PurchaseLine>();

                var purchaseLines = (from pl in db.Set<PurchaseLine>()
                                                    join pt in db.Set<PurchaseTable>()
                                                    on pl.PurchaseTableGUID equals pt.PurchaseTableGUID
                                                    join ct in db.Set<CustomerTable>()
                                                    on pt.CustomerTableGUID equals ct.CustomerTableGUID
                                                    join ca in db.Set<CreditAppTable>()
                                                    on pt.CreditAppTableGUID equals ca.CreditAppTableGUID

                                                    join iat in db.Set<AddressTrans>()
                                                    on ca.InvoiceAddressGUID equals iat.AddressTransGUID into ljIAddressTrans
                                                    from iat in ljIAddressTrans.DefaultIfEmpty()

                                                    join mat in db.Set<AddressTrans>()
                                                    on ca.InvoiceAddressGUID equals mat.AddressTransGUID into ljMAddressTrans
                                                    from mat in ljMAddressTrans.DefaultIfEmpty()

                                                    where pt.DocumentStatusGUID == purchasePostedStatus && pl.PurchaseLineInvoiceTableGUID == null 
                                                    select new { 
                                                        pt.PurchaseDate,
                                                        pl.DueDate,
                                                        pt.CustomerTableGUID,
                                                        ct.Name,
                                                        ct.CurrencyGUID,
                                                        IAddress1 = iat.Address1,
                                                        IAddress2 = iat.Address2,
                                                        iat.TaxBranchId,
                                                        iat.TaxBranchName,
                                                        MAddress1 = mat.Address1,
                                                        MAddress2 = mat.Address2,
                                                        pt.ProductType,
                                                        pt.CreditAppTableGUID,
                                                        pl.PurchaseLineGUID,
                                                        pt.PurchaseId,
                                                        pl.BuyerTableGUID,
                                                        pl.BuyerInvoiceTableGUID,
                                                        pl.BuyerAgreementTableGUID,
                                                        pl.Dimension1GUID,
                                                        pl.Dimension2GUID,
                                                        pl.Dimension3GUID,
                                                        pl.Dimension4GUID,
                                                        pl.Dimension5GUID,
                                                        pl.MethodOfPaymentGUID,
                                                        pl.CompanyGUID,
                                                        pl.RefPurchaseLineGUID,
                                                        pl.PurchaseAmount,
                                                        pl.LinePurchaseAmount,
                                                        pl.ReserveAmount,
                                                        pt.PurchaseTableGUID
                                                    }).ToList();

                int refTypePurchaseLine = (int)RefType.PurchaseLine;

                #region 1.1 Create Invoice

                foreach (var item in purchaseLines)
                {
                    decimal exchangeRate = exchangeRateService.GetExchangeRate(item.CurrencyGUID, item.PurchaseDate);
                    InvoiceTable invoiceTable = new InvoiceTable
                    {
                        InvoiceId = "",
                        IssuedDate = item.PurchaseDate,
                        DueDate = item.DueDate,
                        CustomerTableGUID = item.CustomerTableGUID,
                        CustomerName = item.Name,
                        CurrencyGUID = item.CurrencyGUID,
                        ExchangeRate = exchangeRate,
                        InvoiceAddress1 = item.IAddress1,
                        InvoiceAddress2 = item.IAddress2,
                        TaxBranchId = item.TaxBranchId,
                        TaxBranchName = item.TaxBranchName,
                        MailingInvoiceAddress1 = item.MAddress1,
                        MailingInvoiceAddress2 = item.MAddress2,
                        DocumentStatusGUID = invoiceDraftStatus,
                        InvoiceTypeGUID = companyParameter.FTInvTypeGUID.Value,
                        ProductInvoice = invoiceType.ProductInvoice,
                        SuspenseInvoiceType = invoiceType.SuspenseInvoiceType,
                        ProductType = item.ProductType,
                        CreditAppTableGUID = item.CreditAppTableGUID,
                        RefType = refTypePurchaseLine,
                        RefGUID = item.PurchaseLineGUID,
                        DocumentId = item.PurchaseId,
                        BuyerTableGUID = item.BuyerTableGUID,
                        BuyerInvoiceTableGUID = item.BuyerInvoiceTableGUID,
                        BuyerAgreementTableGUID = item.BuyerAgreementTableGUID,
                        InvoiceAmountBeforeTax = 0,
                        InvoiceAmountBeforeTaxMST = 0,
                        TaxAmount = 0,
                        TaxAmountMST = 0,
                        InvoiceAmount = 0,
                        InvoiceAmountMST = 0,
                        MarketingPeriod = 0,
                        AccountingPeriod = 0,
                        Remark = "",
                        OrigInvoiceAmount = 0,
                        OrigTaxInvoiceAmount = 0,
                        CNReasonGUID = null,
                        Dimension1GUID = item.Dimension1GUID,
                        Dimension2GUID = item.Dimension2GUID,
                        Dimension3GUID = item.Dimension3GUID,
                        Dimension4GUID = item.Dimension4GUID,
                        Dimension5GUID = item.Dimension5GUID,
                        MethodOfPaymentGUID = item.MethodOfPaymentGUID,
                        RefTaxInvoiceGUID = null,
                        OrigInvoiceId = "",
                        OrigTaxInvoiceId = "",
                        RefInvoiceGUID = null,
                        InvoiceTableGUID = Guid.NewGuid(),
                        CompanyGUID = company.CompanyGUID,
                        BranchGUID = company.DefaultBranchGUID.Value
                    };

                    #region 1.2 1.3

                    InvoiceLineAmountParamView invoiceLineAmountParamView = new InvoiceLineAmountParamView
                    {
                        IssuedDate = item.PurchaseDate,
                        InvoiceAmount = (item.RefPurchaseLineGUID != null) ? item.PurchaseAmount : item.LinePurchaseAmount,
                        IncludeTax = true,
                        InvoiceRevenueTypeGUID = invoiceType.AutoGenInvoiceRevenueTypeGUID
                    };
                    InvoiceLineAmountResultView invoicelineAmountResultView = invoiceLineService.CalcInvoiceLineFromInvoiceRevenueType(invoiceLineAmountParamView);

                    InvoiceLine invoiceLine = new InvoiceLine
                    {
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        LineNum = 1,
                        Qty = invoicelineAmountResultView.Qty,
                        UnitPrice = invoicelineAmountResultView.UnitPrice,
                        TotalAmountBeforeTax = invoicelineAmountResultView.TotalAmountBeforeTax,
                        TaxAmount = invoicelineAmountResultView.TaxAmount,
                        WHTAmount = invoicelineAmountResultView.WHTAmount,
                        TotalAmount = invoicelineAmountResultView.TotalAmount,
                        WHTBaseAmount = invoicelineAmountResultView.WHTBaseAmount,
                        InvoiceRevenueTypeGUID = invoiceType.AutoGenInvoiceRevenueTypeGUID.Value,
                        Dimension1GUID = item.Dimension1GUID,
                        Dimension2GUID = item.Dimension2GUID,
                        Dimension3GUID = item.Dimension3GUID,
                        Dimension4GUID = item.Dimension4GUID,
                        Dimension5GUID = item.Dimension5GUID,
                        TaxTableGUID = invoicelineAmountResultView.TaxTableGUID,
                        WithholdingTaxTableGUID = invoicelineAmountResultView.WithholdingTaxTableGUID,
                        ProdUnit = null,
                        InvoiceLineGUID = Guid.NewGuid(),
                        CompanyGUID = invoiceTable.CompanyGUID,
                        BranchGUID = invoiceTable.BranchGUID
                    };
                    invoiceLine = invoiceService.ReplaceInvoiceLineInvoiceText(invoiceLine);
                    #endregion

                    #region 1.4 1.5

                    InvoiceLineAmountParamView invoiceLineAmountParamView2 = new InvoiceLineAmountParamView
                    {
                        IssuedDate = item.PurchaseDate,
                        InvoiceAmount = item.ReserveAmount,
                        IncludeTax = true,
                        InvoiceRevenueTypeGUID = companyParameter.FTReserveInvRevenueTypeGUID
                    };
                    InvoiceLineAmountResultView invoicelineAmountResultView2 = invoiceLineService.CalcInvoiceLineFromInvoiceRevenueType(invoiceLineAmountParamView2);

                    InvoiceLine invoiceLine2 = new InvoiceLine
                    {
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        LineNum = 2,
                        Qty = invoicelineAmountResultView2.Qty,
                        UnitPrice = invoicelineAmountResultView2.UnitPrice,
                        TotalAmountBeforeTax = invoicelineAmountResultView2.TotalAmountBeforeTax,
                        TaxAmount = invoicelineAmountResultView2.TaxAmount,
                        WHTAmount = invoicelineAmountResultView2.WHTAmount,
                        TotalAmount = invoicelineAmountResultView2.TotalAmount,
                        WHTBaseAmount = invoicelineAmountResultView2.WHTBaseAmount,
                        InvoiceRevenueTypeGUID = companyParameter.FTReserveInvRevenueTypeGUID.Value,
                        Dimension1GUID = item.Dimension1GUID,
                        Dimension2GUID = item.Dimension2GUID,
                        Dimension3GUID = item.Dimension3GUID,
                        Dimension4GUID = item.Dimension4GUID,
                        Dimension5GUID = item.Dimension5GUID,
                        TaxTableGUID = invoicelineAmountResultView2.TaxTableGUID,
                        WithholdingTaxTableGUID = invoicelineAmountResultView2.WithholdingTaxTableGUID,
                        ProdUnit = null,
                        InvoiceLineGUID = Guid.NewGuid(),
                        CompanyGUID = invoiceTable.CompanyGUID,
                        BranchGUID = invoiceTable.BranchGUID
                    };
                    invoiceLine2 = invoiceService.ReplaceInvoiceLineInvoiceText(invoiceLine2);
                    #endregion

                    #region 1.6 1.7
                    decimal sumTotalAmountBeforeTax = invoiceLine.TotalAmountBeforeTax + invoiceLine2.TotalAmountBeforeTax;
                    decimal sumTaxAmount = invoiceLine.TaxAmount + invoiceLine2.TaxAmount;
                    decimal sumTotalAmount = invoiceLine.TotalAmount + invoiceLine2.TotalAmount;

                    invoiceTable.InvoiceAmountBeforeTax = sumTotalAmountBeforeTax;
                    invoiceTable.InvoiceAmountBeforeTaxMST = sumTotalAmountBeforeTax * invoiceTable.ExchangeRate;
                    invoiceTable.TaxAmount = sumTaxAmount;
                    invoiceTable.TaxAmountMST = sumTaxAmount * invoiceTable.ExchangeRate;
                    invoiceTable.InvoiceAmount = sumTotalAmount;
                    invoiceTable.InvoiceAmountMST = sumTotalAmount * invoiceTable.ExchangeRate;
                    #endregion

                    List<InvoiceTable> invoiceTablesParm = new List<InvoiceTable>();
                    List<InvoiceLine> invoiceLinesParm = new List<InvoiceLine>();

                    invoiceTablesParm.Add(invoiceTable);
                    invoiceLinesParm.Add(invoiceLine);
                    invoiceLinesParm.Add(invoiceLine2);

                    #region 2
                    PostInvoiceResultView postInvoiceResultView = invoiceService.PostInvoice(invoiceTablesParm, invoiceLinesParm, RefType.PurchaseTable, item.PurchaseTableGUID);
                    invoiceTable_CreateList.AddRange(postInvoiceResultView.InvoiceTables);
                    invoiceLine_CreateList.AddRange(postInvoiceResultView.InvoiceLines);
                    processTrans_CreateList.AddRange(postInvoiceResultView.ProcessTranses);
                    custTranse_CreateList.AddRange(postInvoiceResultView.CustTranses);
                    creditAppTranse_CreateList.AddRange(postInvoiceResultView.CreditAppTranses);
                    taxInvoiceTable_CreateList.AddRange(postInvoiceResultView.TaxInvoiceTables);
                    taxInvoiceLine_CreateList.AddRange(postInvoiceResultView.TaxInvoiceLines);
                    retentionTrans_CreateList.AddRange(postInvoiceResultView.RetentionTranses);
                    #endregion

                    #region 3 
                    PurchaseLine purchaseLine = purchaseLineRepo.GetByIdvw(item.PurchaseLineGUID).ToPurchaseLine();
                    purchaseLine.PurchaseLineInvoiceTableGUID = invoiceTable.InvoiceTableGUID;
                    purchaseLine_UpdateList.Add(purchaseLine);
                    #endregion
                };
                #endregion

                GenPULineInvoiceResultView genPULineInvoiceResultView = new GenPULineInvoiceResultView
                {
                    InvoiceTable = invoiceTable_CreateList,
                    InvoiceLine = invoiceLine_CreateList,
                    CreditAppTrans = creditAppTranse_CreateList,
                    CustTrans = custTranse_CreateList,
                    ProcessTrans = processTrans_CreateList,
                    PurchaseLine = purchaseLine_UpdateList,
                    RetentionTrans = retentionTrans_CreateList,
                    TaxInvoiceTable = taxInvoiceTable_CreateList,
                    TaxInvoiceLine = taxInvoiceLine_CreateList
                };


                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkInsert(genPULineInvoiceResultView.InvoiceTable);
                    this.BulkInsert(genPULineInvoiceResultView.InvoiceLine);
                    this.BulkInsert(genPULineInvoiceResultView.CustTrans);
                    this.BulkInsert(genPULineInvoiceResultView.CreditAppTrans);
                    this.BulkInsert(genPULineInvoiceResultView.TaxInvoiceTable);
                    this.BulkInsert(genPULineInvoiceResultView.TaxInvoiceLine);
                    this.BulkInsert(genPULineInvoiceResultView.RetentionTrans);
                    this.BulkInsert(genPULineInvoiceResultView.ProcessTrans);

                    this.BulkUpdate(genPULineInvoiceResultView.PurchaseLine);
                    UnitOfWork.Commit(transaction);
                }

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.90024", new string[] { "LABEL.PURCHASE_LINE_INVOICE_ID", "LABEL.PURCHASE_LINE", genPULineInvoiceResultView.InvoiceTable.Count().ToString() });
                genPULineInvoiceResultView.Notification = success;
                return genPULineInvoiceResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GenWDLineInvoice
        public bool ValidateGenWDLineInvoice(string companyGUID)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(companyGUID.StringToGuid());


                if (companyParameter.PFInvTypeGUID == null)
                {
                    ex.AddData("ERROR.90047", "LABEL.PF_WITHDRAWAL_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                else
                {
                    IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                    InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParameter.PFInvTypeGUID.Value);
                    if (invoiceType.AutoGenInvoiceRevenueTypeGUID == null)
                    {
                        ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.PF_WITHDRAWAL_INVOICE_TYPE_ID");

                    }
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        public GenWDLineInvoiceResultView GenWDLineInvoice(string companyGUID)
        {
            try
            {
                IInvoiceService invoiceService = new InvoiceService(db);
                IInvoiceLineService invoiceLineService = new InvoiceLineService(db);
                IExchangeRateService exchangeRateService = new ExchangeRateService(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);

                ICompanyRepo companyRepo = new CompanyRepo(db);
                Company company = companyRepo.GetCompanyByIdNoTracking(companyGUID.StringToGuid());

                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(company.CompanyGUID);

                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParameter.PFInvTypeGUID.Value);

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                Guid purchaseWithdrawalStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(WithdrawalStatus.Posted).ToString()).DocumentStatusGUID;
                Guid invoiceDraftStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(InvoiceStatus.Draft).ToString()).DocumentStatusGUID;

                // create list
                List<InvoiceTable> invoiceTable_CreateList = new List<InvoiceTable>();
                List<InvoiceLine> invoiceLine_CreateList = new List<InvoiceLine>();
                List<ProcessTrans> processTrans_CreateList = new List<ProcessTrans>();
                List<RetentionTrans> retentionTrans_CreateList = new List<RetentionTrans>();

                List<CustTrans> custTranse_CreateList = new List<CustTrans>();
                List<CreditAppTrans> creditAppTranse_CreateList = new List<CreditAppTrans>();
                List<TaxInvoiceTable> taxInvoiceTable_CreateList = new List<TaxInvoiceTable>();
                List<TaxInvoiceLine> taxInvoiceLine_CreateList = new List<TaxInvoiceLine>();

                List<WithdrawalLine> withdrawalLine_UpdateList = new List<WithdrawalLine>();
                int withdrawalLineType = (int)WithdrawalLineType.Principal;

                var withdrawalLines = (from wl in db.Set<WithdrawalLine>()
                                       join wt in db.Set<WithdrawalTable>()
                                       on wl.WithdrawalTableGUID equals wt.WithdrawalTableGUID

                                       where wt.DocumentStatusGUID == purchaseWithdrawalStatus && wl.WithdrawalLineInvoiceTableGUID == null
                                       && wl.WithdrawalLineType == withdrawalLineType
                                       select new { wl, wt }).ToList();

                
                foreach(var item in withdrawalLines)
                {
                    #region 1 Create Invoice
                    Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView
                    {
                        RefType = RefType.WithdrawalLine,
                        RefGUID = item.wl.WithdrawalLineGUID,
                        IssuedDate = item.wt.WithdrawalDate,
                        DueDate = item.wt.DueDate,
                        CustomerTableGUID = item.wt.CustomerTableGUID,
                        ProductType = (ProductType)item.wt.ProductType,
                        InvoiceTypeGUID = companyParameter.PFInvTypeGUID.Value,
                        AutoGenInvoiceRevenueType = true,
                        InvoiceAmount = item.wt.WithdrawalAmount,
                        IncludeTax = true,
                        CompanyGUID = item.wt.CompanyGUID,
                        InvoiceRevenueTypeGUID = null,
                        DocumentId = item.wt.WithdrawalId,
                        CreditAppTableGUID = item.wt.CreditAppTableGUID,
                        BuyerTableGUID = item.wt.BuyerTableGUID,
                        Dimension1GUID = item.wl.Dimension1GUID,
                        Dimension2GUID = item.wl.Dimension2GUID,
                        Dimension3GUID = item.wl.Dimension3GUID,
                        Dimension4GUID = item.wl.Dimension4GUID,
                        Dimension5GUID = item.wl.Dimension5GUID,
                        MethodOfPaymentGUID = item.wt.MethodOfPaymentGUID,
                        BuyerInvoiceTableGUID = null,
                        BuyerAgreementTableGUID = item.wt.BuyerAgreementTableGUID,
                    };
                    GenInvoiceResultView genInvoiceResultView = invoiceService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
                    #endregion

                    #region 2 post invoice
                    PostInvoiceResultView postInvoiceResultView = invoiceService.PostInvoice(new List<InvoiceTable>() { genInvoiceResultView.InvoiceTable }, new List<InvoiceLine>() { genInvoiceResultView.InvoiceLine }, RefType.WithdrawalTable, item.wl.WithdrawalTableGUID);
                    invoiceTable_CreateList.AddRange(postInvoiceResultView.InvoiceTables);
                    invoiceLine_CreateList.AddRange(postInvoiceResultView.InvoiceLines);
                    processTrans_CreateList.AddRange(postInvoiceResultView.ProcessTranses);
                    custTranse_CreateList.AddRange(postInvoiceResultView.CustTranses);
                    creditAppTranse_CreateList.AddRange(postInvoiceResultView.CreditAppTranses);
                    taxInvoiceTable_CreateList.AddRange(postInvoiceResultView.TaxInvoiceTables);
                    taxInvoiceLine_CreateList.AddRange(postInvoiceResultView.TaxInvoiceLines);
                    retentionTrans_CreateList.AddRange(postInvoiceResultView.RetentionTranses);
                    #endregion

                    #region 3 update wl 
                    item.wl.WithdrawalLineInvoiceTableGUID = genInvoiceResultView.InvoiceTable.InvoiceTableGUID;
                    withdrawalLine_UpdateList.Add(item.wl);
                    #endregion
                }


                GenWDLineInvoiceResultView genWDLineInvoiceResultView = new GenWDLineInvoiceResultView
                {
                    InvoiceTable = invoiceTable_CreateList,
                    InvoiceLine = invoiceLine_CreateList,
                    CreditAppTrans = creditAppTranse_CreateList,
                    CustTrans = custTranse_CreateList,
                    ProcessTrans = processTrans_CreateList,
                    WithdrawalLine = withdrawalLine_UpdateList,
                    RetentionTrans = retentionTrans_CreateList,
                    TaxInvoiceTable = taxInvoiceTable_CreateList,
                    TaxInvoiceLine = taxInvoiceLine_CreateList
                };


                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkInsert(genWDLineInvoiceResultView.InvoiceTable);
                    this.BulkInsert(genWDLineInvoiceResultView.InvoiceLine);
                    this.BulkInsert(genWDLineInvoiceResultView.CustTrans);
                    this.BulkInsert(genWDLineInvoiceResultView.CreditAppTrans);
                    this.BulkInsert(genWDLineInvoiceResultView.TaxInvoiceTable);
                    this.BulkInsert(genWDLineInvoiceResultView.TaxInvoiceLine);
                    this.BulkInsert(genWDLineInvoiceResultView.RetentionTrans);
                    this.BulkInsert(genWDLineInvoiceResultView.ProcessTrans);

                    this.BulkUpdate(genWDLineInvoiceResultView.WithdrawalLine);
                    UnitOfWork.Commit(transaction);
                }

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.90024", new string[] { "LABEL.WITHDRAWAL_LINE_INVOICE_ID", "LABEL.WITHDRAWAL_LINE", genWDLineInvoiceResultView.InvoiceTable.Count().ToString() });
                genWDLineInvoiceResultView.Notification = success;
                return genWDLineInvoiceResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
