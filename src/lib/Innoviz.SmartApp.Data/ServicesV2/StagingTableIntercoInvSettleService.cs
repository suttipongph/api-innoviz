using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IStagingTableIntercoInvSettleService
	{

		StagingTableIntercoInvSettleItemView GetStagingTableIntercoInvSettleById(string id);
		StagingTableIntercoInvSettleItemView CreateStagingTableIntercoInvSettle(StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleView);
		StagingTableIntercoInvSettleItemView UpdateStagingTableIntercoInvSettle(StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleView);
		bool DeleteStagingTableIntercoInvSettle(string id);

		#region function
		#region Method118_GenIntercompanySettlementStaging
		GenIntercompanySettlementStagingView GenIntercompanySettlementStaging(IntercompanyInvoiceSettlement intercompanyInvoiceSettlement);
		#endregion Method118_GenIntercompanySettlementStaging
		#endregion function
	}
	public class StagingTableIntercoInvSettleService : SmartAppService, IStagingTableIntercoInvSettleService
	{
		public StagingTableIntercoInvSettleService(SmartAppDbContext context) : base(context) { }
		public StagingTableIntercoInvSettleService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public StagingTableIntercoInvSettleService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTableIntercoInvSettleService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public StagingTableIntercoInvSettleService() : base() { }

		public StagingTableIntercoInvSettleItemView GetStagingTableIntercoInvSettleById(string id)
		{
			try
			{
				IStagingTableIntercoInvSettleRepo stagingTableIntercoInvSettleRepo = new StagingTableIntercoInvSettleRepo(db);
				return stagingTableIntercoInvSettleRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableIntercoInvSettleItemView CreateStagingTableIntercoInvSettle(StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				stagingTableIntercoInvSettleView = accessLevelService.AssignOwnerBU(stagingTableIntercoInvSettleView);
				IStagingTableIntercoInvSettleRepo stagingTableIntercoInvSettleRepo = new StagingTableIntercoInvSettleRepo(db);
				StagingTableIntercoInvSettle stagingTableIntercoInvSettle = stagingTableIntercoInvSettleView.ToStagingTableIntercoInvSettle();
				stagingTableIntercoInvSettle = stagingTableIntercoInvSettleRepo.CreateStagingTableIntercoInvSettle(stagingTableIntercoInvSettle);
				base.LogTransactionCreate<StagingTableIntercoInvSettle>(stagingTableIntercoInvSettle);
				UnitOfWork.Commit();
				return stagingTableIntercoInvSettle.ToStagingTableIntercoInvSettleItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTableIntercoInvSettleItemView UpdateStagingTableIntercoInvSettle(StagingTableIntercoInvSettleItemView stagingTableIntercoInvSettleView)
		{
			try
			{
				IStagingTableIntercoInvSettleRepo stagingTableIntercoInvSettleRepo = new StagingTableIntercoInvSettleRepo(db);
				StagingTableIntercoInvSettle inputStagingTableIntercoInvSettle = stagingTableIntercoInvSettleView.ToStagingTableIntercoInvSettle();
				StagingTableIntercoInvSettle dbStagingTableIntercoInvSettle = stagingTableIntercoInvSettleRepo.Find(inputStagingTableIntercoInvSettle.StagingTableIntercoInvSettleGUID);
				dbStagingTableIntercoInvSettle = stagingTableIntercoInvSettleRepo.UpdateStagingTableIntercoInvSettle(dbStagingTableIntercoInvSettle, inputStagingTableIntercoInvSettle);
				base.LogTransactionUpdate<StagingTableIntercoInvSettle>(GetOriginalValues<StagingTableIntercoInvSettle>(dbStagingTableIntercoInvSettle), dbStagingTableIntercoInvSettle);
				UnitOfWork.Commit();
				return dbStagingTableIntercoInvSettle.ToStagingTableIntercoInvSettleItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteStagingTableIntercoInvSettle(string item)
		{
			try
			{
				IStagingTableIntercoInvSettleRepo stagingTableIntercoInvSettleRepo = new StagingTableIntercoInvSettleRepo(db);
				Guid stagingTableIntercoInvSettleGUID = new Guid(item);
				StagingTableIntercoInvSettle stagingTableIntercoInvSettle = stagingTableIntercoInvSettleRepo.Find(stagingTableIntercoInvSettleGUID);
				stagingTableIntercoInvSettleRepo.Remove(stagingTableIntercoInvSettle);
				base.LogTransactionDelete<StagingTableIntercoInvSettle>(stagingTableIntercoInvSettle);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region function
		#region Method118_GenIntercompanySettlementStaging
		public GenIntercompanySettlementStagingView GenIntercompanySettlementStaging(IntercompanyInvoiceSettlement intercompanyInvoiceSettlement)
		{
			try
			{
				IIntercompanyInvoiceSettlementRepo intercompanyInvoiceSettlementRepo = new IntercompanyInvoiceSettlementRepo(db);
				IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				GenIntercompanySettlementStagingView genIntercompanySettlementStagingView = new GenIntercompanySettlementStagingView();

				// Set Variable
				IntercompanyInvoiceTableItemViewMap intercompanyInvoiceTable = intercompanyInvoiceTableRepo.GetIntercompanyInvoiceTableItemViewMapById(intercompanyInvoiceSettlement.IntercompanyInvoiceTableGUID);
				CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(intercompanyInvoiceTable.CustomerTableGUID);
				AddressTransItemViewMap addressTrans = addressTransRepo.GetAddressTransViewMapById(intercompanyInvoiceTable.InvoiceAddressTransGUID);
				string intercompanyInvoiceSettlementId = null;
				string companyTaxBranchId = null;

				#region Validate
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				// 1.1 Validate Tax branch ID in Branch by Default company
				IBranchRepo branchRepo = new BranchRepo(db);
				Branch branch = branchRepo.GetBranchByCompanyGUIDNoTracking(GetCurrentCompany().StringToGuid());
				if (ConditionService.IsNotNullAndNotEmptyGUID(branch.TaxBranchGUID)) // 1.1.1
				{
					companyTaxBranchId = branchRepo.GetBranchByIdNoTracking(branch.TaxBranchGUID.Value).BranchId;
				}
                else	
				if(ConditionService.IsNullOrEmptyGUID(branch.TaxBranchGUID)) // 1.1.2
				{
					ex.AddData("ERROR.90118", new string[] { "LABEL.TAX_BRANCH_ID", "LABEL.BRANCH", branch.BranchId });
				}

				// 1.2 Validate Tax branch Id in Variable.AddressTrans 
				if (!ConditionService.IsNotNullAndNotEmpty(addressTrans.TaxBranchId))
				{
					ex.AddData("ERROR.90118", new string[] { "LABEL.TAX_BRANCH_ID", "LABEL.ADDRESS_TRANSACTIONS", addressTrans.Name });
				}
				// 1.3 Validate Number sequence is manual
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(GetCurrentCompany().StringToGuid(), ReferenceId.IntercompanyInvoiceSettlement);
				NumberSeqTable numberSeqTable = numberSeqTableRepo.GetByNumberSeqTableGUID(numberSeqParameter.NumberSeqTableGUID.HasValue ? numberSeqParameter.NumberSeqTableGUID.Value : (Guid?)null);
				if (numberSeqTable.Manual)
				{
					ex.AddData("ERROR.00317", new string[] { numberSeqTable.NumberSeqCode });
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				#endregion Validate

				// 2. Generate Staging intercompany invoice settlement
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
				IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
				ICustGroupRepo custGroupRepo = new CustGroupRepo(db);
				ICurrencyRepo currencyRepo = new CurrencyRepo(db);
				IContactTransRepo contactTransRepo = new ContactTransRepo(db);
				// 2.1 Get number sequence for IntercompanyInvoiceSettlementId
				intercompanyInvoiceSettlementId = numberSequenceService.GetNumber(numberSeqParameter.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				genIntercompanySettlementStagingView.IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlementId;
				// 2.2 Create 1 record in Table StagingTableIntercoInvSettle
				string methodofPaymentId = intercompanyInvoiceSettlement.MethodOfPaymentGUID.HasValue ? methodOfPaymentRepo.GetMethodOfPaymentByIdNoTracking(intercompanyInvoiceSettlement.MethodOfPaymentGUID.Value).MethodOfPaymentId : null;
				string custGroupId = custGroupRepo.GetCustGroupByIdNoTracking(customerTable.CustGroupGUID).CustGroupId;
				List<ContactTrans> contactTrans = contactTransRepo.GetPrimaryContactTransListByRefType(intercompanyInvoiceTable.CustomerTableGUID, (int)RefType.Customer).ToList();
				string custPhoneValue = ConditionService.IsNotZero(contactTrans.Where(w => w.ContactType == (int)ContactType.Phone).Count()) ? contactTrans.Where(w => w.ContactType == (int)ContactType.Phone).FirstOrDefault().ContactValue : null;
				string custFaxValue = ConditionService.IsNotZero(contactTrans.Where(w => w.ContactType == (int)ContactType.Fax).Count()) ? contactTrans.Where(w => w.ContactType == (int)ContactType.Fax).FirstOrDefault().ContactValue : null;
				genIntercompanySettlementStagingView.StagingTableIntercoInvSettle = new StagingTableIntercoInvSettle()
				{
					IntercompanyInvoiceSettlementId = intercompanyInvoiceSettlementId,
					InvoiceDate = intercompanyInvoiceSettlement.TransDate,
					DueDate = intercompanyInvoiceSettlement.TransDate,
					MethodOfPaymentId = methodofPaymentId,
					TaxCode = intercompanyInvoiceTable.TaxTable_TaxCode,
					SettleInvoiceAmount = intercompanyInvoiceSettlement.SettleInvoiceAmount,
					FeeLedgerAccount = intercompanyInvoiceTable.FeeLedgerAccount,
					InvoiceText = intercompanyInvoiceTable.InvoiceText,
					OrigInvoiceId = intercompanyInvoiceTable.OrigInvoiceId,
					OrigInvoiceAmount = intercompanyInvoiceTable.OrigInvoiceAmount,
					CNReasonId = intercompanyInvoiceTable.CNReason_ReasonId,
					CompanyId = intercompanyInvoiceTable.InterCompany_InterCompanyId,
					RecordType = customerTable.RecordType,
					CustomerId = customerTable.CustomerId,
					Name = intercompanyInvoiceTable.CustomerName,
					AltName = customerTable.AltName,
					CustGroupId = custGroupId,
					TaxId = customerTable.TaxID,
					CurrencyId = intercompanyInvoiceTable.Currency_CurrencyId,
                    CustPhoneValue = custPhoneValue,
                    CustFaxValue = custFaxValue,
                    Address1 = addressTrans.Address1,
					CountryId = addressTrans.AddressCountry_CountryId,
					ProvinceId = addressTrans.AddressProvince_ProvinceId,
					DistrictId = addressTrans.AddressDistrict_DistrictId,
					SubDistrictId = addressTrans.AddressSubDistrict_SubDistrictId,
					PostalCode = addressTrans.AddressPostalCode_PostalCode,
					TaxBranchId = addressTrans.TaxBranchId,
					DimensionCode1 = intercompanyInvoiceTable.Dimension1_DimensionCode,
					DimensionCode2 = intercompanyInvoiceTable.Dimension2_DimensionCode,
					DimensionCode3 = intercompanyInvoiceTable.Dimension3_DimensionCode,
					DimensionCode4 = intercompanyInvoiceTable.Dimension4_DimensionCode,
					DimensionCode5 = intercompanyInvoiceTable.Dimension5_DimensionCode,
					CompanyTaxBranchId = companyTaxBranchId,
					InterfaceStagingBatchId = null,
					InterfaceStatus = (int)InterfaceStatus.None,
					CompanyGUID = intercompanyInvoiceSettlement.CompanyGUID,
					StagingTableIntercoInvSettleGUID = Guid.NewGuid(),
					IntercompanyInvoiceSettlementGUID = intercompanyInvoiceSettlement.IntercompanyInvoiceSettlementGUID
				};

				return genIntercompanySettlementStagingView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Method118_GenIntercompanySettlementStaging
		#endregion function

	}
}
