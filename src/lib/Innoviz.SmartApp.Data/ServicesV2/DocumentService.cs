using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IDocumentService
    {

        DocumentProcessItemView GetDocumentProcessById(string id);
        DocumentProcessItemView CreateDocumentProcess(DocumentProcessItemView documentProcessView);
        DocumentProcessItemView UpdateDocumentProcess(DocumentProcessItemView documentProcessView);
        bool DeleteDocumentProcess(string id);
        DocumentStatusItemView GetDocumentStatusById(string id);
        DocumentStatusItemView CreateDocumentStatus(DocumentStatusItemView documentStatusView);
        DocumentStatusItemView UpdateDocumentStatus(DocumentStatusItemView documentStatusView);
        bool DeleteDocumentStatus(string id);
        void CreateStatus(List<DocumentProcess> processList, List<DocumentStatus> statusList, string username);
        bool InitializeDocumentProcessStatus(string jsonContent, string jsonContent_CUSTOM, string username);
        #region GetSearchCondition
        SearchCondition GetSearchConditionByPrecessId(string processId);
        SearchCondition GetSearchConditionByStatus(List<string> statuses);
        #endregion
        DocumentStatus GetDocumentStatusByStatusId(int statusId);
        DocumentStatusItemView GetInitialDataByProcess(string processId);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusByVerificationTableDropDown(SearchParameter search);
        SearchCondition GetSearchConditionByStatusID(string status);
    }
    public class DocumentService : SmartAppService, IDocumentService
    {
        public DocumentService(SmartAppDbContext context) : base(context) { }
        public DocumentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public DocumentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public DocumentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public DocumentService() : base() { }

        public DocumentProcessItemView GetDocumentProcessById(string id)
        {
            try
            {
                IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
                return documentProcessRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DocumentProcessItemView CreateDocumentProcess(DocumentProcessItemView documentProcessView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                documentProcessView = accessLevelService.AssignOwnerBU(documentProcessView);
                IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
                DocumentProcess documentProcess = documentProcessView.ToDocumentProcess();
                documentProcess = documentProcessRepo.CreateDocumentProcess(documentProcess);
                base.LogTransactionCreate<DocumentProcess>(documentProcess);
                UnitOfWork.Commit();
                return documentProcess.ToDocumentProcessItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DocumentProcessItemView UpdateDocumentProcess(DocumentProcessItemView documentProcessView)
        {
            try
            {
                IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
                DocumentProcess inputDocumentProcess = documentProcessView.ToDocumentProcess();
                DocumentProcess dbDocumentProcess = documentProcessRepo.Find(inputDocumentProcess.DocumentProcessGUID);
                dbDocumentProcess = documentProcessRepo.UpdateDocumentProcess(dbDocumentProcess, inputDocumentProcess);
                base.LogTransactionUpdate<DocumentProcess>(GetOriginalValues<DocumentProcess>(dbDocumentProcess), dbDocumentProcess);
                UnitOfWork.Commit();
                return dbDocumentProcess.ToDocumentProcessItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteDocumentProcess(string item)
        {
            try
            {
                IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
                Guid documentProcessGUID = new Guid(item);
                DocumentProcess documentProcess = documentProcessRepo.Find(documentProcessGUID);
                documentProcessRepo.Remove(documentProcess);
                base.LogTransactionDelete<DocumentProcess>(documentProcess);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public DocumentStatusItemView GetDocumentStatusById(string id)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                return documentStatusRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DocumentStatusItemView CreateDocumentStatus(DocumentStatusItemView documentStatusView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                documentStatusView = accessLevelService.AssignOwnerBU(documentStatusView);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusView.ToDocumentStatus();
                documentStatus = documentStatusRepo.CreateDocumentStatus(documentStatus);
                base.LogTransactionCreate<DocumentStatus>(documentStatus);
                UnitOfWork.Commit();
                return documentStatus.ToDocumentStatusItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DocumentStatusItemView UpdateDocumentStatus(DocumentStatusItemView documentStatusView)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus inputDocumentStatus = documentStatusView.ToDocumentStatus();
                DocumentStatus dbDocumentStatus = documentStatusRepo.Find(inputDocumentStatus.DocumentStatusGUID);
                dbDocumentStatus = documentStatusRepo.UpdateDocumentStatus(dbDocumentStatus, inputDocumentStatus);
                base.LogTransactionUpdate<DocumentStatus>(GetOriginalValues<DocumentStatus>(dbDocumentStatus), dbDocumentStatus);
                UnitOfWork.Commit();
                return dbDocumentStatus.ToDocumentStatusItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteDocumentStatus(string item)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                Guid documentStatusGUID = new Guid(item);
                DocumentStatus documentStatus = documentStatusRepo.Find(documentStatusGUID);
                documentStatusRepo.Remove(documentStatus);
                base.LogTransactionDelete<DocumentStatus>(documentStatus);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool InitializeDocumentProcessStatus(string jsonContent, string jsonContent_CUSTOM, string username)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing DocumentProcessStatus...");
                dynamic msgs = JsonConvert.DeserializeObject(jsonContent);
                List<DocumentProcess> documentProcess = msgs.DocumentProcess.ToObject<List<DocumentProcess>>();
                List<DocumentStatus> documentStatus = msgs.DocumentStatus.ToObject<List<DocumentStatus>>();
                
                dynamic msgs_custom = JsonConvert.DeserializeObject(jsonContent_CUSTOM);
                List<DocumentProcess> documentProcess_custom = msgs_custom.DocumentProcess.ToObject<List<DocumentProcess>>();
                List<DocumentStatus> documentStatus_custom = msgs_custom.DocumentStatus.ToObject<List<DocumentStatus>>();

                CreateStatus(documentProcess.Concat(documentProcess_custom).ToList(),
                            documentStatus.Concat(documentStatus_custom).ToList(), username);

                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateStatus(List<DocumentProcess> processList, List<DocumentStatus> statusList, string username)
        {
            try
            {
                List<DocumentStatus> documentStatuses = db.Set<DocumentStatus>().ToList();
                List<DocumentProcess> documentProcesss = db.Set<DocumentProcess>().ToList();
                List<DocumentStatus> insertStatusList = new List<DocumentStatus>();
                List<DocumentProcess> insertProcessList = new List<DocumentProcess>();

                foreach (DocumentProcess item in processList)
                {
                    if (!documentProcesss.Exists(t => t.DocumentProcessGUID == item.DocumentProcessGUID))
                    {
                        item.CreatedDateTime = DateTime.Now;
                        item.ModifiedDateTime = DateTime.Now;
                        item.CreatedBy = item.CreatedBy ?? username;
                        item.ModifiedBy = item.ModifiedBy ?? username;
                        insertProcessList.Add(item);
                    }
                }
                foreach (DocumentStatus item in statusList)
                {
                    if (!documentStatuses.Exists(t => t.DocumentStatusGUID == item.DocumentStatusGUID))
                    {
                        item.CreatedDateTime = DateTime.Now;
                        item.ModifiedDateTime = DateTime.Now;
                        item.CreatedBy = item.CreatedBy ?? username;
                        item.ModifiedBy = item.ModifiedBy ?? username;
                        insertStatusList.Add(item);
                    }
                }

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    if (insertStatusList.Count != 0)
                    {
                        db.BulkInsert(insertStatusList);
                    }
                    if (insertProcessList.Count != 0)
                    {
                        db.BulkInsert(insertProcessList);
                    }
                    UnitOfWork.Commit(transaction);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #region GetSearchCondition
        public SearchCondition GetSearchConditionByPrecessId(string processId)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = DocumentStatusCondition.ProcessId,
                    Value = processId,
                    Type = ColumnType.INT //Change because search not contain
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchCondition GetSearchConditionByStatus(List<string> statuses)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = DocumentStatusCondition.ProcessId,
                    Values = statuses,
                    Type = ColumnType.STRING
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DocumentStatus GetDocumentStatusByStatusId(int statusId)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                return documentStatusRepo.GetDocumentStatusByStatusId(statusId.ToString());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion
		public DocumentStatusItemView GetInitialDataByProcess(string processId)
		{
			try
			{
				IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
				DocumentProcess documentProcess = documentProcessRepo.GetDocumentProcessByIdNoTracking(processId.StringToGuid());
				return new DocumentStatusItemView
				{
					DocumentStatusGUID = new Guid().GuidNullToString(),
					DocumentProcessGUID = documentProcess.DocumentProcessGUID.GuidNullToString(),
					DocumentProcess_Values = SmartAppUtil.GetDropDownLabel(documentProcess.ProcessId, documentProcess.Description)
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusByVerificationTableDropDown(SearchParameter search)
        {
            try
            {
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = SearchConditionService.GetDocumentStatusByVerificationTableDropDown((int)EnumsDocumentProcessStatus.DocumentProcessStatus.Verification);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
            catch(Exception e)
            {
				throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SearchCondition GetSearchConditionByStatusID(string status)
        {
            try
            {
                SearchCondition condition = new SearchCondition()
                {
                    ColumnName = DocumentStatusCondition.DocumentStatusGUID,
                    Value = status,
                    Type = ColumnType.MASTER
                };
                return condition;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
