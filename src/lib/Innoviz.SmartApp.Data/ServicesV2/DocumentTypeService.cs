using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDocumentTypeService
	{

		DocumentTypeItemView GetDocumentTypeById(string id);
		DocumentTypeItemView CreateDocumentType(DocumentTypeItemView documentTypeView);
		DocumentTypeItemView UpdateDocumentType(DocumentTypeItemView documentTypeView);
		bool DeleteDocumentType(string id);
	}
	public class DocumentTypeService : SmartAppService, IDocumentTypeService
	{
		public DocumentTypeService(SmartAppDbContext context) : base(context) { }
		public DocumentTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DocumentTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DocumentTypeService() : base() { }

		public DocumentTypeItemView GetDocumentTypeById(string id)
		{
			try
			{
				IDocumentTypeRepo documentTypeRepo = new DocumentTypeRepo(db);
				return documentTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentTypeItemView CreateDocumentType(DocumentTypeItemView documentTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentTypeView = accessLevelService.AssignOwnerBU(documentTypeView);
				IDocumentTypeRepo documentTypeRepo = new DocumentTypeRepo(db);
				DocumentType documentType = documentTypeView.ToDocumentType();
				documentType = documentTypeRepo.CreateDocumentType(documentType);
				base.LogTransactionCreate<DocumentType>(documentType);
				UnitOfWork.Commit();
				return documentType.ToDocumentTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentTypeItemView UpdateDocumentType(DocumentTypeItemView documentTypeView)
		{
			try
			{
				IDocumentTypeRepo documentTypeRepo = new DocumentTypeRepo(db);
				DocumentType inputDocumentType = documentTypeView.ToDocumentType();
				DocumentType dbDocumentType = documentTypeRepo.Find(inputDocumentType.DocumentTypeGUID);
				dbDocumentType = documentTypeRepo.UpdateDocumentType(dbDocumentType, inputDocumentType);
				base.LogTransactionUpdate<DocumentType>(GetOriginalValues<DocumentType>(dbDocumentType), dbDocumentType);
				UnitOfWork.Commit();
				return dbDocumentType.ToDocumentTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentType(string item)
		{
			try
			{
				IDocumentTypeRepo documentTypeRepo = new DocumentTypeRepo(db);
				Guid documentTypeGUID = new Guid(item);
				DocumentType documentType = documentTypeRepo.Find(documentTypeGUID);
				documentTypeRepo.Remove(documentType);
				base.LogTransactionDelete<DocumentType>(documentType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
