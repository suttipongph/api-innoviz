using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBusinessUnitService
	{

		BusinessUnitItemView GetBusinessUnitById(string id);
		BusinessUnitItemView CreateBusinessUnit(BusinessUnitItemView businessUnitView);
		BusinessUnitItemView UpdateBusinessUnit(BusinessUnitItemView businessUnitView);
		bool DeleteBusinessUnit(string id);
		IEnumerable<SelectItem<BusinessUnitItemView>> GetBusinessUnitFilterExcludeBusinessUnitDropDown(SearchParameter search);
	}
	public class BusinessUnitService : SmartAppService, IBusinessUnitService
	{
		public BusinessUnitService(SmartAppDbContext context) : base(context) { }
		public BusinessUnitService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BusinessUnitService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessUnitService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BusinessUnitService() : base() { }

		public BusinessUnitItemView GetBusinessUnitById(string id)
		{
			try
			{
				IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
				return businessUnitRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessUnitItemView CreateBusinessUnit(BusinessUnitItemView businessUnitView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				businessUnitView = accessLevelService.AssignOwnerBU(businessUnitView);
				IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
				BusinessUnit businessUnit = businessUnitView.ToBusinessUnit();
				businessUnit = businessUnitRepo.CreateBusinessUnit(businessUnit);
				base.LogTransactionCreate<BusinessUnit>(businessUnit);
				UnitOfWork.Commit();
				return businessUnit.ToBusinessUnitItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessUnitItemView UpdateBusinessUnit(BusinessUnitItemView businessUnitView)
		{
			try
			{
				IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
				BusinessUnit inputBusinessUnit = businessUnitView.ToBusinessUnit();
				BusinessUnit dbBusinessUnit = businessUnitRepo.Find(inputBusinessUnit.BusinessUnitGUID);
				dbBusinessUnit = businessUnitRepo.UpdateBusinessUnit(dbBusinessUnit, inputBusinessUnit);
				base.LogTransactionUpdate<BusinessUnit>(GetOriginalValues<BusinessUnit>(dbBusinessUnit), dbBusinessUnit);
				UnitOfWork.Commit();
				return dbBusinessUnit.ToBusinessUnitItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBusinessUnit(string item)
		{
			try
			{
				IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
				Guid businessUnitGUID = new Guid(item);
				BusinessUnit businessUnit = businessUnitRepo.Find(businessUnitGUID);
				businessUnitRepo.Remove(businessUnit);
				base.LogTransactionDelete<BusinessUnit>(businessUnit);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BusinessUnitItemView>> GetBusinessUnitFilterExcludeBusinessUnitDropDown(SearchParameter search)
        {
            try
            {
				IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
				search = search.GetParentCondition(BusinessUnitCondition.BusinessUnitGUID);
				search.Conditions.SetAllNotEqual();
				return businessUnitRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
	}
}
