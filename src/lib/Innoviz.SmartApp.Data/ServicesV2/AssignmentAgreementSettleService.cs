using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAssignmentAgreementSettleService
	{

		AssignmentAgreementSettleItemView GetAssignmentAgreementSettleById(string id);
		AssignmentAgreementSettleItemView CreateAssignmentAgreementSettle(AssignmentAgreementSettleItemView assignmentAgreementSettleView);
		AssignmentAgreementSettleItemView UpdateAssignmentAgreementSettle(AssignmentAgreementSettleItemView assignmentAgreementSettleView);
		bool DeleteAssignmentAgreementSettle(string id);
		void CreateAssignmentAgreementSettleCollection(IEnumerable<AssignmentAgreementSettleItemView> assignmentAgreementSettlesView);
		UpdateAssignmentAgreementSettleResultView UpdateAssignmentSettlement(UpdateAssignmentAgreementSettleView inputModel);
		List<AssignmentAgreementSettle> InitUpdateAssignmentSettlement(UpdateAssignmentAgreementSettleView inputModel);
	}
	public class AssignmentAgreementSettleService : SmartAppService, IAssignmentAgreementSettleService
	{
		public AssignmentAgreementSettleService(SmartAppDbContext context) : base(context) { }
		public AssignmentAgreementSettleService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AssignmentAgreementSettleService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AssignmentAgreementSettleService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AssignmentAgreementSettleService() : base() { }

		public AssignmentAgreementSettleItemView GetAssignmentAgreementSettleById(string id)
		{
			try
			{
				IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
				return assignmentAgreementSettleRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AssignmentAgreementSettleItemView CreateAssignmentAgreementSettle(AssignmentAgreementSettleItemView assignmentAgreementSettleView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				assignmentAgreementSettleView = accessLevelService.AssignOwnerBU(assignmentAgreementSettleView);
				IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
				AssignmentAgreementSettle assignmentAgreementSettle = assignmentAgreementSettleView.ToAssignmentAgreementSettle();
				assignmentAgreementSettle = assignmentAgreementSettleRepo.CreateAssignmentAgreementSettle(assignmentAgreementSettle);
				base.LogTransactionCreate<AssignmentAgreementSettle>(assignmentAgreementSettle);
				UnitOfWork.Commit();
				return assignmentAgreementSettle.ToAssignmentAgreementSettleItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AssignmentAgreementSettleItemView UpdateAssignmentAgreementSettle(AssignmentAgreementSettleItemView assignmentAgreementSettleView)
		{
			try
			{
				IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
				AssignmentAgreementSettle inputAssignmentAgreementSettle = assignmentAgreementSettleView.ToAssignmentAgreementSettle();
				AssignmentAgreementSettle dbAssignmentAgreementSettle = assignmentAgreementSettleRepo.Find(inputAssignmentAgreementSettle.AssignmentAgreementSettleGUID);
				dbAssignmentAgreementSettle = assignmentAgreementSettleRepo.UpdateAssignmentAgreementSettle(dbAssignmentAgreementSettle, inputAssignmentAgreementSettle);
				base.LogTransactionUpdate<AssignmentAgreementSettle>(GetOriginalValues<AssignmentAgreementSettle>(dbAssignmentAgreementSettle), dbAssignmentAgreementSettle);
				UnitOfWork.Commit();
				return dbAssignmentAgreementSettle.ToAssignmentAgreementSettleItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAssignmentAgreementSettle(string item)
		{
			try
			{
				IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
				Guid assignmentAgreementSettleGUID = new Guid(item);
				AssignmentAgreementSettle assignmentAgreementSettle = assignmentAgreementSettleRepo.Find(assignmentAgreementSettleGUID);
				assignmentAgreementSettleRepo.Remove(assignmentAgreementSettle);
				base.LogTransactionDelete<AssignmentAgreementSettle>(assignmentAgreementSettle);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Migration
		public void CreateAssignmentAgreementSettleCollection(IEnumerable<AssignmentAgreementSettleItemView> assignmentAgreementSettlesView)
		{
			try
			{
				if (assignmentAgreementSettlesView.Any())
				{
					IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
					List<AssignmentAgreementSettle> assignmentAgreementSettles = assignmentAgreementSettlesView.ToAssignmentAgreementSettle().ToList();
					assignmentAgreementSettleRepo.ValidateAdd(assignmentAgreementSettles);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(assignmentAgreementSettles, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Migration


		#region function

		#region Update Assignment Settlement
		public bool GetUpdateAssignmentSettlementValidation(UpdateAssignmentAgreementSettleView inputModel)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
				AssignmentAgreementSettle assignmentAgreementSettle = assignmentAgreementSettleRepo.GetAssignmentAgreementSettleByIdNoTracking(inputModel.AssignmentAgreementSettleGUID.StringToGuid());
				IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
				IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
				AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(assignmentAgreementSettle.AssignmentAgreementTableGUID);

				IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
				AssignmentMethod assignmentMethod = assignmentMethodRepo.GetAssignmentMethodByIdNoTracking(assignmentAgreementTable.AssignmentMethodGUID);

				Decimal assignmentAgreementBalance = assignmentAgreementTableService.GetAssignmentBalance(assignmentAgreementTable.AssignmentAgreementTableGUID);
				if (assignmentMethod.ValidateAssignmentBalance == true && ((assignmentAgreementBalance + (assignmentAgreementSettle.SettledAmount - inputModel.NewSettledAmount)) < 0))
				{
					ex.AddData("ERROR.90172", new string[] { "LABEL.SETTLED_AMOUNT", "LABEL.ASSIGNMENT_AGREEMENT_SETTLE", "LABEL.ASSIGNMENT_AGREEMENT_AMOUNT" });
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}

		public List<AssignmentAgreementSettle> InitUpdateAssignmentSettlement(UpdateAssignmentAgreementSettleView inputModel)
		{
			try
			{
				NotificationResponse success = new NotificationResponse();

				GetUpdateAssignmentSettlementValidation(inputModel);

				IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
				AssignmentAgreementSettle assignmentAgreementSettle = assignmentAgreementSettleRepo.GetAssignmentAgreementSettleByIdNoTracking(inputModel.AssignmentAgreementSettleGUID.StringToGuid());
				AssignmentAgreementSettle revertAssignmentAgreementSettle = new AssignmentAgreementSettle()
				{
					RefType = assignmentAgreementSettle.RefType,
					RefAssignmentAgreementSettleGUID = assignmentAgreementSettle.AssignmentAgreementSettleGUID,
					AssignmentAgreementTableGUID = assignmentAgreementSettle.AssignmentAgreementTableGUID,
					SettledDate = inputModel.NewSettledDate.StringToDate(),
					SettledAmount = assignmentAgreementSettle.SettledAmount * (-1),
					BuyerAgreementTableGUID = assignmentAgreementSettle.BuyerAgreementTableGUID,
					DocumentReasonGUID = inputModel.DocumentReasonGUID.StringToGuidNull(),
					RefGUID = assignmentAgreementSettle.RefGUID,
					CompanyGUID = assignmentAgreementSettle.CompanyGUID,
					DocumentId = assignmentAgreementSettle.DocumentId,
					InvoiceTableGUID = assignmentAgreementSettle.InvoiceTableGUID
				};

				AssignmentAgreementSettle newSettledAmount = new AssignmentAgreementSettle()
				{
					RefType = assignmentAgreementSettle.RefType,
					RefAssignmentAgreementSettleGUID = assignmentAgreementSettle.AssignmentAgreementSettleGUID,
					AssignmentAgreementTableGUID = assignmentAgreementSettle.AssignmentAgreementTableGUID,
					SettledDate = inputModel.NewSettledDate.StringToDate(),
					SettledAmount = inputModel.NewSettledAmount,
					BuyerAgreementTableGUID = assignmentAgreementSettle.BuyerAgreementTableGUID,
					DocumentReasonGUID = inputModel.DocumentReasonGUID.StringToGuidNull(),
					RefGUID = assignmentAgreementSettle.RefGUID,
					CompanyGUID = assignmentAgreementSettle.CompanyGUID,
					DocumentId = assignmentAgreementSettle.DocumentId,
					InvoiceTableGUID = assignmentAgreementSettle.InvoiceTableGUID
				};

				List<AssignmentAgreementSettle> createAssignmentSettlementList = new List<AssignmentAgreementSettle>();
				createAssignmentSettlementList.Add(revertAssignmentAgreementSettle);
				createAssignmentSettlementList.Add(newSettledAmount);

				return createAssignmentSettlementList;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public UpdateAssignmentAgreementSettleResultView UpdateAssignmentSettlement(UpdateAssignmentAgreementSettleView inputModel)
		{
			try
			{
				NotificationResponse success = new NotificationResponse();

				List<AssignmentAgreementSettle> createAssignmentSettlementList = InitUpdateAssignmentSettlement(inputModel);

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					base.BulkInsert(createAssignmentSettlementList, true);
					UnitOfWork.Commit(transaction);
				}

				UpdateAssignmentAgreementSettleResultView updateAssignmentAgreementSettleResultView = new UpdateAssignmentAgreementSettleResultView();
				success.AddData("SUCCESS.90025", new string[] { "LABEL.ASSIGNMENT_AGREEMENT_SETTLE" });
				updateAssignmentAgreementSettleResultView.RevertAssignmentAgreementSettle = createAssignmentSettlementList.First();
				updateAssignmentAgreementSettleResultView.NewSettledAmount = createAssignmentSettlementList.Last();
				updateAssignmentAgreementSettleResultView.Notification = success;

				return updateAssignmentAgreementSettleResultView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion

		#endregion
	}
}
