using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICustBankService
	{

		CustBankItemView GetCustBankById(string id);
		CustBankItemView CreateCustBank(CustBankItemView custBankView);
		CustBankItemView UpdateCustBank(CustBankItemView custBankView);
		bool DeleteCustBank(string id);
		void CreateCustBankCollection(List<CustBankItemView> custBankItemViews);
		CustBankItemView GetCustBankInitialData(string refGUID);
		#region DropdownByCondition
		IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBankItemByBankAccountControl(SearchParameter search);
		IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBankItemByPDC(SearchParameter search);
		IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBankItemByCustomer(SearchParameter search);
		#endregion DropdownByCondition
	}
	public class CustBankService : SmartAppService, ICustBankService
	{
		public CustBankService(SmartAppDbContext context) : base(context) { }
		public CustBankService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CustBankService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustBankService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CustBankService() : base() { }

		public CustBankItemView GetCustBankById(string id)
		{
			try
			{
				ICustBankRepo custBankRepo = new CustBankRepo(db);
				return custBankRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustBankItemView CreateCustBank(CustBankItemView custBankView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				custBankView = accessLevelService.AssignOwnerBU(custBankView);
				ICustBankRepo custBankRepo = new CustBankRepo(db);
				CustBank custBank = custBankView.ToCustBank();
				custBank = custBankRepo.CreateCustBank(custBank);
				base.LogTransactionCreate<CustBank>(custBank);
				UnitOfWork.Commit();
				return custBank.ToCustBankItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustBankItemView UpdateCustBank(CustBankItemView custBankView)
		{
			try
			{
				ICustBankRepo custBankRepo = new CustBankRepo(db);
				CustBank inputCustBank = custBankView.ToCustBank();
				CustBank dbCustBank = custBankRepo.Find(inputCustBank.CustBankGUID);
				dbCustBank = custBankRepo.UpdateCustBank(dbCustBank, inputCustBank);
				base.LogTransactionUpdate<CustBank>(GetOriginalValues<CustBank>(dbCustBank), dbCustBank);
				UnitOfWork.Commit();
				return dbCustBank.ToCustBankItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCustBank(string item)
		{
			try
			{
				ICustBankRepo custBankRepo = new CustBankRepo(db);
				Guid custBankGUID = new Guid(item);
				CustBank custBank = custBankRepo.Find(custBankGUID);
				custBankRepo.Remove(custBank);
				base.LogTransactionDelete<CustBank>(custBank);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropdownByCondition
		public IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBankItemByBankAccountControl(SearchParameter search)
		{
			try
			{
				ICustBankRepo custBankRepo = new CustBankRepo(db);
				search = search.GetParentCondition(CustBankCondition.CustomerTableGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetCustBankItemByBankAccountControlCondition(false, true);
				search.Conditions.AddRange(searchConditions);
				return custBankRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBankItemByPDC(SearchParameter search)
		{
			try
			{
				ICustBankRepo custBankRepo = new CustBankRepo(db);
				search = search.GetParentCondition(CustBankCondition.CustomerTableGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetCustBankItemByPDCCondition(false, true);
				search.Conditions.AddRange(searchConditions);
				return custBankRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBankItemByCustomer(SearchParameter search)
		{
			try
			{
				ICustBankRepo custBankRepo = new CustBankRepo(db);
				search = search.GetParentCondition(CustBankCondition.CustomerTableGUID);
				return custBankRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropdownByCondition
		#region migration
		public void CreateCustBankCollection(List<CustBankItemView> custBankItemViews)
		{
			try
			{
				if (custBankItemViews.Any())
				{
					ICustBankRepo custBankRepo = new CustBankRepo(db);
					List<CustBank> custBank = custBankItemViews.ToCustBank().ToList();
					custBankRepo.ValidateAdd(custBank);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(custBank, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
        #endregion
        public CustBankItemView GetCustBankInitialData(string refGUID)
		{
			try
			{
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTableItemView = customerTableRepo.GetByIdvw(refGUID.StringToGuid());
				return new CustBankItemView
				{
					CustBankGUID = new Guid().GuidNullToString(),
					CustomerTableGUID = refGUID,
					BankAccountControl = false,
					PDC = false,
					Primary = false,
					InActive = false,
					CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTableItemView.CustomerId, customerTableItemView.Name)
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
