using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBookmarkDocumentTemplateTableService
	{

		BookmarkDocumentTemplateLineItemView GetBookmarkDocumentTemplateLineById(string id);
		BookmarkDocumentTemplateLineItemView CreateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineView);
		BookmarkDocumentTemplateLineItemView UpdateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineView);
		bool DeleteBookmarkDocumentTemplateLine(string id);
		BookmarkDocumentTemplateTableItemView GetBookmarkDocumentTemplateTableById(string id);
		BookmarkDocumentTemplateTableItemView CreateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableView);
		BookmarkDocumentTemplateTableItemView UpdateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableView);
		bool DeleteBookmarkDocumentTemplateTable(string id);
		bool IsManualByDocumentConditionTemplateTable();
		BookmarkDocumentTemplateLineItemView GetBookmarkDocumentTemplateTableInitialData(string bookmarkDocumentTemplateTableGUID);
		BookmarkDocumentTemplateTableItemViewResult CopyBookmarkDocumentFromTemplate(BookmarkDocumentTemplateTableItemView paramView);
		bool GetCopyBookmarkDocumentValidation(BookmarkDocumentTemplateTableItemView paramView);
	}
	public class BookmarkDocumentTemplateTableService : SmartAppService, IBookmarkDocumentTemplateTableService
	{
		public BookmarkDocumentTemplateTableService(SmartAppDbContext context) : base(context) { }
		public BookmarkDocumentTemplateTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BookmarkDocumentTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BookmarkDocumentTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BookmarkDocumentTemplateTableService() : base() { }

		public BookmarkDocumentTemplateLineItemView GetBookmarkDocumentTemplateLineById(string id)
		{
			try
			{
				IBookmarkDocumentTemplateLineRepo bookmarkDocumentTemplateLineRepo = new BookmarkDocumentTemplateLineRepo(db);
				return bookmarkDocumentTemplateLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTemplateLineItemView CreateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				bookmarkDocumentTemplateLineView = accessLevelService.AssignOwnerBU(bookmarkDocumentTemplateLineView);
				IBookmarkDocumentTemplateLineRepo bookmarkDocumentTemplateLineRepo = new BookmarkDocumentTemplateLineRepo(db);
				BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineView.ToBookmarkDocumentTemplateLine();
				bookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineRepo.CreateBookmarkDocumentTemplateLine(bookmarkDocumentTemplateLine);
				base.LogTransactionCreate<BookmarkDocumentTemplateLine>(bookmarkDocumentTemplateLine);
				UnitOfWork.Commit();
				return bookmarkDocumentTemplateLine.ToBookmarkDocumentTemplateLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTemplateLineItemView UpdateBookmarkDocumentTemplateLine(BookmarkDocumentTemplateLineItemView bookmarkDocumentTemplateLineView)
		{
			try
			{
				IBookmarkDocumentTemplateLineRepo bookmarkDocumentTemplateLineRepo = new BookmarkDocumentTemplateLineRepo(db);
				BookmarkDocumentTemplateLine inputBookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineView.ToBookmarkDocumentTemplateLine();
				BookmarkDocumentTemplateLine dbBookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineRepo.Find(inputBookmarkDocumentTemplateLine.BookmarkDocumentTemplateLineGUID);
				dbBookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineRepo.UpdateBookmarkDocumentTemplateLine(dbBookmarkDocumentTemplateLine, inputBookmarkDocumentTemplateLine);
				base.LogTransactionUpdate<BookmarkDocumentTemplateLine>(GetOriginalValues<BookmarkDocumentTemplateLine>(dbBookmarkDocumentTemplateLine), dbBookmarkDocumentTemplateLine);
				UnitOfWork.Commit();
				return dbBookmarkDocumentTemplateLine.ToBookmarkDocumentTemplateLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBookmarkDocumentTemplateLine(string item)
		{
			try
			{
				IBookmarkDocumentTemplateLineRepo bookmarkDocumentTemplateLineRepo = new BookmarkDocumentTemplateLineRepo(db);
				Guid bookmarkDocumentTemplateLineGUID = new Guid(item);
				BookmarkDocumentTemplateLine bookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineRepo.Find(bookmarkDocumentTemplateLineGUID);
				bookmarkDocumentTemplateLineRepo.Remove(bookmarkDocumentTemplateLine);
				base.LogTransactionDelete<BookmarkDocumentTemplateLine>(bookmarkDocumentTemplateLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public BookmarkDocumentTemplateTableItemView GetBookmarkDocumentTemplateTableById(string id)
		{
			try
			{
				IBookmarkDocumentTemplateTableRepo bookmarkDocumentTemplateTableRepo = new BookmarkDocumentTemplateTableRepo(db);
				return bookmarkDocumentTemplateTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTemplateTableItemView CreateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				bookmarkDocumentTemplateTableView = accessLevelService.AssignOwnerBU(bookmarkDocumentTemplateTableView);
				IBookmarkDocumentTemplateTableRepo bookmarkDocumentTemplateTableRepo = new BookmarkDocumentTemplateTableRepo(db);
				BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable = bookmarkDocumentTemplateTableView.ToBookmarkDocumentTemplateTable();
				GenDocumentConditionTemplateTableNumberSeqCode(bookmarkDocumentTemplateTable);
				bookmarkDocumentTemplateTable = bookmarkDocumentTemplateTableRepo.CreateBookmarkDocumentTemplateTable(bookmarkDocumentTemplateTable);
				base.LogTransactionCreate<BookmarkDocumentTemplateTable>(bookmarkDocumentTemplateTable);
				UnitOfWork.Commit();
				return bookmarkDocumentTemplateTable.ToBookmarkDocumentTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentTemplateTableItemView UpdateBookmarkDocumentTemplateTable(BookmarkDocumentTemplateTableItemView bookmarkDocumentTemplateTableView)
		{
			try
			{
				IBookmarkDocumentTemplateTableRepo bookmarkDocumentTemplateTableRepo = new BookmarkDocumentTemplateTableRepo(db);
				BookmarkDocumentTemplateTable inputBookmarkDocumentTemplateTable = bookmarkDocumentTemplateTableView.ToBookmarkDocumentTemplateTable();
				BookmarkDocumentTemplateTable dbBookmarkDocumentTemplateTable = bookmarkDocumentTemplateTableRepo.Find(inputBookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID);
				dbBookmarkDocumentTemplateTable = bookmarkDocumentTemplateTableRepo.UpdateBookmarkDocumentTemplateTable(dbBookmarkDocumentTemplateTable, inputBookmarkDocumentTemplateTable);
				base.LogTransactionUpdate<BookmarkDocumentTemplateTable>(GetOriginalValues<BookmarkDocumentTemplateTable>(dbBookmarkDocumentTemplateTable), dbBookmarkDocumentTemplateTable);
				UnitOfWork.Commit();
				return dbBookmarkDocumentTemplateTable.ToBookmarkDocumentTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBookmarkDocumentTemplateTable(string item)
		{
			try
			{
				IBookmarkDocumentTemplateTableRepo bookmarkDocumentTemplateTableRepo = new BookmarkDocumentTemplateTableRepo(db);
				Guid bookmarkDocumentTemplateTableGUID = new Guid(item);
				BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable = bookmarkDocumentTemplateTableRepo.Find(bookmarkDocumentTemplateTableGUID);
				bookmarkDocumentTemplateTableRepo.Remove(bookmarkDocumentTemplateTable);
				base.LogTransactionDelete<BookmarkDocumentTemplateTable>(bookmarkDocumentTemplateTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsManualByDocumentConditionTemplateTable()
		{
			try
			{
				Guid companyId = GetCurrentCompany().StringToGuid();
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(companyId, ReferenceId.BookmarkDocumentTemplateTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenDocumentConditionTemplateTableNumberSeqCode(BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(bookmarkDocumentTemplateTable.CompanyGUID, ReferenceId.BookmarkDocumentTemplateTable);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(bookmarkDocumentTemplateTable.CompanyGUID, ReferenceId.BookmarkDocumentTemplateTable);
					bookmarkDocumentTemplateTable.BookmarkDocumentTemplateId = numberSequenceService.GetNumber(bookmarkDocumentTemplateTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public BookmarkDocumentTemplateLineItemView GetBookmarkDocumentTemplateTableInitialData(string bookmarkDocumentTemplateTableGUID)
		{
			try
			{
				IBookmarkDocumentTemplateTableRepo bookmarkDocumentTemplateTableRepo = new BookmarkDocumentTemplateTableRepo(db);
				BookmarkDocumentTemplateTable bookmarkDocumentTemplateTable = bookmarkDocumentTemplateTableRepo.GetBookmarkDocumentTemplateTableByIdNoTrackingByAccessLevel(bookmarkDocumentTemplateTableGUID.StringToGuid());
				return new BookmarkDocumentTemplateLineItemView
				{
					BookmarkDocumentTemplateTableGUID = bookmarkDocumentTemplateTable.BookmarkDocumentTemplateTableGUID.GuidNullToString(),
					DocumentTemplateTable_Values = SmartAppUtil.GetDropDownLabel(bookmarkDocumentTemplateTable.BookmarkDocumentTemplateId ,bookmarkDocumentTemplateTable.Description)

				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public BookmarkDocumentTemplateTableItemViewResult CopyBookmarkDocumentFromTemplate(BookmarkDocumentTemplateTableItemView paramView)
        {

			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IBookmarkDocumentTemplateLineRepo bookmarkDocumentTemplateLineRepo = new BookmarkDocumentTemplateLineRepo(db);
				BookmarkDocumentTemplateTableItemViewResult result = new BookmarkDocumentTemplateTableItemViewResult();
				NotificationResponse success = new NotificationResponse();
				DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				var draft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Draft).ToString());


				List<BookmarkDocumentTemplateLine> bookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineRepo.GetDocumentTemplateLineByBookmarkDocumentTemplateTable(paramView.BookmarkDocumentTemplateTableGUID.StringToGuid());
				if (bookmarkDocumentTemplateLine != null)
				{
					List<BookmarkDocumentTrans> createList = new List< BookmarkDocumentTrans>();
					bookmarkDocumentTemplateLine.ForEach(item =>
					{
						BookmarkDocumentTrans createItem = new BookmarkDocumentTrans()
						{
							BookmarkDocumentTransGUID = Guid.NewGuid(),
							DocumentStatusGUID = draft.DocumentStatusGUID,
							ReferenceExternalDate = null,
							ReferenceExternalId = null,
							Remark = null,
							BookmarkDocumentGUID = item.BookmarkDocumentGUID,
							DocumentTemplateTableGUID = item.DocumentTemplateTableGUID,
							RefType = Int32.Parse(paramView.RefType),
							RefGUID = paramView.RefGUID.StringToGuid(),
							CompanyGUID = db.GetCompanyFilter().StringToGuid()
						};
						createList.Add(createItem);
					});
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(createList);
						UnitOfWork.Commit(transaction);
					}
					success.AddData("SUCCESS.90002", new string[] { "LABEL.BOOKMARK_DOCUMENT_TEMPLATE" });
					result.Notification = success;
					return result;
				}
				else
				{
					return result;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public bool GetCopyBookmarkDocumentValidation(BookmarkDocumentTemplateTableItemView paramView)
        {
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IBookmarkDocumentTemplateLineRepo bookmarkDocumentTemplateLineRepo = new BookmarkDocumentTemplateLineRepo(db);
				List<BookmarkDocumentTemplateLine> bookmarkDocumentTemplateLine = bookmarkDocumentTemplateLineRepo.GetDocumentTemplateLineByBookmarkDocumentTemplateTable(paramView.BookmarkDocumentTemplateTableGUID.StringToGuid());
				IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
				IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType(Int32.Parse(paramView.RefType), paramView.RefGUID.StringToGuid());
				List<BookmarkDocumentTrans> accessTransList = new List<BookmarkDocumentTrans>();
                if (bookmarkDocumentTemplateLine == null|| bookmarkDocumentTrans==null)
                {
					return true;
                }

				bookmarkDocumentTemplateLine.ForEach(item =>
				{
					accessTransList.AddTo(bookmarkDocumentTrans.Where(wh => wh.BookmarkDocumentGUID == item.BookmarkDocumentGUID).ToList());
				});	
				if (accessTransList.Any())
				{
					ex.AddData("ERROR.90137");
				}
		

				if (bookmarkDocumentTemplateLine.Count()==0)
                {
					ex.AddData("ERROR.90003");

				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
    }
}
