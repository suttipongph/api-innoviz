using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBusinessSizeService
	{

		BusinessSizeItemView GetBusinessSizeById(string id);
		BusinessSizeItemView CreateBusinessSize(BusinessSizeItemView businessSizeView);
		BusinessSizeItemView UpdateBusinessSize(BusinessSizeItemView businessSizeView);
		bool DeleteBusinessSize(string id);
	}
	public class BusinessSizeService : SmartAppService, IBusinessSizeService
	{
		public BusinessSizeService(SmartAppDbContext context) : base(context) { }
		public BusinessSizeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BusinessSizeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BusinessSizeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BusinessSizeService() : base() { }

		public BusinessSizeItemView GetBusinessSizeById(string id)
		{
			try
			{
				IBusinessSizeRepo businessSizeRepo = new BusinessSizeRepo(db);
				return businessSizeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessSizeItemView CreateBusinessSize(BusinessSizeItemView businessSizeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				businessSizeView = accessLevelService.AssignOwnerBU(businessSizeView);
				IBusinessSizeRepo businessSizeRepo = new BusinessSizeRepo(db);
				BusinessSize businessSize = businessSizeView.ToBusinessSize();
				businessSize = businessSizeRepo.CreateBusinessSize(businessSize);
				base.LogTransactionCreate<BusinessSize>(businessSize);
				UnitOfWork.Commit();
				return businessSize.ToBusinessSizeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BusinessSizeItemView UpdateBusinessSize(BusinessSizeItemView businessSizeView)
		{
			try
			{
				IBusinessSizeRepo businessSizeRepo = new BusinessSizeRepo(db);
				BusinessSize inputBusinessSize = businessSizeView.ToBusinessSize();
				BusinessSize dbBusinessSize = businessSizeRepo.Find(inputBusinessSize.BusinessSizeGUID);
				dbBusinessSize = businessSizeRepo.UpdateBusinessSize(dbBusinessSize, inputBusinessSize);
				base.LogTransactionUpdate<BusinessSize>(GetOriginalValues<BusinessSize>(dbBusinessSize), dbBusinessSize);
				UnitOfWork.Commit();
				return dbBusinessSize.ToBusinessSizeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBusinessSize(string item)
		{
			try
			{
				IBusinessSizeRepo businessSizeRepo = new BusinessSizeRepo(db);
				Guid businessSizeGUID = new Guid(item);
				BusinessSize businessSize = businessSizeRepo.Find(businessSizeGUID);
				businessSizeRepo.Remove(businessSize);
				base.LogTransactionDelete<BusinessSize>(businessSize);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
