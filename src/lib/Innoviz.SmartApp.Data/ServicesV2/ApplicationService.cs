using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IApplicationService
	{

		//ApplicationAssetItemView GetApplicationAssetById(string id);
		//ApplicationAssetItemView CreateApplicationAsset(ApplicationAssetItemView applicationAssetView);
		//ApplicationAssetItemView UpdateApplicationAsset(ApplicationAssetItemView applicationAssetView);
		//bool DeleteApplicationAsset(string id);
		//ApplicationCalcCompulsoryItemView GetApplicationCalcCompulsoryById(string id);
		//ApplicationCalcCompulsoryItemView CreateApplicationCalcCompulsory(ApplicationCalcCompulsoryItemView applicationCalcCompulsoryView);
		//ApplicationCalcCompulsoryItemView UpdateApplicationCalcCompulsory(ApplicationCalcCompulsoryItemView applicationCalcCompulsoryView);
		//bool DeleteApplicationCalcCompulsory(string id);
		//ApplicationCalcInsuranceItemView GetApplicationCalcInsuranceById(string id);
		//ApplicationCalcInsuranceItemView CreateApplicationCalcInsurance(ApplicationCalcInsuranceItemView applicationCalcInsuranceView);
		//ApplicationCalcInsuranceItemView UpdateApplicationCalcInsurance(ApplicationCalcInsuranceItemView applicationCalcInsuranceView);
		//bool DeleteApplicationCalcInsurance(string id);
		//ApplicationCalcMaintenanceItemView GetApplicationCalcMaintenanceById(string id);
		//ApplicationCalcMaintenanceItemView CreateApplicationCalcMaintenance(ApplicationCalcMaintenanceItemView applicationCalcMaintenanceView);
		//ApplicationCalcMaintenanceItemView UpdateApplicationCalcMaintenance(ApplicationCalcMaintenanceItemView applicationCalcMaintenanceView);
		//bool DeleteApplicationCalcMaintenance(string id);
		//ApplicationCalcTableItemView GetApplicationCalcTableById(string id);
		//ApplicationCalcTableItemView CreateApplicationCalcTable(ApplicationCalcTableItemView applicationCalcTableView);
		//ApplicationCalcTableItemView UpdateApplicationCalcTable(ApplicationCalcTableItemView applicationCalcTableView);
		//bool DeleteApplicationCalcTable(string id);
		//ApplicationCalcVehicleTaxItemView GetApplicationCalcVehicleTaxById(string id);
		//ApplicationCalcVehicleTaxItemView CreateApplicationCalcVehicleTax(ApplicationCalcVehicleTaxItemView applicationCalcVehicleTaxView);
		//ApplicationCalcVehicleTaxItemView UpdateApplicationCalcVehicleTax(ApplicationCalcVehicleTaxItemView applicationCalcVehicleTaxView);
		//bool DeleteApplicationCalcVehicleTax(string id);
		//ApplicationHistoryItemView GetApplicationHistoryById(string id);
		//ApplicationHistoryItemView CreateApplicationHistory(ApplicationHistoryItemView applicationHistoryView);
		//ApplicationHistoryItemView UpdateApplicationHistory(ApplicationHistoryItemView applicationHistoryView);
		//bool DeleteApplicationHistory(string id);
		//ApplicationLineItemView GetApplicationLineById(string id);
		//ApplicationLineItemView CreateApplicationLine(ApplicationLineItemView applicationLineView);
		//ApplicationLineItemView UpdateApplicationLine(ApplicationLineItemView applicationLineView);
		//bool DeleteApplicationLine(string id);
		//ApplicationPaymScheduleItemView GetApplicationPaymScheduleById(string id);
		//ApplicationPaymScheduleItemView CreateApplicationPaymSchedule(ApplicationPaymScheduleItemView applicationPaymScheduleView);
		//ApplicationPaymScheduleItemView UpdateApplicationPaymSchedule(ApplicationPaymScheduleItemView applicationPaymScheduleView);
		//bool DeleteApplicationPaymSchedule(string id);
		ApplicationTableItemView GetApplicationTableById(string id);
		ApplicationTableItemView CreateApplicationTable(ApplicationTableItemView applicationTableView);
		ApplicationTableItemView UpdateApplicationTable(ApplicationTableItemView applicationTableView);
		bool DeleteApplicationTable(string id);

		IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItemNotCancelApplicationTableByCustomer(SearchParameter search);
	}
	public class ApplicationService : SmartAppService, IApplicationService
	{
		public ApplicationService(SmartAppDbContext context) : base(context) { }
		public ApplicationService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ApplicationService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ApplicationService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ApplicationService() : base() { }

		//public ApplicationAssetItemView GetApplicationAssetById(string id)
		//{
		//	try
		//	{
		//		IApplicationAssetRepo applicationAssetRepo = new ApplicationAssetRepo(db);
		//		return applicationAssetRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationAssetItemView CreateApplicationAsset(ApplicationAssetItemView applicationAssetView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationAssetView = accessLevelService.AssignOwnerBU(applicationAssetView);
		//		IApplicationAssetRepo applicationAssetRepo = new ApplicationAssetRepo(db);
		//		ApplicationAsset applicationAsset = applicationAssetView.ToApplicationAsset();
		//		applicationAsset = applicationAssetRepo.CreateApplicationAsset(applicationAsset);
		//		base.LogTransactionCreate<ApplicationAsset>(applicationAsset);
		//		UnitOfWork.Commit();
		//		return applicationAsset.ToApplicationAssetItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationAssetItemView UpdateApplicationAsset(ApplicationAssetItemView applicationAssetView)
		//{
		//	try
		//	{
		//		IApplicationAssetRepo applicationAssetRepo = new ApplicationAssetRepo(db);
		//		ApplicationAsset inputApplicationAsset = applicationAssetView.ToApplicationAsset();
		//		ApplicationAsset dbApplicationAsset = applicationAssetRepo.Find(inputApplicationAsset.ApplicationAssetGUID);
		//		dbApplicationAsset = applicationAssetRepo.UpdateApplicationAsset(dbApplicationAsset, inputApplicationAsset);
		//		base.LogTransactionUpdate<ApplicationAsset>(GetOriginalValues<ApplicationAsset>(dbApplicationAsset), dbApplicationAsset);
		//		UnitOfWork.Commit();
		//		return dbApplicationAsset.ToApplicationAssetItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationAsset(string item)
		//{
		//	try
		//	{
		//		IApplicationAssetRepo applicationAssetRepo = new ApplicationAssetRepo(db);
		//		Guid applicationAssetGUID = new Guid(item);
		//		ApplicationAsset applicationAsset = applicationAssetRepo.Find(applicationAssetGUID);
		//		applicationAssetRepo.Remove(applicationAsset);
		//		base.LogTransactionDelete<ApplicationAsset>(applicationAsset);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationCalcCompulsoryItemView GetApplicationCalcCompulsoryById(string id)
		//{
		//	try
		//	{
		//		IApplicationCalcCompulsoryRepo applicationCalcCompulsoryRepo = new ApplicationCalcCompulsoryRepo(db);
		//		return applicationCalcCompulsoryRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcCompulsoryItemView CreateApplicationCalcCompulsory(ApplicationCalcCompulsoryItemView applicationCalcCompulsoryView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationCalcCompulsoryView = accessLevelService.AssignOwnerBU(applicationCalcCompulsoryView);
		//		IApplicationCalcCompulsoryRepo applicationCalcCompulsoryRepo = new ApplicationCalcCompulsoryRepo(db);
		//		ApplicationCalcCompulsory applicationCalcCompulsory = applicationCalcCompulsoryView.ToApplicationCalcCompulsory();
		//		applicationCalcCompulsory = applicationCalcCompulsoryRepo.CreateApplicationCalcCompulsory(applicationCalcCompulsory);
		//		base.LogTransactionCreate<ApplicationCalcCompulsory>(applicationCalcCompulsory);
		//		UnitOfWork.Commit();
		//		return applicationCalcCompulsory.ToApplicationCalcCompulsoryItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcCompulsoryItemView UpdateApplicationCalcCompulsory(ApplicationCalcCompulsoryItemView applicationCalcCompulsoryView)
		//{
		//	try
		//	{
		//		IApplicationCalcCompulsoryRepo applicationCalcCompulsoryRepo = new ApplicationCalcCompulsoryRepo(db);
		//		ApplicationCalcCompulsory inputApplicationCalcCompulsory = applicationCalcCompulsoryView.ToApplicationCalcCompulsory();
		//		ApplicationCalcCompulsory dbApplicationCalcCompulsory = applicationCalcCompulsoryRepo.Find(inputApplicationCalcCompulsory.ApplicationCalcCompulsoryGUID);
		//		dbApplicationCalcCompulsory = applicationCalcCompulsoryRepo.UpdateApplicationCalcCompulsory(dbApplicationCalcCompulsory, inputApplicationCalcCompulsory);
		//		base.LogTransactionUpdate<ApplicationCalcCompulsory>(GetOriginalValues<ApplicationCalcCompulsory>(dbApplicationCalcCompulsory), dbApplicationCalcCompulsory);
		//		UnitOfWork.Commit();
		//		return dbApplicationCalcCompulsory.ToApplicationCalcCompulsoryItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationCalcCompulsory(string item)
		//{
		//	try
		//	{
		//		IApplicationCalcCompulsoryRepo applicationCalcCompulsoryRepo = new ApplicationCalcCompulsoryRepo(db);
		//		Guid applicationCalcCompulsoryGUID = new Guid(item);
		//		ApplicationCalcCompulsory applicationCalcCompulsory = applicationCalcCompulsoryRepo.Find(applicationCalcCompulsoryGUID);
		//		applicationCalcCompulsoryRepo.Remove(applicationCalcCompulsory);
		//		base.LogTransactionDelete<ApplicationCalcCompulsory>(applicationCalcCompulsory);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationCalcInsuranceItemView GetApplicationCalcInsuranceById(string id)
		//{
		//	try
		//	{
		//		IApplicationCalcInsuranceRepo applicationCalcInsuranceRepo = new ApplicationCalcInsuranceRepo(db);
		//		return applicationCalcInsuranceRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcInsuranceItemView CreateApplicationCalcInsurance(ApplicationCalcInsuranceItemView applicationCalcInsuranceView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationCalcInsuranceView = accessLevelService.AssignOwnerBU(applicationCalcInsuranceView);
		//		IApplicationCalcInsuranceRepo applicationCalcInsuranceRepo = new ApplicationCalcInsuranceRepo(db);
		//		ApplicationCalcInsurance applicationCalcInsurance = applicationCalcInsuranceView.ToApplicationCalcInsurance();
		//		applicationCalcInsurance = applicationCalcInsuranceRepo.CreateApplicationCalcInsurance(applicationCalcInsurance);
		//		base.LogTransactionCreate<ApplicationCalcInsurance>(applicationCalcInsurance);
		//		UnitOfWork.Commit();
		//		return applicationCalcInsurance.ToApplicationCalcInsuranceItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcInsuranceItemView UpdateApplicationCalcInsurance(ApplicationCalcInsuranceItemView applicationCalcInsuranceView)
		//{
		//	try
		//	{
		//		IApplicationCalcInsuranceRepo applicationCalcInsuranceRepo = new ApplicationCalcInsuranceRepo(db);
		//		ApplicationCalcInsurance inputApplicationCalcInsurance = applicationCalcInsuranceView.ToApplicationCalcInsurance();
		//		ApplicationCalcInsurance dbApplicationCalcInsurance = applicationCalcInsuranceRepo.Find(inputApplicationCalcInsurance.ApplicationCalcInsuranceGUID);
		//		dbApplicationCalcInsurance = applicationCalcInsuranceRepo.UpdateApplicationCalcInsurance(dbApplicationCalcInsurance, inputApplicationCalcInsurance);
		//		base.LogTransactionUpdate<ApplicationCalcInsurance>(GetOriginalValues<ApplicationCalcInsurance>(dbApplicationCalcInsurance), dbApplicationCalcInsurance);
		//		UnitOfWork.Commit();
		//		return dbApplicationCalcInsurance.ToApplicationCalcInsuranceItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationCalcInsurance(string item)
		//{
		//	try
		//	{
		//		IApplicationCalcInsuranceRepo applicationCalcInsuranceRepo = new ApplicationCalcInsuranceRepo(db);
		//		Guid applicationCalcInsuranceGUID = new Guid(item);
		//		ApplicationCalcInsurance applicationCalcInsurance = applicationCalcInsuranceRepo.Find(applicationCalcInsuranceGUID);
		//		applicationCalcInsuranceRepo.Remove(applicationCalcInsurance);
		//		base.LogTransactionDelete<ApplicationCalcInsurance>(applicationCalcInsurance);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationCalcMaintenanceItemView GetApplicationCalcMaintenanceById(string id)
		//{
		//	try
		//	{
		//		IApplicationCalcMaintenanceRepo applicationCalcMaintenanceRepo = new ApplicationCalcMaintenanceRepo(db);
		//		return applicationCalcMaintenanceRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcMaintenanceItemView CreateApplicationCalcMaintenance(ApplicationCalcMaintenanceItemView applicationCalcMaintenanceView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationCalcMaintenanceView = accessLevelService.AssignOwnerBU(applicationCalcMaintenanceView);
		//		IApplicationCalcMaintenanceRepo applicationCalcMaintenanceRepo = new ApplicationCalcMaintenanceRepo(db);
		//		ApplicationCalcMaintenance applicationCalcMaintenance = applicationCalcMaintenanceView.ToApplicationCalcMaintenance();
		//		applicationCalcMaintenance = applicationCalcMaintenanceRepo.CreateApplicationCalcMaintenance(applicationCalcMaintenance);
		//		base.LogTransactionCreate<ApplicationCalcMaintenance>(applicationCalcMaintenance);
		//		UnitOfWork.Commit();
		//		return applicationCalcMaintenance.ToApplicationCalcMaintenanceItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcMaintenanceItemView UpdateApplicationCalcMaintenance(ApplicationCalcMaintenanceItemView applicationCalcMaintenanceView)
		//{
		//	try
		//	{
		//		IApplicationCalcMaintenanceRepo applicationCalcMaintenanceRepo = new ApplicationCalcMaintenanceRepo(db);
		//		ApplicationCalcMaintenance inputApplicationCalcMaintenance = applicationCalcMaintenanceView.ToApplicationCalcMaintenance();
		//		ApplicationCalcMaintenance dbApplicationCalcMaintenance = applicationCalcMaintenanceRepo.Find(inputApplicationCalcMaintenance.ApplicationCalcMaintenanceGUID);
		//		dbApplicationCalcMaintenance = applicationCalcMaintenanceRepo.UpdateApplicationCalcMaintenance(dbApplicationCalcMaintenance, inputApplicationCalcMaintenance);
		//		base.LogTransactionUpdate<ApplicationCalcMaintenance>(GetOriginalValues<ApplicationCalcMaintenance>(dbApplicationCalcMaintenance), dbApplicationCalcMaintenance);
		//		UnitOfWork.Commit();
		//		return dbApplicationCalcMaintenance.ToApplicationCalcMaintenanceItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationCalcMaintenance(string item)
		//{
		//	try
		//	{
		//		IApplicationCalcMaintenanceRepo applicationCalcMaintenanceRepo = new ApplicationCalcMaintenanceRepo(db);
		//		Guid applicationCalcMaintenanceGUID = new Guid(item);
		//		ApplicationCalcMaintenance applicationCalcMaintenance = applicationCalcMaintenanceRepo.Find(applicationCalcMaintenanceGUID);
		//		applicationCalcMaintenanceRepo.Remove(applicationCalcMaintenance);
		//		base.LogTransactionDelete<ApplicationCalcMaintenance>(applicationCalcMaintenance);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationCalcTableItemView GetApplicationCalcTableById(string id)
		//{
		//	try
		//	{
		//		IApplicationCalcTableRepo applicationCalcTableRepo = new ApplicationCalcTableRepo(db);
		//		return applicationCalcTableRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcTableItemView CreateApplicationCalcTable(ApplicationCalcTableItemView applicationCalcTableView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationCalcTableView = accessLevelService.AssignOwnerBU(applicationCalcTableView);
		//		IApplicationCalcTableRepo applicationCalcTableRepo = new ApplicationCalcTableRepo(db);
		//		ApplicationCalcTable applicationCalcTable = applicationCalcTableView.ToApplicationCalcTable();
		//		applicationCalcTable = applicationCalcTableRepo.CreateApplicationCalcTable(applicationCalcTable);
		//		base.LogTransactionCreate<ApplicationCalcTable>(applicationCalcTable);
		//		UnitOfWork.Commit();
		//		return applicationCalcTable.ToApplicationCalcTableItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcTableItemView UpdateApplicationCalcTable(ApplicationCalcTableItemView applicationCalcTableView)
		//{
		//	try
		//	{
		//		IApplicationCalcTableRepo applicationCalcTableRepo = new ApplicationCalcTableRepo(db);
		//		ApplicationCalcTable inputApplicationCalcTable = applicationCalcTableView.ToApplicationCalcTable();
		//		ApplicationCalcTable dbApplicationCalcTable = applicationCalcTableRepo.Find(inputApplicationCalcTable.ApplicationCalcTableGUID);
		//		dbApplicationCalcTable = applicationCalcTableRepo.UpdateApplicationCalcTable(dbApplicationCalcTable, inputApplicationCalcTable);
		//		base.LogTransactionUpdate<ApplicationCalcTable>(GetOriginalValues<ApplicationCalcTable>(dbApplicationCalcTable), dbApplicationCalcTable);
		//		UnitOfWork.Commit();
		//		return dbApplicationCalcTable.ToApplicationCalcTableItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationCalcTable(string item)
		//{
		//	try
		//	{
		//		IApplicationCalcTableRepo applicationCalcTableRepo = new ApplicationCalcTableRepo(db);
		//		Guid applicationCalcTableGUID = new Guid(item);
		//		ApplicationCalcTable applicationCalcTable = applicationCalcTableRepo.Find(applicationCalcTableGUID);
		//		applicationCalcTableRepo.Remove(applicationCalcTable);
		//		base.LogTransactionDelete<ApplicationCalcTable>(applicationCalcTable);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationCalcVehicleTaxItemView GetApplicationCalcVehicleTaxById(string id)
		//{
		//	try
		//	{
		//		IApplicationCalcVehicleTaxRepo applicationCalcVehicleTaxRepo = new ApplicationCalcVehicleTaxRepo(db);
		//		return applicationCalcVehicleTaxRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcVehicleTaxItemView CreateApplicationCalcVehicleTax(ApplicationCalcVehicleTaxItemView applicationCalcVehicleTaxView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationCalcVehicleTaxView = accessLevelService.AssignOwnerBU(applicationCalcVehicleTaxView);
		//		IApplicationCalcVehicleTaxRepo applicationCalcVehicleTaxRepo = new ApplicationCalcVehicleTaxRepo(db);
		//		ApplicationCalcVehicleTax applicationCalcVehicleTax = applicationCalcVehicleTaxView.ToApplicationCalcVehicleTax();
		//		applicationCalcVehicleTax = applicationCalcVehicleTaxRepo.CreateApplicationCalcVehicleTax(applicationCalcVehicleTax);
		//		base.LogTransactionCreate<ApplicationCalcVehicleTax>(applicationCalcVehicleTax);
		//		UnitOfWork.Commit();
		//		return applicationCalcVehicleTax.ToApplicationCalcVehicleTaxItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationCalcVehicleTaxItemView UpdateApplicationCalcVehicleTax(ApplicationCalcVehicleTaxItemView applicationCalcVehicleTaxView)
		//{
		//	try
		//	{
		//		IApplicationCalcVehicleTaxRepo applicationCalcVehicleTaxRepo = new ApplicationCalcVehicleTaxRepo(db);
		//		ApplicationCalcVehicleTax inputApplicationCalcVehicleTax = applicationCalcVehicleTaxView.ToApplicationCalcVehicleTax();
		//		ApplicationCalcVehicleTax dbApplicationCalcVehicleTax = applicationCalcVehicleTaxRepo.Find(inputApplicationCalcVehicleTax.ApplicationCalcVehicleTaxGUID);
		//		dbApplicationCalcVehicleTax = applicationCalcVehicleTaxRepo.UpdateApplicationCalcVehicleTax(dbApplicationCalcVehicleTax, inputApplicationCalcVehicleTax);
		//		base.LogTransactionUpdate<ApplicationCalcVehicleTax>(GetOriginalValues<ApplicationCalcVehicleTax>(dbApplicationCalcVehicleTax), dbApplicationCalcVehicleTax);
		//		UnitOfWork.Commit();
		//		return dbApplicationCalcVehicleTax.ToApplicationCalcVehicleTaxItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationCalcVehicleTax(string item)
		//{
		//	try
		//	{
		//		IApplicationCalcVehicleTaxRepo applicationCalcVehicleTaxRepo = new ApplicationCalcVehicleTaxRepo(db);
		//		Guid applicationCalcVehicleTaxGUID = new Guid(item);
		//		ApplicationCalcVehicleTax applicationCalcVehicleTax = applicationCalcVehicleTaxRepo.Find(applicationCalcVehicleTaxGUID);
		//		applicationCalcVehicleTaxRepo.Remove(applicationCalcVehicleTax);
		//		base.LogTransactionDelete<ApplicationCalcVehicleTax>(applicationCalcVehicleTax);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationHistoryItemView GetApplicationHistoryById(string id)
		//{
		//	try
		//	{
		//		IApplicationHistoryRepo applicationHistoryRepo = new ApplicationHistoryRepo(db);
		//		return applicationHistoryRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationHistoryItemView CreateApplicationHistory(ApplicationHistoryItemView applicationHistoryView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationHistoryView = accessLevelService.AssignOwnerBU(applicationHistoryView);
		//		IApplicationHistoryRepo applicationHistoryRepo = new ApplicationHistoryRepo(db);
		//		ApplicationHistory applicationHistory = applicationHistoryView.ToApplicationHistory();
		//		applicationHistory = applicationHistoryRepo.CreateApplicationHistory(applicationHistory);
		//		base.LogTransactionCreate<ApplicationHistory>(applicationHistory);
		//		UnitOfWork.Commit();
		//		return applicationHistory.ToApplicationHistoryItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationHistoryItemView UpdateApplicationHistory(ApplicationHistoryItemView applicationHistoryView)
		//{
		//	try
		//	{
		//		IApplicationHistoryRepo applicationHistoryRepo = new ApplicationHistoryRepo(db);
		//		ApplicationHistory inputApplicationHistory = applicationHistoryView.ToApplicationHistory();
		//		ApplicationHistory dbApplicationHistory = applicationHistoryRepo.Find(inputApplicationHistory.ApplicationHistoryGUID);
		//		dbApplicationHistory = applicationHistoryRepo.UpdateApplicationHistory(dbApplicationHistory, inputApplicationHistory);
		//		base.LogTransactionUpdate<ApplicationHistory>(GetOriginalValues<ApplicationHistory>(dbApplicationHistory), dbApplicationHistory);
		//		UnitOfWork.Commit();
		//		return dbApplicationHistory.ToApplicationHistoryItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationHistory(string item)
		//{
		//	try
		//	{
		//		IApplicationHistoryRepo applicationHistoryRepo = new ApplicationHistoryRepo(db);
		//		Guid applicationHistoryGUID = new Guid(item);
		//		ApplicationHistory applicationHistory = applicationHistoryRepo.Find(applicationHistoryGUID);
		//		applicationHistoryRepo.Remove(applicationHistory);
		//		base.LogTransactionDelete<ApplicationHistory>(applicationHistory);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationLineItemView GetApplicationLineById(string id)
		//{
		//	try
		//	{
		//		IApplicationLineRepo applicationLineRepo = new ApplicationLineRepo(db);
		//		return applicationLineRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationLineItemView CreateApplicationLine(ApplicationLineItemView applicationLineView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationLineView = accessLevelService.AssignOwnerBU(applicationLineView);
		//		IApplicationLineRepo applicationLineRepo = new ApplicationLineRepo(db);
		//		ApplicationLine applicationLine = applicationLineView.ToApplicationLine();
		//		applicationLine = applicationLineRepo.CreateApplicationLine(applicationLine);
		//		base.LogTransactionCreate<ApplicationLine>(applicationLine);
		//		UnitOfWork.Commit();
		//		return applicationLine.ToApplicationLineItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationLineItemView UpdateApplicationLine(ApplicationLineItemView applicationLineView)
		//{
		//	try
		//	{
		//		IApplicationLineRepo applicationLineRepo = new ApplicationLineRepo(db);
		//		ApplicationLine inputApplicationLine = applicationLineView.ToApplicationLine();
		//		ApplicationLine dbApplicationLine = applicationLineRepo.Find(inputApplicationLine.ApplicationLineGUID);
		//		dbApplicationLine = applicationLineRepo.UpdateApplicationLine(dbApplicationLine, inputApplicationLine);
		//		base.LogTransactionUpdate<ApplicationLine>(GetOriginalValues<ApplicationLine>(dbApplicationLine), dbApplicationLine);
		//		UnitOfWork.Commit();
		//		return dbApplicationLine.ToApplicationLineItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationLine(string item)
		//{
		//	try
		//	{
		//		IApplicationLineRepo applicationLineRepo = new ApplicationLineRepo(db);
		//		Guid applicationLineGUID = new Guid(item);
		//		ApplicationLine applicationLine = applicationLineRepo.Find(applicationLineGUID);
		//		applicationLineRepo.Remove(applicationLine);
		//		base.LogTransactionDelete<ApplicationLine>(applicationLine);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		//public ApplicationPaymScheduleItemView GetApplicationPaymScheduleById(string id)
		//{
		//	try
		//	{
		//		IApplicationPaymScheduleRepo applicationPaymScheduleRepo = new ApplicationPaymScheduleRepo(db);
		//		return applicationPaymScheduleRepo.GetByIdvw(id.StringToGuid());
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationPaymScheduleItemView CreateApplicationPaymSchedule(ApplicationPaymScheduleItemView applicationPaymScheduleView)
		//{
		//	try
		//	{
		//		ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
		//		applicationPaymScheduleView = accessLevelService.AssignOwnerBU(applicationPaymScheduleView);
		//		IApplicationPaymScheduleRepo applicationPaymScheduleRepo = new ApplicationPaymScheduleRepo(db);
		//		ApplicationPaymSchedule applicationPaymSchedule = applicationPaymScheduleView.ToApplicationPaymSchedule();
		//		applicationPaymSchedule = applicationPaymScheduleRepo.CreateApplicationPaymSchedule(applicationPaymSchedule);
		//		base.LogTransactionCreate<ApplicationPaymSchedule>(applicationPaymSchedule);
		//		UnitOfWork.Commit();
		//		return applicationPaymSchedule.ToApplicationPaymScheduleItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public ApplicationPaymScheduleItemView UpdateApplicationPaymSchedule(ApplicationPaymScheduleItemView applicationPaymScheduleView)
		//{
		//	try
		//	{
		//		IApplicationPaymScheduleRepo applicationPaymScheduleRepo = new ApplicationPaymScheduleRepo(db);
		//		ApplicationPaymSchedule inputApplicationPaymSchedule = applicationPaymScheduleView.ToApplicationPaymSchedule();
		//		ApplicationPaymSchedule dbApplicationPaymSchedule = applicationPaymScheduleRepo.Find(inputApplicationPaymSchedule.ApplicationPaymScheduleGUID);
		//		dbApplicationPaymSchedule = applicationPaymScheduleRepo.UpdateApplicationPaymSchedule(dbApplicationPaymSchedule, inputApplicationPaymSchedule);
		//		base.LogTransactionUpdate<ApplicationPaymSchedule>(GetOriginalValues<ApplicationPaymSchedule>(dbApplicationPaymSchedule), dbApplicationPaymSchedule);
		//		UnitOfWork.Commit();
		//		return dbApplicationPaymSchedule.ToApplicationPaymScheduleItemView();
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}
		//public bool DeleteApplicationPaymSchedule(string item)
		//{
		//	try
		//	{
		//		IApplicationPaymScheduleRepo applicationPaymScheduleRepo = new ApplicationPaymScheduleRepo(db);
		//		Guid applicationPaymScheduleGUID = new Guid(item);
		//		ApplicationPaymSchedule applicationPaymSchedule = applicationPaymScheduleRepo.Find(applicationPaymScheduleGUID);
		//		applicationPaymScheduleRepo.Remove(applicationPaymSchedule);
		//		base.LogTransactionDelete<ApplicationPaymSchedule>(applicationPaymSchedule);
		//		UnitOfWork.Commit();
		//		return true;
		//	}
		//	catch (Exception e)
		//	{
		//		throw SmartAppUtil.AddStackTrace(e);
		//	}
		//}

		public ApplicationTableItemView GetApplicationTableById(string id)
		{
			try
			{
				IApplicationTableRepo applicationTableRepo = new ApplicationTableRepo(db);
				return applicationTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ApplicationTableItemView CreateApplicationTable(ApplicationTableItemView applicationTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				applicationTableView = accessLevelService.AssignOwnerBU(applicationTableView);
				IApplicationTableRepo applicationTableRepo = new ApplicationTableRepo(db);
				ApplicationTable applicationTable = applicationTableView.ToApplicationTable();
				applicationTable = applicationTableRepo.CreateApplicationTable(applicationTable);
				base.LogTransactionCreate<ApplicationTable>(applicationTable);
				UnitOfWork.Commit();
				return applicationTable.ToApplicationTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ApplicationTableItemView UpdateApplicationTable(ApplicationTableItemView applicationTableView)
		{
			try
			{
				IApplicationTableRepo applicationTableRepo = new ApplicationTableRepo(db);
				ApplicationTable inputApplicationTable = applicationTableView.ToApplicationTable();
				ApplicationTable dbApplicationTable = applicationTableRepo.Find(inputApplicationTable.ApplicationTableGUID);
				dbApplicationTable = applicationTableRepo.UpdateApplicationTable(dbApplicationTable, inputApplicationTable);
				base.LogTransactionUpdate<ApplicationTable>(GetOriginalValues<ApplicationTable>(dbApplicationTable), dbApplicationTable);
				UnitOfWork.Commit();
				return dbApplicationTable.ToApplicationTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteApplicationTable(string item)
		{
			try
			{
				IApplicationTableRepo applicationTableRepo = new ApplicationTableRepo(db);
				Guid applicationTableGUID = new Guid(item);
				ApplicationTable applicationTable = applicationTableRepo.Find(applicationTableGUID);
				applicationTableRepo.Remove(applicationTable);
				base.LogTransactionDelete<ApplicationTable>(applicationTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItemNotCancelApplicationTableByCustomer(SearchParameter search)
        {
            try
            {
				IApplicationTableRepo applicationTableRepo = new ApplicationTableRepo(db);
				IDocumentService documentService = new DocumentService(db);
				search = search.GetParentCondition(ApplicationTableCondition.CustomertableGUID);
				string Cancelled = ((int)ApplicationStatus.Cancelled).ToString();
				List<SearchCondition> searchConditions = SearchConditionService.GetApplicationNotStatusCondition(new string[] { Cancelled });
                search.Conditions.AddRange(searchConditions);
                return applicationTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
