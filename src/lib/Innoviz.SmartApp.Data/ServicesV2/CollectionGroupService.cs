using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICollectionGroupService
	{

		CollectionGroupItemView GetCollectionGroupById(string id);
		CollectionGroupItemView CreateCollectionGroup(CollectionGroupItemView collectionGroupView);
		CollectionGroupItemView UpdateCollectionGroup(CollectionGroupItemView collectionGroupView);
		bool DeleteCollectionGroup(string id);
	}
	public class CollectionGroupService : SmartAppService, ICollectionGroupService
	{
		public CollectionGroupService(SmartAppDbContext context) : base(context) { }
		public CollectionGroupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CollectionGroupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CollectionGroupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CollectionGroupService() : base() { }

		public CollectionGroupItemView GetCollectionGroupById(string id)
		{
			try
			{
				ICollectionGroupRepo collectionGroupRepo = new CollectionGroupRepo(db);
				return collectionGroupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionGroupItemView CreateCollectionGroup(CollectionGroupItemView collectionGroupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				collectionGroupView = accessLevelService.AssignOwnerBU(collectionGroupView);
				ICollectionGroupRepo collectionGroupRepo = new CollectionGroupRepo(db);
				CollectionGroup collectionGroup = collectionGroupView.ToCollectionGroup();
				collectionGroup = collectionGroupRepo.CreateCollectionGroup(collectionGroup);
				base.LogTransactionCreate<CollectionGroup>(collectionGroup);
				UnitOfWork.Commit();
				return collectionGroup.ToCollectionGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionGroupItemView UpdateCollectionGroup(CollectionGroupItemView collectionGroupView)
		{
			try
			{
				ICollectionGroupRepo collectionGroupRepo = new CollectionGroupRepo(db);
				CollectionGroup inputCollectionGroup = collectionGroupView.ToCollectionGroup();
				CollectionGroup dbCollectionGroup = collectionGroupRepo.Find(inputCollectionGroup.CollectionGroupGUID);
				dbCollectionGroup = collectionGroupRepo.UpdateCollectionGroup(dbCollectionGroup, inputCollectionGroup);
				base.LogTransactionUpdate<CollectionGroup>(GetOriginalValues<CollectionGroup>(dbCollectionGroup), dbCollectionGroup);
				UnitOfWork.Commit();
				return dbCollectionGroup.ToCollectionGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCollectionGroup(string item)
		{
			try
			{
				ICollectionGroupRepo collectionGroupRepo = new CollectionGroupRepo(db);
				Guid collectionGroupGUID = new Guid(item);
				CollectionGroup collectionGroup = collectionGroupRepo.Find(collectionGroupGUID);
				collectionGroupRepo.Remove(collectionGroup);
				base.LogTransactionDelete<CollectionGroup>(collectionGroup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
