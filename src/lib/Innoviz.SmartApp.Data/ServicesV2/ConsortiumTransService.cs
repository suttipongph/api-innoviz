using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IConsortiumTransService
	{

		ConsortiumTransItemView GetConsortiumTransById(string id);
		ConsortiumTransItemView CreateConsortiumTrans(ConsortiumTransItemView consortiumTransView);
		ConsortiumTransItemView UpdateConsortiumTrans(ConsortiumTransItemView consortiumTransView);
		bool DeleteConsortiumTrans(string id);
		IEnumerable<ConsortiumTrans> CopyConsortiumTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType);
		IEnumerable<ConsortiumTrans> CopyConsortiumTrans(Guid consortiumTableGUID, Guid toRefGUID, int toRefType);
		ConsortiumTransItemView GetConsortiumTransInitialData(string refId, string refGUID, RefType refType);
		ConsortiumTransItemView GetConsortiumTransInitialDataByMainAgreementTable(string refId, string refGUID, RefType refType);
		ConsortiumTransItemView GetConsortiumTransInitialDataByBusinessCollateralAgmTable(string refId, string refGUID, string CreditAppTable, RefType refType);
		MainAgreementConsortiumBookmarkView GetMainAgreementConsortiumBookmarkValue(Guid refGuid);
		void CreateConsortiumTransCollection(IEnumerable<ConsortiumTransItemView> consortiumTransItemViews);
	}
	public class ConsortiumTransService : SmartAppService, IConsortiumTransService
	{
		public ConsortiumTransService(SmartAppDbContext context) : base(context) { }
		public ConsortiumTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ConsortiumTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ConsortiumTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ConsortiumTransService() : base() { }

		public ConsortiumTransItemView GetConsortiumTransById(string id)
		{
			try
			{
				IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
				return consortiumTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTransItemView CreateConsortiumTrans(ConsortiumTransItemView consortiumTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				consortiumTransView = accessLevelService.AssignOwnerBU(consortiumTransView);
				IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
				ConsortiumTrans consortiumTrans = consortiumTransView.ToConsortiumTrans();
				consortiumTrans = consortiumTransRepo.CreateConsortiumTrans(consortiumTrans);
				base.LogTransactionCreate<ConsortiumTrans>(consortiumTrans);
				UnitOfWork.Commit();
				return consortiumTrans.ToConsortiumTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTransItemView UpdateConsortiumTrans(ConsortiumTransItemView consortiumTransView)
		{
			try
			{
				IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
				ConsortiumTrans inputConsortiumTrans = consortiumTransView.ToConsortiumTrans();
				ConsortiumTrans dbConsortiumTrans = consortiumTransRepo.Find(inputConsortiumTrans.ConsortiumTransGUID);
				dbConsortiumTrans = consortiumTransRepo.UpdateConsortiumTrans(dbConsortiumTrans, inputConsortiumTrans);
				base.LogTransactionUpdate<ConsortiumTrans>(GetOriginalValues<ConsortiumTrans>(dbConsortiumTrans), dbConsortiumTrans);
				UnitOfWork.Commit();
				return dbConsortiumTrans.ToConsortiumTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteConsortiumTrans(string item)
		{
			try
			{
				IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
				Guid consortiumTransGUID = new Guid(item);
				ConsortiumTrans consortiumTrans = consortiumTransRepo.Find(consortiumTransGUID);
				consortiumTransRepo.Remove(consortiumTrans);
				base.LogTransactionDelete<ConsortiumTrans>(consortiumTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<ConsortiumTrans> CopyConsortiumTrans(Guid consortiumTableGUID, Guid toRefGUID, int toRefType)
		{
			try
			{
				IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
				IEnumerable<ConsortiumLine> originConsortiumLineList = consortiumLineRepo.GetConsortiumLineByConsortiumTableNoTracking(consortiumTableGUID);
				List<ConsortiumTrans> copyConsortiumTransList = new List<ConsortiumTrans>();
				if (originConsortiumLineList.Count() != 0)
				{
					foreach (ConsortiumLine item in originConsortiumLineList)
					{
						copyConsortiumTransList.Add(new ConsortiumTrans
						{
							ConsortiumTransGUID = Guid.NewGuid(),
							CompanyGUID = item.CompanyGUID,
							RefType = toRefType,
							RefGUID = toRefGUID,
							Ordering = item.Ordering,
							CustomerName = item.CustomerName,
							AuthorizedPersonTypeGUID = item.AuthorizedPersonTypeGUID,
							OperatedBy = item.OperatedBy,
							Address = item.Address,
							Remark = item.Remark
						});
					}

				}
				return copyConsortiumTransList;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<ConsortiumTrans> CopyConsortiumTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType)
		{
			try
			{
				IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
				IEnumerable<ConsortiumTrans> originConsortiumTrans = consortiumTransRepo.GeConsortiumTransByReferenceTableNoTracking(fromRefGUID, fromRefType);
				List<ConsortiumTrans> copyConsortiumTransList = new List<ConsortiumTrans>();
				if (originConsortiumTrans.Count() != 0)
				{
					foreach (ConsortiumTrans item in originConsortiumTrans)
					{
						copyConsortiumTransList.Add(new ConsortiumTrans
						{
							ConsortiumTransGUID = Guid.NewGuid(),
							RefType = toRefType,
							RefGUID = toRefGUID,
							Ordering = item.Ordering,
							CustomerName = item.CustomerName,
							AuthorizedPersonTypeGUID = item.AuthorizedPersonTypeGUID,
							OperatedBy = item.OperatedBy,
							Address = item.Address,
							Remark = item.Remark,
							CompanyGUID = item.CompanyGUID
						});
					}
				}

				return copyConsortiumTransList;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ConsortiumTransItemView GetConsortiumTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new ConsortiumTransItemView
				{
					ConsortiumTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ConsortiumTransItemView GetConsortiumTransInitialDataByMainAgreementTable(string refId, string refGUID, RefType refType)
		{
			try
			{
				IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
				MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
				return new ConsortiumTransItemView
				{
					ConsortiumTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					MainAgreementTable_ConsortiumTableGUID = mainAgreementTable.ConsortiumTableGUID.GuidNullToString()
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTransItemView GetConsortiumTransInitialDataByBusinessCollateralAgmTable(string refId, string refGUID,string CreditAppTable, RefType refType)
		{
			try
			{
				IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(CreditAppTable.StringToGuid());

				return new ConsortiumTransItemView
				{
					ConsortiumTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					MainAgreementTable_ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID.GuidNullToString()
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region MainAgreementConsortiumBookmark
		public MainAgreementConsortiumBookmarkView GetMainAgreementConsortiumBookmarkValue(Guid refGuid)
		{
			try
			{
				IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
				MainAgreementConsortiumBookmarkView agreementConsortiumBookmarkView = consortiumTransRepo.GetMainAgreementConsortiumBookmarkValue(refGuid);

				return agreementConsortiumBookmarkView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Migration
		public void CreateConsortiumTransCollection(IEnumerable<ConsortiumTransItemView> consortiumTransItemViews)
		{
			try
			{
				if (consortiumTransItemViews.Any())
				{
					IConsortiumTransRepo consortiumTransRepo = new ConsortiumTransRepo(db);
					List<ConsortiumTrans> consortiumTranss = consortiumTransItemViews.ToConsortiumTrans().ToList();
					consortiumTranss.ForEach(f => f.ConsortiumTransGUID = Guid.NewGuid());
					consortiumTransRepo.ValidateAdd(consortiumTranss);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(consortiumTranss);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Migration
	}
}
