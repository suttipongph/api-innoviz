using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ICreditAppReqBusCollateralInfoService
    {

        CreditAppReqBusinessCollateralItemView GetCreditAppReqBusinessCollateralById(string id);
        CreditAppReqBusinessCollateralItemView CreateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateralView);
        CreditAppReqBusinessCollateralItemView UpdateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateralItemView creditAppReqBusinessCollateralView);
        bool DeleteCreditAppReqBusinessCollateral(string id);
        CustBusinessCollateralItemView GetCustBusinessCollateralById(string id);
        CustBusinessCollateralItemView CreateCustBusinessCollateral(CustBusinessCollateralItemView custBusinessCollateralView);
        CustBusinessCollateralItemView UpdateCustBusinessCollateral(CustBusinessCollateralItemView custBusinessCollateralView);
        bool DeleteCustBusinessCollateral(string id);
        IEnumerable<SelectItem<CustBusinessCollateralItemView>> GetDropDownItemNotCancelCustBusinessCollateralByCustomer(SearchParameter search);
        void CreateCreditAppReqBusinessCollateralCollection(List<CreditAppReqBusinessCollateralItemView> creditAppReqBusinessCollateralItemViews);
        void CreateCustBusinessCollateralCollection(List<CustBusinessCollateralItemView> custBusinessCollateralItemViews);
        #region shared
        List<CustBusinessCollateral> GenCustBusinessCollateral(Guid businessCollateralAgmTableGUID);
        #endregion

    }
    public class CreditAppReqBusCollateralInfoService : SmartAppService, ICreditAppReqBusCollateralInfoService
    {
        public CreditAppReqBusCollateralInfoService(SmartAppDbContext context) : base(context) { }
        public CreditAppReqBusCollateralInfoService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public CreditAppReqBusCollateralInfoService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CreditAppReqBusCollateralInfoService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public CreditAppReqBusCollateralInfoService() : base() { }
        #region CreditAppReqBusinessCollateral

        public CreditAppReqBusinessCollateralItemView GetCreditAppReqBusinessCollateralById(string id)
        {
            try
            {
                ICreditAppReqBusinessCollateralRepo CreditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                return CreditAppReqBusinessCollateralRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppReqBusinessCollateralItemView CreateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateralItemView CreditAppReqBusinessCollateralView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                CreditAppReqBusinessCollateralView = accessLevelService.AssignOwnerBU(CreditAppReqBusinessCollateralView);
                ICreditAppReqBusinessCollateralRepo CreditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                CreditAppReqBusinessCollateral CreditAppReqBusinessCollateral = CreditAppReqBusinessCollateralView.ToCreditAppReqBusinessCollateral();
                CreditAppReqBusinessCollateral = CreditAppReqBusinessCollateralRepo.CreateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateral);
                base.LogTransactionCreate<CreditAppReqBusinessCollateral>(CreditAppReqBusinessCollateral);
                UnitOfWork.Commit();
                return CreditAppReqBusinessCollateral.ToCreditAppReqBusinessCollateralItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppReqBusinessCollateralItemView UpdateCreditAppReqBusinessCollateral(CreditAppReqBusinessCollateralItemView CreditAppReqBusinessCollateralView)
        {
            try
            {
                ICreditAppReqBusinessCollateralRepo CreditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                CreditAppReqBusinessCollateral inputCreditAppReqBusinessCollateral = CreditAppReqBusinessCollateralView.ToCreditAppReqBusinessCollateral();
                CreditAppReqBusinessCollateral dbCreditAppReqBusinessCollateral = CreditAppReqBusinessCollateralRepo.Find(inputCreditAppReqBusinessCollateral.CreditAppReqBusinessCollateralGUID);
                dbCreditAppReqBusinessCollateral = CreditAppReqBusinessCollateralRepo.UpdateCreditAppReqBusinessCollateral(dbCreditAppReqBusinessCollateral, inputCreditAppReqBusinessCollateral);
                base.LogTransactionUpdate<CreditAppReqBusinessCollateral>(GetOriginalValues<CreditAppReqBusinessCollateral>(dbCreditAppReqBusinessCollateral), dbCreditAppReqBusinessCollateral);
                UnitOfWork.Commit();
                return dbCreditAppReqBusinessCollateral.ToCreditAppReqBusinessCollateralItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCreditAppReqBusinessCollateral(string item)
        {
            try
            {
                ICreditAppReqBusinessCollateralRepo CreditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                Guid CreditAppReqBusinessCollateralGUID = new Guid(item);
                CreditAppReqBusinessCollateral CreditAppReqBusinessCollateral = CreditAppReqBusinessCollateralRepo.Find(CreditAppReqBusinessCollateralGUID);
                CreditAppReqBusinessCollateralRepo.Remove(CreditAppReqBusinessCollateral);
                base.LogTransactionDelete<CreditAppReqBusinessCollateral>(CreditAppReqBusinessCollateral);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        #endregion
        public CustBusinessCollateralItemView GetCustBusinessCollateralById(string id)
		{
			try
			{
				ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
				return custBusinessCollateralRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustBusinessCollateralItemView CreateCustBusinessCollateral(CustBusinessCollateralItemView custBusinessCollateralView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				custBusinessCollateralView = accessLevelService.AssignOwnerBU(custBusinessCollateralView);
				ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
				CustBusinessCollateral custBusinessCollateral = custBusinessCollateralView.ToCustBusinessCollateral();
				custBusinessCollateral = custBusinessCollateralRepo.CreateCustBusinessCollateral(custBusinessCollateral);
				base.LogTransactionCreate<CustBusinessCollateral>(custBusinessCollateral);
				UnitOfWork.Commit();
				return custBusinessCollateral.ToCustBusinessCollateralItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustBusinessCollateralItemView UpdateCustBusinessCollateral(CustBusinessCollateralItemView custBusinessCollateralView)
		{
			try
			{
				ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
				CustBusinessCollateral inputCustBusinessCollateral = custBusinessCollateralView.ToCustBusinessCollateral();
				CustBusinessCollateral dbCustBusinessCollateral = custBusinessCollateralRepo.Find(inputCustBusinessCollateral.CustBusinessCollateralGUID);
				dbCustBusinessCollateral = custBusinessCollateralRepo.UpdateCustBusinessCollateral(dbCustBusinessCollateral, inputCustBusinessCollateral);
				base.LogTransactionUpdate<CustBusinessCollateral>(GetOriginalValues<CustBusinessCollateral>(dbCustBusinessCollateral), dbCustBusinessCollateral);
				UnitOfWork.Commit();
				return dbCustBusinessCollateral.ToCustBusinessCollateralItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCustBusinessCollateral(string item)
		{
			try
			{
				ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
				Guid custBusinessCollateralGUID = new Guid(item);
				CustBusinessCollateral custBusinessCollateral = custBusinessCollateralRepo.Find(custBusinessCollateralGUID);
				custBusinessCollateralRepo.Remove(custBusinessCollateral);
				base.LogTransactionDelete<CustBusinessCollateral>(custBusinessCollateral);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
        }
        public void CreateCreditAppReqBusinessCollateralCollection(List<CreditAppReqBusinessCollateralItemView> creditAppReqBusinessCollateralItemViews)
        {
            try
            {
                if (creditAppReqBusinessCollateralItemViews.Any())
                {
                    ICreditAppReqBusinessCollateralRepo creditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                    List<CreditAppReqBusinessCollateral> creditAppReqBusinessCollaterals = creditAppReqBusinessCollateralItemViews.ToCreditAppReqBusinessCollateral().ToList();
                    creditAppReqBusinessCollateralRepo.ValidateAdd(creditAppReqBusinessCollaterals);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(creditAppReqBusinessCollaterals, false);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void CreateCustBusinessCollateralCollection(List<CustBusinessCollateralItemView> custBusinessCollateralItemViews)
        {
            try
            {
                if (custBusinessCollateralItemViews.Any())
                {
                    ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
                    List<CustBusinessCollateral> custBusinessCollaterals = custBusinessCollateralItemViews.ToCustBusinessCollateral().ToList();
                    custBusinessCollateralRepo.ValidateAdd(custBusinessCollaterals);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(custBusinessCollaterals, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #region Dropdown
        public IEnumerable<SelectItem<CustBusinessCollateralItemView>> GetDropDownItemNotCancelCustBusinessCollateralByCustomer(SearchParameter search)
        {
            try
            {
                ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
                IDocumentService documentService = new DocumentService(db);
                search = search.GetParentCondition(CustBusinessCollateralCondition.CustomertableGUID);
                string Cancelled = ((int)ApplicationStatus.Cancelled).ToString();
                SearchCondition searchCondition = SearchConditionService.GetCancelledCustBusinessCollateralCondition(false);
                search.Conditions.Add(searchCondition);
                return custBusinessCollateralRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region shared
        public List<CustBusinessCollateral> GenCustBusinessCollateral(Guid businessCollateralAgmTableGUID)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                List<CustBusinessCollateral> custBusinessCollaterals = new List<CustBusinessCollateral>();
                ICreditAppReqBusinessCollateralRepo creditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(businessCollateralAgmTableGUID);
                List<BusinessCollateralAgmLine> businessCollateralAgmLines =  businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(businessCollateralAgmTableGUID).ToList();
                NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.CustomerBusinessCollateral);
                businessCollateralAgmLines.ForEach((item) => {
                    CustBusinessCollateral custBusinessCollateral = new CustBusinessCollateral
                    {
                        CustBusinessCollateralGUID = Guid.NewGuid(),
                        CustBusinessCollateralId = numberSequenceService.GetNumber(businessCollateralAgmTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault()),
                        BusinessCollateralStatusGUID = item.BusinessCollateralStatusGUID,
                        Cancelled = false,
                        CustomerTableGUID = businessCollateralAgmTable.CustomerTableGUID,
                        DBDRegistrationId = businessCollateralAgmTable.DBDRegistrationId,
                        DBDRegistrationDate = businessCollateralAgmTable.DBDRegistrationDate,
                        DBDRegistrationAmount = businessCollateralAgmTable.DBDRegistrationAmount,
                        DBDRegistrationDescription = businessCollateralAgmTable.DBDRegistrationDescription,
                        Remark = businessCollateralAgmTable.AttachmentRemark,
                        CreditAppRequestTableGUID = creditAppReqBusinessCollateralRepo.GetCreditAppReqBusinessCollateralByIdNoTracking(item.CreditAppReqBusinessCollateralGUID).CreditAppRequestTableGUID,
                        BusinessCollateralAgmLineGUID = item.BusinessCollateralAgmLineGUID,
                        BusinessCollateralTypeGUID = item.BusinessCollateralTypeGUID,
                        BusinessCollateralSubTypeGUID = item.BusinessCollateralSubTypeGUID,
                        Description = item.Description,
                        BusinessCollateralValue = item.BusinessCollateralValue,
                        BuyerTableGUID = item.BuyerTableGUID,
                        BuyerName = item.BuyerName,
                        BuyerTaxIdentificationId = item.BuyerTaxIdentificationId,
                        BankGroupGUID = item.BankGroupGUID,
                        BankTypeGUID = item.BankTypeGUID,
                        AccountNumber = item.AccountNumber,
                        RefAgreementId = item.RefAgreementId,
                        RefAgreementDate = item.RefAgreementDate,
                        PreferentialCreditorNumber = item.PreferentialCreditorNumber,
                        Lessee = item.Lessee,
                        Lessor = item.Lessor,
                        RegistrationPlateNumber = item.RegistrationPlateNumber,
                        MachineNumber = item.MachineNumber,
                        MachineRegisteredStatus = item.MachineRegisteredStatus,
                        ChassisNumber = item.ChassisNumber,
                        RegisteredPlace = item.RegisteredPlace,
                        GuaranteeAmount = item.GuaranteeAmount,
                        Quantity = item.Quantity,
                        Unit = item.Unit,
                        ProjectName = item.ProjectName,
                        TitleDeedNumber = item.TitleDeedNumber,
                        TitleDeedSubDistrict = item.TitleDeedSubDistrict,
                        TitleDeedProvince = item.TitleDeedProvince,
                        TitleDeedDistrict = item.TitleDeedDistrict,
                        CapitalValuation = item.CapitalValuation,
                        ValuationCommittee = item.ValuationCommittee,
                        DateOfValuation = item.DateOfValuation,
                        Owner = item.Owner,
                        CompanyGUID = businessCollateralAgmTable.CompanyGUID
                    };
                    custBusinessCollaterals.Add(custBusinessCollateral);
                });
                return custBusinessCollaterals;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
