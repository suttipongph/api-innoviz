using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IFreeTextInvoiceTableService
	{

		FreeTextInvoiceTableItemView GetFreeTextInvoiceTableById(string id);
		FreeTextInvoiceTableItemView CreateFreeTextInvoiceTable(FreeTextInvoiceTableItemView freeTextInvoiceTableView);
		FreeTextInvoiceTableItemView UpdateFreeTextInvoiceTable(FreeTextInvoiceTableItemView freeTextInvoiceTableView);
		FreeTextInvoiceTableItemView GetInitialCreateData();
		bool GetFreeTextInvoiceNumberSequence(string companyId);
		bool DeleteFreeTextInvoiceTable(string id);
		IEnumerable<SelectItem<InvoiceTableItemView>> GetDropDownItemInvoiceTable(SearchParameter search);
		IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItemInvoiceTypeTable(SearchParameter search);
		IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTable(SearchParameter search);
		IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTrans(SearchParameter search);
		IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetDropDownItemInvoiceRevenueType(SearchParameter search);
		FreeTextInvoiceTableItemView GetAmount(FreeTextInvoiceTableItemView freeTextInvoiceTableView);
		#region Function
		#region post free text invoice
		PostFreeTextInvoiceView GetPostFreeTextInvoiceById(string freeTextInvoiceTableGUID);
		PostFreeTextInvoiceViewMap InitPostFreeTextInvoice(PostFreeTextInvoiceView view);
		PostFreeTextInvoiceViewMap InitPostFreeTextInvoice(FreeTextInvoiceTable freeTextInvoiceTable);
		PostFreeTextInvoiceResultView PostFreeTextInvoice(PostFreeTextInvoiceView view);
		bool GetPostFreeTextInvoiceValidation(FreeTextInvoiceTable freeTextInvoiceTable);
		#endregion post free text invoice
		#endregion Function
	}
	public class FreeTextInvoiceTableService : SmartAppService, IFreeTextInvoiceTableService
	{
		public FreeTextInvoiceTableService(SmartAppDbContext context) : base(context) { }
		public FreeTextInvoiceTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public FreeTextInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public FreeTextInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public FreeTextInvoiceTableService() : base() { }

		public FreeTextInvoiceTableItemView GetFreeTextInvoiceTableById(string id)
		{
			try
			{
				IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
				return freeTextInvoiceTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FreeTextInvoiceTableItemView CreateFreeTextInvoiceTable(FreeTextInvoiceTableItemView freeTextInvoiceTableView)
		{
			try
			{
				
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				freeTextInvoiceTableView = accessLevelService.AssignOwnerBU(freeTextInvoiceTableView);

				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
				ITaxTableService taxTableService = new TaxTableService(db);
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				if (freeTextInvoiceTableView.TaxTableGUID != null)
				{
					var taxModel = taxTableRepo.GetTaxTableByIdNoTracking(freeTextInvoiceTableView.TaxTableGUID.StringToGuid());
					if (taxModel == null)
					{
						taxModel = new TaxTable();
					}

					decimal taxValue = 0;
					try
					{
						taxValue = taxTableService.GetTaxValue(freeTextInvoiceTableView.TaxTableGUID.StringToGuid(), freeTextInvoiceTableView.IssuedDate.StringToDate(), false);
					}
					catch (Exception e)
					{
						ex.AddData("ERROR.90007", new string[] { "LABEL.TAX_TABLE", SmartAppUtil.GetDropDownLabel(taxModel.TaxCode, taxModel.Description) });
					}
				}
				if (freeTextInvoiceTableView.WithholdingTaxTableGUID != null)
				{
					var whdModel = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(freeTextInvoiceTableView.WithholdingTaxTableGUID.StringToGuid());
					if (whdModel == null)
					{
						whdModel = new WithholdingTaxTable();
					}
					decimal withholdingTaxValue = 0;
					try
					{
						withholdingTaxValue = withholdingTaxTableService.GetWHTValue(freeTextInvoiceTableView.WithholdingTaxTableGUID.StringToGuid(), freeTextInvoiceTableView.IssuedDate.StringToDate(), false);
					}
					catch (Exception e)
					{
						ex.AddData("ERROR.90007", new string[] { "LABEL.WITHHOLDING_TAX_TABLE", SmartAppUtil.GetDropDownLabel(whdModel.WHTCode, whdModel.Description) });
					}
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
				FreeTextInvoiceTable freeTextInvoiceTable = freeTextInvoiceTableView.ToFreeTextInvoiceTable();
				GenFreeTextInvoiceNumberSeqCode(freeTextInvoiceTable);

				freeTextInvoiceTable = freeTextInvoiceTableRepo.CreateFreeTextInvoiceTable(freeTextInvoiceTable);
				base.LogTransactionCreate<FreeTextInvoiceTable>(freeTextInvoiceTable);
				UnitOfWork.Commit();
				return freeTextInvoiceTable.ToFreeTextInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenFreeTextInvoiceNumberSeqCode(FreeTextInvoiceTable freeTextInvoiceTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(freeTextInvoiceTable.CompanyGUID, ReferenceId.FreeTextInvoice);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(freeTextInvoiceTable.CompanyGUID, ReferenceId.FreeTextInvoice);
					freeTextInvoiceTable.FreeTextInvoiceId = numberSequenceService.GetNumber(freeTextInvoiceTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public FreeTextInvoiceTableItemView UpdateFreeTextInvoiceTable(FreeTextInvoiceTableItemView freeTextInvoiceTableView)
		{
			try
			{
				IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
				FreeTextInvoiceTable inputFreeTextInvoiceTable = freeTextInvoiceTableView.ToFreeTextInvoiceTable();

				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
				ITaxTableService taxTableService = new TaxTableService(db);
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
				ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
				var whdModel = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(freeTextInvoiceTableView.WithholdingTaxTableGUID.StringToGuid());
				var taxModel = taxTableRepo.GetTaxTableByIdNoTracking(freeTextInvoiceTableView.TaxTableGUID.StringToGuid());
				if (whdModel == null)
				{
					whdModel = new WithholdingTaxTable();
				}
				if (taxModel == null)
				{
					taxModel = new TaxTable();
				}

				decimal taxValue = 0;
				decimal withholdingTaxValue = 0;
				try
				{
					if (ConditionService.IsNotNull(freeTextInvoiceTableView.TaxTableGUID))
					{
						taxValue = taxTableService.GetTaxValue(freeTextInvoiceTableView.TaxTableGUID.StringToGuid(), freeTextInvoiceTableView.IssuedDate.StringToDate(), false);
					}
				}
				catch (Exception e)
				{
					ex.AddData("ERROR.90007", new string[] { "LABEL.TAX_TABLE", SmartAppUtil.GetDropDownLabel(taxModel.TaxCode, taxModel.Description) });
				}
				try
				{
					if (ConditionService.IsNotNull(freeTextInvoiceTableView.WithholdingTaxTableGUID))
					{
						withholdingTaxValue = withholdingTaxTableService.GetWHTValue(freeTextInvoiceTableView.WithholdingTaxTableGUID.StringToGuid(), freeTextInvoiceTableView.IssuedDate.StringToDate(), false);
					}
				}
				catch (Exception e)
				{
					ex.AddData("ERROR.90007", new string[] { "LABEL.WITHHOLDING_TAX_TABLE", SmartAppUtil.GetDropDownLabel(whdModel.WHTCode, whdModel.Description) });
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}

				FreeTextInvoiceTable dbFreeTextInvoiceTable = freeTextInvoiceTableRepo.Find(inputFreeTextInvoiceTable.FreeTextInvoiceTableGUID);
				dbFreeTextInvoiceTable = freeTextInvoiceTableRepo.UpdateFreeTextInvoiceTable(dbFreeTextInvoiceTable, inputFreeTextInvoiceTable);
				base.LogTransactionUpdate<FreeTextInvoiceTable>(GetOriginalValues<FreeTextInvoiceTable>(dbFreeTextInvoiceTable), dbFreeTextInvoiceTable);
				UnitOfWork.Commit();
				return dbFreeTextInvoiceTable.ToFreeTextInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteFreeTextInvoiceTable(string item)
		{
			try
			{
				IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
				Guid freeTextInvoiceTableGUID = new Guid(item);
				FreeTextInvoiceTable freeTextInvoiceTable = freeTextInvoiceTableRepo.Find(freeTextInvoiceTableGUID);
				freeTextInvoiceTableRepo.Remove(freeTextInvoiceTable);
				base.LogTransactionDelete<FreeTextInvoiceTable>(freeTextInvoiceTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetFreeTextInvoiceNumberSequence(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.FreeTextInvoice);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public FreeTextInvoiceTableItemView GetInitialCreateData()
        {
			try
			{
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus statusModel = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)FreeTextInvoiceStatus.Draft).ToString());
				FreeTextInvoiceTableItemView model = new FreeTextInvoiceTableItemView();
				model.ProductType = (int)ProductType.None;
				model.DocumentStatusGUID = statusModel.DocumentStatusGUID.ToString();
				model.IssuedDate = DateTime.Now.DateToString();
				model.DocumentStatus_Description = statusModel.Description;
				model.DocumentStatus_StatusId = statusModel.StatusId;
				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceTableItemView>> GetDropDownItemInvoiceTable(SearchParameter search)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				search = search.GetParentCondition(new string[] { InvoiceTableCondition.CustomerTableGUID, InvoiceTableCondition.ProductType }  );
				SearchCondition searchConditionSuspenseInvoiceType = SearchConditionService.GetInvoiceTableBySuspenseInvoiceType(SuspenseInvoiceType.None.GetAttrValue().ToString());
				SearchCondition searchConditionProductInvoice = SearchConditionService.GetInvoiceTableByProductInvoice(false.ToString());
				search.Conditions.Add(searchConditionProductInvoice);
				search.Conditions.Add(searchConditionSuspenseInvoiceType);

				return invoiceTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItemInvoiceTypeTable(SearchParameter search)
		{
			try
			{
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				search = search.GetParentRankProductTypeCondition(new string[] { InvoiceRevenueTypeCondition.ProductType, InvoiceRevenueTypeCondition.ProductType });
				SearchCondition searchConditionSuspenseInvoiceType = SearchConditionService.GetInvoiceTableBySuspenseInvoiceType(SuspenseInvoiceType.None.GetAttrValue().ToString());
				SearchCondition searchConditionProductInvoice = SearchConditionService.GetInvoiceTableByProductInvoice(false.ToString());
				search.Conditions.Add(searchConditionProductInvoice);
				search.Conditions.Add(searchConditionSuspenseInvoiceType);
				return invoiceTypeRepo.GetDropDownItemByAutoGenInvoiceRevenueTypeNull(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTable(SearchParameter search)
		{
			try
			{
				search = search.GetParentCondition(new string[] { CreditAppTableCondition.CustomerTableGUID, CreditAppTableCondition.ProductType, CreditAppTableCondition.ExpiryDate } );
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				return creditAppTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTrans(SearchParameter search)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				search = search.GetParentCondition(new string[] { AddressTransCondition.RefGUID, AddressTransCondition.RefType });
				return addressTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetDropDownItemInvoiceRevenueType(SearchParameter search)
		{
			try
			{
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				search = search.GetParentCondition(InvoiceRevenueTypeCondition.ProductType);
				string productType = search.Conditions.First().Value;
				search.Conditions.RemoveAt(0);
				SearchCondition searchConditionInvoiceRevenueTypeByProductType = SearchConditionService.GetInvoiceRevenueTypeByProductTypeCondition(new List<string>() { "0",productType});
				SearchCondition searchConditionInvoiceRevenueTypeByIntercompany = SearchConditionService.GetInvoiceRevenueTypeByIntercompany(null);
				search.Conditions.Add(searchConditionInvoiceRevenueTypeByProductType);
				search.Conditions.Add(searchConditionInvoiceRevenueTypeByIntercompany);
				return invoiceRevenueTypeRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public FreeTextInvoiceTableItemView GetAmount(FreeTextInvoiceTableItemView freeTextInvoiceTableView)
        {
			try
			{
				FreeTextInvoiceTableItemView model = new FreeTextInvoiceTableItemView();
				IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				ITaxTableService taxTableService = new TaxTableService(db);
				decimal taxValue = 0;
				model.TaxValueError = null;
				model.WithholdingTaxValueError = null;
				decimal withholdingTaxValue = 0;
				if (ConditionService.IsNotNullAndNotEmptyGUID(freeTextInvoiceTableView.TaxTableGUID.StringToGuidNull()))
				{
					try
					{
						taxValue = taxTableService.GetTaxValue(freeTextInvoiceTableView.TaxTableGUID.StringToGuid(), freeTextInvoiceTableView.IssuedDate.StringToDate(), false);
					}
					catch (Exception e)
					{
						taxValue = -1;
						SmartAppUtil.AddStackTrace(e);
					}
				}
				if (ConditionService.IsNotNullAndNotEmptyGUID(freeTextInvoiceTableView.WithholdingTaxTableGUID.StringToGuidNull()))
				{
					try
					{
						withholdingTaxValue = withholdingTaxTableService.GetWHTValue(freeTextInvoiceTableView.WithholdingTaxTableGUID.StringToGuid(), freeTextInvoiceTableView.IssuedDate.StringToDate(), false);
					}
					catch (Exception e)
					{
						withholdingTaxValue = -1;
						SmartAppUtil.AddStackTrace(e);
					}
				}
                if (taxValue == -1)
                {
					ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
					var taxModel = taxTableRepo.GetTaxTableByIdNoTracking(freeTextInvoiceTableView.TaxTableGUID.StringToGuid());
                    if (taxModel == null)
                    {
						taxModel = new TaxTable();
					}
					taxValue = 0;
					model.TaxValueError = SmartAppUtil.GetDropDownLabel(taxModel.TaxCode,taxModel.Description);

				}
				if (withholdingTaxValue == -1)
				{
					IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
					var whtModel = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(freeTextInvoiceTableView.WithholdingTaxTableGUID.StringToGuid());
					if (whtModel == null)
					{
						whtModel = new WithholdingTaxTable();
					}
					withholdingTaxValue = 0;
					model.WithholdingTaxValueError = SmartAppUtil.GetDropDownLabel(whtModel.WHTCode,whtModel.Description);
				}
				model.TaxAmount = serviceFeeTransService.CalcTaxAmount(freeTextInvoiceTableView.InvoiceAmountBeforeTax, taxValue, false);
				model.InvoiceAmount = freeTextInvoiceTableView.InvoiceAmountBeforeTax + model.TaxAmount;
				model.WHTAmount = serviceFeeTransService.CalcTaxAmount(freeTextInvoiceTableView.InvoiceAmountBeforeTax, withholdingTaxValue, false);
				return model;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region function
		#region post freeTextInvoice
		public PostFreeTextInvoiceView GetPostFreeTextInvoiceById(string freeTextInvoiceTableGUID)
		{
			try
			{
				IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
				PostFreeTextInvoiceView postFreeTextInvoiceView = freeTextInvoiceTableRepo.GetPostFreeTextInvoiceById(freeTextInvoiceTableGUID.StringToGuid());
				return postFreeTextInvoiceView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostFreeTextInvoiceResultView PostFreeTextInvoice(PostFreeTextInvoiceView view)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
				PostFreeTextInvoiceResultView result = new PostFreeTextInvoiceResultView();
				NotificationResponse success = new NotificationResponse();
				FreeTextInvoiceTable freeTextInvoiceTable = freeTextInvoiceTableRepo.GetFreeTextInvoiceTableByIdNoTracking(view.FreeTextInvoiceTableGUID.StringToGuid());

				PostFreeTextInvoiceViewMap postFreeTextInvoice = InitPostFreeTextInvoice(freeTextInvoiceTable);

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					#region Insert data from post invoice
					// 1. GenInvoiceFromFreeTextInvoice
					// 2. PostInvoice
					this.BulkInsert(postFreeTextInvoice.InvoiceTable, false);
					this.BulkInsert(postFreeTextInvoice.InvoiceLine, false);
					this.BulkInsert(postFreeTextInvoice.CustTrans, false);
					this.BulkInsert(postFreeTextInvoice.RetentionTrans, false);
					this.BulkInsert(postFreeTextInvoice.CreditAppTrans, false);
					this.BulkInsert(postFreeTextInvoice.TaxInvoiceTable, false);
					this.BulkInsert(postFreeTextInvoice.TaxInvoiceLine, false);
					this.BulkInsert(postFreeTextInvoice.ProcessTrans, false);
					#endregion Insert data from post invoice

					#region Update FreeTextInvoiceTable
					// 3. Update FreeTextInvoiceTable 
					this.BulkUpdate(postFreeTextInvoice.FreeTextInvoiceTable.FirstToList());
					#endregion Update FreeTextInvoiceTable

					UnitOfWork.Commit(transaction);
				}

				success.AddData("SUCCESS.90004", new string[] { "LABEL.FREE_TEXT_INVOICE", SmartAppUtil.GetDropDownLabel(freeTextInvoiceTable.FreeTextInvoiceId, freeTextInvoiceTable.IssuedDate.DateToString()) });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostFreeTextInvoiceViewMap InitPostFreeTextInvoice(PostFreeTextInvoiceView view)
		{
			try
			{
				IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
				FreeTextInvoiceTable freeTextInvoiceTable = freeTextInvoiceTableRepo.GetFreeTextInvoiceTableByIdNoTracking(view.FreeTextInvoiceTableGUID.StringToGuid());
				return InitPostFreeTextInvoice(freeTextInvoiceTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostFreeTextInvoiceViewMap InitPostFreeTextInvoice(FreeTextInvoiceTable freeTextInvoiceTable)
		{
			try
			{
				IInvoiceService invoiceService = new InvoiceService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				List<InvoiceTable> createInvoiceTables = new List<InvoiceTable>();
				List<InvoiceLine> createInvoiceLines = new List<InvoiceLine>();
				List<CustTrans> createCustTranses = new List<CustTrans>();
				List<CreditAppTrans> createCreditAppTranses = new List<CreditAppTrans>();
				List<TaxInvoiceTable> createTaxInvoiceTables = new List<TaxInvoiceTable>();
				List<TaxInvoiceLine> createTaxInvoiceLines = new List<TaxInvoiceLine>();
				List<ProcessTrans> createProcessTranses = new List<ProcessTrans>();
				List<RetentionTrans> createRetentionTranses = new List<RetentionTrans>();
				PostFreeTextInvoiceResultView result = new PostFreeTextInvoiceResultView();
				NotificationResponse success = new NotificationResponse();

				#region Validate
				GetPostFreeTextInvoiceValidation(freeTextInvoiceTable);
				#endregion Validate

				#region 1. GenInvoiceFromFreeTextInvoice
				GenInvoiceFromFreeTextInvoiceResult genInvoiceFromFreeTextInvoiceResult = invoiceService.GenInvoiceFromFreeTextInvoice(freeTextInvoiceTable.FirstToList());
				#endregion 1.Create Invoice from FreeTextInvoiceLine = Pricipal

				#region 2. PostInvoice
				PostInvoiceResultView postInvoiceResultView = invoiceService.PostInvoice(genInvoiceFromFreeTextInvoiceResult.InvoiceTable, genInvoiceFromFreeTextInvoiceResult.InvoiceLine, RefType.FreeTextInvoice, freeTextInvoiceTable.FreeTextInvoiceTableGUID);
				#endregion Postinvoice

				#region 3. Update FreeTextInvoiceTable 
				DocumentStatus freeTextInvoiceStatusPosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)FreeTextInvoiceStatus.Posted).ToString());
				freeTextInvoiceTable.DocumentStatusGUID = freeTextInvoiceStatusPosted.DocumentStatusGUID;
				freeTextInvoiceTable.InvoiceTableGUID = postInvoiceResultView.InvoiceTables.FirstOrDefault().InvoiceTableGUID;
				freeTextInvoiceTable.ModifiedBy = db.GetUserName();
				freeTextInvoiceTable.ModifiedDateTime = DateTime.Now;
				#endregion 3. Update FreeTextInvoiceTable 

				#region Setup data from post freeTextInvoice
				#region Setup data from post invoice
				createInvoiceTables.AddRange(postInvoiceResultView.InvoiceTables);
				createInvoiceLines.AddRange(postInvoiceResultView.InvoiceLines);
				createTaxInvoiceTables.AddRange(postInvoiceResultView.TaxInvoiceTables);
				createTaxInvoiceLines.AddRange(postInvoiceResultView.TaxInvoiceLines);
				createCustTranses.AddRange(postInvoiceResultView.CustTranses);
				createCreditAppTranses.AddRange(postInvoiceResultView.CreditAppTranses);
				createProcessTranses.AddRange(postInvoiceResultView.ProcessTranses);
				createRetentionTranses.AddRange(postInvoiceResultView.RetentionTranses);
				#endregion Setup data from post invoice

				#region Setup data and ordering to PostFreeTextInvoiceViewMap
				PostFreeTextInvoiceViewMap postFreeTextInvoice = new PostFreeTextInvoiceViewMap
				{
					FreeTextInvoiceTable = freeTextInvoiceTable,
					InvoiceTable = createInvoiceTables.OrderBy(o => o.InvoiceId).ToList(),
					InvoiceLine = (from invoiceLine in createInvoiceLines
								   join invoiceTable in createInvoiceTables
								   on invoiceLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceLine
								   from invoiceTable in ljInvoiceTableInvoiceLine.DefaultIfEmpty()
								   select new
								   {
									   invoiceLine,
									   invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
								   }).OrderBy(o => o.invoiceId).Select(s => s.invoiceLine).ToList(),
					CustTrans = (from custTrans in createCustTranses
								 join invoiceTable in createInvoiceTables
								 on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableCustTrans
								 from invoiceTable in ljInvoiceTableCustTrans.DefaultIfEmpty()
								 select new
								 {
									 custTrans,
									 invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
								 }).OrderBy(o => o.invoiceId).Select(s => s.custTrans).ToList(),
					CreditAppTrans = createCreditAppTranses,
					ProcessTrans = (from processTrans in createProcessTranses
									join invoiceTable in createInvoiceTables
									on new { processTrans.RefType, InvoiceTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.Invoice, invoiceTable.InvoiceTableGUID } into ljInvoiceTableProcessTrans
									from invoiceTable in ljInvoiceTableProcessTrans.DefaultIfEmpty()
									select new
									{
										processTrans,
										processTrans.RefType,
										RefId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
									}).OrderBy(o => o.RefType).ThenBy(t => t.RefId).ThenBy(t => t.processTrans.CreditAppTableGUID).Select(s => s.processTrans).ToList(),
					RetentionTrans = createRetentionTranses,
					TaxInvoiceTable = createTaxInvoiceTables.OrderBy(o => o.TaxInvoiceId).ToList(),
					TaxInvoiceLine = (from taxInvoiceLine in createTaxInvoiceLines
									  join taxInvoiceTable in createTaxInvoiceTables
									  on taxInvoiceLine.TaxInvoiceTableGUID equals taxInvoiceTable.TaxInvoiceTableGUID into ljTaxInvoiceTableTaxInvoiceLine
									  from taxInvoiceTable in ljTaxInvoiceTableTaxInvoiceLine.DefaultIfEmpty()
									  select new
									  {
										  taxInvoiceLine,
										  taxInvoiceId = taxInvoiceTable != null ? taxInvoiceTable.TaxInvoiceId : string.Empty
									  }).OrderBy(o => o.taxInvoiceId).Select(s => s.taxInvoiceLine).ToList(),
				};
				#endregion SetupData and Ordering to PostFreeTextInvoiceViewMap
				#endregion Setup data from post freeTextInvoice

				return postFreeTextInvoice;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetPostFreeTextInvoiceValidation(FreeTextInvoiceTable freeTextInvoiceTable)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				#region Validate
				// Validate No.1
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus freeTextInvoiceStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)FreeTextInvoiceStatus.Draft).ToString());

				if (freeTextInvoiceTable.DocumentStatusGUID != freeTextInvoiceStatusDraft.DocumentStatusGUID) // Validate No.1
				{
					ex.AddData("ERROR.90012", new string[] { "LABEL.FREE_TEXT_INVOICE" });
				}
				if (freeTextInvoiceTable.InvoiceAmount == 0) // Validate No.2
				{
					ex.AddData("ERROR.90049", new string[] { "LABEL.INVOICE_AMOUNT" });
				}
				if (freeTextInvoiceTable.InvoiceAmount != freeTextInvoiceTable.InvoiceAmountBeforeTax + freeTextInvoiceTable.TaxAmount) // Validate No.3
				{
					ex.AddData("ERROR.90154", new string[] { "LABEL.AMOUNT_BEFORE_TAX", "LABEL.TAX_AMOUNT", "LABEL.INVOICE_AMOUNT" });
				}
				#endregion Validate

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion function
	}
}
