using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICustVisitingTransService
	{

		CustVisitingTransItemView GetCustVisitingTransById(string id);
		CustVisitingTransItemView CreateCustVisitingTrans(CustVisitingTransItemView custVisitingTransView);
		CustVisitingTransItemView UpdateCustVisitingTrans(CustVisitingTransItemView custVisitingTransView);
		bool DeleteCustVisitingTrans(string id);
		CustVisitingTransItemView GetCustVisitingTransInitialData(string refId, string refGUID, RefType refType);
		#region function
		IEnumerable<SelectItem<CustVisitingTransItemView>> GetCustVisitingTransByCustomer(SearchParameter search);
		CopyCustVisitingView CopyCustVisiting(CopyCustVisitingView paramView);
		CopyCustVisitingView GetCopyCustVisitingById(string id);
		#endregion
		#region Migration
		void CreateCustVisitingTransCollection(IEnumerable<CustVisitingTransItemView> custVisitingTransViews);
        #endregion
    }
    public class CustVisitingTransService : SmartAppService, ICustVisitingTransService
	{
		public CustVisitingTransService(SmartAppDbContext context) : base(context) { }
		public CustVisitingTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CustVisitingTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustVisitingTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CustVisitingTransService() : base() { }

		public CustVisitingTransItemView GetCustVisitingTransById(string id)
		{
			try
			{
				ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
				return custVisitingTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustVisitingTransItemView CreateCustVisitingTrans(CustVisitingTransItemView custVisitingTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				custVisitingTransView = accessLevelService.AssignOwnerBU(custVisitingTransView);
				ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
				CustVisitingTrans custVisitingTrans = custVisitingTransView.ToCustVisitingTrans();
				custVisitingTrans = custVisitingTransRepo.CreateCustVisitingTrans(custVisitingTrans);
				base.LogTransactionCreate<CustVisitingTrans>(custVisitingTrans);
				UnitOfWork.Commit();
				return custVisitingTrans.ToCustVisitingTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustVisitingTransItemView UpdateCustVisitingTrans(CustVisitingTransItemView custVisitingTransView)
		{
			try
			{
				ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
				CustVisitingTrans inputCustVisitingTrans = custVisitingTransView.ToCustVisitingTrans();
				CustVisitingTrans dbCustVisitingTrans = custVisitingTransRepo.Find(inputCustVisitingTrans.CustVisitingTransGUID);
				dbCustVisitingTrans = custVisitingTransRepo.UpdateCustVisitingTrans(dbCustVisitingTrans, inputCustVisitingTrans);
				base.LogTransactionUpdate<CustVisitingTrans>(GetOriginalValues<CustVisitingTrans>(dbCustVisitingTrans), dbCustVisitingTrans);
				UnitOfWork.Commit();
				return dbCustVisitingTrans.ToCustVisitingTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCustVisitingTrans(string item)
		{
			try
			{
				ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
				Guid custVisitingTransGUID = new Guid(item);
				CustVisitingTrans custVisitingTrans = custVisitingTransRepo.Find(custVisitingTransGUID);
				custVisitingTransRepo.Remove(custVisitingTrans);
				base.LogTransactionDelete<CustVisitingTrans>(custVisitingTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustVisitingTransItemView GetCustVisitingTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				IEmployeeTableService employeeTableService = new EmployeeTableService(db);
				EmployeeTableItemView employeeTable = employeeTableService.GetEmployeeTableByUserIdAndCompany(db.GetUserId(), db.GetCompanyFilter());
				return new CustVisitingTransItemView
				{
					CustVisitingTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					ResponsibleByGUID = (employeeTable != null ) ? employeeTable.EmployeeTableGUID : null
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region function

		public IEnumerable<SelectItem<CustVisitingTransItemView>> GetCustVisitingTransByCustomer(SearchParameter search)
		{
			try
			{
				ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
				search = search.GetParentCondition(CustomerVisitingTransCondition.RefGUID);
				return custVisitingTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CopyCustVisitingView CopyCustVisiting(CopyCustVisitingView paramView)
		{
			try
			{
				ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
				CopyCustVisitingView result = new CopyCustVisitingView();
				NotificationResponse success = new NotificationResponse();

				CustVisitingTrans custVisitingTrans = custVisitingTransRepo.GetCustVisitingTransByIdNoTracking(paramView.CustVisitingTransGUID.StringToGuid());
				if (custVisitingTrans != null)
				{
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						try
						{							
								CustVisitingTrans createItem = new CustVisitingTrans()
								{
									CustVisitingTransGUID = Guid.NewGuid(),
									ResponsibleByGUID = custVisitingTrans.ResponsibleByGUID,
									CustVisitingDate = custVisitingTrans.CustVisitingDate,
									CustVisitingMemo = custVisitingTrans.CustVisitingMemo,
									Description = custVisitingTrans.Description,
									RefType = paramView.RefType,
									RefGUID = paramView.RefGUID.StringToGuid(),
									CompanyGUID = db.GetCompanyFilter().StringToGuid()
								};
								
							BulkInsert(createItem.FirstToList());
							UnitOfWork.Commit(transaction);
						}
						catch (Exception e)
						{
							transaction.Rollback();
							throw SmartAppUtil.AddStackTrace(e);
						}
					}
				}
				success.AddData("SUCCESS.90002", new string[] { paramView.ResultLabel });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CopyCustVisitingView GetCopyCustVisitingById(string id)
		{
			try
			{
				ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
				CopyCustVisitingView copyCustVisitingView = new CopyCustVisitingView();
				CopyCustVisitingView custVisitingTrans = custVisitingTransRepo.GetCopyCustVisitingById(id.StringToGuid());
				return custVisitingTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region Migration
		public void CreateCustVisitingTransCollection(IEnumerable<CustVisitingTransItemView> custVisitingTransViews)
		{
			try
			{
				if (custVisitingTransViews.Any())
				{
					ICustVisitingTransRepo custVisitingTransRepo = new CustVisitingTransRepo(db);
					List<CustVisitingTrans> custVisitingTranss = custVisitingTransViews.ToCustVisitingTrans().ToList();
					custVisitingTransRepo.ValidateAdd(custVisitingTranss);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(custVisitingTranss, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
