using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IRelatedPersonTableService
	{
		#region Dropdown
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByCreditAppRequestTable(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByCreditAppRequestTableGuarantor(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByAmendCA(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByCreditAppRequestTableOwnerTrans(SearchParameter search);
		IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans(SearchParameter search);
		#endregion Dropdown
		RelatedPersonTableItemView GetRelatedPersonTableById(string id);
		RelatedPersonTableItemView CreateRelatedPersonTable(RelatedPersonTableItemView relatedPersonTableView);
		RelatedPersonTableItemView UpdateRelatedPersonTable(RelatedPersonTableItemView relatedPersonTableView);
		bool DeleteRelatedPersonTable(string id);
		bool IsManualByRelatedPerson(string companyId);
		MemoTransItemView GetMemoTransInitialData(string refGUID);
		AddressTransItemView GetAddressTransInitialData(string refGUID);
		int CalcAge(DateTime? dateOfBirth);
		void CreateRelatedPersonTableCollection(List<RelatedPersonTableItemView> relatedPersonTableItemViews);
	}
	public class RelatedPersonTableService : SmartAppService, IRelatedPersonTableService
	{
		public RelatedPersonTableService(SmartAppDbContext context) : base(context) { }
		public RelatedPersonTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public RelatedPersonTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RelatedPersonTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public RelatedPersonTableService() : base() { }

        #region CRUD
        public RelatedPersonTableItemView GetRelatedPersonTableById(string id)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				return relatedPersonTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RelatedPersonTableItemView CreateRelatedPersonTable(RelatedPersonTableItemView relatedPersonTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				relatedPersonTableView = accessLevelService.AssignOwnerBU(relatedPersonTableView);
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				RelatedPersonTable relatedPersonTable = relatedPersonTableView.ToRelatedPersonTable();
				GenRelaedPersonNumberSeqCode(relatedPersonTable);
				relatedPersonTable = relatedPersonTableRepo.CreateRelatedPersonTable(relatedPersonTable);
				base.LogTransactionCreate<RelatedPersonTable>(relatedPersonTable);
				UnitOfWork.Commit();
				return relatedPersonTable.ToRelatedPersonTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RelatedPersonTableItemView UpdateRelatedPersonTable(RelatedPersonTableItemView relatedPersonTableView)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				RelatedPersonTable inputRelatedPersonTable = relatedPersonTableView.ToRelatedPersonTable();
				RelatedPersonTable dbRelatedPersonTable = relatedPersonTableRepo.Find(inputRelatedPersonTable.RelatedPersonTableGUID);
				dbRelatedPersonTable = relatedPersonTableRepo.UpdateRelatedPersonTable(dbRelatedPersonTable, inputRelatedPersonTable);
				base.LogTransactionUpdate<RelatedPersonTable>(GetOriginalValues<RelatedPersonTable>(dbRelatedPersonTable), dbRelatedPersonTable);
				UnitOfWork.Commit();
				return dbRelatedPersonTable.ToRelatedPersonTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteRelatedPersonTable(string item)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				Guid relatedPersonTableGUID = new Guid(item);
				RelatedPersonTable relatedPersonTable = relatedPersonTableRepo.Find(relatedPersonTableGUID);
				relatedPersonTableRepo.Remove(relatedPersonTable);
				base.LogTransactionDelete<RelatedPersonTable>(relatedPersonTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion CRUD
        #region NumberSeq
        public void GenRelaedPersonNumberSeqCode(RelatedPersonTable relatedPersonTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(relatedPersonTable.CompanyGUID, ReferenceId.RelatedPerson);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(relatedPersonTable.CompanyGUID, ReferenceId.RelatedPerson);
					relatedPersonTable.RelatedPersonId = numberSequenceService.GetNumber(relatedPersonTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}

		public bool IsManualByRelatedPerson(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.RelatedPerson);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion NumberSeq
        #region InitialData
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
		{
			try
			{
				IMemoTransService memoTransService = new MemoTransService(db);
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				string refId = relatedPersonTableRepo.GetRelatedPersonTableByIdNoTracking(refGUID.StringToGuid()).RelatedPersonId;
				return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.RelatedPerson);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressTransItemView GetAddressTransInitialData(string refGUID)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				string refId = relatedPersonTableRepo.GetRelatedPersonTableByIdNoTracking(refGUID.StringToGuid()).RelatedPersonId;
				return addressTransService.GetAddressTransInitialData(refId, refGUID, Models.Enum.RefType.RelatedPerson);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InitialData
		#region Dropdown
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByCreditAppRequestTable(SearchParameter search)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				search = search.GetParentCondition(RelatedPersonTableCondition.CreditAppRequestTableGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetRelatedPersonTableByRefTypeCondition(false, (int)RefType.Customer);
				search.Conditions.AddRange(searchConditions);
				return relatedPersonTableRepo.GetDropDownItemByCreditAppRequestTable(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByCreditAppRequestTableGuarantor(SearchParameter search)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				search = search.GetParentCondition(RelatedPersonTableCondition.CreditAppRequestTableGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetRelatedPersonTableByRefTypeCondition(false, (int)RefType.Customer);
				search.Conditions.AddRange(searchConditions);
				return relatedPersonTableRepo.GetDropDownItemByCreditAppRequestTableGuarantor(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByCreditAppRequestTableOwnerTrans(SearchParameter search)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				search = search.GetParentCondition(RelatedPersonTableCondition.CreditAppRequestTableGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetRelatedPersonTableByRefTypeCondition(false, (int)RefType.Customer);
				search.Conditions.AddRange(searchConditions);
				return relatedPersonTableRepo.GetDropDownItemByCreditAppRequestTableOwnerTrans(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans(SearchParameter search)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				search = search.GetParentCondition(RelatedPersonTableCondition.CreditAppRequestTableGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetRelatedPersonTableByRefTypeCondition(false, (int)RefType.Customer);
				search.Conditions.AddRange(searchConditions);
				return relatedPersonTableRepo.GetDropDownItemByCreditAppRequestTableAuthorizedPersonTrans(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTableItemByAmendCA(SearchParameter search)
		{
			try
			{
				IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
				search = search.GetParentCondition(RelatedPersonTableCondition.CreditAppRequestTableGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetRelatedPersonTableByRefTypeCondition(true, (int)RefType.CreditAppRequestTable);
				search.Conditions.AddRange(searchConditions);
				return relatedPersonTableRepo.GetDropDownItemByAmendCa(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public int CalcAge(DateTime? dateOfBirth)
		{
			try
			{
				return (dateOfBirth == null) ? 0 : Convert.ToInt32(Math.Floor(DateTime.Now.Subtract(dateOfBirth.Value).TotalDays / 365.25));
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region migration
		public void CreateRelatedPersonTableCollection(List<RelatedPersonTableItemView> relatedPersonTableItemViews)
		{
			try
			{
				if (relatedPersonTableItemViews.Any())
				{
					IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
					List<RelatedPersonTable> relatedPersonTable = relatedPersonTableItemViews.ToRelatedPersonTable().ToList();
					relatedPersonTableRepo.ValidateAdd(relatedPersonTable);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(relatedPersonTable, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion
	}
}
