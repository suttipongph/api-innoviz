using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IBuyerReceiptTableService
    {

        BuyerReceiptTableItemView GetBuyerReceiptTableById(string id);
        BuyerReceiptTableItemView CreateBuyerReceiptTable(BuyerReceiptTableItemView buyerReceiptTableView);
        BuyerReceiptTable CreateBuyerReceiptTable(BuyerReceiptTable buyerReceiptTable);
        BuyerReceiptTableItemView UpdateBuyerReceiptTable(BuyerReceiptTableItemView buyerReceiptTableView);
        bool DeleteBuyerReceiptTable(string id);
        bool IsManualBuyerReceiptTable(string companyId);
        BuyerReceiptTableItemView GetBuyerReceiptInitialPurchaseLineData(string refGUID);
        BuyerReceiptTableItemView GetBuyerReceiptInitialWithdrawalLineData(string refGUID);
        PrintBuyerReceipt GetPrintBuyerReceiptById(string refGUID);
        #region dropdown
        IEnumerable<SelectItem<BuyerReceiptTableItemView>> GetDropDownItemByReceiptTempTable(SearchParameter search);
        #endregion
        #region function
        #region cancel buyerReceiptTable
        CancelBuyerReceiptTableView GetCancelBuyerReceiptTableById(string buyerReceiptTableGUID);
        CancelBuyerReceiptTableResultView CancelBuyerReceiptTable(CancelBuyerReceiptTableView buyerReceiptTable);
        #endregion
        #endregion
    }
    public class BuyerReceiptTableService : SmartAppService, IBuyerReceiptTableService
    {
        public BuyerReceiptTableService(SmartAppDbContext context) : base(context) { }
        public BuyerReceiptTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public BuyerReceiptTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public BuyerReceiptTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public BuyerReceiptTableService() : base() { }

        public BuyerReceiptTableItemView GetBuyerReceiptTableById(string id)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                return buyerReceiptTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenBuyerRecieptNumberSeqCode(BuyerReceiptTable buyerReceiptTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = numberSequenceService.IsManualByReferenceId(buyerReceiptTable.CompanyGUID, ReferenceId.BuyerReceipt);
                if (!isManual)
                {
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(buyerReceiptTable.CompanyGUID, ReferenceId.BuyerReceipt);
                    buyerReceiptTable.BuyerReceiptId = numberSequenceService.GetNumber(buyerReceiptTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public BuyerReceiptTableItemView CreateBuyerReceiptTable(BuyerReceiptTableItemView buyerReceiptTableView)
        {
            try
            {
                
                BuyerReceiptTable buyerReceiptTable = buyerReceiptTableView.ToBuyerReceiptTable();
                GenBuyerRecieptNumberSeqCode(buyerReceiptTable);
                buyerReceiptTable = CreateBuyerReceiptTable(buyerReceiptTable);
                UnitOfWork.Commit();
                return buyerReceiptTable.ToBuyerReceiptTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerReceiptTable CreateBuyerReceiptTable(BuyerReceiptTable buyerReceiptTable)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                buyerReceiptTable = accessLevelService.AssignOwnerBU(buyerReceiptTable);
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                buyerReceiptTable = buyerReceiptTableRepo.CreateBuyerReceiptTable(buyerReceiptTable);
                base.LogTransactionCreate<BuyerReceiptTable>(buyerReceiptTable);
                return buyerReceiptTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerReceiptTableItemView UpdateBuyerReceiptTable(BuyerReceiptTableItemView buyerReceiptTableView)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                BuyerReceiptTable inputBuyerReceiptTable = buyerReceiptTableView.ToBuyerReceiptTable();
                BuyerReceiptTable dbBuyerReceiptTable = buyerReceiptTableRepo.Find(inputBuyerReceiptTable.BuyerReceiptTableGUID);
                dbBuyerReceiptTable = buyerReceiptTableRepo.UpdateBuyerReceiptTable(dbBuyerReceiptTable, inputBuyerReceiptTable);
                base.LogTransactionUpdate<BuyerReceiptTable>(GetOriginalValues<BuyerReceiptTable>(dbBuyerReceiptTable), dbBuyerReceiptTable);
                UnitOfWork.Commit();
                return dbBuyerReceiptTable.ToBuyerReceiptTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteBuyerReceiptTable(string item)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                Guid buyerReceiptTableGUID = new Guid(item);
                BuyerReceiptTable buyerReceiptTable = buyerReceiptTableRepo.Find(buyerReceiptTableGUID);
                buyerReceiptTableRepo.Remove(buyerReceiptTable);
                base.LogTransactionDelete<BuyerReceiptTable>(buyerReceiptTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualBuyerReceiptTable(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.BuyerReceipt);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerReceiptTableItemView GetBuyerReceiptInitialPurchaseLineData(string refGUID)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                return buyerReceiptTableRepo.GetBuyerReceiptInitialPurchaseLineData(refGUID.StringToGuid(), (int)RefType.CollectionFollowUp);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerReceiptTableItemView GetBuyerReceiptInitialWithdrawalLineData(string refGUID)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                return buyerReceiptTableRepo.GetBuyerReceiptInitialWithdrawalLineData(refGUID.StringToGuid(), (int)RefType.CollectionFollowUp);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region dropdown
        public IEnumerable<SelectItem<BuyerReceiptTableItemView>> GetDropDownItemByReceiptTempTable(SearchParameter search)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                search = search.GetParentCondition(new string[] { BuyerReceiptTableCondition.CustomerTableGUID, BuyerReceiptTableCondition.BuyerTableGUID });
                SearchCondition searchCondition = SearchConditionService.GetBuyerReceiptTableByCancelCondition(false);
                search.Conditions.Add(searchCondition);
                return buyerReceiptTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region function
        #region cancel buyerReceiptTable
        public CancelBuyerReceiptTableView GetCancelBuyerReceiptTableById(string buyerReceiptTableGUID)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                BuyerReceiptTableItemView buyerReceiptTable = buyerReceiptTableRepo.GetByIdvw(buyerReceiptTableGUID.StringToGuid());

                CancelBuyerReceiptTableView cancelBuyerReceiptTableView = new CancelBuyerReceiptTableView
                {
                    BuyerReceiptTableGUID = buyerReceiptTable.BuyerReceiptTableGUID.GuidNullToString(),
                    CustomerTable_Values = buyerReceiptTable.CustomerTable_Values,
                    BuyerReceiptAmount = buyerReceiptTable.BuyerReceiptAmount,
                    BuyerReceiptDate = buyerReceiptTable.BuyerReceiptDate,
                    BuyerReceiptTable_Values = SmartAppUtil.GetDropDownLabel(buyerReceiptTable.BuyerReceiptId, buyerReceiptTable.BuyerReceiptDate),
                    BuyerTable_Values = buyerReceiptTable.BuyerTable_Values,
                    Cancel = buyerReceiptTable.Cancel
                };
                return cancelBuyerReceiptTableView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CancelBuyerReceiptTableResultView CancelBuyerReceiptTable(CancelBuyerReceiptTableView buyerReceiptTable)
        {
            try
            {
                CancelBuyerReceiptTableResultView result = new CancelBuyerReceiptTableResultView();
                NotificationResponse success = new NotificationResponse();
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);

                BuyerReceiptTableItemView buyerReceiptTableItemView = buyerReceiptTableRepo.GetByIdvw(buyerReceiptTable.BuyerReceiptTableGUID.StringToGuid());
                buyerReceiptTableItemView.Cancel = true;
                UpdateBuyerReceiptTable(buyerReceiptTableItemView);

                success.AddData("SUCCESS.90008", new string[] { "LABEL.BUYER_RECEIPT", SmartAppUtil.GetDropDownLabel(buyerReceiptTableItemView.BuyerReceiptId, buyerReceiptTableItemView.BuyerReceiptDate) });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region Report
        public PrintBuyerReceipt GetPrintBuyerReceiptById(string refGUID)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                BuyerReceiptTableItemView buyerReceiptTableItemView = buyerReceiptTableRepo.GetByIdvw(refGUID.StringToGuid());
                PrintBuyerReceipt printBuyerReceipt = new PrintBuyerReceipt
                {
                    BuyerReceiptTableGUID = buyerReceiptTableItemView.BuyerReceiptTableGUID,
                    BuyerReceiptId = buyerReceiptTableItemView.BuyerReceiptId,
                    BuyerReceiptDate = buyerReceiptTableItemView.BuyerReceiptDate,
                    BuyerId = buyerReceiptTableItemView.BuyerTable_Values,
                    CustomerId = buyerReceiptTableItemView.CustomerTable_Values,
                    BuyerReceiptAmount = buyerReceiptTableItemView.BuyerReceiptAmount,
                    Cancel = buyerReceiptTableItemView.Cancel
                };
                return printBuyerReceipt;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
