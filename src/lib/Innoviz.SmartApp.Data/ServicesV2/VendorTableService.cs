using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IVendorTableService
	{

		VendorTableItemView GetVendorTableById(string id);
		VendorTableItemView CreateVendorTable(VendorTableItemView vendorTableView);
		VendorTableItemView UpdateVendorTable(VendorTableItemView vendorTableView);
		bool DeleteVendorTable(string id);
		bool IsManual(string companyId);
		ContactTransItemView GetContactTransInitialData(string refGUID);
		AddressTransItemView GetAddressTransInitialData(string refGUID);
		void CreateVendorTableCollection(List<VendorTableItemView> vendorTableItemViews);
	}
	public class VendorTableService : SmartAppService, IVendorTableService
	{
		public VendorTableService(SmartAppDbContext context) : base(context) { }
		public VendorTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public VendorTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VendorTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public VendorTableService() : base() { }

		public VendorTableItemView GetVendorTableById(string id)
		{
			try
			{
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
				return vendorTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendorTableItemView CreateVendorTable(VendorTableItemView vendorTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				vendorTableView = accessLevelService.AssignOwnerBU(vendorTableView);
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
				VendorTable vendorTable = vendorTableView.ToVendorTable();
				GenVendorNumberSeqCode(vendorTable);
				vendorTable = vendorTableRepo.CreateVendorTable(vendorTable);
				base.LogTransactionCreate<VendorTable>(vendorTable);
				UnitOfWork.Commit();
				return vendorTable.ToVendorTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendorTableItemView UpdateVendorTable(VendorTableItemView vendorTableView)
		{
			try
			{
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
				VendorTable inputVendorTable = vendorTableView.ToVendorTable();
				VendorTable dbVendorTable = vendorTableRepo.Find(inputVendorTable.VendorTableGUID);
				dbVendorTable = vendorTableRepo.UpdateVendorTable(dbVendorTable, inputVendorTable);
				base.LogTransactionUpdate<VendorTable>(GetOriginalValues<VendorTable>(dbVendorTable), dbVendorTable);
				UnitOfWork.Commit();
				return dbVendorTable.ToVendorTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteVendorTable(string item)
		{
			try
			{
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
				Guid vendorTableGUID = new Guid(item);
				VendorTable vendorTable = vendorTableRepo.Find(vendorTableGUID);
				vendorTableRepo.Remove(vendorTable);
				base.LogTransactionDelete<VendorTable>(vendorTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsManual(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.Vendor);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenVendorNumberSeqCode(VendorTable vendorTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(vendorTable.CompanyGUID, ReferenceId.Vendor);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(vendorTable.CompanyGUID, ReferenceId.Vendor);
					vendorTable.VendorId = numberSequenceService.GetNumber(vendorTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public ContactTransItemView GetContactTransInitialData(string refGUID)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db);
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
				string refId = vendorTableRepo.GetVendorTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).VendorId;
				return ContactTransService.GetContactTransInitialData(refId, refGUID, Models.Enum.RefType.Vendor);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressTransItemView GetAddressTransInitialData(string refGUID)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
				string refId = vendorTableRepo.GetVendorTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).VendorId;
				return addressTransService.GetAddressTransInitialData(refId, refGUID, Models.Enum.RefType.Vendor);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region migration
		public void CreateVendorTableCollection(List<VendorTableItemView> vendorTableItemViews)
		{
			try
			{
				if (vendorTableItemViews.Any())
				{
					IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
					List<VendorTable> vendorTable = vendorTableItemViews.ToVendorTable().ToList();
					vendorTableRepo.ValidateAdd(vendorTable);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(vendorTable, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion
	}
}
