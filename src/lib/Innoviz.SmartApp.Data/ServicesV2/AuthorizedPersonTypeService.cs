using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAuthorizedPersonTypeService
	{

		AuthorizedPersonTypeItemView GetAuthorizedPersonTypeById(string id);
		AuthorizedPersonTypeItemView CreateAuthorizedPersonType(AuthorizedPersonTypeItemView authorizedPersonTypeView);
		AuthorizedPersonTypeItemView UpdateAuthorizedPersonType(AuthorizedPersonTypeItemView authorizedPersonTypeView);
		bool DeleteAuthorizedPersonType(string id);
	}
	public class AuthorizedPersonTypeService : SmartAppService, IAuthorizedPersonTypeService
	{
		public AuthorizedPersonTypeService(SmartAppDbContext context) : base(context) { }
		public AuthorizedPersonTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AuthorizedPersonTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AuthorizedPersonTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AuthorizedPersonTypeService() : base() { }

		public AuthorizedPersonTypeItemView GetAuthorizedPersonTypeById(string id)
		{
			try
			{
				IAuthorizedPersonTypeRepo authorizedPersonTypeRepo = new AuthorizedPersonTypeRepo(db);
				return authorizedPersonTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonTypeItemView CreateAuthorizedPersonType(AuthorizedPersonTypeItemView authorizedPersonTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				authorizedPersonTypeView = accessLevelService.AssignOwnerBU(authorizedPersonTypeView);
				IAuthorizedPersonTypeRepo authorizedPersonTypeRepo = new AuthorizedPersonTypeRepo(db);
				AuthorizedPersonType authorizedPersonType = authorizedPersonTypeView.ToAuthorizedPersonType();
				authorizedPersonType = authorizedPersonTypeRepo.CreateAuthorizedPersonType(authorizedPersonType);
				base.LogTransactionCreate<AuthorizedPersonType>(authorizedPersonType);
				UnitOfWork.Commit();
				return authorizedPersonType.ToAuthorizedPersonTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonTypeItemView UpdateAuthorizedPersonType(AuthorizedPersonTypeItemView authorizedPersonTypeView)
		{
			try
			{
				IAuthorizedPersonTypeRepo authorizedPersonTypeRepo = new AuthorizedPersonTypeRepo(db);
				AuthorizedPersonType inputAuthorizedPersonType = authorizedPersonTypeView.ToAuthorizedPersonType();
				AuthorizedPersonType dbAuthorizedPersonType = authorizedPersonTypeRepo.Find(inputAuthorizedPersonType.AuthorizedPersonTypeGUID);
				dbAuthorizedPersonType = authorizedPersonTypeRepo.UpdateAuthorizedPersonType(dbAuthorizedPersonType, inputAuthorizedPersonType);
				base.LogTransactionUpdate<AuthorizedPersonType>(GetOriginalValues<AuthorizedPersonType>(dbAuthorizedPersonType), dbAuthorizedPersonType);
				UnitOfWork.Commit();
				return dbAuthorizedPersonType.ToAuthorizedPersonTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAuthorizedPersonType(string item)
		{
			try
			{
				IAuthorizedPersonTypeRepo authorizedPersonTypeRepo = new AuthorizedPersonTypeRepo(db);
				Guid authorizedPersonTypeGUID = new Guid(item);
				AuthorizedPersonType authorizedPersonType = authorizedPersonTypeRepo.Find(authorizedPersonTypeGUID);
				authorizedPersonTypeRepo.Remove(authorizedPersonType);
				base.LogTransactionDelete<AuthorizedPersonType>(authorizedPersonType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
