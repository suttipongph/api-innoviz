using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IParentCompanyService
	{

		ParentCompanyItemView GetParentCompanyById(string id);
		ParentCompanyItemView CreateParentCompany(ParentCompanyItemView parentCompanyView);
		ParentCompanyItemView UpdateParentCompany(ParentCompanyItemView parentCompanyView);
		bool DeleteParentCompany(string id);
	}
	public class ParentCompanyService : SmartAppService, IParentCompanyService
	{
		public ParentCompanyService(SmartAppDbContext context) : base(context) { }
		public ParentCompanyService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ParentCompanyService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ParentCompanyService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ParentCompanyService() : base() { }

		public ParentCompanyItemView GetParentCompanyById(string id)
		{
			try
			{
				IParentCompanyRepo parentCompanyRepo = new ParentCompanyRepo(db);
				return parentCompanyRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ParentCompanyItemView CreateParentCompany(ParentCompanyItemView parentCompanyView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				parentCompanyView = accessLevelService.AssignOwnerBU(parentCompanyView);
				IParentCompanyRepo parentCompanyRepo = new ParentCompanyRepo(db);
				ParentCompany parentCompany = parentCompanyView.ToParentCompany();
				parentCompany = parentCompanyRepo.CreateParentCompany(parentCompany);
				base.LogTransactionCreate<ParentCompany>(parentCompany);
				UnitOfWork.Commit();
				return parentCompany.ToParentCompanyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ParentCompanyItemView UpdateParentCompany(ParentCompanyItemView parentCompanyView)
		{
			try
			{
				IParentCompanyRepo parentCompanyRepo = new ParentCompanyRepo(db);
				ParentCompany inputParentCompany = parentCompanyView.ToParentCompany();
				ParentCompany dbParentCompany = parentCompanyRepo.Find(inputParentCompany.ParentCompanyGUID);
				dbParentCompany = parentCompanyRepo.UpdateParentCompany(dbParentCompany, inputParentCompany);
				base.LogTransactionUpdate<ParentCompany>(GetOriginalValues<ParentCompany>(dbParentCompany), dbParentCompany);
				UnitOfWork.Commit();
				return dbParentCompany.ToParentCompanyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteParentCompany(string item)
		{
			try
			{
				IParentCompanyRepo parentCompanyRepo = new ParentCompanyRepo(db);
				Guid parentCompanyGUID = new Guid(item);
				ParentCompany parentCompany = parentCompanyRepo.Find(parentCompanyGUID);
				parentCompanyRepo.Remove(parentCompany);
				base.LogTransactionDelete<ParentCompany>(parentCompany);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
