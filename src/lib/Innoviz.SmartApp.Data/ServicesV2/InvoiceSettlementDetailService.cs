using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IInvoiceSettlementDetailService
    {

        InvoiceSettlementDetailItemView GetInvoiceSettlementDetailById(string id);
        InvoiceSettlementDetailItemView CreateInvoiceSettlementDetail(InvoiceSettlementDetailItemView invoiceSettlementDetailView);
        InvoiceSettlementDetailItemView UpdateInvoiceSettlementDetail(InvoiceSettlementDetailItemView invoiceSettlementDetailView);
        bool DeleteInvoiceSettlementDetail(string id);
        List<InvoiceSettlementDetail> GetByReferenceAndSuspenseInvoiceType(int refType, string refGUID, int suspenseInvoiceType);
        #region shared
        CalcSettlePurchaseAmountResultView CalcSettlePurchaseAmount(Guid invoiceTableGuid, decimal settleAmountParam);
        CalcSettlePurchaseAmountResultView CalcSettlePurchaseAmount(string invoiceTableGuid, decimal settleAmountParam);
        List<InvoiceSettlementDetail> GetByReferenceAndSuspenseInvoiceType(RefType refType, Guid refGUID, SuspenseInvoiceType suspenseInvoiceType);
        decimal CalcSettleTaxAmount(decimal invoiceAmount, decimal taxAmount, decimal settleInvoiceAmount, Guid? taxTableGUID);
        List<InvoiceSettlementDetail> GenInvoiceSettlementDetailFromServiceFeeTrans(List<ServiceFeeTrans> serviceFeeTransList, List<InvoiceTable> invoiceTableList,
                                                                                    string documentId, RefType refType, Guid refGUID);
        List<InvoiceSettlementDetail> GenInvoiceSettlementDetailFromInvoiceSettlementDetail(List<InvoiceSettlementDetail> invSettlementDetailList,
                                                                                            RefType toRefType, Guid toRefGUID, Guid? refReceiptTempPaymDetailGUID = null);
        List<InvoiceSettlementDetail> GenInvoiceSettlementDetailFromInvoice(List<InvoiceTable> invoiceTableList,
                                                                            RefType refType, Guid refGUID, Guid? refReceiptTempPaymDetailGUID = null);
        #endregion
        #region InitialData
        InvoiceSettlementDetailItemView GetInvoiceSettlementDetailInitialData(InvoiceSettlementDetailItemView invoiceSettlementDetailItemView);
        #endregion InitialData
        CalcSettleInvoiceAmountResultView GetCalcSettleInvoiceAmount(InvoiceSettlementDetailItemView invoiceSettlementDetailItemView, bool editWHT = true);
        void CreateInvoiceSettlementDetailCollection(IEnumerable<InvoiceSettlementDetailItemView> InvoiceSettlementDetailItemView);
    }
    public class InvoiceSettlementDetailService : SmartAppService, IInvoiceSettlementDetailService
    {
        public InvoiceSettlementDetailService(SmartAppDbContext context) : base(context) { }
        public InvoiceSettlementDetailService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public InvoiceSettlementDetailService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public InvoiceSettlementDetailService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public InvoiceSettlementDetailService() : base() { }

        public InvoiceSettlementDetailItemView GetInvoiceSettlementDetailById(string id)
        {
            try
            {
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                return invoiceSettlementDetailRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceSettlementDetailItemView CreateInvoiceSettlementDetail(InvoiceSettlementDetailItemView invoiceSettlementDetailView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                invoiceSettlementDetailView = accessLevelService.AssignOwnerBU(invoiceSettlementDetailView);
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                InvoiceSettlementDetail invoiceSettlementDetail = invoiceSettlementDetailView.ToInvoiceSettlementDetail();
                invoiceSettlementDetail = invoiceSettlementDetailRepo.CreateInvoiceSettlementDetail(invoiceSettlementDetail);
                base.LogTransactionCreate<InvoiceSettlementDetail>(invoiceSettlementDetail);

                #region Create Relation spec 0020307: LIT1213
                CreateUpdateInvoiceSettlementDetailRelatedFunction(invoiceSettlementDetail);
                #endregion

                UnitOfWork.Commit();
                return invoiceSettlementDetail.ToInvoiceSettlementDetailItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceSettlementDetailItemView UpdateInvoiceSettlementDetail(InvoiceSettlementDetailItemView invoiceSettlementDetailView)
        {
            try
            {
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                InvoiceSettlementDetail inputInvoiceSettlementDetail = invoiceSettlementDetailView.ToInvoiceSettlementDetail();
                InvoiceSettlementDetail dbInvoiceSettlementDetail = invoiceSettlementDetailRepo.Find(inputInvoiceSettlementDetail.InvoiceSettlementDetailGUID);
                dbInvoiceSettlementDetail = invoiceSettlementDetailRepo.UpdateInvoiceSettlementDetail(dbInvoiceSettlementDetail, inputInvoiceSettlementDetail);
                base.LogTransactionUpdate<InvoiceSettlementDetail>(GetOriginalValues<InvoiceSettlementDetail>(dbInvoiceSettlementDetail), dbInvoiceSettlementDetail);
                
                #region Update Relation spec 0020308: LIT1215
                CreateUpdateInvoiceSettlementDetailRelatedFunction(inputInvoiceSettlementDetail);
                #endregion

                UnitOfWork.Commit();
                return dbInvoiceSettlementDetail.ToInvoiceSettlementDetailItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteInvoiceSettlementDetail(string item)
        {
            try
            {
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                Guid invoiceSettlementDetailGUID = new Guid(item);
                InvoiceSettlementDetail invoiceSettlementDetail = invoiceSettlementDetailRepo.Find(invoiceSettlementDetailGUID);
                invoiceSettlementDetailRepo.Remove(invoiceSettlementDetail);
                base.LogTransactionDelete<InvoiceSettlementDetail>(invoiceSettlementDetail);

                #region Update Relation spec 0020308: LIT1215
                DeleteInvoiceSettlementDetailRelatedFunction(invoiceSettlementDetail);
                #endregion

                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public List<InvoiceSettlementDetail> GetByReferenceAndSuspenseInvoiceType(int refType, string refGUID, int suspenseInvoiceType)
        {
            try
            {
                return GetByReferenceAndSuspenseInvoiceType((RefType)refType, refGUID.StringToGuid(), (SuspenseInvoiceType)suspenseInvoiceType);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region shared
        public CalcSettlePurchaseAmountResultView CalcSettlePurchaseAmount(Guid invoiceTableGuid, decimal settleAmountParam)
        {
            try
            {
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceTableGuid);
                CalcSettlePurchaseAmountResultView calcSettlePurchaseAmountResultView = new CalcSettlePurchaseAmountResultView();
                CustTrans custTrans = custTransRepo.GetCustTransByInvoiceTableNoTracking(invoiceTableGuid);
                decimal balanceAmount = (custTrans != null) ? custTrans.TransAmount - custTrans.SettleAmount: 0;
                decimal settleAmount = settleAmountParam;
                decimal purchaseAmount = purchaseTableService.GetPurchaseLineAmount(invoiceTable.RefGUID);
                decimal purchaseAmountBalance = purchaseTableService.GetPurchaseLineAmountBalance(invoiceTable.RefGUID);
                if (settleAmount >= purchaseAmountBalance)
                {
                    calcSettlePurchaseAmountResultView.SettlePurchaseAmount = purchaseAmountBalance;
                    calcSettlePurchaseAmountResultView.SettleInvoiceAmount = balanceAmount;
                }
                else
                {
                    calcSettlePurchaseAmountResultView.SettlePurchaseAmount = settleAmount;
                    calcSettlePurchaseAmountResultView.SettleInvoiceAmount = purchaseAmountBalance == 0 ? 0 : (settleAmount * balanceAmount) / purchaseAmountBalance;
                }
                calcSettlePurchaseAmountResultView.BalanceAmount = balanceAmount;
                calcSettlePurchaseAmountResultView.PurchaseAmount = purchaseAmount;
                calcSettlePurchaseAmountResultView.PurchaseAmountBalance = purchaseAmountBalance;
                calcSettlePurchaseAmountResultView.SettleReserveAmount = calcSettlePurchaseAmountResultView.SettleInvoiceAmount - calcSettlePurchaseAmountResultView.SettlePurchaseAmount;
                calcSettlePurchaseAmountResultView.ReserveToBeRefund = (settleAmount - purchaseAmountBalance) < 0 ? 0 : settleAmount - purchaseAmountBalance;
                calcSettlePurchaseAmountResultView.WHTAmount = 0;
                calcSettlePurchaseAmountResultView.SettleTaxAmount = 0;
                return calcSettlePurchaseAmountResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CalcSettlePurchaseAmountResultView CalcSettlePurchaseAmount(string invoiceTableGuid, decimal settleAmountParam)
        {
            try
            {
                return CalcSettlePurchaseAmount(invoiceTableGuid.StringToGuid(), settleAmountParam);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<InvoiceSettlementDetail> GetByReferenceAndSuspenseInvoiceType(RefType refType, Guid refGUID,SuspenseInvoiceType suspenseInvoiceType)
        {
            try
            {
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                return invoiceSettlementDetailRepo.GetByReferenceAndSuspenseInvoiceType(refType, refGUID, suspenseInvoiceType).ToList();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal CalcSettleTaxAmount(decimal invoiceAmount, decimal taxAmount, decimal settleInvoiceAmount, Guid? taxTableGUID)
        {
            try
            {
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                TaxTable taxTable = taxTableGUID.HasValue ? taxTableRepo.GetTaxTableByIdNoTracking(taxTableGUID.Value) : null;
                if (taxTable != null)
                {
                    if (ConditionService.IsNotNullAndNotEmptyGUID(taxTable.PaymentTaxTableGUID) && taxAmount != 0 && invoiceAmount != 0)
                    {
                        if (invoiceAmount != settleInvoiceAmount)
                        {
                            return ((settleInvoiceAmount * taxAmount) / invoiceAmount).Round();
                        }
                        else
                        {
                            return taxAmount.Round();
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<InvoiceSettlementDetail> GenInvoiceSettlementDetailFromServiceFeeTrans(List<ServiceFeeTrans> serviceFeeTransList, List<InvoiceTable> invoiceTableList,
                                                                                    string documentId, RefType refType, Guid refGUID)
        {
            try
            {
                var result =
                    (from serviceFeeTrans in serviceFeeTransList.Where(w => w.SettleAmount > 0)
                     join invoice in invoiceTableList.Where(w => w.RefType == (int)RefType.ServiceFeeTrans)
                     on serviceFeeTrans.ServiceFeeTransGUID equals invoice.RefGUID
                     select new InvoiceSettlementDetail
                     {
                         ProductType = invoice.ProductType,
                         DocumentId = documentId,
                         InvoiceTableGUID = invoice.InvoiceTableGUID,
                         InvoiceDueDate = invoice.DueDate,
                         InvoiceTypeGUID = invoice.InvoiceTypeGUID,
                         SuspenseInvoiceType = invoice.SuspenseInvoiceType,
                         InvoiceAmount = invoice.InvoiceAmount,
                         BalanceAmount = invoice.InvoiceAmount,
                         SettleAmount = serviceFeeTrans.SettleAmount,
                         WHTAmount = serviceFeeTrans.SettleWHTAmount,
                         SettleInvoiceAmount = serviceFeeTrans.SettleInvoiceAmount,
                         SettleTaxAmount = CalcSettleTaxAmount(invoiceAmount: invoice.InvoiceAmount,
                                                                taxAmount: serviceFeeTrans.TaxAmount,
                                                                settleInvoiceAmount: serviceFeeTrans.SettleInvoiceAmount,
                                                                taxTableGUID: serviceFeeTrans.TaxTableGUID),
                         WHTSlipReceivedByBuyer = false,
                         WHTSlipReceivedByCustomer = false,
                         BuyerAgreementTableGUID = null,
                         AssignmentAgreementTableGUID = null,
                         SettleAssignmentAmount = 0.0m,
                         PurchaseAmount = 0.0m,
                         PurchaseAmountBalance = 0.0m,
                         SettlePurchaseAmount = 0.0m,
                         SettleReserveAmount = 0.0m,
                         InterestCalculationMethod = (int)InterestCalculationMethod.Minimum,
                         RetentionAmountAccum = 0.0m,
                         RefType = (int)refType,
                         RefGUID = refGUID,
                         RefReceiptTempPaymDetailGUID = null,
                         InvoiceSettlementDetailGUID = Guid.NewGuid(),
                         CompanyGUID = serviceFeeTrans.CompanyGUID
                     }).ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<InvoiceSettlementDetail> GenInvoiceSettlementDetailFromInvoice(List<InvoiceTable> invoiceTableList,
                                                                            RefType refType, Guid refGUID, Guid? refReceiptTempPaymDetailGUID = null)
        {
            try
            {
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);

                List<Guid> invoiceTableGUIDs = invoiceTableList.Select(s => s.InvoiceTableGUID).ToList();
                var custTransByInvoice = custTransRepo.GetCustTransByInvoiceTableNoTracking(invoiceTableGUIDs);
                
                // concern: performance
                var invoiceLineTaxTables = invoiceLineRepo.GetInvoiceLineNotNullTaxTableByInvoiceTableGUIDNoTracking(invoiceTableGUIDs)
                                                .GroupBy(g => g.InvoiceTableGUID)
                                                .Select(s => new { InvoiceTableGUID = s.Key, TaxTableGUID = s.FirstOrDefault().TaxTableGUID });

                var result =
                    (from invoiceTable in invoiceTableList
                     join invoiceLine in invoiceLineTaxTables
                     on invoiceTable.InvoiceTableGUID equals invoiceLine.InvoiceTableGUID into ljInvoiceTableInvoiceLine

                     from invoiceLine in ljInvoiceTableInvoiceLine.DefaultIfEmpty()
                     join custTrans in custTransByInvoice
                     on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID into ljInvoiceTableCustTrans

                     from custTrans in ljInvoiceTableCustTrans.DefaultIfEmpty()
                     select new InvoiceSettlementDetail
                     {
                         ProductType = invoiceTable.ProductType,
                         DocumentId = invoiceTable.DocumentId,
                         InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                         InvoiceDueDate = invoiceTable.DueDate,
                         InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID,
                         SuspenseInvoiceType = invoiceTable.SuspenseInvoiceType,
                         InvoiceAmount = invoiceTable.InvoiceAmount,
                         BalanceAmount = (custTrans != null) ? (custTrans.TransAmount - custTrans.SettleAmount) : 0.0m,
                         SettleAmount = (custTrans != null) ? (custTrans.TransAmount - custTrans.SettleAmount) : 0.0m,
                         WHTAmount = 0.0m,
                         SettleInvoiceAmount = (custTrans != null) ? (custTrans.TransAmount - custTrans.SettleAmount) : 0.0m,
                         SettleTaxAmount = CalcSettleTaxAmount(invoiceAmount: invoiceTable.InvoiceAmount,
                                                                taxAmount: invoiceTable.TaxAmount,
                                                                settleInvoiceAmount: (custTrans != null) ? (custTrans.TransAmount - custTrans.SettleAmount) : 0.0m,
                                                                taxTableGUID: (invoiceLine != null) ? invoiceLine.TaxTableGUID : null),
                         WHTSlipReceivedByBuyer = false,
                         WHTSlipReceivedByCustomer = false,
                         BuyerAgreementTableGUID = invoiceTable.BuyerAgreementTableGUID,
                         AssignmentAgreementTableGUID = null,
                         SettleAssignmentAmount = 0.0m,
                         PurchaseAmount = 0.0m,
                         PurchaseAmountBalance = 0.0m,
                         SettlePurchaseAmount = 0.0m,
                         SettleReserveAmount = 0.0m,
                         InterestCalculationMethod = (int)InterestCalculationMethod.Minimum,
                         RetentionAmountAccum = 0.0m,
                         RefType = (int)refType,
                         RefGUID = refGUID,
                         RefReceiptTempPaymDetailGUID = refReceiptTempPaymDetailGUID,
                         InvoiceSettlementDetailGUID = Guid.NewGuid(),
                         CompanyGUID = invoiceTable.CompanyGUID
                     }).ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<InvoiceSettlementDetail> GenInvoiceSettlementDetailFromInvoiceSettlementDetail(List<InvoiceSettlementDetail> invoiceSettlementDetailList,
                                                                                            RefType toRefType, Guid toRefGUID, Guid? refReceiptTempPaymDetailGUID = null)
        {
            try
            {
                var result = invoiceSettlementDetailList.Select(s => new InvoiceSettlementDetail
                {
                    ProductType = s.ProductType,
                    DocumentId = s.DocumentId,
                    InvoiceTableGUID = s.InvoiceTableGUID,
                    InvoiceDueDate = s.InvoiceDueDate,
                    InvoiceTypeGUID = s.InvoiceTypeGUID,
                    SuspenseInvoiceType = s.SuspenseInvoiceType,
                    InvoiceAmount = s.InvoiceAmount,
                    BalanceAmount = s.BalanceAmount,
                    SettleAmount = s.SettleAmount,
                    WHTAmount = s.WHTAmount,
                    SettleInvoiceAmount = s.SettleInvoiceAmount,
                    SettleTaxAmount = s.SettleTaxAmount,
                    WHTSlipReceivedByBuyer = false,
                    WHTSlipReceivedByCustomer = false,
                    BuyerAgreementTableGUID = null,
                    AssignmentAgreementTableGUID = null,
                    SettleAssignmentAmount = 0.0m,
                    PurchaseAmount = 0.0m,
                    PurchaseAmountBalance = 0.0m,
                    SettlePurchaseAmount = 0.0m,
                    SettleReserveAmount = 0.0m,
                    InterestCalculationMethod = (int)InterestCalculationMethod.Minimum,
                    RetentionAmountAccum = 0.0m,
                    RefType = (int)toRefType,
                    RefGUID = toRefGUID,
                    RefReceiptTempPaymDetailGUID = refReceiptTempPaymDetailGUID,
                    InvoiceSettlementDetailGUID = Guid.NewGuid(),
                    CompanyGUID = s.CompanyGUID
                }).ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region InitialData
        public InvoiceSettlementDetailItemView GetInvoiceSettlementDetailInitialData(InvoiceSettlementDetailItemView invoiceSettlementDetailItemView)
        {
            try
            {
                Guid invoiceTableGUID = invoiceSettlementDetailItemView.InvoiceTableGUID.StringToGuid();
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                IInvoiceService invoiceService = new InvoiceService(db);
                decimal whtBalance = invoiceService.GetSettleWHTBalance(invoiceTableGUID);
                decimal settleTaxBalance = invoiceService.GetSettleTaxBalance(invoiceTableGUID);
                InvoiceOutstandingListViewMap invoiceOutstandingListViewMap = invoiceTableRepo.GetInvoiceSettlementDetailInitialData(invoiceSettlementDetailItemView.InvoiceTableGUID);
                invoiceSettlementDetailItemView.InvoiceSettlementDetailGUID = Guid.NewGuid().GuidNullToString();
                invoiceSettlementDetailItemView.ProductType = invoiceOutstandingListViewMap.ProductType; 
                invoiceSettlementDetailItemView.DocumentId = invoiceOutstandingListViewMap.DocumentId;
                invoiceSettlementDetailItemView.InvoiceTableGUID = invoiceOutstandingListViewMap.InvoiceTableGUID.GuidNullToString();
                invoiceSettlementDetailItemView.InvoiceDueDate = invoiceOutstandingListViewMap.DueDate.DateToString();
                invoiceSettlementDetailItemView.InvoiceTypeGUID = invoiceOutstandingListViewMap.InvoiceTypeGUID.GuidNullToString();
                invoiceSettlementDetailItemView.InvoiceAmount = invoiceOutstandingListViewMap.InvoiceAmount;
                invoiceSettlementDetailItemView.BalanceAmount = invoiceOutstandingListViewMap.BalanceAmount;
                invoiceSettlementDetailItemView.BuyerAgreementTableGUID = invoiceOutstandingListViewMap.BuyerAgreementTableGUID.GuidNullToString();
                invoiceSettlementDetailItemView.InterestCalculationMethod = (int)InterestCalculationMethod.Minimum;
                invoiceSettlementDetailItemView.MaxSettleAmount = invoiceOutstandingListViewMap.BalanceAmount - whtBalance;
                invoiceSettlementDetailItemView.ReceivedFrom = invoiceSettlementDetailItemView.ReceivedFrom;
                invoiceSettlementDetailItemView.InvoiceTable_Values = invoiceOutstandingListViewMap.InvoiceTable_Values;
                invoiceSettlementDetailItemView.InvoiceType_Values = invoiceOutstandingListViewMap.InvoiceType_Values;
                invoiceSettlementDetailItemView.Currency_Values = invoiceOutstandingListViewMap.Currency_Values;
                invoiceSettlementDetailItemView.CreditAppTable_Values = invoiceOutstandingListViewMap.CreditAppTable_Values;
                invoiceSettlementDetailItemView.SettleAmount = invoiceSettlementDetailItemView.MaxSettleAmount;
                invoiceSettlementDetailItemView.SettleInvoiceAmount = invoiceOutstandingListViewMap.BalanceAmount;
                invoiceSettlementDetailItemView.RefReceiptTempPaymDetailGUID = invoiceSettlementDetailItemView.RefReceiptTempPaymDetailGUID;
                invoiceSettlementDetailItemView.InvoiceTable_ProductType = invoiceOutstandingListViewMap.ProductType;
                invoiceSettlementDetailItemView.InvoiceTable_ProductInvoice = invoiceOutstandingListViewMap.ProductInvoice;
                invoiceSettlementDetailItemView.SuspenseInvoiceType = invoiceOutstandingListViewMap.SuspenseInvoiceType;
                if (invoiceSettlementDetailItemView.ReceivedFrom == (int)ReceivedFrom.Buyer && invoiceOutstandingListViewMap.ProductInvoice)
                {
                    switch (invoiceOutstandingListViewMap.RefType)
                    {
                        case (int)RefType.PurchaseLine:
                            IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                            PurchaseLine purchaseLine = purchaseLineRepo
                                .GetPurchaseLineByInvoice(invoiceSettlementDetailItemView.InvoiceTableGUID.StringToGuid())
                                .FirstOrDefault();
                            if(purchaseLine != null)
                            {
                                invoiceSettlementDetailItemView.AssignmentAgreementTableGUID = purchaseLine.AssignmentAgreementTableGUID.GuidNullToString();
                                invoiceSettlementDetailItemView.SettleAssignmentAmount = purchaseLine.AssignmentAmount;
                            }
                            break;
                        case (int)RefType.WithdrawalLine:
                            WithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                            WithdrawalTable withdrawalTable = withdrawalTableRepo
                                .GetWithdrawalTableByInvoice(invoiceSettlementDetailItemView.InvoiceTableGUID.StringToGuid());                            
                            if (withdrawalTable != null)
                            {
                                invoiceSettlementDetailItemView.AssignmentAgreementTableGUID = withdrawalTable.AssignmentAgreementTableGUID.GuidNullToString();
                                invoiceSettlementDetailItemView.SettleAssignmentAmount = withdrawalTable.AssignmentAmount;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (invoiceOutstandingListViewMap.ProductType == (int)ProductType.Factoring && invoiceOutstandingListViewMap.ProductInvoice)
                {
                    CalcSettlePurchaseAmountResultView calcSettlePurchaseAmountResultView = CalcSettlePurchaseAmount(invoiceTableGUID, invoiceSettlementDetailItemView.SettleAmount);
                    invoiceSettlementDetailItemView.PurchaseAmount = calcSettlePurchaseAmountResultView.PurchaseAmount;
                    invoiceSettlementDetailItemView.PurchaseAmountBalance = calcSettlePurchaseAmountResultView.PurchaseAmountBalance;
                    invoiceSettlementDetailItemView.SettlePurchaseAmount = calcSettlePurchaseAmountResultView.SettlePurchaseAmount;
                    invoiceSettlementDetailItemView.SettleReserveAmount = calcSettlePurchaseAmountResultView.SettleReserveAmount;
                }
                else
                {
                    invoiceSettlementDetailItemView.SettleTaxAmount = settleTaxBalance;
                    invoiceSettlementDetailItemView.WHTAmount = whtBalance;
                }
                if(!string.IsNullOrWhiteSpace(invoiceSettlementDetailItemView.AssignmentAgreementTableGUID) 
                && invoiceSettlementDetailItemView.AssignmentAgreementTableGUID != Guid.Empty.GuidNullToString())
                {
                    IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                    AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.Find(invoiceSettlementDetailItemView.AssignmentAgreementTableGUID.StringToGuid());
                    invoiceSettlementDetailItemView.AssignmentAgreementTable_Values = SmartAppUtil.GetDropDownLabel(assignmentAgreementTable.InternalAssignmentAgreementId, assignmentAgreementTable.Description);
                }
                invoiceSettlementDetailItemView.InvoiceTable_WHTBalance = invoiceSettlementDetailItemView.WHTAmount;
                return invoiceSettlementDetailItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion  InitialData
        public CalcSettleInvoiceAmountResultView GetCalcSettleInvoiceAmount(InvoiceSettlementDetailItemView invoiceSettlementDetailItemView, bool editWHT = true)
        {
            try
            {
                IInvoiceService invoiceService = new InvoiceService(db);
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceSettlementDetailItemView.InvoiceTableGUID.StringToGuid());
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                CustTrans custTrans = custTransRepo.GetCustTransByInvoiceTableNoTracking(invoiceTable.InvoiceTableGUID);
                CalcSettleInvoiceAmountResultView calcSettleInvoiceAmountResultView = invoiceService.CalcSettleInvoiceAmount(
                    invoiceTable,
                    custTrans,
                    invoiceSettlementDetailItemView.SettleAmount,
                    invoiceSettlementDetailItemView.WHTAmount,
                    editWHT);
                return calcSettleInvoiceAmountResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region CreateUpdateInvoiceSettlementDetailRelatedFunction
        private void CreateUpdateInvoiceSettlementDetailRelatedFunction(InvoiceSettlementDetail invoiceSettlementDetail)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                #region LIT1213, LIT1215
                #region 1
                InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceSettlementDetail.InvoiceTableGUID.Value);
                if (
                    invoiceTable.ProductInvoice &&
                    invoiceSettlementDetail.RefType == (int)RefType.ReceiptTemp &&
                    invoiceSettlementDetail.SuspenseInvoiceType == (int)SuspenseInvoiceType.None
                    )
                {
                    IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                    IProductSettledTransService productSettledTransService = new ProductSettledTransService(db);

                    ProductSettledTrans productSettledTrans = productSettledTransService.RecalculateProductSettledTrans(invoiceSettlementDetail);
                    ProductSettledTrans dbProductSettledTrans = productSettledTransRepo.GetProductSettledTransByIdNoTracking(productSettledTrans.ProductSettledTransGUID);
                    if (dbProductSettledTrans == null)
                        productSettledTransService.CreateProductSettledTrans(productSettledTrans);
                    else
                        productSettledTransService.UpdateProductSettledTrans(productSettledTrans);
                }
                #endregion
                #region 2
                if (
                    invoiceSettlementDetail.RefType == (int)RefType.ReceiptTemp &&
                    invoiceSettlementDetail.SuspenseInvoiceType == (int)SuspenseInvoiceType.None
                    )
                {
                    IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);

                    ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetByReference(RefType.ReceiptTemp, invoiceSettlementDetail.RefGUID.GetValueOrDefault())
                        .Where(w => w.InvoiceSettlementDetailGUID != invoiceSettlementDetail.InvoiceSettlementDetailGUID && w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                        .GroupBy(g => g.RefGUID)
                        .Select(s => s.Sum(sum => sum.SettleAmount))
                        .FirstOrDefault();
                    receiptTempTable.SettleAmount = invoiceSettlementDetailSettleAmount + invoiceSettlementDetail.SettleAmount;
                    receiptTempTableService.UpdateReceiptTempTable(receiptTempTable);
                }
                #endregion
                #region 3
                if (
                    invoiceSettlementDetail.RefType == (int)RefType.CustomerRefund &&
                    invoiceSettlementDetail.SuspenseInvoiceType == (int)SuspenseInvoiceType.None
                    )
                {
                    ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);

                    ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);

                    CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetByReference(RefType.CustomerRefund, invoiceSettlementDetail.RefGUID.GetValueOrDefault())
                        .Where(w => w.InvoiceSettlementDetailGUID != invoiceSettlementDetail.InvoiceSettlementDetailGUID && w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                        .GroupBy(g => g.RefGUID)
                        .Select(s => s.Sum(sum => sum.SettleAmount))
                        .FirstOrDefault();
                    customerRefundTable.SettleAmount = invoiceSettlementDetailSettleAmount + invoiceSettlementDetail.SettleAmount;
                    customerRefundTableService.UpdateCustomerRefundTable(customerRefundTable);
                }
                #endregion
                #endregion
                #region LIT1618, LIT1365
                #region 1_Suspense
                if (invoiceSettlementDetail.RefType == (int)RefType.ReceiptTemp &&
                    invoiceSettlementDetail.SuspenseInvoiceType != (int)SuspenseInvoiceType.None
                    )
                {
                    IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);

                    ReceiptTempPaymDetail receiptTempPaymDetail = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByIdNoTracking(invoiceSettlementDetail.RefReceiptTempPaymDetailGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByRefReceiptTempPaymDetailNoTracking(invoiceSettlementDetail.RefReceiptTempPaymDetailGUID.GetValueOrDefault())
                        .Where(w => w.SuspenseInvoiceType != (int)SuspenseInvoiceType.None)
                        .GroupBy(g => g.RefReceiptTempPaymDetailGUID)
                        .Select(s => s.Sum(sum => sum.SettleAmount))
                        .FirstOrDefault();
                    receiptTempPaymDetail.ReceiptAmount = (-1) * (invoiceSettlementDetailSettleAmount + invoiceSettlementDetail.SettleAmount);
                    receiptTempTableService.UpdateReceiptTempPaymDetail(receiptTempPaymDetail);
                }
                #endregion
                #region 2_Suspense
                if (
                    invoiceSettlementDetail.RefType == (int)RefType.CustomerRefund &&
                    invoiceSettlementDetail.SuspenseInvoiceType != (int)SuspenseInvoiceType.None
                    )
                {
                    ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                    ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);

                    CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetByReference(RefType.CustomerRefund, invoiceSettlementDetail.RefGUID.GetValueOrDefault())
                        .Where(w => w.InvoiceSettlementDetailGUID != invoiceSettlementDetail.InvoiceSettlementDetailGUID && w.SuspenseInvoiceType != (int)SuspenseInvoiceType.None)
                        .GroupBy(g => g.RefGUID)
                        .Select(s => s.Sum(sum => sum.SettleAmount))
                        .FirstOrDefault();
                    customerRefundTable.SuspenseAmount = (-1) * (invoiceSettlementDetailSettleAmount + invoiceSettlementDetail.SettleAmount);
                    customerRefundTableService.UpdateCustomerRefundTable(customerRefundTable);
                }
                #endregion
                #endregion
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region DeleteInvoiceSettlementDetailRelatedFunction
        private void DeleteInvoiceSettlementDetailRelatedFunction(InvoiceSettlementDetail invoiceSettlementDetail)
        {
            try
            {
                #region 1
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                IProductSettledTransService productSettledTransService = new ProductSettledTransService(db);
                ProductSettledTrans productSettledTrans = productSettledTransRepo.GetProductSettledTransByInvoiceSettlementDetailGuid(invoiceSettlementDetail.InvoiceSettlementDetailGUID);
                if (productSettledTrans != null)
                {
                    productSettledTransService.DeleteProductSettledTrans(productSettledTrans);
                }
                #endregion
                #region 2
                if (invoiceSettlementDetail.RefType == (int)RefType.ReceiptTemp && invoiceSettlementDetail.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                {
                    IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);

                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);

                    ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetByReference(RefType.ReceiptTemp, invoiceSettlementDetail.RefGUID.GetValueOrDefault())
                                                                                             .Where(w => w.InvoiceSettlementDetailGUID != invoiceSettlementDetail.InvoiceSettlementDetailGUID
                                                                                                        && w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                                                                                             .GroupBy(g => g.RefGUID).Select(s => s.Sum(sum => sum.SettleAmount))
                                                                                             .FirstOrDefault();
                    receiptTempTable.SettleAmount = invoiceSettlementDetailSettleAmount;
                    receiptTempTableService.UpdateReceiptTempTable(receiptTempTable);
                }
                #endregion
                #region 3
                if (invoiceSettlementDetail.RefType == (int)RefType.CustomerRefund && invoiceSettlementDetail.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                {
                    ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                    ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);

                    CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetByReference(RefType.CustomerRefund, invoiceSettlementDetail.RefGUID.GetValueOrDefault())
                        .Where(w => w.InvoiceSettlementDetailGUID != invoiceSettlementDetail.InvoiceSettlementDetailGUID
                                    && w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                        .GroupBy(g => g.RefGUID)
                        .Select(s => s.Sum(sum => sum.SettleAmount))
                        .FirstOrDefault();
                    customerRefundTable.SettleAmount = invoiceSettlementDetailSettleAmount;
                    customerRefundTableService.UpdateCustomerRefundTable(customerRefundTable);
                }
                #endregion

                #region LIT1365
                #region 1_Suspense
                if (
                    invoiceSettlementDetail.RefType == (int)RefType.ReceiptTemp &&
                    invoiceSettlementDetail.SuspenseInvoiceType != (int)SuspenseInvoiceType.None
                    )
                {
                    IReceiptTempPaymDetailRepo receiptTempPaymDetailRepo = new ReceiptTempPaymDetailRepo(db);
                    IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);

                    ReceiptTempPaymDetail receiptTempPaymDetail = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByIdNoTracking(invoiceSettlementDetail.RefReceiptTempPaymDetailGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByRefReceiptTempPaymDetailNoTracking(invoiceSettlementDetail.RefReceiptTempPaymDetailGUID.GetValueOrDefault())
                        .Where(w => w.InvoiceSettlementDetailGUID != invoiceSettlementDetail.InvoiceSettlementDetailGUID
                                 && w.SuspenseInvoiceType != (int)SuspenseInvoiceType.None)
                        .GroupBy(g => g.RefReceiptTempPaymDetailGUID)
                        .Select(s => s.Sum(sum => sum.SettleAmount))
                        .FirstOrDefault();
                    receiptTempPaymDetail.ReceiptAmount = (-1) * invoiceSettlementDetailSettleAmount;
                    receiptTempTableService.UpdateReceiptTempPaymDetail(receiptTempPaymDetail);

                    ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault());
                    decimal receiptTempPaymDetailReceiptAmount = receiptTempPaymDetailRepo.GetReceiptTempPaymDetailByReceiptTempNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault())
                        .Where(w => w.ReceiptTempPaymDetailGUID != receiptTempPaymDetail.ReceiptTempPaymDetailGUID)
                        .GroupBy(g => g.ReceiptTempTableGUID)
                        .Select(s => s.Sum(sum => sum.ReceiptAmount))
                        .FirstOrDefault();

                    receiptTempTable.ReceiptAmount = receiptTempPaymDetailReceiptAmount + receiptTempPaymDetail.ReceiptAmount;
                    receiptTempTableService.UpdateReceiptTempTable(receiptTempTable);
                }
                #endregion
                #region 2_Suspense
                if (
                    invoiceSettlementDetail.RefType == (int)RefType.CustomerRefund &&
                    invoiceSettlementDetail.SuspenseInvoiceType != (int)SuspenseInvoiceType.None
                    )
                {
                    ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                    ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);

                    CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTracking(invoiceSettlementDetail.RefGUID.GetValueOrDefault());
                    decimal invoiceSettlementDetailSettleAmount = invoiceSettlementDetailRepo.GetByReference(RefType.CustomerRefund, invoiceSettlementDetail.RefGUID.GetValueOrDefault())
                        .Where(w => w.InvoiceSettlementDetailGUID != invoiceSettlementDetail.InvoiceSettlementDetailGUID
                                 && w.SuspenseInvoiceType != (int)SuspenseInvoiceType.None)
                        .GroupBy(g => g.RefGUID)
                        .Select(s => s.Sum(sum => sum.SettleAmount))
                        .FirstOrDefault();
                    customerRefundTable.SuspenseAmount = invoiceSettlementDetailSettleAmount;
                    customerRefundTableService.UpdateCustomerRefundTable(customerRefundTable);
                }
                #endregion
                #endregion

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Migration
        public void CreateInvoiceSettlementDetailCollection(IEnumerable<InvoiceSettlementDetailItemView> InvoiceSettlementDetailItemView)
        {
            try
            {
                if (InvoiceSettlementDetailItemView.Any())
                {
                    IInvoiceSettlementDetailRepo InvoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                    List<InvoiceSettlementDetail> InvoiceSettlementDetails = InvoiceSettlementDetailItemView.ToInvoiceSettlementDetail().ToList();
                    InvoiceSettlementDetailRepo.ValidateAdd(InvoiceSettlementDetails);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(InvoiceSettlementDetails, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion Migration
    }
}
