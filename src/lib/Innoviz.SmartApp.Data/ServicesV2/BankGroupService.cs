using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBankGroupService
	{

		BankGroupItemView GetBankGroupById(string id);
		BankGroupItemView CreateBankGroup(BankGroupItemView bankGroupView);
		BankGroupItemView UpdateBankGroup(BankGroupItemView bankGroupView);
		bool DeleteBankGroup(string id);
	}
	public class BankGroupService : SmartAppService, IBankGroupService
	{
		public BankGroupService(SmartAppDbContext context) : base(context) { }
		public BankGroupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BankGroupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BankGroupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BankGroupService() : base() { }

		public BankGroupItemView GetBankGroupById(string id)
		{
			try
			{
				IBankGroupRepo bankGroupRepo = new BankGroupRepo(db);
				return bankGroupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BankGroupItemView CreateBankGroup(BankGroupItemView bankGroupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				bankGroupView = accessLevelService.AssignOwnerBU(bankGroupView);
				IBankGroupRepo bankGroupRepo = new BankGroupRepo(db);
				BankGroup bankGroup = bankGroupView.ToBankGroup();
				bankGroup = bankGroupRepo.CreateBankGroup(bankGroup);
				base.LogTransactionCreate<BankGroup>(bankGroup);
				UnitOfWork.Commit();
				return bankGroup.ToBankGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BankGroupItemView UpdateBankGroup(BankGroupItemView bankGroupView)
		{
			try
			{
				IBankGroupRepo bankGroupRepo = new BankGroupRepo(db);
				BankGroup inputBankGroup = bankGroupView.ToBankGroup();
				BankGroup dbBankGroup = bankGroupRepo.Find(inputBankGroup.BankGroupGUID);
				dbBankGroup = bankGroupRepo.UpdateBankGroup(dbBankGroup, inputBankGroup);
				base.LogTransactionUpdate<BankGroup>(GetOriginalValues<BankGroup>(dbBankGroup), dbBankGroup);
				UnitOfWork.Commit();
				return dbBankGroup.ToBankGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBankGroup(string item)
		{
			try
			{
				IBankGroupRepo bankGroupRepo = new BankGroupRepo(db);
				Guid bankGroupGUID = new Guid(item);
				BankGroup bankGroup = bankGroupRepo.Find(bankGroupGUID);
				bankGroupRepo.Remove(bankGroup);
				base.LogTransactionDelete<BankGroup>(bankGroup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
