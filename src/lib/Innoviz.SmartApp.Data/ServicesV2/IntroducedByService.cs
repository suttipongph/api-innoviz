using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IIntroducedByService
	{

		IntroducedByItemView GetIntroducedByById(string id);
		IntroducedByItemView CreateIntroducedBy(IntroducedByItemView introducedByView);
		IntroducedByItemView UpdateIntroducedBy(IntroducedByItemView introducedByView);
		bool DeleteIntroducedBy(string id);
	}
	public class IntroducedByService : SmartAppService, IIntroducedByService
	{
		public IntroducedByService(SmartAppDbContext context) : base(context) { }
		public IntroducedByService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public IntroducedByService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntroducedByService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public IntroducedByService() : base() { }

		public IntroducedByItemView GetIntroducedByById(string id)
		{
			try
			{
				IIntroducedByRepo introducedByRepo = new IntroducedByRepo(db);
				return introducedByRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntroducedByItemView CreateIntroducedBy(IntroducedByItemView introducedByView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				introducedByView = accessLevelService.AssignOwnerBU(introducedByView);
				IIntroducedByRepo introducedByRepo = new IntroducedByRepo(db);
				IntroducedBy introducedBy = introducedByView.ToIntroducedBy();
				introducedBy = introducedByRepo.CreateIntroducedBy(introducedBy);
				base.LogTransactionCreate<IntroducedBy>(introducedBy);
				UnitOfWork.Commit();
				return introducedBy.ToIntroducedByItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntroducedByItemView UpdateIntroducedBy(IntroducedByItemView introducedByView)
		{
			try
			{
				IIntroducedByRepo introducedByRepo = new IntroducedByRepo(db);
				IntroducedBy inputIntroducedBy = introducedByView.ToIntroducedBy();
				IntroducedBy dbIntroducedBy = introducedByRepo.Find(inputIntroducedBy.IntroducedByGUID);
				dbIntroducedBy = introducedByRepo.UpdateIntroducedBy(dbIntroducedBy, inputIntroducedBy);
				base.LogTransactionUpdate<IntroducedBy>(GetOriginalValues<IntroducedBy>(dbIntroducedBy), dbIntroducedBy);
				UnitOfWork.Commit();
				return dbIntroducedBy.ToIntroducedByItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteIntroducedBy(string item)
		{
			try
			{
				IIntroducedByRepo introducedByRepo = new IntroducedByRepo(db);
				Guid introducedByGUID = new Guid(item);
				IntroducedBy introducedBy = introducedByRepo.Find(introducedByGUID);
				introducedByRepo.Remove(introducedBy);
				base.LogTransactionDelete<IntroducedBy>(introducedBy);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
