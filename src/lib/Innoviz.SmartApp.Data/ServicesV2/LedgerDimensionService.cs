using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ILedgerDimensionService
	{

		LedgerDimensionItemView GetLedgerDimensionById(string id);
		LedgerDimensionItemView CreateLedgerDimension(LedgerDimensionItemView ledgerDimensionView);
		LedgerDimensionItemView UpdateLedgerDimension(LedgerDimensionItemView ledgerDimensionView);
		bool DeleteLedgerDimension(string id);
	}
	public class LedgerDimensionService : SmartAppService, ILedgerDimensionService
	{
		public LedgerDimensionService(SmartAppDbContext context) : base(context) { }
		public LedgerDimensionService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public LedgerDimensionService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LedgerDimensionService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public LedgerDimensionService() : base() { }

		public LedgerDimensionItemView GetLedgerDimensionById(string id)
		{
			try
			{
				ILedgerDimensionRepo ledgerDimensionRepo = new LedgerDimensionRepo(db);
				return ledgerDimensionRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerDimensionItemView CreateLedgerDimension(LedgerDimensionItemView ledgerDimensionView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ledgerDimensionView = accessLevelService.AssignOwnerBU(ledgerDimensionView);
				ILedgerDimensionRepo ledgerDimensionRepo = new LedgerDimensionRepo(db);
				LedgerDimension ledgerDimension = ledgerDimensionView.ToLedgerDimension();
				ledgerDimension = ledgerDimensionRepo.CreateLedgerDimension(ledgerDimension);
				base.LogTransactionCreate<LedgerDimension>(ledgerDimension);
				UnitOfWork.Commit();
				return ledgerDimension.ToLedgerDimensionItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerDimensionItemView UpdateLedgerDimension(LedgerDimensionItemView ledgerDimensionView)
		{
			try
			{
				ILedgerDimensionRepo ledgerDimensionRepo = new LedgerDimensionRepo(db);
				LedgerDimension inputLedgerDimension = ledgerDimensionView.ToLedgerDimension();
				LedgerDimension dbLedgerDimension = ledgerDimensionRepo.Find(inputLedgerDimension.LedgerDimensionGUID);
				dbLedgerDimension = ledgerDimensionRepo.UpdateLedgerDimension(dbLedgerDimension, inputLedgerDimension);
				base.LogTransactionUpdate<LedgerDimension>(GetOriginalValues<LedgerDimension>(dbLedgerDimension), dbLedgerDimension);
				UnitOfWork.Commit();
				return dbLedgerDimension.ToLedgerDimensionItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteLedgerDimension(string item)
		{
			try
			{
				ILedgerDimensionRepo ledgerDimensionRepo = new LedgerDimensionRepo(db);
				Guid ledgerDimensionGUID = new Guid(item);
				LedgerDimension ledgerDimension = ledgerDimensionRepo.Find(ledgerDimensionGUID);
				ledgerDimensionRepo.Remove(ledgerDimension);
				base.LogTransactionDelete<LedgerDimension>(ledgerDimension);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
