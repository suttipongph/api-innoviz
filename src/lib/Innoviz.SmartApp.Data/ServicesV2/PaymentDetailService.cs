using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IPaymentDetailService
	{

		PaymentDetailItemView GetPaymentDetailById(string id);
		PaymentDetailItemView CreatePaymentDetail(PaymentDetailItemView paymentDetailView);
		PaymentDetailItemView UpdatePaymentDetail(PaymentDetailItemView paymentDetailView);
		bool DeletePaymentDetail(string id);
		PaymentDetailItemView GetPaymentDetailInitialData(string refId, string refGUID, RefType refType);
		void CreatePaymentDetailCollection(IEnumerable<PaymentDetailItemView> PaymentDetailItemView);
	}
	public class PaymentDetailService : SmartAppService, IPaymentDetailService
	{
		public PaymentDetailService(SmartAppDbContext context) : base(context) { }
		public PaymentDetailService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public PaymentDetailService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public PaymentDetailService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public PaymentDetailService() : base() { }

		public PaymentDetailItemView GetPaymentDetailById(string id)
		{
			try
			{
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				return paymentDetailRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PaymentDetailItemView CreatePaymentDetail(PaymentDetailItemView paymentDetailView)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				paymentDetailView = accessLevelService.AssignOwnerBU(paymentDetailView);
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				PaymentDetail paymentDetail = paymentDetailView.ToPaymentDetail();
				paymentDetail = paymentDetailRepo.CreatePaymentDetail(paymentDetail);
				customerRefundTableService.UpdateCustomerRefundTablePaymentAmount(paymentDetailView);
				base.LogTransactionCreate<PaymentDetail>(paymentDetail);
				UnitOfWork.Commit();
				return paymentDetail.ToPaymentDetailItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PaymentDetailItemView UpdatePaymentDetail(PaymentDetailItemView paymentDetailView)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				PaymentDetail inputPaymentDetail = paymentDetailView.ToPaymentDetail();
				PaymentDetail dbPaymentDetail = paymentDetailRepo.Find(inputPaymentDetail.PaymentDetailGUID);
				dbPaymentDetail = paymentDetailRepo.UpdatePaymentDetail(dbPaymentDetail, inputPaymentDetail);
				customerRefundTableService.UpdateCustomerRefundTablePaymentAmount(paymentDetailView);
				base.LogTransactionUpdate<PaymentDetail>(GetOriginalValues<PaymentDetail>(dbPaymentDetail), dbPaymentDetail);
				UnitOfWork.Commit();
				return dbPaymentDetail.ToPaymentDetailItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeletePaymentDetail(string item)
		{
			try
			{
				ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				Guid paymentDetailGUID = new Guid(item);
				PaymentDetail paymentDetail = paymentDetailRepo.Find(paymentDetailGUID);
				paymentDetailRepo.Remove(paymentDetail);
				customerRefundTableService.UpdateCustomerRefundTablePaymentAmountOnRemove(paymentDetail);
				base.LogTransactionDelete<PaymentDetail>(paymentDetail);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PaymentDetailItemView GetPaymentDetailInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new PaymentDetailItemView
				{
					PaymentDetailGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Migration
		public void CreatePaymentDetailCollection(IEnumerable<PaymentDetailItemView> PaymentDetailItemView)
		{
			try
			{
				if (PaymentDetailItemView.Any())
				{
					IPaymentDetailRepo PaymentDetailRepo = new PaymentDetailRepo(db);
					List<PaymentDetail> PaymentDetails = PaymentDetailItemView.ToPaymentDetail().ToList();
					PaymentDetailRepo.ValidateAdd(PaymentDetails);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(PaymentDetails, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Migration


	}
}
