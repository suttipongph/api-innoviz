using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IInvoiceTypeService
    {

        InvoiceTypeItemView GetInvoiceTypeById(string id);
        InvoiceTypeItemView CreateInvoiceType(InvoiceTypeItemView invoiceTypeView);
        InvoiceTypeItemView UpdateInvoiceType(InvoiceTypeItemView invoiceTypeView);
        bool DeleteInvoiceType(string id);
        IEnumerable<SelectItem<InvoiceTypeItemView>> GetInvoiceTypeByPaymentDetail(SearchParameter search);
        IEnumerable<SelectItem<InvoiceTypeItemView>> GetInvoiceTypeByReceiptTempTableNoneProductType(SearchParameter search);
        IEnumerable<SelectItem<InvoiceTypeItemView>> GetInvoiceTypeByReceiptTempTableProductType(SearchParameter search);
        IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItemInvoiceTypeTableByProduct(SearchParameter search);
    }
    public class InvoiceTypeService : SmartAppService, IInvoiceTypeService
    {
        public InvoiceTypeService(SmartAppDbContext context) : base(context) { }
        public InvoiceTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public InvoiceTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public InvoiceTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public InvoiceTypeService() : base() { }

        public InvoiceTypeItemView GetInvoiceTypeById(string id)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                return invoiceTypeRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceTypeItemView CreateInvoiceType(InvoiceTypeItemView invoiceTypeView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                invoiceTypeView = accessLevelService.AssignOwnerBU(invoiceTypeView);
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                InvoiceType invoiceType = invoiceTypeView.ToInvoiceType();
                invoiceType = invoiceTypeRepo.CreateInvoiceType(invoiceType);
                base.LogTransactionCreate<InvoiceType>(invoiceType);
                UnitOfWork.Commit();
                return invoiceType.ToInvoiceTypeItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceTypeItemView UpdateInvoiceType(InvoiceTypeItemView invoiceTypeView)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                InvoiceType inputInvoiceType = invoiceTypeView.ToInvoiceType();
                InvoiceType dbInvoiceType = invoiceTypeRepo.Find(inputInvoiceType.InvoiceTypeGUID);
                dbInvoiceType = invoiceTypeRepo.UpdateInvoiceType(dbInvoiceType, inputInvoiceType);
                base.LogTransactionUpdate<InvoiceType>(GetOriginalValues<InvoiceType>(dbInvoiceType), dbInvoiceType);
                UnitOfWork.Commit();
                return dbInvoiceType.ToInvoiceTypeItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteInvoiceType(string item)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                Guid invoiceTypeGUID = new Guid(item);
                InvoiceType invoiceType = invoiceTypeRepo.Find(invoiceTypeGUID);
                invoiceTypeRepo.Remove(invoiceType);
                base.LogTransactionDelete<InvoiceType>(invoiceType);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<InvoiceTypeItemView>> GetInvoiceTypeByPaymentDetail(SearchParameter search)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                SearchCondition searchCondition = SearchConditionService.GetInvoiceTypeBySuspenseInvoiceType(((int)SuspenseInvoiceType.SuspenseAccount).ToString().FirstToList());
                search.Conditions.Add(searchCondition);
                return invoiceTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<InvoiceTypeItemView>> GetInvoiceTypeByReceiptTempTableNoneProductType(SearchParameter search)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                SearchCondition searchCondition1 = SearchConditionService.GetInvoiceTypeBySuspenseInvoiceType(((int)SuspenseInvoiceType.SuspenseAccount).ToString().FirstToList());
                SearchCondition searchCondition2 = SearchConditionService.GetInvoiceTypeByDirectReceiptCondition(true);
                search.Conditions.Add(searchCondition1);
                search.Conditions.Add(searchCondition2);
                return invoiceTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<InvoiceTypeItemView>> GetInvoiceTypeByReceiptTempTableProductType(SearchParameter search)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                search = search.GetParentCondition(InvoiceTypeCondition.ProductType);
                SearchCondition searchCondition = SearchConditionService.GetInvoiceTypeByDirectReceiptCondition(true);
                search.Conditions.Add(searchCondition);
                return invoiceTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItemInvoiceTypeTableByProduct(SearchParameter search)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                SearchCondition searchCondition = SearchConditionService.GetInvoiceRevenueTypeByProductTypeCondition(search.Conditions.Select(s => s.Value).ToList());
                search.Conditions = searchCondition.FirstToList();
                return invoiceTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

    }
}
