using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IJointVentureTransService
    {

        JointVentureTransItemView GetJointVentureTransById(string id);
        JointVentureTransItemView CreateJointVentureTrans(JointVentureTransItemView jointVentureTransView);
        JointVentureTrans CreateJointVentureTrans(JointVentureTrans jointVentureTrans);
        JointVentureTransItemView UpdateJointVentureTrans(JointVentureTransItemView jointVentureTransView);
        bool DeleteJointVentureTrans(string id);
        JointVentureTransItemView GetJointVentureTransInitialData(string refId, string refGUID, RefType refType);
        IEnumerable<JointVentureTrans> CopyJointVentureTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType);
        void CreateJointVentureTransCollection(List<JointVentureTransItemView> jointVentureTransItemViews);
        #region Bookmark
        MainAgreementJVBookmarkView GetMainAgreementJVBookmarkValue(Guid refGuid);
        #endregion
    }
    public class JointVentureTransService : SmartAppService, IJointVentureTransService
    {
        public JointVentureTransService(SmartAppDbContext context) : base(context) { }
        public JointVentureTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public JointVentureTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public JointVentureTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public JointVentureTransService() : base() { }

        public JointVentureTransItemView GetJointVentureTransById(string id)
        {
            try
            {
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                return jointVentureTransRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JointVentureTransItemView CreateJointVentureTrans(JointVentureTransItemView jointVentureTransView)
        {
            try
            {
                JointVentureTrans jointVentureTrans = jointVentureTransView.ToJointVentureTrans();
                jointVentureTrans = CreateJointVentureTrans(jointVentureTrans);
                UnitOfWork.Commit();
                return jointVentureTrans.ToJointVentureTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JointVentureTrans CreateJointVentureTrans(JointVentureTrans jointVentureTrans)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                jointVentureTrans = accessLevelService.AssignOwnerBU(jointVentureTrans);
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                jointVentureTrans = jointVentureTransRepo.CreateJointVentureTrans(jointVentureTrans);
                base.LogTransactionCreate<JointVentureTrans>(jointVentureTrans);
                return jointVentureTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JointVentureTransItemView UpdateJointVentureTrans(JointVentureTransItemView jointVentureTransView)
        {
            try
            {
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                JointVentureTrans inputJointVentureTrans = jointVentureTransView.ToJointVentureTrans();
                JointVentureTrans dbJointVentureTrans = jointVentureTransRepo.Find(inputJointVentureTrans.JointVentureTransGUID);
                dbJointVentureTrans = jointVentureTransRepo.UpdateJointVentureTrans(dbJointVentureTrans, inputJointVentureTrans);
                base.LogTransactionUpdate<JointVentureTrans>(GetOriginalValues<JointVentureTrans>(dbJointVentureTrans), dbJointVentureTrans);
                UnitOfWork.Commit();
                return dbJointVentureTrans.ToJointVentureTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteJointVentureTrans(string item)
        {
            try
            {
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                Guid jointVentureTransGUID = new Guid(item);
                JointVentureTrans jointVentureTrans = jointVentureTransRepo.Find(jointVentureTransGUID);
                jointVentureTransRepo.Remove(jointVentureTrans);
                base.LogTransactionDelete<JointVentureTrans>(jointVentureTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public JointVentureTransItemView GetJointVentureTransInitialData(string refId, string refGUID, RefType refType)
        {
            try
            {
                return new JointVentureTransItemView
                {
                    JointVentureTransGUID = new Guid().GuidNullToString(),
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<JointVentureTrans> CopyJointVentureTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType)
        {
            try
            {
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                IEnumerable<JointVentureTrans> originJointVentureTrans = jointVentureTransRepo.GetJointVentureTransByReferenceTableNoTracking(fromRefGUID, fromRefType);
                List<JointVentureTrans> copyJointVentureTrans = new List<JointVentureTrans>();
                if (originJointVentureTrans.Count() != 0)
                {
                    foreach (JointVentureTrans item in originJointVentureTrans)
                    {
                        copyJointVentureTrans.Add(new JointVentureTrans
                        {
                            JointVentureTransGUID = Guid.NewGuid(),
                            Owner = item.Owner,
                            OwnerBusinessUnitGUID = item.OwnerBusinessUnitGUID,
                            CompanyGUID = item.CompanyGUID,
                            RefType = toRefType,
                            RefGUID = toRefGUID,
                            Ordering = item.Ordering,
                            Name = item.Name,
                            AuthorizedPersonTypeGUID = item.AuthorizedPersonTypeGUID,
                            OperatedBy = item.OperatedBy,
                            Address = item.Address,
                            Remark = item.Remark
                        });
                    }
                }
                return copyJointVentureTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateJointVentureTransCollection(List<JointVentureTransItemView> jointVentureTransItemViews)
        {
            try
            {
                if (jointVentureTransItemViews.Any())
                {
                    IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                    List<JointVentureTrans> jointVentureTrans = jointVentureTransItemViews.ToJointVentureTrans().ToList();
                    jointVentureTransRepo.ValidateAdd(jointVentureTrans);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(jointVentureTrans, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #region Bookmark
        public MainAgreementJVBookmarkView GetMainAgreementJVBookmarkValue(Guid refGuid)
        {
            try
            {
                IJointVentureTransRepo jointVentureTransRepo = new JointVentureTransRepo(db);
                return jointVentureTransRepo.GetMainAgreementJVBookmarkValue(refGuid);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
