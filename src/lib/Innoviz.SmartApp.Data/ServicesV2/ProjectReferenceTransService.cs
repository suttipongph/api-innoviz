using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IProjectReferenceTransService
	{

		ProjectReferenceTransItemView GetProjectReferenceTransById(string id);
		ProjectReferenceTransItemView CreateProjectReferenceTrans(ProjectReferenceTransItemView projectReferenceTransView);
		ProjectReferenceTransItemView UpdateProjectReferenceTrans(ProjectReferenceTransItemView projectReferenceTransView);
		bool DeleteProjectReferenceTrans(string id);
		ProjectReferenceTransItemView GetProjectReferenceTransInitialData(string refId, string refGUID, RefType refType);
		void CreateProjectReferenceTransCollection(List<ProjectReferenceTransItemView> projectReferenceTransItemViews);
	}
	public class ProjectReferenceTransService : SmartAppService, IProjectReferenceTransService
	{
		public ProjectReferenceTransService(SmartAppDbContext context) : base(context) { }
		public ProjectReferenceTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ProjectReferenceTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProjectReferenceTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ProjectReferenceTransService() : base() { }

		public ProjectReferenceTransItemView GetProjectReferenceTransById(string id)
		{
			try
			{
				IProjectReferenceTransRepo projectReferenceTransRepo = new ProjectReferenceTransRepo(db);
				return projectReferenceTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProjectReferenceTransItemView CreateProjectReferenceTrans(ProjectReferenceTransItemView projectReferenceTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				projectReferenceTransView = accessLevelService.AssignOwnerBU(projectReferenceTransView);
				IProjectReferenceTransRepo projectReferenceTransRepo = new ProjectReferenceTransRepo(db);
				ProjectReferenceTrans projectReferenceTrans = projectReferenceTransView.ToProjectReferenceTrans();
				projectReferenceTrans = projectReferenceTransRepo.CreateProjectReferenceTrans(projectReferenceTrans);
				base.LogTransactionCreate<ProjectReferenceTrans>(projectReferenceTrans);
				UnitOfWork.Commit();
				return projectReferenceTrans.ToProjectReferenceTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProjectReferenceTransItemView UpdateProjectReferenceTrans(ProjectReferenceTransItemView projectReferenceTransView)
		{
			try
			{
				IProjectReferenceTransRepo projectReferenceTransRepo = new ProjectReferenceTransRepo(db);
				ProjectReferenceTrans inputProjectReferenceTrans = projectReferenceTransView.ToProjectReferenceTrans();
				ProjectReferenceTrans dbProjectReferenceTrans = projectReferenceTransRepo.Find(inputProjectReferenceTrans.ProjectReferenceTransGUID);
				dbProjectReferenceTrans = projectReferenceTransRepo.UpdateProjectReferenceTrans(dbProjectReferenceTrans, inputProjectReferenceTrans);
				base.LogTransactionUpdate<ProjectReferenceTrans>(GetOriginalValues<ProjectReferenceTrans>(dbProjectReferenceTrans), dbProjectReferenceTrans);
				UnitOfWork.Commit();
				return dbProjectReferenceTrans.ToProjectReferenceTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteProjectReferenceTrans(string item)
		{
			try
			{
				IProjectReferenceTransRepo projectReferenceTransRepo = new ProjectReferenceTransRepo(db);
				Guid projectReferenceTransGUID = new Guid(item);
				ProjectReferenceTrans projectReferenceTrans = projectReferenceTransRepo.Find(projectReferenceTransGUID);
				projectReferenceTransRepo.Remove(projectReferenceTrans);
				base.LogTransactionDelete<ProjectReferenceTrans>(projectReferenceTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProjectReferenceTransItemView GetProjectReferenceTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new ProjectReferenceTransItemView
				{
					ProjectReferenceTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region migration
		public void CreateProjectReferenceTransCollection(List<ProjectReferenceTransItemView> projectReferenceTransItemViews)
		{
			try
			{
				if (projectReferenceTransItemViews.Any())
				{
					IProjectReferenceTransRepo projectReferenceTransRepo = new ProjectReferenceTransRepo(db);
					List<ProjectReferenceTrans> projectReferenceTrans = projectReferenceTransItemViews.ToProjectReferenceTrans().ToList();
					projectReferenceTransRepo.ValidateAdd(projectReferenceTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(projectReferenceTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion

	}
}
