using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ICustomerAgingService
    {
        List<CustomerAgingViewMap> GetCustomerAging(ReportAgingView model);
        List<CustomerAgingView> GetReportAging(ReportAgingView model);
    }
    public class CustomerAgingService : SmartAppService, ICustomerAgingService
    {
        public CustomerAgingService(SmartAppDbContext context) : base(context) { }
        public CustomerAgingService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public CustomerAgingService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CustomerAgingService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public CustomerAgingService() : base() { }

        public List<CustomerAgingViewMap> GetCustomerAging(ReportAgingView parameter)
        {
            try
            {
                IAgingReportSetupRepo agingReportSetupRepo = new AgingReportSetupRepo(db);
                AgingReportSetupView agingReportSetupView = agingReportSetupRepo.GetAgingReportSetupView(parameter.CompanyGUID.StringToGuid());

                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                List<CustomerAgingViewMap> customerAgingViews = invoiceTableRepo.GetListForCustomerAging(parameter);
                customerAgingViews = customerAgingViews.OrderBy(c => c.CustomerId).ThenBy(b => b.BuyerId).ThenBy(d => d.Days).Select(s =>
                {
                    s.NetARBucket1 = (s.Days >= agingReportSetupView.From1 && s.Days <= agingReportSetupView.To1) ? s.NetAR : 0;
                    s.NetARBucket2 = (s.Days >= agingReportSetupView.From2 && s.Days <= agingReportSetupView.To2) ? s.NetAR : 0;
                    s.NetARBucket3 = (s.Days >= agingReportSetupView.From3 && s.Days <= agingReportSetupView.To3) ? s.NetAR : 0;
                    s.NetARBucket4 = (s.Days >= agingReportSetupView.From4 && s.Days <= agingReportSetupView.To4) ? s.NetAR : 0;
                    s.NetARBucket5 = (s.Days >= agingReportSetupView.From5 && s.Days <= agingReportSetupView.To5) ? s.NetAR : 0;
                    s.NetARBucket6 = (s.Days >= agingReportSetupView.From6 && s.Days <= agingReportSetupView.To6) ? s.NetAR : 0;
                    return s;
                }).ToList();

                return customerAgingViews;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public List<CustomerAgingView> GetReportAging(ReportAgingView parameter)
        {
            try
            {
                List<CustomerAgingView> customerAgingViews = GetCustomerAging(parameter).AsQueryable().ToMaps<CustomerAgingViewMap, CustomerAgingView>();
                return customerAgingViews;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
