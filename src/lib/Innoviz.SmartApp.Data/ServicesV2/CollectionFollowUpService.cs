using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICollectionFollowUpService
	{

		CollectionFollowUpItemView GetCollectionFollowUpById(string id);
		CollectionFollowUpItemView CreateCollectionFollowUp(CollectionFollowUpItemView collectionFollowUpView);
		CollectionFollowUpItemView UpdateCollectionFollowUp(CollectionFollowUpItemView collectionFollowUpView);
		bool DeleteCollectionFollowUp(string id);
		CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryPurcaseLineOutstanding(Guid guid);
		CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryWithdrawalLineOutstand(Guid guid);
		MemoTransItemView GetMemoTransInitialData(string refGUID);
		#region Dropdown
		IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItemCollectionFollowUpRefAndPaymentType(SearchParameter search);

		#endregion
	}
	public class CollectionFollowUpService : SmartAppService, ICollectionFollowUpService
	{
		public CollectionFollowUpService(SmartAppDbContext context) : base(context) { }
		public CollectionFollowUpService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CollectionFollowUpService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CollectionFollowUpService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CollectionFollowUpService() : base() { }

		public CollectionFollowUpItemView GetCollectionFollowUpById(string id)
		{
			try
			{
				ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
				return collectionFollowUpRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionFollowUpItemView CreateCollectionFollowUp(CollectionFollowUpItemView collectionFollowUpView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				collectionFollowUpView = accessLevelService.AssignOwnerBU(collectionFollowUpView);
				ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
				CollectionFollowUp collectionFollowUp = collectionFollowUpView.ToCollectionFollowUp();
				collectionFollowUp = collectionFollowUpRepo.CreateCollectionFollowUp(collectionFollowUp);
				base.LogTransactionCreate<CollectionFollowUp>(collectionFollowUp);
				UnitOfWork.Commit();
				return collectionFollowUp.ToCollectionFollowUpItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionFollowUpItemView UpdateCollectionFollowUp(CollectionFollowUpItemView collectionFollowUpView)
		{
			try
			{
				ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
				CollectionFollowUp inputCollectionFollowUp = collectionFollowUpView.ToCollectionFollowUp();
				CollectionFollowUp dbCollectionFollowUp = collectionFollowUpRepo.Find(inputCollectionFollowUp.CollectionFollowUpGUID);
				dbCollectionFollowUp = collectionFollowUpRepo.UpdateCollectionFollowUp(dbCollectionFollowUp, inputCollectionFollowUp);
				base.LogTransactionUpdate<CollectionFollowUp>(GetOriginalValues<CollectionFollowUp>(dbCollectionFollowUp), dbCollectionFollowUp);
				UnitOfWork.Commit();
				return dbCollectionFollowUp.ToCollectionFollowUpItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCollectionFollowUp(string item)
		{
			try
			{
				ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
				Guid collectionFollowUpGUID = new Guid(item);
				CollectionFollowUp collectionFollowUp = collectionFollowUpRepo.Find(collectionFollowUpGUID);
				collectionFollowUpRepo.Remove(collectionFollowUp);
				base.LogTransactionDelete<CollectionFollowUp>(collectionFollowUp);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryPurcaseLineOutstanding(Guid guid)
		{
			try
			{
				ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
				return collectionFollowUpRepo.GetCollectionFollowUpInitialDataByInquiryPurcaseLineOutstanding(guid);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionFollowUpItemView GetCollectionFollowUpInitialDataByInquiryWithdrawalLineOutstand(Guid guid)
		{
			try
			{
				ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
				return collectionFollowUpRepo.GetCollectionFollowUpInitialDataByInquiryWithdrawalLineOutstand(guid);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MemoTransItemView GetMemoTransInitialData(string refGUID)
		{
			try
			{
				IMemoTransService memoTransService = new MemoTransService(db);
				ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
				CollectionFollowUp collectionFollowUp = collectionFollowUpRepo.GetCollectionFollowUpByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
				string refId = SmartAppUtil.GetDropDownLabel(collectionFollowUp.CollectionDate.DateToString(), collectionFollowUp.Description);
				return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.CollectionFollowUp);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Dropdown
		public IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItemCollectionFollowUpRefAndPaymentType(SearchParameter search)
		{
			try
			{
				ISysDropDownService sysDropDownService = new SysDropDownService(db);
				search.GetParentCondition(new string[] { JobChequeCondition.RefGUID, JobChequeCondition.PaymentType, JobChequeCondition.CollectionFollowUpResult });
				return sysDropDownService.GetDropDownItemCollectionFollowUp(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

	}
}
