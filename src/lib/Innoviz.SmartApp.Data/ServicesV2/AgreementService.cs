﻿using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IAgreementService
    {
        #region function
        #region manage agreement
        bool GetManageAgreementValidation(ManageAgreementView view);
        ManageAgreementView ManageAgreement(ManageAgreementView view);
        ManageAgreementView GetManageAgreementByRefType(ManageAgreementView view);
        ManageAssignAgreementValidationResult GetManageAssignmentAgreementValidation(ManageAgreementView view);
        #endregion

        #endregion
    }
    public class AgreementService : SmartAppService, IAgreementService
    {
        public AgreementService(SmartAppDbContext context) : base(context) { }
        public AgreementService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public AgreementService() : base() { }
        #region function
        #region manage agreement
        public bool GetManageAgreementValidation(ManageAgreementView view)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                #region reftype
                Guid docStatusGuid = new Guid();
                if (view.RefType == (int)RefType.AssignmentAgreement)
                {
                    IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                    docStatusGuid = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(view.RefGUID.StringToGuid()).DocumentStatusGUID.Value;
                }
                else if (view.RefType == (int)RefType.MainAgreement)
                {
                    IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                    docStatusGuid = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(view.RefGUID.StringToGuid()).DocumentStatusGUID;
                }
                else if (view.RefType == (int)RefType.GuarantorAgreement)
                {
                    IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                    docStatusGuid = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(view.RefGUID.StringToGuid()).DocumentStatusGUID.Value;
                }
                else if (view.RefType == (int)RefType.BusinessCollateralAgreement)
                {
                    IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                    docStatusGuid = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(view.RefGUID.StringToGuid()).DocumentStatusGUID;
                }
                #endregion
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(docStatusGuid);

                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    if (int.Parse(documentStatus.StatusCode) != (int)DocumentStatusCode.Draft)
                    {
                        ex.AddData("ERROR.90012", new string[] { view.Label });
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Send)
                {
                    if (int.Parse(documentStatus.StatusCode) != (int)DocumentStatusCode.Posted)
                    {
                        ex.AddData("ERROR.90013", new string[] { view.Label });
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    if (int.Parse(documentStatus.StatusCode) != (int)DocumentStatusCode.Sent)
                    {
                        ex.AddData("ERROR.90014", new string[] { view.Label });
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Close)
                {
                    if (int.Parse(documentStatus.StatusCode) != (int)DocumentStatusCode.Signed)
                    {
                        ex.AddData("ERROR.90015", new string[] { view.Label });
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Cancel)
                {
                    if (int.Parse(documentStatus.StatusCode) > (int)DocumentStatusCode.Sent)
                    {
                        ex.AddData("ERROR.90016", new string[] { view.Label });
                    }
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public ManageAssignAgreementValidationResult GetManageAssignmentAgreementValidation(ManageAgreementView view)
        {
            try
            {
                ManageAssignAgreementValidationResult result = new ManageAssignAgreementValidationResult();
                result.RefGUID = view.RefGUID;
                result.RefType = view.RefType;
                result.IsValid = GetManageAgreementValidation(view);

                IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                List<AssignmentAgreementLine> assignmentLines = assignmentAgreementLineRepo.GetAssignmentAgreementLineByAssignmentAgreementTableGuidNoTracking(view.RefGUID.StringToGuid());
                result.HasLine = assignmentLines.Any();
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private bool GetAssignmentAgreementValidation(ManageAgreementView view)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                bool isManual = numberSequenceService.IsManualByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.AssignmentAgreement);
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.AssignmentAgreement, view.RefGUID.StringToGuid());
                DocumentStatus completedBookmark = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());   
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    if (string.IsNullOrEmpty(view.AgreementId) && isManual)
                    {
                        ex.AddData("ERROR.90058", new string[] { "LABEL.ASSIGNMENT_AGREEMENT_ID" });
                    }
                    bool isDuplicateAssignmentAgreementId = assignmentAgreementTableRepo.IsDuplicateAssignmentAgreementId(view.RefGUID.StringToGuid(), view.AgreementId);
                    if (isDuplicateAssignmentAgreementId)
                    {
                        ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.ASSIGNMENT_AGREEMENT_ID" });
                    }
                }
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    if (bookmarkDocumentTrans.ToList().Any(a => a.DocumentStatusGUID != completedBookmark.DocumentStatusGUID))
                    {
                        ex.AddData("ERROR.90020");
                    }
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private bool GetMainAgreementValidation(ManageAgreementView view)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetMainAgreementNumberSeqGUID(db.GetCompanyFilter().StringToGuid(), view.ProductType);
                bool isManual = numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.MainAgreement, view.RefGUID.StringToGuid());
                DocumentStatus completedBookmark = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    if (string.IsNullOrEmpty(view.AgreementId) && isManual)
                    {
                        ex.AddData("ERROR.90058", new string[] { view.Label });
                    }
                    bool isDuplicateMainAgreementId = mainAgreementTableRepo.IsDuplicateMainAgreementId(view.RefGUID.StringToGuid(), view.AgreementId);
                    if (isDuplicateMainAgreementId)
                    {
                        ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.MAIN_AGREEMENT_ID" });
                    }
                }
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    if (bookmarkDocumentTrans.ToList().Any(a => a.DocumentStatusGUID != completedBookmark.DocumentStatusGUID))
                    {
                        ex.AddData("ERROR.90020");
                    }
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private bool GetGuarantorAgreementValidation(ManageAgreementView view)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IGuarantorAgreementLineRepo guarantorAgreementLineRepo = new GuarantorAgreementLineRepo(db);
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetGuarantorAgreementNumberSeqGUID(db.GetCompanyFilter().StringToGuid(), view.ProductType);
                bool isManual = numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
                DocumentStatus completedBookmark = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                bool isNotGuarantorAgreementLine = ConditionService.IsEqualZero(guarantorAgreementLineRepo.GetLineListByGuarantorAgreement(view.RefGUID.StringToGuid()).Count());
                
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    if (string.IsNullOrEmpty(view.AgreementId) && isManual)
                    {
                        ex.AddData("ERROR.90058", new string[] { view.Label });
                    }
                    if (isNotGuarantorAgreementLine)
                    {
                        ex.AddData("ERROR.90127");
                    }
                    bool isDuplicateGuarantorAgreementId = guarantorAgreementTableRepo.IsDuplicateGuarantorAgreementId(view.RefGUID.StringToGuid(), view.AgreementId);
                    if (isDuplicateGuarantorAgreementId)
                    {
                        ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.GUARANTOR_AGREEMENT_ID" });
                    }
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private bool GetBusinessCollateralAgreementValidation(ManageAgreementView view)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                bool isManual = numberSequenceService.IsManualByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.BusinessCollateralAgreement);
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.BusinessCollateralAgreement, view.RefGUID.StringToGuid());
                DocumentStatus completedBookmark = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    if (string.IsNullOrEmpty(view.AgreementId) && isManual)
                    {
                        ex.AddData("ERROR.90058", new string[] { view.Label });
                    }
                    bool isDuplicateBusinessCollateralAgmId = businessCollateralAgmTableRepo.IsDuplicateBusinessCollateralAgmId(view.RefGUID.StringToGuid(), view.AgreementId);
                    if(isDuplicateBusinessCollateralAgmId)
                    {
                        ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.BUSINESS_COLLATERAL_AGREEMENT_ID" });
                    }
                }
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    if (bookmarkDocumentTrans.ToList().Any(a => a.DocumentStatusGUID != completedBookmark.DocumentStatusGUID))
                    {
                        ex.AddData("ERROR.90020");
                    }
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public ManageAgreementView ManageAgreement(ManageAgreementView view)
        {
            try
            {
                IInvoiceService invoiceService = new InvoiceService(db);
                ICustTransService custTransService = new CustTransService(db);
                ICreditAppTransService creditAppTransService = new CreditAppTransService(db);
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                IProcessTransService processTransService = new ProcessTransService(db);
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                NotificationResponse success = new NotificationResponse();
                PostInvoiceResultView postInvoiceResultView = new PostInvoiceResultView();
                List<string> labels = new List<string>();
                bool isValid = false;
                #region action
                isValid = GetManageAgreementValidation(view);
                #endregion
                #region reftype
                if (view.RefType == (int)RefType.AssignmentAgreement)
                {
                    isValid = GetAssignmentAgreementValidation(view);
                }
                else if (view.RefType == (int)RefType.MainAgreement)
                {
                    isValid = GetMainAgreementValidation(view);
                }
                else if (view.RefType == (int)RefType.GuarantorAgreement)
                {
                    isValid = GetGuarantorAgreementValidation(view);
                }
                else if (view.RefType == (int)RefType.BusinessCollateralAgreement)
                {
                    isValid = GetBusinessCollateralAgreementValidation(view);
                }
                #endregion
                if (isValid)
                {
                    AssignmentAgreementManageAgreementView assignmentAgreementManageAgreementView = new AssignmentAgreementManageAgreementView();
                    MainAgreementManageAgreementView mainAgreementManageAgreementView = new MainAgreementManageAgreementView();
                    GuarantorAgreementManageAgreementView guarantorAgreementManageAgreementView = new GuarantorAgreementManageAgreementView();
                    BusinessCollateralAgreementManageAgreementView businessCollateralAgreementManageAgreementView = new BusinessCollateralAgreementManageAgreementView();
                    if (view.RefType == (int)RefType.AssignmentAgreement)
                    {
                        IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                        assignmentAgreementManageAgreementView = UpdateAssignmentAgreementManageAgreement(view);
                        postInvoiceResultView = assignmentAgreementManageAgreementView.PostInvoiceResultView;
                        assignmentAgreementManageAgreementView.AssignmentAgreementTable = assignmentAgreementTableService.UpdateAssignmentAgreementTable(assignmentAgreementManageAgreementView.AssignmentAgreementTable);
                        if(assignmentAgreementManageAgreementView.RefAssignmentAgreementTable != null)
                            assignmentAgreementManageAgreementView.RefAssignmentAgreementTable = assignmentAgreementTableService.UpdateAssignmentAgreementTable(assignmentAgreementManageAgreementView.RefAssignmentAgreementTable);
                        labels.Add("LABEL.ASSIGNMENT_AGREEMENT");
                        labels.Add(SmartAppUtil.GetDropDownLabel(assignmentAgreementManageAgreementView.AssignmentAgreementTable.InternalAssignmentAgreementId, assignmentAgreementManageAgreementView.AssignmentAgreementTable.Description));
                    }
                    else if (view.RefType == (int)RefType.MainAgreement)
                    {
                        IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                        mainAgreementManageAgreementView = UpdateMainAgreementManageAgreement(view);
                        postInvoiceResultView = mainAgreementManageAgreementView.PostInvoiceResultView;
                        MainAgreementTable mainAgreementTable = mainAgreementManageAgreementView.MainAgreementTables.Where(w => w.MainAgreementTableGUID == view.RefGUID.StringToGuid()).FirstOrDefault();
                        labels.Add("LABEL.MAIN_AGREEMENT");
                        labels.Add(SmartAppUtil.GetDropDownLabel(mainAgreementTable.InternalMainAgreementId, mainAgreementTable.Description));
                    }
                    else if (view.RefType == (int)RefType.GuarantorAgreement)
                    {
                        IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                        guarantorAgreementManageAgreementView = UpdateGuarantorAgreementManageAgreement(view);
                        GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementManageAgreementView.GuarantorAgreementTables.Where(w => w.GuarantorAgreementTableGUID == view.RefGUID.StringToGuid()).FirstOrDefault();
                        labels.Add("LABEL.GUARANTOR_AGREEMENT");
                        labels.Add(SmartAppUtil.GetDropDownLabel(guarantorAgreementTable.InternalGuarantorAgreementId, guarantorAgreementTable.Description));
                    }
                    else if (view.RefType == (int)RefType.BusinessCollateralAgreement)
                    {
                        IBusinessCollateralAgmTableService businessCollateralAgmTableService = new BusinessCollateralAgmTableService(db);
                        businessCollateralAgreementManageAgreementView = UpdateBusinessCollateralAgmManageAgreement(view);
                        BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgreementManageAgreementView.BusinessCollateralAgmTables.Where(w => w.BusinessCollateralAgmTableGUID == view.RefGUID.StringToGuid()).FirstOrDefault();
                        postInvoiceResultView = businessCollateralAgreementManageAgreementView.PostInvoiceResultView;
                        labels.Add("LABEL.BUSINESS_COLLATERAL_AGREEMENT");
                        labels.Add(SmartAppUtil.GetDropDownLabel(businessCollateralAgmTable.InternalBusinessCollateralAgmId, businessCollateralAgmTable.Description));
                    }
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        #region manage agreement
                        #region assignment agreement
                        if (ConditionService.IsNotZero(assignmentAgreementManageAgreementView.IntercompanyInvoiceTable.Count()))
                        {
                            this.BulkInsert(assignmentAgreementManageAgreementView.IntercompanyInvoiceTable);
                        }
                        #endregion assignment agreement
                        #region main agreement
                        if (ConditionService.IsNotZero(mainAgreementManageAgreementView.MainAgreementTables.Count()))
                        {
                            IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                            mainAgreementTableRepo.ValidateUpdate(mainAgreementManageAgreementView.MainAgreementTables);
                            this.BulkUpdate(mainAgreementManageAgreementView.MainAgreementTables);
                        }
                        if (ConditionService.IsNotZero(mainAgreementManageAgreementView.GuarantorAgreementTables.Count()))
                        {
                            this.BulkUpdate(mainAgreementManageAgreementView.GuarantorAgreementTables);
                        }
                        if (ConditionService.IsNotZero(mainAgreementManageAgreementView.IntercompanyInvoiceTable.Count()))
                        {
                            this.BulkInsert(mainAgreementManageAgreementView.IntercompanyInvoiceTable);
                        }
                        #endregion main agreement
                        #region guarantor agreement
                        if (ConditionService.IsNotZero(guarantorAgreementManageAgreementView.GuarantorAgreementTables.Count()))
                        {
                            IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                            guarantorAgreementTableRepo.ValidateUpdate(guarantorAgreementManageAgreementView.GuarantorAgreementTables);
                            this.BulkUpdate(guarantorAgreementManageAgreementView.GuarantorAgreementTables);
                        }
                        #endregion guarantor agreement
                        #region business collateral agreement
                        if (ConditionService.IsNotZero(businessCollateralAgreementManageAgreementView.BusinessCollateralAgmTables.Count()))
                        {
                            IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                            businessCollateralAgmTableRepo.ValidateUpdate(businessCollateralAgreementManageAgreementView.BusinessCollateralAgmTables);
                            this.BulkUpdate(businessCollateralAgreementManageAgreementView.BusinessCollateralAgmTables);
                        }
                        if (ConditionService.IsNotZero(businessCollateralAgreementManageAgreementView.CreateCustBusinessCollaterals.Count()))
                        {
                            this.BulkInsert(businessCollateralAgreementManageAgreementView.CreateCustBusinessCollaterals);
                        }
                        if (ConditionService.IsNotZero(businessCollateralAgreementManageAgreementView.UpdateCustBusinessCollaterals.Count()))
                        {
                            this.BulkUpdate(businessCollateralAgreementManageAgreementView.UpdateCustBusinessCollaterals);
                        }
                        if (ConditionService.IsNotZero(businessCollateralAgreementManageAgreementView.IntercompanyInvoiceTable.Count()))
                        {
                            this.BulkInsert(businessCollateralAgreementManageAgreementView.IntercompanyInvoiceTable);
                        }
                        if (ConditionService.IsNotZero(businessCollateralAgreementManageAgreementView.BusinessCollateralAgmLines.Count()))
                        {
                            this.BulkUpdate(businessCollateralAgreementManageAgreementView.BusinessCollateralAgmLines);
                        }
                        #endregion business collateral agreement
                        #endregion manage agreement

                        #region post invoice
                        if (postInvoiceResultView != null)
                        {
                            if (ConditionService.IsNotZero(postInvoiceResultView.InvoiceTables.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.InvoiceTables, false);
                            }
                            if (ConditionService.IsNotZero(postInvoiceResultView.InvoiceLines.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.InvoiceLines, false);
                            }
                            if (ConditionService.IsNotZero(postInvoiceResultView.CustTranses.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.CustTranses, false);
                            }
                            if (ConditionService.IsNotZero(postInvoiceResultView.CreditAppTranses.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.CreditAppTranses, false);
                            }
                            if (ConditionService.IsNotZero(postInvoiceResultView.TaxInvoiceTables.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.TaxInvoiceTables, false);
                            }
                            if (ConditionService.IsNotZero(postInvoiceResultView.TaxInvoiceLines.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.TaxInvoiceLines, false);
                            }
                            if (ConditionService.IsNotZero(postInvoiceResultView.ProcessTranses.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.ProcessTranses, false);
                            }
                            if (ConditionService.IsNotZero(postInvoiceResultView.RetentionTranses.Count()))
                            {
                                this.BulkInsert(postInvoiceResultView.RetentionTranses, false);
                            }
                        }
                        #endregion post invoice

                        UnitOfWork.Commit(transaction);
                    }

                    switch (view.ManageAgreementAction)
                    {
                        case (int)ManageAgreementAction.Post:
                            success.AddData("SUCCESS.90004", labels.ToArray());
                            break;
                        case (int)ManageAgreementAction.Send:
                            success.AddData("SUCCESS.90005", labels.ToArray());
                            break;
                        case (int)ManageAgreementAction.Sign:
                            success.AddData("SUCCESS.90006", labels.ToArray());
                            break;
                        case (int)ManageAgreementAction.Close:
                            success.AddData("SUCCESS.90007", labels.ToArray());
                            break;
                        case (int)ManageAgreementAction.Cancel:
                            success.AddData("SUCCESS.90008", labels.ToArray());
                            break;
                    }
                    view.Notification = success;
                }
                return view;
            }
            catch (Exception ex)
            {

                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private AssignmentAgreementManageAgreementView UpdateAssignmentAgreementManageAgreement(ManageAgreementView view)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementManageAgreementView assAGMManageAgreementView = new AssignmentAgreementManageAgreementView();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(view.RefGUID.StringToGuid());
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.AssignmentAgreement);
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Posted).ToString());
                    assignmentAgreementTable.AssignmentAgreementId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                    assignmentAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Send)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Sent).ToString());
                    assignmentAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                    IInvoiceService invoiceService = new InvoiceService(db);
                    IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.AssignmentAgreement, view.RefGUID.StringToGuid());

                    GenInvoiceFromServiceFeeTransParamView genInvoiceFromServiceFeeTransView = new GenInvoiceFromServiceFeeTransParamView()
                    {
                        ServiceFeeTrans = serviceFeeTrans.ToList(),
                        CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID,
                        IssuedDate = assignmentAgreementTable.AgreementDate,
                        DocumentId = assignmentAgreementTable.AssignmentAgreementId,
                    };
                    GenInvoiceFromServiceFeeTransResultView genInvoiceFromServiceFeeTransResultView =
                    serviceFeeTransService.GenInvoiceFromServiceFeeTrans(genInvoiceFromServiceFeeTransView);
                    assAGMManageAgreementView.IntercompanyInvoiceTable = genInvoiceFromServiceFeeTransResultView.IntercompanyInvoiceTable;
                    assAGMManageAgreementView.PostInvoiceResultView = invoiceService.PostInvoice(genInvoiceFromServiceFeeTransResultView.InvoiceTable, genInvoiceFromServiceFeeTransResultView.InvoiceLine, RefType.AssignmentAgreement, view.RefGUID.StringToGuid());
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Signed).ToString());
                    assignmentAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    assignmentAgreementTable.SigningDate = view.SigningDate.StringToDate();

                    if (assignmentAgreementTable.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation && assignmentAgreementTable.RefAssignmentAgreementTableGUID.HasValue)
                    {
                        AssignmentAgreementTable refAssignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(assignmentAgreementTable.RefAssignmentAgreementTableGUID.Value);
                        if(refAssignmentAgreementTable != null)
                        {
                            DocumentStatus cancelStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Cancelled).ToString());
                            refAssignmentAgreementTable.DocumentStatusGUID = cancelStatus.DocumentStatusGUID;
                            assAGMManageAgreementView.RefAssignmentAgreementTable = refAssignmentAgreementTable;
                        }
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Close)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Closed).ToString());
                    assignmentAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    assignmentAgreementTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                    assignmentAgreementTable.Remark = view.ReasonRemark;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Cancel)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Cancelled).ToString());
                    assignmentAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    assignmentAgreementTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                    assignmentAgreementTable.Remark = view.ReasonRemark;
                }
                assignmentAgreementTable.ModifiedBy = db.GetUserName();
                assignmentAgreementTable.ModifiedDateTime = DateTime.Now;
                assAGMManageAgreementView.AssignmentAgreementTable = assignmentAgreementTable;
                return assAGMManageAgreementView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private MainAgreementManageAgreementView UpdateMainAgreementManageAgreement(ManageAgreementView view)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementManageAgreementView mainAgreementManageAgreementView = new MainAgreementManageAgreementView();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(view.RefGUID.StringToGuid());
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetMainAgreementNumberSeqGUID(db.GetCompanyFilter().StringToGuid(), view.ProductType);
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Posted).ToString());
                    mainAgreementTable.MainAgreementId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqTableGUID);
                    mainAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Send)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Sent).ToString());
                    mainAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                    IInvoiceService invoiceService = new InvoiceService(db);
                    IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.MainAgreement, view.RefGUID.StringToGuid());
                    DateTime signingDate = view.SigningDate.StringToDate();

                    GenInvoiceFromServiceFeeTransParamView genInvoiceFromServiceFeeTransView = new GenInvoiceFromServiceFeeTransParamView()
                    {
                        ServiceFeeTrans = serviceFeeTrans.ToList(),
                        CustomerTableGUID = mainAgreementTable.CustomerTableGUID,
                        IssuedDate = mainAgreementTable.AgreementDate,
                        DocumentId = mainAgreementTable.MainAgreementId,
                        ProductType = (ProductType)view.ProductType,
                        CreditAppTableGUID = mainAgreementTable.CreditAppTableGUID
                    };
                    GenInvoiceFromServiceFeeTransResultView genInvoiceFromServiceFeeTransResultView =
                    serviceFeeTransService.GenInvoiceFromServiceFeeTrans(genInvoiceFromServiceFeeTransView);
                    mainAgreementManageAgreementView.IntercompanyInvoiceTable = genInvoiceFromServiceFeeTransResultView.IntercompanyInvoiceTable;
                    mainAgreementManageAgreementView.PostInvoiceResultView = invoiceService.PostInvoice(genInvoiceFromServiceFeeTransResultView.InvoiceTable, genInvoiceFromServiceFeeTransResultView.InvoiceLine, RefType.MainAgreement, view.RefGUID.StringToGuid());
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Signed).ToString());
                    mainAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    mainAgreementTable.SigningDate = signingDate;
                    // 3. Update CreditAppTable.InactiveDate && ExpiryDate
                    ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(mainAgreementTable.CreditAppTableGUID);
                    if (creditAppTable.CreditLimitExpiration == (int)CreditLimitExpiration.ByTransaction)
                    {
                        ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                        CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);
                        creditAppTable.InactiveDate = signingDate.AddDays(creditLimitType.InactiveDay);
                        creditAppTable.ExpiryDate = signingDate.AddDays(creditLimitType.CreditLimitExpiryDay);
                        creditAppTable.ModifiedBy = db.GetUserName();
                        creditAppTable.ModifiedDateTime = DateTime.Now;
                        ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                        creditAppTableService.UpdateCreditAppTable(creditAppTable);
                    }
                    // 4. WHEN Caller.AgreementDocType = NoticeOfCancellation THEN Update Status > MainAgreementTableByMain .MainAgreementTable = Caller.RefMainAgreementTableGUID
                    if (mainAgreementTable.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation && mainAgreementTable.RefMainAgreementTableGUID.HasValue)
                    {
                        DocumentStatus mainAgreementStatusCancelled = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Cancelled).ToString());
                        MainAgreementTable mainAgreementTableByRefMainAgreement = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(mainAgreementTable.RefMainAgreementTableGUID.Value);
                        mainAgreementTableByRefMainAgreement.DocumentStatusGUID = mainAgreementStatusCancelled.DocumentStatusGUID;
                        mainAgreementTableByRefMainAgreement.ModifiedBy = db.GetUserName();
                        mainAgreementTableByRefMainAgreement.ModifiedDateTime = DateTime.Now;
                        IMainAgreementTableService mainAgreementTableService = new MainAgreementTableService(db);
                        mainAgreementTableService.UpdateMainAgreementTable(mainAgreementTableByRefMainAgreement);
                        // 4.A Update Status > MainAgreementTable.RefMainAgreementTable = MainAgreementTableByMain.MainAgreementTable and MainAgreementTable.DocType = Addendum
                        List<MainAgreementTable> refMainAgreementTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(mainAgreementTableByRefMainAgreement.MainAgreementTableGUID, AgreementDocType.Addendum);
                        refMainAgreementTables.Select(s =>
                        {
                            s.DocumentStatusGUID = mainAgreementStatusCancelled.DocumentStatusGUID;
                            s.ModifiedBy = db.GetUserName();
                            s.ModifiedDateTime = DateTime.Now;
                            return s;
                        }).ToList();
                        // 4.B Update Status > GuarantorAgreementTable.RefMainAgreementTable = MainAgreementTableByMain.MainAgreementTable
                        DocumentStatus gurantorStatusCancelled = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Cancelled).ToString());
                        IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                        List<GuarantorAgreementTable> guarantorAgreementTables = guarantorAgreementTableRepo.GetListByMainAgreementTable(mainAgreementTableByRefMainAgreement.MainAgreementTableGUID);
                        guarantorAgreementTables.Select(s =>
                        {
                            s.DocumentStatusGUID = gurantorStatusCancelled.DocumentStatusGUID;
                            s.ModifiedBy = db.GetUserName();
                            s.ModifiedDateTime = DateTime.Now;
                            return s;
                        }).ToList();
                        mainAgreementManageAgreementView.MainAgreementTables.AddRange(refMainAgreementTables);
                        mainAgreementManageAgreementView.GuarantorAgreementTables.AddRange(guarantorAgreementTables);
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Close)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Closed).ToString());
                    mainAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    mainAgreementTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                    // Update Status > MainAgreementTable.RefMainAgreementTable = Caller.MainAgreementTable and MainAgreementTable.AgreementDocType = Addendum
                    List<MainAgreementTable> refMainAgreementTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(mainAgreementTable.MainAgreementTableGUID, AgreementDocType.Addendum);
                    refMainAgreementTables.Select(s =>
                    {
                        s.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                        s.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                        s.ModifiedBy = db.GetUserName();
                        s.ModifiedDateTime = DateTime.Now;
                        return s;
                    }).ToList();
                    mainAgreementManageAgreementView.MainAgreementTables.AddRange(refMainAgreementTables);
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Cancel)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Cancelled).ToString());
                    mainAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    mainAgreementTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                }
                mainAgreementTable.ModifiedBy = db.GetUserName();
                mainAgreementTable.ModifiedDateTime = DateTime.Now;
                mainAgreementManageAgreementView.MainAgreementTables.Add(mainAgreementTable);
                return mainAgreementManageAgreementView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private BusinessCollateralAgreementManageAgreementView UpdateBusinessCollateralAgmManageAgreement(ManageAgreementView view)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                BusinessCollateralAgreementManageAgreementView businessCollateralAgreementManageAgreementView = new BusinessCollateralAgreementManageAgreementView();
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(view.RefGUID.StringToGuid());

                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.BusinessCollateralAgreement);
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Posted).ToString());
                    businessCollateralAgmTable.BusinessCollateralAgmId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                    businessCollateralAgmTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Send)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Sent).ToString());
                    businessCollateralAgmTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                    IInvoiceService invoiceService = new InvoiceService(db);
                    // get shared method: Method017_GenInvoiceFromServiceFeeTrans
                    IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.BusinessCollateralAgreement, view.RefGUID.StringToGuid());

                    GenInvoiceFromServiceFeeTransParamView genInvoiceFromServiceFeeTransView = new GenInvoiceFromServiceFeeTransParamView()
                    {
                        ServiceFeeTrans = serviceFeeTrans.ToList(),
                        CustomerTableGUID = businessCollateralAgmTable.CustomerTableGUID,
                        IssuedDate = businessCollateralAgmTable.AgreementDate,
                        DocumentId = businessCollateralAgmTable.BusinessCollateralAgmId,
                        ProductType = (ProductType)view.ProductType,
                        CreditAppTableGUID = businessCollateralAgmTable.CreditAppTableGUID
                    };
                    // get shared method: Method017_GenInvoiceFromServiceFeeTrans
                    GenInvoiceFromServiceFeeTransResultView genInvoiceFromServiceFeeTransResultView =
                    serviceFeeTransService.GenInvoiceFromServiceFeeTrans(genInvoiceFromServiceFeeTransView);
                    businessCollateralAgreementManageAgreementView.IntercompanyInvoiceTable = genInvoiceFromServiceFeeTransResultView.IntercompanyInvoiceTable;
                    // get shared method: Method022_PostInvoice 
                    businessCollateralAgreementManageAgreementView.PostInvoiceResultView = invoiceService.PostInvoice(genInvoiceFromServiceFeeTransResultView.InvoiceTable, genInvoiceFromServiceFeeTransResultView.InvoiceLine, RefType.BusinessCollateralAgreement, view.RefGUID.StringToGuid());
                    // get shared method: shared method: Method110_GenCustBusinessCollateral
                    ICreditAppReqBusCollateralInfoService creditAppReqBusCollateralInfoService = new CreditAppReqBusCollateralInfoService(db);
                    if (businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.New || businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.Addendum)
                    {
                        List<CustBusinessCollateral> createCustBusinessCollaterals = creditAppReqBusCollateralInfoService.GenCustBusinessCollateral(businessCollateralAgmTable.BusinessCollateralAgmTableGUID);
                        businessCollateralAgreementManageAgreementView.CreateCustBusinessCollaterals.AddRange(createCustBusinessCollaterals);
                    }
                    // 1 Update status = Sign
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Signed).ToString());
                    businessCollateralAgmTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    // 2 Update SigningDate = Signing date
                    businessCollateralAgmTable.SigningDate = view.SigningDate.StringNullToDateNull();
                    // 3 WHEN Caller.AgreementDocType = NoticeOfCancellation THEN Update Status And Cancel
                    if (businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation)
                    {
                        #region 3.1
                        IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                        ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
                        IEnumerable<BusinessCollateralAgmLine> businessCollateralAgmLines = businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(businessCollateralAgmTable.BusinessCollateralAgmTableGUID);
                        List<BusinessCollateralAgmLine> originalBusinessCollateralAgmLines = businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByIdNoTracking(businessCollateralAgmLines.Select(s => s.OriginalBusinessCollateralAgreementLineGUID.Value)).ToList();
                        originalBusinessCollateralAgmLines.Select(s =>
                        {
                            s.Cancelled = true;
                            s.ModifiedBy = db.GetUserName();
                            s.ModifiedDateTime = DateTime.Now;
                            return s;
                        }).ToList();
                        businessCollateralAgreementManageAgreementView.BusinessCollateralAgmLines.AddRange(originalBusinessCollateralAgmLines);
                        #endregion 3.1 

                        #region 3.2
                        DocumentStatus businessCollateralAgmStatusCancelled = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Cancelled).ToString());
                        IEnumerable<Guid> originalBusinessCollateralAgmTableGUID = businessCollateralAgmLines.Select(s => s.OriginalBusinessCollateralAgreementTableGUID.Value).Distinct();
                        List<BusinessCollateralAgmTable> originalBusinessCollateralAgmTableList = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTracking(originalBusinessCollateralAgmTableGUID).ToList();
                        originalBusinessCollateralAgmTableList.ForEach(originalBusinessCollateralAgmTable =>
                        {
                            IEnumerable<BusinessCollateralAgmLine> businessCollateralAgmLinesByOriginalBusinessCollateralAgmTable = businessCollateralAgmLineRepo.GetBusinessCollateralAgmLineByBusinessCollatralAgmTableNoTracking(originalBusinessCollateralAgmTable.BusinessCollateralAgmTableGUID);
                            List<BusinessCollateralAgmLine> businessCollateralAgmLineNotCancel = businessCollateralAgmLinesByOriginalBusinessCollateralAgmTable.Where(w => (!originalBusinessCollateralAgmLines.Select(s => s.BusinessCollateralAgmLineGUID).Contains(w.BusinessCollateralAgmLineGUID)) && w.Cancelled == false).ToList();
                            bool isNotBusinessCollateralAgmLineNotCancel = !ConditionService.IsNotZero(businessCollateralAgmLineNotCancel.Count());
                            if (isNotBusinessCollateralAgmLineNotCancel)
                            {
                                originalBusinessCollateralAgmTable.DocumentStatusGUID = businessCollateralAgmStatusCancelled.DocumentStatusGUID;
                                originalBusinessCollateralAgmTable.ModifiedBy = db.GetUserName();
                                originalBusinessCollateralAgmTable.ModifiedDateTime = DateTime.Now;
                            }
                            businessCollateralAgreementManageAgreementView.BusinessCollateralAgmTables.Add(originalBusinessCollateralAgmTable);
                        });
                        #endregion 3.2 

                        #region 3.3
                        List<CustBusinessCollateral> updateCustBusinessCollaterals = custBusinessCollateralRepo.GetListByBusinessCollateralAgmLines(businessCollateralAgmLines.Select(s => s.OriginalBusinessCollateralAgreementLineGUID.Value)).ToList();
                        updateCustBusinessCollaterals.Select(s =>
                        {
                            s.Cancelled = true;
                            s.ModifiedBy = db.GetUserName();
                            s.ModifiedDateTime = DateTime.Now;
                            return s;
                        }).ToList();
                        businessCollateralAgreementManageAgreementView.UpdateCustBusinessCollaterals.AddRange(updateCustBusinessCollaterals);
                        #endregion 3.3
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Close)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Closed).ToString());
                    businessCollateralAgmTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    businessCollateralAgmTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                    // 3 Update Status > IF Caller.AgreementDocType = New
                    if (businessCollateralAgmTable.AgreementDocType == (int)AgreementDocType.New)
                    {
                        List<BusinessCollateralAgmTable> refBusinessCollateralAgmTables = businessCollateralAgmTableRepo.GetListByRefBusinessCollateralAgmTableAndAgreementDocType(businessCollateralAgmTable.BusinessCollateralAgmTableGUID, AgreementDocType.Addendum);
                        refBusinessCollateralAgmTables.Select(s =>
                        {
                            s.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                            s.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                            s.ModifiedBy = db.GetUserName();
                            s.ModifiedDateTime = DateTime.Now;
                            return s;
                        }).ToList();
                        businessCollateralAgreementManageAgreementView.BusinessCollateralAgmTables.AddRange(refBusinessCollateralAgmTables);
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Cancel)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BusinessCollateralAgreementStatus.Cancelled).ToString());
                    businessCollateralAgmTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    businessCollateralAgmTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                }
                businessCollateralAgmTable.ModifiedBy = db.GetUserName();
                businessCollateralAgmTable.ModifiedDateTime = DateTime.Now;
                businessCollateralAgreementManageAgreementView.BusinessCollateralAgmTables.Add(businessCollateralAgmTable);
                return businessCollateralAgreementManageAgreementView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private GuarantorAgreementManageAgreementView UpdateGuarantorAgreementManageAgreement(ManageAgreementView view)
        {
            try
            {
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                GuarantorAgreementManageAgreementView guarantorAgreementManageAgreement = new GuarantorAgreementManageAgreementView();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                GuarantorAgreementTable guarantorAgreementTable = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(view.RefGUID.StringToGuid());
                if (view.ManageAgreementAction == (int)ManageAgreementAction.Post)
                {
                    INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetGuarantorAgreementNumberSeqGUID(db.GetCompanyFilter().StringToGuid(), view.ProductType);
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Posted).ToString());
                    guarantorAgreementTable.GuarantorAgreementId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqTableGUID);
                    guarantorAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Send)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Sent).ToString());
                    guarantorAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Sign)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Signed).ToString());
                    guarantorAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    guarantorAgreementTable.SigningDate = view.SigningDate.StringNullToDateNull();
                    // 3. WHEN AgreementDocType = Extend THEN Update Status > GuarantorAgreementByMain
                    if (guarantorAgreementTable.AgreementDocType == (int)AgreementDocType.Extend && guarantorAgreementTable.RefGuarantorAgreementTableGUID.HasValue)
                    {
                        DocumentStatus closedStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Closed).ToString());
                        GuarantorAgreementTable guarantorAgreementTableByRefGuarantorAgreement = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTracking(guarantorAgreementTable.RefGuarantorAgreementTableGUID.Value);
                        guarantorAgreementTableByRefGuarantorAgreement.DocumentStatusGUID = closedStatus.DocumentStatusGUID;
                        guarantorAgreementTableByRefGuarantorAgreement.ModifiedBy = db.GetUserName();
                        guarantorAgreementTableByRefGuarantorAgreement.ModifiedDateTime = DateTime.Now;
                        IGuarantorAgreementTableService guarantorAgreementTableService = new GuarantorAgreementTableService(db);
                        guarantorAgreementManageAgreement.GuarantorAgreementTables.Add(guarantorAgreementTableByRefGuarantorAgreement);
                    }
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Close)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Closed).ToString());
                    guarantorAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    guarantorAgreementTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                }
                else if (view.ManageAgreementAction == (int)ManageAgreementAction.Cancel)
                {
                    DocumentStatus nextStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)GuarantorAgreementStatus.Cancelled).ToString());
                    guarantorAgreementTable.DocumentStatusGUID = nextStatus.DocumentStatusGUID;
                    guarantorAgreementTable.DocumentReasonGUID = view.DocumentReasonGUID.StringToGuid();
                }
                guarantorAgreementTable.ModifiedBy = db.GetUserName();
                guarantorAgreementTable.ModifiedDateTime = DateTime.Now;
                guarantorAgreementManageAgreement.GuarantorAgreementTables.Add(guarantorAgreementTable);
                return guarantorAgreementManageAgreement;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public ManageAgreementView GetManageAgreementByRefType(ManageAgreementView view)
        {
            try
            {
                ManageAgreementView result = new ManageAgreementView();
                switch (view.RefType)
                {
                    case (int)RefType.AssignmentAgreement:
                        IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                        AssignmentAgreementTableItemView assignmentAgreementTable = assignmentAgreementTableRepo.GetByIdvw(view.RefGUID.StringToGuid());
                        result = new ManageAgreementView
                        {
                            RefType = view.RefType,
                            RefGUID = view.RefGUID,
                            ManageAgreementAction = view.ManageAgreementAction,
                            ProductType = view.ProductType,
                            Label = view.Label,
                            ManageAgreementGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                            DocumentStatusGUID = assignmentAgreementTable.DocumentStatusGUID,
                            InternalAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
                            AgreementId = assignmentAgreementTable.AssignmentAgreementId,
                            Description = assignmentAgreementTable.Description,
                            AgreementDocType = assignmentAgreementTable.AgreementDocType,
                            AgreementDate = assignmentAgreementTable.AgreementDate,
                            CustomerTable_Values = assignmentAgreementTable.CustomerTable_Values,
                            CustomerName = assignmentAgreementTable.CustomerName,
                            BuyerTable_Values = assignmentAgreementTable.buyerTable_Values,
                            BuyerName = assignmentAgreementTable.BuyerName,
                            AssignmentMethodId = assignmentAgreementTable.AssignmentMethod_AssignmentMethodId,
                            Amount = assignmentAgreementTable.AssignmentAgreementAmount,
                        };
                        break;
                    case (int)RefType.MainAgreement:
                        IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                        MainAgreementTableItemView mainAgreementTable = mainAgreementTableRepo.GetByIdvw(view.RefGUID.StringToGuid());
                        result = new ManageAgreementView
                        {
                            RefType = view.RefType,
                            RefGUID = view.RefGUID,
                            ManageAgreementAction = view.ManageAgreementAction,
                            ProductType = view.ProductType,
                            Label = view.Label,
                            ManageAgreementGUID = mainAgreementTable.MainAgreementTableGUID,
                            DocumentStatusGUID = mainAgreementTable.DocumentStatusGUID,
                            InternalAgreementId = mainAgreementTable.InternalMainAgreementId,
                            AgreementId = mainAgreementTable.MainAgreementId,
                            Description = mainAgreementTable.Description,
                            AgreementDocType = mainAgreementTable.AgreementDocType,
                            AgreementDate = mainAgreementTable.AgreementDate,
                            CustomerTable_Values = mainAgreementTable.CustomerTable_Values,
                            CustomerName = mainAgreementTable.CustomerName,
                            BuyerTable_Values = mainAgreementTable.BuyerTable_Values,
                            BuyerName = mainAgreementTable.BuyerName,
                        };
                        break;
                    case (int)RefType.GuarantorAgreement:
                        IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                        GuarantorAgreementTableItemView guarantorAgreementTable = guarantorAgreementTableRepo.GetByIdvw(view.RefGUID.StringToGuid());
                        result = new ManageAgreementView
                        {
                            RefType = view.RefType,
                            RefGUID = view.RefGUID,
                            ManageAgreementAction = view.ManageAgreementAction,
                            ProductType = view.ProductType,
                            Label = view.Label,
                            ManageAgreementGUID = guarantorAgreementTable.GuarantorAgreementTableGUID,
                            DocumentStatusGUID = guarantorAgreementTable.DocumentStatusGUID,
                            InternalAgreementId = guarantorAgreementTable.InternalGuarantorAgreementId,
                            AgreementId = guarantorAgreementTable.GuarantorAgreementId,
                            Description = guarantorAgreementTable.Description,
                            AgreementDocType = guarantorAgreementTable.AgreementDocType,
                            AgreementDate = guarantorAgreementTable.AgreementDate,
                            CustomerTable_Values = guarantorAgreementTable.CustomerTable_Value,
                            CustomerName = guarantorAgreementTable.MainAgreementTable_CustomerName,
                            ExpiryDate = guarantorAgreementTable.ExpiryDate,
                            Affiliate = guarantorAgreementTable.Affiliate,
                        };
                        break;
                    case (int)RefType.BusinessCollateralAgreement:
                        IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                        BusinessCollateralAgmTableItemView businessCollateralAgmTable = businessCollateralAgmTableRepo.GetByIdvw(view.RefGUID.StringToGuid());
                        result = new ManageAgreementView
                        {
                            RefType = view.RefType,
                            RefGUID = view.RefGUID,
                            ManageAgreementAction = view.ManageAgreementAction,
                            ProductType = view.ProductType,
                            Label = view.Label,
                            ManageAgreementGUID = businessCollateralAgmTable.BusinessCollateralAgmTableGUID,
                            DocumentStatusGUID = businessCollateralAgmTable.DocumentStatusGUID,
                            InternalAgreementId = businessCollateralAgmTable.InternalBusinessCollateralAgmId,
                            AgreementId = businessCollateralAgmTable.BusinessCollateralAgmId,
                            Description = businessCollateralAgmTable.Description,
                            AgreementDocType = businessCollateralAgmTable.AgreementDocType,
                            AgreementDate = businessCollateralAgmTable.AgreementDate,
                            CustomerTable_Values = businessCollateralAgmTable.CustomerTable_Values,
                            CustomerName = businessCollateralAgmTable.CustomerName,
                        };
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion
        #endregion
    }
}
