using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IBuyerAgreementTableService
    {

        BuyerAgreementLineItemView GetBuyerAgreementLineById(string id);
        BuyerAgreementLineItemView CreateBuyerAgreementLine(BuyerAgreementLineItemView buyerAgreementLineView);
        BuyerAgreementLineItemView UpdateBuyerAgreementLine(BuyerAgreementLineItemView buyerAgreementLineView);
        bool DeleteBuyerAgreementLine(string id);
        BuyerAgreementTableItemView GetBuyerAgreementTableById(string id);
        BuyerAgreementTableItemView CreateBuyerAgreementTable(BuyerAgreementTableItemView buyerAgreementTableView);
        BuyerAgreementTableItemView UpdateBuyerAgreementTable(BuyerAgreementTableItemView buyerAgreementTableView);
        bool DeleteBuyerAgreementTable(string id);
        bool IsManualByBuyerAgreement(string companyId);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(SearchParameter search);
        IEnumerable<SelectItem<BuyerAgreementLineItemView>> GetDropDownItemBuyerAgreementLineTableByBuyerAgreementTrans(SearchParameter search);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByPurchaseLine(SearchParameter search);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByPurchaseLineValidateBuyer(SearchParameter search);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementLineTableByBuyerAndCustomer(SearchParameter search);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByWithdrawal(SearchParameter search);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByCustomerAndBuyer(SearchParameter search);
        #region function
        BuyerAgreementTableItemView GetBuyerAgreementTableSumLineById(string id);
        #endregion
        void CreateBuyerAgreementLineCollection(List<BuyerAgreementLineItemView> buyerAgreementLineItemViews);
        void CreateBuyerAgreementTableCollection(List<BuyerAgreementTableItemView> buyerAgreementTableItemViews);
    }
    public class BuyerAgreementTableService : SmartAppService, IBuyerAgreementTableService
    {
        public BuyerAgreementTableService(SmartAppDbContext context) : base(context) { }
        public BuyerAgreementTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public BuyerAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public BuyerAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public BuyerAgreementTableService() : base() { }

        public BuyerAgreementLineItemView GetBuyerAgreementLineById(string id)
        {
            try
            {
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                return buyerAgreementLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementLineItemView CreateBuyerAgreementLine(BuyerAgreementLineItemView buyerAgreementLineView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                buyerAgreementLineView = accessLevelService.AssignOwnerBU(buyerAgreementLineView);
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                BuyerAgreementLine buyerAgreementLine = buyerAgreementLineView.ToBuyerAgreementLine();
                buyerAgreementLine = buyerAgreementLineRepo.CreateBuyerAgreementLine(buyerAgreementLine);
                base.LogTransactionCreate<BuyerAgreementLine>(buyerAgreementLine);
                UnitOfWork.Commit();
                return buyerAgreementLine.ToBuyerAgreementLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementLineItemView UpdateBuyerAgreementLine(BuyerAgreementLineItemView buyerAgreementLineView)
        {
            try
            {
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                BuyerAgreementLine inputBuyerAgreementLine = buyerAgreementLineView.ToBuyerAgreementLine();
                BuyerAgreementLine dbBuyerAgreementLine = buyerAgreementLineRepo.Find(inputBuyerAgreementLine.BuyerAgreementLineGUID);
                dbBuyerAgreementLine = buyerAgreementLineRepo.UpdateBuyerAgreementLine(dbBuyerAgreementLine, inputBuyerAgreementLine);
                base.LogTransactionUpdate<BuyerAgreementLine>(GetOriginalValues<BuyerAgreementLine>(dbBuyerAgreementLine), dbBuyerAgreementLine);
                UnitOfWork.Commit();
                return dbBuyerAgreementLine.ToBuyerAgreementLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteBuyerAgreementLine(string item)
        {
            try
            {
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                Guid buyerAgreementLineGUID = new Guid(item);
                BuyerAgreementLine buyerAgreementLine = buyerAgreementLineRepo.Find(buyerAgreementLineGUID);
                buyerAgreementLineRepo.Remove(buyerAgreementLine);
                base.LogTransactionDelete<BuyerAgreementLine>(buyerAgreementLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BuyerAgreementTableItemView GetBuyerAgreementTableById(string id)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                return buyerAgreementTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementTableItemView CreateBuyerAgreementTable(BuyerAgreementTableItemView buyerAgreementTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                buyerAgreementTableView = accessLevelService.AssignOwnerBU(buyerAgreementTableView);
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                BuyerAgreementTable buyerAgreementTable = buyerAgreementTableView.ToBuyerAgreementTable();
                GenBuyerAgreementNumberSeqCode(buyerAgreementTable);
                buyerAgreementTable = buyerAgreementTableRepo.CreateBuyerAgreementTable(buyerAgreementTable);
                base.LogTransactionCreate<BuyerAgreementTable>(buyerAgreementTable);
                UnitOfWork.Commit();
                return buyerAgreementTable.ToBuyerAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementTableItemView UpdateBuyerAgreementTable(BuyerAgreementTableItemView buyerAgreementTableView)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                BuyerAgreementTable inputBuyerAgreementTable = buyerAgreementTableView.ToBuyerAgreementTable();
                BuyerAgreementTable dbBuyerAgreementTable = buyerAgreementTableRepo.Find(inputBuyerAgreementTable.BuyerAgreementTableGUID);
                dbBuyerAgreementTable = buyerAgreementTableRepo.UpdateBuyerAgreementTable(dbBuyerAgreementTable, inputBuyerAgreementTable);
                base.LogTransactionUpdate<BuyerAgreementTable>(GetOriginalValues<BuyerAgreementTable>(dbBuyerAgreementTable), dbBuyerAgreementTable);
                UnitOfWork.Commit();
                return dbBuyerAgreementTable.ToBuyerAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteBuyerAgreementTable(string item)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                Guid buyerAgreementTableGUID = new Guid(item);
                BuyerAgreementTable buyerAgreementTable = buyerAgreementTableRepo.Find(buyerAgreementTableGUID);
                buyerAgreementTableRepo.Remove(buyerAgreementTable);
                base.LogTransactionDelete<BuyerAgreementTable>(buyerAgreementTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByBuyerAgreement(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.BuyerAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenBuyerAgreementNumberSeqCode(BuyerAgreementTable buyerAgreementTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = numberSequenceService.IsManualByReferenceId(buyerAgreementTable.CompanyGUID, ReferenceId.BuyerAgreement);
                if (!isManual)
                {
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(buyerAgreementTable.CompanyGUID, ReferenceId.BuyerAgreement);
                    buyerAgreementTable.BuyerAgreementId = numberSequenceService.GetNumber(buyerAgreementTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search = search.GetParentCondition(BuyerAgreementTableCondition.CreditAppRequestLineGUID);
                return buyerAgreementTableRepo.GetDropDownItemByBuyerAgreementTrans(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<BuyerAgreementLineItemView>> GetDropDownItemBuyerAgreementLineTableByBuyerAgreementTrans(SearchParameter search)
        {
            try
            {
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                search = search.GetParentCondition(BuyerAgreementTableCondition.BuyerAgreementTableGUID);
                return buyerAgreementLineRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByPurchaseLine(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { BuyerAgreementTableCondition.CustomerTableGUID, BuyerAgreementTableCondition.BuyerTableGUID });
                return buyerAgreementTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByPurchaseLineValidateBuyer(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { BuyerAgreementTableCondition.BuyerAgreementTransRefGUID });
                return buyerAgreementTableRepo.GetDropDownItemByPurchaseLine(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementLineTableByBuyerAndCustomer(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { BuyerAgreementTableCondition.BuyerTableGUID , BuyerAgreementTableCondition.CustomerTableGUID});
                return buyerAgreementTableRepo.GetDropDownItemByAssignmentAgreement(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByWithdrawal(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search = search.GetParentCondition(BuyerAgreementTableCondition.BuyerAgreementTransRefGUID);
                SearchCondition searchCondition = SearchConditionService.GetBuyerAgreementTransByRefTypeConditionForBuyerAgreementTable(Convert.ToInt32(RefType.CreditAppLine).ToString());
                search.Conditions.Add(searchCondition);
                return buyerAgreementTableRepo.GetDropDownItemByWithdrawalTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByCustomerAndBuyer(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { BuyerAgreementTableCondition.CustomerTableGUID, BuyerAgreementTableCondition.BuyerTableGUID });
                return buyerAgreementTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region function
        public BuyerAgreementTableItemView GetBuyerAgreementTableSumLineById(string id)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                BuyerAgreementTableItemView buyerAgreementTableItemView = buyerAgreementTableRepo.GetBuyerAgreementTableSumLineById(id.StringToGuid());
                return buyerAgreementTableItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region migration
        public void CreateBuyerAgreementLineCollection(List<BuyerAgreementLineItemView> buyerAgreementLineItemViews)
        {
            try
            {
                if (buyerAgreementLineItemViews.Any())
                {
                    IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                    List<BuyerAgreementLine> buyerAgreementLine = buyerAgreementLineItemViews.ToBuyerAgreementLine().ToList();
                    buyerAgreementLineRepo.ValidateAdd(buyerAgreementLine);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(buyerAgreementLine, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void CreateBuyerAgreementTableCollection(List<BuyerAgreementTableItemView> buyerAgreementTableItemViews)
        {
            try
            {
                if (buyerAgreementTableItemViews.Any())
                {
                    IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                    List<BuyerAgreementTable> buyerAgreementTable = buyerAgreementTableItemViews.ToBuyerAgreementTable().ToList();
                    buyerAgreementTableRepo.ValidateAdd(buyerAgreementTable);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(buyerAgreementTable, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion
    }
}
