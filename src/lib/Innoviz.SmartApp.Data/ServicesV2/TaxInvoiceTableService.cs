using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ITaxInvoiceTableService
	{

		TaxInvoiceLineItemView GetTaxInvoiceLineById(string id);
		TaxInvoiceLineItemView CreateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView);
		TaxInvoiceLineItemView UpdateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView);
		bool DeleteTaxInvoiceLine(string id);
		TaxInvoiceTableItemView GetTaxInvoiceTableById(string id);
		TaxInvoiceTableItemView CreateTaxInvoiceTable(TaxInvoiceTableItemView taxInvoiceTableView);
		TaxInvoiceTable CreateTaxInvoiceTable(TaxInvoiceTable taxInvoiceTable);
		TaxInvoiceTableItemView UpdateTaxInvoiceTable(TaxInvoiceTableItemView taxInvoiceTableView);
		bool DeleteTaxInvoiceTable(string id);
		PrintTaxInvoiceReportView GetPrintTaxInvoiceCopyById(string taxInvoiceGUID);
		PrintTaxInvoiceReportView GetPrintTaxInvoiceOriginalById(string taxInvoiceGUID);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemTaxInvoiceDocumentStatus(SearchParameter search);
		 

	}
	public class TaxInvoiceTableService : SmartAppService, ITaxInvoiceTableService
	{
		public TaxInvoiceTableService(SmartAppDbContext context) : base(context) { }
		public TaxInvoiceTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public TaxInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public TaxInvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public TaxInvoiceTableService() : base() { }

		public TaxInvoiceLineItemView GetTaxInvoiceLineById(string id)
		{
			try
			{
				ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
				return taxInvoiceLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceLineItemView CreateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				taxInvoiceLineView = accessLevelService.AssignOwnerBU(taxInvoiceLineView);
				ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
				TaxInvoiceLine taxInvoiceLine = taxInvoiceLineView.ToTaxInvoiceLine();
				taxInvoiceLine = taxInvoiceLineRepo.CreateTaxInvoiceLine(taxInvoiceLine);
				base.LogTransactionCreate<TaxInvoiceLine>(taxInvoiceLine);
				UnitOfWork.Commit();
				return taxInvoiceLine.ToTaxInvoiceLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceLineItemView UpdateTaxInvoiceLine(TaxInvoiceLineItemView taxInvoiceLineView)
		{
			try
			{
				ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
				TaxInvoiceLine inputTaxInvoiceLine = taxInvoiceLineView.ToTaxInvoiceLine();
				TaxInvoiceLine dbTaxInvoiceLine = taxInvoiceLineRepo.Find(inputTaxInvoiceLine.TaxInvoiceLineGUID);
				dbTaxInvoiceLine = taxInvoiceLineRepo.UpdateTaxInvoiceLine(dbTaxInvoiceLine, inputTaxInvoiceLine);
				base.LogTransactionUpdate<TaxInvoiceLine>(GetOriginalValues<TaxInvoiceLine>(dbTaxInvoiceLine), dbTaxInvoiceLine);
				UnitOfWork.Commit();
				return dbTaxInvoiceLine.ToTaxInvoiceLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteTaxInvoiceLine(string item)
		{
			try
			{
				ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
				Guid taxInvoiceLineGUID = new Guid(item);
				TaxInvoiceLine taxInvoiceLine = taxInvoiceLineRepo.Find(taxInvoiceLineGUID);
				taxInvoiceLineRepo.Remove(taxInvoiceLine);
				base.LogTransactionDelete<TaxInvoiceLine>(taxInvoiceLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public TaxInvoiceTableItemView GetTaxInvoiceTableById(string id)
		{
			try
			{
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				return taxInvoiceTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceTableItemView CreateTaxInvoiceTable(TaxInvoiceTableItemView taxInvoiceTableView)
		{
			try
			{
				TaxInvoiceTable taxInvoiceTable = taxInvoiceTableView.ToTaxInvoiceTable();
				taxInvoiceTable = CreateTaxInvoiceTable(taxInvoiceTable);
				UnitOfWork.Commit();
				return taxInvoiceTable.ToTaxInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceTable CreateTaxInvoiceTable(TaxInvoiceTable taxInvoiceTable)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				taxInvoiceTable = accessLevelService.AssignOwnerBU(taxInvoiceTable);
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				taxInvoiceTable = taxInvoiceTableRepo.CreateTaxInvoiceTable(taxInvoiceTable);
				base.LogTransactionCreate<TaxInvoiceTable>(taxInvoiceTable);
				return taxInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public TaxInvoiceTableItemView UpdateTaxInvoiceTable(TaxInvoiceTableItemView taxInvoiceTableView)
		{
			try
			{
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				TaxInvoiceTable inputTaxInvoiceTable = taxInvoiceTableView.ToTaxInvoiceTable();
				TaxInvoiceTable dbTaxInvoiceTable = taxInvoiceTableRepo.Find(inputTaxInvoiceTable.TaxInvoiceTableGUID);
				dbTaxInvoiceTable = taxInvoiceTableRepo.UpdateTaxInvoiceTable(dbTaxInvoiceTable, inputTaxInvoiceTable);
				base.LogTransactionUpdate<TaxInvoiceTable>(GetOriginalValues<TaxInvoiceTable>(dbTaxInvoiceTable), dbTaxInvoiceTable);
				UnitOfWork.Commit();
				return dbTaxInvoiceTable.ToTaxInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteTaxInvoiceTable(string item)
		{
			try
			{
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				Guid taxInvoiceTableGUID = new Guid(item);
				TaxInvoiceTable taxInvoiceTable = taxInvoiceTableRepo.Find(taxInvoiceTableGUID);
				taxInvoiceTableRepo.Remove(taxInvoiceTable);
				base.LogTransactionDelete<TaxInvoiceTable>(taxInvoiceTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Report
		public PrintTaxInvoiceReportView GetPrintTaxInvoiceCopyById(string taxInvoiceGUID)
		{
			try
			{
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				TaxInvoiceTableItemView taxInvoiceModel = taxInvoiceTableRepo.GetByIdvw(taxInvoiceGUID.StringToGuid());
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTable = customerTableRepo.GetByIdvw(taxInvoiceModel.CustomerTableGUID.StringToGuid());
				if (customerTable == null)
				{
					customerTable = new CustomerTableItemView();
				}

				PrintTaxInvoiceReportView printReceiptReportView = new PrintTaxInvoiceReportView
				{
					TaxInvoiceTableGUID = taxInvoiceGUID,
					PrintTaxInvoiceGUID = taxInvoiceGUID,
					CustomerId = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name) ,
					DocumentId = taxInvoiceModel.DocumentId,
					DueDate = taxInvoiceModel.DueDate,
					InvoiceAmount = taxInvoiceModel.InvoiceAmount,
					InvoiceAmountBeforeTax = taxInvoiceModel.InvoiceAmountBeforeTax,
					InvoiceId = taxInvoiceModel.InvoiceTable_Values,
					InvoiceTypeId = taxInvoiceModel.InvoiceType_Values,
					IssuedDate = taxInvoiceModel.IssuedDate,
					TaxAmount = taxInvoiceModel.TaxAmount,
					TaxInvoiceId = taxInvoiceModel.TaxInvoiceId,
					IsCopy = true
				};
				return printReceiptReportView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PrintTaxInvoiceReportView GetPrintTaxInvoiceOriginalById(string taxInvoiceGUID)
		{
			try
			{
				ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
				TaxInvoiceTableItemView taxInvoiceModel = taxInvoiceTableRepo.GetByIdvw(taxInvoiceGUID.StringToGuid());
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTable = customerTableRepo.GetByIdvw(taxInvoiceModel.CustomerTableGUID.StringToGuid());
				if (customerTable == null)
				{
					customerTable = new CustomerTableItemView();
				}

				PrintTaxInvoiceReportView printReceiptReportView = new PrintTaxInvoiceReportView
				{
					TaxInvoiceTableGUID = taxInvoiceGUID,
					PrintTaxInvoiceGUID = taxInvoiceGUID,
					CustomerId = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					DocumentId = taxInvoiceModel.DocumentId,
					DueDate = taxInvoiceModel.DueDate,
					InvoiceAmount = taxInvoiceModel.InvoiceAmount,
					InvoiceAmountBeforeTax = taxInvoiceModel.InvoiceAmountBeforeTax,
					InvoiceId = taxInvoiceModel.InvoiceTable_Values,
					InvoiceTypeId = taxInvoiceModel.InvoiceType_Values,
					IssuedDate = taxInvoiceModel.IssuedDate,
					TaxAmount = taxInvoiceModel.TaxAmount,
					TaxInvoiceId = taxInvoiceModel.TaxInvoiceId,
					IsCopy = false
				};
				return printReceiptReportView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemTaxInvoiceDocumentStatus(SearchParameter search)
        {

				try
				{
					IDocumentService documentService = new DocumentService(db);
					IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
					SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.TaxInvoice);
					search.Conditions.Add(searchCondition);
					return documentStatusRepo.GetDropDownItem(search);
				}
				catch (Exception e)
				{
					throw SmartAppUtil.AddStackTrace(e);
				}
	
		}
        #endregion Report
    }
}
