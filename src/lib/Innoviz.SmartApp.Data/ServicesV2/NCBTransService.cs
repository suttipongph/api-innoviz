using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface INCBTransService
	{

		NCBTransItemView GetNCBTransById(string id);
		NCBTransItemView CreateNCBTrans(NCBTransItemView ncbTransView);
		NCBTransItemView UpdateNCBTrans(NCBTransItemView ncbTransView);
		bool DeleteNCBTrans(string id);
		NCBTransItemView GetNCBTransInitialData(string refId, string refGUID, RefType refType);
		void CreateNCBTransCollection(List<NCBTransItemView> ncbTransItemViews);
	}
	public class NCBTransService : SmartAppService, INCBTransService
	{
		public NCBTransService(SmartAppDbContext context) : base(context) { }
		public NCBTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public NCBTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NCBTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public NCBTransService() : base() { }

		public NCBTransItemView GetNCBTransById(string id)
		{
			try
			{
				INCBTransRepo ncbTransRepo = new NCBTransRepo(db);
				return ncbTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NCBTransItemView CreateNCBTrans(NCBTransItemView ncbTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ncbTransView = accessLevelService.AssignOwnerBU(ncbTransView);
				INCBTransRepo ncbTransRepo = new NCBTransRepo(db);
				NCBTrans ncbTrans = ncbTransView.ToNCBTrans();
				ncbTrans = ncbTransRepo.CreateNCBTrans(ncbTrans);
				base.LogTransactionCreate<NCBTrans>(ncbTrans);
				UnitOfWork.Commit();
				return ncbTrans.ToNCBTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NCBTransItemView UpdateNCBTrans(NCBTransItemView ncbTransView)
		{
			try
			{
				INCBTransRepo ncbTransRepo = new NCBTransRepo(db);
				NCBTrans inputNCBTrans = ncbTransView.ToNCBTrans();
				NCBTrans dbNCBTrans = ncbTransRepo.Find(inputNCBTrans.NCBTransGUID);
				dbNCBTrans = ncbTransRepo.UpdateNCBTrans(dbNCBTrans, inputNCBTrans);
				base.LogTransactionUpdate<NCBTrans>(GetOriginalValues<NCBTrans>(dbNCBTrans), dbNCBTrans);
				UnitOfWork.Commit();
				return dbNCBTrans.ToNCBTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteNCBTrans(string item)
		{
			try
			{
				INCBTransRepo ncbTransRepo = new NCBTransRepo(db);
				Guid ncbTransGUID = new Guid(item);
				NCBTrans ncbTrans = ncbTransRepo.Find(ncbTransGUID);
				ncbTransRepo.Remove(ncbTrans);
				base.LogTransactionDelete<NCBTrans>(ncbTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NCBTransItemView GetNCBTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new NCBTransItemView
				{
					NCBTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateNCBTransCollection(List<NCBTransItemView> ncbTransItemViews)
		{
			try
			{
				if (ncbTransItemViews.Any())
				{
					INCBTransRepo ncbTransRepo = new NCBTransRepo(db);
					List<NCBTrans> ncbTrans = ncbTransItemViews.ToNCBTrans().ToList();
					ncbTransRepo.ValidateAdd(ncbTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(ncbTrans, false);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
	}
}
