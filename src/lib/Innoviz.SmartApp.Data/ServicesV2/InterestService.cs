﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IInterestService
    {
        #region shared
        DateTime FindActualInterestDate(DateTime date, Guid productSubTypeGuid);
        #endregion
    }
    public class InterestService : SmartAppService, IInterestService
    {
        public InterestService(SmartAppDbContext context) : base(context) { }
        public InterestService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public InterestService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public InterestService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public InterestService() : base() { }
        #region shared
        public DateTime FindActualInterestDate(DateTime date, Guid productSubTypeGuid)
        {
            try
            {
                IProductSubTypeService productSubTypeService = new ProductSubTypeService(db);
                return productSubTypeService.FindActualInterestDate(date, productSubTypeGuid);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
