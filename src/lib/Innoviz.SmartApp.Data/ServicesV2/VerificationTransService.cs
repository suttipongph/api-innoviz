using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IVerificationTransService
	{

		VerificationTransItemView GetVerificationTransById(string id);
		VerificationTransItemView CreateVerificationTrans(VerificationTransItemView verificationTransView);
		VerificationTransItemView UpdateVerificationTrans(VerificationTransItemView verificationTransView);
		bool DeleteVerificationTrans(string id);
		VerificationTransItemView GetVerificationTransInitialData(string refId, string refGUID, RefType refType, string documentId);
		IEnumerable<VerificationTrans> CopyVerificationTransactions(string fromRefGUID, RefType fromRefType, string toRefGUID, RefType toRefType, string documentId);
		void CreateVerificationTransCollection(IEnumerable<VerificationTransItemView> VerificationTransItemView);
	}
	public class VerificationTransService : SmartAppService, IVerificationTransService
	{
		public VerificationTransService(SmartAppDbContext context) : base(context) { }
		public VerificationTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public VerificationTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VerificationTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public VerificationTransService() : base() { }

		public VerificationTransItemView GetVerificationTransById(string id)
		{
			try
			{
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				return verificationTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VerificationTransItemView CreateVerificationTrans(VerificationTransItemView verificationTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				verificationTransView = accessLevelService.AssignOwnerBU(verificationTransView);
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				VerificationTrans verificationTrans = verificationTransView.ToVerificationTrans();
				verificationTrans = verificationTransRepo.CreateVerificationTrans(verificationTrans);
				base.LogTransactionCreate<VerificationTrans>(verificationTrans);
				UnitOfWork.Commit();
				return verificationTrans.ToVerificationTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VerificationTransItemView UpdateVerificationTrans(VerificationTransItemView verificationTransView)
		{
			try
			{
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				VerificationTrans inputVerificationTrans = verificationTransView.ToVerificationTrans();
				VerificationTrans dbVerificationTrans = verificationTransRepo.Find(inputVerificationTrans.VerificationTransGUID);
				dbVerificationTrans = verificationTransRepo.UpdateVerificationTrans(dbVerificationTrans, inputVerificationTrans);
				base.LogTransactionUpdate<VerificationTrans>(GetOriginalValues<VerificationTrans>(dbVerificationTrans), dbVerificationTrans);
				UnitOfWork.Commit();
				return dbVerificationTrans.ToVerificationTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteVerificationTrans(string item)
		{
			try
			{
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				Guid verificationTransGUID = new Guid(item);
				VerificationTrans verificationTrans = verificationTransRepo.Find(verificationTransGUID);
				verificationTransRepo.Remove(verificationTrans);
				base.LogTransactionDelete<VerificationTrans>(verificationTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VerificationTransItemView GetVerificationTransInitialData(string refId, string refGUID, RefType refType, string documentId)
		{
			try
			{
				return new VerificationTransItemView
				{
					VerificationTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					DocumentId = documentId
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<VerificationTrans> CopyVerificationTransactions(string fromRefGUID, RefType fromRefType, string toRefGUID, RefType toRefType, string documentId)
		{
			try
			{
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				IEnumerable<VerificationTrans> originVerificationTrans = verificationTransRepo.GetVerificationTransByReferanceNoTracking((int)fromRefType, fromRefGUID.StringToGuid());
				List<VerificationTrans> verificationTransList = new List<VerificationTrans>();

				if (originVerificationTrans.Count() != 0)
				{
					foreach (VerificationTrans item in originVerificationTrans)
					{
						verificationTransList.Add(new VerificationTrans
						{
							CompanyGUID = item.CompanyGUID,
							RefGUID = toRefGUID.StringToGuid(),
							RefType = (int)toRefType,
							VerificationTableGUID = item.VerificationTableGUID,
							DocumentId = documentId
						});
					}
				}
				return verificationTransList;
            }
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Migration
		public void CreateVerificationTransCollection(IEnumerable<VerificationTransItemView> VerificationTransItemView)
		{
			try
			{
				if (VerificationTransItemView.Any())
				{
					IVerificationTransRepo VerificationTransRepo = new VerificationTransRepo(db);
					List<VerificationTrans> VerificationTranss = VerificationTransItemView.ToVerificationTrans().ToList();
					VerificationTransRepo.ValidateAdd(VerificationTranss);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(VerificationTranss, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion Migration

	}
}
