using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IServiceFeeConditionTransService
	{

		ServiceFeeConditionTransItemView GetServiceFeeConditionTransById(string id);
		ServiceFeeConditionTransItemView CreateServiceFeeConditionTrans(ServiceFeeConditionTransItemView serviceFeeConditionTransView);
		ServiceFeeConditionTransItemView UpdateServiceFeeConditionTrans(ServiceFeeConditionTransItemView serviceFeeConditionTransView);
		bool DeleteServiceFeeConditionTrans(string id);
		ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialDataByCreditAppTable(string refId, string refGUID, RefType refType);
		ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialDataByCreditAppRequestTable(string refId, string refGUID, RefType refType);
		ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialDataByCreditAppRequestLine(string refId, string refGUID, RefType refType);
		void CreateServiceFeeConditionTransCollection(List<ServiceFeeConditionTransItemView> serviceFeeConditionTransItemViews);
		#region K2 Function
		ServiceFeeConditionTrans GenServiceFeeCondCreditReqFeeByCAReq(Guid creditAppRequestTableGUID, string owner = null, Guid? ownerBusinessUnitGUID = null);
		IEnumerable<ServiceFeeConditionTrans> CopyServiceFeeConditionTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, string owner = null, Guid? ownerBusinessUnitGUID = null, int? customOrdering = null);
		#endregion
	}
    public class ServiceFeeConditionTransService : SmartAppService, IServiceFeeConditionTransService
	{
		public ServiceFeeConditionTransService(SmartAppDbContext context) : base(context) { }
		public ServiceFeeConditionTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ServiceFeeConditionTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ServiceFeeConditionTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ServiceFeeConditionTransService() : base() { }

		public ServiceFeeConditionTransItemView GetServiceFeeConditionTransById(string id)
		{
			try
			{
				IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
				return serviceFeeConditionTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeConditionTransItemView CreateServiceFeeConditionTrans(ServiceFeeConditionTransItemView serviceFeeConditionTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				serviceFeeConditionTransView = accessLevelService.AssignOwnerBU(serviceFeeConditionTransView);
				IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
				ServiceFeeConditionTrans serviceFeeConditionTrans = serviceFeeConditionTransView.ToServiceFeeConditionTrans();
				serviceFeeConditionTrans = serviceFeeConditionTransRepo.CreateServiceFeeConditionTrans(serviceFeeConditionTrans);
				base.LogTransactionCreate<ServiceFeeConditionTrans>(serviceFeeConditionTrans);
				UnitOfWork.Commit();
				return serviceFeeConditionTrans.ToServiceFeeConditionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeConditionTransItemView UpdateServiceFeeConditionTrans(ServiceFeeConditionTransItemView serviceFeeConditionTransView)
		{
			try
			{
				IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
				ServiceFeeConditionTrans inputServiceFeeConditionTrans = serviceFeeConditionTransView.ToServiceFeeConditionTrans();
				ServiceFeeConditionTrans dbServiceFeeConditionTrans = serviceFeeConditionTransRepo.Find(inputServiceFeeConditionTrans.ServiceFeeConditionTransGUID);
				dbServiceFeeConditionTrans = serviceFeeConditionTransRepo.UpdateServiceFeeConditionTrans(dbServiceFeeConditionTrans, inputServiceFeeConditionTrans);
				base.LogTransactionUpdate<ServiceFeeConditionTrans>(GetOriginalValues<ServiceFeeConditionTrans>(dbServiceFeeConditionTrans), dbServiceFeeConditionTrans);
				UnitOfWork.Commit();
				return dbServiceFeeConditionTrans.ToServiceFeeConditionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteServiceFeeConditionTrans(string item)
		{
			try
			{
				IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
				Guid serviceFeeConditionTransGUID = new Guid(item);
				ServiceFeeConditionTrans serviceFeeConditionTrans = serviceFeeConditionTransRepo.Find(serviceFeeConditionTransGUID);
				serviceFeeConditionTransRepo.Remove(serviceFeeConditionTrans);
				base.LogTransactionDelete<ServiceFeeConditionTrans>(serviceFeeConditionTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialDataByCreditAppTable(string refId, string refGUID, RefType refType)
		{
			try
			{
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(refGUID.StringToGuid());
				return new ServiceFeeConditionTransItemView
				{
					ServiceFeeConditionTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					CreditAppRequestTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
					CreditAppRequestTable_CreditAppRequestId = creditAppTable.CreditAppId,
					InvoiceRevenueType_ProductType = creditAppTable.ProductType,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialDataByCreditAppRequestTable(string refId, string refGUID, RefType refType)
		{
			try
			{
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(refGUID.StringToGuid());
				return new ServiceFeeConditionTransItemView
				{
					ServiceFeeConditionTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString(),
					CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
					InvoiceRevenueType_ProductType = creditAppRequestTable.ProductType,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialDataByCreditAppRequestLine(string refId, string refGUID, RefType refType)
		{
			try
			{
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTracking(refGUID.StringToGuid());
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(creditAppRequestLine.CreditAppRequestTableGUID);

				return new ServiceFeeConditionTransItemView
				{
					ServiceFeeConditionTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString(),
					CreditAppRequestTable_CreditAppRequestId = creditAppRequestTable.CreditAppRequestId,
					InvoiceRevenueType_ProductType = creditAppRequestTable.ProductType
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateServiceFeeConditionTransCollection(List<ServiceFeeConditionTransItemView> serviceFeeConditionTransItemViews)
		{
			try
			{
				if (serviceFeeConditionTransItemViews.Any())
				{
					IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
					List<ServiceFeeConditionTrans> serviceFeeConditionTrans = serviceFeeConditionTransItemViews.ToServiceFeeConditionTrans().ToList();
					serviceFeeConditionTransRepo.ValidateAdd(serviceFeeConditionTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(serviceFeeConditionTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#region K2 function
		public ServiceFeeConditionTrans GenServiceFeeCondCreditReqFeeByCAReq(Guid creditAppRequestTableGUID, string owner = null, Guid? ownerBusinessUnitGUID = null)
        {
			try
			{
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(creditAppRequestTableGUID);
				ServiceFeeConditionTrans result = new ServiceFeeConditionTrans();
				if (creditAppRequestTable != null && creditAppRequestTable.CreditRequestFeeAmount != 0)
				{
					return serviceFeeConditionTransRepo.GenServiceFeeCondCreditReqFeeByCAReq(creditAppRequestTableGUID, owner, ownerBusinessUnitGUID);
				}
				else
				{
					return null;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<ServiceFeeConditionTrans> CopyServiceFeeConditionTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, string owner = null, Guid? ownerBusinessUnitGUID = null, int? customOrdering = null)
		{
			try
			{
				IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
				List<ServiceFeeConditionTrans> ServiceFeeConditionTrans = serviceFeeConditionTransRepo.GetServiceFeeConditionTransByRefernceNoTracking(fromRefGUID, fromRefType).ToList();
				if (ServiceFeeConditionTrans.Any())
				{
					if(fromInactive != null)
                    {
						ServiceFeeConditionTrans = ServiceFeeConditionTrans.Where(w => w.Inactive == fromInactive.Value).ToList();
					}
					List<ServiceFeeConditionTrans> toServiceFeeConditionTrans = serviceFeeConditionTransRepo.GetServiceFeeConditionTransByRefernceNoTracking(toRefGUID, toRefType).ToList();

					int ordering = (customOrdering.HasValue) ? customOrdering.Value + 1
															 : toServiceFeeConditionTrans.Any() 
																? toServiceFeeConditionTrans.Max(m => m.Ordering) + 1
																: 1;
                    foreach (ServiceFeeConditionTrans f in ServiceFeeConditionTrans.OrderBy(o => o.Ordering).ToList())
                    {
						f.ServiceFeeConditionTransGUID = Guid.NewGuid();
						f.RefGUID = toRefGUID;
						f.RefType = toRefType;
						f.Ordering = ordering;
						f.Owner = owner;
						f.OwnerBusinessUnitGUID = ownerBusinessUnitGUID;
						ordering++;
					}
				}
				return ServiceFeeConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
