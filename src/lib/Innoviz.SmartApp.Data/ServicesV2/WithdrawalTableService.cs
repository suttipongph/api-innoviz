using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.ReportViewer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IWithdrawalTableService
	{

		WithdrawalLineItemView GetWithdrawalLineById(string id);
		WithdrawalLineItemView CreateWithdrawalLine(WithdrawalLineItemView withdrawalLineView);
		WithdrawalLineItemView UpdateWithdrawalLine(WithdrawalLineItemView withdrawalLineView);
		bool DeleteWithdrawalLine(string id);
		WithdrawalTableItemView GetWithdrawalTableById(string id);
		WithdrawalTableItemView CreateWithdrawalTable(WithdrawalTableItemView withdrawalTableView);
		WithdrawalTable CreateWithdrawalTable(WithdrawalTable withdrawalTable);
		WithdrawalTableItemView UpdateWithdrawalTable(WithdrawalTableItemView withdrawalTableView);
		WithdrawalTable UpdateWithdrawalTable(WithdrawalTable withdrawalTable);
		bool DeleteWithdrawalTable(string id);
		IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemWithdrawalTableStatus(SearchParameter search);
		AccessModeView GetWithdrawalTableAccessMode(string withdrawalTableGUID);
		bool IsManualByWithdrawalTable(string companyId);
		bool IsWithdrawalLineByWithdrawalTableEmpty(string withdrawalTableGUID);
		WithdrawalTableItemView GetWithdrawalTableInitialDataByCreditAppTable(CreditAppTable creditAppTable);
		WithdrawalTableItemView GetWithdrawalTableByWithdrawalDate(WithdrawalTableItemView input);
		WithdrawalTableItemView GetWithdrawalTableByTermExtensionInvoiceRevenueType(WithdrawalTableItemView input);
		AccessModeView GetReceiptTempTableAccessMode(string wTableithdrawalGUID);
		PrintWithdrawalTable GetPrintWithdrawalTableById(string withdrawalTableGUID);
		PrintWithdrawalTermExtension GetPrintPrintWithdrawalTermExtensionById(string withdrawalTableGUID);
		bool GetWithdrawalTermExtensionDisable(string id);
		MemoTransItemView GetMemoTransInitialData(string refGUID);
		#region RecalculateInterest
		RecalWithdrawalInterestViewMap InitRecalWithdrawalInterest(RecalWithdrawalInterestParamView parm);
		RecalWithdrawalInterestViewMap InitRecalWithdrawalInterest(WithdrawalTable withdrawalTable);
		RecalWithdrawalInterestResultView RecalWithdrawalInterest(RecalWithdrawalInterestParamView parm);
		RecalWithdrawalInterestParamView GetRecalWithdrawalInterestById(string withdrawalTableGUID);
		#endregion

		#region shared
		decimal CalcPDCInterestOutstanding(Guid orgWithdrawalTableGUID);
		decimal CalcPDCInterestOutstanding(IEnumerable<CustTrans> custTransCalcPDCInterestOutstanding);
		decimal CalcWithdrawalNetPaidByWithdrawalTable(Guid WithdrawalTableGUID);
		decimal CalcTermExtensionFeeAmount(Guid companyGUID, Guid creditTermGUID, Guid? termExtensionInvoiceRevenueTypeGUID = null);
		#endregion

		#region Inquiry withdrawal line outstand
		InquiryWithdrawalLineOutstandItemView GetInquiryWithdrawalLineOutstandById(string id);
		CollectionFollowUpItemView GetCollectionFollowUpInitialData(string refGUID);
		#endregion

		#region Payment detail
		PaymentDetailItemView GetPaymentDetailInitialData(string refGUID);
		#endregion
		#region  Verification trans
		VerificationTransItemView GetVerificationTransInitialData(string refGUID);
		#endregion
		#region Service fee trans
		ServiceFeeTransItemView GetServiceFeeTransInitialData(string refGUID);
		#endregion
		#region function
		#region extendTerm
		ExtendTermParamView GetExtendTermById(string withdrawalTableGUID);
		bool GetExtendTermValidation(ExtendTermParamView view);
		ExtendTermResultView ExtendTerm(ExtendTermParamView view);
		#endregion
		#region post withdrawal
		PostWithdrawalView GetPostWithdrawalById(string withdrawalTableGUID);
		PostWithdrawalViewMap InitPostWithdrawal(PostWithdrawalView view);
		PostWithdrawalViewMap InitPostWithdrawal(WithdrawalTable withdrawalTable);
		PostWithdrawalResultView PostWithdrawal(PostWithdrawalView view);
		bool GetPostWithdrawalValidation(WithdrawalTable withdrawalTable);
		#endregion
		#region cancel withdrawal
		CancelWithdrawalView GetCancelWithdrawalById(string withdrawalTableGUID);
		CancelWithdrawalResultView CancelWithdrawal(CancelWithdrawalView withdrawalTable);
		#endregion
		#endregion
		void CreateWithdrawalTableCollection(IEnumerable<WithdrawalTableItemView> WithdrawalTableItemView);
		void CreateWithdrawalLineCollection(List<WithdrawalLineItemView> withdrawalLineItemViews);

		#region buyer agreement trans
		BuyerAgreementTransItemView GetBuyerAgreementTransInitialData(string refGUID);
		#endregion
		FileInformation RenderReportWithdrawalTermExtension(RptReportWithdrawalTermExtensionReportView rptReportWithdrawalTermExtensionReportView);
	}
    public class WithdrawalTableService : SmartAppService, IWithdrawalTableService
	{
		public WithdrawalTableService(SmartAppDbContext context) : base(context) { }
		public WithdrawalTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public WithdrawalTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public WithdrawalTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public WithdrawalTableService() : base() { }

		public WithdrawalLineItemView GetWithdrawalLineById(string id)
		{
			try
			{
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				return withdrawalLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalLineItemView CreateWithdrawalLine(WithdrawalLineItemView withdrawalLineView)
		{
			try
			{
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				WithdrawalLine withdrawalLine = withdrawalLineView.ToWithdrawalLine();
				withdrawalLine = CreateWithdrawalLine(withdrawalLine);
				UnitOfWork.Commit();
				return withdrawalLine.ToWithdrawalLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalLine CreateWithdrawalLine(WithdrawalLine withdrawalLine)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				withdrawalLine = accessLevelService.AssignOwnerBU(withdrawalLine);
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				withdrawalLine = withdrawalLineRepo.CreateWithdrawalLine(withdrawalLine);
				base.LogTransactionCreate<WithdrawalLine>(withdrawalLine);				
				return withdrawalLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalLineItemView UpdateWithdrawalLine(WithdrawalLineItemView withdrawalLineView)
		{
			try
			{
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				WithdrawalLine inputWithdrawalLine = withdrawalLineView.ToWithdrawalLine();
				WithdrawalLine dbWithdrawalLine = withdrawalLineRepo.Find(inputWithdrawalLine.WithdrawalLineGUID);
				dbWithdrawalLine = withdrawalLineRepo.UpdateWithdrawalLine(dbWithdrawalLine, inputWithdrawalLine);
				base.LogTransactionUpdate<WithdrawalLine>(GetOriginalValues<WithdrawalLine>(dbWithdrawalLine), dbWithdrawalLine);
				UnitOfWork.Commit();
				return dbWithdrawalLine.ToWithdrawalLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteWithdrawalLine(string item)
		{
			try
			{
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				Guid withdrawalLineGUID = new Guid(item);
				WithdrawalLine withdrawalLine = withdrawalLineRepo.Find(withdrawalLineGUID);
				withdrawalLineRepo.Remove(withdrawalLine);
				base.LogTransactionDelete<WithdrawalLine>(withdrawalLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public WithdrawalTableItemView GetWithdrawalTableById(string id)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTableItemView withdrawalTableItemView = withdrawalTableRepo.GetByIdvw(id.StringToGuid());
				withdrawalTableItemView.NetPaid = (withdrawalTableItemView.TermExtension == false) ? CalcWithdrawalNetPaidByWithdrawalTable(withdrawalTableItemView.WithdrawalTableGUID.StringToGuid()) : 0;
				return withdrawalTableItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTableItemView CreateWithdrawalTable(WithdrawalTableItemView withdrawalTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				withdrawalTableView = accessLevelService.AssignOwnerBU(withdrawalTableView);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableView.ToWithdrawalTable();
				GenWithdrawalTableNumberSeqCode(withdrawalTable);
				using (var transaction = UnitOfWork.ContextTransaction())
				{
					withdrawalTable.WithdrawalTableGUID = Guid.NewGuid();
					withdrawalTableRepo.ValidateAdd(withdrawalTable);
					this.BulkInsert(new List<WithdrawalTable> { withdrawalTable });
					RecalWithdrawalInterestViewMap recalWithdrawalInterest = InitRecalWithdrawalInterest(withdrawalTable);
					this.BulkInsert(recalWithdrawalInterest.WithdrawalLines);
					base.LogTransactionCreate<WithdrawalTable>(withdrawalTable);
					UnitOfWork.Commit(transaction);
				}
			
				return withdrawalTable.ToWithdrawalTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTable CreateWithdrawalTable(WithdrawalTable withdrawalTable)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				withdrawalTable = accessLevelService.AssignOwnerBU(withdrawalTable);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				withdrawalTable = withdrawalTableRepo.CreateWithdrawalTable(withdrawalTable);
				base.LogTransactionCreate<WithdrawalTable>(withdrawalTable);
				return withdrawalTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTableItemView UpdateWithdrawalTable(WithdrawalTableItemView withdrawalTableView)
		{
			try
			{
				WithdrawalTable inputWithdrawalTable = withdrawalTableView.ToWithdrawalTable();
				WithdrawalTable dbWithdrawalTable = UpdateWithdrawalTable(inputWithdrawalTable);
				UnitOfWork.Commit();
				return dbWithdrawalTable.ToWithdrawalTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTable UpdateWithdrawalTable(WithdrawalTable withdrawalTable)
        {
            try
            {
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable dbWithdrawalTable = withdrawalTableRepo.Find(withdrawalTable.WithdrawalTableGUID);
				dbWithdrawalTable = withdrawalTableRepo.UpdateWithdrawalTable(dbWithdrawalTable, withdrawalTable);
				base.LogTransactionUpdate<WithdrawalTable>(GetOriginalValues<WithdrawalTable>(dbWithdrawalTable), dbWithdrawalTable);
				return dbWithdrawalTable;
			}
			catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public bool DeleteWithdrawalTable(string item)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				Guid withdrawalTableGUID = new Guid(item);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.Find(withdrawalTableGUID);
				withdrawalTableRepo.Remove(withdrawalTable);
				base.LogTransactionDelete<WithdrawalTable>(withdrawalTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				search = search.GetParentCondition(WithdrawalTableCondition.CreditAppTableGUID);
				string Cancelled = ((int)WithdrawalStatus.Cancelled).ToString();
				List<SearchCondition> searchConditions = SearchConditionService.GetWithdrawalTableNotStatusCondition(new string[] { Cancelled });
				search.Conditions.AddRange(searchConditions);
				return withdrawalTableRepo.GetDropDownItemByMainAgreementTable(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemWithdrawalTableStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Withdrawal);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AccessModeView GetWithdrawalTableAccessMode(string withdrawalTableGUID)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(withdrawalTable.DocumentStatusGUID);

				bool condition = Convert.ToInt32(documentStatus.StatusId) <= Convert.ToInt32(WithdrawalStatus.Draft);

				return new AccessModeView
				{
					CanCreate = condition,
					CanDelete = condition,
					CanView = condition,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenWithdrawalTableNumberSeqCode(WithdrawalTable withdrawalTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = IsManualByWithdrawalTable(withdrawalTable.CompanyGUID.GuidNullToString());
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(withdrawalTable.CompanyGUID, ReferenceId.Withdrawal);
					withdrawalTable.WithdrawalId = numberSequenceService.GetNumber(withdrawalTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public bool IsManualByWithdrawalTable(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.Withdrawal);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsWithdrawalLineByWithdrawalTableEmpty(string withdrawalTableGUID)
		{
			try
			{
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				IEnumerable<WithdrawalLine> withdrawalLines = withdrawalLineRepo.GetWithdrawalLineByWithdrawalTableNoTracking(withdrawalTableGUID.StringToGuid());
				if (withdrawalLines.Count() > 0)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTableItemView GetWithdrawalTableInitialDataByCreditAppTable(CreditAppTable creditAppTable)
		{
			try
			{
				IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
				RetentionConditionTrans retentionConditionTrans = retentionConditionTransService
					.GetRetentionConditionTransByReferenceAndMethod(creditAppTable.CreditAppTableGUID, (int)RetentionDeductionMethod.Withdrawal).FirstOrDefault();

				DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(Convert.ToInt32(WithdrawalStatus.Draft).ToString());

				CustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(creditAppTable.CustomerTableGUID);

				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
				string operReportSignatureGUID = null;
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(creditAppTable.CompanyGUID);
				if(companyParameter.OperReportSignatureGUID != null)
                {
					EmployeeTable employeeTable = employeeTableRepo.GetByEmployeeGUIDNoTracking(companyParameter.OperReportSignatureGUID.Value);
					if(employeeTable.InActive == false)
                    {
						operReportSignatureGUID = employeeTable.EmployeeTableGUID.GuidNullToString();
					}
				}

				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);

				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				int creditTermDay = 0;
				if (creditAppTable.CreditTermGUID != null)
                {
					CreditTerm creditTerm = creditTermRepo.GetCreditTermByIdNoTracking(creditAppTable.CreditTermGUID.Value);
					creditTermDay = creditTerm.NumberOfDays;
				}

				return new WithdrawalTableItemView
				{
					WithdrawalTableGUID = new Guid().GuidNullToString(),
					CreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
					CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
					ProductType = creditAppTable.ProductType,
					CustomerTableGUID = customerTable.CustomerTableGUID.GuidNullToString(),
					CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					TotalInterestPct = creditAppTable.TotalInterestPct,
					CreditTermGUID = creditAppTable.CreditTermGUID.GuidNullToString(),
					Dimension1GUID = creditAppTable.Dimension1GUID.GuidNullToString(),
					Dimension2GUID = creditAppTable.Dimension2GUID.GuidNullToString(),
					Dimension3GUID = creditAppTable.Dimension3GUID.GuidNullToString(),
					Dimension4GUID = creditAppTable.Dimension4GUID.GuidNullToString(),
					Dimension5GUID = creditAppTable.Dimension5GUID.GuidNullToString(),
					RetentionCalculateBase = (retentionConditionTrans != null) ? retentionConditionTrans.RetentionCalculateBase : (int)RetentionCalculateBase.None,
					RetentionPct = (retentionConditionTrans != null) ? retentionConditionTrans.RetentionPct :0,
					RetentionAmount = 0,
					DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
					DocumentStatus_StatusId = documentStatus.StatusId,
					DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
					WithdrawalDate = DateTime.Now.DateToString(),
					DueDate = DateTime.Now.AddDays(creditTermDay).DateToString(),
					OperationMarkComment = true,
					CreditMarkComment = true,
					MarketingMarkComment = true,
					InterestCutDay = companyParameter.PFInterestCutDay,

					CreditLimitType_CreditLimitConditionType = creditLimitType.CreditLimitConditionType,
					CreditLimitType_ValidateBuyerAgreement = creditLimitType.ValidateBuyerAgreement,
					// share
					TermExtension = false,
					NumberOfExtension = 0,
					OperReportSignatureGUID = operReportSignatureGUID
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public WithdrawalTableItemView GetWithdrawalTableByWithdrawalDate(WithdrawalTableItemView input)
		{
			try
			{
				if (input.WithdrawalDate != null && input.CreditTermGUID != null)
                {
					ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
					CreditTerm creditTerm = creditTermRepo.GetCreditTermByIdNoTracking(input.CreditTermGUID.StringToGuid());
					input.DueDate = input.WithdrawalDate.StringToDate().AddDays(creditTerm.NumberOfDays).DateToString();
                }

				return input;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public WithdrawalTableItemView GetWithdrawalTableByTermExtensionInvoiceRevenueType(WithdrawalTableItemView input)
		{
			try
			{
				if (input.TermExtensionInvoiceRevenueTypeGUID != null && input.CreditTermGUID != null)
				{
					input.TermExtensionFeeAmount = CalcTermExtensionFeeAmount(input.CompanyGUID.StringToGuid(), input.CreditTermGUID.StringToGuid(), input.TermExtensionInvoiceRevenueTypeGUID.StringToGuidNull());
				}
				else
                {
					input.TermExtensionFeeAmount = 0;
				}

				return input;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetWithdrawalTermExtensionDisable(string id)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(id.StringToGuid());
				DocumentStatus withdrawalStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(withdrawalTable.DocumentStatusGUID);
				return !(withdrawalStatus.StatusId == ((int)WithdrawalStatus.Posted).ToString());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public MemoTransItemView GetMemoTransInitialData(string refGUID)
		{
			try
			{
				IMemoTransService memoTransService = new MemoTransService(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				string refId = withdrawalTableRepo.GetWithdrawalTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).WithdrawalId;
				return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.WithdrawalTable);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Recalculate interest
		private bool ValidateRecalWithdrawalInterest(WithdrawalTable withdrawalTable)
        {
            try
            {
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus withdrawalStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(withdrawalTable.DocumentStatusGUID);
				// Validate No.4
				ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
				decimal creditLimitBalanceByCreditConditionType = creditAppTableService.GetCreditLimitBalanceByCreditConditionType(withdrawalTable.CreditAppTableGUID.ToString(), withdrawalTable.WithdrawalDate.DateToString());

				// Validate No.5
				decimal creditLimitBalanceByCreditAppLine = withdrawalTable.CreditAppLineGUID.HasValue ? creditAppTableService.GetCreditLimitBalanceByCreditAppLine(withdrawalTable.CreditAppLineGUID.ToString()) : 0;

				// Validate No.6
				IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
				IEnumerable<BuyerCreditLimitByProduct> buyerCreditLimitByProducts = buyerCreditLimitByProductRepo.GetBuyerCreditLimitByProductByBuyer(withdrawalTable.BuyerTableGUID.HasValue ? withdrawalTable.BuyerTableGUID.Value : Guid.Empty)
																												 .Where(w => w.ProductType == withdrawalTable.ProductType);
				bool isBuyerCreditLimitByProducts = ConditionService.IsNotZero(buyerCreditLimitByProducts.Count());
				decimal creditLimitBalanceByProduct = creditAppTableService.GetCreditLimitBalanceByProduct(withdrawalTable.BuyerTableGUID.ToString(), (ProductType)withdrawalTable.ProductType);

				if (withdrawalStatus.StatusId != ((int)WithdrawalStatus.Draft).ToString() || withdrawalTable.TermExtension)
                {
					ex.AddData("ERROR.90036", "LABEL.WITHDRAWAL");
                }
				if(withdrawalTable.WithdrawalAmount <= 0)
                {
					ex.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.WITHDRAWAL_AMOUNT");
                }
				// R04
				if (withdrawalTable.InterestCutDay <= 0)
				{
					ex.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.INTEREST_CUT_DAY");
				}
				if (!withdrawalTable.TermExtension)
				{
					if (withdrawalTable.WithdrawalAmount > creditLimitBalanceByCreditConditionType) //Validate No.4
					{
						ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_LIMIT_CONDITION_TYPE", creditLimitBalanceByCreditConditionType.ToString() });
					}
					if (withdrawalTable.CreditAppLineGUID.HasValue)
					{
						if (withdrawalTable.WithdrawalAmount > creditLimitBalanceByCreditAppLine) //Validate No.5
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_APPLICATION_LINE", creditLimitBalanceByCreditAppLine.ToString() });
						}
						if (isBuyerCreditLimitByProducts && withdrawalTable.WithdrawalAmount > creditLimitBalanceByProduct) //Validate No.6
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.BUYER_CREDIT_LIMIT_BY_PRODUCT", creditLimitBalanceByProduct.ToString() });
						}
					}
				}
				if (ex.MessageList.Count() > 1)
                {
					throw ex;
                }
				return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public RecalWithdrawalInterestViewMap InitRecalWithdrawalInterest(RecalWithdrawalInterestParamView parm)
        {
            try
            {
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(parm.WithdrawalTableGUID.StringToGuid());
				if (string.IsNullOrEmpty(parm.CreditTermGUID) || string.IsNullOrEmpty(parm.WithdrawalDate))
				{
					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("Invalid parameter values.");
					throw ex;
				}
				// set withdrawalTable
				withdrawalTable.WithdrawalDate = parm.WithdrawalDate.StringToDate();
				withdrawalTable.CreditTermGUID = parm.CreditTermGUID.StringToGuid();
				withdrawalTable.WithdrawalAmount = parm.WithdrawalAmount;
				return InitRecalWithdrawalInterest(withdrawalTable);
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public RecalWithdrawalInterestViewMap InitRecalWithdrawalInterest(WithdrawalTable withdrawalTable)
        {
            try
            {
				RecalWithdrawalInterestViewMap result = new RecalWithdrawalInterestViewMap();
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);

				List<WithdrawalLine> withdrawalLineToDelete = withdrawalLineRepo.GetWithdrawalLineByWithdrawalTableGuid(withdrawalTable.WithdrawalTableGUID).ToList();
				result.OrigWithdrawalLines = withdrawalLineToDelete;

				if (ValidateRecalWithdrawalInterest(withdrawalTable))
                {
					#region set withdrawal table due date
					CreditTerm creditTerm = creditTermRepo.GetCreditTermByIdNoTracking(withdrawalTable.CreditTermGUID);
					withdrawalTable.DueDate = withdrawalTable.WithdrawalDate.AddDays(creditTerm.NumberOfDays);
					result.WithdrawalTable = withdrawalTable;
					#endregion
					#region create withdrawal lines
					result.WithdrawalLines = CreateWithdrawalLinesForRecalInterest(withdrawalTable);
					#endregion
				}
				return result;
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public RecalWithdrawalInterestResultView RecalWithdrawalInterest(RecalWithdrawalInterestParamView parm)
        {
            try
            {
				RecalWithdrawalInterestResultView result = new RecalWithdrawalInterestResultView();
				RecalWithdrawalInterestViewMap recalWithdrawalInterest = InitRecalWithdrawalInterest(parm);

				recalWithdrawalInterest.WithdrawalTable = UpdateWithdrawalTable(recalWithdrawalInterest.WithdrawalTable);

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					// delete old lines
					if(recalWithdrawalInterest.OrigWithdrawalLines != null && 
						recalWithdrawalInterest.OrigWithdrawalLines.Count() > 0)
                    {
						this.BulkDelete(recalWithdrawalInterest.OrigWithdrawalLines);
                    }

					this.BulkInsert(recalWithdrawalInterest.WithdrawalLines);
					UnitOfWork.Commit(transaction);
				}

				NotificationResponse success = new NotificationResponse();
				success.AddData("SUCCESS.90010", new string[] { recalWithdrawalInterest.WithdrawalTable.WithdrawalId });
				result.Notification = success;
				return result;

			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public RecalWithdrawalInterestParamView GetRecalWithdrawalInterestById(string withdrawalTableGUID)
        {
            try
            {
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				var withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalTableGUID.StringToGuid());
				RecalWithdrawalInterestParamView result = new RecalWithdrawalInterestParamView
				{
					WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID.GuidNullToString(),
					CreditTermGUID = withdrawalTable.CreditTermGUID.GuidNullToString(),
					WithdrawalAmount = withdrawalTable.WithdrawalAmount,
					WithdrawalId = SmartAppUtil.GetDropDownLabel(withdrawalTable.WithdrawalId, withdrawalTable.Description),
					DueDate = withdrawalTable.DueDate.DateToString(),
					TotalInterestPct = withdrawalTable.TotalInterestPct,
					WithdrawalDate = withdrawalTable.WithdrawalDate.DateToString()
				};
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private List<WithdrawalLine> CreateWithdrawalLinesForRecalInterest(WithdrawalTable withdrawalTable)
        {
            try
            {
				List<WithdrawalLine> withdrawalLines = new List<WithdrawalLine>();
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				IInterestService interestService = new InterestService(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);

				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(withdrawalTable.CompanyGUID);

				int lineNum = 1;

				#region InterestType = WithdrawalLineType.Principal
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(withdrawalTable.CreditAppTableGUID);
				DateTime prinInterestDate = interestService.FindActualInterestDate(withdrawalTable.DueDate, creditAppTable.ProductSubTypeGUID);
				int prinInterestDay = Convert.ToInt32((prinInterestDate - withdrawalTable.WithdrawalDate).TotalDays);

				decimal prinInterestAmount = companyParameter.DaysPerYear == 0 ? 0.0m :
					DataTypeHandler.Round(((withdrawalTable.WithdrawalAmount * (withdrawalTable.TotalInterestPct / 100)) / companyParameter.DaysPerYear) * prinInterestDay);

				WithdrawalLine principalLine = new WithdrawalLine
				{
					WithdrawalLineGUID = Guid.NewGuid(),
					WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
					WithdrawalLineType = (int)WithdrawalLineType.Principal,
					StartDate = withdrawalTable.WithdrawalDate,
					DueDate = withdrawalTable.DueDate,
					InterestDate = prinInterestDate,
					InterestDay = prinInterestDay,
					InterestAmount = prinInterestAmount,
					WithdrawalLineInvoiceTableGUID = null,
					ClosedForTermExtension = false,
					CustomerPDCTableGUID = null,
					BuyerPDCTableGUID = null,
					BillingDate = null,
					CollectionDate = withdrawalTable.DueDate,
					Dimension1GUID = withdrawalTable.Dimension1GUID,
					Dimension2GUID = withdrawalTable.Dimension2GUID,
					Dimension3GUID = withdrawalTable.Dimension3GUID,
					Dimension4GUID = withdrawalTable.Dimension4GUID,
					Dimension5GUID = withdrawalTable.Dimension5GUID,
					CompanyGUID = withdrawalTable.CompanyGUID,
					Owner = GetUserName(),
					OwnerBusinessUnitGUID = GetCurrentOwnerBusinessUnitGUID().StringToGuid(),
					LineNum = lineNum
				};
				withdrawalLines.Add(principalLine);
				#endregion

				#region InterestType = WithdrawalLineType.Interest
				DateTime startDate = withdrawalTable.WithdrawalDate;
				DateTime interestDate = DateTime.MinValue;

				while (interestDate < principalLine.InterestDate)
				{
					lineNum += 1;
					var intDate = startDate.Day < withdrawalTable.InterestCutDay ?
						startDate : startDate.AddMonths(1);
					interestDate = new DateTime(intDate.Year, intDate.Month, withdrawalTable.InterestCutDay);

					if (interestDate > principalLine.InterestDate)
					{
						interestDate = principalLine.InterestDate;
					}

					int interestDay = Convert.ToInt32((interestDate - startDate).TotalDays);

					#region interestAmount
					decimal interestAmount;
					if (companyParameter.DaysPerYear == 0)
					{
						interestAmount = 0.0m;
					}
					else
					{
						if (interestDate == principalLine.InterestDate)
						{
							interestAmount = principalLine.InterestAmount -
								withdrawalLines.Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Interest &&
															w.InterestDate < interestDate)
												.Sum(s => s.InterestAmount);
						}
						else
						{
							interestAmount =
								DataTypeHandler.Round((
									(withdrawalTable.WithdrawalAmount * (withdrawalTable.TotalInterestPct / 100)) / companyParameter.DaysPerYear)
									* interestDay);
						}
					}
					#endregion
					WithdrawalLine interestLine = new WithdrawalLine
					{
						WithdrawalLineGUID = Guid.NewGuid(),
						WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
						WithdrawalLineType = (int)WithdrawalLineType.Interest,
						StartDate = startDate,
						DueDate = startDate,
						InterestDate = interestDate,
						InterestDay = interestDay,
						InterestAmount = interestAmount,
						WithdrawalLineInvoiceTableGUID = null,
						ClosedForTermExtension = false,
						CustomerPDCTableGUID = null,
						BuyerPDCTableGUID = null,
						BillingDate = null,
						CollectionDate = startDate,
						Dimension1GUID = withdrawalTable.Dimension1GUID,
						Dimension2GUID = withdrawalTable.Dimension2GUID,
						Dimension3GUID = withdrawalTable.Dimension3GUID,
						Dimension4GUID = withdrawalTable.Dimension4GUID,
						Dimension5GUID = withdrawalTable.Dimension5GUID,
						CompanyGUID = withdrawalTable.CompanyGUID,
						Owner = GetUserName(),
						OwnerBusinessUnitGUID = GetCurrentOwnerBusinessUnitGUID().StringToGuid(),
						LineNum = lineNum
					};
					withdrawalLines.Add(interestLine);
					startDate = interestDate;
				}
				#endregion
				return withdrawalLines;
			}
			catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region shared
		public decimal CalcPDCInterestOutstanding(Guid orgWithdrawalTableGUID)
        {
            try
            {
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				IEnumerable<CustTrans> custTrans = custTransRepo.GetCustTransCalcPDCInterestOutstanding(orgWithdrawalTableGUID);
				return CalcPDCInterestOutstanding(custTrans);
			}
			catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public decimal CalcPDCInterestOutstanding(IEnumerable<CustTrans> custTransCalcPDCInterestOutstanding)
		{
			try
			{
				return (custTransCalcPDCInterestOutstanding.Sum(s => s.TransAmount) - custTransCalcPDCInterestOutstanding.Sum(s => s.SettleAmount)).Round();
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public decimal CalcWithdrawalNetPaidByWithdrawalTable(Guid WithdrawalTableGUID)
		{
			try
			{
				IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);

				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(WithdrawalTableGUID);

				decimal sumSettleServiceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID).Sum(s => s.SettleAmount);

				decimal sumSettleInvoiceSettlementDetail = invoiceSettlementDetailService.GetByReferenceAndSuspenseInvoiceType(RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID, SuspenseInvoiceType.None).Sum(s => s.SettleAmount);

				decimal sumSuspenseSettlementDetail = invoiceSettlementDetailService.GetByReferenceAndSuspenseInvoiceType(RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID, SuspenseInvoiceType.SuspenseAccount).Sum(s => s.SettleAmount);

				return withdrawalTable.WithdrawalAmount - (withdrawalTable.RetentionAmount + sumSettleServiceFeeTrans + sumSettleInvoiceSettlementDetail + sumSuspenseSettlementDetail);
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public decimal CalcTermExtensionFeeAmount(Guid companyGUID, Guid creditTermGUID, Guid? termExtensionInvoiceRevenueTypeGUID = null)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(companyGUID);

				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				CreditTerm creditTerm = creditTermRepo.GetCreditTermByIdNoTracking(creditTermGUID);

				if (companyParameter.PFTermExtensionDaysPerFeeAmount == 0 || termExtensionInvoiceRevenueTypeGUID == null)
                {
					return 0;
                }
				else
                {
					IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
					InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByIdNoTracking(termExtensionInvoiceRevenueTypeGUID.Value);
					return ((creditTerm.NumberOfDays / companyParameter.PFTermExtensionDaysPerFeeAmount).Round(0) * invoiceRevenueType.FeeAmount).Round();
                }
 			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region Inquiry withdrawal line outstanding
		public InquiryWithdrawalLineOutstandItemView GetInquiryWithdrawalLineOutstandById(string id)
		{
			try
			{
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				return withdrawalLineRepo.GetInquiryWithdrawalLineOutstandByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CollectionFollowUpItemView GetCollectionFollowUpInitialData(string refGUID)
        {
			ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db);
			return collectionFollowUpService.GetCollectionFollowUpInitialDataByInquiryWithdrawalLineOutstand(refGUID.StringToGuid());
		}
		#endregion

		#region Payment detail
		public PaymentDetailItemView GetPaymentDetailInitialData(string refGUID)
		{
			try
			{
				IPaymentDetailService paymentDetailService = new PaymentDetailService(db);
				WithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				string refId = withdrawalTableRepo.GetWithdrawalTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).WithdrawalId;
				return paymentDetailService.GetPaymentDetailInitialData(refId, refGUID, Models.Enum.RefType.WithdrawalTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Verification trans
		public VerificationTransItemView GetVerificationTransInitialData(string refGUID)
		{
			try
			{
				IVerificationTransService verificationTransService = new VerificationTransService(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
				string refId = withdrawalTable.WithdrawalId;
				string documentId = withdrawalTable.WithdrawalId;
				return verificationTransService.GetVerificationTransInitialData(refId, refGUID, Models.Enum.RefType.WithdrawalTable, documentId);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region function
		#region extendTerm
		public ExtendTermParamView GetExtendTermById(string withdrawalTableGUID)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalExtendTerm(withdrawalTableGUID.StringToGuid());
				if (withdrawalTable == null)
				{
					withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalTableGUID.StringToGuid());
				}
				WithdrawalTableItemView withdrawalTableItemView = withdrawalTableRepo.GetWithdrawalItemViewExtendTerm(withdrawalTable.WithdrawalTableGUID);
				IEnumerable<WithdrawalLineItemViewMap> withdrawalLine = withdrawalLineRepo.GetWithdrawalLineItemViewMapByWithdrawalTableGuid(withdrawalTable.WithdrawalTableGUID)
					.Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Principal);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(withdrawalTable.CompanyGUID);
				ExtendTermParamView extendTermView = new ExtendTermParamView()
				{
					WithdrawalTableGUID = withdrawalTableItemView.WithdrawalTableGUID,
					WithdrawalId = withdrawalTableItemView.WithdrawalId,
					DocumentStatus = withdrawalTableItemView.DocumentStatus_StatusId,
					CustomerId = withdrawalTableItemView.CustomerTable_CustomerId,
					BuyerId = withdrawalTableItemView.BuyerTable_BuyerId,
					WithdrawalDate = withdrawalTableItemView.WithdrawalDate,
					CreditTermId = withdrawalTableItemView.CreditTerm_CreditTermId,
					TermExtension = withdrawalTableItemView.TermExtension,
					NumberOfExtension = withdrawalTableItemView.NumberOfExtension,
					TotalInterestPct = withdrawalTableItemView.TotalInterestPct,
					WithdrawalAmount = withdrawalTableItemView.WithdrawalAmount,
					Outstanding = withdrawalLine.Sum(s => s.CustTrans_TransAmount - s.CustTrans_SettleAmount),
					ExtendCreditTermGUID = companyParameter.PFExtendCreditTermGUID.ToString(),
					DocumentStatusGUID = withdrawalTableItemView.DocumentStatusGUID,
					DueDate = withdrawalTableItemView.DueDate,
					CompanyGUID = withdrawalTableItemView.CompanyGUID
				};
				return extendTermView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExtendTermResultView ExtendTerm(ExtendTermParamView view)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
				IVerificationTransService verificationTransService = new VerificationTransService(db);
				IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);

				ExtendTermResultView result = new ExtendTermResultView();
				NotificationResponse success = new NotificationResponse();
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalExtendTerm(view.WithdrawalTableGUID.StringToGuid());
				if (withdrawalTable == null)
				{
					withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(view.WithdrawalTableGUID.StringToGuid());
				}
				List<VerificationTrans> verificationTransesResult = new List<VerificationTrans>();
				List<BuyerAgreementTrans> buyerAgreementTransResult = new List<BuyerAgreementTrans>();

				WithdrawalTable withdrawalTableResult = CreateWithdrawalExtendTerm(withdrawalTable, view);
				WithdrawalLine withdrawalLinePrinciple = CreateWithdrawLinePrincipalExtendTerm(withdrawalTableResult);
				WithdrawalLine withdrawalLineInterest = CreateWithdrawLineInterestExtendTerm(withdrawalTableResult, withdrawalLinePrinciple);
				verificationTransesResult.AddRange(verificationTransService.CopyVerificationTransactions(withdrawalTable.WithdrawalTableGUID.ToString(), RefType.WithdrawalTable, withdrawalTableResult.WithdrawalTableGUID.ToString(), RefType.WithdrawalTable, withdrawalTableResult.WithdrawalId));
				buyerAgreementTransResult.AddRange(buyerAgreementTransService.CopyBuyerAgreementTrans(withdrawalTable.WithdrawalTableGUID, withdrawalTableResult.WithdrawalTableGUID, (int)RefType.WithdrawalTable, (int)RefType.WithdrawalTable));

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					this.BulkInsert(new List<WithdrawalTable>() { withdrawalTableResult });
					this.BulkInsert(new List<WithdrawalLine>() { withdrawalLinePrinciple });
					this.BulkInsert(new List<WithdrawalLine>() { withdrawalLineInterest });
					this.BulkInsert(verificationTransesResult.ToList(), true);
					this.BulkInsert(buyerAgreementTransResult.ToList(), true);
					UnitOfWork.Commit(transaction);
				}
				success.AddData("SUCCESS.90011", new string[] { "LABEL.WITHDRAWAL" });
				result.Notification = success;
				return result;

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetExtendTermValidation(ExtendTermParamView view)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(view.WithdrawalTableGUID.StringToGuid());
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(view.DocumentStatusGUID.StringToGuid());
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(withdrawalTable.CreditAppTableGUID);
				List<WithdrawalTable> extendWithdrawalTables = withdrawalTableRepo.GetWithdrawalTableByExtendWithdrawalTableGUID(view.WithdrawalTableGUID.StringToGuid());
				DocumentStatus draftStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)WithdrawalStatus.Draft).ToString());

				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(view.CompanyGUID.StringToGuid(), ReferenceId.Withdrawal);
				NumberSeqTable numberSeqTable = numberSeqTableRepo.GetByNumberSeqTableGUID(numberSeqParameter.NumberSeqTableGUID.HasValue ? numberSeqParameter.NumberSeqTableGUID.Value : (Guid?)null);

				if (documentStatus.StatusId != ((int)WithdrawalStatus.Posted).ToString())
				{
					ex.AddData("ERROR.90013", new string[] { "LABEL.WITHDRAWAL" });
				}
				if (creditAppTable.InactiveDate != null && view.ExtendWithdrawalDate.StringToDate() >= creditAppTable.InactiveDate)
				{
					ex.AddData("ERROR.90108", new string[] { "LABEL.EXTEND_WITHDRAWAL_DATE", "LABEL.INACTIVE_DATE", "LABEL.CREDIT_APP_TABLE" });
				}
				if (creditAppTable.InactiveDate == null && view.ExtendWithdrawalDate.StringToDate() >= creditAppTable.ExpiryDate)
				{
					ex.AddData("ERROR.90108", new string[] { "LABEL.EXTEND_WITHDRAWAL_DATE", "LABEL.EXPIRY_DATE", "LABEL.CREDIT_APP_TABLE" });
				}
				if (extendWithdrawalTables.Exists(x => x.DocumentStatusGUID == draftStatus.DocumentStatusGUID))
				{
					ex.AddData("ERROR.90039");
				}
				if (numberSequenceService.IsManualByReferenceId(view.CompanyGUID.StringToGuid(), ReferenceId.Withdrawal))
				{
					ex.AddData("ERROR.00576", new string[] { numberSeqTable.NumberSeqCode });
				}
				if (view.ExtendWithdrawalDate.StringToDate() < withdrawalTable.DueDate)
				{
					ex.AddData("ERROR.DATE_CANNOT_LESS_THAN", new string[] { view.ExtendWithdrawalDate, withdrawalTable.DueDate.DateToString() });
				}
				if (view.Outstanding == 0)
				{
					ex.AddData("ERROR.90069");
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private WithdrawalTable CreateWithdrawalExtendTerm(WithdrawalTable withdrawalTableParam, ExtendTermParamView view)
		{
			try
			{
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);

				NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.Withdrawal);
				DocumentStatus draftStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)WithdrawalStatus.Draft).ToString());
				DocumentStatus postedtStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)WithdrawalStatus.Posted).ToString());
				CreditTerm creditTerm = creditTermRepo.GetCreditTermByIdNoTracking(view.ExtendCreditTermGUID.StringToGuid());
				WithdrawalTable oriWithdrawalTable = withdrawalTableRepo.GetWithdrawalTableByOriWithdrawalTableGUID(withdrawalTableParam.OriginalWithdrawalTableGUID ?? withdrawalTableParam.WithdrawalTableGUID)
					.Where(w => w.DocumentStatusGUID == postedtStatus.DocumentStatusGUID).OrderByDescending(o => o.NumberOfExtension).FirstOrDefault();
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(withdrawalTableParam.CompanyGUID);
				decimal termExtensionFeeAmount = CalcTermExtensionFeeAmount(withdrawalTableParam.CompanyGUID, view.ExtendCreditTermGUID.StringToGuid(), companyParameter.PFTermExtensionInvRevenueTypeGUID);
				WithdrawalLine withdrawalLines = withdrawalLineRepo.GetWithdrawalLineByWithdrawalTableGuid(withdrawalTableParam.WithdrawalTableGUID)
					.Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Principal).FirstOrDefault();
				WithdrawalTable withdrawalTable = new WithdrawalTable()
				{
					WithdrawalTableGUID = Guid.NewGuid(),
					WithdrawalId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault()),
					Description = view.Description,
					ProductType = withdrawalTableParam.ProductType,
					CreditAppTableGUID = withdrawalTableParam.CreditAppTableGUID,
					CreditAppLineGUID = withdrawalTableParam.CreditAppLineGUID,
					DocumentStatusGUID = draftStatus.DocumentStatusGUID,
					CustomerTableGUID = withdrawalTableParam.CustomerTableGUID,
					BuyerTableGUID = withdrawalTableParam.BuyerTableGUID,
					BuyerAgreementTableGUID = withdrawalTableParam.BuyerAgreementTableGUID,
					Remark = withdrawalTableParam.Remark,
					OperReportSignatureGUID = withdrawalTableParam.OperReportSignatureGUID,
					WithdrawalDate = view.ExtendWithdrawalDate.StringToDate(),
					CreditTermGUID = view.ExtendCreditTermGUID.StringToGuid(),
					DueDate = view.ExtendWithdrawalDate.StringToDate().AddDays(creditTerm.NumberOfDays),
					TotalInterestPct = withdrawalTableParam.TotalInterestPct,
					WithdrawalAmount = view.Outstanding,
					InterestCutDay = 0,
					MethodOfPaymentGUID = withdrawalTableParam.MethodOfPaymentGUID,
					// NetPaid = 0,
					RetentionCalculateBase = withdrawalTableParam.RetentionCalculateBase,
					RetentionPct = withdrawalTableParam.RetentionPct,
					RetentionAmount = 0,
					AssignmentAgreementTableGUID = withdrawalTableParam.AssignmentAgreementTableGUID,
					AssignmentAmount = withdrawalTableParam.AssignmentAmount,
					TermExtension = true,
					ExtendWithdrawalTableGUID = withdrawalTableParam.WithdrawalTableGUID,
					OriginalWithdrawalTableGUID = withdrawalTableParam.OriginalWithdrawalTableGUID ?? withdrawalTableParam.WithdrawalTableGUID,
					NumberOfExtension = oriWithdrawalTable == null ? 1 : oriWithdrawalTable.NumberOfExtension + 1,
					TermExtensionInvoiceRevenueTypeGUID = companyParameter.PFTermExtensionInvRevenueTypeGUID,
					TermExtensionFeeAmount = termExtensionFeeAmount,
					SettleTermExtensionFeeAmount = termExtensionFeeAmount,
					OperationMarkComment = false,
					OperationComment = string.Empty,
					CreditMarkComment = false,
					CreditComment = string.Empty,
					ReceiptTempTableGUID = null,
					CompanyGUID = withdrawalTableParam.CompanyGUID
				};
				if (withdrawalLines != null)
				{
					withdrawalTable.ExtendInterestDate = withdrawalLines.InterestDate;
				}
				return withdrawalTable;
			}
			catch (Exception e)
			{

				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private WithdrawalLine CreateWithdrawLinePrincipalExtendTerm(WithdrawalTable withdrawalTable)
		{
			try
			{
				IInterestService interestService = new InterestService(db);
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);

				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(withdrawalTable.CreditAppTableGUID);
				DateTime interestDate = interestService.FindActualInterestDate(withdrawalTable.DueDate, creditAppTable.ProductSubTypeGUID);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(withdrawalTable.CompanyGUID);
				WithdrawalLine withdrawalLine = new WithdrawalLine()
				{
					WithdrawalLineGUID = Guid.NewGuid(),
					WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
					LineNum = 1,
					WithdrawalLineType = (int)WithdrawalLineType.Principal,
					StartDate = withdrawalTable.ExtendInterestDate.GetValueOrDefault(),
					DueDate = withdrawalTable.DueDate,
					InterestDate = interestDate,
					InterestDay = (int)(interestDate - withdrawalTable.ExtendInterestDate.GetValueOrDefault()).TotalDays,
					InterestAmount = companyParameter.DaysPerYear == 0 ? 0 :
					(((withdrawalTable.WithdrawalAmount * (withdrawalTable.TotalInterestPct / 100)) / companyParameter.DaysPerYear) * (int)(interestDate - withdrawalTable.ExtendInterestDate.GetValueOrDefault()).TotalDays).Round(),
					WithdrawalLineInvoiceTableGUID = null,
					ClosedForTermExtension = false,
					CustomerPDCTableGUID = null,
					BuyerPDCTableGUID = null,
					BillingDate = null,
					CollectionDate = withdrawalTable.DueDate,
					CompanyGUID = withdrawalTable.CompanyGUID
				};
				return withdrawalLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private WithdrawalLine CreateWithdrawLineInterestExtendTerm(WithdrawalTable withdrawalTable, WithdrawalLine principalLine)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(withdrawalTable.CompanyGUID);
				WithdrawalLine withdrawalLine = new WithdrawalLine()
				{
					WithdrawalLineGUID = Guid.NewGuid(),
					WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID,
					LineNum = 2,
					WithdrawalLineType = (int)WithdrawalLineType.Interest,
					StartDate = withdrawalTable.ExtendInterestDate.GetValueOrDefault(),
					DueDate = withdrawalTable.ExtendInterestDate.GetValueOrDefault(),
					InterestDate = principalLine.InterestDate,
					InterestDay = (int)(principalLine.InterestDate - withdrawalTable.ExtendInterestDate.GetValueOrDefault()).TotalDays,
					InterestAmount = companyParameter.DaysPerYear == 0 ? 0 :
					(((withdrawalTable.WithdrawalAmount * (withdrawalTable.TotalInterestPct / 100)) / companyParameter.DaysPerYear) * (int)(principalLine.InterestDate - withdrawalTable.ExtendInterestDate.GetValueOrDefault()).TotalDays).Round(),
					WithdrawalLineInvoiceTableGUID = null,
					ClosedForTermExtension = false,
					CustomerPDCTableGUID = null,
					BuyerPDCTableGUID = null,
					BillingDate = null,
					CollectionDate = withdrawalTable.ExtendInterestDate.GetValueOrDefault(),
					CompanyGUID = withdrawalTable.CompanyGUID
				};
				return withdrawalLine;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region post withdrawal
		public PostWithdrawalView GetPostWithdrawalById(string withdrawalTableGUID)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTableItemView withdrawalTableItemView = withdrawalTableRepo.GetWithdrawalItemViewPostWithdrawal(withdrawalTableGUID.StringToGuid());
				PostWithdrawalView postWithdrawalView = new PostWithdrawalView
				{
					WithdrawalTableGUID = withdrawalTableItemView.WithdrawalTableGUID,
					WithdrawalTable_Values = withdrawalTableItemView.WithdrawalId,
					DocumentStatus_Values = withdrawalTableItemView.DocumentStatus_Values,
					CustomerTable_Values = withdrawalTableItemView.CustomerTable_Values,
					BuyerTable_Values = withdrawalTableItemView.BuyerTable_Values,
					WithdrawalDate = withdrawalTableItemView.WithdrawalDate,
					CreditTerm_Values = withdrawalTableItemView.CreditTerm_CreditTermId,
					DueDate = withdrawalTableItemView.DueDate,
					TermExtension = withdrawalTableItemView.TermExtension,
					NumberOfExtension = withdrawalTableItemView.NumberOfExtension,
					TotalInterestPct = withdrawalTableItemView.TotalInterestPct,
					WithdrawalAmount = withdrawalTableItemView.WithdrawalAmount
				};
				return postWithdrawalView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostWithdrawalResultView PostWithdrawal(PostWithdrawalView view)
		{
			try
			{
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				PostWithdrawalResultView result = new PostWithdrawalResultView();
				NotificationResponse success = new NotificationResponse();
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(view.WithdrawalTableGUID.StringToGuid());

				PostWithdrawalViewMap postWithdrawal = InitPostWithdrawal(withdrawalTable);

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					#region Create ReceiptTempTable
					// 4.CreateReceiptTemp
					// 18.Update ReceiptTempTable
					this.BulkInsert(postWithdrawal.ReceiptTempTable.FirstToList(), false);
					#endregion Create ReceiptTempTable

					#region Update WithdrawalTable
					// 11.Update WithdrawalTable
					this.BulkUpdate(postWithdrawal.WithdrawalTable.FirstToList());
					#endregion Update WithdrawalTable

					#region Insert ReceiptTempPaymentDetail
					// 16.Create ReceiptTempPaymentDetail (MethodOfPayment = CompanyParameter.SuspenseMethodOfPaymentGUID)
					// 17.Create ReceiptTempPaymentDetail (MethodOfPayment = CompanyParameter.SettlementMethodOfPaymentGUID)
					this.BulkInsert(postWithdrawal.ReceiptTempPaymDetail);
					#endregion Insert ReceiptTempPaymentDetail

					#region Insert data from post invoice
					// PostInvoice from process 1, 2, 3, 7, 8, 13
					this.BulkInsert(postWithdrawal.InvoiceTable, false);
					this.BulkInsert(postWithdrawal.InvoiceLine, false);
					this.BulkInsert(postWithdrawal.CustTrans, false);
					this.BulkInsert(postWithdrawal.RetentionTrans, false);
					#endregion Insert data from post invoice

					#region Insert IntercompanyInvoiceTable
					// 3.Create Invoice from ServiceFeeTrans
					this.BulkInsert(postWithdrawal.IntercompanyInvoiceTable, false);
					#endregion Insert IntercompanyInvoiceTable

					#region Insert InvoiceSettlementDetail
					// 5.Create InvoiceSettlementDetail from ServiceFeeTrans
					// 6.Create InvoiceSettlementDetail from InvoiceSettlementDetail (SuspenseInvoiceType.None)
					// 14.Create InvoiceSettlementDetail from TermExtensionFeeAmount
					// 15.Create InvoiceSettlementDetail from  WithdrawalLine = Pricipal (ExtendWithdrawalTableGUID)
					// 16.Create ReceiptTempPaymentDetail (MethodOfPayment = CompanyParameter.SuspenseMethodOfPaymentGUID)
					this.BulkInsert(postWithdrawal.InvoiceSettlementDetail);
					#endregion Insert InvoiceSettlementDetail

					#region Insert/Update data from post settlement
					// 19.PostSettlement
					if (ConditionService.IsNotZero(postWithdrawal.ReceiptTable.Count()))
					{
						this.BulkInsert(postWithdrawal.ReceiptTable, false);
					}
					if (ConditionService.IsNotZero(postWithdrawal.ReceiptLine.Count()))
					{
						this.BulkInsert(postWithdrawal.ReceiptLine, false);
					}
					if (ConditionService.IsNotZero(postWithdrawal.PaymentHistory.Count()))
					{
						this.BulkInsert(postWithdrawal.PaymentHistory, false);
					}
					if (ConditionService.IsNotZero(postWithdrawal.CustTrans_Update.Count()))
					{
						this.BulkUpdate(postWithdrawal.CustTrans_Update);
					}
					#endregion Insert/Update data from post settlement

					#region Insert data from post invoice and post settlement
					// PostInvoice from process 1, 2, 3, 7, 8, 13
					// 19.PostSettlement
					this.BulkInsert(postWithdrawal.CreditAppTrans, false);
					this.BulkInsert(postWithdrawal.TaxInvoiceTable, false);
					this.BulkInsert(postWithdrawal.TaxInvoiceLine, false);
                    #endregion Insert data from post invoice and post settlement

                    #region Insert ProcessTrans
                    // 9.Create VendorPaymentTrans from PaymentDetail
                    // PostInvoice from process 1, 2, 3, 7, 8, 13
                    // 19.PostSettlement
                    this.BulkInsert(postWithdrawal.ProcessTrans);
					#endregion Insert ProcessTrans

					#region Insert VendorPaymentTrans
					// 9.Create VendorPaymentTrans from PaymentDetail
					this.BulkInsert(postWithdrawal.VendorPaymentTrans);
					#endregion Insert VendorPaymentTrans

					#region Insert InterestRealizedTrans
					// 10.Create InterestRealizedTrans from WithdrawalLine = Principal
					this.BulkInsert(postWithdrawal.InterestRealizedTrans);
					#endregion Insert InterestRealizedTrans

					#region Update WithdrawalLine
					// 1.Create Invoice from WithdrawalLine = Pricipal
					// 2.Create Invoice from WithdrawalLine = Interest
					// 12.Update WithdrawalLine.ClosedForTermExtention = True
					this.BulkUpdate(postWithdrawal.WithdrawalLine);
					#endregion Update WithdrawalLine
					UnitOfWork.Commit(transaction);
                }

				success.AddData("SUCCESS.90004", new string[] { "LABEL.WITHDRAWAL", SmartAppUtil.GetDropDownLabel(withdrawalTable.WithdrawalId, withdrawalTable.Description) });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostWithdrawalViewMap InitPostWithdrawal(PostWithdrawalView view)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(view.WithdrawalTableGUID.StringToGuid());
				return InitPostWithdrawal(withdrawalTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PostWithdrawalViewMap InitPostWithdrawal(WithdrawalTable withdrawalTable)
		{
			try
			{
				IInvoiceService invoiceService = new InvoiceService(db);
				ICustTransService custTransService = new CustTransService(db);
				ICreditAppTransService creditAppTransService = new CreditAppTransService(db);
				ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
				IProcessTransService processTransService = new ProcessTransService(db);
				IRetentionTransService retentionTransService = new RetentionTransService(db);
				IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				IExchangeRateService exchangeRateService = new ExchangeRateService(db);
				IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
				IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
				ICustTransRepo custTransRepo = new CustTransRepo(db);
				List<InvoiceTable> createInvoiceTables = new List<InvoiceTable>();
				List<InvoiceLine> createInvoiceLines = new List<InvoiceLine>();
				List<TaxInvoiceLine> createTaxInvoiceLines = new List<TaxInvoiceLine>();
				List<CreditAppTrans> createCreditAppTranses = new List<CreditAppTrans>();
				List<CustTrans> createCustTranses = new List<CustTrans>();
				List<ProcessTrans> createProcessTranses = new List<ProcessTrans>();
				List<RetentionTrans> createRetentionTranses = new List<RetentionTrans>();
				List<TaxInvoiceTable> createTaxInvoiceTables = new List<TaxInvoiceTable>();
				List<VendorPaymentTrans> createVendorPaymentTrans = new List<VendorPaymentTrans>();
				List<ReceiptTable> createReceiptTables = new List<ReceiptTable>();
				List<ReceiptLine> createReceiptLines = new List<ReceiptLine>();
				List<PaymentHistory> createPaymentHistorys = new List<PaymentHistory>();
				List<IntercompanyInvoiceTable> createIntercompanyInvoiceTable = new List<IntercompanyInvoiceTable>();
				List<CustTrans> updateCustTranses = new List<CustTrans>();
				List<InvoiceTable> invoiceTableWithForPostInvoice = new List<InvoiceTable>();
				List<InvoiceLine> invoiceLineWithForPostInvoice = new List<InvoiceLine>();
				PostWithdrawalResultView result = new PostWithdrawalResultView();
				NotificationResponse success = new NotificationResponse();
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(withdrawalTable.CompanyGUID);
				IEnumerable<WithdrawalLine> withdrawalLines = withdrawalLineRepo.GetWithdrawalLineByWithdrawalTableNoTracking(withdrawalTable.WithdrawalTableGUID);
				IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID);
				IEnumerable<InvoiceSettlementDetail> invoiceSettlementDetails = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByReferanceNoTracking(withdrawalTable.WithdrawalTableGUID, (int)RefType.WithdrawalTable);
				IEnumerable<PaymentDetail> paymentDetails = paymentDetailRepo.GetPaymentDetailByReferanceNoTracking(withdrawalTable.WithdrawalTableGUID, (int)RefType.WithdrawalTable);

				#region Validate
				GetPostWithdrawalValidation(withdrawalTable);
				#endregion Validate

				#region 1.Create Invoice from WithdrawalLine = Pricipal
				List<WithdrawalLine> updateWithdrawalLines = new List<WithdrawalLine>();
				List<WithdrawalLine> withdrawalLineTypePrincipal = withdrawalLines.Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Principal).ToList();
				bool isWithdrawalLineTypePrincipal = ConditionService.IsNotZero(withdrawalLineTypePrincipal.Count());
				if (isWithdrawalLineTypePrincipal)
				{
					withdrawalLineTypePrincipal.ForEach(withdrawalLine =>
					{
						// 1.1
						Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView()
						{
							RefType = RefType.WithdrawalLine,
							RefGUID = withdrawalLine.WithdrawalLineGUID,
							IssuedDate = withdrawalTable.WithdrawalDate,
							DueDate = withdrawalTable.DueDate,
							CustomerTableGUID = withdrawalTable.CustomerTableGUID,
							ProductType = (ProductType)withdrawalTable.ProductType,
							InvoiceTypeGUID = companyParameter.PFInvTypeGUID.Value,
							AutoGenInvoiceRevenueType = true,
							InvoiceAmount = withdrawalTable.WithdrawalAmount,
							IncludeTax = true,
							CompanyGUID = withdrawalTable.CompanyGUID,
							InvoiceRevenueTypeGUID = null,
							DocumentId = withdrawalTable.WithdrawalId,
							CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
							BuyerTableGUID = withdrawalTable.BuyerTableGUID,
							Dimension1GUID = withdrawalLine.Dimension1GUID,
							Dimension2GUID = withdrawalLine.Dimension2GUID,
							Dimension3GUID = withdrawalLine.Dimension3GUID,
							Dimension4GUID = withdrawalLine.Dimension4GUID,
							Dimension5GUID = withdrawalLine.Dimension5GUID,
							MethodOfPaymentGUID = withdrawalTable.MethodOfPaymentGUID,
							BuyerInvoiceTableGUID = null,
							BuyerAgreementTableGUID = withdrawalTable.BuyerAgreementTableGUID
						};
						GenInvoiceResultView genInvoiceResultView = invoiceService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
						// 1.2
						invoiceTableWithForPostInvoice.Add(genInvoiceResultView.InvoiceTable);
						invoiceLineWithForPostInvoice.Add(genInvoiceResultView.InvoiceLine);
						// 1.3
						withdrawalLine.WithdrawalLineInvoiceTableGUID = genInvoiceResultView.InvoiceTable.InvoiceTableGUID;
						withdrawalLine.ModifiedBy = db.GetUserName();
						withdrawalLine.ModifiedDateTime = DateTime.Now;
					});
					updateWithdrawalLines.AddRange(withdrawalLineTypePrincipal);
				}
				#endregion 1.Create Invoice from WithdrawalLine = Pricipal

				#region 2.Create Invoice from WithdrawalLine = Interest
				List<WithdrawalLine> withdrawalLineTypeInterest = withdrawalLines.Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Interest).OrderBy(o => o.LineNum).ToList();
				bool isWithdrawalLineTypeInterest = ConditionService.IsNotZero(withdrawalLineTypeInterest.Count());
				if (isWithdrawalLineTypeInterest)
				{
					withdrawalLineTypeInterest.ForEach(withdrawalLine =>
					{
						// 2.1
						Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView()
						{
							RefType = RefType.WithdrawalLine,
							RefGUID = withdrawalLine.WithdrawalLineGUID,
							IssuedDate = withdrawalTable.WithdrawalDate,
							DueDate = withdrawalLine.DueDate,
							CustomerTableGUID = withdrawalTable.CustomerTableGUID,
							ProductType = (ProductType)withdrawalTable.ProductType,
							InvoiceTypeGUID = companyParameter.PFUnearnedInterestInvTypeGUID.Value,
							AutoGenInvoiceRevenueType = true,
							InvoiceAmount = withdrawalLine.InterestAmount,
							IncludeTax = true,
							CompanyGUID = withdrawalTable.CompanyGUID,
							InvoiceRevenueTypeGUID = null,
							DocumentId = withdrawalTable.WithdrawalId,
							CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
							BuyerTableGUID = withdrawalTable.BuyerTableGUID,
							Dimension1GUID = withdrawalLine.Dimension1GUID,
							Dimension2GUID = withdrawalLine.Dimension2GUID,
							Dimension3GUID = withdrawalLine.Dimension3GUID,
							Dimension4GUID = withdrawalLine.Dimension4GUID,
							Dimension5GUID = withdrawalLine.Dimension5GUID,
							MethodOfPaymentGUID = withdrawalTable.MethodOfPaymentGUID,
							BuyerInvoiceTableGUID = null,
							BuyerAgreementTableGUID = withdrawalTable.BuyerAgreementTableGUID
						};
						GenInvoiceResultView genInvoiceResultView = invoiceService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
						// 2.2
						invoiceTableWithForPostInvoice.Add(genInvoiceResultView.InvoiceTable);
						invoiceLineWithForPostInvoice.Add(genInvoiceResultView.InvoiceLine);
						// 2.3
						withdrawalLine.WithdrawalLineInvoiceTableGUID = genInvoiceResultView.InvoiceTable.InvoiceTableGUID;
						withdrawalLine.ModifiedBy = db.GetUserName();
						withdrawalLine.ModifiedDateTime = DateTime.Now;
					});
					updateWithdrawalLines.AddRange(withdrawalLineTypeInterest);
				}
				#endregion 2.Create Invoice from WithdrawalLine = Interest

				#region 3.Create Invoice from ServiceFeeTrans
				List<InvoiceTable> genInvoiceTableFromServiceFeeTransResultViews = new List<InvoiceTable>();
				bool isServiceFeeTrans = ConditionService.IsNotZero(serviceFeeTrans.Count());
				if (isServiceFeeTrans)
				{
					// 3.1
					GenInvoiceFromServiceFeeTransParamView genInvoiceFromServiceFeeTransView = new GenInvoiceFromServiceFeeTransParamView()
					{
						ServiceFeeTrans = serviceFeeTrans.OrderBy(o => o.Ordering).ToList(),
						CustomerTableGUID = withdrawalTable.CustomerTableGUID,
						IssuedDate = withdrawalTable.WithdrawalDate,
						DocumentId = withdrawalTable.WithdrawalId,
						ProductType = (ProductType)withdrawalTable.ProductType,
						CreditAppTableGUID = withdrawalTable.CreditAppTableGUID
					};
					GenInvoiceFromServiceFeeTransResultView genInvoiceFromServiceFeeTransResultView = serviceFeeTransService.GenInvoiceFromServiceFeeTrans(genInvoiceFromServiceFeeTransView);
					genInvoiceTableFromServiceFeeTransResultViews.AddRange(genInvoiceFromServiceFeeTransResultView.InvoiceTable);
					createIntercompanyInvoiceTable.AddRange(genInvoiceFromServiceFeeTransResultView.IntercompanyInvoiceTable);
					//3.2
					invoiceTableWithForPostInvoice.AddRange(genInvoiceFromServiceFeeTransResultView.InvoiceTable);
					invoiceLineWithForPostInvoice.AddRange(genInvoiceFromServiceFeeTransResultView.InvoiceLine);
				}
				#endregion 3.Create Invoice from ServiceFeeTrans

				#region 4.CreateReceiptTemp
				ReceiptTempTable createReceiptTempTable = new ReceiptTempTable();
				List<ServiceFeeTrans> serviceFeeTransSettleAmountGreaterThanZero = serviceFeeTrans.Where(w => w.SettleAmount > 0).ToList();
				bool isServiceFeeTransSettleAmountGreaterThanZero = ConditionService.IsNotZero(serviceFeeTransSettleAmountGreaterThanZero.Count());
				bool isInvoiceSettlementDetail = ConditionService.IsNotZero(invoiceSettlementDetails.Count());
				if (withdrawalTable.TermExtension || (isServiceFeeTransSettleAmountGreaterThanZero || isInvoiceSettlementDetail))
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.ReceiptTemp);
					DocumentStatus temporaryReceiptStatusPosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)TemporaryReceiptStatus.Posted).ToString());
					CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(withdrawalTable.CustomerTableGUID);
					createReceiptTempTable = new ReceiptTempTable()
					{
						ReceiptTempId = numberSequenceService.GetNumber(withdrawalTable.CompanyGUID,null , numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault()),
						ReceiptDate = withdrawalTable.WithdrawalDate,
						TransDate = withdrawalTable.WithdrawalDate,
						ProductType = withdrawalTable.ProductType,
						ReceivedFrom = (int)ReceivedFrom.Customer,
						CustomerTableGUID = withdrawalTable.CustomerTableGUID,
						BuyerTableGUID = withdrawalTable.BuyerTableGUID,
						BuyerReceiptTableGUID = null,
						DocumentStatusGUID = temporaryReceiptStatusPosted.DocumentStatusGUID,
						DocumentReasonGUID = null,
						Remark = string.Empty,
						ReceiptAmount = 0,
						SettleFeeAmount = 0,
						SettleAmount = 0,
						CurrencyGUID = customerTable.CurrencyGUID,
						ExchangeRate = exchangeRateService.GetExchangeRate(customerTable.CurrencyGUID, withdrawalTable.WithdrawalDate),
						SuspenseInvoiceTypeGUID = null,
						SuspenseAmount = 0,
						CreditAppTableGUID = null,
						ReceiptTempRefType = (int)ReceiptTempRefType.Product,
						RefReceiptTempGUID = null,
						MainReceiptTempGUID = null,
						Dimension1GUID = withdrawalTable.Dimension1GUID,
						Dimension2GUID = withdrawalTable.Dimension2GUID,
						Dimension3GUID = withdrawalTable.Dimension3GUID,
						Dimension4GUID = withdrawalTable.Dimension4GUID,
						Dimension5GUID = withdrawalTable.Dimension5GUID,
						ReceiptTempTableGUID = Guid.NewGuid(),
						CompanyGUID = withdrawalTable.CompanyGUID
					};
				}
				else {
					createReceiptTempTable = null;
				}
				#endregion 4.CreateReceiptTemp

				#region 5.Create InvoiceSettlementDetail from ServiceFeeTrans
				List<InvoiceSettlementDetail> createInvoiceSettlementDetails = new List<InvoiceSettlementDetail>();
				List<InvoiceSettlementDetail> invoiceSettlementDetailsFromServiceFeeTrans = new List<InvoiceSettlementDetail>();
				if (isServiceFeeTransSettleAmountGreaterThanZero)
				{
					invoiceSettlementDetailsFromServiceFeeTrans = invoiceSettlementDetailService.GenInvoiceSettlementDetailFromServiceFeeTrans(serviceFeeTransSettleAmountGreaterThanZero,
																																			   genInvoiceTableFromServiceFeeTransResultViews,
																																			   withdrawalTable.WithdrawalId,
																																			   RefType.ReceiptTemp,
																																			   createReceiptTempTable.ReceiptTempTableGUID);
                    createInvoiceSettlementDetails.AddRange(invoiceSettlementDetailsFromServiceFeeTrans);

                }
				#endregion 5.Create InvoiceSettlementDetail from ServiceFeeTrans

				#region 6.Create InvoiceSettlementDetail from InvoiceSettlementDetail (SuspenseInvoiceType.None)
				List<InvoiceSettlementDetail> invoiceSettlementDetailsBySuspenseInvoiceTypeNone = invoiceSettlementDetails.Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None).ToList();
				List<InvoiceSettlementDetail> invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone = new List<InvoiceSettlementDetail>();
				bool isInvoiceSettlementDetailsBySuspenseInvoiceTypeNone = ConditionService.IsNotZero(invoiceSettlementDetailsBySuspenseInvoiceTypeNone.Count());
				if (isInvoiceSettlementDetailsBySuspenseInvoiceTypeNone)
				{
					invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone = invoiceSettlementDetailService.GenInvoiceSettlementDetailFromInvoiceSettlementDetail(invoiceSettlementDetailsBySuspenseInvoiceTypeNone, RefType.ReceiptTemp, createReceiptTempTable.ReceiptTempTableGUID);
                    createInvoiceSettlementDetails.AddRange(invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone);
                }
				#endregion 6.Create InvoiceSettlementDetail from InvoiceSettlementDetail (SuspenseInvoiceType.None)

				#region 7.Create invoice from RetentionAmount 
				if (withdrawalTable.RetentionAmount != 0)
				{
					BuyerAgreementTable buyerAgreementTable = buyerAgreementTableRepo.GetBuyerAgreementTableByIdNoTracking(withdrawalTable.BuyerAgreementTableGUID.HasValue ? withdrawalTable.BuyerAgreementTableGUID.Value : Guid.Empty);
					// 7.1
					Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView()
					{
						RefType = RefType.WithdrawalTable,
						RefGUID = withdrawalTable.WithdrawalTableGUID,
						IssuedDate = withdrawalTable.WithdrawalDate,
						DueDate = withdrawalTable.WithdrawalDate,
						CustomerTableGUID = withdrawalTable.CustomerTableGUID,
						ProductType = (ProductType)withdrawalTable.ProductType,
						InvoiceTypeGUID = companyParameter.PFRetentionInvTypeGUID.Value,
						AutoGenInvoiceRevenueType = true,
						InvoiceAmount = withdrawalTable.RetentionAmount * (-1),
						IncludeTax = true,
						CompanyGUID = withdrawalTable.CompanyGUID,
						InvoiceRevenueTypeGUID = null,
						DocumentId = withdrawalTable.WithdrawalId,
						CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
						BuyerTableGUID = buyerAgreementTable != null ? (Guid?)buyerAgreementTable.BuyerTableGUID : null,
						Dimension1GUID = withdrawalTable.Dimension1GUID,
						Dimension2GUID = withdrawalTable.Dimension2GUID,
						Dimension3GUID = withdrawalTable.Dimension3GUID,
						Dimension4GUID = withdrawalTable.Dimension4GUID,
						Dimension5GUID = withdrawalTable.Dimension5GUID,
						MethodOfPaymentGUID = null,
						BuyerInvoiceTableGUID = null,
						BuyerAgreementTableGUID = withdrawalTable.BuyerAgreementTableGUID
					};
					GenInvoiceResultView genInvoiceResultView = invoiceService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
					// 7.2
					invoiceTableWithForPostInvoice.Add(genInvoiceResultView.InvoiceTable);
					invoiceLineWithForPostInvoice.Add(genInvoiceResultView.InvoiceLine);
				}
				#endregion 7.Create invoice from RetentionAmount 

				#region 8.Create Invoice from PaymentDetail - SuspenseAccount
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(withdrawalTable.CreditAppTableGUID);
				List<PaymentDetail> paymentDetailByPaidToTypeSuspense = paymentDetails.Where(w => w.PaidToType == (int)PaidToType.Suspense && w.PaymentAmount != 0).ToList();
				bool isPaymentDetailByPaidToTypeSuspense = ConditionService.IsNotZero(paymentDetailByPaidToTypeSuspense.Count());
				if (isPaymentDetailByPaidToTypeSuspense)
				{
					paymentDetailByPaidToTypeSuspense.ForEach(paymentDetail =>
					{
						// 8.1
						InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(paymentDetail.InvoiceTypeGUID.Value);
						Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView()
						{
							RefType = RefType.PaymentDetail,
							RefGUID = paymentDetail.PaymentDetailGUID,
							IssuedDate = withdrawalTable.WithdrawalDate,
							DueDate = withdrawalTable.WithdrawalDate,
							CustomerTableGUID = paymentDetail.CustomerTableGUID.Value,
							ProductType = (ProductType)invoiceType.ProductType,
							InvoiceTypeGUID = paymentDetail.InvoiceTypeGUID.Value,
							AutoGenInvoiceRevenueType = true,
							InvoiceAmount = paymentDetail.PaymentAmount * (-1),
							IncludeTax = true,
							CompanyGUID = withdrawalTable.CompanyGUID,
							InvoiceRevenueTypeGUID = null,
							DocumentId = withdrawalTable.WithdrawalId,
							CreditAppTableGUID = (creditAppTable != null && paymentDetail.SuspenseTransfer == false) ? (Guid?)withdrawalTable.CreditAppTableGUID : null,
							BuyerTableGUID = null,
							Dimension1GUID = withdrawalTable.Dimension1GUID,
							Dimension2GUID = withdrawalTable.Dimension2GUID,
							Dimension3GUID = withdrawalTable.Dimension3GUID,
							Dimension4GUID = withdrawalTable.Dimension4GUID,
							Dimension5GUID = withdrawalTable.Dimension5GUID,
							MethodOfPaymentGUID = null,
							BuyerInvoiceTableGUID = null,
							BuyerAgreementTableGUID = null
						};
						GenInvoiceResultView genInvoiceResultView = invoiceService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
						// 8.2
						invoiceTableWithForPostInvoice.Add(genInvoiceResultView.InvoiceTable);
						invoiceLineWithForPostInvoice.Add(genInvoiceResultView.InvoiceLine);
					});
					// 8.3
					List<ProcessTrans> processTransFromSuspensePaymentDetail = processTransService.GenProcessTransFromSuspensePaymentDetail(paymentDetailByPaidToTypeSuspense, (ProductType)withdrawalTable.ProductType, withdrawalTable.WithdrawalId, withdrawalTable.WithdrawalDate);
					createProcessTranses.AddRange(processTransFromSuspensePaymentDetail);
				}
				#endregion 8.Create Invoice from PaymentDetail - SuspenseAccount

				#region 9.Create VendorPaymentTrans from PaymentDetail
				MethodOfPayment methodOfPayment = methodOfPaymentRepo.GetMethodOfPaymentByIdNoTracking(companyParameter.SettlementMethodOfPaymentGUID.Value);
				List<PaymentDetail> paymentDetailByPaidToTypeCustomerOrVendor = paymentDetails.Where(w => w.VendorTableGUID.HasValue && (w.PaidToType == (int)PaidToType.Customer || w.PaidToType == (int)PaidToType.Vendor)).OrderBy(o => o.PaymentDetailGUID).ToList();
				bool isPaymentDetailByPaidToTypeCustomerOrVendor = ConditionService.IsNotZero(paymentDetailByPaidToTypeCustomerOrVendor.Count());
				if (isPaymentDetailByPaidToTypeCustomerOrVendor)
				{
					DocumentStatus vendorPaymentStatusConfirmed = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)VendorPaymentStatus.Confirmed).ToString());
					paymentDetailByPaidToTypeCustomerOrVendor.ForEach(paymentDetail =>
					{
						// 9.1
						VendorPaymentTrans vendorPaymentTrans = new VendorPaymentTrans()
						{
							CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
							VendorTableGUID = paymentDetail.VendorTableGUID,
							PaymentDate = withdrawalTable.WithdrawalDate,
							OffsetAccount = methodOfPayment.AccountNum.ToString(),
							TaxTableGUID = null,
							AmountBeforeTax = paymentDetail.PaymentAmount,
							TaxAmount = 0,
							TotalAmount = paymentDetail.PaymentAmount,
							DocumentStatusGUID = vendorPaymentStatusConfirmed.DocumentStatusGUID,
							Dimension1 = withdrawalTable.Dimension1GUID,
							Dimension2 = withdrawalTable.Dimension2GUID,
							Dimension3 = withdrawalTable.Dimension3GUID,
							Dimension4 = withdrawalTable.Dimension4GUID,
							Dimension5 = withdrawalTable.Dimension5GUID,
							RefType = (int)RefType.WithdrawalTable,
							RefGUID = withdrawalTable.WithdrawalTableGUID,
							VendorPaymentTransGUID = Guid.NewGuid(),
							CompanyGUID = paymentDetail.CompanyGUID
						};
						createVendorPaymentTrans.Add(vendorPaymentTrans);
						// 9.2
						ProcessTrans processTrans = processTransService.GenProcessTransFromVendorPaymentTrans(vendorPaymentTrans, withdrawalTable.CustomerTableGUID, (ProductType)withdrawalTable.ProductType, withdrawalTable.WithdrawalId);
						createProcessTranses.Add(processTrans);
					});
				}
				#endregion 9.Create VendorPaymentTrans from PaymentDetail

				#region 10.Create InterestRealizedTrans from WithdrawalLine = Principal
				List<InterestRealizedTrans> createInterestRealizedTranses = new List<InterestRealizedTrans>();
				if (isWithdrawalLineTypePrincipal)
				{
					DateTime startDate = new DateTime();
					DateTime endDate = new DateTime();
					int lineNum = 1;
					WithdrawalLine withdrawalLine = withdrawalLineTypePrincipal.FirstOrDefault();

					if (withdrawalTable.TermExtension == true)
					{
						startDate = withdrawalTable.ExtendInterestDate.Value;
						lineNum = 1;
					}
					else
					{
						startDate = withdrawalTable.WithdrawalDate;
						lineNum = 1;
					}

					do
					{
						DateTime lastDayOfMonthByStartDate = new DateTime(startDate.Year, startDate.Month, DateTime.DaysInMonth(startDate.Year, startDate.Month));

						if (lastDayOfMonthByStartDate < withdrawalLine.InterestDate)
						{
							endDate = lastDayOfMonthByStartDate;
						}
						else
						{
							endDate = withdrawalLine.InterestDate.AddDays(-1);
						}

						int interestDay = (endDate - startDate).Days + 1;
						decimal interestPerDay = (withdrawalLine.InterestAmount / withdrawalLine.InterestDay).Round();
						decimal accInterestAmount = 0;

						if (endDate == withdrawalLine.InterestDate.AddDays(-1))
						{
							accInterestAmount = withdrawalLine.InterestAmount - createInterestRealizedTranses.Where(w => w.StartDate < startDate).Sum(s => s.AccInterestAmount);
						}
						else
						{
							accInterestAmount = (interestDay * interestPerDay).Round();
						}

						InterestRealizedTrans interestRealizedTrans = new InterestRealizedTrans()
						{
							DocumentId = withdrawalTable.WithdrawalId,
							ProductType = withdrawalTable.ProductType,
							LineNum = lineNum,
							StartDate = startDate,
							EndDate = endDate,
							AccountingDate = lastDayOfMonthByStartDate,
							InterestDay = interestDay,
							InterestPerDay = interestPerDay,
							AccInterestAmount = accInterestAmount,
							Accrued = false,
							RefType = (int)RefType.WithdrawalLine,
							RefGUID = withdrawalLine.WithdrawalLineGUID,
							InterestRealizedTransGUID = Guid.NewGuid(),
							CompanyGUID = withdrawalTable.CompanyGUID,
							Dimension1GUID = withdrawalLine.Dimension1GUID,
							Dimension2GUID = withdrawalLine.Dimension2GUID,
							Dimension3GUID = withdrawalLine.Dimension3GUID,
							Dimension4GUID = withdrawalLine.Dimension4GUID,
							Dimension5GUID = withdrawalLine.Dimension5GUID
						};
						createInterestRealizedTranses.Add(interestRealizedTrans);

						if (endDate != withdrawalLine.InterestDate.AddDays(-1))
						{
							startDate = endDate.AddDays(1);
							lineNum = lineNum + 1;
						}
					} while (endDate != withdrawalLine.InterestDate.AddDays(-1));
				}
				#endregion 10.Create InterestRealizedTrans from WithdrawalLine = Principal

				#region 11.Update WithdrawalTable
				DocumentStatus withdrawalStatusPosted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)WithdrawalStatus.Posted).ToString());
				withdrawalTable.DocumentStatusGUID = withdrawalStatusPosted.DocumentStatusGUID;
				if (createReceiptTempTable != null)
				{
					withdrawalTable.ReceiptTempTableGUID = createReceiptTempTable.ReceiptTempTableGUID;
				}
				withdrawalTable.ModifiedBy = db.GetUserName();
				withdrawalTable.ModifiedDateTime = DateTime.Now;
				#endregion

				// 13.Create Invoice from TermExtensionFeeAmount
				GenInvoiceResultView genInvoiceResultViewByTermExtensionFeeAmount = new GenInvoiceResultView();
				// 14.Create InvoiceSettlementDetail from TermExtensionFeeAmount
				InvoiceSettlementDetail invoiceSettlementDetailByTermExtensionFeeAmount = new InvoiceSettlementDetail();
				// 15.Create InvoiceSettlementDetail from  WithdrawalLine = Pricipal (ExtendWithdrawalTableGUID)
				List<InvoiceSettlementDetail> invoiceSettlementDetailsByWithdrawalLineTypePrincipal = new List<InvoiceSettlementDetail>();
				if (withdrawalTable.TermExtension == true)
				{
					#region 12.Update WithdrawalLine.ClosedForTermExtention = True
					List<WithdrawalLine> extendWithdrawalLineTypePrincipal = withdrawalLineRepo.GetWithdrawalLineByWithdrawalTableNoTracking(withdrawalTable.ExtendWithdrawalTableGUID.Value).Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Principal).ToList(); ;
					bool isExtendWithdrawalLineTypePrincipal = ConditionService.IsNotZero(extendWithdrawalLineTypePrincipal.Count());
					if (isExtendWithdrawalLineTypePrincipal)
					{
						extendWithdrawalLineTypePrincipal.Select(s =>
						{
							s.ClosedForTermExtension = true;
							s.ModifiedBy = db.GetUserName();
							s.ModifiedDateTime = DateTime.Now;
							return s;
						}).ToList();
                        updateWithdrawalLines.AddRange(extendWithdrawalLineTypePrincipal);
                    }
					#endregion 12.Update WithdrawalLine.ClosedForTermExtention = True

					#region 13.Create Invoice from TermExtensionFeeAmount
					if (withdrawalTable.TermExtensionFeeAmount != 0)
					{
						// 13.1
						Invoice1LineSpecificParamView invoice1LineSpecificParamView = new Invoice1LineSpecificParamView()
						{
							RefType = RefType.WithdrawalTable,
							RefGUID = withdrawalTable.WithdrawalTableGUID,
							IssuedDate = withdrawalTable.WithdrawalDate,
							DueDate = withdrawalTable.WithdrawalDate,
							CustomerTableGUID = withdrawalTable.CustomerTableGUID,
							ProductType = (ProductType)withdrawalTable.ProductType,
							InvoiceTypeGUID = companyParameter.ServiceFeeInvTypeGUID.Value,
							AutoGenInvoiceRevenueType = false,
							InvoiceAmount = withdrawalTable.TermExtensionFeeAmount,
							IncludeTax = true,
							CompanyGUID = withdrawalTable.CompanyGUID,
							InvoiceRevenueTypeGUID = withdrawalTable.TermExtensionInvoiceRevenueTypeGUID,
							DocumentId = withdrawalTable.WithdrawalId,
							CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
							BuyerTableGUID = null,
							Dimension1GUID = withdrawalTable.Dimension1GUID,
							Dimension2GUID = withdrawalTable.Dimension2GUID,
							Dimension3GUID = withdrawalTable.Dimension3GUID,
							Dimension4GUID = withdrawalTable.Dimension4GUID,
							Dimension5GUID = withdrawalTable.Dimension5GUID,
							MethodOfPaymentGUID = null,
							BuyerInvoiceTableGUID = null,
							BuyerAgreementTableGUID = null,

						};
						genInvoiceResultViewByTermExtensionFeeAmount = invoiceService.GenInvoice1LineSpecific(invoice1LineSpecificParamView);
						// 13.2
						invoiceTableWithForPostInvoice.Add(genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable);
						invoiceLineWithForPostInvoice.Add(genInvoiceResultViewByTermExtensionFeeAmount.InvoiceLine);
					}
					#endregion 13.Create Invoice from TermExtensionFeeAmount

					#region 14.Create InvoiceSettlementDetail from TermExtensionFeeAmount
					if (withdrawalTable.SettleTermExtensionFeeAmount > 0)
					{
						invoiceSettlementDetailByTermExtensionFeeAmount = new InvoiceSettlementDetail()
						{
							ProductType = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.ProductType,
							DocumentId = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.DocumentId,
							InvoiceTableGUID = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.InvoiceTableGUID,
							InvoiceDueDate = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.DueDate,
							InvoiceTypeGUID = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.InvoiceTypeGUID,
							SuspenseInvoiceType = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.SuspenseInvoiceType,
							InvoiceAmount = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.InvoiceAmount,
							BalanceAmount = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.InvoiceAmount,
							SettleAmount = withdrawalTable.SettleTermExtensionFeeAmount,
							WHTAmount = 0,
							SettleInvoiceAmount = withdrawalTable.SettleTermExtensionFeeAmount,
							SettleTaxAmount = invoiceSettlementDetailService.CalcSettleTaxAmount(genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.InvoiceAmount,
																								 genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.TaxAmount,
																								 withdrawalTable.SettleTermExtensionFeeAmount,
																								 genInvoiceResultViewByTermExtensionFeeAmount.InvoiceLine.TaxTableGUID),
							WHTSlipReceivedByBuyer = false,
							WHTSlipReceivedByCustomer = false,
							BuyerAgreementTableGUID = null,
							AssignmentAgreementTableGUID = null,
							SettleAssignmentAmount = 0,
							PurchaseAmount = 0,
							PurchaseAmountBalance = 0,
							SettlePurchaseAmount = 0,
							SettleReserveAmount = 0,
							InterestCalculationMethod = (int)InterestCalculationMethod.Minimum,
							RetentionAmountAccum = 0,
							RefType = (int)RefType.ReceiptTemp,
							RefGUID = createReceiptTempTable.ReceiptTempTableGUID,
							RefReceiptTempPaymDetailGUID = null,
							InvoiceSettlementDetailGUID = Guid.NewGuid(),
							CompanyGUID = withdrawalTable.CompanyGUID
						};
                        createInvoiceSettlementDetails.Add(invoiceSettlementDetailByTermExtensionFeeAmount);
                    }
                    #endregion 14.Create InvoiceSettlementDetail from TermExtensionFeeAmount

                    #region 15.Create InvoiceSettlementDetail from  WithdrawalLine = Pricipal (ExtendWithdrawalTableGUID)
                    if (isExtendWithdrawalLineTypePrincipal)
					{
						InvoiceTable invoiceTableByWithdrawalLineTypePrincipal = invoiceTableRepo.GetInvoiceTableByIdNoTracking(extendWithdrawalLineTypePrincipal.FirstOrDefault().WithdrawalLineInvoiceTableGUID.Value);
						invoiceSettlementDetailsByWithdrawalLineTypePrincipal = invoiceSettlementDetailService.GenInvoiceSettlementDetailFromInvoice(invoiceTableByWithdrawalLineTypePrincipal.FirstToList(), RefType.ReceiptTemp, createReceiptTempTable.ReceiptTempTableGUID);
						createInvoiceSettlementDetails.AddRange(invoiceSettlementDetailsByWithdrawalLineTypePrincipal);
					}
					#endregion 15.Create InvoiceSettlementDetail from  WithdrawalLine = Pricipal (ExtendWithdrawalTableGUID)
				}
				else 
				{
					invoiceSettlementDetailByTermExtensionFeeAmount = null;
				}

				#region 16.Create ReceiptTempPaymentDetail (MethodOfPayment = CompanyParameter.SuspenseMethodOfPaymentGUID)
				List<ReceiptTempPaymDetail> createReceiptTempPaymDetails = new List<ReceiptTempPaymDetail>();
				ReceiptTempPaymDetail receiptTempPaymDetailBySuspenseMethodOfPayment = new ReceiptTempPaymDetail();
				List<InvoiceSettlementDetail> invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeSuspenseAccount = new List<InvoiceSettlementDetail>();
				List<InvoiceSettlementDetail> invoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount = invoiceSettlementDetails.Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount).ToList();
				bool isInvoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount = ConditionService.IsNotZero(invoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount.Count());
				if (isInvoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount)
				{
					// 16.1
					receiptTempPaymDetailBySuspenseMethodOfPayment = new ReceiptTempPaymDetail()
					{
						ReceiptTempTableGUID = createReceiptTempTable.ReceiptTempTableGUID,
						MethodOfPaymentGUID = companyParameter.SuspenseMethodOfPaymentGUID.HasValue ? companyParameter.SuspenseMethodOfPaymentGUID.Value : Guid.Empty,
						ReceiptAmount = invoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount.Sum(s => s.SettleAmount) * (-1),
						ChequeTableGUID = null,
						ChequeBankGroupGUID = null,
						ChequeBranch = null,
						TransferReference = string.Empty,
						ReceiptTempPaymDetailGUID = Guid.NewGuid(),
						CompanyGUID = withdrawalTable.CompanyGUID
					};
					createReceiptTempPaymDetails.Add(receiptTempPaymDetailBySuspenseMethodOfPayment);

					//16.2
					invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeSuspenseAccount =
						invoiceSettlementDetailService.GenInvoiceSettlementDetailFromInvoiceSettlementDetail(invoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount,
																											 RefType.ReceiptTemp,
																											 createReceiptTempTable.ReceiptTempTableGUID,
																											 receiptTempPaymDetailBySuspenseMethodOfPayment.ReceiptTempPaymDetailGUID);
					createInvoiceSettlementDetails.AddRange(invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeSuspenseAccount);
				}
				#endregion 16.Create ReceiptTempPaymentDetail (MethodOfPayment = CompanyParameter.SuspenseMethodOfPaymentGUID)

				#region 17.Create ReceiptTempPaymentDetail (MethodOfPayment = CompanyParameter.SettlementMethodOfPaymentGUID)
				ReceiptTempPaymDetail receiptTempPaymDetailBySettlementMethodOfPayment = new ReceiptTempPaymDetail();
				bool isInvoiceSettlementDetailsFromServiceFeeTransBySettleAmountGreaterThanZero = ConditionService.IsNotZero(invoiceSettlementDetailsFromServiceFeeTrans.Where(w => w.SettleAmount > 0).Count());
				bool isInvoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone = ConditionService.IsNotZero(invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone.Count());
				bool isInvoiceSettlementDetailsByWithdrawalLineTypePrincipal = ConditionService.IsNotZero(invoiceSettlementDetailsByWithdrawalLineTypePrincipal.Count());
				if (isInvoiceSettlementDetailsFromServiceFeeTransBySettleAmountGreaterThanZero
					|| isInvoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone
					|| invoiceSettlementDetailByTermExtensionFeeAmount != null
					|| isInvoiceSettlementDetailsByWithdrawalLineTypePrincipal)
				{
					List<InvoiceSettlementDetail> invoiceSettlementDetailsUseInProcess = new List<InvoiceSettlementDetail>();
					invoiceSettlementDetailsUseInProcess.AddRange(invoiceSettlementDetailsFromServiceFeeTrans); // Sub-Process No.5
					invoiceSettlementDetailsUseInProcess.AddRange(invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone); // Sub-Process No.6
					if (invoiceSettlementDetailByTermExtensionFeeAmount != null)
					{
						invoiceSettlementDetailsUseInProcess.Add(invoiceSettlementDetailByTermExtensionFeeAmount); // Sub-Process No.14
					}
					invoiceSettlementDetailsUseInProcess.AddRange(invoiceSettlementDetailsByWithdrawalLineTypePrincipal); // Sub-Process No.15
					invoiceSettlementDetailsUseInProcess.AddRange(invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeSuspenseAccount); // Sub-Process No.16

					receiptTempPaymDetailBySettlementMethodOfPayment = new ReceiptTempPaymDetail()
					{
						ReceiptTempTableGUID = createReceiptTempTable.ReceiptTempTableGUID,
						MethodOfPaymentGUID = companyParameter.SettlementMethodOfPaymentGUID.HasValue ? companyParameter.SettlementMethodOfPaymentGUID.Value : Guid.Empty,
						ReceiptAmount = invoiceSettlementDetailsUseInProcess.Where(w => w.RefType == (int)RefType.ReceiptTemp && w.RefGUID == createReceiptTempTable.ReceiptTempTableGUID).Sum(s => s.SettleAmount),
						ChequeTableGUID = null,
						ChequeBankGroupGUID = null,
						ChequeBranch = null,
						TransferReference = string.Empty,
						ReceiptTempPaymDetailGUID = Guid.NewGuid(),
						CompanyGUID = withdrawalTable.CompanyGUID
					};
					createReceiptTempPaymDetails.Add(receiptTempPaymDetailBySettlementMethodOfPayment);
				}
				#endregion 17.Create ReceiptTempPaymentDetail (MethodOfPayment = CompanyParameter.SettlementMethodOfPaymentGUID)

				#region Post invoice
				PostInvoiceResultView postInvoiceResultView = invoiceService.PostInvoice(invoiceTableWithForPostInvoice, invoiceLineWithForPostInvoice, RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID);
				#endregion Post invoice

				ViewPostSettlementResult viewPostSettlementResult = new ViewPostSettlementResult();
				List<CustTrans> updateCustTrans = new List<CustTrans>();
				if (createReceiptTempTable != null)
				{
					#region 18.Update ReceiptTempTable
					List<InvoiceSettlementDetail> invoiceSettlementDetailsUseInProcess = new List<InvoiceSettlementDetail>();
					invoiceSettlementDetailsUseInProcess.AddRange(invoiceSettlementDetailsFromServiceFeeTrans); // Sub-Process No.5
					invoiceSettlementDetailsUseInProcess.AddRange(invoiceSettlementDetailsFromInvoiceSettlementDetailBySuspenseInvoiceTypeNone); // Sub-Process No.6
					if (invoiceSettlementDetailByTermExtensionFeeAmount != null)
					{
						invoiceSettlementDetailsUseInProcess.Add(invoiceSettlementDetailByTermExtensionFeeAmount); // Sub-Process No.14
					}
					invoiceSettlementDetailsUseInProcess.AddRange(invoiceSettlementDetailsByWithdrawalLineTypePrincipal); // Sub-Process No.15

					createReceiptTempTable.ReceiptAmount = receiptTempPaymDetailBySuspenseMethodOfPayment.ReceiptAmount + receiptTempPaymDetailBySettlementMethodOfPayment.ReceiptAmount;
					createReceiptTempTable.SettleAmount = invoiceSettlementDetailsUseInProcess.Where(w => w.RefType == (int)RefType.ReceiptTemp && w.RefGUID == createReceiptTempTable.ReceiptTempTableGUID && w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None).Sum(s => s.SettleAmount);
					#endregion 18.Update ReceiptTempTable

					#region 19.PostSettlement
					// case 1 get data post invoice from 3.2
					List<InvoiceTable> invoiceTablesCase1 = postInvoiceResultView.InvoiceTables.Where(w => genInvoiceTableFromServiceFeeTransResultViews.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)).ToList();
					List<InvoiceLine> invoiceLinesCase1 = postInvoiceResultView.InvoiceLines.Where(w => invoiceTablesCase1.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)).ToList();
					List<CustTrans> custTransCase1 = postInvoiceResultView.CustTranses.Where(w => invoiceTablesCase1.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)).ToList();
					// case 2 get data post invoice from 13.2
					List<InvoiceTable> invoiceTablesCase2 = genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable != null ? postInvoiceResultView.InvoiceTables.Where(w => genInvoiceResultViewByTermExtensionFeeAmount.InvoiceTable.InvoiceTableGUID == w.InvoiceTableGUID).ToList() : new List<InvoiceTable>();
					List<InvoiceLine> invoiceLinesCase2 = postInvoiceResultView.InvoiceLines.Where(w => invoiceTablesCase2.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)).ToList();
					List<CustTrans> custTransCase2 = postInvoiceResultView.CustTranses.Where(w => invoiceTablesCase2.Select(s => s.InvoiceTableGUID).Contains(w.InvoiceTableGUID)).ToList();
					// case 3 get invoiceTableGUID from 15.1
					IEnumerable<Guid> invoiceTableGUIDs = invoiceSettlementDetailsByWithdrawalLineTypePrincipal.Where(w => w.InvoiceTableGUID != null).Select(s => s.InvoiceTableGUID.Value);
					// case 4 get invoiceTableGUID from invoiceSettlementDetail By WithdrawalTable
					invoiceTableGUIDs = invoiceTableGUIDs.Concat(invoiceSettlementDetails.Where(w => w.InvoiceTableGUID != null).Select(s => s.InvoiceTableGUID.Value));
					// find list from case 3 & 4
					List<InvoiceTable> invoiceTablesCase3And4 = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceTableGUIDs);
					List<InvoiceLine> invoiceLinesCase3And4 = invoiceLineRepo.GetInvoiceLinesByInvoiceTableGuid(invoiceTableGUIDs);
					List<CustTrans> custTransCase3And4 = custTransRepo.GetCustTransByInvoiceTableNoTracking(invoiceTableGUIDs.ToList()).ToList();

					ViewPostSettlementParameter viewPostSettlementParameter = new ViewPostSettlementParameter()
					{
						RefType = RefType.ReceiptTemp,
						InvoiceSettlementDetails = createInvoiceSettlementDetails,
						InvoiceTables = invoiceTablesCase1.Concat(invoiceTablesCase2).Concat(invoiceTablesCase3And4).ToList(),
						InvoiceLines = invoiceLinesCase1.Concat(invoiceLinesCase2).Concat(invoiceLinesCase3And4).ToList(),
						CustTrans = custTransCase1.Concat(custTransCase2).Concat(custTransCase3And4).ToList(),
						ReceiptTempTable = createReceiptTempTable,
						SourceRefType = RefType.WithdrawalTable,
						SourceRefId = withdrawalTable.WithdrawalId
					};
					viewPostSettlementResult = receiptTempTableService.PostSettlement(viewPostSettlementParameter);
					// CustTrans from PostInvoice - Update value
					postInvoiceResultView.CustTranses.Where(w => viewPostSettlementResult.UpdateCustTrans.Select(s => s.CustTransGUID).Contains(w.CustTransGUID)).Select(s =>
					{
						var custTransByPostSettlement = viewPostSettlementResult.UpdateCustTrans.Where(w => w.CustTransGUID == s.CustTransGUID);
						s = custTransByPostSettlement != null ? custTransByPostSettlement.FirstOrDefault() : s;
						return s;
					});
					// CustTrans from Database - Get record
					updateCustTrans = viewPostSettlementResult.UpdateCustTrans.Where(w => custTransCase3And4.Select(s => s.CustTransGUID).Contains(w.CustTransGUID)).ToList();
					#endregion 19.PostSettlement
				}
				else 
				{
					viewPostSettlementResult = null;
				}

				#region Setup data from post withdrawal
				#region Setup data from post invoice
				createInvoiceTables.AddRange(postInvoiceResultView.InvoiceTables);
				createInvoiceLines.AddRange(postInvoiceResultView.InvoiceLines);
				createTaxInvoiceLines.AddRange(postInvoiceResultView.TaxInvoiceLines);
				createCustTranses.AddRange(postInvoiceResultView.CustTranses);
				createCreditAppTranses.AddRange(postInvoiceResultView.CreditAppTranses);
				createTaxInvoiceTables.AddRange(postInvoiceResultView.TaxInvoiceTables);
				createProcessTranses.AddRange(postInvoiceResultView.ProcessTranses);
				createRetentionTranses.AddRange(postInvoiceResultView.RetentionTranses);
				#endregion Setup data from post invoice

				#region Setup data from post settlement
				if (viewPostSettlementResult != null)
				{
					if (viewPostSettlementResult.CreateCreditAppTrans != null)
					{
						createCreditAppTranses.AddRange(viewPostSettlementResult.CreateCreditAppTrans);
					}
					createReceiptTables.AddRange(viewPostSettlementResult.CreateReceiptTable);
					createReceiptLines.AddRange(viewPostSettlementResult.CreateReceiptLine);
					createTaxInvoiceTables.AddRange(viewPostSettlementResult.CreateTaxInvoiceTable);
					createTaxInvoiceLines.AddRange(viewPostSettlementResult.CreateTaxInvoiceLine);
					createPaymentHistorys.AddRange(viewPostSettlementResult.CreatePaymentHistory);
					createProcessTranses.AddRange(viewPostSettlementResult.CreateProcessTrans);
					updateCustTranses.AddRange(updateCustTrans);
				}
				#endregion Setup data from post settlement

				#region Setup data and ordering to PostWithdrawalViewMap
				PostWithdrawalViewMap postWithdrawal = new PostWithdrawalViewMap
				{
					WithdrawalTable = withdrawalTable,
					WithdrawalLine = updateWithdrawalLines.OrderBy(o => o.WithdrawalTableGUID).ThenBy(o => o.WithdrawalLineGUID).ToList(),
					ReceiptTempTable = createReceiptTempTable,
					InvoiceTable = createInvoiceTables.OrderBy(o => o.InvoiceId).ToList(),
					InvoiceLine = (from invoiceLine in createInvoiceLines
								   join invoiceTable in createInvoiceTables
								   on invoiceLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceLine
								   from invoiceTable in ljInvoiceTableInvoiceLine.DefaultIfEmpty()
								   select new
								   {
									   invoiceLine,
									   invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
								   }).OrderBy(o => o.invoiceId).Select(s => s.invoiceLine).ToList(),
					CustTrans = (from custTrans in createCustTranses
								 join invoiceTable in createInvoiceTables
								 on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableCustTrans
								 from invoiceTable in ljInvoiceTableCustTrans.DefaultIfEmpty()
								 select new
								 {
									 custTrans,
									 invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
								 }).OrderBy(o => o.invoiceId).Select(s => s.custTrans).ToList(),
					CustTrans_Update = updateCustTranses.OrderBy(o => o.InvoiceTableGUID).ToList(),
					CreditAppTrans = createCreditAppTranses,
					ProcessTrans = (from processTrans in createProcessTranses
									join invoiceTable in createInvoiceTables
									on new { processTrans.RefType, InvoiceTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.Invoice, invoiceTable.InvoiceTableGUID } into ljInvoiceTableProcessTrans
									from invoiceTable in ljInvoiceTableProcessTrans.DefaultIfEmpty()
									join vendorPaymentTrans in createVendorPaymentTrans
									on new { processTrans.RefType, VendorPaymentTransGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.VendorPaymentTrans, vendorPaymentTrans.VendorPaymentTransGUID } into ljVendorPaymentTransProcessTrans
									from vendorPaymentTrans in ljVendorPaymentTransProcessTrans.DefaultIfEmpty()
									join receiptTempTable in createReceiptTempTable.FirstToList()
									on new { processTrans.RefType, ReceiptTempTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.ReceiptTemp, receiptTempTable.ReceiptTempTableGUID } into ljReceiptTempTableProcessTrans
									from receiptTempTable in ljReceiptTempTableProcessTrans.DefaultIfEmpty()
									select new
									{
										processTrans,
										processTrans.RefType,
										RefId = invoiceTable != null ? invoiceTable.InvoiceId : (vendorPaymentTrans != null ? vendorPaymentTrans.VendorTableGUID.ToString() : (receiptTempTable != null ? receiptTempTable.ReceiptTempId : string.Empty))
									}).OrderBy(o => o.RefType).ThenBy(t => t.RefId).ThenBy(t => t.processTrans.CreditAppTableGUID).Select(s => s.processTrans).ToList(),
					RetentionTrans = createRetentionTranses,
					VendorPaymentTrans = createVendorPaymentTrans.OrderBy(o => o.VendorTableGUID).ToList(),
					InvoiceSettlementDetail = (from invoiceSettlementDetail in createInvoiceSettlementDetails
											   join invoiceTable in createInvoiceTables
											   on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
											   from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
											   select new
											   {
												   invoiceSettlementDetail,
												   invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : invoiceSettlementDetail.InvoiceTableGUID.ToString()
											   }).OrderBy(o => o.invoiceId).Select(s => s.invoiceSettlementDetail).ToList(),
					InterestRealizedTrans = createInterestRealizedTranses,
					ReceiptTempPaymDetail = createReceiptTempPaymDetails,
					ReceiptTable = createReceiptTables.OrderBy(o => o.ReceiptId).ToList(),
					ReceiptLine = (from receiptLine in createReceiptLines
								   join receiptTable in createReceiptTables
								   on receiptLine.ReceiptTableGUID equals receiptTable.ReceiptTableGUID into ljReceiptTableReceiptLine
								   from receiptTable in ljReceiptTableReceiptLine.DefaultIfEmpty()
								   select new
								   {
									   receiptLine,
									   receiptId = receiptTable != null ? receiptTable.ReceiptId : string.Empty
								   }).OrderBy(o => o.receiptId).Select(s => s.receiptLine).ToList(),
					TaxInvoiceTable = createTaxInvoiceTables.OrderBy(o => o.TaxInvoiceId).ToList(),
					TaxInvoiceLine = (from taxInvoiceLine in createTaxInvoiceLines
									  join taxInvoiceTable in createTaxInvoiceTables
									  on taxInvoiceLine.TaxInvoiceTableGUID equals taxInvoiceTable.TaxInvoiceTableGUID into ljTaxInvoiceTableTaxInvoiceLine
									  from taxInvoiceTable in ljTaxInvoiceTableTaxInvoiceLine.DefaultIfEmpty()
									  select new
									  {
										  taxInvoiceLine,
										  taxInvoiceId = taxInvoiceTable != null ? taxInvoiceTable.TaxInvoiceId : string.Empty
									  }).OrderBy(o => o.taxInvoiceId).Select(s => s.taxInvoiceLine).ToList(),
					PaymentHistory = (from paymentHistory in createPaymentHistorys
									  join invoiceTable in createInvoiceTables
									  on paymentHistory.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTablePaymentHistory
									  from invoiceTable in ljInvoiceTablePaymentHistory.DefaultIfEmpty()
									  select new
									  {
										  paymentHistory,
										  invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
									  }).OrderBy(o => o.invoiceId).ThenBy(t => t.paymentHistory.PaymentBaseAmount).Select(s => s.paymentHistory).ToList(),
					IntercompanyInvoiceTable = createIntercompanyInvoiceTable.OrderBy(o => o.IntercompanyInvoiceId).ToList()
				};
				#endregion SetupData and Ordering to PostWithdrawalViewMap
				#endregion Setup data from post withdrawal

				return postWithdrawal;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetPostWithdrawalValidation(WithdrawalTable withdrawalTable)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

                #region Validate
                // Validate No.1
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus withdrawalStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)WithdrawalStatus.Draft).ToString());

				// Validate No.2
				IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
				IEnumerable<PaymentDetail> paymentDetails = paymentDetailRepo.GetPaymentDetailByReferanceNoTracking(withdrawalTable.WithdrawalTableGUID, (int)RefType.WithdrawalTable);
				decimal sumPaymentAmountByPaymentDetail = paymentDetails.Sum(s => s.PaymentAmount);
				decimal netPaid = (withdrawalTable.TermExtension == false) ? CalcWithdrawalNetPaidByWithdrawalTable(withdrawalTable.WithdrawalTableGUID) : 0;

				// Validate No.3
				IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
				IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
				DocumentStatus verificationStatusApproved = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)VerificationDocumentStatus.Approved).ToString());
				IEnumerable<VerificationTrans> verifications = verificationTransRepo.GetVerificationTransByReferanceNoTracking((int)RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID);
				IEnumerable<VerificationTable> verificationTables = verificationTableRepo.GetVerificationTableByVerificationTableGUIDListNoTracking(verifications.Select(s => s.VerificationTableGUID).ToList());
				bool isNotVerificationTransStatusApproved = ConditionService.IsEqualZero(verificationTables.Where(w => w.DocumentStatusGUID == verificationStatusApproved.DocumentStatusGUID).Count());

				// Validate No.4
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				IEnumerable<WithdrawalLine> withdrawalLines = withdrawalLineRepo.GetWithdrawalLineByWithdrawalTableNoTracking(withdrawalTable.WithdrawalTableGUID);
				bool isWithdrawalLineFieldCustomerPDCTableNotValue = ConditionService.IsNotZero(withdrawalLines.Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Principal && w.CustomerPDCTableGUID == null).Count());

				// Validate No.5
				IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				IEnumerable<ChequeTable> chequeTables = chequeTableRepo.GetChequeTableByWithdrawalLineTypePrincipal(withdrawalTable.CompanyGUID, withdrawalTable.WithdrawalTableGUID);
				CompanyParameter companyParamete = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(withdrawalTable.CompanyGUID);
				decimal sumChequeTableAmount = chequeTables.Sum(s => s.Amount);

				// Validate No.6 & Validate No.7
				IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
				ICustTransRepo custTranseRepo = new CustTransRepo(db); 
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				IEnumerable<InvoiceSettlementDetail> invoiceSettlementDetails = invoiceSettlementDetailRepo.GetInvoiceSettlementDetailByReferanceNoTracking(withdrawalTable.WithdrawalTableGUID, (int)RefType.WithdrawalTable);
				IEnumerable<InvoiceSettlementDetail> invoiceSettlementDetailBySuspenseInvoiceTypeNoneOrSuspenseAccount = invoiceSettlementDetails.Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None
																																							  || w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount);
				List<String> invoiceIdByCalcBalanceAmountNotEqualOutstanding = new List<string>();
				invoiceSettlementDetailBySuspenseInvoiceTypeNoneOrSuspenseAccount.Where(w => w.InvoiceTableGUID.HasValue).ToList().ForEach(invoiceSettlementDetail =>
				{
					CustTrans custTrans = custTranseRepo.GetCustTransByInvoiceTableNoTracking(invoiceSettlementDetail.InvoiceTableGUID.Value);
					if (invoiceSettlementDetail.BalanceAmount != (custTrans.TransAmount - custTrans.SettleAmount))
					{
						invoiceIdByCalcBalanceAmountNotEqualOutstanding.Add(invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceSettlementDetail.InvoiceTableGUID.Value).InvoiceId);
					}
				});
				bool isBalanceAmountNotEqualOutstanding = ConditionService.IsNotZero(invoiceIdByCalcBalanceAmountNotEqualOutstanding.Count());

				// Validate No.8
				IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
				List<MainAgreementTable> signedMainAgm = mainAgreementTableRepo.GetMainAgreementTableByCreditAppAndMainAgmTableStatus(withdrawalTable.CreditAppTableGUID, MainAgreementStatus.Signed);
				bool isNotMainAgreementStatusSigned = ConditionService.IsEqualZero(signedMainAgm.Count());

				// Validate No.9
				IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
				decimal assignmentBalance = assignmentAgreementTableService.GetAssignmentBalance(withdrawalTable.AssignmentAgreementTableGUID.HasValue ? withdrawalTable.AssignmentAgreementTableGUID.Value : Guid.Empty);

				// Validate No.10
				ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
				decimal creditLimitBalanceByCreditConditionType = creditAppTableService.GetCreditLimitBalanceByCreditConditionType(withdrawalTable.CreditAppTableGUID.ToString(), withdrawalTable.WithdrawalDate.DateToString());

				// Validate No.11
				decimal creditLimitBalanceByCreditAppLine = withdrawalTable.CreditAppLineGUID.HasValue ? creditAppTableService.GetCreditLimitBalanceByCreditAppLine(withdrawalTable.CreditAppLineGUID.ToString()) : 0;

				// Validate No.12
				IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
				IEnumerable<BuyerCreditLimitByProduct> buyerCreditLimitByProducts = buyerCreditLimitByProductRepo.GetBuyerCreditLimitByProductByBuyer(withdrawalTable.BuyerTableGUID.HasValue ? withdrawalTable.BuyerTableGUID.Value : Guid.Empty)
																												 .Where(w => w.ProductType == withdrawalTable.ProductType);
				bool isBuyerCreditLimitByProducts = ConditionService.IsNotZero(buyerCreditLimitByProducts.Count());
				decimal creditLimitBalanceByProduct = creditAppTableService.GetCreditLimitBalanceByProduct(withdrawalTable.BuyerTableGUID.ToString(), (ProductType)withdrawalTable.ProductType);

				// Validate No.13 Condition 1
				IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
				IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID);
				decimal sumSettleAmountByServiceFeeTrans = serviceFeeTrans.Sum(s => s.SettleAmount);
				// Validate No.13 Condition 2
				IEnumerable<InvoiceSettlementDetail> invoiceSettlementDetailsBySuspenseInvoiceTypeNone = invoiceSettlementDetails.Where(w => (w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None));
				bool isNotInvoiceSettlementDetailsBySuspenseInvoiceTypeNone = ConditionService.IsEqualZero(invoiceSettlementDetailsBySuspenseInvoiceTypeNone.Count());
				// Validate No.13 Condition 4
				IEnumerable<InvoiceSettlementDetail> invoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount = invoiceSettlementDetails.Where(w => (w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount));
				bool isInvoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount = ConditionService.IsNotZero(invoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount.Count());

				// Validate No.14
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(withdrawalTable.CreditAppTableGUID);
				CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);
				IEnumerable<BuyerAgreementTrans> buyerAgreementTrans = buyerAgreementTransRepo.GetBuyerAgreementTransByReferanceNoTracking((int)RefType.WithdrawalTable, withdrawalTable.WithdrawalTableGUID);
				bool isNotBuyerAgreementTrans = ConditionService.IsEqualZero(buyerAgreementTrans.ToList().Count());

				if (withdrawalTable.DocumentStatusGUID != withdrawalStatusDraft.DocumentStatusGUID) // Validate No.1
				{
					ex.AddData("ERROR.90012", new string[] { "LABEL.WITHDRAWAL_TABLE" });
				}
				if (sumPaymentAmountByPaymentDetail != netPaid) // Validate No.2
				{
					ex.AddData("ERROR.90040", new string[] { "LABEL.NET_PAID" });
				}
				if (isNotVerificationTransStatusApproved && withdrawalTable.BuyerTableGUID != null) // Validate No.3
				{
					ex.AddData("ERROR.90041");
				}
				if (isWithdrawalLineFieldCustomerPDCTableNotValue) // Validate No.4
				{
					ex.AddData("ERROR.90042", new string[] { "LABEL.CUSTOMER_PDC_ID" });
				}
				if (Math.Abs(sumChequeTableAmount- withdrawalTable.WithdrawalAmount) > Math.Abs(companyParamete.ChequeToleranceAmount)) // Validate No.5
				{
					ex.AddData("ERROR.90043", new string[] { "LABEL.CUSTOMER_PDC_AMOUNT", "LABEL.WITHDRAWAL_AMOUNT", companyParamete.ChequeToleranceAmount.ToString() });
				}
				if (isBalanceAmountNotEqualOutstanding) // Validate No.6 & Validate No.7 
				{
					invoiceIdByCalcBalanceAmountNotEqualOutstanding.ForEach(invoiceId =>
					{
						ex.AddData("ERROR.90046", new string[] { "LABEL.INVOICE_SETTLEMENT_DETAIL", invoiceId, "LABEL.POST" });
					});
				}
				if (isNotMainAgreementStatusSigned) //Validate No.8
				{
					ex.AddData("ERROR.90074");
				}
				if (withdrawalTable.AssignmentAmount > assignmentBalance && withdrawalTable.AssignmentAgreementTableGUID != null) //Validate No.9
				{
					ex.AddData("ERROR.90071", new string[] { "LABEL.ASSIGNMENT_AMOUNT", "LABEL.ASSIGNMENT_AGREEMENT_OUTSTANDING", assignmentBalance.ToString() });
				}
				if (!withdrawalTable.TermExtension)
				{
					if (withdrawalTable.WithdrawalAmount > creditLimitBalanceByCreditConditionType) //Validate No.10
					{
						ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_LIMIT_CONDITION_TYPE", creditLimitBalanceByCreditConditionType.ToString() });
					}
					if (withdrawalTable.CreditAppLineGUID.HasValue)
					{
						if (withdrawalTable.WithdrawalAmount > creditLimitBalanceByCreditAppLine) //Validate No.11
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.CREDIT_APPLICATION_LINE", creditLimitBalanceByCreditAppLine.ToString() });
						}
						if (isBuyerCreditLimitByProducts && withdrawalTable.WithdrawalAmount > creditLimitBalanceByProduct) //Validate No.12
						{
							ex.AddData("ERROR.90107", new string[] { "LABEL.WITHDRAWAL_AMOUNT", "LABEL.BUYER_CREDIT_LIMIT_BY_PRODUCT", creditLimitBalanceByProduct.ToString() });
						}
					}
				}
				if (sumSettleAmountByServiceFeeTrans == 0 && isNotInvoiceSettlementDetailsBySuspenseInvoiceTypeNone && withdrawalTable.SettleTermExtensionFeeAmount == 0 && isInvoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount) //Validate No.13
				{
					ex.AddData("ERROR.90110");
				}
				if (creditLimitType.ValidateBuyerAgreement && isNotBuyerAgreementTrans) //Validate No.14
				{
					ex.AddData("ERROR.90146");
				}
				#endregion Validate
				#region Validate - Company parameter
				// Validate No.15
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
				InvoiceType pfInvoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParamete.PFInvTypeGUID.HasValue ? companyParamete.PFInvTypeGUID.Value : Guid.Empty);
				Guid? autoGenInvoiceRevenueTypeByPFInvoiceType = pfInvoiceType != null ? pfInvoiceType.AutoGenInvoiceRevenueTypeGUID : null;

				// Validate No.17
				InvoiceType pfUnearnedInterestInvoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParamete.PFUnearnedInterestInvTypeGUID.HasValue ? companyParamete.PFUnearnedInterestInvTypeGUID.Value : Guid.Empty);
				Guid? autoGenInvoiceRevenueTypeByPFUnearnedInterestInvoiceType = pfUnearnedInterestInvoiceType != null ? pfUnearnedInterestInvoiceType.AutoGenInvoiceRevenueTypeGUID : null;

				// Validate No.18
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isServiceFeeTransSettleAmountGreaterThanZero = ConditionService.IsNotZero(serviceFeeTrans.Where(w => w.SettleAmount > 0).Count());
				bool isInvoiceSettlementDetail = ConditionService.IsNotZero(invoiceSettlementDetails.Count());
				bool isNumberSeqManualByRefReceiptTemp = numberSequenceService.IsManualByReferenceId(withdrawalTable.CompanyGUID, "ReceiptTemp");
				string numberSeqCode = isNumberSeqManualByRefReceiptTemp ? numberSeqTableRepo.GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(withdrawalTable.CompanyGUID, "ReceiptTemp").NumberSeqCode : null;

				// Validate No.19 & Validate No.20
				bool isInvoiceSettlementDetailsBySuspenseInvoiceTypeNoneOrSuspenseAccount = ConditionService.IsNotZero(invoiceSettlementDetailBySuspenseInvoiceTypeNoneOrSuspenseAccount.Count());

				// Validate No.22
				InvoiceType pfRetentionInvoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParamete.PFRetentionInvTypeGUID.HasValue ? companyParamete.PFRetentionInvTypeGUID.Value : Guid.Empty);
				Guid? autoGenInvoiceRevenueTypeByPFRetentionInvoiceType = pfRetentionInvoiceType != null ? pfRetentionInvoiceType.AutoGenInvoiceRevenueTypeGUID : null;

				// Validate No.24
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				MethodOfPayment methodOfPayment = methodOfPaymentRepo.GetMethodOfPaymentByIdNoTracking(companyParamete.SettlementMethodOfPaymentGUID.HasValue ? companyParamete.SettlementMethodOfPaymentGUID.Value : Guid.Empty);
				IEnumerable<PaymentDetail> paymentDetailByVendorAndPaidToTypeCustomerOrVendor = paymentDetails.Where(w => w.VendorTableGUID.HasValue && (w.PaidToType == (int)PaidToType.Customer || w.PaidToType == (int)PaidToType.Vendor));
				bool isPaymentDetailByVendorAndPaidToTypeCustomerOrVendor = ConditionService.IsNotZero(paymentDetailByVendorAndPaidToTypeCustomerOrVendor.Count());

				// Validate No.25
				List<String> invoiceTypeIdByAutoGenInvoiceRevenueTypeNotValue = new List<string>();
				paymentDetailByVendorAndPaidToTypeCustomerOrVendor.Where(w => w.InvoiceTypeGUID.HasValue).ToList().ForEach(paymentDetail =>
				{
					InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(paymentDetail.InvoiceTypeGUID.Value);
					if (invoiceType.AutoGenInvoiceRevenueTypeGUID == null)
					{
						invoiceTypeIdByAutoGenInvoiceRevenueTypeNotValue.Add(invoiceType.InvoiceTypeId);
					}
				});
				bool isInvoiceTypeAutoGenInvoiceRevenueTypeNotValue = ConditionService.IsNotZero(invoiceTypeIdByAutoGenInvoiceRevenueTypeNotValue.Count());

				if (companyParamete.PFInvTypeGUID == null) // Validate No.14
				{
					ex.AddData("ERROR.90047", new string[] { "LABEL.PF_WITHDRAWAL_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER" });
				}
				if (autoGenInvoiceRevenueTypeByPFInvoiceType == null) // Validate No.15
				{
					ex.AddData("ERROR.90047", new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.PF_WITHDRAWAL_INVOICE_TYPE_ID" });
				}
				if (companyParamete.PFUnearnedInterestInvTypeGUID == null) // Validate No.16
				{
					ex.AddData("ERROR.90047", new string[] { "LABEL.PF_UNEARNED_INTEREST_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER" });
				}
				if (autoGenInvoiceRevenueTypeByPFUnearnedInterestInvoiceType == null) // Validate No.17
				{
					ex.AddData("ERROR.90047", new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.PF_UNEARNED_INTEREST_INVOICE_TYPE_ID" });
				}
				if ((isServiceFeeTransSettleAmountGreaterThanZero || isInvoiceSettlementDetail) && isNumberSeqManualByRefReceiptTemp) // Validate No.18
				{
					ex.AddData("ERROR.00576", new string[] { numberSeqCode });
				}
				if (isInvoiceSettlementDetailsBySuspenseInvoiceTypeNoneOrSuspenseAccount && companyParamete.SettlementMethodOfPaymentGUID == null) // Validate No.19 & Validate No.20
				{
					ex.AddData("ERROR.90047", new string[] { "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER" });
				}
				if (withdrawalTable.RetentionAmount != 0)
				{
					if (companyParamete.PFRetentionInvTypeGUID == null) // Validate No.21
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.PF_RETENTION_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER" });
					}
					if (autoGenInvoiceRevenueTypeByPFRetentionInvoiceType == null) // Validate No.22
					{
						ex.AddData("ERROR.90047", new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.PF_RETENTION_INVOICE_TYPE_ID" });
					}
					if (pfRetentionInvoiceType == null || pfRetentionInvoiceType.SuspenseInvoiceType != (int)SuspenseInvoiceType.Retention) // Validate No.23
					{
						ex.AddData("ERROR.90114", new string[] { "LABEL.PF_RETENTION_INVOICE_TYPE_ID", "LABEL.SUSPENSE_INVOICE_TYPE", "ENUM.RETENTION" });
					}
				}
				if (isPaymentDetailByVendorAndPaidToTypeCustomerOrVendor)
				{
					if (methodOfPayment == null) // Validate No.24
					{
						ex.AddData("ERROR.90059", new string[] { "LABEL.ACCOUNT_NUMBER", "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID" });
					}
					if (isInvoiceTypeAutoGenInvoiceRevenueTypeNotValue) // Validate No.25
					{
						invoiceTypeIdByAutoGenInvoiceRevenueTypeNotValue.ForEach(invoiceTypeId =>
						{
							ex.AddData("ERROR.90118", new string[] { "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.INVOICE_TYPE", invoiceTypeId });
						});
					}
				}
				#endregion Validate - Company parameter
				#region Validate - Term extension
				if (withdrawalTable.ExtendWithdrawalTableGUID.HasValue)
				{
					// Validate - Term extendsion No.1
					decimal sumSettleAmountByInvoiceSettlementDetailBySuspenseInvoiceTypeSuspenseAccount = invoiceSettlementDetailsBySuspenseInvoiceTypeSuspenseAccount.Sum(s => s.SettleAmount);

					// Validate - Term extendsion No.2
					IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
					WithdrawalTable extendWithdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalTable.ExtendWithdrawalTableGUID.Value);
					IEnumerable<WithdrawalLineItemViewMap> extendWithdrawalLine = withdrawalLineRepo.GetWithdrawalLineItemViewMapByWithdrawalTableGuid(extendWithdrawalTable.WithdrawalTableGUID).Where(w => w.WithdrawalLineType == (int)WithdrawalLineType.Principal);

					if ((sumSettleAmountByServiceFeeTrans + withdrawalTable.SettleTermExtensionFeeAmount + withdrawalTable.RetentionAmount + sumSettleAmountByInvoiceSettlementDetailBySuspenseInvoiceTypeSuspenseAccount) != 0) // Validate - Term extendsion No.1
					{
						ex.AddData("ERROR.90045", new string[] { "LABEL.SETTLE_AMOUNT", "LABEL.SERVICE_FEE_TRANSACTIONS", "LABEL.TERM_EXTENSION_FEE_AMOUNT", "LABEL.RETENTION_AMOUNT", "LABEL.SETTLE_AMOUNT", "LABEL.SUSPENSE_SETTLEMENT" });
					} 
					if (withdrawalTable.WithdrawalAmount != extendWithdrawalLine.Sum(s => s.CustTrans_TransAmount - s.CustTrans_SettleAmount)) // Validate - Term extendsion No.2
					{
						ex.AddData("ERROR.90046", new string[] { "LABEL.EXTEND_WITHDRAWAL_ID", extendWithdrawalTable.WithdrawalId, "LABEL.POST" });
					}
					if (withdrawalTable.TermExtensionFeeAmount != 0)
					{
						if (companyParamete.ServiceFeeInvTypeGUID == null) // Validate - Term extendsion No.3
						{
							ex.AddData("ERROR.90047", new string[] { "LABEL.SERVICE_FEE_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER" });
						}
						if (withdrawalTable.TermExtensionInvoiceRevenueTypeGUID == null) // Validate - Term extendsion No.4
						{
							ex.AddData("ERROR.90042", new string[] { "LABEL.TERM_EXTENSION_SERVICE_FEE_TYPE_ID" });
						}
					}
				}
				#endregion Validate - Term extension

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region cancel withdrawal
		public CancelWithdrawalView GetCancelWithdrawalById(string withdrawalTableGUID)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTableItemView withdrawalTable = withdrawalTableRepo.GetByIdvw(withdrawalTableGUID.StringToGuid());

				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				CreditTerm creditTerm = creditTermRepo.GetCreditTermByIdNoTracking(withdrawalTable.CreditTermGUID.StringToGuid());

				CancelWithdrawalView cancelWithdrawalView = new CancelWithdrawalView
				{
					WithdrawalTableGUID = withdrawalTable.WithdrawalTableGUID.GuidNullToString(),
					WithdrawalTable_Values = SmartAppUtil.GetDropDownLabel(withdrawalTable.WithdrawalId, withdrawalTable.Description),
					BuyerTable_Values = withdrawalTable.BuyerTable_Values,
					CreditTerm_Values = SmartAppUtil.GetDropDownLabel(creditTerm.CreditTermId, creditTerm.Description),
					CustomerTable_Values = withdrawalTable.CustomerTable_Values,
					DocumentStatus_Values = withdrawalTable.DocumentStatus_Values,
					DueDate = withdrawalTable.DueDate,
					TotalInterestPct = withdrawalTable.TotalInterestPct,
					WithdrawalAmount = withdrawalTable.WithdrawalAmount,
					WithdrawalDate = withdrawalTable.WithdrawalDate
				};
				return cancelWithdrawalView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public bool GetCancelWithdrawalValidation(string withdrawalTableGUID)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalTableGUID.StringToGuid());

				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				// Validate No.1
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus withdrawalStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)WithdrawalStatus.Draft).ToString());

				if (withdrawalTable.DocumentStatusGUID != withdrawalStatusDraft.DocumentStatusGUID) // Validate No.1
				{
					ex.AddData("ERROR.90012", new string[] { "LABEL.WITHDRAWAL_TABLE" });
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public CancelWithdrawalResultView CancelWithdrawal(CancelWithdrawalView withdrawalTable)
		{
			try
			{
				// validate
				GetCancelWithdrawalValidation(withdrawalTable.WithdrawalTableGUID);
				CancelWithdrawalResultView result = new CancelWithdrawalResultView();
				NotificationResponse success = new NotificationResponse();
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTableItemView withdrawalTableItemView = withdrawalTableRepo.GetByIdvw(withdrawalTable.WithdrawalTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus withdrawalStatusCancel = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)WithdrawalStatus.Cancelled).ToString());

				withdrawalTableItemView.DocumentStatusGUID = withdrawalStatusCancel.DocumentStatusGUID.GuidNullToString();
				withdrawalTableItemView.DocumentReasonGUID = withdrawalTable.DocumentReasonGUID;

				UpdateWithdrawalTable(withdrawalTableItemView);

				success.AddData("SUCCESS.90008", new string[] { "LABEL.WITHDRAWAL", SmartAppUtil.GetDropDownLabel(withdrawalTableItemView.WithdrawalId, withdrawalTableItemView.Description) });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#endregion
		#region Service fee trans
		public ServiceFeeTransItemView GetServiceFeeTransInitialData(string refGUID)
		{
			try
			{
				IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				string refId = withdrawalTableRepo.GetWithdrawalTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).WithdrawalId.ToString();
				return serviceFeeTransService.GetServiceFeeTransInitialDataByWithdrawalTable(refId, refGUID, Models.Enum.RefType.WithdrawalTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public AccessModeView GetReceiptTempTableAccessMode(string withdrawalTableGUID)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawalTableTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalTableGUID.StringToGuid());

				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(withdrawalTableTable.DocumentStatusGUID);

				bool condition = (documentStatus.StatusId == Convert.ToInt32(WithdrawalStatus.Draft).ToString());

				return new AccessModeView
				{
					CanCreate = false,
					CanDelete = false,
					CanView = true,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Migration
		public void CreateWithdrawalTableCollection(IEnumerable<WithdrawalTableItemView> WithdrawalTableItemView)
		{
			try
			{
				if (WithdrawalTableItemView.Any())
				{
					IWithdrawalTableRepo WithdrawalTableRepo = new WithdrawalTableRepo(db);
					List<WithdrawalTable> WithdrawalTables = WithdrawalTableItemView.ToWithdrawalTable().ToList();
					WithdrawalTableRepo.ValidateAdd(WithdrawalTables);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(WithdrawalTables, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateWithdrawalLineCollection(List<WithdrawalLineItemView> withdrawalLineItemViews)
		{
			try
			{
				if (withdrawalLineItemViews.Any())
				{
					IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
					List<WithdrawalLine> withdrawalLine = withdrawalLineItemViews.ToWithdrawalLine().ToList();
					withdrawalLineRepo.ValidateAdd(withdrawalLine);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(withdrawalLine, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion Migration
		#region print report
		public PrintWithdrawalTable GetPrintWithdrawalTableById(string refGuid)
        {
            try
            {
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTableItemView withdrawalTableItemView = withdrawalTableRepo.GetByIdvw(refGuid.StringToGuid());
				DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatusItemView documentStatusItemView = documentStatusRepo.GetByIdvw(withdrawalTableItemView.DocumentStatusGUID.StringToGuid());
				PrintWithdrawalTable printWithdrawalTable = new PrintWithdrawalTable
				{
					WithdrawalId = withdrawalTableItemView.WithdrawalId,
					DocumentStatus = documentStatusItemView.Description,
					CustomerId = withdrawalTableItemView.CustomerTable_Values,
					BuyerId = withdrawalTableItemView.BuyerTable_Values,
					TermExtension = withdrawalTableItemView.TermExtension,
					NumberOfExtension = withdrawalTableItemView.NumberOfExtension,
					WithdrawalDate = withdrawalTableItemView.WithdrawalDate,
					DueDate = withdrawalTableItemView.DueDate,
					CreditTermId = withdrawalTableItemView.CreditTerm_Values,
					TotalInterestPct = withdrawalTableItemView.TotalInterestPct,
					WithdrawalAmount = withdrawalTableItemView.WithdrawalAmount,
					WithdrawalTableGUID = withdrawalTableItemView.WithdrawalTableGUID

				};
				return printWithdrawalTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PrintWithdrawalTermExtension GetPrintPrintWithdrawalTermExtensionById(string refGuid)
		{
			try
			{
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTableItemView withdrawalTableItemView = withdrawalTableRepo.GetByIdvw(refGuid.StringToGuid());
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTable = customerTableRepo.GetByIdvw(withdrawalTableItemView.CustomerTableGUID.StringToGuid());
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				BuyerTableItemView buyerTableItemView = buyerTableRepo.GetByIdvw(withdrawalTableItemView.BuyerTableGUID.StringToGuid());
				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				CreditTermItemView creditTermItemView = creditTermRepo.GetByIdvw(withdrawalTableItemView.CreditTermGUID.StringToGuid());
				DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				DocumentStatusItemView doc = documentStatusRepo.GetByIdvw(withdrawalTableItemView.DocumentStatusGUID.StringToGuid());
				PrintWithdrawalTermExtension printWithdrawalTermExtension = new PrintWithdrawalTermExtension
				{
					WithdrawalId = withdrawalTableItemView.WithdrawalId,
					DocumentStatus = doc.Description,
					CustomerId = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					BuyerId = SmartAppUtil.GetDropDownLabel(buyerTableItemView.BuyerId, buyerTableItemView.BuyerName),
					TermExtension = withdrawalTableItemView.TermExtension,
					NumberOfExtension = withdrawalTableItemView.NumberOfExtension,
					WithdrawalDate = withdrawalTableItemView.WithdrawalDate,
					DueDate = withdrawalTableItemView.DueDate,
					CreditTermId = SmartAppUtil.GetDropDownLabel(creditTermItemView.CreditTermId, creditTermItemView.Description),
					TotalInterestPct = withdrawalTableItemView.TotalInterestPct,
					WithdrawalAmount = withdrawalTableItemView.WithdrawalAmount,
					WithdrawalTableGUID = withdrawalTableItemView.WithdrawalTableGUID
				};

				return printWithdrawalTermExtension;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#endregion

		#region buyer agreement trans
		public BuyerAgreementTransItemView GetBuyerAgreementTransInitialData(string refGUID)
		{
			try
			{
				IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				WithdrawalTable withdrawal = withdrawalTableRepo.GetWithdrawalTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
				BuyerAgreementTransItemView result = buyerAgreementTransService.GetBuyerAgreementTransInitialData(withdrawal.WithdrawalId, refGUID, Models.Enum.RefType.WithdrawalTable);
				result.BuyerAgreementTableGUID = withdrawal.BuyerAgreementTableGUID.GuidNullToString();
				if(withdrawal.BuyerAgreementTableGUID != null)
                {
					IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
					BuyerAgreementTable buyerAgreementTable = buyerAgreementTableRepo.GetBuyerAgreementTableByIdNoTracking(withdrawal.BuyerAgreementTableGUID.Value);
					result.BuyerAgreementTable_BuyerAgreementId = buyerAgreementTable.BuyerAgreementId;
				}
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		public FileInformation RenderReportWithdrawalTermExtension(RptReportWithdrawalTermExtensionReportView rptReportWithdrawalTermExtensionReportView)
		{
			try
			{
                #region Validate
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
				// 1. To due date < From due date
				if (ConditionService.IsFromDateGreaterThanToDate(rptReportWithdrawalTermExtensionReportView.FromDueDate, rptReportWithdrawalTermExtensionReportView.ToDueDate))
				{
					ex.AddData("ERROR.90165", new string[] { "LABEL.TO_DUE_DATE", "LABEL.FROM_DUE_DATE" });
				}
				// 2. To withdrawal date < From withdrawal date
				if (ConditionService.IsFromDateGreaterThanToDate(rptReportWithdrawalTermExtensionReportView.FromWithdrawalDate, rptReportWithdrawalTermExtensionReportView.ToWithdrawalDate))
				{
					ex.AddData("ERROR.90165", new string[] { "LABEL.TO_WITHDRAWAL_DATE", "LABEL.FROM_WITHDRAWAL_DATE" });
				}
                #endregion Validate

                if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
                else
				{
					IReportViewerService reportViewerService = new ReportViewerService();
					return reportViewerService.RenderReport(rptReportWithdrawalTermExtensionReportView);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
