using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IChequeTableService
    {

        ChequeTableItemView GetChequeTableById(string id);
        ChequeTableItemView CreateChequeTable(ChequeTableItemView chequeTableView);
        ChequeTableItemView UpdateChequeTable(ChequeTableItemView chequeTableView);
        ChequeTableItemViewResult UpdateCompleteCheque(ChequeTableItemView chequeTableItemView);
        ChequeTableItemViewResult UpdateDepositStatus(ChequeTableItemView chequeTableItemView);
        ChequeTableItemViewResult UpdateBounceStatus(ChequeTableItemView chequeTableItemView);
        ChequeTableItemViewResult UpdateReplaceStatus(ChequeTableItemView chequeTableItemView);
        ChequeTableItemViewResult UpdateReturnStatus(ChequeTableItemView chequeTableItemView);
        ChequeTableItemViewResult UpdateCancelStatus(ChequeTableItemView chequeTableItemView);
        AccessModeView GetAccessModeChequeTable(string Id);
        AccessModeView GetAccessModeChequeTableByPurchase(string Id);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusDocumentProcessDropdown(SearchParameter search);
        ChequeTableItemView GetChequeTableInitialData(string id);
        ChequeTableItemView GetChequeTableInitialDataByWithdrawal(string id);
        ChequeTableItemView GetChequeTableInitialDataByPurchase(string id);
        ChequeTableItemView GetChequeTableInitialDataByPDC(string refGUID);
        ChequeTableItemView GetInitialDataCompleteFunction(string id);
        ChequeTableItemView GetInitialDataDepositStatus(string id);
        ChequeTableItemView GetInitialDataBounceStatus(string id);
        ChequeTableItemView GetInitialDataReplaceStatus(string id);
        ChequeTableItemView GetInitialDataReturnStatus(string id);
        ChequeTableItemView GetInitialDataCancelStatus(string id);
        bool DeleteChequeTable(string id);
        IEnumerable<SelectItem<ChequeTableItemView>> GetRefPDCDropDown(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByPurchaseLineCustomer(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByPurchaseLineBuyer(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByWithdrawalLineCustomer(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByWithdrawalLineBuyer(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByReceiptTempPaymDetail(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByCustomerAndDocStatus(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByCustomerBuyerStatus(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByDocStatus(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByCustomerBuyerReplaced(SearchParameter search);

        bool GetDocumentStatusValidation(ChequeTableItemView vmodel);
        bool GetDeposittStatusValidation(ChequeTableItemView vmodel);
        bool GetBouncetStatusValidation(ChequeTableItemView vmodel);
        bool GetReplaceStatusValidation(ChequeTableItemView vmodel);
        bool GetReturnStatusValidation(ChequeTableItemView vmodel);
        bool GetCancelStatusValidation(ChequeTableItemView vmodel);
        void CreateChequeTableCollection(List<ChequeTableItemView> chequeTableItemViews);
    }
    public class ChequeTableService : SmartAppService, IChequeTableService
    {
        public ChequeTableService(SmartAppDbContext context) : base(context) { }
        public ChequeTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public ChequeTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ChequeTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public ChequeTableService() : base() { }

        public ChequeTableItemView GetChequeTableById(string id)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                return chequeTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView CreateChequeTable(ChequeTableItemView chequeTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                chequeTableView = accessLevelService.AssignOwnerBU(chequeTableView);
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                ChequeTable chequeTable = chequeTableView.ToChequeTable();
                chequeTable = chequeTableRepo.CreateChequeTable(chequeTable);
                base.LogTransactionCreate<ChequeTable>(chequeTable);
                UnitOfWork.Commit();
                return chequeTable.ToChequeTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView UpdateChequeTable(ChequeTableItemView chequeTableView)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                ChequeTable inputChequeTable = chequeTableView.ToChequeTable();
                ChequeTable dbChequeTable = chequeTableRepo.Find(inputChequeTable.ChequeTableGUID);
                dbChequeTable = chequeTableRepo.UpdateChequeTable(dbChequeTable, inputChequeTable);
                base.LogTransactionUpdate<ChequeTable>(GetOriginalValues<ChequeTable>(dbChequeTable), dbChequeTable);
                UnitOfWork.Commit();
                return dbChequeTable.ToChequeTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteChequeTable(string item)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                Guid chequeTableGUID = new Guid(item);
                ChequeTable chequeTable = chequeTableRepo.Find(chequeTableGUID);
                chequeTableRepo.Remove(chequeTable);
                base.LogTransactionDelete<ChequeTable>(chequeTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusDocumentProcessDropdown(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Cheque);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ChequeTableItemView GetChequeTableInitialDataByPurchase(string id)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeDocumentStatus.Created).ToString());
                var result = chequeTableRepo.GetChequeTableInitialDataByPurchase(id.StringToGuid());
                result.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                result.DocumentStatus_StatusId = documentStatus.Description;
                result.document_StatusId = documentStatus.Description;
                result.DocumentStatus_Values = documentStatus.Description;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }


        }
        public ChequeTableItemView GetChequeTableInitialDataByPDC(string refGUID)
        {

            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeDocumentStatus.Created).ToString());
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                var result = chequeTableRepo.GetChequeTableInitialDataByPurchase(refGUID.StringToGuid());
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(refGUID.StringToGuid());
                result.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                result.RefGUID = refGUID;
                result.RefType = (int)RefType.PurchaseTable;
                result.CustomerTableGUID = purchaseTable.CustomerTableGUID.GuidNullToString();
                result.DocumentStatus_StatusId = documentStatus.StatusId;
                result.DocumentStatus_Values = documentStatus.Description;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ChequeTableItemView GetChequeTableInitialData(string id)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                return chequeTableRepo.GetChequeTableInitialData(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        public ChequeTableItemView GetChequeTableInitialDataByWithdrawal(string id)
        {
            IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
            var result = chequeTableRepo.GetChequeTableInitialDataByWithdrawal(id.StringToGuid());
            return result;

        }
        public AccessModeView GetAccessModeChequeTableByPurchase(string Id)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                AccessModeView accessModeView = purchaseTableRepo.purchaseTableRepoGetAccessModeChequeTableByPurchase(Id.StringToGuid());
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeChequeTable(string Id)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(Id.StringToGuid());
                string _ChequeDocumentStatus = GetStatusIdByChequeTable(Id);
                bool isCreated = (Convert.ToInt32(_ChequeDocumentStatus) <= (int)EnumsDocumentProcessStatus.ChequeDocumentStatus.Created);
                accessModeView.CanCreate = true;
                accessModeView.CanView = true;
                accessModeView.CanDelete = isCreated;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string GetStatusIdByChequeTable(string Id)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(Id.StringToGuid());
                return documentStatusRepo.GetDocumentStatusByIdNoTracking(chequeTable.DocumentStatusGUID).StatusId;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetRefPDCDropDown(SearchParameter search)
        {

            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentService documentService = new DocumentService(db);
                var Replaced = documentService.GetDocumentStatusByStatusId((int)ChequeDocumentStatus.Replaced);
                SearchCondition searchCondition = documentService.GetSearchConditionByStatusID(Replaced.DocumentStatusGUID.ToString());
                search.Conditions.Add(searchCondition);
                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByPurchaseLineCustomer(SearchParameter search)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                search = search.GetParentCondition(new string[] { ChequeTableCondition.RefGUID, ChequeTableCondition.CustomerTableGUID });
                SearchCondition condition1 = SearchConditionService.GetChequeTableByReceivedFromCondition(new List<string>() { Convert.ToInt32(ReceivedFrom.Customer).ToString() });
                SearchCondition condition2 = SearchConditionService.GetChequeTableByRefTypeCondition(new List<string>() { Convert.ToInt32(RefType.PurchaseTable).ToString() });
                SearchCondition condition3 = SearchConditionService.GetChequeTableByStatusIdCondition(new List<string>() { Convert.ToInt32(ChequeStatus.Created).ToString() });

                search.Conditions.Add(condition1);
                search.Conditions.Add(condition2);
                search.Conditions.Add(condition3);

                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByPurchaseLineBuyer(SearchParameter search)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                search = search.GetParentCondition(new string[] { ChequeTableCondition.RefGUID, ChequeTableCondition.BuyerTableGUID });
                SearchCondition condition1 = SearchConditionService.GetChequeTableByReceivedFromCondition(new List<string>() { Convert.ToInt32(ReceivedFrom.Buyer).ToString() });
                SearchCondition condition2 = SearchConditionService.GetChequeTableByRefTypeCondition(new List<string>() { Convert.ToInt32(RefType.PurchaseTable).ToString() });
                SearchCondition condition3 = SearchConditionService.GetChequeTableByStatusIdCondition(new List<string>() { Convert.ToInt32(ChequeStatus.Created).ToString() });

                search.Conditions.Add(condition1);
                search.Conditions.Add(condition2);
                search.Conditions.Add(condition3);

                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByWithdrawalLineCustomer(SearchParameter search)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                search = search.GetParentCondition(new string[] { ChequeTableCondition.RefGUID, ChequeTableCondition.CustomerTableGUID, ChequeTableCondition.BuyerTableGUID });
                SearchCondition condition1 = SearchConditionService.GetChequeTableByReceivedFromCondition(new List<string>() { Convert.ToInt32(ReceivedFrom.Customer).ToString() });
                SearchCondition condition2 = SearchConditionService.GetChequeTableByRefTypeCondition(new List<string>() { Convert.ToInt32(RefType.WithdrawalTable).ToString() });
                SearchCondition condition3 = SearchConditionService.GetChequeTableByStatusIdCondition(new List<string>() { Convert.ToInt32(ChequeStatus.Created).ToString() });

                search.Conditions.Add(condition1);
                search.Conditions.Add(condition2);
                search.Conditions.Add(condition3);

                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByWithdrawalLineBuyer(SearchParameter search)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                search = search.GetParentCondition(new string[] { ChequeTableCondition.RefGUID, ChequeTableCondition.CustomerTableGUID, ChequeTableCondition.BuyerTableGUID });
                SearchCondition condition1 = SearchConditionService.GetChequeTableByReceivedFromCondition(new List<string>() { Convert.ToInt32(ReceivedFrom.Buyer).ToString() });
                SearchCondition condition2 = SearchConditionService.GetChequeTableByRefTypeCondition(new List<string>() { Convert.ToInt32(RefType.WithdrawalTable).ToString() });
                SearchCondition condition3 = SearchConditionService.GetChequeTableByStatusIdCondition(new List<string>() { Convert.ToInt32(ChequeStatus.Created).ToString() });

                search.Conditions.Add(condition1);
                search.Conditions.Add(condition2);
                search.Conditions.Add(condition3);

                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByReceiptTempPaymDetail(SearchParameter search)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                search = search.GetParentCondition(new string[] { ChequeTableCondition.CustomerTableGUID, ChequeTableCondition.BuyerTableGUID, ChequeTableCondition.ReceivedFrom });
                SearchCondition condition = SearchConditionService.GetChequeTableByStatusIdCondition(new List<string>() {
                    Convert.ToInt32(ChequeStatus.Created).ToString(),
                    Convert.ToInt32(ChequeStatus.Deposit).ToString(),
                    Convert.ToInt32(ChequeStatus.Completed).ToString(),
                    Convert.ToInt32(ChequeStatus.Bounced).ToString(),
                    Convert.ToInt32(ChequeStatus.Replaced).ToString(),
                });
                search.Conditions.Add(condition);

                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByCustomerAndDocStatus(SearchParameter search)
        {
            try
            {

                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search.GetParentCondition(new string[] { JobChequeCondition.Customer, JobChequeCondition.DocumentStatusId });
                return sysDropDownService.GetDropDownItemChequeTableBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByCustomerBuyerStatus(SearchParameter search)
        {
            try
            {

                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search.GetParentCondition(new string[] { JobChequeCondition.Customer, JobChequeCondition.Buyer, JobChequeCondition.DocumentStatusId });
                return sysDropDownService.GetDropDownItemChequeTableBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByDocStatus(SearchParameter search)
        {
            try
            {

                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search.GetParentCondition(new string[] { JobChequeCondition.DocumentStatusId });
                return sysDropDownService.GetDropDownItemChequeTableBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableByCustomerBuyerReplaced(SearchParameter search)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                search = search.GetParentCondition(new string[] { ChequeTableCondition.CustomerTableGUID, ChequeTableCondition.BuyerTableGUID, ChequeTableCondition.RefType });
                SearchCondition condition1 = SearchConditionService.GetChequeTableByStatusIdCondition(new List<string>() { Convert.ToInt32(ChequeStatus.Replaced).ToString() });

                search.Conditions.Add(condition1);

                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetDocumentStatusValidation(ChequeTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(vmodel.ChequeTableGUID.StringToGuid());
                DocumentStatus deposit = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Deposit).ToString());

                if (chequeTable.DocumentStatusGUID != deposit.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90054", new string[] { "LABEL.CHEQUE" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetDeposittStatusValidation(ChequeTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(vmodel.ChequeTableGUID.StringToGuid());
                DocumentStatus create = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Created).ToString());
                DocumentStatus bounced = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Bounced).ToString());

                if (chequeTable.DocumentStatusGUID != create.DocumentStatusGUID && chequeTable.DocumentStatusGUID != bounced.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90053", new string[] { "LABEL.CHEQUE" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ChequeTableItemViewResult UpdateCompleteCheque(ChequeTableItemView chequeTableItemView)
        {
            try
            {
                ChequeTableItemViewResult result = new ChequeTableItemViewResult();
                NotificationResponse success = new NotificationResponse();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Completed).ToString());
                ChequeTableItemView chequeTableItem = chequeTableRepo.GetByIdvw(chequeTableItemView.ChequeTableGUID.StringToGuid());
                chequeTableItem.CompletedDate = chequeTableItemView.CompletedDate;
                chequeTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();

                UpdateChequeTable(chequeTableItem);

                success.AddData("SUCCESS.90016", new string[] { "LABEL.CHEQUE", chequeTableItem.ChequeNo });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ChequeTableItemView GetInitialDataCompleteFunction(string id)
        {
            try
            {
                ChequeTableItemView chequeTablemodel = new ChequeTableItemView();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                chequeTablemodel = chequeTableRepo.GetByIdvw(id.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Completed).ToString());
                chequeTablemodel.DocumentStatus_Values = documentStatus.Description;
                return chequeTablemodel;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView GetInitialDataDepositStatus(string id)
        {
            try
            {
                ChequeTableItemView chequeTablemodel = new ChequeTableItemView();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                chequeTablemodel = chequeTableRepo.GetByIdvw(id.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Deposit).ToString());
                chequeTablemodel.DocumentStatus_Values = documentStatus.Description;
                return chequeTablemodel;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemViewResult UpdateDepositStatus(ChequeTableItemView chequeTableItemView)
        {
            try
            {
                ChequeTableItemViewResult result = new ChequeTableItemViewResult();
                NotificationResponse success = new NotificationResponse();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Deposit).ToString());
                ChequeTableItemView chequeTableItem = chequeTableRepo.GetByIdvw(chequeTableItemView.ChequeTableGUID.StringToGuid());
                chequeTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();

                UpdateChequeTable(chequeTableItem);

                success.AddData("SUCCESS.90016", new string[] { "LABEL.CHEQUE", chequeTableItem.ChequeNo });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetBouncetStatusValidation(ChequeTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(vmodel.ChequeTableGUID.StringToGuid());
                DocumentStatus deposit = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Deposit).ToString());

                if (chequeTable.DocumentStatusGUID != deposit.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90054", new string[] { "LABEL.CHEQUE" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView GetInitialDataBounceStatus(string id)
        {
            try
            {
                ChequeTableItemView chequeTablemodel = new ChequeTableItemView();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                chequeTablemodel = chequeTableRepo.GetByIdvw(id.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Bounced).ToString());
                chequeTablemodel.DocumentStatus_Values = documentStatus.Description;
                return chequeTablemodel;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemViewResult UpdateBounceStatus(ChequeTableItemView chequeTableItemView)
        {
            try
            {
                ChequeTableItemViewResult result = new ChequeTableItemViewResult();
                NotificationResponse success = new NotificationResponse();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Bounced).ToString());
                ChequeTableItemView chequeTableItem = chequeTableRepo.GetByIdvw(chequeTableItemView.ChequeTableGUID.StringToGuid());
                chequeTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();

                UpdateChequeTable(chequeTableItem);

                success.AddData("SUCCESS.90016", new string[] { "LABEL.CHEQUE", chequeTableItem.ChequeNo });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetReplaceStatusValidation(ChequeTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(vmodel.ChequeTableGUID.StringToGuid());
                DocumentStatus create = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Created).ToString());
                DocumentStatus bounced = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Bounced).ToString());

                if (chequeTable.DocumentStatusGUID != create.DocumentStatusGUID && chequeTable.DocumentStatusGUID != bounced.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90053", new string[] { "LABEL.CHEQUE" });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView GetInitialDataReplaceStatus(string id)
        {
            try
            {
                ChequeTableItemView chequeTablemodel = new ChequeTableItemView();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                chequeTablemodel = chequeTableRepo.GetByIdvw(id.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Replaced).ToString());
                chequeTablemodel.DocumentStatus_Values = documentStatus.Description;
                return chequeTablemodel;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemViewResult UpdateReplaceStatus(ChequeTableItemView chequeTableItemView)
        {
            try
            {
                ChequeTableItemViewResult result = new ChequeTableItemViewResult();
                NotificationResponse success = new NotificationResponse();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Replaced).ToString());
                ChequeTableItemView chequeTableItem = chequeTableRepo.GetByIdvw(chequeTableItemView.ChequeTableGUID.StringToGuid());
                chequeTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();

                UpdateChequeTable(chequeTableItem);

                success.AddData("SUCCESS.90016", new string[] { "LABEL.CHEQUE", chequeTableItem.ChequeNo });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetReturnStatusValidation(ChequeTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(vmodel.ChequeTableGUID.StringToGuid());
                DocumentStatus create = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Created).ToString());
                DocumentStatus bounced = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Bounced).ToString());

                if (chequeTable.DocumentStatusGUID != create.DocumentStatusGUID && chequeTable.DocumentStatusGUID != bounced.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90053", new string[] { "LABEL.CHEQUE" });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView GetInitialDataReturnStatus(string id)
        {
            try
            {
                ChequeTableItemView chequeTablemodel = new ChequeTableItemView();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                chequeTablemodel = chequeTableRepo.GetByIdvw(id.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Returned).ToString());
                chequeTablemodel.DocumentStatus_Values = documentStatus.Description;
                return chequeTablemodel;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemViewResult UpdateReturnStatus(ChequeTableItemView chequeTableItemView)
        {
            try
            {
                ChequeTableItemViewResult result = new ChequeTableItemViewResult();
                NotificationResponse success = new NotificationResponse();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Returned).ToString());
                ChequeTableItemView chequeTableItem = chequeTableRepo.GetByIdvw(chequeTableItemView.ChequeTableGUID.StringToGuid());
                chequeTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();

                UpdateChequeTable(chequeTableItem);

                success.AddData("SUCCESS.90016", new string[] { "LABEL.CHEQUE", chequeTableItem.ChequeNo });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetCancelStatusValidation(ChequeTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ChequeTable chequeTable = chequeTableRepo.GetChequeTableByIdNoTracking(vmodel.ChequeTableGUID.StringToGuid());
                DocumentStatus create = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)ChequeStatus.Created).ToString());


                if (chequeTable.DocumentStatusGUID != create.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90055", new string[] { "LABEL.CHEQUE" });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemView GetInitialDataCancelStatus(string id)
        {
            try
            {
                ChequeTableItemView chequeTablemodel = new ChequeTableItemView();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                chequeTablemodel = chequeTableRepo.GetByIdvw(id.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Cancelled).ToString());
                chequeTablemodel.DocumentStatus_Values = documentStatus.Description;
                return chequeTablemodel;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ChequeTableItemViewResult UpdateCancelStatus(ChequeTableItemView chequeTableItemView)
        {
            try
            {
                ChequeTableItemViewResult result = new ChequeTableItemViewResult();
                NotificationResponse success = new NotificationResponse();
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)ChequeStatus.Cancelled).ToString());
                ChequeTableItemView chequeTableItem = chequeTableRepo.GetByIdvw(chequeTableItemView.ChequeTableGUID.StringToGuid());
                chequeTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();

                UpdateChequeTable(chequeTableItem);

                success.AddData("SUCCESS.90016", new string[] { "LABEL.CHEQUE", chequeTableItem.ChequeNo });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region migration
        public void CreateChequeTableCollection(List<ChequeTableItemView> chequeTableItemViews)
        {
            try
            {
                if (chequeTableItemViews.Any())
                {
                    IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                    List<ChequeTable> chequeTable = chequeTableItemViews.ToChequeTable().ToList();
                    chequeTableRepo.ValidateAdd(chequeTable);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(chequeTable, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion
    }
}
