using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBuyerTableService
	{

		BuyerTableItemView GetBuyerTableById(string id);
		BuyerTableItemView CreateBuyerTable(BuyerTableItemView buyerTableView);
		BuyerTableItemView UpdateBuyerTable(BuyerTableItemView buyerTableView);
		bool DeleteBuyerTable(string id);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemBuyerStatus(SearchParameter search);
		bool IsManualByBuyer(string companyId);
		AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID);
		MemoTransItemView GetMemoTransInitialData(string refId);
		ContactPersonTransItemView GetContactPersonTransInitialData(string refGUID);
		ContactTransItemView GetContactTransInitialData(string refGUID);
		BuyerTableItemView GetBuyerTableInitialData(string companyGUID);
		BuyerAgreementLineItemView GetBuyerAgreementLineInitialData(string buyAgreementTableGUID);
		AddressTransItemView GetAddressTransInitialData(string refGUID);
		DocumentConditionTransItemView GetDocumentConditionTransInitialData(string refGUID);
		FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refGUID);
		IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByCreditAppRequest(SearchParameter search);
		IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search);
		IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByCreditAppTable(SearchParameter search);
		IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search);
		SearchResult<CreditOutstandingViewMap> GetCreditOutstandingByCustomer(Guid BuyerTableGUID, DateTime asOfDate);
		void CreateBuyerTableCollection(List<BuyerTableItemView> buyerTableItemViews);
		#region function
		UpdateBuyerTableBlacklistStatusResultView GetUpdateBuyerTableBlacklistStatusById(string id);
		UpdateBuyerTableBlacklistStatusResultView UpdateBuyerTableBlacklistStatus(UpdateBuyerTableBlacklistStatusParamView functionView);
        #endregion
    }
    public class BuyerTableService : SmartAppService, IBuyerTableService
	{
		public BuyerTableService(SmartAppDbContext context) : base(context) { }
		public BuyerTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BuyerTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BuyerTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BuyerTableService() : base() { }

		public BuyerTableItemView GetBuyerTableById(string id)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				return buyerTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerTableItemView CreateBuyerTable(BuyerTableItemView buyerTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				buyerTableView = accessLevelService.AssignOwnerBU(buyerTableView);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				BuyerTable buyerTable = buyerTableView.ToBuyerTable();
				GenBuyerNumberSeqCode(buyerTable);
				buyerTable = buyerTableRepo.CreateBuyerTable(buyerTable);
				base.LogTransactionCreate<BuyerTable>(buyerTable);
				UnitOfWork.Commit();
				return buyerTable.ToBuyerTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerTableItemView UpdateBuyerTable(BuyerTableItemView buyerTableView)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				BuyerTable inputBuyerTable = buyerTableView.ToBuyerTable();
				BuyerTable dbBuyerTable = buyerTableRepo.Find(inputBuyerTable.BuyerTableGUID);
				dbBuyerTable = buyerTableRepo.UpdateBuyerTable(dbBuyerTable, inputBuyerTable);
				base.LogTransactionUpdate<BuyerTable>(GetOriginalValues<BuyerTable>(dbBuyerTable), dbBuyerTable);
				UnitOfWork.Commit();
				return dbBuyerTable.ToBuyerTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBuyerTable(string item)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				Guid buyerTableGUID = new Guid(item);
				BuyerTable buyerTable = buyerTableRepo.Find(buyerTableGUID);
				buyerTableRepo.Remove(buyerTable);
				base.LogTransactionDelete<BuyerTable>(buyerTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region NumberSeq
		public void GenBuyerNumberSeqCode(BuyerTable buyerTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(buyerTable.CompanyGUID, ReferenceId.Buyer);
                if (!isManual)
                {
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(buyerTable.CompanyGUID, ReferenceId.Buyer);
					buyerTable.BuyerId = numberSequenceService.GetNumber(buyerTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public bool IsManualByBuyer(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.Buyer);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion NumberSeq
		#region InitialData
		public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
				IMemoTransService memoTransService = new MemoTransService(db);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				string refId = buyerTableRepo.GetBuyerTableByIdNoTracking(refGUID.StringToGuid()).BuyerId;
				return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.Buyer);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactPersonTransItemView GetContactPersonTransInitialData(string refGUID)
		{
			try
			{
				IContactPersonTransService contactPersonTransService = new ContactPersonTransService(db);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				string refId = buyerTableRepo.GetBuyerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).BuyerId;
				return contactPersonTransService.GetContactPersonTransInitialData(refId, refGUID, Models.Enum.RefType.Buyer);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactTransItemView GetContactTransInitialData(string refGUID)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				string refId = buyerTableRepo.GetBuyerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).BuyerId;
				return ContactTransService.GetContactTransInitialData(refId, refGUID, Models.Enum.RefType.Buyer);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID)
		{
			try
			{
				IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				string refId = buyerTableRepo.GetBuyerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).BuyerId;
				return authorizedPersonTransService.GetAuthorizedPersonTransInitialData(refId, refGUID, Models.Enum.RefType.Buyer);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BuyerTableItemView GetBuyerTableInitialData(string companyGUID)
		{
			try
			{
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTrackingByAccessLevel(companyGUID.StringToGuid());
				return new BuyerTableItemView
				{
					BuyerTableGUID = new Guid().GuidNullToString(),
					CurrencyGUID = companyParameter.HomeCurrencyGUID.GuidNullToString(),
					IdentificationType = (int)IdentificationType.TaxId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public BuyerAgreementLineItemView GetBuyerAgreementLineInitialData(string buyAgreementTableGUID)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
				IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
				BuyerAgreementTable buyerAgreementTable = buyerAgreementTableRepo.GetBuyerAgreementTableByIdNoTrackingByAccessLevel(buyAgreementTableGUID.StringToGuid());
				List<BuyerAgreementLine> buyerAgreementLines = buyerAgreementLineRepo.GetBuyerAgreementLineByBuyerAgreementTableIdNoTracking(buyAgreementTableGUID.StringToGuid()).ToList();
				return new BuyerAgreementLineItemView
				{
					BuyerAgreementTableGUID = buyAgreementTableGUID,
					BuyerAgreementTable_Values = SmartAppUtil.GetDropDownLabel(buyerAgreementTable.BuyerAgreementId, buyerAgreementTable.Description),
					Period = buyerAgreementLines.Any() ? buyerAgreementLines.Max(m => m.Period) + 1 : 1,
				};
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

		public AddressTransItemView GetAddressTransInitialData(string refGUID)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				string refId = buyerTableRepo.GetBuyerTableByIdNoTracking(refGUID.StringToGuid()).BuyerId;
				return addressTransService.GetAddressTransInitialData(refId, refGUID, Models.Enum.RefType.Buyer);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTransItemView GetDocumentConditionTransInitialData(string refGUID)
		{
			try
			{
				IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				string refId = buyerTableRepo.GetBuyerTableByIdNoTracking(refGUID.StringToGuid()).BuyerId;
				return documentConditionTransService.GetDocumentConditionTransInitialData(refId, refGUID, Models.Enum.RefType.Buyer);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refGUID)
		{
			try
			{
				IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				string refId = buyerTableRepo.GetBuyerTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).BuyerId;
				return financialStatementTransService.GetFinancialStatementTransInitialData(refId, refGUID, Models.Enum.RefType.Buyer);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region DropDown
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemBuyerStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Buyer);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByCreditAppRequest(SearchParameter search)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				return buyerTableRepo.GetDropDownItemByCreditAppRequest(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByMainAgreementTable(SearchParameter search)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				search = search.GetParentCondition(BuyerTableCondition.CreditAppTableGUID);
				return buyerTableRepo.GetDropDownItemByMainAgreementTable(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByCreditAppTable(SearchParameter search)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				search = search.GetParentCondition(BuyerTableCondition.CreditAppTableGUID);
				return buyerTableRepo.GetDropDownItemByMainAgreementTable(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemByPurchaseLine(SearchParameter search)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				search = search.GetParentCondition(new string[] { BuyerTableCondition.CreditAppTableGUID, BuyerTableCondition.ExpiryDate });
				DateTime purchaseDate = search.Conditions[1].Value.StringToDate();
				search.Conditions.RemoveAt(1);
				return buyerTableRepo.GetDropDownItemByPurchaseLine(search, purchaseDate);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropDown
		#region function
		public UpdateBuyerTableBlacklistStatusResultView GetUpdateBuyerTableBlacklistStatusById(string id)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				UpdateBuyerTableBlacklistStatusResultView buyerTableItemView = buyerTableRepo.GetBlacklistStatusById(id.StringToGuid());				
				return buyerTableItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public UpdateBuyerTableBlacklistStatusResultView UpdateBuyerTableBlacklistStatus(UpdateBuyerTableBlacklistStatusParamView functionView)
		{
			try
			{
				IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
				NotificationResponse success = new NotificationResponse();
				BuyerTable inputBuyerTable = buyerTableRepo.GetBuyerTableByIdNoTracking(functionView.BuyerTableGUID.StringToGuid());
				inputBuyerTable.BlacklistStatusGUID = functionView.BlacklistStatusGUID.StringToGuid();
				BuyerTable dbBuyerTable = buyerTableRepo.Find(inputBuyerTable.BuyerTableGUID);
				dbBuyerTable = buyerTableRepo.UpdateBuyerTable(dbBuyerTable, inputBuyerTable);
				base.LogTransactionUpdate<BuyerTable>(GetOriginalValues<BuyerTable>(dbBuyerTable), dbBuyerTable);
				UnitOfWork.Commit();
				UpdateBuyerTableBlacklistStatusResultView updateBuyerResultView = new UpdateBuyerTableBlacklistStatusResultView();
				success.AddData("SUCCESS.00075", new string[] { "LABEL.BLACKLIST_STATUS"});
				updateBuyerResultView.Notification = success;
				return updateBuyerResultView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public SearchResult<CreditOutstandingViewMap> GetCreditOutstandingByCustomer(Guid BuyerTableGUID, DateTime asOfDate)
		{
			try
			{
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				SearchParameter search = new SearchParameter();
				var temp1 = creditAppTableRepo.GetCreditOutstandingByCustomer(BuyerTableGUID, asOfDate);
				var result = new SearchResult<CreditOutstandingViewMap>();
				result = temp1.SetSearchResult<CreditOutstandingViewMap>(temp1.Count(), search);
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion
        #region migration
        public void CreateBuyerTableCollection(List<BuyerTableItemView> buyerTableItemViews)
		{
			try
			{
				if (buyerTableItemViews.Any())
				{
					IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
					List<BuyerTable> buyerTable = buyerTableItemViews.ToBuyerTable().ToList();
					buyerTableRepo.ValidateAdd(buyerTable);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(buyerTable, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
        #endregion

    }
}
