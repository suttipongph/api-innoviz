using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBankTypeService
	{

		BankTypeItemView GetBankTypeById(string id);
		BankTypeItemView CreateBankType(BankTypeItemView bankTypeView);
		BankTypeItemView UpdateBankType(BankTypeItemView bankTypeView);
		bool DeleteBankType(string id);
	}
	public class BankTypeService : SmartAppService, IBankTypeService
	{
		public BankTypeService(SmartAppDbContext context) : base(context) { }
		public BankTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BankTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BankTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BankTypeService() : base() { }

		public BankTypeItemView GetBankTypeById(string id)
		{
			try
			{
				IBankTypeRepo bankTypeRepo = new BankTypeRepo(db);
				return bankTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BankTypeItemView CreateBankType(BankTypeItemView bankTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				bankTypeView = accessLevelService.AssignOwnerBU(bankTypeView);
				IBankTypeRepo bankTypeRepo = new BankTypeRepo(db);
				BankType bankType = bankTypeView.ToBankType();
				bankType = bankTypeRepo.CreateBankType(bankType);
				base.LogTransactionCreate<BankType>(bankType);
				UnitOfWork.Commit();
				return bankType.ToBankTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BankTypeItemView UpdateBankType(BankTypeItemView bankTypeView)
		{
			try
			{
				IBankTypeRepo bankTypeRepo = new BankTypeRepo(db);
				BankType inputBankType = bankTypeView.ToBankType();
				BankType dbBankType = bankTypeRepo.Find(inputBankType.BankTypeGUID);
				dbBankType = bankTypeRepo.UpdateBankType(dbBankType, inputBankType);
				base.LogTransactionUpdate<BankType>(GetOriginalValues<BankType>(dbBankType), dbBankType);
				UnitOfWork.Commit();
				return dbBankType.ToBankTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBankType(string item)
		{
			try
			{
				IBankTypeRepo bankTypeRepo = new BankTypeRepo(db);
				Guid bankTypeGUID = new Guid(item);
				BankType bankType = bankTypeRepo.Find(bankTypeGUID);
				bankTypeRepo.Remove(bankType);
				base.LogTransactionDelete<BankType>(bankType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
