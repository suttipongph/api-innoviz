using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ISysDropDownService
    {
        #region AddressCountry
        IEnumerable<SelectItem<AddressCountryItemView>> GetDropDownItemAddressCountry(SearchParameter search);
        #endregion AddressCountry
        #region AddressDistrict
        IEnumerable<SelectItem<AddressDistrictItemView>> GetDropDownItemAddressDistrict(SearchParameter search);
        #endregion AddressDistrict
        #region AddressPostalCode
        IEnumerable<SelectItem<AddressPostalCodeItemView>> GetDropDownItemAddressPostalCode(SearchParameter search);
        #endregion AddressPostalCode
        #region AddressProvince
        IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItemAddressProvince(SearchParameter search);
        #endregion AddressProvince
        #region AddressSubDistrict
        IEnumerable<SelectItem<AddressSubDistrictItemView>> GetDropDownItemAddressSubDistrict(SearchParameter search);
        #endregion AddressSubDistrict
        #region InterestType
        IEnumerable<SelectItem<InterestTypeItemView>> GetDropDownItemInterestType(SearchParameter search);
        #endregion InterestType
        #region CustomerTable
        IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemCustomerTable(SearchParameter search);
        IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemCustomerTableBy(SearchParameter search);
        
        #endregion CustomerTable
        #region Gender
        IEnumerable<SelectItem<GenderItemView>> GetDropDownItemGender(SearchParameter search);
        #endregion Gender
        #region MaritalStatus
        IEnumerable<SelectItem<MaritalStatusItemView>> GetDropDownItemMaritalStatus(SearchParameter search);
        #endregion MaritalStatus
        #region Nationality
        IEnumerable<SelectItem<NationalityItemView>> GetDropDownItemNationality(SearchParameter search);
        #endregion Nationality
        #region Occupation
        IEnumerable<SelectItem<OccupationItemView>> GetDropDownItemOccupation(SearchParameter search);
        #endregion Occupation
        #region Race
        IEnumerable<SelectItem<RaceItemView>> GetDropDownItemRace(SearchParameter search);
        #endregion Race
        #region BankGroup
        IEnumerable<SelectItem<BankGroupItemView>> GetDropDownItemBankGroup(SearchParameter search);
        #endregion BankGroup
        #region RegistrationType
        IEnumerable<SelectItem<RegistrationTypeItemView>> GetDropDownItemRegistrationType(SearchParameter search);
        #endregion RegistrationType
        #region BusinessCollateralType
        IEnumerable<SelectItem<BusinessCollateralTypeItemView>> GetDropDownItemBusinessCollateralType(SearchParameter search);
        #endregion BusinessCollateralType
        #region BlacklistStatus
        IEnumerable<SelectItem<BlacklistStatusItemView>> GetDropDownItemBlacklistStatus(SearchParameter search);
        #endregion BlacklistStatus
        #region BusinessSegment
        IEnumerable<SelectItem<BusinessSegmentItemView>> GetDropDownItemBusinessSegment(SearchParameter search);
        #endregion BusinessSegment
        #region BusinessSize
        IEnumerable<SelectItem<BusinessSizeItemView>> GetDropDownItemBusinessSize(SearchParameter search);
        #endregion BusinessSize
        #region BusinessType
        IEnumerable<SelectItem<BusinessTypeItemView>> GetDropDownItemBusinessType(SearchParameter search);
        #endregion BusinessType
        #region CompanyBank
        IEnumerable<SelectItem<CompanyBankItemView>> GetDropDownItemCompanyBank(SearchParameter search);
        #endregion CompanyBank
        #region CreditScoring
        IEnumerable<SelectItem<CreditScoringItemView>> GetDropDownItemCreditScoring(SearchParameter search);
        #endregion CreditScoring
        #region Currency
        IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItemCurrency(SearchParameter search);
        IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItemPlusExchangeRate(SearchParameter search);
        
        #endregion Currency
        #region DocumentReason
        IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItemDocumentReason(SearchParameter search);
        #endregion DocumentReason
        #region GuarantorType
        IEnumerable<SelectItem<GuarantorTypeItemView>> GetDropDownItemGuarantorType(SearchParameter search);
        #endregion GuarantorType
        #region KYC
        IEnumerable<SelectItem<KYCSetupItemView>> GetDropDownItemKYCSetup(SearchParameter search);
        #endregion KYC
        #region LineOfBusiness
        IEnumerable<SelectItem<LineOfBusinessItemView>> GetDropDownItemLineOfBusiness(SearchParameter search);
        #endregion LineOfBusiness
        #region Language
        IEnumerable<SelectItem<LanguageItemView>> GetDropDownItemLanguage(SearchParameter search);
        #endregion Language
        #region VendGroup
        IEnumerable<SelectItem<VendGroupItemView>> GetDropDownItemVendGroup(SearchParameter search);
        #endregion VendGroup
        #region GradeClassification
        IEnumerable<SelectItem<GradeClassificationItemView>> GetDropDownItemGradeClassification(SearchParameter search);
        #endregion GradeClassification
        #region CustGroup
        IEnumerable<SelectItem<CustGroupItemView>> GetDropDownItemCustGroup(SearchParameter search);
        #endregion CustGroup
        #region EmployeeTable
        IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItemEmployeeTable(SearchParameter search);
        #endregion EmployeeTable
        #region IntroducedBy
        IEnumerable<SelectItem<IntroducedByItemView>> GetDropDownItemIntroducedBy(SearchParameter search);
        #endregion IntroducedBy
        #region LedgerDimension
        IEnumerable<SelectItem<LedgerDimensionItemView>> GetDropDownItemLedgerDimension(SearchParameter search);
        #endregion LedgerDimension
        #region MethodOfPayment
        IEnumerable<SelectItem<MethodOfPaymentItemView>> GetDropDownItemMethodOfPayment(SearchParameter search);
        #endregion MethodOfPayment
        #region NCBAccountStatus
        IEnumerable<SelectItem<NCBAccountStatusItemView>> GetDropDownItemNCBAccountStatus(SearchParameter search);
        #endregion NCBAccountStatus
        #region NumberSeqTable
        IEnumerable<SelectItem<NumberSeqTableItemView>> GetDropDownItemNumberSeqTable(SearchParameter search);
        #endregion NumberSeqTable
        #region NumberSeqSegmentType
        IEnumerable<SelectItem<NumberSeqSegmentItemView>> GetDropDownItemNumberSeqSegment(SearchParameter search);
        #endregion NumberSeqSegmentType
        #region ParentCompany
        IEnumerable<SelectItem<ParentCompanyItemView>> GetDropDownItemParentCompany(SearchParameter search);
        #endregion ParentCompany
        #region Territory
        IEnumerable<SelectItem<TerritoryItemView>> GetDropDownItemTerritory(SearchParameter search);
        #endregion Territory
        #region VendorTable
        IEnumerable<SelectItem<VendorTableItemView>> GetDropDownItemVendorTable(SearchParameter search);
        #endregion VendorTableBu
        #region WithholdingTaxGroup
        IEnumerable<SelectItem<WithholdingTaxGroupItemView>> GetDropDownItemWithholdingTaxGroup(SearchParameter search);
        #endregion WithholdingTaxGroup
        #region RelatedPersonTable
        IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTable(SearchParameter search);
        #endregion RelatedPersonTable
        #region BuyerTable
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemBuyerTable(SearchParameter search);
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemBuyerTableBy(SearchParameter search);
        #endregion
        #region BuyerInvoiceTable
        IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItemBuyerInvoiceTable(SearchParameter search);
        #endregion BuyerInvoiceTable
        #region AuthorizedPersonType
        public IEnumerable<SelectItem<AuthorizedPersonTypeItemView>> GetDropDownItemAuthorizedPersonType(SearchParameter search);
        #endregion
        #region TaxTable
        IEnumerable<SelectItem<TaxTableItemView>> GetDropDownItemTaxTable(SearchParameter search);
        #endregion TaxTable
        #region TaxValue
        IEnumerable<SelectItem<TaxValueItemView>> GetDropDownItemTaxValue(SearchParameter search);
        #endregion TaxValue
        #region BuyerAgreementTable
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTable(SearchParameter search);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(SearchParameter search);
        #endregion BuyerAgreementTable
        #region ExposureGroup
        IEnumerable<SelectItem<ExposureGroupItemView>> GetDropDownItemExposureGroup(SearchParameter search);
        #endregion ExposureGroup
        #region Ownership 
        IEnumerable<SelectItem<OwnershipItemView>> GetDropDownItemOwnership(SearchParameter search);
        #endregion Ownership
        #region PropertyType 
        IEnumerable<SelectItem<PropertyTypeItemView>> GetDropDownItemPropertyType(SearchParameter search);
        #endregion PropertyType
        #region EmplTeam
        IEnumerable<SelectItem<EmplTeamItemView>> GetDropDownItemEmplTeam(SearchParameter search);
        #endregion EmplTeam
        #region Department
        IEnumerable<SelectItem<DepartmentItemView>> GetDropDownItemDepartment(SearchParameter search);
        #endregion Department
        #region ServiceFeeCondTemplateTable
        IEnumerable<SelectItem<ServiceFeeCondTemplateTableItemView>> GetDropDownItemServiceFeeCondTemplateTable(SearchParameter search);
        #endregion ServiceFeeCondTemplateTable
        #region InvoiceRevenueType
        IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetDropDownItemInvoiceRevenueType(SearchParameter search);
        #endregion InvoiceRevenueType
        #region IntercompanyTable
        IEnumerable<SelectItem<IntercompanyItemView>> GetDropDownItemIntercompanyTable(SearchParameter search);
        #endregion IntercompanyTable
        #region DocumentType
        IEnumerable<SelectItem<DocumentTypeItemView>> GetDropDownItemDocumentType(SearchParameter search);
        #endregion DocumentType
        #region AddressTrans
        IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTrans(SearchParameter search);
        #endregion AddressTrans
        #region ApplicationTable
        //IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItemApplicationTable(SearchParameter search);
        #endregion ApplicationTable
        #region AssignmentAgreementTable
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementTable(SearchParameter search);
        #endregion AssignmentAgreementTable
        #region AssignmentMethod
        IEnumerable<SelectItem<AssignmentMethodItemView>> GetDropDownItemAssignmentMethod(SearchParameter search);
        #endregion AssignmentMethod
        #region BillingResponsibleBy
        IEnumerable<SelectItem<BillingResponsibleByItemView>> GetDropDownItemBillingResponsibleBy(SearchParameter search);
        #endregion BillingResponsibleBy
        #region ConsortiumTable
        IEnumerable<SelectItem<ConsortiumTableItemView>> GetDropDownItemConsortiumTable(SearchParameter search);
        #endregion ConsortiumTable
        #region ContactPersonTrans
        IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemContactPersonTrans(SearchParameter search);
        #endregion ContactPersonTrans
        #region CreditAppRequestTable
        IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTable(SearchParameter search);
        #endregion CreditAppRequestTable
        #region CreditAppTable
        IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTable(SearchParameter search);
        #endregion CreditAppTable
        #region CreditLimitType
        IEnumerable<SelectItem<CreditLimitTypeItemView>> GetDropDownItemCreditLimitType(SearchParameter search);
        #endregion CreditLimitType
        #region CreditTerm
        IEnumerable<SelectItem<CreditTermItemView>> GetDropDownItemCreditTerm(SearchParameter search);
        #endregion CreditTerm
        #region CustBank
        IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBank(SearchParameter search);
        #endregion CustBank
        #region ProductSubType
        IEnumerable<SelectItem<ProductSubTypeItemView>> GetDropDownItemProductSubType(SearchParameter search);
        #endregion ProductSubType
        #region WithholdingTaxValue
        IEnumerable<SelectItem<WithholdingTaxValueItemView>> GetDropDownItemWithholdingTaxValue(SearchParameter search);
        #endregion WithholdingTaxValue
        #region WithholdingTaxTable
        IEnumerable<SelectItem<WithholdingTaxTableItemView>> GetDropDownItemWithholdingTaxTable(SearchParameter search);
        #endregion WithholdingTaxTable
        #region Branch
        IEnumerable<SelectItem<BranchItemView>> GetDropDownItemBranch(SearchParameter search);
        #endregion Branch
        #region BankType
        IEnumerable<SelectItem<BankTypeItemView>> GetDropDownItemBankType(SearchParameter search);
        #endregion BankType
        #region BusiniessUnit
        IEnumerable<SelectItem<BusinessUnitItemView>> GetDropDownItemBusinessUnit(SearchParameter search);
        #endregion BusiniessUnit
        #region DocumentConditionTemplateTable
        IEnumerable<SelectItem<DocumentConditionTemplateTableItemView>> GetDropDownItemDocumentConditionTemplateTable(SearchParameter search);
        #endregion DocumentConditionTemplateTable
        #region CreditType
        IEnumerable<SelectItem<CreditTypeItemView>> GetDropDownItemCreditType(SearchParameter search);
        #endregion CreditType
        #region CalendarGroup
        IEnumerable<SelectItem<CalendarGroupItemView>> GetDropDownItemCalendarGroup(SearchParameter search);
        #endregion CalendarGroup
        #region ApplicationTable
        IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItemApplicationTable(SearchParameter search);
        #endregion

        #region DocumentTemplateTable
        IEnumerable<SelectItem<DocumentTemplateTableItemView>> GetDropDownItemDocumentTemplateTable(SearchParameter search);
        #endregion DocumentTemplateTable
        #region BuyerAgreementLine
        IEnumerable<SelectItem<BuyerAgreementLineItemView>> GetDropDownItemBuyerAgreementLine(SearchParameter search);
        #endregion BuyerAgreementLine
        #region InvoiceType
        IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItemInvoiceType(SearchParameter search);
        #endregion
        #region BusinessCollateralStatus
        IEnumerable<SelectItem<BusinessCollateralStatusItemView>> GetDropDownItemBusinessCollateralStatus(SearchParameter search);
        #endregion
        #region BusinessCollateralSubType
        IEnumerable<SelectItem<BusinessCollateralSubTypeItemView>> GetDropDownItemBusinessCollateralSubType(SearchParameter search);

        #endregion
        #region WithdrawalTable
        IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItemWithdrawalTable(SearchParameter search);
        #endregion
        #region BookmarkDocument
        IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocument(SearchParameter search);
        #endregion BookmarkDocument
        #region BookmarkDocumentTemplateTable
        IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search);
        #endregion BookmarkDocumentTemplateTable
        #region LedgerFiscalYear
        IEnumerable<SelectItem<LedgerFiscalYearItemView>> GetDropDownItemLedgerFiscalYear(SearchParameter search);
        #endregion
        #region CustBusinessCollateral
        IEnumerable<SelectItem<CustBusinessCollateralItemView>> GetDropDownItemCustBusinessCollateral(SearchParameter search);
        #endregion
        #region AuthorizedPersonTrans
        IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTrans(SearchParameter search);
        #endregion
        #region CompanySignature
        IEnumerable<SelectItem<CompanySignatureItemView>> GetDropDownItemCompanySignature(SearchParameter search);
        #endregion
        #region PurchaseLine
        IEnumerable<SelectItem<PurchaseLineItemView>> GetDropDownItemPurchaseLine(SearchParameter search);
        #endregion PurchaseLine
        #region PurchaseTable
        IEnumerable<SelectItem<PurchaseTableItemView>> GetDropDownItemPurchaseTable(SearchParameter search);
        #endregion PurchaseTable
        #region ConsortiumLine
        IEnumerable<SelectItem<ConsortiumLineItemView>> GetDropDownItemConsortiumLine(SearchParameter search);
        #endregion
        #region Company
        IEnumerable<SelectItem<CompanyItemView>> GetDropDownItemCompany(SearchParameter search);
        #endregion Company
        #region ChequeTable
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTable(SearchParameter search);
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableBy(SearchParameter search);
        #endregion ChequeTable
        #region VerificationType
        IEnumerable<SelectItem<VerificationTypeItemView>> GetDropDownItemVerificationType(SearchParameter search);
        #endregion
        #region DocumentStatus
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemDocumentStatus(SearchParameter search);
        #endregion DocumentStatus
        #region AssignmentAgreementSettle
        IEnumerable<SelectItem<AssignmentAgreementSettleItemView>> GetDropDownItemAssignmentAgreementSettle(SearchParameter search);
        #endregion AssignmentAgreementSettle
        #region CreditAppReqBusinessCollateral
        IEnumerable<SelectItem<CreditAppReqBusinessCollateralItemView>> GetDropDownItemCreditAppReqBusinessCollateral(SearchParameter search);
        #endregion
        #region JobType
        IEnumerable<SelectItem<JobTypeItemView>> GetDropDownItemJobType(SearchParameter search);
        #endregion JobType
        #region InvoiceTable
        IEnumerable<SelectItem<InvoiceTableItemView>> GetDropDownItemInvoiceTable(SearchParameter search);
        #endregion InvoiceTable
        #region ProdUnitTable
        IEnumerable<SelectItem<ProdUnitItemView>> GetDropDownItemProdUnitTable(SearchParameter search);
        #endregion
        #region TaxInvoiceTable
        IEnumerable<SelectItem<TaxInvoiceTableItemView>> GetDropDownItemTaxInvoiceTable(SearchParameter search);
        #endregion TaxInvoiceTable

        #region GuarantorTrans
        IEnumerable<SelectItem<GuarantorTransItemView>> GetDropDownItemGuarantorTrans(SearchParameter search);
        #endregion GuarantorTrans
        #region GetVerificationTableDropDown
        IEnumerable<SelectItem<VerificationTableItemView>> GetVerificationTableDropDown(SearchParameter search);
        #endregion GetVerificationTableDropDown
        IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemWithDocumentStatusChequeTable(SearchParameter search, string statusCode);
        #region VerificationTable
        IEnumerable<SelectItem<VerificationTableItemView>> GetDropDownItemVerificationTable(SearchParameter search);
        #endregion
        #region GetFreeTextInvoiceTableDropDown
        IEnumerable<SelectItem<FreeTextInvoiceTableItemView>> GetFreeTextInvoiceTableDropDown(SearchParameter search);
        #endregion GetFreeTextInvoiceTableDropDown
        #region BuyerAgreementTrans
        IEnumerable<SelectItem<BuyerAgreementTransItemView>> GetDropDownItemBuyerAgreementTrans(SearchParameter search);
        #endregion
        #region CreditAppLine
        IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLine(SearchParameter search);
        #endregion
        #region ReceiptTempTable
        IEnumerable<SelectItem<ReceiptTempTableItemView>> GetDropDownItemReceiptTempTable(SearchParameter search);
        #endregion
        #region MessengerJobTable
        IEnumerable<SelectItem<MessengerJobTableItemView>> GetDropDownItemMessengerJobTable(SearchParameter search);
        #endregion
        #region MessengerTable
        IEnumerable<SelectItem<MessengerTableItemView>> GetDropDownItemMessengerTable(SearchParameter search);
        #endregion MessengerTable
        #region ContactPerson
        IEnumerable<SelectItem<ContactPersonTransItemView>> GetContactPersonDropDown(SearchParameter search);
        #endregion MessengerTable

        #region CollectionFollowUp
        IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItemCollectionFollowUp(SearchParameter search);
        #endregion CollectionFollowUp

        #region BuyerReceiptTable
        IEnumerable<SelectItem<BuyerReceiptTableItemView>> GetDropDownItemBuyerReceiptTable(SearchParameter search);
        #endregion BuyerReceiptTable
        #region InvoiceSettlementDetail
        IEnumerable<SelectItem<InvoiceSettlementDetailItemView>> GetDropDownItemInvoiceSettlementDetail(SearchParameter search);
        #endregion

        #region MigrationTable
        IEnumerable<SelectItem<MigrationTableItemView>> GetDropDownItemMigrationTable(SearchParameter search);
        #endregion MigrationTable
        #region CreditAppReqAssignment
        IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItemCreditAppReqAssignment(SearchParameter search);
        #endregion CreditAppReqAssignment
        #region DocumentReturnTable
        IEnumerable<SelectItem<DocumentReturnTableItemView>> GetDropDownItemDocumentReturnTable(SearchParameter search);
        #endregion DocumentReturnTable
        #region DocumentReturnMethod
        IEnumerable<SelectItem<DocumentReturnMethodItemView>> GetDropDownItemDocumentReturnMethod(SearchParameter search);
        #endregion DocumentReturnMethod
        #region VendorPaymentTrans
        IEnumerable<SelectItem<VendorPaymentTransItemView>> GetDropDownItemVendorPaymentTrans(SearchParameter search);
        #endregion VendorPaymentTrans
        #region BusinessCollateralAgmLine
        IEnumerable<SelectItem<BusinessCollateralAgmLineItemView>> GetDropDownItemBusinessCollateralAgmLine(SearchParameter search);
        #endregion BusinessCollateralAgmLine
        #region ProcesTrans
        IEnumerable<SelectItem<ProcessTransItemView>> GetDropDownItemProcesTrans(SearchParameter search);
        #endregion ProcesTrans
        #region BusinessCollateralAgmTable
        IEnumerable<SelectItem<BusinessCollateralAgmTableItemView>> GetDropDownItemBusinessCollateralAgmTable(SearchParameter search);
        #endregion BusinessCollateralAgmTable
    }

    public class SysDropDownService : SmartAppService, ISysDropDownService
    {
        public SysDropDownService(SmartAppDbContext context) : base(context) { }
        public SysDropDownService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public SysDropDownService() : base() { }
        #region NumberSeqSegmentType
        public IEnumerable<SelectItem<NumberSeqSegmentItemView>> GetDropDownItemNumberSeqSegment(SearchParameter search)
        {
            try
            {
                INumberSeqSegmentRepo numberSeqSegmentRepo = new NumberSeqSegmentRepo(db);
                return numberSeqSegmentRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion NumberSeqSegmentType
        #region AddressCountry
        public IEnumerable<SelectItem<AddressCountryItemView>> GetDropDownItemAddressCountry(SearchParameter search)
        {
            try
            {
                IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
                return addressCountryRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AddressCountry
        #region AddressDistrict
        public IEnumerable<SelectItem<AddressDistrictItemView>> GetDropDownItemAddressDistrict(SearchParameter search)
        {
            try
            {
                IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
                return addressDistrictRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AddressDistrict
        #region AddressPostalCode
        public IEnumerable<SelectItem<AddressPostalCodeItemView>> GetDropDownItemAddressPostalCode(SearchParameter search)
        {
            try
            {
                IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
                return addressPostalCodeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AddressPostalCode
        #region AddressProvince
        public IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItemAddressProvince(SearchParameter search)
        {
            try
            {
                IAddressTransService addressTransService = new AddressTransService(db);
                return addressTransService.GetDropDownItemAddressProvince(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AddressProvince
        #region AddressSubDistrict
        public IEnumerable<SelectItem<AddressSubDistrictItemView>> GetDropDownItemAddressSubDistrict(SearchParameter search)
        {
            try
            {
                IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
                return addressSubDistrictRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AddressSubDistrict
        #region BusinessCollateralType
        public IEnumerable<SelectItem<BusinessCollateralTypeItemView>> GetDropDownItemBusinessCollateralType(SearchParameter search)
        {
            try
            {
                IBusinessCollateralTypeRepo businessCollateralTypeRepo = new BusinessCollateralTypeRepo(db);
                return businessCollateralTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessCollateralType
        #region BusinessCollateralAgmLine
        public IEnumerable<SelectItem<BusinessCollateralAgmLineItemView>> GetDropDownItemBusinessCollateralAgmLine(SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmLineRepo businessCollateralAgmLineRepo = new BusinessCollateralAgmLineRepo(db);
                search = search.GetParentCondition(BusinessCollateralAgmTableCondition.BusinessCollateralAgmTableGUID);
                return businessCollateralAgmLineRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessCollateralAgmLine
        #region BusinessCollateralAgmTable
        public IEnumerable<SelectItem<BusinessCollateralAgmTableItemView>> GetDropDownItemBusinessCollateralAgmTable(SearchParameter search)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                return businessCollateralAgmTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessCollateralAgmTable
        #region CustomerTable
        public IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemCustomerTable(SearchParameter search)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                return customerTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CustomerTableItemView>> GetDropDownItemCustomerTableBy(SearchParameter search)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                return customerTableRepo.GetDropDownItemBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustomerTable
        #region DocumentReason
        public IEnumerable<SelectItem<DocumentReasonItemView>> GetDropDownItemDocumentReason(SearchParameter search)
        {
            try
            {
                IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
                search = search.GetParentCondition(new string[] { DocumentReasonCondition.RefType });
                return documentReasonRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentReason
        #region Gender
        public IEnumerable<SelectItem<GenderItemView>> GetDropDownItemGender(SearchParameter search)
        {
            try
            {
                IGenderRepo genderRepo = new GenderRepo(db);
                return genderRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Gender
        #region MaritalStatus
        public IEnumerable<SelectItem<MaritalStatusItemView>> GetDropDownItemMaritalStatus(SearchParameter search)
        {
            try
            {
                IMaritalStatusRepo maritalStatusRepo = new MaritalStatusRepo(db);
                return maritalStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion MaritalStatus
        #region Nationality
        public IEnumerable<SelectItem<NationalityItemView>> GetDropDownItemNationality(SearchParameter search)
        {
            try
            {
                INationalityRepo nationalityRepo = new NationalityRepo(db);
                return nationalityRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Nationality
        #region NumberSeqTable
        public IEnumerable<SelectItem<NumberSeqTableItemView>> GetDropDownItemNumberSeqTable(SearchParameter search)
        {
            try
            {
                INumberSeqTableRepo NumberSeqTableRepo = new NumberSeqTableRepo(db);
                return NumberSeqTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion NumberSeqTable
        #region Occupation
        public IEnumerable<SelectItem<OccupationItemView>> GetDropDownItemOccupation(SearchParameter search)
        {
            try
            {
                IOccupationRepo occupationRepo = new OccupationRepo(db);
                return occupationRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Occupation
        #region Race
        public IEnumerable<SelectItem<RaceItemView>> GetDropDownItemRace(SearchParameter search)
        {
            try
            {
                IRaceRepo raceRepo = new RaceRepo(db);
                return raceRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Race
        #region BankGroup
        public IEnumerable<SelectItem<BankGroupItemView>> GetDropDownItemBankGroup(SearchParameter search)
        {
            try
            {
                IBankGroupRepo bankGroupRepo = new BankGroupRepo(db);
                return bankGroupRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BankGroup
        #region RegistrationType
        public IEnumerable<SelectItem<RegistrationTypeItemView>> GetDropDownItemRegistrationType(SearchParameter search)
        {
            try
            {
                IRegistrationTypeRepo registrationTypeRepo = new RegistrationTypeRepo(db);
                return registrationTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion RegistrationType
        #region BlacklistStatus
        public IEnumerable<SelectItem<BlacklistStatusItemView>> GetDropDownItemBlacklistStatus(SearchParameter search)
        {
            try
            {
                IBlacklistStatusRepo blacklistStatusRepo = new BlacklistStatusRepo(db);
                return blacklistStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BlacklistStatus
        #region BusinessSegment
        public IEnumerable<SelectItem<BusinessSegmentItemView>> GetDropDownItemBusinessSegment(SearchParameter search)
        {
            try
            {
                IBusinessSegmentRepo businessSegmentRepo = new BusinessSegmentRepo(db);
                return businessSegmentRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessSegment		
        #region BusinessSize
        public IEnumerable<SelectItem<BusinessSizeItemView>> GetDropDownItemBusinessSize(SearchParameter search)
        {
            try
            {
                IBusinessSizeRepo businessSizeRepo = new BusinessSizeRepo(db);
                return businessSizeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessSize
        #region BusinessType
        public IEnumerable<SelectItem<BusinessTypeItemView>> GetDropDownItemBusinessType(SearchParameter search)
        {
            try
            {
                IBusinessTypeRepo businessTypeRepo = new BusinessTypeRepo(db);
                return businessTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessType
        #region CompanyBank
        public IEnumerable<SelectItem<CompanyBankItemView>> GetDropDownItemCompanyBank(SearchParameter search)
        {
            try
            {
                ICompanyBankRepo companyBankRepo = new CompanyBankRepo(db);
                return companyBankRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CompanyBank
        #region CreditScoring
        public IEnumerable<SelectItem<CreditScoringItemView>> GetDropDownItemCreditScoring(SearchParameter search)
        {
            try
            {
                ICreditScoringRepo creditScoringRepo = new CreditScoringRepo(db);
                return creditScoringRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditScoring
        #region Currency
        public IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItemCurrency(SearchParameter search)
        {
            try
            {
                ICurrencyRepo currencyRepo = new CurrencyRepo(db);
                return currencyRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
          public IEnumerable<SelectItem<CurrencyItemView>> GetDropDownItemPlusExchangeRate(SearchParameter search)
        {
            try
            {
                ICurrencyRepo currencyRepo = new CurrencyRepo(db);
                return currencyRepo.GetDropDownItemPlusExchangeRate(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Currency
        #region GuarantorType
        public IEnumerable<SelectItem<GuarantorTypeItemView>> GetDropDownItemGuarantorType(SearchParameter search)
        {
            try
            {
                IGuarantorTypeRepo guarantorTypeRepo = new GuarantorTypeRepo(db);
                return guarantorTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GuarantorType
        #region KYC
        public IEnumerable<SelectItem<KYCSetupItemView>> GetDropDownItemKYCSetup(SearchParameter search)
        {
            try
            {
                IKYCSetupRepo kycSetupRepo = new KYCSetupRepo(db);
                return kycSetupRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion KYC
        #region LineOfBusiness
        public IEnumerable<SelectItem<LineOfBusinessItemView>> GetDropDownItemLineOfBusiness(SearchParameter search)
        {
            try
            {
                ILineOfBusinessRepo lineOfBusinessRepo = new LineOfBusinessRepo(db);
                return lineOfBusinessRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion LineOfBusiness
        #region Language
        public IEnumerable<SelectItem<LanguageItemView>> GetDropDownItemLanguage(SearchParameter search)
        {
            try
            {
                ILanguageRepo languageRepo = new LanguageRepo(db);
                return languageRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Language
        #region VendGroup
        public IEnumerable<SelectItem<VendGroupItemView>> GetDropDownItemVendGroup(SearchParameter search)
        {
            try
            {
                IVendGroupRepo vendGroupRepo = new VendGroupRepo(db);
                return vendGroupRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VendGroup
        #region GradeClassification
        public IEnumerable<SelectItem<GradeClassificationItemView>> GetDropDownItemGradeClassification(SearchParameter search)
        {
            try
            {
                IGradeClassificationRepo gradeClassificationRepo = new GradeClassificationRepo(db);
                return gradeClassificationRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GradeClassification
        #region CustGroup
        public IEnumerable<SelectItem<CustGroupItemView>> GetDropDownItemCustGroup(SearchParameter search)
        {
            try
            {
                ICustGroupRepo custGroupRepo = new CustGroupRepo(db);
                return custGroupRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustGroup
        #region EmployeeTable
        public IEnumerable<SelectItem<EmployeeTableItemView>> GetDropDownItemEmployeeTable(SearchParameter search)
        {
            try
            {
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                return employeeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion EmployeeTable
        #region IntroducedBy
        public IEnumerable<SelectItem<IntroducedByItemView>> GetDropDownItemIntroducedBy(SearchParameter search)
        {
            try
            {
                IIntroducedByRepo introducedByRepo = new IntroducedByRepo(db);
                return introducedByRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion IntroducedBy
        #region LedgerDimension
        public IEnumerable<SelectItem<LedgerDimensionItemView>> GetDropDownItemLedgerDimension(SearchParameter search)
        {
            try
            {
                ILedgerDimensionRepo ledgerDimensionRepo = new LedgerDimensionRepo(db);
                return ledgerDimensionRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion LedgerDimension
        #region MethodOfPayment
        public IEnumerable<SelectItem<MethodOfPaymentItemView>> GetDropDownItemMethodOfPayment(SearchParameter search)
        {
            try
            {
                IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
                return methodOfPaymentRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion MethodOfPayment
        #region NCBAccountStatus
        public IEnumerable<SelectItem<NCBAccountStatusItemView>> GetDropDownItemNCBAccountStatus(SearchParameter search)
        {
            try
            {
                INCBAccountStatusRepo ncbAccountStatusRepo = new NCBAccountStatusRepo(db);
                return ncbAccountStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion NCBAccountStatus
        #region ParentCompany
        public IEnumerable<SelectItem<ParentCompanyItemView>> GetDropDownItemParentCompany(SearchParameter search)
        {
            try
            {
                IParentCompanyRepo parentCompanyRepo = new ParentCompanyRepo(db);
                return parentCompanyRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ParentCompany
        #region Territory
        public IEnumerable<SelectItem<TerritoryItemView>> GetDropDownItemTerritory(SearchParameter search)
        {
            try
            {
                ITerritoryRepo territoryRepo = new TerritoryRepo(db);
                return territoryRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Territory
        #region VendorTable
        public IEnumerable<SelectItem<VendorTableItemView>> GetDropDownItemVendorTable(SearchParameter search)
        {
            try
            {
                IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
                return vendorTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VendorTable
        #region WithholdingTaxGroup
        public IEnumerable<SelectItem<WithholdingTaxGroupItemView>> GetDropDownItemWithholdingTaxGroup(SearchParameter search)
        {
            try
            {
                IWithholdingTaxGroupRepo withholdingTaxGroupRepo = new WithholdingTaxGroupRepo(db);
                return withholdingTaxGroupRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion WithholdingTaxGroup
        #region InterestType
        public IEnumerable<SelectItem<InterestTypeItemView>> GetDropDownItemInterestType(SearchParameter search)
        {
            try
            {
                IInterestTypeRepo interestTypeRepo = new InterestTypeRepo(db);
                return interestTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InterestType
        #region RelatedPersonTable
        public IEnumerable<SelectItem<RelatedPersonTableItemView>> GetDropDownItemRelatedPersonTable(SearchParameter search)
        {
            try
            {
                IRelatedPersonTableRepo RelatedPersonTableRepo = new RelatedPersonTableRepo(db);
                return RelatedPersonTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion RelatedPersonTable
        #region BuyerTable
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemBuyerTable(SearchParameter search)
        {
            try
            {
                IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
                return buyerTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerTableItemView>> GetDropDownItemBuyerTableBy(SearchParameter search)
        {
            try
            {
                IBuyerTableService buyerTableService = new BuyerTableService(db);
                return buyerTableService.GetDropDownItemByCreditAppTable(search);
                //ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                //return customerTableRepo.GetDropDownItemBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerTable
        #region BuyerInvoiceTable
        public IEnumerable<SelectItem<BuyerInvoiceTableItemView>> GetDropDownItemBuyerInvoiceTable(SearchParameter search)
        {
            try
            {
                IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
                return buyerInvoiceTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerInvoiceTable
        #region TaxTable
        public IEnumerable<SelectItem<TaxTableItemView>> GetDropDownItemTaxTable(SearchParameter search)
        {
            try
            {
                ITaxTableRepo TaxTableRepo = new TaxTableRepo(db);
                return TaxTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion TaxTable
        #region TaxValue
        public IEnumerable<SelectItem<TaxValueItemView>> GetDropDownItemTaxValue(SearchParameter search)
        {
            try
            {
                ITaxValueRepo TaxValueRepo = new TaxValueRepo(db);
                return TaxValueRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion TaxValue
        #region AuthorizedPersonType
        public IEnumerable<SelectItem<AuthorizedPersonTypeItemView>> GetDropDownItemAuthorizedPersonType(SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTypeRepo authorizedPersonTypeRepo = new AuthorizedPersonTypeRepo(db);
                return authorizedPersonTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonType
        #region BuyerAgreementTable
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTable(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                return buyerAgreementTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgreementTableByBuyerAgreementTrans(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                return buyerAgreementTableRepo.GetDropDownItemByBuyerAgreementTrans(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementTable
        #region ExposureGroup
        public IEnumerable<SelectItem<ExposureGroupItemView>> GetDropDownItemExposureGroup(SearchParameter search)
        {
            try
            {
                IExposureGroupRepo exposureGroupRepo = new ExposureGroupRepo(db);
                return exposureGroupRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ExposureGroup
        #region Ownership
        public IEnumerable<SelectItem<OwnershipItemView>> GetDropDownItemOwnership(SearchParameter search)
        {
            try
            {
                IOwnershipRepo OwnershipRepo = new OwnershipRepo(db);
                return OwnershipRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Ownership
        #region PropertyType
        public IEnumerable<SelectItem<PropertyTypeItemView>> GetDropDownItemPropertyType(SearchParameter search)
        {
            try
            {
                IPropertyTypeRepo PropertyTypeRepo = new PropertyTypeRepo(db);
                return PropertyTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion PropertyType
        #region EmplTeam
        public IEnumerable<SelectItem<EmplTeamItemView>> GetDropDownItemEmplTeam(SearchParameter search)
        {
            try
            {
                IEmplTeamRepo EmplTeamRepo = new EmplTeamRepo(db);
                return EmplTeamRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion EmplTeam
        #region DocumentConditionTemplateTable
        public IEnumerable<SelectItem<DocumentConditionTemplateTableItemView>> GetDropDownItemDocumentConditionTemplateTable(SearchParameter search)
        {
            try
            {
                IDocumentConditionTemplateTableRepo documentConditionTemplateTableRepo = new DocumentConditionTemplateTableRepo(db);
                return documentConditionTemplateTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentConditionTemplateTable
        #region Department
        public IEnumerable<SelectItem<DepartmentItemView>> GetDropDownItemDepartment(SearchParameter search)
        {
            try
            {
                IDepartmentRepo DepartmentRepo = new DepartmentRepo(db);
                return DepartmentRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Department
        #region DocumentType
        public IEnumerable<SelectItem<DocumentTypeItemView>> GetDropDownItemDocumentType(SearchParameter search)
        {
            try
            {
                IDocumentTypeRepo documentTypeRepo = new DocumentTypeRepo(db);
                return documentTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region AddressTrans
        public IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTrans(SearchParameter search)
        {
            try
            {
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                return addressTransRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AddressTrans
        #region ApplicationTable
        //public IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItemApplicationTable(SearchParameter search)
        //{
        //	try
        //	{
        //		IApplicationTableRepo applicationTableRepo = new ApplicationTableRepo(db);
        //		return applicationTableRepo.GetDropDownItem(search);
        //	}
        //	catch (Exception e)
        //	{
        //		throw SmartAppUtil.AddStackTrace(e);
        //	}
        //}
        #endregion ApplicationTable
        #region AssignmentAgreementTable
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementTable(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                return assignmentAgreementTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AssignmentAgreementTable
        #region AssignmentMethod
        public IEnumerable<SelectItem<AssignmentMethodItemView>> GetDropDownItemAssignmentMethod(SearchParameter search)
        {
            try
            {
                IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
                return assignmentMethodRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AssignmentMethod
        #region BillingResponsibleBy
        public IEnumerable<SelectItem<BillingResponsibleByItemView>> GetDropDownItemBillingResponsibleBy(SearchParameter search)
        {
            try
            {
                IBillingResponsibleByRepo billingResponsibleByRepo = new BillingResponsibleByRepo(db);
                return billingResponsibleByRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BillingResponsibleBy
        #region ConsortiumTable
        public IEnumerable<SelectItem<ConsortiumTableItemView>> GetDropDownItemConsortiumTable(SearchParameter search)
        {
            try
            {
                IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
                return consortiumTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ConsortiumTable
        #region ContactPersonTrans
        public IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemContactPersonTrans(SearchParameter search)
        {
            try
            {
                IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
                return contactPersonTransRepo.GetDropDownItemJoinRelatedPerson(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ContactPersonTrans
        #region CreditAppRequestTable
        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTable(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppRequestTable
        #region CreditAppTable
        public IEnumerable<SelectItem<CreditAppTableItemView>> GetDropDownItemCreditAppTable(SearchParameter search)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                return creditAppTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppTable
        #region CreditLimitType
        public IEnumerable<SelectItem<CreditLimitTypeItemView>> GetDropDownItemCreditLimitType(SearchParameter search)
        {
            try
            {
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                return creditLimitTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditLimitType
        #region CreditTerm
        public IEnumerable<SelectItem<CreditTermItemView>> GetDropDownItemCreditTerm(SearchParameter search)
        {
            try
            {
                ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
                return creditTermRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditTerm
        #region CustBank
        public IEnumerable<SelectItem<CustBankItemView>> GetDropDownItemCustBank(SearchParameter search)
        {
            try
            {
                ICustBankRepo custBankRepo = new CustBankRepo(db);
                return custBankRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustBank
        #region ProductSubType
        public IEnumerable<SelectItem<ProductSubTypeItemView>> GetDropDownItemProductSubType(SearchParameter search)
        {
            try
            {
                IProductSubTypeRepo productSubTypeRepo = new ProductSubTypeRepo(db);
                return productSubTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ProductSubType
        #region WithholdingTaxValue
        public IEnumerable<SelectItem<WithholdingTaxValueItemView>> GetDropDownItemWithholdingTaxValue(SearchParameter search)
        {
            try
            {
                IWithholdingTaxValueRepo WithholdingTaxValueRepo = new WithholdingTaxValueRepo(db);
                return WithholdingTaxValueRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion WithholdingTaxValue
        #region WithholdingTaxTable
        public IEnumerable<SelectItem<WithholdingTaxTableItemView>> GetDropDownItemWithholdingTaxTable(SearchParameter search)
        {
            try
            {
                IWithholdingTaxTableRepo WithholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                return WithholdingTaxTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion WithholdingTaxTable
        #region InvoiceRevenueType
        public IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetDropDownItemInvoiceRevenueType(SearchParameter search)
        {
            try
            {
                IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
                return invoiceRevenueTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceRevenueType
        #region IntercompanyTable
        public IEnumerable<SelectItem<IntercompanyItemView>> GetDropDownItemIntercompanyTable(SearchParameter search)
        {
            try
            {
                IIntercompanyRepo intercompanyTableRepo = new IntercompanyRepo(db);
                return intercompanyTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion IntercompanyTable
        #region Branch
        public IEnumerable<SelectItem<BranchItemView>> GetDropDownItemBranch(SearchParameter search)
        {
            try
            {
                IBranchRepo BranchRepo = new BranchRepo(db);
                return BranchRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Branch
        #region BankType
        public IEnumerable<SelectItem<BankTypeItemView>> GetDropDownItemBankType(SearchParameter search)
        {
            try
            {
                IBankTypeRepo BankTypeRepo = new BankTypeRepo(db);
                return BankTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BankType
        #region BusiniessUnit
        public IEnumerable<SelectItem<BusinessUnitItemView>> GetDropDownItemBusinessUnit(SearchParameter search)
        {
            try
            {
                IBusinessUnitRepo businessUnitRepo = new BusinessUnitRepo(db);
                return businessUnitRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusiniessUnit
        #region ServiceFeeCondTemplateTable
        public IEnumerable<SelectItem<ServiceFeeCondTemplateTableItemView>> GetDropDownItemServiceFeeCondTemplateTable(SearchParameter search)
        {
            try
            {
                IServiceFeeCondTemplateTableRepo serviceFeeCondTemplateTableRepo = new ServiceFeeCondTemplateTableRepo(db);
                return serviceFeeCondTemplateTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ServiceFeeCondTemplateTable
        #region CreditType
        public IEnumerable<SelectItem<CreditTypeItemView>> GetDropDownItemCreditType(SearchParameter search)
        {
            try
            {
                ICreditTypeRepo creditTypeRepo = new CreditTypeRepo(db);
                return creditTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditType
        #region CalendarGroup
        public IEnumerable<SelectItem<CalendarGroupItemView>> GetDropDownItemCalendarGroup(SearchParameter search)
        {
            try
            {
                ICalendarGroupRepo calendarGroupRepo = new CalendarGroupRepo(db);
                return calendarGroupRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CalendarGroup
        #region ApplicationTable
        public IEnumerable<SelectItem<ApplicationTableItemView>> GetDropDownItemApplicationTable(SearchParameter search)
        {
            try
            {
                IApplicationTableRepo applicationTableRepo = new ApplicationTableRepo(db);
                return applicationTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ApplicationTable
        #region DocumentConditionTemplateTable
        public IEnumerable<SelectItem<DocumentTemplateTableItemView>> GetDropDownItemDocumentTemplateTable(SearchParameter search)
        {
            try
            {
                IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
                return documentTemplateTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion DocumentConditionTemplateTable
        #region BuyerAgreementLine
        public IEnumerable<SelectItem<BuyerAgreementLineItemView>> GetDropDownItemBuyerAgreementLine(SearchParameter search)
        {
            try
            {
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                return buyerAgreementLineRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementLine
        #region InvoiceType
        public IEnumerable<SelectItem<InvoiceTypeItemView>> GetDropDownItemInvoiceType(SearchParameter search)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                return invoiceTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementLine
        #region BusinessCollateralStatus
        public IEnumerable<SelectItem<BusinessCollateralStatusItemView>> GetDropDownItemBusinessCollateralStatus(SearchParameter search)
        {
            try
            {
                IBusinessCollateralStatusRepo businessCollateralStatusRepo = new BusinessCollateralStatusRepo(db);
                return businessCollateralStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessCollateralStatus
        #region BusinessCollateralSubType
        public IEnumerable<SelectItem<BusinessCollateralSubTypeItemView>> GetDropDownItemBusinessCollateralSubType(SearchParameter search)
        {
            try
            {
                IBusinessCollateralSubTypeRepo businessCollateralSubTypeRepo = new BusinessCollateralSubTypeRepo(db);
                return businessCollateralSubTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BusinessCollateralSubType
        #region WithdrawalTable
        public IEnumerable<SelectItem<WithdrawalTableItemView>> GetDropDownItemWithdrawalTable(SearchParameter search)
        {
            try
            {
                IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                return withdrawalTableRepo.GetDropDownItem(search);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region BookmarkDocument
        public IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocument(SearchParameter search)
        {
            try
            {
                IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
                return bookmarkDocumentRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BookmarkDocument
        #region BookmarkDocumentTemplateTable
        public IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search)
        {
            try
            {
                IBookmarkDocumentTemplateTableRepo bookmarkDocumentTemplateTableRepo = new BookmarkDocumentTemplateTableRepo(db);
                return bookmarkDocumentTemplateTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BookmarkDocumentTemplateTable
        #region LedgerFiscalYear
        public IEnumerable<SelectItem<LedgerFiscalYearItemView>> GetDropDownItemLedgerFiscalYear(SearchParameter search)
        {
            try
            {
                ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
                return ledgerFiscalYearRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion LedgerFiscalYear
        #region CustBusinessCollateral
        public IEnumerable<SelectItem<CustBusinessCollateralItemView>> GetDropDownItemCustBusinessCollateral(SearchParameter search)
        {
            try
            {
                ICustBusinessCollateralRepo custBusinessCollateralRepo = new CustBusinessCollateralRepo(db);
                return custBusinessCollateralRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CustBusinessCollateral
        #region AuthorizedPersonTrans
        public IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTrans(SearchParameter search)
        {
            try
            {
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                return authorizedPersonTransRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AuthorizedPersonTrans
        #region CompanySignature
        public IEnumerable<SelectItem<CompanySignatureItemView>> GetDropDownItemCompanySignature(SearchParameter search)
        {
            try
            {
                ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
                return companySignatureRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CompanySignature
        #region PurchaseLine
        public IEnumerable<SelectItem<PurchaseLineItemView>> GetDropDownItemPurchaseLine(SearchParameter search)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion PurchaseLine
        #region PurchaseTable
        public IEnumerable<SelectItem<PurchaseTableItemView>> GetDropDownItemPurchaseTable(SearchParameter search)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                return purchaseTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion PurchaseTable
        #region ConsortiumLine
        public IEnumerable<SelectItem<ConsortiumLineItemView>> GetDropDownItemConsortiumLine(SearchParameter search)
        {
            try
            {
                IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
                return consortiumLineRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ConsortiumLine

        #region Company
        public IEnumerable<SelectItem<CompanyItemView>> GetDropDownItemCompany(SearchParameter search)
        {
            try
            {
                ICompanyRepo companyRepo = new CompanyRepo(db);
                return companyRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Company
        #region CreditAppReqBusinessCollateral
        public IEnumerable<SelectItem<CreditAppReqBusinessCollateralItemView>> GetDropDownItemCreditAppReqBusinessCollateral(SearchParameter search)
        {
            try
            {
                ICreditAppReqBusinessCollateralRepo creditAppReqBusinessCollateralRepo = new CreditAppReqBusinessCollateralRepo(db);
                return creditAppReqBusinessCollateralRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppReqBusinessCollacteral
        #region VerificationType
        public IEnumerable<SelectItem<VerificationTypeItemView>> GetDropDownItemVerificationType(SearchParameter search)
        {
            try
            {
                IVerificationTypeRepo verificationTypeRepo = new VerificationTypeRepo(db);
                return verificationTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region JobType
        public IEnumerable<SelectItem<JobTypeItemView>> GetDropDownItemJobType(SearchParameter search)
        {
            try
            {
                IJobTypeRepo jobTypeRepo = new JobTypeRepo(db);
                return jobTypeRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion JobType
        #region InvoiceTable
        public IEnumerable<SelectItem<InvoiceTableItemView>> GetDropDownItemInvoiceTable(SearchParameter search)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                return invoiceTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceTable
        #region ProdUnitTable
        public IEnumerable<SelectItem<ProdUnitItemView>> GetDropDownItemProdUnitTable(SearchParameter search)
        {
            try
            {
                IProdUnitRepo prodUnitRepo = new ProdUnitRepo(db);
                return prodUnitRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ProdUnitTable        

        #region AssignmentAgreementSettle
        public IEnumerable<SelectItem<AssignmentAgreementSettleItemView>> GetDropDownItemAssignmentAgreementSettle(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
                return assignmentAgreementSettleRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion AssignmentAgreementSettle
        #region TaxInvoiceTable
        public IEnumerable<SelectItem<TaxInvoiceTableItemView>> GetDropDownItemTaxInvoiceTable(SearchParameter search)
        {
            try
            {
                ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
                return taxInvoiceTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Company

        #region ChequeTable
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTable(SearchParameter search)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                return chequeTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemChequeTableBy(SearchParameter search)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                return chequeTableRepo.GetDropDownItemBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<ChequeTableItemView>> GetDropDownItemWithDocumentStatusChequeTable(SearchParameter search, string statusCode)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);
                search = search.GetParentCondition(new string[] { ChequeCondition.CustomerGUID, ChequeCondition.BuyerGUID, ChequeCondition.RefGUID });
                List<SearchCondition> tempConditions = search.Conditions;
                search.Conditions = new SearchCondition[] { tempConditions[0] }.ToList();
                return chequeTableRepo.GetDropDownByStatusItem(search, statusCode);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ChequeTable

        #region DocumentStatus
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemDocumentStatus(SearchParameter search)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentStatus

        #region VerificationTable
        public IEnumerable<SelectItem<VerificationTableItemView>> GetDropDownItemVerificationTable(SearchParameter search)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                return verificationTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion VerificationTable
        #region GetVerificationTableDropDown
        public IEnumerable<SelectItem<VerificationTableItemView>> GetVerificationTableDropDown(SearchParameter search)
        {
            try
            {
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                return verificationTableRepo.GetDropDownItemBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GetVerificationTableDropDown

        #region GetFreeTextInvoiceTableDropDown
        public IEnumerable<SelectItem<FreeTextInvoiceTableItemView>> GetFreeTextInvoiceTableDropDown(SearchParameter search)
        {
            try
            {
                IFreeTextInvoiceTableRepo freeTextInvoiceTableRepo = new FreeTextInvoiceTableRepo(db);
                return freeTextInvoiceTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GetFreeTextInvoiceTableDropDown
        #region GuarantorTrans
        public IEnumerable<SelectItem<GuarantorTransItemView>> GetDropDownItemGuarantorTrans(SearchParameter search)
        {
            try
            {
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                return guarantorTransRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        #endregion GuarantorTrans

        #region BuyerAgreementTrans
        public IEnumerable<SelectItem<BuyerAgreementTransItemView>> GetDropDownItemBuyerAgreementTrans(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                return buyerAgreementTransRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region
        public IEnumerable<SelectItem<MessengerJobTableItemView>> GetDropDownItemMessengerJobTable(SearchParameter search)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                return messengerJobTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerAgreementTrans
        #endregion
        #region CreditAppLine
        public IEnumerable<SelectItem<CreditAppLineItemView>> GetDropDownItemCreditAppLine(SearchParameter search)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                return creditAppLineRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppLines

        #region ReceiptTempTable
        public IEnumerable<SelectItem<ReceiptTempTableItemView>> GetDropDownItemReceiptTempTable(SearchParameter search)
        {
            try
            {
                IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                return receiptTempTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppLine
        #region MessengerTable
        public IEnumerable<SelectItem<MessengerTableItemView>> GetDropDownItemMessengerTable(SearchParameter search)
        {
            try
            {
                IMessengerTableRepo messengerTableRepo = new MessengerTableRepo(db);
                return messengerTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion MessengerTable
        #region ContactPerson

        public IEnumerable<SelectItem<ContactPersonTransItemView>> GetContactPersonDropDown(SearchParameter search)
        {
            try
            {
                IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
                search = search.GetParentCondition(ContactPersonCondition.RefGUID);

                return contactPersonTransRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ContactPerson

        #region ContactPerson

        public IEnumerable<SelectItem<CollectionFollowUpItemView>> GetDropDownItemCollectionFollowUp(SearchParameter search)
        {
            try
            {
                ICollectionFollowUpRepo collectionFollowUpRepo = new CollectionFollowUpRepo(db);
                return collectionFollowUpRepo.GetDropDownItemBy(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion ContactPerson
        #region BuyerReceiptTable
        public IEnumerable<SelectItem<BuyerReceiptTableItemView>> GetDropDownItemBuyerReceiptTable(SearchParameter search)
        {
            try
            {
                IBuyerReceiptTableRepo buyerReceiptTableRepo = new BuyerReceiptTableRepo(db);
                return buyerReceiptTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion BuyerReceiptTables
        #region InvoiceSettlementDetail
        public IEnumerable<SelectItem<InvoiceSettlementDetailItemView>> GetDropDownItemInvoiceSettlementDetail(SearchParameter search)
        {
            try
            {
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                return invoiceSettlementDetailRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InvoiceSettlementDetail
        #region MigrationTable
        public IEnumerable<SelectItem<MigrationTableItemView>> GetDropDownItemMigrationTable(SearchParameter search)
        {
            try
            {
                IMigrationTableRepo migrationTableRepo = new MigrationTableRepo(db);
                return migrationTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion MigrationTable

        #region CreditAppReqAssignment
        public IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItemCreditAppReqAssignment(SearchParameter search)
        {
            try
            {
                ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
                return creditAppReqAssignmentRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion CreditAppReqAssignment
        #region DocumentReturnTable
        public IEnumerable<SelectItem<DocumentReturnTableItemView>> GetDropDownItemDocumentReturnTable(SearchParameter search)
        {
            try
            {
                IDocumentReturnTableRepo documentReturnTableRepo = new DocumentReturnTableRepo(db);
                return documentReturnTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentReturnTable
        #region DocumentReturnMethod
        public IEnumerable<SelectItem<DocumentReturnMethodItemView>> GetDropDownItemDocumentReturnMethod(SearchParameter search)
        {
            try
            {
                IDocumentReturnMethodRepo documentReturnMethodRepo = new DocumentReturnMethodRepo(db);
                return documentReturnMethodRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentReturnMethod
        #region DocumentReturnMethod
        public IEnumerable<SelectItem<VendorPaymentTransItemView>> GetDropDownItemVendorPaymentTrans(SearchParameter search)
        {
            try
            {
                IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
                return vendorPaymentTransRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentReturnMethod
        
        #region DocumentReturnMethod
        public IEnumerable<SelectItem<ProcessTransItemView>> GetDropDownItemProcesTrans(SearchParameter search)
        {
            try
            {
                IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
                return processTransRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion DocumentReturnMethod
    }

}