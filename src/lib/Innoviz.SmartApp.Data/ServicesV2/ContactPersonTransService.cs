using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IContactPersonTransService
	{

		ContactPersonTransItemView GetContactPersonTransById(string id);
		ContactPersonTransItemView CreateContactPersonTrans(ContactPersonTransItemView contactPersonTransView);
		ContactPersonTransItemView UpdateContactPersonTrans(ContactPersonTransItemView contactPersonTransView);
		bool DeleteContactPersonTrans(string id);
		ContactPersonTransItemView GetContactPersonTransInitialData(string refId, string refGUID, RefType refType);
		IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemContactPersonTransItemByCustomer(SearchParameter search);
		IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemContactPersonTransItemByBuyer(SearchParameter search);
		void CreateContactPersonTransCollection(List<ContactPersonTransItemView> contactPersonTransItemViews);
	}
	public class ContactPersonTransService : SmartAppService, IContactPersonTransService
	{
		public ContactPersonTransService(SmartAppDbContext context) : base(context) { }
		public ContactPersonTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ContactPersonTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ContactPersonTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ContactPersonTransService() : base() { }

		public ContactPersonTransItemView GetContactPersonTransById(string id)
		{
			try
			{
				IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
				return contactPersonTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactPersonTransItemView CreateContactPersonTrans(ContactPersonTransItemView contactPersonTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				contactPersonTransView = accessLevelService.AssignOwnerBU(contactPersonTransView);
				IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
				ContactPersonTrans contactPersonTrans = contactPersonTransView.ToContactPersonTrans();
				contactPersonTrans = contactPersonTransRepo.CreateContactPersonTrans(contactPersonTrans);
				base.LogTransactionCreate<ContactPersonTrans>(contactPersonTrans);
				UnitOfWork.Commit();
				return contactPersonTrans.ToContactPersonTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactPersonTransItemView UpdateContactPersonTrans(ContactPersonTransItemView contactPersonTransView)
		{
			try
			{
				IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
				ContactPersonTrans inputContactPersonTrans = contactPersonTransView.ToContactPersonTrans();
				ContactPersonTrans dbContactPersonTrans = contactPersonTransRepo.Find(inputContactPersonTrans.ContactPersonTransGUID);
				dbContactPersonTrans = contactPersonTransRepo.UpdateContactPersonTrans(dbContactPersonTrans, inputContactPersonTrans);
				base.LogTransactionUpdate<ContactPersonTrans>(GetOriginalValues<ContactPersonTrans>(dbContactPersonTrans), dbContactPersonTrans);
				UnitOfWork.Commit();
				return dbContactPersonTrans.ToContactPersonTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteContactPersonTrans(string item)
		{
			try
			{
				IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
				Guid contactPersonTransGUID = new Guid(item);
				ContactPersonTrans contactPersonTrans = contactPersonTransRepo.Find(contactPersonTransGUID);
				contactPersonTransRepo.Remove(contactPersonTrans);
				base.LogTransactionDelete<ContactPersonTrans>(contactPersonTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactPersonTransItemView GetContactPersonTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new ContactPersonTransItemView
				{
					ContactPersonTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropdownByCondition
		public IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemContactPersonTransItemByCustomer(SearchParameter search)
		{
			try
			{
				IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
				search = search.GetParentCondition(ContactPersonTransCondition.RefGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetContactPersonTransItemByRefTypeCondition(false, (int)RefType.Customer);
				search.Conditions.AddRange(searchConditions);
				return contactPersonTransRepo.GetDropDownItemByActive(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<ContactPersonTransItemView>> GetDropDownItemContactPersonTransItemByBuyer(SearchParameter search)
		{
			try
			{
				IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
				search = search.GetParentCondition(ContactPersonTransCondition.RefGUID);
				List<SearchCondition> searchConditions = SearchConditionService.GetContactPersonTransItemByRefTypeCondition(false, (int)RefType.Buyer);
				search.Conditions.AddRange(searchConditions);
				return contactPersonTransRepo.GetDropDownItemByActive(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropdownByCondition
		#region migration
		public void CreateContactPersonTransCollection(List<ContactPersonTransItemView> contactPersonTransItemViews)
		{
			try
			{
				if (contactPersonTransItemViews.Any())
				{
					IContactPersonTransRepo contactPersonTransRepo = new ContactPersonTransRepo(db);
					List<ContactPersonTrans> contactPersonTrans = contactPersonTransItemViews.ToContactPersonTrans().ToList();
					contactPersonTransRepo.ValidateAdd(contactPersonTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(contactPersonTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion
	}
}
