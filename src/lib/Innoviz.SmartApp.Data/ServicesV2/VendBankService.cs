using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IVendBankService
	{

		VendBankItemView GetVendBankById(string id);
		VendBankItemView CreateVendBank(VendBankItemView vendBankView);
		VendBankItemView UpdateVendBank(VendBankItemView vendBankView);
		bool DeleteVendBank(string id);
		void CreateVendBankCollection(List<VendBankItemView> vendBankItemViews);
	}
	public class VendBankService : SmartAppService, IVendBankService
	{
		public VendBankService(SmartAppDbContext context) : base(context) { }
		public VendBankService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public VendBankService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public VendBankService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public VendBankService() : base() { }

		public VendBankItemView GetVendBankById(string id)
		{
			try
			{
				IVendBankRepo vendBankRepo = new VendBankRepo(db);
				return vendBankRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendBankItemView CreateVendBank(VendBankItemView vendBankView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				vendBankView = accessLevelService.AssignOwnerBU(vendBankView);
				IVendBankRepo vendBankRepo = new VendBankRepo(db);
				VendBank vendBank = vendBankView.ToVendBank();
				vendBank = vendBankRepo.CreateVendBank(vendBank);
				base.LogTransactionCreate<VendBank>(vendBank);
				UnitOfWork.Commit();
				return vendBank.ToVendBankItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public VendBankItemView UpdateVendBank(VendBankItemView vendBankView)
		{
			try
			{
				IVendBankRepo vendBankRepo = new VendBankRepo(db);
				VendBank inputVendBank = vendBankView.ToVendBank();
				VendBank dbVendBank = vendBankRepo.Find(inputVendBank.VendBankGUID);
				dbVendBank = vendBankRepo.UpdateVendBank(dbVendBank, inputVendBank);
				base.LogTransactionUpdate<VendBank>(GetOriginalValues<VendBank>(dbVendBank), dbVendBank);
				UnitOfWork.Commit();
				return dbVendBank.ToVendBankItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteVendBank(string item)
		{
			try
			{
				IVendBankRepo vendBankRepo = new VendBankRepo(db);
				Guid vendBankGUID = new Guid(item);
				VendBank vendBank = vendBankRepo.Find(vendBankGUID);
				vendBankRepo.Remove(vendBank);
				base.LogTransactionDelete<VendBank>(vendBank);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region migration
		public void CreateVendBankCollection(List<VendBankItemView> vendBankItemViews)
		{
			try
			{
				if (vendBankItemViews.Any())
				{
					IVendBankRepo vendBankRepo = new VendBankRepo(db);
					List<VendBank> vendBank = vendBankItemViews.ToVendBank().ToList();
					vendBankRepo.ValidateAdd(vendBank);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(vendBank, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion

	}
}
