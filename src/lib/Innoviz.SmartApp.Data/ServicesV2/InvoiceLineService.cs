﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IInvoiceLineService
    {
        InvoiceLineAmountResultView CalcInvoiceLineFromInvoiceRevenueType(InvoiceLineAmountParamView parm);
    }
    public class InvoiceLineService : SmartAppService, IInvoiceLineService
    {
        public InvoiceLineService(SmartAppDbContext context) : base(context) { }
        public InvoiceLineService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public InvoiceLineService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public InvoiceLineService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public InvoiceLineService() : base() { }

        public InvoiceLineAmountResultView CalcInvoiceLineFromInvoiceRevenueType(InvoiceLineAmountParamView parm)
        {
            try
            {
                InvoiceLineAmountResultView result = new InvoiceLineAmountResultView();
                
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                ITaxTableService taxTableService = new TaxTableService(db);
                IWithholdingTaxTableService whtService = new WithholdingTaxTableService(db);
                IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
                
                InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByIdNoTracking(parm.InvoiceRevenueTypeGUID);
                decimal taxValue = 0.0m, taxAmount = 0.0m, invoiceAmount = 0.0m, totalAmountBeforeTax = 0.0m, whtValue = 0.0m, whtBaseAmount = 0.0m, whtAmount = 0.0m;
                
                #region validation
                if (invoiceRevenueType == null)
                {
                    SmartAppException ex = new SmartAppException("ERROR.ERROR");
                    ex.AddData("ERROR.90037", "LABEL.SERVICE_FEE_TYPE_ID");
                    throw ex;
                }
                #endregion

                #region 1. check fee tax
                if(invoiceRevenueType.FeeTaxGUID != null)
                {
                    taxValue = taxTableService.GetTaxValue(invoiceRevenueType.FeeTaxGUID.Value, parm.IssuedDate);
                    taxAmount = serviceFeeTransService.CalcTaxAmount(parm.InvoiceAmount, taxValue, parm.IncludeTax);
                }
                #endregion
                #region 2. calculate AmountBeforeTax, InvoiceAmount
                if (parm.IncludeTax)
                {
                    invoiceAmount = parm.InvoiceAmount;
                    totalAmountBeforeTax = invoiceAmount - taxAmount;
                }
                else
                {
                    totalAmountBeforeTax = parm.InvoiceAmount;
                    invoiceAmount = totalAmountBeforeTax + taxAmount;
                }
                #endregion

                #region 3. calculate WHT
                if(invoiceRevenueType.FeeWHTGUID != null)
                {
                    whtValue = whtService.GetWHTValue(invoiceRevenueType.FeeWHTGUID.Value, parm.IssuedDate);
                    whtBaseAmount = totalAmountBeforeTax;
                    whtAmount = serviceFeeTransService.CalcWHTAmount(totalAmountBeforeTax, whtValue);
                }
                #endregion

                #region 4. assign values
                result.Qty = totalAmountBeforeTax < 0 ? -1 : 1;
                result.UnitPrice = Math.Abs(totalAmountBeforeTax);
                result.TotalAmountBeforeTax = totalAmountBeforeTax;
                result.TaxTableGUID = invoiceRevenueType.FeeTaxGUID;
                result.TaxAmount = taxAmount;
                result.TotalAmount = invoiceAmount;
                result.WithholdingTaxTableGUID = invoiceRevenueType.FeeWHTGUID;
                result.WHTBaseAmount = whtBaseAmount;
                result.WHTAmount = whtAmount;
                #endregion

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

    }
}
