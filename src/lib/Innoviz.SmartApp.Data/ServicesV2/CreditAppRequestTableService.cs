﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Core.Service.ConditionService;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ICreditAppRequestTableService
    {

        IEnumerable<SelectItem<CreditAppRequestTableItemView>> CreditAppRequestDropdownByDocumentStatus(SearchParameter search);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemCreditAppRequestStatus(SearchParameter search);

        CreditAppRequestLineItemView GetCreditAppRequestLineById(string id);
        CreditAppRequestTableItemView GetByIdvwByCreditAppRequestLine(string id);
        CreditAppRequestLineItemView CreateCreditAppRequestLine(CreditAppRequestLineItemView creditAppRequestLineView);
        CreditAppRequestLineItemView UpdateCreditAppRequestLine(CreditAppRequestLineItemView creditAppRequestLineView);
        CreditAppRequestLine UpdateCreditAppRequestLine(CreditAppRequestLine creditAppRequestLine);
        bool DeleteCreditAppRequestLine(string id);
        CreditAppRequestTableItemView GetCreditAppRequestTableById(string id);
        CreditAppRequestTableItemView CreateCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView, bool isCopyRef = true);
        CreditAppRequestTableItemView UpdateCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView);
        CreditAppRequestTable UpdateCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable);
        bool DeleteCreditAppRequestTable(string id);
        CreditAppRequestTableItemView GetCreditAppRequestTableInitialData(int productType);
        CreditAppRequestTableItemView GetReviewCreditAppRequestInitialData(string id);
        MemoTransItemView GetMemoTransInitialDataByAmendCaline(string refGUID);
        CreditAppRequestTableItemView GetLoanRequestOrBuyerMatchingCreditAppRequestTableInitialData(string creditApId, CreditAppRequestType creditAppRequestType);
        bool IsManualByCreditAppRequest(int productType);
        CreditAppRequestLineItemView GetCreditAppRequestLineInitialData(string creditAppRequestTableGUID);
        AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID);
        NCBTransItemView GetNCBTransInitialDataByAuthorizedPersonTrans(string refGUID);
        string GetStatusIdByCreditAppRequest(string creditAppRequestId);
        string GetStatusIdByCreditAppRequest(Guid creditAppRequestId);
        FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refGUID);
        FinancialStatementTransItemView GetFinancialStatementTransChildInitialData(string refGUID);
        TaxReportTransItemView GetTaxReportTransInitialData(string refGUID);
        ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialData(string refGUID);
        ServiceFeeConditionTransItemView GetServiceFeeConditionTransChildInitialData(string refGUID);
        AccessModeView GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(string creditAppRequestId);
        RetentionConditionTransItemView GetRetentionConditionTransInitialData(string refGUID);
        CustVisitingTransItemView GetCustVisitingTransInitialData(string refGUID);
        GuarantorTransItemView GetGuarantorTransInitialData(string refGUID);
        NCBTransItemView GetNCBTransInitialDataByCreditAppRequestTable(string refGUID);
        MemoTransItemView GetMemoTransInitialData(string refGUID);
        MemoTransItemView GetMemoTransChildInitialData(string refGUID);
        OwnerTransItemView GetOwnerTransInitialData(string refGUID);
        BuyerAgreementTransItemView GetBuyerAgreementTransChildInitialData(string refGUID);
        DocumentConditionTransItemView GetDocumentConditionTransChildInitialData(string refGUID);
        FinancialCreditTransItemView GetFinancialCreditTransInitialData(string refGUID);
        BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refGUID);
        CreditAppReqBusinessCollateralItemView GetCreditAppReqBusinessCollateralInitialData(string creditAppRequestTableGUID);
        IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTableByBusinessCollateralAgmTable(SearchParameter search);
        #region function
        CancelCreditAppRequestResultView GetCancelCreditApplicationRequestByAppReqGUID(string appReqGUID);
        CancelCreditAppRequestResultView CancelCreditApplicationRequest(CancelCreditAppRequestParamView paramView);
        #endregion
        void GenCreditAppRequestNumberSeqCode(CreditAppRequestTable creditAppRequestTable);
        decimal GetInterestTypeValueByCreditAppRequest(CreditAppRequestTableItemView creditAppRequestTableItem);
        AuthorizedPersonTransListView GetAuthorizedPersonTransInitialListData(string amendCAId);
        GuarantorTransListView GetGuarantorTransInitialListData(string amendCAId);
        #region GetAccessMode
        AccessModeView GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string amendCAId, string creditAppRequestTableGUID = null);
        AccessModeView GetAccessModeByCreditAppRequestTableStatusBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead(string amendCAId, string creditAppRequestTableGUID = null);
        AccessModeView GetAccessModeCARequestTableByAuthorizedperson(string authoriedPersonGUID);
        AccessModeView GetAccessModeByCreditAppRequestTableCreditLimitType(string creditAppRequestId);
        AccessModeView GetReviewCreditAppAccessModeByCreditAppRequestTable(string creditAppRequestId);
        AccessModeView GetAccessModeByCreditAppRequestTableStatusLessThenWaitingForMarketingStaff(string amendCAId, string creditAppRequestTableGUID = null);

        #endregion
        #region K2 Function
        void UpdateCreditApplicationRequestK2(UpdateCreditApplicationRequestK2ParmView item);
        void ApproveCreditApplicationRequestK2(ApproveCreditApplicationRequestK2ParmView item);
        void ApproveAmendcustomerCreditLimitK2(ApproveAmendcustomerCreditLimitK2ParmView item);
        void ApproveAmendCustomerInformationK2(ApproveAmendCustomerInformationK2ParmView item);
        void AmendCustomerBuyerCreditLimitK2(AmendCustomerBuyerCreditLimitK2ParmView item);
        void ApproveAmendBuyerInformationK2(ApproveAmendBuyerInformationK2ParmView item);
        GetCreditApplicationRequestEmailContentK2ResultView GetCreditApplicationRequestEmailContentK2(GetCreditApplicationRequestEmailContentK2ParmView item);
        #endregion
        AccessModeView GetAccessModeByCreditAppRequestLine(string amendLineCAId, string creditAppRequestTableGUID = null);
        CreditAppRequestLineItemView GetLoanRequestOrBuyerMatchingCreditAppRequestLineInitialData(string creditAppRequestTableId);
        #region Shared
        bool ValidateMarketingComment(Guid creditAppRequestTableGUID);
        bool ValidateCreditComment(Guid creditAppRequestTableGUID);
        bool ValidateApproverComment(Guid creditAppRequestTableGUID);
        bool ValidateApprovalDecision(Guid creditAppRequestTableGUID);
        bool ValidateApprovedCreditLimitLineRequest(Guid creditAppRequestTableGUID);
        #endregion Shared
        #region WorkFlow
        WorkFlowCreditAppRequestView GetWorkFlowCreditAppRequestById(string id);
        Task<WorkflowResultView> ValidateAndStartWorkflow(WorkFlowCreditAppRequestView item);
        Task<WorkflowResultView> ValidateAndActionWorkflow(WorkFlowCreditAppRequestView item);
        #endregion WorkFlow
        #region ReviewCreditAppRequest
        CreditAppRequestTableItemView CreateReviewCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView);
        CreditAppRequestTableItemView UpdateReviewCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView);
        bool DeleteReviewCreditAppRequestLine(string item);
        CreditAppRequestTableItemView GetReviewCreditAppRequestById(string id);
        BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialDataByCreditAppLine(string refGUID);

        #region functions
        UpdateReviewResultView GetUpdateReviewResultById(string creditAppRequestTableGUID);
        UpdateReviewResultResultView UpdateReviewResult(UpdateReviewResultView UpdateReviewResultView);
        #endregion
        #endregion
        #region CloseCustomerCreditLimit
        CreditAppRequestTableItemView GetCloseCustomerCreditLimitInitialData(string id);
        CreditAppRequestTableItemView CreateCloseCustomerCreditLimitCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView);
        AccessModeView GetAccessModeLoanCloseCreditLimit(string id);
        CreditAppRequestTableItemView GetClosingCreditAppRequestById(string id);
        #endregion CloseCustomerCreditLimit
        #region updateClosingResult
        UpdateClosingResultResultView UpdateClosingResult(UpdateClosingResultView UpdateClosingResultView);
        UpdateClosingResultView GetUpdateClosingResultById(string creditAppRequestTableGUID);
        #endregion
        void CreateCreditAppRequestTableCollection(List<CreditAppRequestTableItemView> creditAppRequestTableItemViews);
        void CreateCreditAppRequestLineCollection(List<CreditAppRequestLineItemView> creditAppRequestLineItemViews);
        #region Dropdown
        IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTable(SearchParameter search);

        #endregion
        IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTableByAssignmentAgreementTable(SearchParameter search);

        BuyerReviewCreditLimitBookmarkView GetBuyerReviewCreditLimitBookmarkValue(Guid refGUID, Guid bookmarkDocumentTransGUID);
        BookmarkDocumentQueryCreditApplicationPF GetBookmarkDocumentQueryCreditApplicationPFValues(Guid refGUID, Guid bookmarkDocumentTransGUID);
        BookmarkDocumentQueryCusotmerAmendInformation GetBookmarkDocumentQueryCustomerAmendInfo(Guid refGUID, Guid bookmarkDocumentTransGUID);
        BookmarkDocumentQueryBuyerAmendInformation GetBookmarkDocumentQueryBuyerAmendInfo(Guid refGUID, Guid bookmarkDocumentTransGUID);
        QueryBuyerAmendCreditLimit GetBookmarkDocumentQueryBuyerAmendCreditLimit(Guid refGUID, Guid bookmarkDocumentTransGUID);
        GenBookmarkBuyerCreditApplicationView GenBookmarkBuyerCreditAppValue(Guid refGuid,
            Guid bookmarkDocumentTransGuid);
        CopyFinancialStatementTransView CopyFinancialStatementTransByCART(CopyFinancialStatementTransView paramView);
        CopyFinancialStatementTransView CopyFinancialStatementTransByCARL(CopyFinancialStatementTransView paramView);
        CreditLimitClosingBookmarkView GetCreditLimitClosingBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid);
        AccessModeView GetBookMarkDocumentTransAccessModeByCreditAppRequestLine(string creditAppRLineId);
    }
    public class CreditAppRequestTableService : SmartAppService, ICreditAppRequestTableService
    {
        public CreditAppRequestTableService(SmartAppDbContext context) : base(context) { }
        public CreditAppRequestTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public CreditAppRequestTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public CreditAppRequestTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public CreditAppRequestTableService() : base() { }
        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> CreditAppRequestDropdownByDocumentStatus(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                search = search.GetParentCondition(new string[] { CreditAppRequestTableCondition.CustomerTableGUID, CreditAppRequestTableCondition.ProductType, CreditAppRequestTableCondition.ConsortiumTableGUID, CreditAppRequestTableCondition.StatusId});
                return creditAppRequestTableRepo.CreditAppRequestDropdownByDocumentStatus(search);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CreditAppRequestTableItemView GetByIdvwByCreditAppRequestLine(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetByIdvwByCreditAppRequestLine(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLineItemView GetCreditAppRequestLineById(string id)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                return creditAppRequestLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLineItemView CreateCreditAppRequestLine(CreditAppRequestLineItemView creditAppRequestLineView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                creditAppRequestLineView = accessLevelService.AssignOwnerBU(creditAppRequestLineView);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestLine creditAppRequestLine = creditAppRequestLineView.ToCreditAppRequestLine();
                creditAppRequestLine = creditAppRequestLineRepo.CreateCreditAppRequestLine(creditAppRequestLine);
                CopyReferenceCreditAppRequestLine(creditAppRequestLine); // SaveDataOnCreate
                base.LogTransactionCreate<CreditAppRequestLine>(creditAppRequestLine);
                UnitOfWork.Commit();
                return creditAppRequestLine.ToCreditAppRequestLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLineItemView UpdateCreditAppRequestLine(CreditAppRequestLineItemView creditAppRequestLineView)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestLine inputCreditAppRequestLine = creditAppRequestLineView.ToCreditAppRequestLine();
                CreditAppRequestLine dbCreditAppRequestLine = creditAppRequestLineRepo.Find(inputCreditAppRequestLine.CreditAppRequestLineGUID);
                dbCreditAppRequestLine = creditAppRequestLineRepo.UpdateCreditAppRequestLine(dbCreditAppRequestLine, inputCreditAppRequestLine);
                base.LogTransactionUpdate<CreditAppRequestLine>(GetOriginalValues<CreditAppRequestLine>(dbCreditAppRequestLine), dbCreditAppRequestLine);
                UnitOfWork.Commit();
                return dbCreditAppRequestLine.ToCreditAppRequestLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialDataByAmendCaline(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
                Guid creditappRequestLineGUID = creditAppRequestLineAmendRepo.GetCreditAppRequestLineAmendByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestLineGUID;
                string refId = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(creditappRequestLineGUID).LineNum.ToString();
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLine UpdateCreditAppRequestLine(CreditAppRequestLine creditAppRequestLine)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestLine dbCreditAppRequestLine = creditAppRequestLineRepo.Find(creditAppRequestLine.CreditAppRequestLineGUID);
                dbCreditAppRequestLine = creditAppRequestLineRepo.UpdateCreditAppRequestLine(dbCreditAppRequestLine, creditAppRequestLine);
                base.LogTransactionUpdate<CreditAppRequestLine>(GetOriginalValues<CreditAppRequestLine>(dbCreditAppRequestLine), dbCreditAppRequestLine);
                return dbCreditAppRequestLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCreditAppRequestLine(string item)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                Guid creditAppRequestLineGUID = new Guid(item);
                CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.Find(creditAppRequestLineGUID);
                creditAppRequestLineRepo.Remove(creditAppRequestLine);
                base.LogTransactionDelete<CreditAppRequestLine>(creditAppRequestLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CreditAppRequestTableItemView GetCreditAppRequestTableById(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView CreateCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView, bool isCopyRef = true)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                creditAppRequestTableView = accessLevelService.AssignOwnerBU(creditAppRequestTableView);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableView.ToCreditAppRequestTable();
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                if (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest || creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching)
                {
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppRequestTable.RefCreditAppTableGUID.Value);
                    ValidateCreateLoanRequestOrBuyerMatchingCreditAppRequestTable(creditAppRequestTable, creditAppTable);
                }
                else
                {
                    ValidateCreateCreditAppRequestTable(creditAppRequestTable);
                }
                GenCreditAppRequestNumberSeqCode(creditAppRequestTable);
                #region R02
                creditAppRequestTable.CustomerCreditLimit = creditAppTableRepo.GetCustomerCreditLimit(creditAppRequestTable.CustomerTableGUID, creditAppRequestTable.ProductType, creditAppRequestTable.RequestDate);
                #endregion
                creditAppRequestTable.CreditAppRequestTableGUID = Guid.NewGuid();
                CopyReferenceCreditAppRequestTableResultView resultRef = null;
                if (isCopyRef)
                {
                    resultRef = CopyReferenceCreditAppRequestTable(creditAppRequestTable); // SaveDataOnCreate
                }
                creditAppRequestTableRepo.ValidateAdd(creditAppRequestTable);
                #region BulkInsert
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        BulkInsert(creditAppRequestTable.FirstToList());
                        if (isCopyRef && resultRef != null)
                        {
                            if (resultRef.AuthorizedPersonTrans.Any())
                            {
                                BulkInsert(resultRef.AuthorizedPersonTrans);
                            }
                            if (resultRef.GuarantorTrans.Any())
                            {
                                BulkInsert(resultRef.GuarantorTrans);
                            }
                            if (resultRef.OwnerTrans.Any())
                            {
                                BulkInsert(resultRef.OwnerTrans);
                            }
                            if (resultRef.CAReqRetentionOutstandings.Any())
                            {
                                BulkInsert(resultRef.CAReqRetentionOutstandings);
                            }
                            if (resultRef.CAReqCreditOutStandings.Any())
                            {
                                BulkInsert(resultRef.CAReqCreditOutStandings);
                            }
                            if (resultRef.CAReqAssignmentOutstandings.Any())
                            {
                                BulkInsert(resultRef.CAReqAssignmentOutstandings);
                            }
                            if (resultRef.CAReqBuyerCreditOutstandings.Any())
                            {
                                BulkInsert(resultRef.CAReqBuyerCreditOutstandings);
                            }                            
                        }
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }

                #endregion

                return creditAppRequestTable.ToCreditAppRequestTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateCreateCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable)
        {
            try
            {
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppRequestTable.CreditLimitTypeGUID);
                if (creditLimitType.ValidateCreditLimitType)
                {
                    IsCreditAppTableNotExpired(creditAppRequestTable);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CopyReferenceCreditAppRequestTableResultView CopyReferenceCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable)
        {
            try
            {
                string userName = db.GetUserName();
                DateTime systemDate = DateTime.Now;
                CopyReferenceCreditAppRequestTableResultView result = new CopyReferenceCreditAppRequestTableResultView();
                #region Copy Reference
                if(creditAppRequestTable.CreditAppRequestType != (int)CreditAppRequestType.BuyerMatching &&
                   creditAppRequestTable.CreditAppRequestType != (int)CreditAppRequestType.LoanRequest)
                {
                    IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                    result.AuthorizedPersonTrans = authorizedPersonTransService.GetActiveAuthorizedPersonTrans(creditAppRequestTable.CustomerTableGUID, (int)RefType.Customer).ToList();
                    if (result.AuthorizedPersonTrans.Any())
                    {
                        result.AuthorizedPersonTrans.ForEach(f =>
                        {
                            f.AuthorizedPersonTransGUID = Guid.NewGuid();
                            f.RefGUID = creditAppRequestTable.CreditAppRequestTableGUID;
                            f.RefType = (int)RefType.CreditAppRequestTable;
                            f.CreatedBy = userName;
                            f.ModifiedBy = userName;
                            f.CreatedDateTime = systemDate;
                            f.ModifiedDateTime = systemDate;
                            f.Owner = userName;
                            f.OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID;
                        });
                    }
                    IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                    result.GuarantorTrans = guarantorTransService.GetActiveGuarantorTrans(creditAppRequestTable.CustomerTableGUID, (int)RefType.Customer).ToList();
                    if (result.GuarantorTrans.Any())
                    {
                        result.GuarantorTrans.ForEach(f =>
                        {
                            f.GuarantorTransGUID = Guid.NewGuid();
                            f.RefGUID = creditAppRequestTable.CreditAppRequestTableGUID;
                            f.RefType = (int)RefType.CreditAppRequestTable;
                            f.CreatedBy = userName;
                            f.ModifiedBy = userName;
                            f.CreatedDateTime = systemDate;
                            f.ModifiedDateTime = systemDate;
                            f.Owner = userName;
                            f.OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID;
                        });

                    }
                    IOwnerTransService ownerTransService = new OwnerTransService(db);
                    result.OwnerTrans = ownerTransService.GetActiveOwnerTrans(creditAppRequestTable.CustomerTableGUID, (int)RefType.Customer).ToList();
                    if (result.OwnerTrans.Any())
                    {
                        result.OwnerTrans.ForEach(f =>
                        {
                            f.OwnerTransGUID = Guid.NewGuid();
                            f.RefGUID = creditAppRequestTable.CreditAppRequestTableGUID;
                            f.RefType = (int)RefType.CreditAppRequestTable;
                            f.CreatedBy = userName;
                            f.ModifiedBy = userName;
                            f.CreatedDateTime = systemDate;
                            f.ModifiedDateTime = systemDate;
                            f.Owner = userName;
                            f.OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID;
                        });
                    }

                }
                else
                {
                    result.AuthorizedPersonTrans = new List<AuthorizedPersonTrans>();
                    result.GuarantorTrans = new List<GuarantorTrans>();
                    result.OwnerTrans = new List<OwnerTrans>();
                }
                IRetentionTransRepo retentionTransRepo = new RetentionTransRepo(db);
                result.CAReqRetentionOutstandings = new List<CAReqRetentionOutstanding>();
                List<RetentionOutstandingView> retentionOutstandingViews = retentionTransRepo.GetRetentionOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID);
                if (retentionOutstandingViews.Any())
                {
                    retentionOutstandingViews.ForEach(f =>
                    {
                        result.CAReqRetentionOutstandings.Add(new CAReqRetentionOutstanding
                        {
                            CAReqRetentionOutstandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID,
                            ProductType = f.ProductType,
                            MaximumRetention = f.MaximumRetention,
                            AccumRetentionAmount = f.AccumRetentionAmount,
                            RemainingAmount = f.RemainingAmount,
                            CreditAppTableGUID = f.CreditAppTableGUID,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            CompanyGUID = f.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                        });
                    });
                }
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                result.CAReqCreditOutStandings = new List<CAReqCreditOutStanding>();
                List<CreditOutstandingViewMap> creditOustandingView = creditAppTableRepo.GetCreditOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID, creditAppRequestTable.RequestDate);
                if (creditOustandingView.Any())
                {
                    creditOustandingView.ForEach(f =>
                    {
                        result.CAReqCreditOutStandings.Add(new CAReqCreditOutStanding
                        {
                            CAReqCreditOutStandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID,
                            ProductType = f.ProductType,
                            CreditLimitTypeGUID = f.CreditLimitTypeGUID,
                            AccumRetentionAmount = f.AccumRetentionAmount,
                            AsOfDate = f.AsOfDate,
                            CreditAppTableGUID = f.CreditAppTableGUID,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            ApprovedCreditLimit = f.ApprovedCreditLimit,
                            CreditLimitBalance = f.CreditLimitBalance,
                            ARBalance = f.ARBalance,
                            ReserveToBeRefund = f.ReserveToBeRefund,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                        });
                    });
                }
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                result.CAReqAssignmentOutstandings = new List<CAReqAssignmentOutstanding>();
                List<AssignmentAgreementOutstandingView> assignmentAgreementOutstandingViews = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID);
                if (assignmentAgreementOutstandingViews.Any())
                {
                    assignmentAgreementOutstandingViews.ForEach(f =>
                    {
                        result.CAReqAssignmentOutstandings.Add(new CAReqAssignmentOutstanding
                        {
                            CAReqAssignmentOutstandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID.StringToGuid(),
                            AssignmentAgreementTableGUID = f.AssignmentAgreementTableGUID.StringToGuid(),
                            BuyerTableGUID = f.BuyerTableGUID.StringToGuid(),
                            AssignmentAgreementAmount = f.AssignmentAgreementAmount,
                            SettleAmount = f.SettledAmount,
                            RemainingAmount = f.RemainingAmount,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                        });
                    });
                }
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                result.CAReqBuyerCreditOutstandings = new List<CAReqBuyerCreditOutstanding>();
                List<CALineOutstandingViewMap> caLineOutstandingViewMap = new List<CALineOutstandingViewMap>();
                if(creditAppRequestTable.RefCreditAppTableGUID.HasValue)
                {
                    caLineOutstandingViewMap = creditAppLineRepo.GetCALineOutstandingByCA((int)RefType.CreditAppTable, creditAppRequestTable.RefCreditAppTableGUID.Value, true, creditAppRequestTable.RequestDate).ToList();
                    if (caLineOutstandingViewMap.Any())
                    {
                        caLineOutstandingViewMap.ForEach(f =>
                        {
                            result.CAReqBuyerCreditOutstandings.Add(new CAReqBuyerCreditOutstanding
                            {
                                CAReqBuyerCreditOutstandingGUID = Guid.NewGuid(),
                                CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                                CreditAppTableGUID = f.CreditAppTableGUID,
                                CreditAppLineGUID = f.CreditAppLineGUID,
                                LineNum = f.LineNum,
                                BuyerTableGUID = f.BuyerTableGUID,
                                Status = f.Status,
                                ApprovedCreditLimitLine = f.ApprovedCreditLimitLine,
                                ARBalance = f.ARBalance,
                                AssignmentMethodGUID = f.AssignmentMethodGUID,
                                BillingResponsibleByGUID = f.BillingResponsibleByGUID,
                                MethodOfPaymentGUID = f.MethodOfPaymentGUID,
                                ProductType = f.ProductType,
                                MaxPurchasePct = f.MaxPurchasePct,
                                CompanyGUID = creditAppRequestTable.CompanyGUID,
                                Owner = userName,
                                OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                            });
                        });
                    }
                }
                #endregion Copy Reference
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView UpdateCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView)
        {
            try
            {
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableView.ToCreditAppRequestTable();
                if (!(creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest || creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching))
                {
                    if (creditAppRequestTableView.CreditAppRequestType != (int)CreditAppRequestType.CloseCustomerCreditLimit)
                    {
                        ValidateCreateCreditAppRequestTable(creditAppRequestTable);
                    }
                }
                UpdateCreditAppRequestTable(creditAppRequestTable);
                UnitOfWork.Commit();
                return creditAppRequestTable.ToCreditAppRequestTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteCreditAppRequestTable(string item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                Guid creditAppRequestTableGUID = new Guid(item);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTableGUID);
                creditAppRequestTableRepo.Remove(creditAppRequestTable);
                base.LogTransactionDelete<CreditAppRequestTable>(creditAppRequestTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region NumberSeq
        public bool IsManualByCreditAppRequest(int productType)
        {
            try
            {
                Guid companyGUID = GetCurrentCompany().StringToGuid();
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetCreditAppRequestNumberSeqGUID(companyGUID, productType);
                return numberSequenceService.IsManualByNumberSeqTableGUID(numberSeqTableGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenCreditAppRequestNumberSeqCode(CreditAppRequestTable creditAppRequestTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByCreditAppRequest(creditAppRequestTable.ProductType);
                if (!isManual)
                {
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetCreditAppRequestNumberSeqGUID(creditAppRequestTable.CompanyGUID, creditAppRequestTable.ProductType);
                    creditAppRequestTable.CreditAppRequestId = numberSequenceService.GetNumber(creditAppRequestTable.CompanyGUID, null, numberSeqTableGUID);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion NumberSeq
        #region InitialData
        public CreditAppRequestTableItemView GetCreditAppRequestTableInitialData(int productType)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)CreditAppRequestStatus.Draft);
                return new CreditAppRequestTableItemView
                {
                    CreditAppRequestTableGUID = new Guid().GuidNullToString(),
                    CreditAppRequestType = (int)CreditAppRequestType.MainCreditLimit,
                    RequestDate = DateTime.Now.DateToString(),
                    DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
                    DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                    DocumentStatus_StatusId = documentStatus.StatusId,
                    ProductType = productType,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView GetReviewCreditAppRequestInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTableItemView creditAppRequestTableItemView = creditAppRequestTableRepo.GetReviewCreditAppRequestInitialData(id);
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                ICustTransService custTransService = new CustTransService(db);
                creditAppRequestTableItemView.CustomerBuyerOutstanding = creditAppTableService.GetCustomerBuyerOutstanding(creditAppRequestTableItemView.RefCreditAppTableGUID.StringToGuid(), creditAppRequestTableItemView.BuyerTableGUID.StringToGuid());
                creditAppRequestTableItemView.CustomerAllBuyerOutstanding = custTransService.GetAllCustomerBuyerOutstanding(creditAppRequestTableItemView.ProductType, creditAppRequestTableItemView.BuyerTableGUID.StringToGuid());
                return creditAppRequestTableItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView GetLoanRequestOrBuyerMatchingCreditAppRequestTableInitialData(string creditApId, CreditAppRequestType creditAppRequestType)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetLoanRequestOrBuyerMatchingCreditAppRequestTableInitialData(creditApId.StringToGuid(), (int)creditAppRequestType);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CreditAppRequestLineItemView GetCreditAppRequestLineInitialData(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(creditAppRequestTableGUID.StringToGuid());
                DocumentStatus documentStatus = documentStatusRepo.Find(creditAppRequestTable.DocumentStatusGUID);
                IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo.GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID.StringToGuid());
                return new CreditAppRequestLineItemView
                {
                    CreditAppRequestLineGUID = new Guid().GuidNullToString(),
                    CreditAppRequestTableGUID = creditAppRequestTableGUID,
                    LineNum = (creditAppRequestLines.Any()) ? creditAppRequestLines.Max(m => m.LineNum) + 1 : 1,
                    CreditAppRequestTable_ProductType = creditAppRequestTable.ProductType,
                    CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                    CreditAppRequestTable_StatusId = documentStatus.StatusId,
                    MaxPurchasePct = creditAppRequestTable.MaxPurchasePct,
                    PurchaseFeePct = creditAppRequestTable.PurchaseFeePct,
                    PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestLineItemView GetLoanRequestOrBuyerMatchingCreditAppRequestLineInitialData(string creditAppRequestTableId)
        {
            try
            {
                Guid creditAppRequestTableGUID = creditAppRequestTableId.StringToGuid();
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo.GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID);
                CreditAppRequestLineItemView result = (from creditAppRequestTable in db.Set<CreditAppRequestTable>()
                                                       join creditAppLine in db.Set<CreditAppLine>()
                                                       on creditAppRequestTable.RefCreditAppTableGUID equals creditAppLine.CreditAppTableGUID into ljCreditAppLine
                                                       from creditAppLine in ljCreditAppLine.DefaultIfEmpty()
                                                       join documentStatus in db.Set<DocumentStatus>() on creditAppRequestTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                                                       where creditAppRequestTable.CreditAppRequestTableGUID == creditAppRequestTableGUID
                                                       select new CreditAppRequestLineItemView
                                                       {
                                                           CreditAppRequestLineGUID = new Guid().GuidNullToString(),
                                                           CreditAppRequestTableGUID = creditAppRequestTableId,
                                                           LineNum = creditAppLine.LineNum,
                                                           CreditAppRequestTable_ProductType = creditAppRequestTable.ProductType,
                                                           CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                                                           CreditAppRequestTable_StatusId = documentStatus.StatusId,
                                                           BuyerTableGUID = creditAppLine.BuyerTableGUID.GuidNullToString(),
                                                           MaxPurchasePct = creditAppRequestTable.MaxPurchasePct,
                                                           PurchaseFeePct = creditAppRequestTable.PurchaseFeePct,
                                                           PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase,
                                                           // for GetRemainingCreditLoanRequest then clear value to null
                                                           RefCreditAppLineGUID = creditAppRequestTable.RefCreditAppTableGUID.GuidNullToString(),
                                                       }).OrderBy(w => w.LineNum).FirstOrDefault();
                result.LineNum = (creditAppRequestLines.Any()) ? creditAppRequestLines.Max(m => m.LineNum) + 1 : 1;
                result.RemainingCreditLoanRequest = creditAppTableService.GetRemainingCreditLoanRequest(result.RefCreditAppLineGUID.StringToGuid());
                result.RefCreditAppLineGUID = null;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppReqBusinessCollateralItemView GetCreditAppReqBusinessCollateralInitialData(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTableGUID.StringToGuid());
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable customerTable = customerTableRepo.Find(creditAppRequestTable.CustomerTableGUID);
                return new CreditAppReqBusinessCollateralItemView
                {
                    CreditAppReqBusinessCollateralGUID = new Guid().GuidNullToString(),
                    CreditAppRequestTableGUID = creditAppRequestTableGUID,
                    CustomerTableGUID = creditAppRequestTable.CustomerTableGUID.GuidNullToString(),
                    CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                    CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                    IsNew = true,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion InitialData
        public void CopyReferenceCreditAppRequestLine(CreditAppRequestLine creditAppRequestLine)
        {
            try
            {
                string userName = db.GetUserName();
                DateTime systemDate = DateTime.Now;
                #region Copy Reference
                IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
                List<DocumentConditionTrans> documentConditionTrans = documentConditionTransService.GetDocumentConditionTransByReference(creditAppRequestLine.BuyerTableGUID, (int)RefType.Buyer).ToList();
                if (documentConditionTrans.Any())
                {
                    documentConditionTrans.ForEach(f =>
                    {
                        f.DocumentConditionTransGUID = Guid.NewGuid();
                        f.RefGUID = creditAppRequestLine.CreditAppRequestLineGUID;
                        f.RefType = (int)RefType.CreditAppRequestLine;
                        f.CreatedBy = userName;
                        f.ModifiedBy = userName;
                        f.CreatedDateTime = systemDate;
                        f.ModifiedDateTime = systemDate;
                        f.Owner = userName;
                        f.OwnerBusinessUnitGUID = creditAppRequestLine.OwnerBusinessUnitGUID;
                    });
                }
                #region Create
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        if (documentConditionTrans.Any())
                        {
                            BulkInsert(documentConditionTrans);
                        }
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region relatedInfo
        public AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refGUID)
        {
            try
            {
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return authorizedPersonTransService.GetAuthorizedPersonTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public NCBTransItemView GetNCBTransInitialDataByAuthorizedPersonTrans(string refGUID)
        {
            try
            {
                INCBTransService ncbTransService = new NCBTransService(db);
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                IRelatedPersonTableRepo relatedPersonTableRepo = new RelatedPersonTableRepo(db);
                AuthorizedPersonTrans authorizedPersonTrans = authorizedPersonTransRepo.GetAuthorizedPersonTransByIdNoTracking(refGUID.StringToGuid());
                string refId = relatedPersonTableRepo.GetRelatedPersonTableByIdNoTrackingByAccessLevel(authorizedPersonTrans.RelatedPersonTableGUID).RelatedPersonId;
                return ncbTransService.GetNCBTransInitialData(refId, refGUID, Models.Enum.RefType.AuthorizedPersonTrans);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string GetStatusIdByCreditAppRequest(string creditAppRequestId)
        {
            try
            {
                return GetStatusIdByCreditAppRequest(creditAppRequestId.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string GetStatusIdByCreditAppRequest(Guid creditAppRequestId)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(creditAppRequestId);
                return documentStatusRepo.GetDocumentStatusByIdNoTracking(creditAppRequestTable.DocumentStatusGUID).StatusId;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refGUID)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return financialStatementTransService.GetFinancialStatementTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FinancialStatementTransItemView GetFinancialStatementTransChildInitialData(string refGUID)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                string refId = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).LineNum.ToString();
                return financialStatementTransService.GetFinancialStatementTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public TaxReportTransItemView GetTaxReportTransInitialData(string refGUID)
        {
            try
            {
                ITaxReportTransService taxReportTransService = new TaxReportTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return taxReportTransService.GetTaxReportTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeConditionTransItemView GetServiceFeeConditionTransInitialData(string refGUID)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return serviceFeeConditionTransService.GetServiceFeeConditionTransInitialDataByCreditAppRequestTable(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeConditionTransItemView GetServiceFeeConditionTransChildInitialData(string refGUID)
        {
            try
            {
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                string refId = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).LineNum.ToString();
                return serviceFeeConditionTransService.GetServiceFeeConditionTransInitialDataByCreditAppRequestLine(refId, refGUID, Models.Enum.RefType.CreditAppRequestLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public RetentionConditionTransItemView GetRetentionConditionTransInitialData(string refGUID)
        {
            try
            {
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                return retentionConditionTransService.GetRetentionConditionTransInitialData(creditAppRequestTable.CreditAppRequestId, refGUID, (int)RefType.CreditAppRequestTable, creditAppRequestTable.ProductType);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CustVisitingTransItemView GetCustVisitingTransInitialData(string refGUID)
        {
            try
            {
                ICustVisitingTransService custVisitingTransService = new CustVisitingTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return custVisitingTransService.GetCustVisitingTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView GetGuarantorTransInitialData(string refGUID)
        {
            try
            {
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return guarantorTransService.GetGuarantorTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public NCBTransItemView GetNCBTransInitialDataByCreditAppRequestTable(string refGUID)
        {
            try
            {
                INCBTransService ncbTransService = new NCBTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return ncbTransService.GetNCBTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransChildInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                int lineNum = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).LineNum;
                return memoTransService.GetMemoTransInitialData(lineNum.ToString(), refGUID, Models.Enum.RefType.CreditAppRequestLine);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public AccessModeView GetBookMarkDocumentTransAccessModeByCreditAppRequestTable(string creditAppRequestId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestId.StringToGuid());
                string _CreditAppRequestStatus = GetStatusIdByCreditAppRequest(creditAppRequestId);
                bool isLessThanApproved = (Convert.ToInt32(_CreditAppRequestStatus) <= (int)CreditAppRequestStatus.Approved);

                accessModeView.CanCreate = isLessThanApproved;
                accessModeView.CanView = true;
                accessModeView.CanDelete = isLessThanApproved;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetBookMarkDocumentTransAccessModeByCreditAppRequestLine(string creditAppRLineId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTracking(creditAppRLineId.StringToGuid());
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestLine.CreditAppRequestTableGUID);
                string _CreditAppRequestStatus = GetStatusIdByCreditAppRequest(creditAppRequestLine.CreditAppRequestTableGUID);
                bool isLessThanApproved = (Convert.ToInt32(_CreditAppRequestStatus) <= (int)CreditAppRequestStatus.Approved);

                accessModeView.CanCreate = isLessThanApproved;
                accessModeView.CanView = true;
                accessModeView.CanDelete = isLessThanApproved;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AuthorizedPersonTransListView GetAuthorizedPersonTransInitialListData(string amendCAId)
        {
            try
            {
                AuthorizedPersonTransListView authorizedPersonTransListView = new AuthorizedPersonTransListView();
                AccessModeView accessModeView = new AccessModeView();
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(amendCAId.StringToGuid());
                authorizedPersonTransListView.RefGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString();
                authorizedPersonTransListView.RefType = (int)RefType.CreditAppRequestTable;
                authorizedPersonTransListView.AccessModeView = GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString());
                return authorizedPersonTransListView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransListView GetGuarantorTransInitialListData(string amendCAId)
        {
            try
            {
                GuarantorTransListView guarantorTransListView = new GuarantorTransListView();
                AccessModeView accessModeView = new AccessModeView();
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(amendCAId.StringToGuid());
                guarantorTransListView.RefGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString();
                guarantorTransListView.RefType = (int)RefType.CreditAppRequestTable;
                guarantorTransListView.AccessModeView = GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString());
                return guarantorTransListView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public OwnerTransItemView GetOwnerTransInitialData(string refGUID)
        {
            try
            {
                IOwnerTransService ownerTransService = new OwnerTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return ownerTransService.GetOwnerTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementTransItemView GetBuyerAgreementTransChildInitialData(string refGUID)
        {
            try
            {
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                int lineNum = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).LineNum;
                return buyerAgreementTransService.GetBuyerAgreementTransInitialData(lineNum.ToString(), refGUID, Models.Enum.RefType.CreditAppRequestLine);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public DocumentConditionTransItemView GetDocumentConditionTransChildInitialData(string refGUID)
        {
            try
            {
                IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                int lineNum = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).LineNum;
                return documentConditionTransService.GetDocumentConditionTransInitialData(lineNum.ToString(), refGUID, Models.Enum.RefType.CreditAppRequestLine);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public FinancialCreditTransItemView GetFinancialCreditTransInitialData(string refGUID)
        {
            try
            {
                IFinancialCreditTransService financialCreditTransService = new FinancialCreditTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return financialCreditTransService.GetFinancialCreditTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refGUID)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                string refId = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).CreditAppRequestId;
                return bookmarkDocumentTransService.GetBookmarkDocumentTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialDataByCreditAppLine(string refGUID)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                string refId = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).LineNum.ToString();
                return bookmarkDocumentTransService.GetBookmarkDocumentTransInitialData(refId, refGUID, Models.Enum.RefType.CreditAppRequestLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetInterestTypeValueByCreditAppRequest(CreditAppRequestTableItemView creditAppRequestTableItem)
        {
            try
            {
                IInterestTypeService interestTypeService = new InterestTypeService(db);
                return interestTypeService.GetInterestTypeValue(creditAppRequestTableItem.InterestTypeGUID.StringToGuid(), creditAppRequestTableItem.RequestDate.StringToDate());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region function
        public CancelCreditAppRequestResultView GetCancelCreditApplicationRequestByAppReqGUID(string appReqGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CancelCreditAppRequestResultView cancelCreditAppRequestResultView = creditAppRequestTableRepo.GetCancelCreditApplicationRequestByAppReqGUID(appReqGUID.StringToGuid());
                return cancelCreditAppRequestResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CancelCreditAppRequestResultView CancelCreditApplicationRequest(CancelCreditAppRequestParamView paramView)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CancelCreditAppRequestResultView result = new CancelCreditAppRequestResultView();
                NotificationResponse success = new NotificationResponse();
                IDocumentService documentService = new DocumentService(db);
                DocumentStatus cancelledStatus = documentService.GetDocumentStatusByStatusId((int)CreditAppRequestStatus.Cancelled);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(paramView.CreditAppRequestGUID.StringToGuid());
                ValidateCancelCreditApplicationRequest(paramView);
                creditAppRequestTable.DocumentStatusGUID = cancelledStatus.DocumentStatusGUID;
                creditAppRequestTable.DocumentReasonGUID = paramView.DocumentReasonGUID.StringToGuidNull();
                creditAppRequestTable.ModifiedBy = db.GetUserName();
                creditAppRequestTable.ModifiedDateTime = DateTime.Now;
                UpdateCreditAppRequestTable(creditAppRequestTable);
                UnitOfWork.Commit();
                success.AddData("SUCCESS.90008", new string[] { paramView.RefLabel, SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description) });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateCancelCreditApplicationRequest(CancelCreditAppRequestParamView paramView)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus draftStatus = documentService.GetDocumentStatusByStatusId((int)CreditAppRequestStatus.Draft);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(paramView.CreditAppRequestGUID.StringToGuid());
                string tableName = "LABEL.CREDIT_APPLICATION_REQUEST";
                switch (creditAppRequestTable.CreditAppRequestType)
                {
                    case (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit:
                    case (int)CreditAppRequestType.AmendCustomerInfo:
                        tableName = "LABEL.AMEND_CREDIT_APPLICATION";
                        break;
                    case (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit:
                    case (int)CreditAppRequestType.AmendBuyerInfo:
                        tableName = "LABEL.AMEND_CREDIT_APPLICATION_LINE";
                        break;
                    case (int)CreditAppRequestType.ReviewBuyerCreditLimit:
                        tableName = "LABEL.REVIEW_CREDIT_APPLICATION_REQUEST";
                        break;
                    case (int)CreditAppRequestType.CloseCustomerCreditLimit:
                        tableName = "LABEL.CLOSING_CREDIT_LIMIT_REQUEST";
                        break;
                    default:
                        break;
                }
                if (creditAppRequestTable.DocumentStatusGUID != draftStatus.DocumentStatusGUID)
                {
                    SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                    smartAppException.AddData("ERROR.90012", tableName);
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTable UpdateCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable dbCreditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTable.CreditAppRequestTableGUID);
                creditAppRequestTableRepo.UpdateCreditAppRequestTable(dbCreditAppRequestTable, creditAppRequestTable);
                base.LogTransactionUpdate<CreditAppRequestTable>(GetOriginalValues<CreditAppRequestTable>(dbCreditAppRequestTable), dbCreditAppRequestTable);
                return dbCreditAppRequestTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTableByBusinessCollateralAgmTable(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                search = search.GetParentCondition( new string[] { CreditAppRequestTableCondition.CustomerTableGUID, CreditAppRequestTableCondition.ProductType});
                SearchCondition searchCondition = SearchConditionService.GetCreditAppRequestTableStatusCondition(Convert.ToInt32(CreditAppRequestStatus.Approved).ToString());
                search.Conditions.Add(searchCondition);
                return creditAppRequestTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region GetAccessMode
        public AccessModeView GetAccessModeByCreditAppRequestTableCreditLimitType(string creditAppRequestId)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTrackingByAccessLevel(creditAppRequestId.StringToGuid());
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID);
                AccessModeView accessModeByEqualWaitingForMarketingStaff = GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, creditAppRequestId);
                string _CreditAppRequestStatus = GetStatusIdByCreditAppRequest(creditAppRequestTable.CreditAppRequestTableGUID);
                bool isDraftStatus = _CreditAppRequestStatus == Convert.ToInt32(CreditAppRequestStatus.Draft).ToString();

                AccessModeView accessModeView = new AccessModeView();
                accessModeView.CanCreate = (creditLimitType.AllowAddLine && (accessModeByEqualWaitingForMarketingStaff.CanCreate || isDraftStatus));
                accessModeView.CanView = true;
                accessModeView.CanDelete = (accessModeByEqualWaitingForMarketingStaff.CanDelete || isDraftStatus);
                accessModeView.AccessStatus = _CreditAppRequestStatus;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string amendCAId, string creditAppRequestTableGUID = null)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                if (String.IsNullOrEmpty(creditAppRequestTableGUID) && !String.IsNullOrEmpty(amendCAId))
                {
                    ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                    CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(amendCAId.StringToGuid());
                    creditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString();
                }
                string _CreditAppRequestStatus = GetStatusIdByCreditAppRequest(creditAppRequestTableGUID);
                bool isEqualWaitingForMarketingStaff = (Convert.ToInt32(_CreditAppRequestStatus) == (int)CreditAppRequestStatus.WaitingForMarketingStaff);
                accessModeView.CanCreate = isEqualWaitingForMarketingStaff;
                accessModeView.CanView = isEqualWaitingForMarketingStaff;
                accessModeView.CanDelete = isEqualWaitingForMarketingStaff;
                accessModeView.AccessStatus = _CreditAppRequestStatus;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByCreditAppRequestTableStatusBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead(string amendCAId, string creditAppRequestTableGUID = null)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                if (String.IsNullOrEmpty(creditAppRequestTableGUID) && !String.IsNullOrEmpty(amendCAId))
                {
                    ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                    CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(amendCAId.StringToGuid());
                    creditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString();
                }
                int creditAppRequestStatus = Convert.ToInt32(GetStatusIdByCreditAppRequest(creditAppRequestTableGUID));
                bool isBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead = (creditAppRequestStatus >= (int)CreditAppRequestStatus.Draft
                                                    && creditAppRequestStatus <= (int)CreditAppRequestStatus.WaitingForAssistMD
                                                    && creditAppRequestStatus != (int)CreditAppRequestStatus.WaitingForMarketingHead);
                accessModeView.CanCreate = isBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead;
                accessModeView.CanView = isBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead;
                accessModeView.CanDelete = isBetweenDraftAndWaitingForAssistMDAndNotWaitingForMarketingHead;
                accessModeView.AccessStatus = creditAppRequestStatus.ToString();
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeCARequestTableByAuthorizedperson(string authoriedPersonGUID)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                AuthorizedPersonTrans authorizedPersonTrans = authorizedPersonTransRepo.GetAuthorizedPersonTransByIdNoTracking(authoriedPersonGUID.StringToGuid());
                string _CreditAppRequestStatus = GetStatusIdByCreditAppRequest(authorizedPersonTrans.RefGUID);
                bool isEqualWaitingForMarketingStaff = (Convert.ToInt32(_CreditAppRequestStatus) == (int)CreditAppRequestStatus.WaitingForMarketingStaff);
                accessModeView.CanCreate = isEqualWaitingForMarketingStaff;
                accessModeView.CanView = isEqualWaitingForMarketingStaff;
                accessModeView.CanDelete = isEqualWaitingForMarketingStaff;
                accessModeView.AccessStatus = _CreditAppRequestStatus;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void IsCreditAppTableNotExpired(CreditAppRequestTable creditAppRequestTable)
        {
            try
            {
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                IEnumerable<CreditAppTableItemView> creditAppTableViews = creditAppTableRepo.GetCreditAppTableNotExpired(creditAppRequestTable);
                if (creditAppTableViews.Any())
                {
                    CreditAppTableItemView creditAppTableItemView = creditAppTableViews.FirstOrDefault();
                    SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                    smartAppException.AddData("ERROR.90001", new string[] { creditAppTableItemView.CreditLimitType_Values, creditAppTableItemView.CreditAppTable_Values });
                    throw SmartAppUtil.AddStackTrace(smartAppException);

                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetReviewCreditAppAccessModeByCreditAppRequestTable(string creditAppRequestId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestId.StringToGuid());
                string _CreditAppRequestStatus = GetStatusIdByCreditAppRequest(creditAppRequestId);
                bool isDraft = (Convert.ToInt32(_CreditAppRequestStatus) == (int)CreditAppRequestStatus.Draft);

                accessModeView.CanCreate = isDraft;
                accessModeView.CanView = isDraft;
                accessModeView.CanDelete = isDraft;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByCreditAppRequestTableStatusLessThenWaitingForMarketingStaff(string amendCAId, string creditAppRequestTableGUID = null)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                if (String.IsNullOrEmpty(creditAppRequestTableGUID) && !String.IsNullOrEmpty(amendCAId))
                {
                    ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                    CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByAmendCANoTrackingByAccessLevel(amendCAId.StringToGuid());
                    creditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString();
                }
                string _CreditAppRequestStatus = GetStatusIdByCreditAppRequest(creditAppRequestTableGUID);
                bool condition = (Convert.ToInt32(_CreditAppRequestStatus) < (int)CreditAppRequestStatus.WaitingForMarketingStaff);
                accessModeView.CanCreate = condition;
                accessModeView.CanView = condition;
                accessModeView.CanDelete = condition;
                accessModeView.AccessStatus = _CreditAppRequestStatus;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public CreditAppRequestLine ValidateRemainingLoanRequest(Guid creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestLine creditAppRequestLine = new CreditAppRequestLine();
                List<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID).ToList();
                if (creditAppRequestLines.Any())
                {
                    decimal remainingCreditLoanRequest = creditAppRequestLines.FirstOrDefault().RemainingCreditLoanRequest;
                    decimal totalCreditLimitLineRequest = creditAppRequestLines.Sum(s => s.CreditLimitLineRequest);
                    creditAppRequestLine.RemainingCreditLoanRequest = remainingCreditLoanRequest;
                    creditAppRequestLine.CreditLimitLineRequest = totalCreditLimitLineRequest;
                }
                return creditAppRequestLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void UpdateCreditApplicationRequestStatus(CreditAppRequestTable creditAppRequestTable, CreditAppRequestStatus creditAppRequestStatus)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)creditAppRequestStatus).ToString());
                creditAppRequestTable.DocumentStatusGUID = documentStatus.DocumentStatusGUID;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region K2 Function
        #region LIT1599 API Update credit application request K2
        public void UpdateCreditApplicationRequestK2(UpdateCreditApplicationRequestK2ParmView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(item.CreditAppRequestTableGUID.StringToGuid());
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                if (item.ProcesInstanceId == 0)
                {
                    smartAppException.AddData("ERROR.90128");
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
                if (item.ProcesInstanceId != creditAppRequestTable.ProcessInstanceId && creditAppRequestTable.ProcessInstanceId != 0)
                {
                    smartAppException.AddData("ERROR.90094");
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
                if (!string.IsNullOrWhiteSpace(item.StatusId))
                {
                    var isEnumDefined = IsEnumDefined<CreditAppRequestStatus>(Convert.ToInt32(item.StatusId));
                    if (!isEnumDefined)
                    {
                        smartAppException.AddData("ERROR.90130");
                        throw SmartAppUtil.AddStackTrace(smartAppException);
                    }
                    IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                    DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(item.StatusId);
                    creditAppRequestTable.DocumentStatusGUID = documentStatus.DocumentStatusGUID;

                }
                if (creditAppRequestTable.ProcessInstanceId == 0)
                {
                    creditAppRequestTable.ProcessInstanceId = item.ProcesInstanceId;
                }
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                creditAppRequestTableService.UpdateCreditAppRequestTable(creditAppRequestTable);
                UnitOfWork.Commit();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion LIT1599 API Update credit application request K2
        #region LIT1600 API Approve credit application request K2
        public void ApproveCreditApplicationRequestK2(ApproveCreditApplicationRequestK2ParmView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(item.ParmDocGUID.StringToGuid());
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                List<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID)
                    .Where(w => w.ApprovalDecision == (int)ApprovalDecision.Approved).ToList();
                ValidateApproveCreditApplicationRequestK2(creditAppRequestTable, creditAppRequestLines);
                // Passed validation 
                bool isMainCreditLimit = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit);
                bool isBuyerMatching = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching);
                bool isLoanRequest = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest);
                bool isFactoring = (creditAppRequestTable.ProductType == (int)ProductType.Factoring);
                ICompanyParameterRepo CompanyParameterRepo = new CompanyParameterRepo(db);
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                IOwnerTransService ownerTransService = new OwnerTransService(db);
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
                IBuyerAgreementTransService buyerAgreementTransService = new BuyerAgreementTransService(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CompanyParameter companyParameter = CompanyParameterRepo.GetCompanyParameterByCompanyNoTracking(item.CompanyGUID.StringToGuid());
                CreditAppTable creditAppTable = new CreditAppTable();
                CreditAppTable refCreditAppTable = new CreditAppTable(); // emppty model
                List<ServiceFeeConditionTrans> allServiceFeeConditionTrans = new List<ServiceFeeConditionTrans>();
                List<AuthorizedPersonTrans> authorizedPersonTranses = new List<AuthorizedPersonTrans>();
                List<GuarantorTrans> guarantorTranses = new List<GuarantorTrans>();
                List<OwnerTrans> ownerTranses = new List<OwnerTrans>();
                List<RetentionConditionTrans> retentionConditionTranses = new List<RetentionConditionTrans>();
                List<DocumentConditionTrans> documentConditionTrans = new List<DocumentConditionTrans>();
                List<BuyerAgreementTrans> buyerAgreementTranses = new List<BuyerAgreementTrans>();
                Guid creditAppTableGUID = (isMainCreditLimit) ? creditAppTable.CreditAppTableGUID // CreditAppTable from 2.1
                                                              : (isBuyerMatching || isLoanRequest)
                                                                ? creditAppRequestTable.RefCreditAppTableGUID.Value
                                                                : new Guid();
                #region 1. Create Credit application case Main credit limit
                if (isMainCreditLimit)
                {
                    #region 1.1 Create CreditAppTable
                    IInterestTypeService interestTypeService = new InterestTypeService(db);
                    decimal interestValue = interestTypeService.GetInterestTypeValue(creditAppRequestTable.InterestTypeGUID, creditAppRequestTable.RequestDate);
                    ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                    creditAppTableGUID = Guid.NewGuid();
                    creditAppTable.CreditAppTableGUID = creditAppTableGUID;
                    creditAppTable.Description = creditAppRequestTable.Description;
                    creditAppTable.ProductType = creditAppRequestTable.ProductType;
                    creditAppTable.ProductSubTypeGUID = creditAppRequestTable.ProductSubTypeGUID;
                    creditAppTable.CreditLimitTypeGUID = creditAppRequestTable.CreditLimitTypeGUID;
                    creditAppTable.ApprovedDate = creditAppRequestTable.ApprovedDate.HasValue ? creditAppRequestTable.ApprovedDate.Value : DateTime.MaxValue; // by business ApprovedDate must not null
                    creditAppTable.ExpectedAgreementSigningDate = null;
                    creditAppTable.DocumentReasonGUID = null;
                    creditAppTable.RefCreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID;
                    creditAppTable.Remark = string.Empty;
                    creditAppTable.CustomerTableGUID = creditAppRequestTable.CustomerTableGUID;
                    creditAppTable.ConsortiumTableGUID = creditAppRequestTable.ConsortiumTableGUID;
                    creditAppTable.RegisteredAddressGUID = creditAppRequestTable.RegisteredAddressGUID;
                    creditAppTable.SalesAvgPerMonth = creditAppRequestTable.SalesAvgPerMonth;
                    creditAppTable.BankAccountControlGUID = creditAppRequestTable.BankAccountControlGUID;
                    creditAppTable.PDCBankGUID = creditAppRequestTable.PDCBankGUID;
                    creditAppTable.ApprovedCreditLimit = creditAppRequestTable.ApprovedCreditLimitRequest;
                    creditAppTable.CreditLimitExpiration = creditAppRequestTable.CreditLimitExpiration;
                    creditAppTable.StartDate = (creditAppRequestTable.StartDate != null) ? creditAppRequestTable.StartDate.Value : creditAppRequestTable.ApprovedDate.Value; // by business ApprovedDate must not null
                    creditAppTable.ExpiryDate = (creditAppRequestTable.CreditLimitExpiration == (int)CreditLimitExpiration.BySpecific) ? (creditAppRequestTable.ExpiryDate.HasValue ? creditAppRequestTable.ExpiryDate.Value : DateTime.MaxValue) : DateTime.MaxValue; // by business ExpiryDate must not null
                    creditAppTable.CreditLimitRemark = creditAppRequestTable.CreditLimitRemark;
                    creditAppTable.InactiveDate = null;
                    creditAppTable.CreditRequestFeePct = creditAppRequestTable.CreditRequestFeePct;
                    creditAppTable.InterestTypeGUID = creditAppRequestTable.InterestTypeGUID;
                    creditAppTable.InterestAdjustment = creditAppRequestTable.InterestAdjustment;
                    creditAppTable.TotalInterestPct = creditAppRequestTable.InterestAdjustment + interestValue;
                    creditAppTable.CACondition = creditAppRequestTable.CACondition;
                    creditAppTable.BillingContactPersonGUID = creditAppRequestTable.BillingContactPersonGUID;
                    creditAppTable.BillingAddressGUID = creditAppRequestTable.BillingAddressGUID;
                    creditAppTable.ReceiptContactPersonGUID = creditAppRequestTable.ReceiptContactPersonGUID;
                    creditAppTable.ReceiptAddressGUID = creditAppRequestTable.ReceiptAddressGUID;
                    creditAppTable.InvoiceAddressGUID = creditAppRequestTable.InvoiceAddressGUID;
                    creditAppTable.MailingReceipAddressGUID = creditAppRequestTable.MailingReceiptAddressGUID;
                    creditAppTable.MaxPurchasePct = creditAppRequestTable.MaxPurchasePct;
                    creditAppTable.MaxRetentionPct = creditAppRequestTable.MaxRetentionPct;
                    creditAppTable.MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount;
                    creditAppTable.PurchaseFeePct = creditAppRequestTable.PurchaseFeePct;
                    creditAppTable.PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase;
                    creditAppTable.CreditTermGUID = creditAppRequestTable.CreditTermGUID;
                    creditAppTable.ExtensionServiceFeeCondTemplateGUID = creditAppRequestTable.ExtensionServiceFeeCondTemplateGUID;
                    creditAppTable.ApplicationTableGUID = creditAppRequestTable.ApplicationTableGUID;
                    creditAppTable.Dimension1GUID = creditAppRequestTable.Dimension1GUID;
                    creditAppTable.Dimension2GUID = creditAppRequestTable.Dimension2GUID;
                    creditAppTable.Dimension3GUID = creditAppRequestTable.Dimension3GUID;
                    creditAppTable.Dimension4GUID = creditAppRequestTable.Dimension4GUID;
                    creditAppTable.Dimension5GUID = creditAppRequestTable.Dimension5GUID;
                    creditAppTable.AssignmentMethodGUID = creditAppRequestTable.AssignmentMethodGUID;
                    creditAppTable.PurchaseWithdrawalCondition = creditAppRequestTable.PurchaseWithdrawalCondition;
                    creditAppTable.CompanyGUID = item.CompanyGUID.StringToGuid();
                    creditAppTable.Owner = item.Owner;
                    creditAppTable.OwnerBusinessUnitGUID = item.OwnerBusinessUnitGUID.StringToGuid();
                    creditAppTableService.GenCreditAppNumberSeqCode(creditAppTable);
                    #endregion 1.1 Create CreditAppTable
                    #region 1.2 CopyAuthorizedPersonTrans
                    authorizedPersonTranses.AddTo<AuthorizedPersonTrans>(authorizedPersonTransService
                       .CopyAuthorizedPersonTrans(creditAppRequestTable.CreditAppRequestTableGUID,
                                 creditAppTable.CreditAppTableGUID,
                                 (int)RefType.CreditAppRequestTable,
                                 (int)RefType.CreditAppTable, false, false,
                                 item.Owner,
                                 item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    #endregion 1.2 CopyAuthorizedPersonTrans
                    #region 1.3 CopyGuarantorTrans
                    guarantorTranses.AddTo<GuarantorTrans>(guarantorTransService
                       .CopyGuarantorTrans(creditAppRequestTable.CreditAppRequestTableGUID,
                                 creditAppTable.CreditAppTableGUID,
                                 (int)RefType.CreditAppRequestTable,
                                 (int)RefType.CreditAppTable, false, false,
                                 item.Owner,
                                 item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    #endregion 1.3 CopyGuarantorTrans
                    #region 1.4 CopyOwnerTrans
                    ownerTranses.AddTo<OwnerTrans>(ownerTransService
                       .CopyOwnerTrans(creditAppRequestTable.CreditAppRequestTableGUID,
                                 creditAppTable.CreditAppTableGUID,
                                 (int)RefType.CreditAppRequestTable,
                                 (int)RefType.CreditAppTable, false,
                                 item.Owner,
                                 item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    #endregion 1.4 CopyOwnerTrans
                    #region 1.5 CopyRetentionConditionTrans
                    retentionConditionTranses.AddTo<RetentionConditionTrans>(retentionConditionTransService
                        .CopyRetentionConditionTrans(creditAppRequestTable.CreditAppRequestTableGUID,
                                  creditAppTable.CreditAppTableGUID,
                                  (int)RefType.CreditAppRequestTable,
                                  (int)RefType.CreditAppTable,
                                  item.Owner,
                                  item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    #endregion 1.5 CopyRetentionConditionTrans
                }
                else
                {
                    refCreditAppTable = creditAppTableRepo.Find(creditAppRequestTable.RefCreditAppTableGUID.Value);
                }
                #endregion 1. Create Credit application case Main credit limit
                #region 2. Create Credit application line
                #region 2.1 reate CreditAppLine
                List<CreditAppLine> RefCreditAppLines = new List<CreditAppLine>();
                if(!isMainCreditLimit)
                {
                    ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                    RefCreditAppLines = creditAppLineRepo.GetCreditAppLineByCreditAppIdNoTracking(creditAppRequestTable.RefCreditAppTableGUID.Value).ToList();
                }
                List<CreditAppLine> CreditAppLines = new List<CreditAppLine>();
                if (creditAppRequestLines.Any())
                {
                    DateTime? reviewDate = (isFactoring) ? creditAppRequestTable.ApprovedDate : DateTime.MaxValue;
                    DateTime expiryDate = (isFactoring) ? (creditAppRequestTable.ApprovedDate.Value.AddDays(companyParameter.BuyerReviewedDay)) : DateTime.MaxValue;
                    int ordering = (isMainCreditLimit) ? 1 : RefCreditAppLines.Max(m => m.LineNum) + 1;
                    foreach (CreditAppRequestLine line in creditAppRequestLines)
                    {
                        Guid creditAppLineGUID = Guid.NewGuid();
                        CreditAppLines.Add(new CreditAppLine
                        {
                            CreditAppLineGUID = creditAppLineGUID,
                            CreditAppTableGUID = creditAppTableGUID,
                            LineNum = ordering,
                            ReviewDate = reviewDate,
                            ExpiryDate = expiryDate,
                            RefCreditAppRequestLineGUID = line.CreditAppRequestLineGUID,
                            BuyerTableGUID = line.BuyerTableGUID,
                            CreditTermGUID = line.BuyerCreditTermGUID,
                            CreditTermDescription = line.CreditTermDescription,
                            BillingDay = line.BillingDay,
                            BillingDescription = line.BillingDescription,
                            BillingAddressGUID = line.BillingAddressGUID,
                            BillingContactPersonGUID = line.BillingContactPersonGUID,
                            BillingResponsibleByGUID = line.BillingResponsibleByGUID,
                            MethodOfBilling = line.MethodOfBilling,
                            BillingRemark = line.BillingRemark,
                            ReceiptDay = line.ReceiptDay,
                            ReceiptDescription = line.ReceiptDescription,
                            ReceiptAddressGUID = line.ReceiptAddressGUID,
                            ReceiptContactPersonGUID = line.ReceiptContactPersonGUID,
                            MethodOfPaymentGUID = line.MethodOfPaymentGUID,
                            ReceiptRemark = line.ReceiptRemark,
                            InvoiceAddressGUID = line.InvoiceAddressGUID,
                            MailingReceiptAddressGUID = line.MailingReceiptAddressGUID,
                            ApprovedCreditLimitLine = line.ApprovedCreditLimitLineRequest,
                            InsuranceCreditLimit = line.InsuranceCreditLimit,
                            AssignmentMethodGUID = line.AssignmentMethodGUID,
                            AcceptanceDocument = line.AcceptanceDocument,
                            AcceptanceDocumentDescription = line.AcceptanceDocumentDescription,
                            AssignmentMethodRemark = line.AssignmentMethodRemark,
                            PaymentCondition = line.PaymentCondition,
                            MaxPurchasePct = line.MaxPurchasePct,
                            PurchaseFeePct = line.PurchaseFeePct,
                            PurchaseFeeCalculateBase = line.PurchaseFeeCalculateBase,
                            AssignmentAgreementTableGUID = line.AssignmentAgreementTableGUID,
                            LineCondition = line.LineCondition,
                            Owner = item.Owner,
                            OwnerBusinessUnitGUID = item.OwnerBusinessUnitGUID.StringToGuid(),
                            CompanyGUID = item.CompanyGUID.StringToGuid()
                        });
                        ordering++;
                        #region 2.2 CopyServiceFeeConditionTrans
                        allServiceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService
                            .CopyServiceFeeConditionTrans(line.CreditAppRequestLineGUID,
                                                          creditAppLineGUID,
                                                          (int)RefType.CreditAppRequestLine,
                                                          (int)RefType.CreditAppLine, false,
                                                          item.Owner,
                                                          item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                        #endregion 2.2 CopyServiceFeeConditionTrans
                        #region 2.3 CopyDocumentConditionTrans
                        documentConditionTrans.AddTo<DocumentConditionTrans>(documentConditionTransService
                            .CopyDocumentConditionTrans(new DocConVerifyType[] { DocConVerifyType.Billing, DocConVerifyType.Receipt },
                                                          line.CreditAppRequestLineGUID,
                                                          creditAppLineGUID,
                                                          (int)RefType.CreditAppRequestLine,
                                                          (int)RefType.CreditAppLine, false, false,
                                                          item.Owner,
                                                          item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                        #endregion 2.3 CopyDocumentConditionTrans
                        #region 2.4 CopyBuyerAgreementTrans
                        buyerAgreementTranses.AddTo<BuyerAgreementTrans>(buyerAgreementTransService
                            .CopyBuyerAgreementTrans(line.CreditAppRequestLineGUID,
                                                          creditAppLineGUID,
                                                          (int)RefType.CreditAppRequestLine,
                                                          (int)RefType.CreditAppLine,
                                                          item.Owner,
                                                          item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                        #endregion 2.4 CopyBuyerAgreementTrans
                    }
                }
                #endregion 2.1 Create CreditAppLine
                #endregion 2. Create Credit application line
                #region 3. สร้าง Service fee condition transaction ที่ผูกกับ Credit application table
                #region 3.1 GenServiceFeeCondCreditReqFeeByCAReq
                int? sftCreditAppTableOrdering = (int?)null;
                allServiceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService
                    .GenServiceFeeCondCreditReqFeeByCAReq(creditAppRequestTable.CreditAppRequestTableGUID,
                                                          item.Owner,
                                                          item.OwnerBusinessUnitGUID.StringToGuid()).FirstToList());
                if (allServiceFeeConditionTrans.Any(a => a.RefType == (int)RefType.CreditAppRequestTable))
                {
                    var serviceFeeConditionTransNotInDB = JsonConvert.SerializeObject(allServiceFeeConditionTrans.Where(w => w.RefType == (int)RefType.CreditAppRequestTable).ToList());
                    ServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
                    List<ServiceFeeConditionTrans> caServiceFeeConditionTrans = serviceFeeConditionTransRepo.GetServiceFeeConditionTransByRefernceNoTracking(creditAppTableGUID, (int)RefType.CreditAppTable).ToList();
                    int maxOrdering = caServiceFeeConditionTrans.Any() ? caServiceFeeConditionTrans.Max(m => m.Ordering) : 0;
                    foreach (var ser in JsonConvert.DeserializeObject<List<ServiceFeeConditionTrans>>(serviceFeeConditionTransNotInDB))
                    {
                        maxOrdering++;
                        ser.Ordering = maxOrdering;
                        ser.RefGUID = creditAppTableGUID;
                        ser.RefType = (int)RefType.CreditAppTable;
                        ser.ServiceFeeConditionTransGUID = Guid.NewGuid();
                        allServiceFeeConditionTrans.Add(ser);
                    }
                    sftCreditAppTableOrdering = allServiceFeeConditionTrans.Where(w => w.RefType == (int)RefType.CreditAppTable).Max(m => m.Ordering);
                }
                #endregion 3.1 GenServiceFeeCondCreditReqFeeByCAReq
                #region 3.2 CopyServiceFeeConditionTrans
                allServiceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService
                    .CopyServiceFeeConditionTrans(creditAppRequestTable.CreditAppRequestTableGUID,
                                                  creditAppTableGUID,
                                                  (int)RefType.CreditAppRequestTable,
                                                  (int)RefType.CreditAppTable, false,
                                                  item.Owner,
                                                  item.OwnerBusinessUnitGUID.StringToGuid(),
                                                  sftCreditAppTableOrdering).ToList());
                #endregion 3.2 CopyServiceFeeConditionTrans
                #endregion 3. สร้าง Service fee condition transaction ที่ผูกกับ Credit application table
                #region 4.Update Expiry date
                bool isByTransaction = (creditAppRequestTable.CreditLimitExpiration == (int)CreditLimitExpiration.ByTransaction);
                bool isByCreditApplication = (creditAppRequestTable.CreditLimitExpiration == (int)CreditLimitExpiration.ByCreditApplication);
                bool isByBuyerAgreement = (creditAppRequestTable.CreditLimitExpiration == (int)CreditLimitExpiration.ByBuyerAgreement);

                DateTime newExpiryDate = (isMainCreditLimit) ? creditAppTable.ExpiryDate
                                                             : refCreditAppTable.ExpiryDate;
                Guid? creditLimitTypeGUID = (isMainCreditLimit) ? creditAppTable.CreditLimitTypeGUID
                                                               : (isByTransaction || isByCreditApplication)
                                                                  ? refCreditAppTable.CreditLimitTypeGUID
                                                                  : (Guid?)null;
                if ((isByTransaction || isByCreditApplication) && isMainCreditLimit)
                {
                    ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                    CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditLimitTypeGUID.Value);
                    newExpiryDate = creditAppRequestTable.ApprovedDate.Value.AddDays(creditLimitType.CreditLimitExpiryDay);
                }
                if (isByBuyerAgreement)
                {
                    newExpiryDate = (isMainCreditLimit) ? buyerAgreementTransService.GetEndDate(buyerAgreementTranses)
                                                        : creditAppTableRepo.GetEndDateByCreditAppTableGUID(refCreditAppTable.CreditAppTableGUID);
                }
                if (isMainCreditLimit)
                {
                    creditAppTable.ExpiryDate = newExpiryDate;
                }
                else if (isBuyerMatching || isLoanRequest)
                {
                    refCreditAppTable.ExpiryDate = newExpiryDate;
                }
                #endregion 4.Update Expiry date
                #region 5.Update Credit application request status
                UpdateCreditApplicationRequestStatus(creditAppRequestTable, CreditAppRequestStatus.Approved);
                #endregion 5.Update Credit application request status
                #region Bulk Insert/Update
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    if (isMainCreditLimit)
                    {
                        BulkInsert(creditAppTable.FirstToList());
                    }
                    else
                    {
                        BulkUpdate(refCreditAppTable.FirstToList());
                    }
                    if (CreditAppLines.Any())
                    {
                        BulkInsert(CreditAppLines);
                    }
                    if (allServiceFeeConditionTrans.Any())
                    {
                        BulkInsert(allServiceFeeConditionTrans);
                    }
                    if (authorizedPersonTranses.Any())
                    {
                        BulkInsert(authorizedPersonTranses);
                    }
                    if (guarantorTranses.Any())
                    {
                        BulkInsert(guarantorTranses);
                    }
                    if (ownerTranses.Any())
                    {
                        BulkInsert(ownerTranses);
                    }
                    if (retentionConditionTranses.Any())
                    {
                        BulkInsert(retentionConditionTranses);
                    }
                    if (documentConditionTrans.Any())
                    {
                        BulkInsert(documentConditionTrans);
                    }
                    if (buyerAgreementTranses.Any())
                    {
                        BulkInsert(buyerAgreementTranses);
                    }
                    BulkUpdate(creditAppRequestTable.FirstToList());
                    UnitOfWork.Commit(transaction);
                }
                #endregion  Bulk Insert/Update
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateApproveCreditApplicationRequestK2(CreditAppRequestTable creditAppRequestTable, List<CreditAppRequestLine> creditAppRequestLines)
        {
            try
            {
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(creditAppRequestTable.CompanyGUID);
                CreditAppTable creditAppTable = creditAppTableRepo.Find(creditAppRequestTable.RefCreditAppTableGUID);
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                // creditAppRequestLines all row is ApprovalDecision.Approved 
                bool isMainCreditLimit = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit);
                bool isBuyerMatching = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching);
                bool isLoanRequest = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest);
                bool creditRequestFeeAmountIsZero = (creditAppRequestTable.CreditRequestFeeAmount > 0);
                bool creditReqInvRevenueTypeGUIDIsNull = (companyParameter.CreditReqInvRevenueTypeGUID == null);
                if (creditAppRequestLines.Any())
                {
                    bool approvedCreditLimitLineRequestIsZero = creditAppRequestLines.Any(a => a.ApprovedCreditLimitLineRequest == 0);
                    if (approvedCreditLimitLineRequestIsZero)
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_LINE_REQUEST" });
                    }
                    if (isBuyerMatching)
                    {
                        ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                        List<CreditAppLineItemView> creditAppLines =
                        creditAppLineRepo.GetQueryForApproveCreditApplicationRequestK2(creditAppRequestTable.RefCreditAppTableGUID.Value).ToList();

                        foreach (var item in creditAppLines)
                        {
                            if (creditAppRequestLines.Any(a => a.BuyerTableGUID == item.BuyerTableGUID.StringToGuid()
                                                            && item.ExpiryDate.StringToDate() <= creditAppRequestTable.RequestDate))
                            {
                                smartAppException.AddData("ERROR.90052", new string[] { item.BuyerTable_Values, "LABEL.BUYER_ID" });
                            }
                        }
                    }
                }
                if (creditAppRequestTable.ApprovedCreditLimitRequest == 0)
                {
                    smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_REQUEST" });
                }
                if (creditAppRequestTable.ApprovedDate == null)
                {
                    smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVED_DATE" });
                }
                if (!ValidateApprovalDecision(creditAppRequestTable.CreditAppRequestTableGUID))
                {
                    smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVAL_DECISION" });
                }
                if (isMainCreditLimit && creditRequestFeeAmountIsZero && creditReqInvRevenueTypeGUIDIsNull)
                {
                    smartAppException.AddData("ERROR.90059", new string[] { "LABEL.COMPANY_PARAMETER", "LABEL.CREDIT_REQUEST_SERVICE_FEE_TYPE_ID" });
                }
                if (isLoanRequest)
                {
                    CreditAppRequestLine remainingLoanRequest = ValidateRemainingLoanRequest(creditAppRequestTable.CreditAppRequestTableGUID);
                    //                  TotalCreditLimitLineRequest > RemainingCreditLoanRequest
                    if (remainingLoanRequest.CreditLimitLineRequest > remainingLoanRequest.RemainingCreditLoanRequest)
                    {
                        smartAppException.AddData("ERROR.90075", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_REQUEST", "LABEL.REMAINING_CREDIT_LOAN_REQUEST", (remainingLoanRequest.RemainingCreditLoanRequest - creditAppRequestTable.ApprovedCreditLimitRequest).DecimalToString() });
                    }
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion LIT1600 API Approve credit application request K2
        #region LIT1607 API Approve amend customer credit limit K2
        public void ApproveAmendcustomerCreditLimitK2(ApproveAmendcustomerCreditLimitK2ParmView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(item.ParmDocGUID.StringToGuid());
                List<ServiceFeeConditionTrans> serviceFeeConditionTrans = new List<ServiceFeeConditionTrans>();
                ValidateApproveAmendcustomerCreditLimitK2(creditAppRequestTable);
                // Passed validation
                #region 1. Update credit application
                #region 1.1 Amend Credit application
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditAppTable refCreditAppTable = creditAppTableRepo.Find(creditAppRequestTable.RefCreditAppTableGUID.Value);
                CreditLimitType creditLimitType = creditLimitTypeRepo.Find(creditAppRequestTable.CreditLimitTypeGUID);
                bool isByTransaction = (creditLimitType.CreditLimitExpiration == (int)CreditLimitExpiration.ByTransaction);
                bool requestDateGreaterThanEqualToInactiveDate = (refCreditAppTable.InactiveDate.HasValue) ? creditAppRequestTable.RequestDate >= refCreditAppTable.InactiveDate.Value: false;
                DateTime? newInactiveDate = isByTransaction && requestDateGreaterThanEqualToInactiveDate
                                            ? creditAppRequestTable.ApprovedDate.Value.AddDays(creditLimitType.InactiveDay)
                                            : refCreditAppTable.InactiveDate;
                DateTime newExpiryDate = isByTransaction && requestDateGreaterThanEqualToInactiveDate
                            ? creditAppRequestTable.ApprovedDate.Value.AddDays(creditLimitType.CloseDay)
                            : refCreditAppTable.ExpiryDate;

                refCreditAppTable.MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount;
                refCreditAppTable.InactiveDate = newInactiveDate;
                refCreditAppTable.ExpiryDate = newExpiryDate;
                refCreditAppTable.ApprovedCreditLimit = creditAppRequestTable.ApprovedCreditLimitRequest;
                #endregion 1.1 Amend Credit application
                #region 1.2 Generate Service fee condition transactions
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                serviceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(
                    serviceFeeConditionTransService.GenServiceFeeCondCreditReqFeeByCAReq(
                                          creditAppRequestTable.CreditAppRequestTableGUID,
                                          item.Owner,
                                          item.OwnerBusinessUnitGUID.StringToGuid()).FirstToList());
                #endregion 1.2 Generate Service fee condition transactions
                #region 1.3	สร้าง Service fee condition transactions
                if (creditAppRequestTable.RefCreditAppTableGUID.HasValue)
                {
                    serviceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService
                    .CopyServiceFeeConditionTrans(creditAppRequestTable.CreditAppRequestTableGUID,
                    creditAppRequestTable.RefCreditAppTableGUID.Value,
                    (int)RefType.CreditAppRequestTable,
                    (int)RefType.CreditAppTable, false,
                    item.Owner,
                    item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                }
                #endregion 1.3	สร้าง Service fee condition transactions
                #endregion 1. Update credit application
                #region 2. Update Credit application request status
                UpdateCreditApplicationRequestStatus(creditAppRequestTable, CreditAppRequestStatus.Approved);
                #endregion 2. Update Credit application request status
                #region Bulk Insert/Update
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    if (serviceFeeConditionTrans.Any())
                    {
                        BulkInsert(serviceFeeConditionTrans);
                    }
                    BulkUpdate(refCreditAppTable.FirstToList());
                    BulkUpdate(creditAppRequestTable.FirstToList());
                    UnitOfWork.Commit(transaction);
                }
                #endregion  Bulk Insert/Update
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void ValidateApproveAmendcustomerCreditLimitK2(CreditAppRequestTable creditAppRequestTable)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(creditAppRequestTable.CompanyGUID);
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                bool isActiveAmendCustomerCreditLimit = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit);
                if (creditAppRequestTable.ApprovedCreditLimitRequest == 0)
                {
                    smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_REQUEST" });
                }
                if (creditAppRequestTable.ApprovedDate == null)
                {
                    smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVED_DATE" });
                }
                if (isActiveAmendCustomerCreditLimit && creditAppRequestTable.CreditRequestFeeAmount > 0 && companyParameter.CreditReqInvRevenueTypeGUID == null)
                {
                    smartAppException.AddData("ERROR.90059", new string[] { "LABEL.COMPANY_PARAMETER", "LABEL.CREDIT_REQUEST_SERVICE_FEE_TYPE_ID" });
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion LIT1607 API Approve amend customer credit limit K2
        #region LIT1608 API Approve amend customer information K2
        public void ApproveAmendCustomerInformationK2(ApproveAmendCustomerInformationK2ParmView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(item.ParmDocGUID.StringToGuid());
                ValidateApproveAmendCustomerInformationK2(creditAppRequestTable);
                // Passed validation
                #region 1.Update credit application
                #region 1.1 Amend Credit application
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                CreditAppTable refCreditAppTable = creditAppTableRepo.Find(creditAppRequestTable.RefCreditAppTableGUID.Value);
                CreditLimitType creditLimitType = creditLimitTypeRepo.Find(creditAppRequestTable.CreditLimitTypeGUID);
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo
                    .GetCreditAppRequestTableAmendByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID);
                List<ServiceFeeConditionTrans> serviceFeeConditionTrans = new List<ServiceFeeConditionTrans>();
                if (creditAppRequestTableAmend != null)
                {
                    if (creditAppRequestTableAmend.AmendRate)
                    {
                        IInterestTypeService interestTypeService = new InterestTypeService(db);
                        decimal interestValue = interestTypeService.GetInterestTypeValue(creditAppRequestTable.InterestTypeGUID, creditAppRequestTable.RequestDate);
                        refCreditAppTable.InterestTypeGUID = creditAppRequestTable.InterestTypeGUID;
                        refCreditAppTable.InterestAdjustment = creditAppRequestTable.InterestAdjustment;
                        refCreditAppTable.TotalInterestPct = creditAppRequestTable.InterestAdjustment + interestValue;
                        refCreditAppTable.MaxRetentionPct = creditAppRequestTable.MaxRetentionPct;
                        refCreditAppTable.MaxRetentionAmount = creditAppRequestTable.MaxRetentionAmount;
                        refCreditAppTable.MaxPurchasePct = creditAppRequestTable.MaxPurchasePct;
                        refCreditAppTable.PurchaseFeePct = creditAppRequestTable.PurchaseFeePct;
                        refCreditAppTable.PurchaseFeeCalculateBase = creditAppRequestTable.PurchaseFeeCalculateBase;
                    }
                }

                #endregion 1.1 Amend Credit application
                #region 1.2 Generate Service fee condition transactions
                IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                serviceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService.GenServiceFeeCondCreditReqFeeByCAReq(
                                          creditAppRequestTable.CreditAppRequestTableGUID,
                                          item.Owner,
                                          item.OwnerBusinessUnitGUID.StringToGuid()).FirstToList());
                #endregion 1.2 Generate Service fee condition transactions
                #region 1.3	สร้าง Service fee condition transactions
                if (creditAppRequestTable.RefCreditAppTableGUID.HasValue)
                {
                        serviceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService
                        .CopyServiceFeeConditionTrans(creditAppRequestTable.CreditAppRequestTableGUID,
                        creditAppRequestTable.RefCreditAppTableGUID.Value,
                        (int)RefType.CreditAppRequestTable,
                        (int)RefType.CreditAppTable, false,
                        item.Owner,
                        item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                }
                #endregion 1.3	สร้าง Service fee condition transactions
                #endregion 1. Update credit application
                #region 2. Update authorized person
                #region 2.1	Update authorized person ที่ถูก inactive
                IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
                List<AuthorizedPersonTrans> authorizedPersonTrans = new List<AuthorizedPersonTrans>();
                List<AuthorizedPersonTrans> refAuthorizedPersonTrans = new List<AuthorizedPersonTrans>();
                refAuthorizedPersonTrans.AddTo<AuthorizedPersonTrans>(authorizedPersonTransRepo
                .GetAuthorizedPersonTransByReference(creditAppRequestTable.CreditAppRequestTableGUID,
                                                    (int)RefType.CreditAppRequestTable)
                                                    .Where(w => w.InActive == true)
                                                    .ToList());
                if (refAuthorizedPersonTrans.Any())
                {
                    List<Guid> refAuthorizedPersonTransGUID = refAuthorizedPersonTrans.Where(w => w.RefAuthorizedPersonTransGUID.HasValue).Select(S => S.RefAuthorizedPersonTransGUID.Value).ToList();
                    refAuthorizedPersonTrans.Clear();
                    refAuthorizedPersonTrans = authorizedPersonTransRepo.GetAuthorizedPersonTransByRefAuthorizedPersonTrans(refAuthorizedPersonTransGUID).ToList();
                    if (refAuthorizedPersonTrans.Any())
                        refAuthorizedPersonTrans.ForEach(f => f.InActive = true);
                }
                #endregion 2.1	Update authorized person ที่ถูก inactive
                #region 2.2 Create authorized person Amend
                authorizedPersonTrans.AddTo<AuthorizedPersonTrans>(authorizedPersonTransService.CopyAuthorizedPersonTrans
                    (
                    creditAppRequestTable.CreditAppRequestTableGUID,
                    refCreditAppTable.CreditAppTableGUID,
                    (int)RefType.CreditAppRequestTable,
                    (int)RefType.CreditAppTable,
                    false, false,
                    item.Owner,
                    item.OwnerBusinessUnitGUID.StringToGuid()
                    ).Where(w => !w.RefAuthorizedPersonTransGUID.HasValue).ToList());
                #endregion 2.2 Create authorized person Amend
                #endregion 2. Update authorized person
                #region 3. Update GuarantorTrans
                #region 3.1	Update guarantor ที่ถูก inactive
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                IGuarantorTransService guarantorTransService = new GuarantorTransService(db);
                List<GuarantorTrans> guarantorTrans = new List<GuarantorTrans>();
                List<GuarantorTrans> refGuarantorTrans = new List<GuarantorTrans>();
                refGuarantorTrans.AddTo<GuarantorTrans>(guarantorTransRepo
                .GetGuarantorTransByReference(creditAppRequestTable.CreditAppRequestTableGUID,
                                              (int)RefType.CreditAppRequestTable)
                                              .Where(w => w.InActive == true).ToList());
                if (refGuarantorTrans.Any())
                {
                    List<Guid> refGuarantorTransGUID = refGuarantorTrans.Where(w => w.RefGuarantorTransGUID.HasValue).Select(S => S.RefGuarantorTransGUID.Value).ToList();
                    refGuarantorTrans.Clear();
                    refGuarantorTrans.AddTo<GuarantorTrans>(guarantorTransRepo.GetAuthorizedPersonTransByRefGuarantorTrans(refGuarantorTransGUID).ToList());
                    if (refGuarantorTrans.Any())
                        refGuarantorTrans.ForEach(f => f.InActive = true);
                }
                #endregion 3.1	Update guarantor ที่ถูก inactive
                #region 3.2 Create GuarantorTrans Amend
                guarantorTrans.AddTo<GuarantorTrans>(guarantorTransService.CopyGuarantorTrans
                    (
                    creditAppRequestTable.CreditAppRequestTableGUID,
                    refCreditAppTable.CreditAppTableGUID,
                    (int)RefType.CreditAppRequestTable,
                    (int)RefType.CreditAppTable,
                    false, false,
                    item.Owner,
                    item.OwnerBusinessUnitGUID.StringToGuid()
                    ).Where(w => !w.RefGuarantorTransGUID.HasValue).ToList());
                #endregion 3.2 Create GuarantorTrans Amend
                #endregion 3. Update GuarantorTrans
                #region 4. Update Credit application request status
                UpdateCreditApplicationRequestStatus(creditAppRequestTable, CreditAppRequestStatus.Approved);
                #endregion 4. Update Credit application request status
                #region Bulk Insert/Update
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    if (authorizedPersonTrans.Any())
                    {
                      BulkInsert(authorizedPersonTrans);
                    }
                    if (guarantorTrans.Any())
                    {
                      BulkInsert(guarantorTrans);
                    }
                    if (refAuthorizedPersonTrans.Any())
                    {
                        BulkUpdate(refAuthorizedPersonTrans);
                    }
                    if (refGuarantorTrans.Any())
                    {
                        BulkUpdate(refGuarantorTrans);
                    }
                    if (serviceFeeConditionTrans.Any())
                    {
                        BulkInsert(serviceFeeConditionTrans);
                    }
                    BulkUpdate(refCreditAppTable.FirstToList());
                    BulkUpdate(creditAppRequestTable.FirstToList());
                    UnitOfWork.Commit(transaction);
                }
                #endregion  Bulk Insert/Update
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void ValidateApproveAmendCustomerInformationK2(CreditAppRequestTable creditAppRequestTable)
        {
            SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
            if (!creditAppRequestTable.ApprovedDate.HasValue)
            {
                smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVED_DATE" });
            }
            if (smartAppException.Data.Count > 0)
            {
                throw SmartAppUtil.AddStackTrace(smartAppException);
            }
        }
        #endregion LIT1608 API Approve amend customer information K2
        #region LIT1609_API Amend customer buyer credit limit K2
        public void AmendCustomerBuyerCreditLimitK2(AmendCustomerBuyerCreditLimitK2ParmView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(item.ParmDocGUID.StringToGuid());
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                List<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID).ToList();

                ValidateAmendCustomerBuyerCreditLimitK2(creditAppRequestTable, creditAppRequestLines);
                // Passed validation
                List<ServiceFeeConditionTrans> serviceFeeConditionTrans = new List<ServiceFeeConditionTrans>();
                #region  1. Update credit application
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditAppTable refCreditAppTable = creditAppTableRepo.Find(creditAppRequestTable.RefCreditAppTableGUID.Value);
                CreditLimitType creditLimitType = creditLimitTypeRepo.Find(creditAppRequestTable.CreditLimitTypeGUID);
                #region 1.1 Amend Credit application
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo
                    .GetCreditAppRequestTableAmendByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID);
                #endregion 1.1 Update Credit application
                #region 1.2 Update Credit application line
                List<CreditAppLine> creditAppLines = new List<CreditAppLine>();
                if (creditAppRequestLines.Any())
                {
                    ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                    List<Guid> refCreditAppLineGUID = creditAppRequestLines.Where(w => w.RefCreditAppLineGUID.HasValue).Select(S => S.RefCreditAppLineGUID.Value).ToList();
                    List<CreditAppLine> tempCreditAppLines = creditAppLineRepo.GetCreditAppLineByRefCreditAppLine(refCreditAppLineGUID).ToList();
                    if (tempCreditAppLines.Any())
                    {
                        var tempData = (from creditAppLine in tempCreditAppLines
                                        join caReLine in creditAppRequestLines on creditAppLine.CreditAppLineGUID equals caReLine.RefCreditAppLineGUID
                                        select new { creditAppLine, caReLine });
                        foreach (var temp in tempData)
                        {
                            temp.creditAppLine.ApprovedCreditLimitLine = temp.caReLine.ApprovedCreditLimitLineRequest;
                            creditAppLines.Add(temp.creditAppLine);
                        }
                    }
                }
                #endregion 1.2 Update Credit application line
                #region 1.3 สร้าง Service fee condition transactions
                if (creditAppRequestLines.Any())
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                    foreach (CreditAppRequestLine caReqLine in creditAppRequestLines)
                    {
                        serviceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService
                        .CopyServiceFeeConditionTrans(caReqLine.CreditAppRequestLineGUID,
                        caReqLine.RefCreditAppLineGUID.Value,
                        (int)RefType.CreditAppRequestLine,
                        (int)RefType.CreditAppLine, false,
                        item.Owner,
                        item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    }
                }
                #endregion 1.3 สร้าง Service fee condition transactions
                #endregion 1.Update credit application
                #region 2.Update Credit application request status
                UpdateCreditApplicationRequestStatus(creditAppRequestTable, CreditAppRequestStatus.Approved);
                #endregion 2.Update Credit application request status
                #region Bulk Insert/Update
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    BulkUpdate(refCreditAppTable.FirstToList());
                    if (creditAppLines.Any())
                    {
                        BulkUpdate(creditAppLines);
                    }
                    BulkUpdate(creditAppRequestTable.FirstToList());
                    if (serviceFeeConditionTrans.Any())
                    {
                        BulkInsert(serviceFeeConditionTrans);
                    }
                    UnitOfWork.Commit(transaction);
                }
                #endregion  Bulk Insert/Update
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void ValidateAmendCustomerBuyerCreditLimitK2(CreditAppRequestTable creditAppRequestTable, List<CreditAppRequestLine> creditAppRequestLines)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                bool creditRequestFeeAmountIsZero = (creditAppRequestTable.CreditRequestFeeAmount > 0);
                if (creditAppRequestLines.Any())
                {
                    bool approvedCreditLimitLineRequestIsZero = creditAppRequestLines.Any(a => a.ApprovedCreditLimitLineRequest == 0);
                    if (approvedCreditLimitLineRequestIsZero)
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_LINE_REQUEST" });
                    }
                }
                if (creditAppRequestTable.ApprovedDate == null)
                {
                    smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVED_DATE" });
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion LIT1609_API Amend customer buyer credit limit K2
        #region LIT1610 API Approve amend buyer information K2
        public void ApproveAmendBuyerInformationK2(ApproveAmendBuyerInformationK2ParmView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(item.ParmDocGUID.StringToGuid());
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                List<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID).ToList();
                ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
                List<CreditAppRequestLineAmend> creditAppRequestLineAmends = creditAppRequestLineAmendRepo
                    .GetCreditAppRequestLineAmendByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID).ToList();
                IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
                IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
                List<DocumentConditionTrans> documentConditionTrans = new List<DocumentConditionTrans>();
                List<DocumentConditionTrans> refDocumentConditionTrans = new List<DocumentConditionTrans>();
                List<ServiceFeeConditionTrans> serviceFeeConditionTrans = new List<ServiceFeeConditionTrans>();
                ValidateApproveAmendBuyerInformationK2(creditAppRequestTable);
                // Passed validation
                #region 1. Amend buyer information
                #region 1.1 Update Credit application line
                List<CreditAppLine> creditAppLines = new List<CreditAppLine>();
                List<CreditAppRequestLine> amendReceiptDocumentCondition = new List<CreditAppRequestLine>();
                List<CreditAppRequestLine> amendBillingDocumentCondition = new List<CreditAppRequestLine>();
                if (creditAppRequestLines.Any())
                {
                    ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                    List<Guid> refCreditAppLineGUID = creditAppRequestLines.Where(w => w.RefCreditAppLineGUID.HasValue).Select(S => S.RefCreditAppLineGUID.Value).ToList();
                    List<CreditAppLine> tempCreditAppLines = creditAppLineRepo.GetCreditAppLineByRefCreditAppLine(refCreditAppLineGUID).ToList();
                    if (tempCreditAppLines.Any())
                    {
                        var tempData = (from creditAppLine in tempCreditAppLines
                                        join caReLine in creditAppRequestLines on creditAppLine.CreditAppLineGUID equals caReLine.RefCreditAppLineGUID
                                        join lineAmned in creditAppRequestLineAmends on caReLine.CreditAppRequestLineGUID equals lineAmned.CreditAppRequestLineGUID
                                        select new { creditAppLine, caReLine, lineAmned });
                        foreach (var temp in tempData)
                        {
                            if (temp.lineAmned.AmendRate)
                            {
                                temp.creditAppLine.MaxPurchasePct = temp.caReLine.MaxPurchasePct;
                                temp.creditAppLine.PurchaseFeePct = temp.caReLine.PurchaseFeePct;
                                temp.creditAppLine.PurchaseFeeCalculateBase = temp.caReLine.PurchaseFeeCalculateBase;
                            }
                            if (temp.lineAmned.AmendAssignmentMethod)
                            {
                                temp.creditAppLine.AssignmentMethodGUID = temp.caReLine.AssignmentMethodGUID;
                                temp.creditAppLine.AssignmentMethodRemark = temp.caReLine.AssignmentMethodRemark;
                            }
                            if (temp.lineAmned.AmendBillingInformation)
                            {
                                temp.creditAppLine.BillingAddressGUID = temp.caReLine.BillingAddressGUID;
                            }
                            if (temp.lineAmned.AmendReceiptInformation)
                            {
                                temp.creditAppLine.ReceiptAddressGUID = temp.caReLine.ReceiptAddressGUID;
                            }
                            if (temp.lineAmned.AmendReceiptDocumentCondition)
                            {
                                amendReceiptDocumentCondition.Add(temp.caReLine);
                            }
                            if (temp.lineAmned.AmendBillingDocumentCondition)
                            {
                                amendBillingDocumentCondition.Add(temp.caReLine);
                            }
                            creditAppLines.Add(temp.creditAppLine);
                        }
                    }
                }
                #endregion 1.1 Update Credit application line
                #endregion 1. Amend buyer information
                #region 2. Update Billing document condition transactions
                if (amendBillingDocumentCondition.Any())
                {
                    #region 2.1 Update billing document condition to inactive
                    List<Guid> creditAppRequestLineGUIDs = amendBillingDocumentCondition.Select(s => s.CreditAppRequestLineGUID).ToList();
                    refDocumentConditionTrans.AddTo<DocumentConditionTrans>(documentConditionTransRepo
                    .GetDocumentConditionTransByReference(creditAppRequestLineGUIDs,
                                    (int)RefType.CreditAppRequestLine)
                                    .Where(w => w.Inactive == true
                                             && w.DocConVerifyType == (int)DocConVerifyType.Billing)
                                    .ToList());
                    // add DocumentConditionTrans only next  Create in region find by RefDocumentConditionTrans 
                    #endregion 2.1 Update billing document condition to inactive
                    #region 2.2 Create billing document condition transactions Amend
                    foreach (CreditAppRequestLine amendBilling in amendBillingDocumentCondition.Where(w => w.RefCreditAppLineGUID.HasValue))
                    {
                        documentConditionTrans.AddTo<DocumentConditionTrans>(documentConditionTransService
                        .CopyDocumentConditionTrans(new DocConVerifyType[] { DocConVerifyType.Billing },
                         amendBilling.CreditAppRequestLineGUID,
                         amendBilling.RefCreditAppLineGUID.Value,
                         (int)RefType.CreditAppRequestLine,
                         (int)RefType.CreditAppLine, false, false,
                         item.Owner,
                         item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    }

                    #endregion 2.2 Create billing document condition transactions Amend
                }

                #endregion 2. Update Billing document condition transactions
                #region 3. Update Receipt document condition transactions
                if (amendReceiptDocumentCondition.Any())
                {
                    #region 3.1 Update receipt document condition to inactive
                    List<Guid> creditAppRequestLineGUIDs = amendReceiptDocumentCondition.Select(s => s.CreditAppRequestLineGUID).ToList();
                    refDocumentConditionTrans.AddTo<DocumentConditionTrans>(documentConditionTransRepo
                    .GetDocumentConditionTransByReference(creditAppRequestLineGUIDs,
                                    (int)RefType.CreditAppRequestLine)
                                    .Where(w => w.Inactive == true
                                             && w.DocConVerifyType == (int)DocConVerifyType.Receipt)
                                    .ToList());
                    // add DocumentConditionTrans only next  Create in region find by RefDocumentConditionTrans 
                    #endregion 3.1 Update receipt document condition to inactive
                    #region 3.2 Create receipt document condition transactions Amend
                    foreach (CreditAppRequestLine amendReceipt in amendReceiptDocumentCondition.Where(w => w.RefCreditAppLineGUID.HasValue))
                    {
                        documentConditionTrans.AddTo<DocumentConditionTrans>(documentConditionTransService
                        .CopyDocumentConditionTrans(new DocConVerifyType[] { DocConVerifyType.Receipt },
                         amendReceipt.CreditAppRequestLineGUID,
                         amendReceipt.RefCreditAppLineGUID.Value,
                         (int)RefType.CreditAppRequestLine,
                         (int)RefType.CreditAppLine, false, false,
                         item.Owner,
                         item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    }
                    #endregion 3.2 Create receipt document condition transactions Amend
                }

                #endregion 3. Update Receipt document condition transactions
                #region 4.	Update Service fee condition transactions

                if (creditAppRequestLines.Any())
                {
                    IServiceFeeConditionTransService serviceFeeConditionTransService = new ServiceFeeConditionTransService(db);
                    foreach (CreditAppRequestLine caReqLine in creditAppRequestLines)
                    {
                        serviceFeeConditionTrans.AddTo<ServiceFeeConditionTrans>(serviceFeeConditionTransService
                        .CopyServiceFeeConditionTrans(caReqLine.CreditAppRequestLineGUID,
                        caReqLine.RefCreditAppLineGUID.Value,
                        (int)RefType.CreditAppRequestLine,
                        (int)RefType.CreditAppLine, false,
                        item.Owner,
                        item.OwnerBusinessUnitGUID.StringToGuid()).ToList());
                    }
                }
                #endregion 4.	Update Service fee condition transactions
                #region find by RefDocumentConditionTrans 
                if (refDocumentConditionTrans.Any())
                {
                    List<Guid> refDocumentConditionTransGUID = refDocumentConditionTrans.Where(w => w.RefDocumentConditionTransGUID.HasValue).Select(S => S.RefDocumentConditionTransGUID.Value).ToList();
                    refDocumentConditionTrans = documentConditionTransRepo.GetDocumentConditionTransByRefDocumentConditionTrans(refDocumentConditionTransGUID).ToList();
                    if (refDocumentConditionTrans.Any())
                        refDocumentConditionTrans.ForEach(f => f.Inactive = true);
                }
                #endregion find by RefDocumentConditionTrans
                #region 5.Update Credit application request status
                UpdateCreditApplicationRequestStatus(creditAppRequestTable, CreditAppRequestStatus.Approved);
                #endregion 5.Update Credit application request status
                #region Bulk Insert/Update
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    if (creditAppLines.Any())
                    {
                        BulkUpdate(creditAppLines);
                    }
                    if (refDocumentConditionTrans.Any())
                    {
                        BulkUpdate(refDocumentConditionTrans);
                    }
                    if (documentConditionTrans.Any())
                    {
                        BulkInsert(documentConditionTrans);
                    }
                    if (serviceFeeConditionTrans.Any())
                    {
                        BulkInsert(serviceFeeConditionTrans);
                    }
                    BulkUpdate(creditAppRequestTable.FirstToList());
                    UnitOfWork.Commit(transaction);
                }
                #endregion  Bulk Insert/Update
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void ValidateApproveAmendBuyerInformationK2(CreditAppRequestTable creditAppRequestTable)
        {
            SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
            IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
            DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(creditAppRequestTable.DocumentStatusGUID);
            string[] listStatusId = new string[]
            {
                ((int)CreditAppRequestStatus.Draft).ToString()
            };
            bool hasStatusByCondition = listStatusId.Any(a => a == documentStatus.StatusId);
            if (creditAppRequestTable.ApprovedDate == null)
            {
                smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVED_DATE" });
            }
            if (!hasStatusByCondition)
            {
                smartAppException.AddData("ERROR.90012", new string[] { "LABEL.CREDIT_APPLICATION_REQUEST" });
            }
            if (smartAppException.Data.Count > 0)
            {
                throw SmartAppUtil.AddStackTrace(smartAppException);
            }
        }
        #endregion LIT1610 API Approve amend buyer information K2
        #region API Get credit application request email content K2
        public GetCreditApplicationRequestEmailContentK2ResultView GetCreditApplicationRequestEmailContentK2(GetCreditApplicationRequestEmailContentK2ParmView item)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetCreditApplicationRequestEmailContentK2(item.ParmDocGUID.StringToGuid(), item.ParmMarketingUser);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion API Get credit application request email content K2
        #endregion K2 Function       
        public AccessModeView GetAccessModeByCreditAppRequestLine(string amendLineCAId, string creditAppRequestTableGUID = null)
        {
            try
            {
                if (String.IsNullOrEmpty(creditAppRequestTableGUID) && !String.IsNullOrEmpty(amendLineCAId))
                {
                    ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                    ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
                    CreditAppRequestLineAmend creditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(amendLineCAId.StringToGuid());
                    CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByCreditAppRequestLine(creditAppRequestLineAmend.CreditAppRequestLineGUID);
                    creditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID.GuidNullToString();
                }
                AccessModeView accessModeView = GetAccessModeByCreditAppRequestTableStatusEqualWaitingForMarketingStaff(string.Empty, creditAppRequestTableGUID);
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateCreateLoanRequestOrBuyerMatchingCreditAppRequestTable(CreditAppRequestTable creditAppRequestTable, CreditAppTable creditAppTable)
        {
            try
            {
                if (!creditAppRequestTable.RefCreditAppTableGUID.HasValue)
                {
                    return;
                }
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IDocumentService documentService = new DocumentService(db);
                string processCreditAppRequestTable = ((int)DocumentProcessStatus.CreditAppRequestTable).ToString();
                string approved = ((int)CreditAppRequestStatus.Approved).ToString();
                List<Guid> documentStatuses = documentStatusRepo
                    .GetDocumentStatusByProcessId(processCreditAppRequestTable)
                    .Where(w => Convert.ToInt32(w.StatusId) < Convert.ToInt32(approved))
                    .Select(s => s.DocumentStatusGUID)
                    .ToList();
                IQueryable<CreditAppRequestTable> creditAppRequestTables = creditAppRequestTableRepo
                    .GetCreditAppRequestTableByRefCreditAppId(creditAppRequestTable.RefCreditAppTableGUID.Value)
                    .Where(w => w.CreditAppRequestType == creditAppRequestTable.CreditAppRequestType && documentStatuses.Contains(w.DocumentStatusGUID));
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                if (creditAppRequestTables.Any())
                {
                    string labelEnum = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest)
                        ? "ENUM.LOAN_REQUEST"
                        : "ENUM.BUYER_MATCHING";

                    smartAppException.AddData("ERROR.90093", new string[] { creditAppRequestTables.FirstOrDefault().CreditAppRequestId, labelEnum });
                }
                if (creditAppTable.InactiveDate.HasValue)
                {
                    if (creditAppRequestTable.RequestDate > creditAppTable.InactiveDate.Value)
                    {
                        smartAppException.AddData("ERROR.90026", new string[] { "LABEL.CREDIT_APPLICATION", SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description) });
                    }
                    else if (creditAppRequestTable.RequestDate > creditAppTable.ExpiryDate)
                    {
                        smartAppException.AddData("ERROR.90113", new string[] { "LABEL.CREDIT_APPLICATION", SystemStaticData.GetTranslatedMessage(((CreditAppRequestType)creditAppRequestTable.CreditAppRequestType).GetAttrCode()) });
                    }
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Shared
        public bool ValidateMarketingComment(Guid creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTableGUID);
                IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID)
                    .Where(w => string.IsNullOrWhiteSpace(w.MarketingComment));
                bool marketingCommentIsNullOrWhiteSpace = !string.IsNullOrWhiteSpace(creditAppRequestTable.MarketingComment);
                return marketingCommentIsNullOrWhiteSpace && !creditAppRequestLines.Any();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateCreditComment(Guid creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTableGUID);
                IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID)
                    .Where(w => string.IsNullOrWhiteSpace(w.CreditComment));
                bool creditCommentIsNullOrWhiteSpace = !string.IsNullOrWhiteSpace(creditAppRequestTable.CreditComment);
                return creditCommentIsNullOrWhiteSpace && !creditAppRequestLines.Any();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateApproverComment(Guid creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTableGUID);
                IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID)
                    .Where(w => string.IsNullOrWhiteSpace(w.ApproverComment));
                bool approverCommentIsNullOrWhiteSpace = !string.IsNullOrWhiteSpace(creditAppRequestTable.ApproverComment);
                return approverCommentIsNullOrWhiteSpace && !creditAppRequestLines.Any();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateApprovalDecision(Guid creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID)
                    .Where(w => w.ApprovalDecision == (int)ApprovalDecision.None);
                return !creditAppRequestLines.Any();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateApprovedCreditLimitLineRequest(Guid creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID)
                    .Where(w => w.ApprovedCreditLimitLineRequest == 0 && w.ApprovalDecision == (int)ApprovalDecision.Approved);
                return !creditAppRequestLines.Any();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateBuyerAgreement(Guid creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTableGUID);
                if(creditAppRequestTable != null)
                {
                   CreditLimitType creditLimitType = creditLimitTypeRepo.Find(creditAppRequestTable.CreditLimitTypeGUID);
                    if (creditLimitType.ValidateBuyerAgreement)
                    {
                        ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                        IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo
                            .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID);
                        if (creditAppRequestLines.Any())
                        {
                            IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                            IEnumerable<BuyerAgreementTrans> buyerAgreementTrans = buyerAgreementTransRepo
                                .GetBuyerAgreementTransCaReqLineByCaReqIdNoTracking(creditAppRequestTableGUID);
                            return buyerAgreementTrans.Any();
                        }
                        return true;
                    }
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Shared
        #region WorkFlow
        public WorkFlowCreditAppRequestView GetWorkFlowCreditAppRequestById(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                CreditAppRequestTableItemView item = creditAppRequestTableRepo.GetByIdwf(id.StringToGuid());
                string userId = db.GetUserId();
                WorkFlowCreditAppRequestView workFlowCreditAppRequestView = new WorkFlowCreditAppRequestView();
                workFlowCreditAppRequestView.CompanyGUID = item.CompanyGUID;
                workFlowCreditAppRequestView.CompanyId = item.CompanyId;
                workFlowCreditAppRequestView.Owner = item.Owner;
                workFlowCreditAppRequestView.OwnerBusinessUnitGUID = item.OwnerBusinessUnitGUID;
                workFlowCreditAppRequestView.OwnerBusinessUnitId = item.OwnerBusinessUnitId;
                workFlowCreditAppRequestView.CreatedBy = item.CreatedBy;
                workFlowCreditAppRequestView.CreatedDateTime = item.CreatedDateTime;
                workFlowCreditAppRequestView.ModifiedBy = item.ModifiedBy;
                workFlowCreditAppRequestView.ModifiedDateTime = item.ModifiedDateTime;
                workFlowCreditAppRequestView.CreditAppRequestTableGUID = item.CreditAppRequestTableGUID;
                workFlowCreditAppRequestView.CreditAppRequestId = item.CreditAppRequestId;
                workFlowCreditAppRequestView.Description = item.Description;
                workFlowCreditAppRequestView.CustomerTable_Values = item.CustomerTable_Values;
                workFlowCreditAppRequestView.DocumentStatus_StatusId = item.DocumentStatus_StatusId;
                workFlowCreditAppRequestView.CreditAppRequestType = item.CreditAppRequestType;
                workFlowCreditAppRequestView.ProductType = item.ProductType;
                workFlowCreditAppRequestView.RefCreditAppTableGUID = item.RefCreditAppTableGUID;
                workFlowCreditAppRequestView.ProcessInstanceId = item.ProcessInstanceId;
                workFlowCreditAppRequestView.MarketingHead = employeeTableService.GetReportToUsernamedByUserId(userId, workFlowCreditAppRequestView.CompanyGUID);
                workFlowCreditAppRequestView.ParmProductType = ConditionService.EnumToList<ProductType>(true)
                                                                               .Find(f => f.code == workFlowCreditAppRequestView.ProductType).status;
                // for ParmFolio
                string enumLabel = SystemStaticData.GetTranslatedMessage(((CreditAppRequestType)item.CreditAppRequestType)
                                                   .GetAttrCode());
                workFlowCreditAppRequestView.ParmFolio = item.UnboundInvoiceAddress
                                                             .Replace("GetTranslatedMessage", enumLabel);

                workFlowCreditAppRequestView.CustomerEmail = item.CustomerEmail;

                // row version
                workFlowCreditAppRequestView.RowVersion = item.RowVersion;
                return workFlowCreditAppRequestView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> ValidateAndStartWorkflow(WorkFlowCreditAppRequestView item)
        {
            try
            {
                ValidateWorkflow(item, true);
                IK2Service k2Service = new K2Service(db);
                return await k2Service.StartWorkflow(item.workflowInstance);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> ValidateAndActionWorkflow(WorkFlowCreditAppRequestView item)
        {
            try
            {
                ValidateWorkflow(item, false);
                IK2Service k2Service = new K2Service(db);
                return await k2Service.ActionWorkflow(item.workflowInstance);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateWorkflow(WorkFlowCreditAppRequestView item, bool isStartWorkflow)
        {
            try
            {
                #region New Instance Repo & Service
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                #endregion New Instance Repo & Service
                #region Variable
                #region Get From Database
                Guid creditAppRequestTableGUID = item.CreditAppRequestTableGUID.StringToGuid();
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(item.CreditAppRequestTableGUID.StringToGuid());
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking((creditAppRequestTable.DocumentStatusGUID));
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(creditAppRequestTable.CompanyGUID);
                CreditAppRequestTableAmend creditAppRequestTableAmend = new CreditAppRequestTableAmend();
                List<CreditAppRequestLine> creditAppRequestLines = new List<CreditAppRequestLine>();
                #endregion Get From Database

                // check row verion
                creditAppRequestTable.CheckRowVersion(item);

                #region  Set CreditAppRequestType
                bool isMainCreditLimit = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.MainCreditLimit);
                bool isBuyerMatching = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.BuyerMatching);
                bool isLoanRequest = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.LoanRequest);
                bool isActiveAmendCustomerCreditLimit = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit);
                bool isAmendCustomerInfo = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerInfo);
                bool isAmendCustomerBuyerCreditLimit = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit);
                bool isAmendBuyerInfo = (creditAppRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendBuyerInfo);
                #endregion Set CreditAppRequestType
                #region Set All Status
                int status = Convert.ToInt32(documentStatus.StatusId);
                bool isDraft = (status == (int)CreditAppRequestStatus.Draft); // 100
                bool isWaitingForMarketingStaff = (status == (int)CreditAppRequestStatus.WaitingForMarketingStaff); // 110
                bool isWaitingForCreditStaff = (status == (int)CreditAppRequestStatus.WaitingForCreditStaff); // 130
                bool isWaitingForCreditHead = (status == (int)CreditAppRequestStatus.WaitingForCreditHead); // 140
                bool isWaitingForAssistMD = (status == (int)CreditAppRequestStatus.WaitingForAssistMD); // 150
                bool isWaitingForBoard2Of3 = (status == (int)CreditAppRequestStatus.WaitingForBoard2Of3); // 160
                bool isWaitingForBoard3Of4 = (status == (int)CreditAppRequestStatus.WaitingForBoard3Of4); // 170
                int waitingForAssistMD = (int)CreditAppRequestStatus.WaitingForAssistMD; // 150
                int waitingForBoard3Of4 = (int)CreditAppRequestStatus.WaitingForBoard3Of4; // 170

                #endregion Set All Status
                #region Action Name
                bool actionIsResubmit = (item.workflowInstance.Action == "Resubmit");
                bool actionIsCancel = (item.workflowInstance.Action == "Cancel");
                bool actionIsSubmitToCredithead = (item.workflowInstance.Action == "Submit to credit head");
                bool actionIsSubmitToBoard2in3 = (item.workflowInstance.Action == "Submit to board 2in3");
                bool actionIsSubmitToBoard3in4 = (item.workflowInstance.Action == "Submit to board 3in4");
                bool actionIsSubmitToAssistMD = (item.workflowInstance.Action == "Submit to assist MD");
                bool actionIsApprove = (item.workflowInstance.Action == "Approve");
                bool actionIsReject = (item.workflowInstance.Action == "Reject");
                #endregion Action Name
                #region Set Variable For Validation
                if (isAmendCustomerInfo)
                {
                    ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                    creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByCreditAppRequestTable(creditAppRequestTableGUID);
                }
                if (isAmendCustomerBuyerCreditLimit || isAmendBuyerInfo)
                {
                    ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                    creditAppRequestLines = creditAppRequestLineRepo.GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID).ToList();
                }
                bool validateStatusIsNotDraft = (isStartWorkflow && !isDraft) && (isMainCreditLimit || isBuyerMatching || isLoanRequest || isActiveAmendCustomerCreditLimit || isAmendCustomerInfo || isAmendCustomerBuyerCreditLimit);
                bool validateCreditLimitRequestIsZero = (isStartWorkflow && isDraft) && (isMainCreditLimit || isBuyerMatching || isLoanRequest);
                bool validateReportToEmployeeTable = isStartWorkflow;
                bool validateCreditRequestFeeAmountIsGeaterZero = (isStartWorkflow && isDraft) && (isMainCreditLimit || isActiveAmendCustomerCreditLimit);
                bool ValidateMarketingComment_Shared = (isDraft || (isWaitingForMarketingStaff && (actionIsResubmit || actionIsCancel))) && (isMainCreditLimit || isBuyerMatching || isLoanRequest);
                bool ValidateMarketingComment_Head = (isDraft || (isWaitingForMarketingStaff && (actionIsResubmit || actionIsCancel))) && (isActiveAmendCustomerCreditLimit || isAmendCustomerInfo);
                bool ValidateMarketingComment_Line = (isDraft || (isWaitingForMarketingStaff && (actionIsResubmit || actionIsCancel))) && (isAmendCustomerBuyerCreditLimit || isAmendBuyerInfo);
                bool validateCustomerNotContact = ((isDraft || (isWaitingForMarketingStaff && actionIsResubmit)) && (isMainCreditLimit || isAmendCustomerInfo)) ||
                                                   (isDraft && (isBuyerMatching || isLoanRequest || isActiveAmendCustomerCreditLimit || isAmendCustomerBuyerCreditLimit));
                bool validateHasAuthorizedActive = (isDraft || (isWaitingForMarketingStaff && actionIsResubmit)) && (isMainCreditLimit || (isAmendCustomerInfo && creditAppRequestTableAmend.AmendAuthorizedPerson));
                bool validateCreditComment_Shared = !isStartWorkflow && ((isWaitingForCreditStaff && actionIsSubmitToCredithead) ||
                                                                         (isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4 || actionIsSubmitToAssistMD)))
                                                                     && (isMainCreditLimit || isBuyerMatching || isLoanRequest);
                bool validateCreditComment_Head = !isStartWorkflow && ((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4 || actionIsSubmitToAssistMD) && (isActiveAmendCustomerCreditLimit || isAmendCustomerInfo)) ||
                                                                      ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && ((actionIsApprove && isActiveAmendCustomerCreditLimit)) || (isAmendCustomerInfo && (actionIsApprove || actionIsReject))) ||
                                                                       (isWaitingForCreditStaff && (actionIsSubmitToCredithead || actionIsApprove) && isAmendCustomerInfo));
                bool validateCreditComment_Line = (!isStartWorkflow && ((isWaitingForCreditStaff && actionIsSubmitToCredithead) ||
                                                                       (isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4 || actionIsSubmitToAssistMD)))
                                                                       && isAmendCustomerBuyerCreditLimit)
                                                  || (isStartWorkflow && isAmendBuyerInfo);

                bool vlidateHasAssistMDAndUserIdIsNull = !isStartWorkflow && (isWaitingForCreditHead) && actionIsSubmitToAssistMD && (isMainCreditLimit || isBuyerMatching || isLoanRequest || isActiveAmendCustomerCreditLimit || isAmendCustomerInfo || isAmendCustomerBuyerCreditLimit);
                bool validateApproverComment_Shared = !isStartWorkflow && (((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                           ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove || actionIsReject)))
                                                                            && (isMainCreditLimit))
                                                                            || ((isBuyerMatching || isLoanRequest) && ((isWaitingForCreditHead && !actionIsSubmitToAssistMD) || (status >= waitingForAssistMD && status <= waitingForBoard3Of4))); ;
                bool validateApproverComment_Head = !isStartWorkflow && ((((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                          ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && actionIsApprove))
                                                                         && (isActiveAmendCustomerCreditLimit || isAmendCustomerInfo)) ||
                                                                         (isWaitingForCreditStaff && (actionIsSubmitToCredithead || actionIsApprove) && isAmendCustomerInfo));
                bool validateApproverComment_Line = (!isStartWorkflow && ((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                         ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove || actionIsReject)))
                                                                         && isAmendCustomerBuyerCreditLimit)
                                                    || (isStartWorkflow && isAmendBuyerInfo);
                bool validateApprovedCreditLimitRequestIsZero_Head = !isStartWorkflow && (((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                                          (status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove))
                                                                                           && (isMainCreditLimit || isActiveAmendCustomerCreditLimit))
                                                                                      || ((isBuyerMatching || isLoanRequest) && ((isWaitingForCreditHead && !actionIsSubmitToAssistMD) || (status >= waitingForAssistMD && status <= waitingForBoard3Of4)));
                bool validateApprovedCreditLimitRequestIsZero_Line = !isStartWorkflow && ((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                                         ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove)))
                                                                                         && isAmendCustomerBuyerCreditLimit;
                bool validateApprovedCreditLimitLineRequest_Shared = !isStartWorkflow && (((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                                         ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove)))
                                                                                         && (isMainCreditLimit))
                                                                                      || ((isBuyerMatching || isLoanRequest) && ((isWaitingForCreditHead && !actionIsSubmitToAssistMD) || (status >= waitingForAssistMD && status <= waitingForBoard3Of4)));
                bool validateApprovalDecision_Shared = !isStartWorkflow && (((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                            ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove || actionIsReject)))
                                                                            && (isMainCreditLimit))
                                                                            || ((isBuyerMatching || isLoanRequest) && ((isWaitingForCreditHead && !actionIsSubmitToAssistMD) || (status >= waitingForAssistMD && status <= waitingForBoard3Of4)));
                bool validateApprovedDateIsNull = (!isStartWorkflow && ((((isWaitingForCreditHead && (actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4)) ||
                                                                        ((status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove)))
                                                                          && (isMainCreditLimit || isActiveAmendCustomerCreditLimit || isAmendCustomerBuyerCreditLimit))) ||
                                                                     isAmendCustomerInfo && ((isWaitingForCreditStaff && (actionIsSubmitToCredithead || actionIsApprove)) ||
                                                                                             (isWaitingForCreditHead && (actionIsSubmitToAssistMD || actionIsSubmitToBoard2in3 || actionIsSubmitToBoard3in4 || actionIsApprove)) ||
                                                                                             (status >= waitingForAssistMD && status <= waitingForBoard3Of4) && (actionIsApprove)))
                                                  || (isStartWorkflow && isAmendBuyerInfo)
                                                  || ((isBuyerMatching || isLoanRequest) && ((isWaitingForCreditHead && !actionIsSubmitToAssistMD) || (status >= waitingForAssistMD && status <= waitingForBoard3Of4)));
                bool ValidateRemainingLoanRequest_Shared = isStartWorkflow && (isDraft) && isLoanRequest;
                bool ValidateInvoiceAddressGUIDIsNull = isMainCreditLimit && (isStartWorkflow || (!isStartWorkflow && isWaitingForMarketingStaff && actionIsResubmit));
                bool ValidateMailingReceiptAddressGUIDIsNull = isMainCreditLimit && (isStartWorkflow || (!isStartWorkflow && isWaitingForMarketingStaff && actionIsResubmit));
                bool ValidateBuyerAgreementTrans = isStartWorkflow || (isWaitingForMarketingStaff && actionIsResubmit && (isMainCreditLimit || isBuyerMatching || isLoanRequest));
                #endregion  Set Variable For Validation
                #endregion Variable
                #region Validate
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                if (validateStatusIsNotDraft)
                {
                    if (!isDraft)
                    {
                        smartAppException.AddData("ERROR.90012", "LABEL.CREDIT_APPLICATION_REQUEST");
                    }
                }
                if (validateCreditLimitRequestIsZero)
                {
                    if (creditAppRequestTable.CreditLimitRequest == 0)
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.CREDIT_LIMIT_REQUEST");
                    }
                }
                if (validateReportToEmployeeTable)
                {
                    if (string.IsNullOrWhiteSpace(item.MarketingHead))
                    {
                        string userId = db.GetUserId();
                        IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                        EmployeeTableItemView employeeTable = employeeTableRepo.GetEmployeeTableByUserIdAndCompany(userId, item.CompanyGUID);
                        smartAppException.AddData("ERROR.90096", SmartAppUtil.GetDropDownLabel(employeeTable.EmployeeId, employeeTable.Name));
                    }
                }
                if (validateCreditRequestFeeAmountIsGeaterZero)
                {
                    if (creditAppRequestTable.CreditRequestFeeAmount > 0 && !companyParameter.CreditReqInvRevenueTypeGUID.HasValue)
                    {
                        smartAppException.AddData("ERROR.90059", new string[] { "LABEL.COMPANY_PARAMETER", "LABEL.CREDIT_REQUEST_SERVICE_FEE_TYPE_ID" });
                    }
                }
                if (ValidateMarketingComment_Shared)
                {
                    if (!ValidateMarketingComment(creditAppRequestTable.CreditAppRequestTableGUID))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.MARKETING_COMMENT" });
                    }
                }
                if (ValidateMarketingComment_Head)
                {
                    if (string.IsNullOrWhiteSpace(creditAppRequestTable.MarketingComment))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.MARKETING_COMMENT" });
                    }
                }
                if (ValidateMarketingComment_Line)
                {
                    if (creditAppRequestLines.Any(a => string.IsNullOrWhiteSpace(a.MarketingComment)))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.MARKETING_COMMENT" });
                    }
                }
                if (validateCustomerNotContact)
                {
                    IContactTransRepo contactTransRepo = new ContactTransRepo(db);
                    IEnumerable<ContactTrans> contactTrans = contactTransRepo.GetPrimaryContactTransByEmailType(creditAppRequestTable.CustomerTableGUID, (int)RefType.Customer);
                    if (!contactTrans.Any())
                    {
                        smartAppException.AddData("ERROR.90140", new string[] { SystemStaticData.GetTranslatedMessage(ContactType.Email.GetAttrCode()), "LABEL.CUSTOMER_ID" });
                    }
                }
                if (validateHasAuthorizedActive)
                {
                    IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
                    IEnumerable<AuthorizedPersonTrans> authorizedPersonTrans = authorizedPersonTransRepo
                        .GetAuthorizedPersonTransByReference(creditAppRequestTable.CreditAppRequestTableGUID, (int)RefType.CreditAppRequestTable)
                        .Where(w => w.InActive == false);
                    if (!authorizedPersonTrans.Any())
                    {
                        smartAppException.AddData("ERROR.90139", new string[] { "LABEL.AUTHORIZED_PERSON" });
                    }
                }
                if (validateCreditComment_Shared)
                {
                    if (!ValidateCreditComment(creditAppRequestTable.CreditAppRequestTableGUID))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.CREDIT_COMMENT" });
                    }
                }
                if (validateCreditComment_Head)
                {
                    if (string.IsNullOrWhiteSpace(creditAppRequestTable.CreditComment))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.CREDIT_COMMENT" });
                    }
                }
                if (validateCreditComment_Line)
                {
                    if (creditAppRequestLines.Any(a => string.IsNullOrWhiteSpace(a.CreditComment)))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.CREDIT_COMMENT" });
                    }
                }
                if (vlidateHasAssistMDAndUserIdIsNull)
                {
                    if (item.workflowInstance.DataFields.Any(a => a.Name == "ParmAssistMD" && string.IsNullOrWhiteSpace(a.Value)))
                    {
                        IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                        EmployeeTable employeeTable = employeeTableRepo.Find(item.AssistMD.StringToGuid());
                        smartAppException.AddData("ERROR.90059", new string[] { "LABEL.USER_NAME", SmartAppUtil.GetDropDownLabel(employeeTable.EmployeeId, employeeTable.Name) });
                    }
                }
                if (validateApproverComment_Shared)
                {
                    if (!ValidateApproverComment(creditAppRequestTable.CreditAppRequestTableGUID))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVER_COMMENT" });
                    }
                }
                if (validateApproverComment_Head)
                {
                    if (string.IsNullOrWhiteSpace(creditAppRequestTable.ApproverComment))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVER_COMMENT" });
                    }
                }
                if (validateApproverComment_Line)
                {
                    if (creditAppRequestLines.Any(a => string.IsNullOrWhiteSpace(a.ApproverComment)))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVER_COMMENT" });
                    }
                }
                if (validateApprovedCreditLimitRequestIsZero_Head)
                {
                    if (creditAppRequestTable.ApprovedCreditLimitRequest == 0)
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_REQUEST" });
                    }
                }
                if (validateApprovedCreditLimitRequestIsZero_Line)
                {
                    if (creditAppRequestLines.Any(a => a.ApprovedCreditLimitLineRequest == 0))
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_REQUEST" });
                    }
                }
                if (validateApprovedCreditLimitLineRequest_Shared)
                {
                    if (!ValidateApprovedCreditLimitLineRequest(creditAppRequestTable.CreditAppRequestTableGUID))
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_LINE_REQUEST" });
                    }
                }
                if (validateApprovalDecision_Shared)
                {
                    if (!ValidateApprovalDecision(creditAppRequestTable.CreditAppRequestTableGUID))
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVAL_DECISION" });
                    }
                }
                if (validateApprovedDateIsNull)
                {
                    if (!creditAppRequestTable.ApprovedDate.HasValue)
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.APPROVED_DATE" });
                    }
                }
                if (ValidateRemainingLoanRequest_Shared)
                {
                    CreditAppRequestLine remainingLoanRequest = ValidateRemainingLoanRequest(creditAppRequestTable.CreditAppRequestTableGUID);
                    //                  TotalCreditLimitLineRequest > RemainingCreditLoanRequest
                    if (remainingLoanRequest.CreditLimitLineRequest > remainingLoanRequest.RemainingCreditLoanRequest)
                    {
                        smartAppException.AddData("ERROR.90075", new string[] { "LABEL.APPROVED_CREDIT_LIMIT_REQUEST", "LABEL.REMAINING_CREDIT_LOAN_REQUEST", (remainingLoanRequest.RemainingCreditLoanRequest - creditAppRequestTable.ApprovedCreditLimitRequest).DecimalToString() });
                    }
                }
                if (ValidateInvoiceAddressGUIDIsNull)
                {
                    if (!creditAppRequestTable.InvoiceAddressGUID.HasValue)
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.INVOICE_ADDRESS_ID" });
                    }
                }
                if (ValidateMailingReceiptAddressGUIDIsNull)
                {
                    if (!creditAppRequestTable.MailingReceiptAddressGUID.HasValue)
                    {
                        smartAppException.AddData("ERROR.00755", new string[] { "LABEL.MAILING_RECEIPT_ADDRESS_ID" });
                    }
                }
                if(ValidateBuyerAgreementTrans)
                {
                    if (!ValidateBuyerAgreement(creditAppRequestTable.CreditAppRequestTableGUID))
                    {
                        smartAppException.AddData("ERROR.90174");
                    }
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(smartAppException);
                }
                #endregion Validate
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion WorkFlow
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemCreditAppRequestStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.CreditAppRequest);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region ReviewCreditAppRequest
        public CreditAppRequestTableItemView CreateReviewCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                creditAppRequestTableView = accessLevelService.AssignOwnerBU(creditAppRequestTableView);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableView.ToCreditAppRequestTable();
                List<CAReqCreditOutStanding> caReqCreditOutStandings = new List<CAReqCreditOutStanding>();
                List<CAReqAssignmentOutstanding> caReqAssignmentOutstanding = new List<CAReqAssignmentOutstanding>();
                List<CAReqBuyerCreditOutstanding> caReqBuyerCreditOutstanding = new List<CAReqBuyerCreditOutstanding>();
                string userName = db.GetUserName();
                #region Create CreditAppRequestTable
                GenCreditAppRequestNumberSeqCode(creditAppRequestTable);
                creditAppRequestTable.MarketingComment = string.Empty;
                creditAppRequestTable.ApproverComment = string.Empty;
                creditAppRequestTable.CreditComment = string.Empty;
                creditAppRequestTable.CreditAppRequestTableGUID = Guid.NewGuid();
                #endregion Create CreditAppRequestTable
                #region Create CreditAppRequest Line
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine creditAppLine = creditAppLineRepo.Find(creditAppRequestTableView.RefCreditAppLineGUID.StringToGuid());
                CreditAppRequestLine creditAppRequestLine = new CreditAppRequestLine();
                creditAppRequestLine.CreditAppRequestLineGUID = Guid.NewGuid();
                creditAppRequestLine.CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID;
                creditAppRequestLine.LineNum = creditAppLine.LineNum;
                creditAppRequestLine.BuyerTableGUID = creditAppRequestTableView.BuyerTableGUID.StringToGuid();
                creditAppRequestLine.MethodOfBilling = creditAppLine.MethodOfBilling;
                creditAppRequestLine.CreditLimitLineRequest = creditAppRequestTableView.CreditLimitLineRequest;
                creditAppRequestLine.PurchaseFeeCalculateBase = creditAppLine.PurchaseFeeCalculateBase;
                creditAppRequestLine.ApprovalDecision = (int)ApprovalDecision.None;
                creditAppRequestLine.BuyerCreditLimit = creditAppRequestTableView.BuyerCreditLimit;
                creditAppRequestLine.CustomerBuyerOutstanding = creditAppRequestTableView.CustomerBuyerOutstanding;
                creditAppRequestLine.AllCustomerBuyerOutstanding = creditAppRequestTableView.CustomerAllBuyerOutstanding;
                creditAppRequestLine.MarketingComment = creditAppRequestTableView.MarketingComment;
                creditAppRequestLine.ApproverComment = creditAppRequestTableView.ApproverComment;
                creditAppRequestLine.CreditComment = creditAppRequestTableView.CreditComment;
                creditAppRequestLine.RefCreditAppLineGUID = creditAppRequestTableView.RefCreditAppLineGUID.StringToGuid();
                creditAppRequestLine.Owner = creditAppRequestTable.Owner;
                creditAppRequestLine.OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID;
                creditAppRequestLine.CompanyGUID = creditAppRequestTable.CompanyGUID;
                #endregion Create CreditAppRequest Line
                #region 4. Create Credit outstanding
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                List<CreditOutstandingViewMap> creditOustandingView = creditAppTableRepo.GetCreditOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID, creditAppRequestTable.RequestDate);
                if (creditOustandingView.Any())
                {
                    creditOustandingView.ForEach(f =>
                    {
                        caReqCreditOutStandings.Add(new CAReqCreditOutStanding
                        {
                            CAReqCreditOutStandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID,
                            ProductType = f.ProductType,
                            CreditLimitTypeGUID = f.CreditLimitTypeGUID,
                            AccumRetentionAmount = f.AccumRetentionAmount,
                            AsOfDate = f.AsOfDate,
                            CreditAppTableGUID = f.CreditAppTableGUID,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            ApprovedCreditLimit = f.ApprovedCreditLimit,
                            CreditLimitBalance = f.CreditLimitBalance,
                            ARBalance = f.ARBalance,
                            ReserveToBeRefund = f.ReserveToBeRefund,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                        });
                    });
                }
                #endregion 4. Create Credit outstanding
                #region 5. Create Assignment agreement outstanding 
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                List<AssignmentAgreementOutstandingView> assignmentAgreementOutstandingViews = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID);
                if (assignmentAgreementOutstandingViews.Any())
                {
                    assignmentAgreementOutstandingViews.ForEach(f =>
                    {
                        caReqAssignmentOutstanding.Add(new CAReqAssignmentOutstanding
                        {
                            CAReqAssignmentOutstandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID.StringToGuid(),
                            AssignmentAgreementTableGUID = f.AssignmentAgreementTableGUID.StringToGuid(),
                            BuyerTableGUID = f.BuyerTableGUID.StringToGuid(),
                            AssignmentAgreementAmount = f.AssignmentAgreementAmount,
                            SettleAmount = f.SettledAmount,
                            RemainingAmount = f.RemainingAmount,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                        });
                    });
                }
                #endregion 5. Create Assignment agreement outstanding 
                #region 6. Create CA buyer credit outstanding
                List<CALineOutstandingViewMap> caLineOutstandingViewMap = new List<CALineOutstandingViewMap>();
                if (creditAppRequestTable.RefCreditAppTableGUID.HasValue)
                {
                    caLineOutstandingViewMap = creditAppLineRepo.GetCALineOutstandingByCA((int)RefType.CreditAppTable, creditAppRequestTable.RefCreditAppTableGUID.Value, true, creditAppRequestTable.RequestDate).ToList();
                    if (caLineOutstandingViewMap.Any())
                    {
                        caLineOutstandingViewMap.ForEach(f =>
                        {
                            caReqBuyerCreditOutstanding.Add(new CAReqBuyerCreditOutstanding
                            {
                                CAReqBuyerCreditOutstandingGUID = Guid.NewGuid(),
                                CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                                CreditAppTableGUID = f.CreditAppTableGUID,
                                CreditAppLineGUID = f.CreditAppLineGUID,
                                LineNum = f.LineNum,
                                BuyerTableGUID = f.BuyerTableGUID,
                                Status = f.Status,
                                ApprovedCreditLimitLine = f.ApprovedCreditLimitLine,
                                ARBalance = f.ARBalance,
                                AssignmentMethodGUID = f.AssignmentMethodGUID,
                                BillingResponsibleByGUID = f.BillingResponsibleByGUID,
                                MethodOfPaymentGUID = f.MethodOfPaymentGUID,
                                ProductType = f.ProductType,
                                MaxPurchasePct = f.MaxPurchasePct,
                                CompanyGUID = creditAppRequestTable.CompanyGUID,
                                Owner = userName,
                                OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                            });
                        });
                    }
                }
                #endregion 6. Create CA buyer credit outstanding
                #region BulkInsert
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        BulkInsert(creditAppRequestTable.FirstToList());
                        BulkInsert(creditAppRequestLine.FirstToList());
                        if (caReqCreditOutStandings.Any())
                        {
                            BulkInsert(caReqCreditOutStandings);
                        }
                        if (caReqAssignmentOutstanding.Any())
                        {
                            BulkInsert(caReqAssignmentOutstanding);
                        }
                        if (caReqBuyerCreditOutstanding.Any())
                        {
                            BulkInsert(caReqBuyerCreditOutstanding);
                        }
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
                #endregion
                return creditAppRequestTable.ToCreditAppRequestTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView UpdateReviewCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableView.ToCreditAppRequestTable();
                CreditAppRequestTable dbCreditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestTable.CreditAppRequestTableGUID);
                #region Update CreditAppRequestTable
                dbCreditAppRequestTable.Description = creditAppRequestTable.Description;
                dbCreditAppRequestTable.RequestDate = creditAppRequestTable.RequestDate;
                dbCreditAppRequestTable.Remark = creditAppRequestTable.Remark;
                UpdateCreditAppRequestTable(dbCreditAppRequestTable);
                #endregion Update CreditAppRequestTable
                #region Update CreditAppRequest Line
                CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTable.CreditAppRequestTableGUID)
                    .FirstOrDefault();
                creditAppRequestLine.MarketingComment = creditAppRequestTableView.MarketingComment;
                creditAppRequestLine.ApproverComment = creditAppRequestTableView.ApproverComment;
                creditAppRequestLine.CreditComment = creditAppRequestTableView.CreditComment;
                UpdateCreditAppRequestLine(creditAppRequestLine);
                #endregion  Update CreditAppRequest Line
                UnitOfWork.Commit();
                return dbCreditAppRequestTable.ToCreditAppRequestTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteReviewCreditAppRequestLine(string item)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                Guid creditAppRequestTableGUID = new Guid(item);
                CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo
                    .GetCreditAppRequestLineByCreditAppRequestTable(creditAppRequestTableGUID)
                    .FirstOrDefault();
                creditAppRequestLineRepo.Remove(creditAppRequestLine);
                base.LogTransactionDelete<CreditAppRequestLine>(creditAppRequestLine);
                DeleteCreditAppRequestTable(item);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView GetReviewCreditAppRequestById(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetReviewByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region UpdateReviewResult
        public UpdateReviewResultView GetUpdateReviewResultById(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTableItemView creditAppRequestTable = creditAppRequestTableRepo.GetByIdvw(creditAppRequestTableGUID.StringToGuid());
                UpdateReviewResultView updateReviewResultView = new UpdateReviewResultView
                {
                    CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                    CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                    ReviewDate = DateTime.Now.DateToString()
                };
                return updateReviewResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetUpdateReviewResultValidation(UpdateReviewResultView UpdateReviewResultView)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(UpdateReviewResultView.CreditAppRequestTableGUID.StringToGuid());

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus creditAppRequestStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CreditAppRequestStatus.Draft).ToString());

                if (creditAppRequestTable.DocumentStatusGUID != creditAppRequestStatusDraft.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90012", new string[] { "LABEL.REVIEW_CREDIT_APPLICATION_REQUEST" });
                }

                if (UpdateReviewResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.None))
                {
                    ex.AddData("ERROR.90057", new string[] { "LABEL.REVIEW_RESULT" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public UpdateReviewResultResultView UpdateReviewResult(UpdateReviewResultView UpdateReviewResultView)
        {
            try
            {
                // validate
                GetUpdateReviewResultValidation(UpdateReviewResultView);
                UpdateReviewResultResultView result = new UpdateReviewResultResultView();
                NotificationResponse success = new NotificationResponse();

                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(UpdateReviewResultView.CreditAppRequestTableGUID.StringToGuid());
                CreditAppRequestTableItemView update_CreditAppRequestTable = creditAppRequestTableRepo.GetReviewByIdvw(UpdateReviewResultView.CreditAppRequestTableGUID.StringToGuid());

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus creditAppRequestApproved = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CreditAppRequestStatus.Approved).ToString());
                DocumentStatus creditAppRequestRejected = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CreditAppRequestStatus.Rejected).ToString());


                if (UpdateReviewResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.Approved))
                {
                    update_CreditAppRequestTable.DocumentStatusGUID = creditAppRequestApproved.DocumentStatusGUID.GuidNullToString();
                }
                else if (UpdateReviewResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.Rejected))
                {
                    update_CreditAppRequestTable.DocumentStatusGUID = creditAppRequestRejected.DocumentStatusGUID.GuidNullToString();
                }
                update_CreditAppRequestTable.DocumentReasonGUID = UpdateReviewResultView.DocumentReasonGUID;
                update_CreditAppRequestTable.ApprovedDate = UpdateReviewResultView.ReviewDate;


                if (update_CreditAppRequestTable.RefCreditAppLineGUID != null)
                {
                    ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                    CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(update_CreditAppRequestTable.RefCreditAppLineGUID.StringToGuidNull().Value);
                    CreditAppLineItemView update_CreditAppLineItemView = creditAppLineRepo.GetByIdvw(update_CreditAppRequestTable.RefCreditAppLineGUID.StringToGuidNull().Value);

                    ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                    CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(creditAppRequestTable.CompanyGUID);

                    if (UpdateReviewResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.Approved))
                    {
                        update_CreditAppLineItemView.ReviewDate = UpdateReviewResultView.ReviewDate;
                        if (UpdateReviewResultView.ReviewDate.StringToDate() <= creditAppLine.ExpiryDate.DateToString().StringToDate())
                        {
                            update_CreditAppLineItemView.ExpiryDate = (creditAppLine.ExpiryDate.AddDays(companyParameter.BuyerReviewedDay)).DateToString();
                        }else if(UpdateReviewResultView.ReviewDate.StringNullToDateNull() > creditAppLine.ExpiryDate)
                        {
                            update_CreditAppLineItemView.ExpiryDate = UpdateReviewResultView.ReviewDate.StringToDate().AddDays(companyParameter.BuyerReviewedDay).DateToString();
                        }
                    }
                    else if (UpdateReviewResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.Rejected))
                    {
                        update_CreditAppLineItemView.ReviewDate = UpdateReviewResultView.ReviewDate;
                    }
                    creditAppLineRepo.UpdateCreditAppLineVoid(creditAppLine, update_CreditAppLineItemView.ToCreditAppLine());
                }

                creditAppRequestTableRepo.UpdateCreditAppRequestTableVoid(creditAppRequestTable, update_CreditAppRequestTable.ToCreditAppRequestTable());
                UnitOfWork.Commit();
                success.AddData("SUCCESS.90009", new string[] { "LABEL.REVIEW_CREDIT_APPLICATION_REQUEST", SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description) });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion ReviewCreditAppRequest
        #region CloseCustomerCreditLimit
        public AccessModeView GetAccessModeLoanCloseCreditLimit(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                IDocumentService documentService = new DocumentService(db);
                Guid approved = documentService.GetDocumentStatusByStatusId((int)CreditAppRequestStatus.Approved).DocumentStatusGUID;
                var hasCloseCustomerCreditLimitIsApproved = creditAppRequestTableRepo
                    .GetCreditAppRequestTableByRefCreditAppId(id.StringToGuid())
                    .Any(a => a.DocumentStatusGUID == approved && a.CreditAppRequestType == (int)CreditAppRequestType.CloseCustomerCreditLimit);
                AccessModeView accessModeView = new AccessModeView();
                accessModeView.CanCreate = !hasCloseCustomerCreditLimitIsApproved;
                accessModeView.CanView = true;
                accessModeView.CanDelete = true;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView GetCloseCustomerCreditLimitInitialData(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTableItemView creditAppRequestTableItemView = creditAppRequestTableRepo.GetCloseCustomerCreditLimitInitialData(id);
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                creditAppRequestTableItemView.CustomerAllBuyerOutstanding = creditAppTableService.GetCustomerAllBuyerOutstanding(creditAppRequestTableItemView.RefCreditAppTableGUID.StringToGuid());
                return creditAppRequestTableItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView CreateCloseCustomerCreditLimitCreditAppRequestTable(CreditAppRequestTableItemView creditAppRequestTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                creditAppRequestTableView = accessLevelService.AssignOwnerBU(creditAppRequestTableView);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableView.ToCreditAppRequestTable();
                List<CAReqCreditOutStanding> caReqCreditOutStandings = new List<CAReqCreditOutStanding>();
                List<CAReqAssignmentOutstanding> caReqAssignmentOutstanding = new List<CAReqAssignmentOutstanding>();
                List<CAReqBuyerCreditOutstanding> caReqBuyerCreditOutstanding = new List<CAReqBuyerCreditOutstanding>();
                string userName = db.GetUserName();
                #region Create CreditAppRequestTable
                GenCreditAppRequestNumberSeqCode(creditAppRequestTable);
                creditAppRequestTable.CreditAppRequestTableGUID = Guid.NewGuid();
                #endregion Create CreditAppRequestTable
                #region 4. Create Credit outstanding
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                List<CreditOutstandingViewMap> creditOustandingView = creditAppTableRepo.GetCreditOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID, creditAppRequestTable.RequestDate);
                if (creditOustandingView.Any())
                {
                    creditOustandingView.ForEach(f =>
                    {
                        caReqCreditOutStandings.Add(new CAReqCreditOutStanding
                        {
                            CAReqCreditOutStandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID,
                            ProductType = f.ProductType,
                            CreditLimitTypeGUID = f.CreditLimitTypeGUID,
                            AccumRetentionAmount = f.AccumRetentionAmount,
                            AsOfDate = f.AsOfDate,
                            CreditAppTableGUID = f.CreditAppTableGUID,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            ApprovedCreditLimit = f.ApprovedCreditLimit,
                            CreditLimitBalance = f.CreditLimitBalance,
                            ARBalance = f.ARBalance,
                            ReserveToBeRefund = f.ReserveToBeRefund,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                        });
                    });
                }
                #endregion 4. Create Credit outstanding
                #region 5. Create Assignment agreement outstanding 
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                List<AssignmentAgreementOutstandingView> assignmentAgreementOutstandingViews = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID);
                if (assignmentAgreementOutstandingViews.Any())
                {
                    assignmentAgreementOutstandingViews.ForEach(f =>
                    {
                        caReqAssignmentOutstanding.Add(new CAReqAssignmentOutstanding
                        {
                            CAReqAssignmentOutstandingGUID = Guid.NewGuid(),
                            CustomerTableGUID = f.CustomerTableGUID.StringToGuid(),
                            AssignmentAgreementTableGUID = f.AssignmentAgreementTableGUID.StringToGuid(),
                            BuyerTableGUID = f.BuyerTableGUID.StringToGuid(),
                            AssignmentAgreementAmount = f.AssignmentAgreementAmount,
                            SettleAmount = f.SettledAmount,
                            RemainingAmount = f.RemainingAmount,
                            CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                            CompanyGUID = creditAppRequestTable.CompanyGUID,
                            Owner = userName,
                            OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                        });
                    });
                }
                #endregion 5. Create Assignment agreement outstanding 
                #region 6. Create CA buyer credit outstanding
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                List<CALineOutstandingViewMap> caLineOutstandingViewMap = new List<CALineOutstandingViewMap>();
                if (creditAppRequestTable.RefCreditAppTableGUID.HasValue)
                {
                    caLineOutstandingViewMap = creditAppLineRepo.GetCALineOutstandingByCA((int)RefType.CreditAppTable, creditAppRequestTable.RefCreditAppTableGUID.Value, true, creditAppRequestTable.RequestDate).ToList();
                    if (caLineOutstandingViewMap.Any())
                    {
                        caLineOutstandingViewMap.ForEach(f =>
                        {
                            caReqBuyerCreditOutstanding.Add(new CAReqBuyerCreditOutstanding
                            {
                                CAReqBuyerCreditOutstandingGUID = Guid.NewGuid(),
                                CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                                CreditAppTableGUID = f.CreditAppTableGUID,
                                CreditAppLineGUID = f.CreditAppLineGUID,
                                LineNum = f.LineNum,
                                BuyerTableGUID = f.BuyerTableGUID,
                                Status = f.Status,
                                ApprovedCreditLimitLine = f.ApprovedCreditLimitLine,
                                ARBalance = f.ARBalance,
                                AssignmentMethodGUID = f.AssignmentMethodGUID,
                                BillingResponsibleByGUID = f.BillingResponsibleByGUID,
                                MethodOfPaymentGUID = f.MethodOfPaymentGUID,
                                ProductType = f.ProductType,
                                MaxPurchasePct = f.MaxPurchasePct,
                                CompanyGUID = creditAppRequestTable.CompanyGUID,
                                Owner = userName,
                                OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
                            });
                        });
                    }
                }
                #endregion 6. Create CA buyer credit outstanding
                #region BulkInsert
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    try
                    {
                        BulkInsert(creditAppRequestTable.FirstToList());
                        if (caReqCreditOutStandings.Any())
                        {
                            BulkInsert(caReqCreditOutStandings);
                        }
                        if (caReqAssignmentOutstanding.Any())
                        {
                            BulkInsert(caReqAssignmentOutstanding);
                        }
                        if (caReqBuyerCreditOutstanding.Any())
                        {
                            BulkInsert(caReqBuyerCreditOutstanding);
                        }
                        UnitOfWork.Commit(transaction);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
                #endregion

                return creditAppRequestTable.ToCreditAppRequestTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CreditAppRequestTableItemView GetClosingCreditAppRequestById(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetClosingByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region UpdateClosingResult
        public UpdateClosingResultView GetUpdateClosingResultById(string creditAppRequestTableGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTableItemView creditAppRequestTable = creditAppRequestTableRepo.GetByIdvw(creditAppRequestTableGUID.StringToGuid());

                DateTime currentDate = DateTime.Now;
                UpdateClosingResultView updateClosingResultView = new UpdateClosingResultView
                {
                    CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
                    CreditAppRequestTable_Values = SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description),
                    ClosingDate = currentDate.DateToString()
                };
                return updateClosingResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetUpdateClosingResultValidation(UpdateClosingResultView UpdateClosingResultView)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(UpdateClosingResultView.CreditAppRequestTableGUID.StringToGuid());

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus creditAppRequestStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CreditAppRequestStatus.Draft).ToString());

                if (creditAppRequestTable.DocumentStatusGUID != creditAppRequestStatusDraft.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90012", new string[] { "LABEL.CLOSING_CREDIT_LIMIT_REQUEST" });
                }

                if (UpdateClosingResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.None))
                {
                    ex.AddData("ERROR.90057", new string[] { "LABEL.CLOSING_RESULT" });
                }

                if (creditAppRequestTable.RefCreditAppTableGUID != null)
                {
                    ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(creditAppRequestTable.RefCreditAppTableGUID.Value);
                    if (UpdateClosingResultView.ClosingDate.StringToDate() > creditAppTable.ExpiryDate)
                    {
                        ex.AddData("ERROR.DATE_CANNOT_LESS_THAN", new string[] { "LABEL.EXPIRY_DATE", "LABEL.CLOSING_DATE" });
                    }
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public UpdateClosingResultResultView UpdateClosingResult(UpdateClosingResultView UpdateClosingResultView)
        {
            try
            {
                // validate
                GetUpdateClosingResultValidation(UpdateClosingResultView);
                UpdateClosingResultResultView result = new UpdateClosingResultResultView();
                NotificationResponse success = new NotificationResponse();

                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(UpdateClosingResultView.CreditAppRequestTableGUID.StringToGuid());
                CreditAppRequestTableItemView update_CreditAppRequestTable = creditAppRequestTableRepo.GetByIdvw(UpdateClosingResultView.CreditAppRequestTableGUID.StringToGuid());

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus creditAppRequestApproved = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CreditAppRequestStatus.Approved).ToString());
                DocumentStatus creditAppRequestRejected = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)CreditAppRequestStatus.Rejected).ToString());


                if (UpdateClosingResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.Approved))
                {
                    update_CreditAppRequestTable.DocumentStatusGUID = creditAppRequestApproved.DocumentStatusGUID.GuidNullToString();
                }
                else if (UpdateClosingResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.Rejected))
                {
                    update_CreditAppRequestTable.DocumentStatusGUID = creditAppRequestRejected.DocumentStatusGUID.GuidNullToString();
                }
                update_CreditAppRequestTable.DocumentReasonGUID = UpdateClosingResultView.DocumentReasonGUID;
                update_CreditAppRequestTable.ApprovedDate = UpdateClosingResultView.ClosingDate;
            


                if (creditAppRequestTable.RefCreditAppTableGUID != null)
                {
                    ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(update_CreditAppRequestTable.RefCreditAppTableGUID.StringToGuidNull().Value);
                    CreditAppTableItemView update_CreditAppTableItemView = creditAppTableRepo.GetByIdvw(update_CreditAppRequestTable.RefCreditAppTableGUID.StringToGuidNull().Value);

                    ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                    CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);

                    if (UpdateClosingResultView.ApprovalDecision == Convert.ToInt32(ApprovalDecision.Approved))
                    {
                        update_CreditAppTableItemView.ExpiryDate = UpdateClosingResultView.ClosingDate;
                        update_CreditAppTableItemView.DocumentReasonGUID = UpdateClosingResultView.DocumentReasonGUID;
                        if (creditLimitType.CreditLimitExpiration == Convert.ToInt32(CreditLimitExpiration.ByTransaction) && creditAppTable.InactiveDate > creditAppTable.ExpiryDate)
                        {
                            update_CreditAppTableItemView.InactiveDate = UpdateClosingResultView.ClosingDate;
                        }
                        creditAppTableRepo.UpdateCreditAppTableVoid(creditAppTable, update_CreditAppTableItemView.ToCreditAppTable());
                    }
                }

                creditAppRequestTableRepo.UpdateCreditAppRequestTableVoid(creditAppRequestTable, update_CreditAppRequestTable.ToCreditAppRequestTable());
                UnitOfWork.Commit();
                success.AddData("SUCCESS.90009", new string[] { "LABEL.UPDATE_CLOSING_RESULT", SmartAppUtil.GetDropDownLabel(creditAppRequestTable.CreditAppRequestId, creditAppRequestTable.Description) });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion CloseCustomerCreditLimit
        public void CreateCreditAppRequestTableCollection(List<CreditAppRequestTableItemView> creditAppRequestTableItemViews)
        {
            try
            {
                if (creditAppRequestTableItemViews.Any())
                {
                    ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                    List<CreditAppRequestTable> creditAppRequestTables = creditAppRequestTableItemViews.ToCreditAppRequestTable().ToList();
                    creditAppRequestTableRepo.ValidateAdd(creditAppRequestTables);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(creditAppRequestTables, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public void CreateCreditAppRequestLineCollection(List<CreditAppRequestLineItemView> creditAppRequestLineItemViews)
        {
            try
            {
                if (creditAppRequestLineItemViews.Any())
                {
                    ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                    List<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineItemViews.ToCreditAppRequestLine().ToList();
                    creditAppRequestLineRepo.ValidateAdd(creditAppRequestLines);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(creditAppRequestLines, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #region Dropdown
        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTable(SearchParameter search)
        {
            try
            {
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                search.GetParentCondition(CreditAppRequestTableCondition.RefCreditAppTableGUID);
                return sysDropDownService.GetDropDownItemCreditAppRequestTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTableByAssignmentAgreementTable(SearchParameter search)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                search = search.GetParentCondition(CreditAppRequestTableCondition.CustomerTableGUID);
                SearchCondition searchCondition = SearchConditionService.GetCreditAppRequestTableStatusCondition(Convert.ToInt32(CreditAppRequestStatus.Approved).ToString());
                search.Conditions.Add(searchCondition);
                return creditAppRequestTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region GetCreditLimitClosingBookmarkView
        public CreditLimitClosingBookmarkView GetCreditLimitClosingBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                CreditLimitClosingBookmarkView creditLimitClosingBookmarkView = creditAppRequestTableRepo.GetCreditLimitClosingBookmarkValue(refGuid, bookmarkDocumentTransGuid);

                ISharedService sharedService = new SharedService(db);
                if (creditLimitClosingBookmarkView != null)
                {
                    // R04
                     creditLimitClosingBookmarkView.CACreateDate = creditLimitClosingBookmarkView.CACreate.DateToString();
                     creditLimitClosingBookmarkView.CustComEstablishedDate = creditLimitClosingBookmarkView.CustComEstablished.HasValue ? creditLimitClosingBookmarkView.CustComEstablished.DateNullToString() : null;
                }

                return creditLimitClosingBookmarkView;
                 }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region BuyerReviewCreditLimitBookmark
        public BuyerReviewCreditLimitBookmarkView GetBuyerReviewCreditLimitBookmarkValue(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                ISharedService sharedService = new SharedService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                BuyerReviewCreditLimitBookmarkView buyerReviewCreditLimitBookmarkView = new BuyerReviewCreditLimitBookmarkView();
                buyerReviewCreditLimitBookmarkView = creditAppRequestTableRepo.GetBuyerReviewCreditLimitBookmarkValue(refGUID, bookmarkDocumentTransGUID);

                return buyerReviewCreditLimitBookmarkView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentQueryCreditApplicationPF GetBookmarkDocumentQueryCreditApplicationPFValues(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetBookmarkDocumentQueryCreditApplicationPFValues(refGUID, bookmarkDocumentTransGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public BookmarkDocumentQueryCusotmerAmendInformation GetBookmarkDocumentQueryCustomerAmendInfo(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetBookmarkDocumentQueryCustomerAmendInformation(refGUID, bookmarkDocumentTransGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentQueryBuyerAmendInformation GetBookmarkDocumentQueryBuyerAmendInfo(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetBookmarkDocumentQueryBuyerAmendInformation(refGUID, bookmarkDocumentTransGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public QueryBuyerAmendCreditLimit GetBookmarkDocumentQueryBuyerAmendCreditLimit(Guid refGUID, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                return creditAppRequestLineRepo.GetBookmarkDocumentQueryBuyerAmendCreditLimit(refGUID, bookmarkDocumentTransGUID);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #region GenBookmarkBuyerCreditApplicationView
        public GenBookmarkBuyerCreditApplicationView GenBookmarkBuyerCreditAppValue(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.BookmarkBuyerCreditApplicationView(refGuid, bookmarkDocumentTransGuid);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public CopyFinancialStatementTransView CopyFinancialStatementTransByCART(CopyFinancialStatementTransView paramView)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
                CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(paramView.RefGUID.StringToGuid());
                IEnumerable<FinancialStatementTrans> copyFinancialStatementTranses = financialStatementTransRepo.GetCopyFinancialStatementTranByRefTypes(creditAppRequestTable.CustomerTableGUID, (int)RefType.Customer);
                return financialStatementTransService.CopyFinancialStatementTrans(paramView, copyFinancialStatementTranses);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CopyFinancialStatementTransView CopyFinancialStatementTransByCARL(CopyFinancialStatementTransView paramView)
        {
            try
            {
                IFinancialStatementTransService financialStatementTransService = new FinancialStatementTransService(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
                CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTracking(paramView.RefGUID.StringToGuid());
                IEnumerable<FinancialStatementTrans> copyFinancialStatementTranses = financialStatementTransRepo.GetCopyFinancialStatementTranByRefTypes(creditAppRequestLine.BuyerTableGUID, (int)RefType.Buyer);
                return financialStatementTransService.CopyFinancialStatementTrans(paramView, copyFinancialStatementTranses);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}