using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IReceiptTableService
	{

		ReceiptLineItemView GetReceiptLineById(string id);
		ReceiptLineItemView CreateReceiptLine(ReceiptLineItemView receiptLineView);
		ReceiptLineItemView UpdateReceiptLine(ReceiptLineItemView receiptLineView);
		bool DeleteReceiptLine(string id);
		ReceiptTableItemView GetReceiptTableById(string id);
		ReceiptTableItemView CreateReceiptTable(ReceiptTableItemView receiptTableView);
		ReceiptTableItemView UpdateReceiptTable(ReceiptTableItemView receiptTableView);
		bool DeleteReceiptTable(string id);
		IEnumerable<SelectItem<DocumentStatusItemView>>  GetDropDownItemReceiptDocumentStatus(SearchParameter search);
		PrintReceiptReportView GetPrintReceiptCopyById(string receiptTableGUID);
		PrintReceiptReportView GetPrintReceiptOriginalById(string receiptTableGUID);
		MemoTransItemView GetMemoTransInitialData(string refGUID);

	}
	public class ReceiptTableService : SmartAppService, IReceiptTableService
	{
		public ReceiptTableService(SmartAppDbContext context) : base(context) { }
		public ReceiptTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ReceiptTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ReceiptTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ReceiptTableService() : base() { }

		public ReceiptLineItemView GetReceiptLineById(string id)
		{
			try
			{
				IReceiptLineRepo receiptLineRepo = new ReceiptLineRepo(db);
				return receiptLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptLineItemView CreateReceiptLine(ReceiptLineItemView receiptLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				receiptLineView = accessLevelService.AssignOwnerBU(receiptLineView);
				IReceiptLineRepo receiptLineRepo = new ReceiptLineRepo(db);
				ReceiptLine receiptLine = receiptLineView.ToReceiptLine();
				receiptLine = receiptLineRepo.CreateReceiptLine(receiptLine);
				base.LogTransactionCreate<ReceiptLine>(receiptLine);
				UnitOfWork.Commit();
				return receiptLine.ToReceiptLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptLineItemView UpdateReceiptLine(ReceiptLineItemView receiptLineView)
		{
			try
			{
				IReceiptLineRepo receiptLineRepo = new ReceiptLineRepo(db);
				ReceiptLine inputReceiptLine = receiptLineView.ToReceiptLine();
				ReceiptLine dbReceiptLine = receiptLineRepo.Find(inputReceiptLine.ReceiptLineGUID);
				dbReceiptLine = receiptLineRepo.UpdateReceiptLine(dbReceiptLine, inputReceiptLine);
				base.LogTransactionUpdate<ReceiptLine>(GetOriginalValues<ReceiptLine>(dbReceiptLine), dbReceiptLine);
				UnitOfWork.Commit();
				return dbReceiptLine.ToReceiptLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteReceiptLine(string item)
		{
			try
			{
				IReceiptLineRepo receiptLineRepo = new ReceiptLineRepo(db);
				Guid receiptLineGUID = new Guid(item);
				ReceiptLine receiptLine = receiptLineRepo.Find(receiptLineGUID);
				receiptLineRepo.Remove(receiptLine);
				base.LogTransactionDelete<ReceiptLine>(receiptLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ReceiptTableItemView GetReceiptTableById(string id)
		{
			try
			{
				IReceiptTableRepo receiptTableRepo = new ReceiptTableRepo(db);
				return receiptTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTableItemView CreateReceiptTable(ReceiptTableItemView receiptTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				receiptTableView = accessLevelService.AssignOwnerBU(receiptTableView);
				IReceiptTableRepo receiptTableRepo = new ReceiptTableRepo(db);
				ReceiptTable receiptTable = receiptTableView.ToReceiptTable();
				receiptTable = receiptTableRepo.CreateReceiptTable(receiptTable);
				base.LogTransactionCreate<ReceiptTable>(receiptTable);
				UnitOfWork.Commit();
				return receiptTable.ToReceiptTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ReceiptTableItemView UpdateReceiptTable(ReceiptTableItemView receiptTableView)
		{
			try
			{
				IReceiptTableRepo receiptTableRepo = new ReceiptTableRepo(db);
				ReceiptTable inputReceiptTable = receiptTableView.ToReceiptTable();
				ReceiptTable dbReceiptTable = receiptTableRepo.Find(inputReceiptTable.ReceiptTableGUID);
				dbReceiptTable = receiptTableRepo.UpdateReceiptTable(dbReceiptTable, inputReceiptTable);
				base.LogTransactionUpdate<ReceiptTable>(GetOriginalValues<ReceiptTable>(dbReceiptTable), dbReceiptTable);
				UnitOfWork.Commit();
				return dbReceiptTable.ToReceiptTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteReceiptTable(string item)
		{
			try
			{
				IReceiptTableRepo receiptTableRepo = new ReceiptTableRepo(db);
				Guid receiptTableGUID = new Guid(item);
				ReceiptTable receiptTable = receiptTableRepo.Find(receiptTableGUID);
				receiptTableRepo.Remove(receiptTable);
				base.LogTransactionDelete<ReceiptTable>(receiptTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemReceiptDocumentStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Receipt);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MemoTransItemView GetMemoTransInitialData(string refGUID)
		{
			try
			{
				IMemoTransService memoTransService = new MemoTransService(db);
				IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
				string refId = receiptTempTableRepo.GetReceiptTempTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).ReceiptTempId;
				return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.ReceiptTemp);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Report
		public PrintReceiptReportView GetPrintReceiptCopyById(string receiptTableGUID)
		{
			try
			{
				IReceiptTableRepo receiptTableRepo = new ReceiptTableRepo(db);
				ReceiptTableItemView receiptTable = receiptTableRepo.GetByIdvw(receiptTableGUID.StringToGuid());


				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTable = customerTableRepo.GetByIdvw(receiptTable.CustomerTableGUID.StringToGuid());
                if (customerTable == null)
                {
					customerTable = new CustomerTableItemView();
                }

				PrintReceiptReportView printReceiptReportView = new PrintReceiptReportView
				{
					ReceiptTableGUID = receiptTableGUID,
					PrintReceiptGUID = receiptTableGUID,
					CustomerId = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId,customerTable.Name),
					ReceiptDate = receiptTable.ReceiptDate,
					ReceiptId = receiptTable.ReceiptId,
					SettleAmount = receiptTable.SettleAmount,
					SettleBaseAmount = receiptTable.SettleBaseAmount,
					SettleTaxAmount = receiptTable.SettleTaxAmount,
					TransDate = receiptTable.TransDate,
					IsCopy = true
				};
				return printReceiptReportView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public PrintReceiptReportView GetPrintReceiptOriginalById(string receiptTableGUID)
		{
			try
			{
				IReceiptTableRepo receiptTableRepo = new ReceiptTableRepo(db);
				ReceiptTableItemView receiptTable = receiptTableRepo.GetByIdvw(receiptTableGUID.StringToGuid());


				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTableItemView customerTable = customerTableRepo.GetByIdvw(receiptTable.CustomerTableGUID.StringToGuid());
				if (customerTable == null)
				{
					customerTable = new CustomerTableItemView();
				}

				PrintReceiptReportView printReceiptReportView = new PrintReceiptReportView
				{
					ReceiptTableGUID = receiptTableGUID,
					PrintReceiptGUID = receiptTableGUID,
					CustomerId = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					ReceiptDate = receiptTable.ReceiptDate,
					ReceiptId = receiptTable.ReceiptId,
					SettleAmount = receiptTable.SettleAmount,
					SettleBaseAmount = receiptTable.SettleBaseAmount,
					SettleTaxAmount = receiptTable.SettleTaxAmount,
					TransDate = receiptTable.TransDate,
					IsCopy = false
				};
				return printReceiptReportView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion report
	}
}
