﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IWithholdingTaxTableService
    {

        WithholdingTaxTableItemView GetWithholdingTaxTableById(string id);
        WithholdingTaxTableItemView CreateWithholdingTaxTable(WithholdingTaxTableItemView withholdingTaxTableView);
        WithholdingTaxTableItemView UpdateWithholdingTaxTable(WithholdingTaxTableItemView withholdingTaxTableView);
        bool DeleteWithholdingTaxTable(string id);
        WithholdingTaxValueItemView GetWithholdingTaxValueById(string id);
        WithholdingTaxValueItemView CreateWithholdingTaxValue(WithholdingTaxValueItemView taxValueView);
        WithholdingTaxValueItemView UpdateWithholdingTaxValue(WithholdingTaxValueItemView taxValueView);
        bool DeleteWithholdingTaxValue(string id);
        WithholdingTaxValueItemView GetWithholdingTaxValueInitialData(string withholdingTaxTableGUID);
        #region shared
        decimal GetWHTValue(Guid whtTableGuid, DateTime asOfDate, bool isThrow = true);
        #endregion
    }
    public class WithholdingTaxTableService : SmartAppService, IWithholdingTaxTableService
    {
        public WithholdingTaxTableService(SmartAppDbContext context) : base(context) { }
        public WithholdingTaxTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public WithholdingTaxTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public WithholdingTaxTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public WithholdingTaxTableService() : base() { }

        public WithholdingTaxTableItemView GetWithholdingTaxTableById(string id)
        {
            try
            {
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                return withholdingTaxTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public WithholdingTaxTableItemView CreateWithholdingTaxTable(WithholdingTaxTableItemView withholdingTaxTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                withholdingTaxTableView = accessLevelService.AssignOwnerBU(withholdingTaxTableView);
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                WithholdingTaxTable withholdingTaxTable = withholdingTaxTableView.ToWithholdingTaxTable();
                withholdingTaxTable = withholdingTaxTableRepo.CreateWithholdingTaxTable(withholdingTaxTable);
                base.LogTransactionCreate<WithholdingTaxTable>(withholdingTaxTable);
                UnitOfWork.Commit();
                return withholdingTaxTable.ToWithholdingTaxTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public WithholdingTaxTableItemView UpdateWithholdingTaxTable(WithholdingTaxTableItemView withholdingTaxTableView)
        {
            try
            {
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                WithholdingTaxTable inputWithholdingTaxTable = withholdingTaxTableView.ToWithholdingTaxTable();
                WithholdingTaxTable dbWithholdingTaxTable = withholdingTaxTableRepo.Find(inputWithholdingTaxTable.WithholdingTaxTableGUID);
                dbWithholdingTaxTable = withholdingTaxTableRepo.UpdateWithholdingTaxTable(dbWithholdingTaxTable, inputWithholdingTaxTable);
                base.LogTransactionUpdate<WithholdingTaxTable>(GetOriginalValues<WithholdingTaxTable>(dbWithholdingTaxTable), dbWithholdingTaxTable);
                UnitOfWork.Commit();
                return dbWithholdingTaxTable.ToWithholdingTaxTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteWithholdingTaxTable(string item)
        {
            try
            {
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                Guid withholdingTaxTableGUID = new Guid(item);
                WithholdingTaxTable withholdingTaxTable = withholdingTaxTableRepo.Find(withholdingTaxTableGUID);
                withholdingTaxTableRepo.Remove(withholdingTaxTable);
                base.LogTransactionDelete<WithholdingTaxTable>(withholdingTaxTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public WithholdingTaxValueItemView GetWithholdingTaxValueById(string id)
        {
            try
            {
                IWithholdingTaxValueRepo taxValueRepo = new WithholdingTaxValueRepo(db);
                return taxValueRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public WithholdingTaxValueItemView CreateWithholdingTaxValue(WithholdingTaxValueItemView taxValueView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                taxValueView = accessLevelService.AssignOwnerBU(taxValueView);
                IWithholdingTaxValueRepo taxValueRepo = new WithholdingTaxValueRepo(db);
                WithholdingTaxValue taxValue = taxValueView.ToWithholdingTaxValue();
                taxValue = taxValueRepo.CreateWithholdingTaxValue(taxValue);
                base.LogTransactionCreate<WithholdingTaxValue>(taxValue);
                UnitOfWork.Commit();
                return taxValue.ToWithholdingTaxValueItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public WithholdingTaxValueItemView UpdateWithholdingTaxValue(WithholdingTaxValueItemView taxValueView)
        {
            try
            {
                IWithholdingTaxValueRepo taxValueRepo = new WithholdingTaxValueRepo(db);
                WithholdingTaxValue inputWithholdingTaxValue = taxValueView.ToWithholdingTaxValue();
                WithholdingTaxValue dbWithholdingTaxValue = taxValueRepo.Find(inputWithholdingTaxValue.WithholdingTaxValueGUID);
                dbWithholdingTaxValue = taxValueRepo.UpdateWithholdingTaxValue(dbWithholdingTaxValue, inputWithholdingTaxValue);
                base.LogTransactionUpdate<WithholdingTaxValue>(GetOriginalValues<WithholdingTaxValue>(dbWithholdingTaxValue), dbWithholdingTaxValue);
                UnitOfWork.Commit();
                return dbWithholdingTaxValue.ToWithholdingTaxValueItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteWithholdingTaxValue(string item)
        {
            try
            {
                IWithholdingTaxValueRepo taxValueRepo = new WithholdingTaxValueRepo(db);
                Guid taxValueGUID = new Guid(item);
                WithholdingTaxValue taxValue = taxValueRepo.Find(taxValueGUID);
                taxValueRepo.Remove(taxValue);
                base.LogTransactionDelete<WithholdingTaxValue>(taxValue);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public WithholdingTaxValueItemView GetWithholdingTaxValueInitialData(string withholdingTaxTableGUID)
        {
            try
            {
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                WithholdingTaxTable withholdingTaxTable = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(withholdingTaxTableGUID.StringToGuid());
                return new WithholdingTaxValueItemView
                {
                    WithholdingTaxValueGUID = new Guid().GuidNullToString(),
                    WithholdingTaxTableGUID = withholdingTaxTableGUID,
                    WithholdingTaxTable_Values = SmartAppUtil.GetDropDownLabel(withholdingTaxTable.WHTCode, withholdingTaxTable.Description),
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region shared
        public decimal GetWHTValue(Guid whtTableGuid, DateTime asOfDate, bool isThrow = true)
        {
            try
            {
                IWithholdingTaxValueRepo withholdingTaxValueRepo = new WithholdingTaxValueRepo(db);
                WithholdingTaxValue withholdingTaxValue = withholdingTaxValueRepo.GetByWHTTableGUIDAndDateNoTracking(whtTableGuid, asOfDate, isThrow);
                return withholdingTaxValue.Value;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
