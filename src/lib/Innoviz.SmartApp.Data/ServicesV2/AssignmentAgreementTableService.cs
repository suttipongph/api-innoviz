using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IAssignmentAgreementTableService
    {

        AssignmentAgreementLineItemView GetAssignmentAgreementLineById(string id);
        AssignmentAgreementLineItemView CreateAssignmentAgreementLine(AssignmentAgreementLineItemView assignmentAgreementLineView);
        AssignmentAgreementLineItemView UpdateAssignmentAgreementLine(AssignmentAgreementLineItemView assignmentAgreementLineView);
        IEnumerable<SelectItem<DocumentReasonItemView>> GetDocumentReasonDropdown(SearchParameter search);
        bool DeleteAssignmentAgreementLine(string id);
        AssignmentAgreementTableItemView GetAssignmentAgreementTableById(string id);
        AssignmentAgreementTableItemView CreateAssignmentAgreementTable(AssignmentAgreementTableItemView assignmentAgreementTableView);
        AssignmentAgreementTableItemView UpdateAssignmentAgreementTable(AssignmentAgreementTableItemView assignmentAgreementTableView);
        UpdateAssignmentAgreementTableAmountResult UpdateAssignmentAgreementTableAmount(UpdateAssignmentAgreementTableAmountView assignmentAgreementTableView);
        AssignmentAgreementTable UpdateAssignmentAgreementTable(AssignmentAgreementTable assignmentAgreementTable);
        bool DeleteAssignmentAgreementTable(string id);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemtNotStatus(SearchParameter search);
        List<int> GetVisibleManageMenu(string assignmentAgreementTableGuid);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemByCustomerTable(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemByPurchaseLine(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemByWithdrawalTable(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementByCustomerAndStatus(SearchParameter search);
        IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetAssignmentAgreementTableByCustomerDropDown(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetAssignmentAgreementTableByBuyerDropDown(SearchParameter search);
        IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetAssignmentAgreementTableDropDown(SearchParameter search);
        #region function
        AssignmentAgreementTableItemView GetAssignmentAgreementInitialData();
        AssignmentAgreementTableItemView CreateManagementAssignmentAgreement(AssignmentAgreementTableItemView model);
        AssignmentAgreementLineItemView GetAssignmentAgreementLineInitialDataByParentId(string id);
        AccessModeView GetAccessModeByAssignmentTableTable(string id);
        AccessModeView GetAccessModeByAssignmentTableByBookmarkDocumentTrans(string id);
        bool IsManualByAssignmentAgreement(string companyId);
        public MemoTransItemView GetMemoTransInitialData(string refGUID);
        #region generateNoticeOfCancellation
        AssignmentAgreementTableItemView GetGenerateNoticeOfCancellationValidation(string assignmentGuid);
        bool GetGenerateNoticeOfCancellationValidation(AssignmentAgreementTableItemView viewModel);
        GenAssignmentAgmNoticeOfCancelView GenerateNoticeOfCancellation(GenAssignmentAgmNoticeOfCancelView viewModel);
        #endregion
        #endregion
        #region relatedinfo
        BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refGUID);
        ConsortiumTransItemView GetConsortiumTransInitialDataByAssignmentAgreementTable(string refGUID);
        AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refGUID);
        JointVentureTransItemView GetJointVentureTransInitialData(string refGUID);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByAssignmentAgreementTable(string refGUID);
        #endregion
        #region Get AccessMode
        AccessModeView GetAccessModeByStatusLessThanSigned(string assignmentAgreementId);
        #endregion
        #region Shared 
        decimal GetAssignmentBalance(Guid assignmentAgreementGUID);
        #endregion

        SearchResult<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstanding(SearchParameter search);

        AssignmentAgreementBookmarkView GetAssignmentAgreementBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid);
        List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstanding(Guid refGUID, int refType);
        CancelAssignmentAgreementView GetCancelAssignmentAgreementValue(Guid refGuid, Guid bookmarkDocumentTransGUID);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemAssignmentAgreementStatus(SearchParameter search);
        void CreateAssignmentAgreementTableCollection(IEnumerable<AssignmentAgreementTableItemView> assignmentAgreementTablesView);
        void CreateAssignmentAgreementLineCollection(IEnumerable<AssignmentAgreementLineItemView> assignmentAgreementLinesView);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgeementTableByCustomerAndBuyer(SearchParameter search);
        bool GetUpdateAssignmentagreementAmountValidation(AssignmentAgreementTableItemView vmodel);



    }
    public class AssignmentAgreementTableService : SmartAppService, IAssignmentAgreementTableService
    {
        public AssignmentAgreementTableService(SmartAppDbContext context) : base(context) { }
        public AssignmentAgreementTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public AssignmentAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public AssignmentAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public AssignmentAgreementTableService() : base() { }

        public AssignmentAgreementLineItemView GetAssignmentAgreementLineById(string id)
        {
            try
            {
                IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                return assignmentAgreementLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementLineItemView CreateAssignmentAgreementLine(AssignmentAgreementLineItemView assignmentAgreementLineView)
        {
            try
            {
                AssignmentAgreementLine assignmentAgreementLine = assignmentAgreementLineView.ToAssignmentAgreementLine();
                assignmentAgreementLine = CreateAssignmentAgreementLine(assignmentAgreementLine);
                UnitOfWork.Commit();
                return assignmentAgreementLine.ToAssignmentAgreementLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementLine CreateAssignmentAgreementLine(AssignmentAgreementLine assignmentAgreementLine)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                assignmentAgreementLine = accessLevelService.AssignOwnerBU(assignmentAgreementLine);
                IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                assignmentAgreementLine = assignmentAgreementLineRepo.CreateAssignmentAgreementLine(assignmentAgreementLine);
                base.LogTransactionCreate<AssignmentAgreementLine>(assignmentAgreementLine);
                return assignmentAgreementLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementLineItemView UpdateAssignmentAgreementLine(AssignmentAgreementLineItemView assignmentAgreementLineView)
        {
            try
            {
                IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                AssignmentAgreementLine inputAssignmentAgreementLine = assignmentAgreementLineView.ToAssignmentAgreementLine();
                AssignmentAgreementLine dbAssignmentAgreementLine = assignmentAgreementLineRepo.Find(inputAssignmentAgreementLine.AssignmentAgreementLineGUID);
                dbAssignmentAgreementLine = assignmentAgreementLineRepo.UpdateAssignmentAgreementLine(dbAssignmentAgreementLine, inputAssignmentAgreementLine);
                base.LogTransactionUpdate<AssignmentAgreementLine>(GetOriginalValues<AssignmentAgreementLine>(dbAssignmentAgreementLine), dbAssignmentAgreementLine);
                UnitOfWork.Commit();
                return dbAssignmentAgreementLine.ToAssignmentAgreementLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteAssignmentAgreementLine(string item)
        {
            try
            {
                IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                Guid assignmentAgreementLineGUID = new Guid(item);
                AssignmentAgreementLine assignmentAgreementLine = assignmentAgreementLineRepo.Find(assignmentAgreementLineGUID);
                assignmentAgreementLineRepo.Remove(assignmentAgreementLine);
                base.LogTransactionDelete<AssignmentAgreementLine>(assignmentAgreementLine);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AssignmentAgreementTableItemView GetAssignmentAgreementTableById(string id)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                return assignmentAgreementTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementTableItemView CreateAssignmentAgreementTable(AssignmentAgreementTableItemView assignmentAgreementTableView)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableView.ToAssignmentAgreementTable();
                assignmentAgreementTable = CreateAssignmentAgreementTable(assignmentAgreementTable);
                UnitOfWork.Commit();
                return assignmentAgreementTable.ToAssignmentAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementTable CreateAssignmentAgreementTable(AssignmentAgreementTable assignmentAgreementTable)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                assignmentAgreementTable = accessLevelService.AssignOwnerBU(assignmentAgreementTable);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                assignmentAgreementTable = assignmentAgreementTableRepo.CreateAssignmentAgreementTable(assignmentAgreementTable);
                base.LogTransactionCreate<AssignmentAgreementTable>(assignmentAgreementTable);
                return assignmentAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementTableItemView UpdateAssignmentAgreementTable(AssignmentAgreementTableItemView assignmentAgreementTableView)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableView.ToAssignmentAgreementTable();
                assignmentAgreementTable = UpdateAssignmentAgreementTable(assignmentAgreementTable);
                UnitOfWork.Commit();
                return assignmentAgreementTable.ToAssignmentAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementTable UpdateAssignmentAgreementTable(AssignmentAgreementTable assignmentAgreementTable)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTable dbAssignmentAgreementTable = assignmentAgreementTableRepo.Find(assignmentAgreementTable.AssignmentAgreementTableGUID);
                dbAssignmentAgreementTable = assignmentAgreementTableRepo.UpdateAssignmentAgreementTable(dbAssignmentAgreementTable, assignmentAgreementTable);
                base.LogTransactionUpdate<AssignmentAgreementTable>(GetOriginalValues<AssignmentAgreementTable>(dbAssignmentAgreementTable), dbAssignmentAgreementTable);
                return dbAssignmentAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteAssignmentAgreementTable(string item)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                Guid assignmentAgreementTableGUID = new Guid(item);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.Find(assignmentAgreementTableGUID);
                assignmentAgreementTableRepo.Remove(assignmentAgreementTable);
                base.LogTransactionDelete<AssignmentAgreementTable>(assignmentAgreementTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemtNotStatus(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                search = search.GetParentCondition(AssignmentAgreementCondition.BuyerTableGUID);
                string Closed = ((int)AssignmentAgreementStatus.Closed).ToString();
                string Cancelled = ((int)AssignmentAgreementStatus.Cancelled).ToString();
                List<SearchCondition> searchConditions = SearchConditionService.GetAssignmentAgreementNotStatusCondition(new string[] { Closed, Cancelled });
                search.Conditions.AddRange(searchConditions);
                return assignmentAgreementTableRepo.GetDropDownItemByStatus(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemByCustomerTable(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                search = search.GetParentCondition(AssignmentAgreementCondition.CustomerTableGUID);
                return assignmentAgreementTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemByPurchaseLine(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { AssignmentAgreementCondition.CustomerTableGUID, AssignmentAgreementCondition.BuyerTableGUID, AssignmentAgreementCondition.AssignmentMethodGUID });
                SearchCondition searchCondition = SearchConditionService.GetAssignmentAgreementStatusCondition(Convert.ToInt32(AssignmentAgreementStatus.Signed).ToString());
                search.Conditions.Add(searchCondition);
                return assignmentAgreementTableRepo.GetDropDownItemByStatus(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementItemByWithdrawalTable(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { AssignmentAgreementCondition.CustomerTableGUID, AssignmentAgreementCondition.BuyerTableGUID });
                SearchCondition searchCondition = SearchConditionService.GetAssignmentAgreementStatusCondition(Convert.ToInt32(AssignmentAgreementStatus.Signed).ToString());
                search.Conditions.Add(searchCondition);
                return assignmentAgreementTableRepo.GetDropDownItemByStatus(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetDropDownItemAssignmentAgreementByCustomerAndStatus(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { AssignmentAgreementCondition.CustomerTableGUID, AssignmentAgreementCondition.StatusId });
                //SearchCondition searchCondition = SearchConditionService.GetAssignmentAgreementStatusCondition(Convert.ToInt32(AssignmentAgreementStatus.Signed).ToString());
                //search.Conditions.Add(searchCondition);
                return assignmentAgreementTableRepo.GetDropDownItemByStatus(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<int> GetVisibleManageMenu(string assignmentAgreementTableGuid)
        {
            try
            {
                List<int> result = new List<int>();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(assignmentAgreementTableGuid.StringToGuid());
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(assignmentAgreementTable.DocumentStatusGUID.GetValueOrDefault());
                DocumentStatus completedBookmarkTrans = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.AssignmentAgreement, assignmentAgreementTableGuid.StringToGuid());
                if ((int)AssignmentAgreementStatus.Draft == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Post);
                }
                if ((int)AssignmentAgreementStatus.Posted == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Send);
                }
                if ((int)AssignmentAgreementStatus.Sent == int.Parse(documentStatus.StatusId) && bookmarkDocumentTrans.All(a => a.DocumentStatusGUID == completedBookmarkTrans.DocumentStatusGUID))
                {
                    result.Add((int)ManageAgreementAction.Sign);
                }
                if ((int)AssignmentAgreementStatus.Signed == int.Parse(documentStatus.StatusId) && assignmentAgreementTable.AgreementDocType == (int)AgreementDocType.New)
                {
                    result.Add((int)ManageAgreementAction.Close);
                }
                if (int.Parse(documentStatus.StatusId) < (int)AssignmentAgreementStatus.Signed)
                {
                    result.Add((int)ManageAgreementAction.Cancel);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region function
        public AssignmentAgreementTableItemView GetAssignmentAgreementInitialData()
        {
            try
            {
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                AssignmentAgreementTableItemView result = new AssignmentAgreementTableItemView();
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                List<string> referenceIds = new List<string>() { ReferenceId.InternalAssignmentAgreement, ReferenceId.AssignmentAgreement };
                IEnumerable<NumberSeqTableItemView> numberSeqTableItemViews = numberSeqTableRepo.GetNumberSeqTableByRefId(referenceIds);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Draft).ToString());
                result.AssignmentAgreementTableGUID = Guid.Empty.ToString();
                result.IsInternalAssignmentAgreementIdManual = numberSequenceService.IsManualByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.InternalAssignmentAgreement);
                result.IsAssignmentAgreementIdManual = numberSequenceService.IsManualByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.AssignmentAgreement);
                result.DocumentStatus_StatusId = documentStatus.StatusId;
                result.DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description);
                result.AgreementDocType = (int)AgreementDocType.New;
                result.AgreementDate = DateTime.Now.DateToString();
                result.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                string refId = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(refGUID.StringToGuid()).InternalAssignmentAgreementId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.AssignmentAgreement);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool ValidateCreateManagementAssignmentAgreement(AssignmentAgreementTable assignmentAgreement)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByInternalAssignmentId(assignmentAgreement.InternalAssignmentAgreementId);
                if (!string.IsNullOrEmpty(assignmentAgreement.InternalAssignmentAgreementId) && assignmentAgreementTable != null)
                {
                    ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.INTERNAL_ASSIGNMENT_AGREEMENT_ID" });
                }
                assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByAssignmentId(assignmentAgreement.AssignmentAgreementId);
                if (!string.IsNullOrEmpty(assignmentAgreement.AssignmentAgreementId) && assignmentAgreementTable != null)
                {
                    ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.ASSIGNMENT_AGREEMENT_ID" });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementTableItemView CreateManagementAssignmentAgreement(AssignmentAgreementTableItemView model)
        {
            try
            {
                AssignmentAgreementTable assignmentAgreementTable = new AssignmentAgreementTable();
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                assignmentAgreementTable = CreateNewAssignmentAgreement(model);
                if (!model.IsInternalAssignmentAgreementIdManual)
                {
                    INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.InternalAssignmentAgreement);
                    assignmentAgreementTable.InternalAssignmentAgreementId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
                if (ValidateCreateManagementAssignmentAgreement(assignmentAgreementTable))
                {
                    IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    assignmentAgreementTable = CreateAssignmentAgreementTable(assignmentAgreementTable);
                    AgreementTableInfo agreementTableInfo = CreateAgreementTableInfo(assignmentAgreementTable);
                    agreementTableInfo = agreementTableInfoService.CreateAgreementTableInfo(agreementTableInfo);
                    IEnumerable<JointVentureTrans> jointVentureTrans = CreateJointVentureTrans(assignmentAgreementTable, assignmentAgreementTable.CustomerTableGUID, (int)RefType.Customer);
                    IEnumerable<ConsortiumTrans> consortiumTrans = CreateConsortiumTrans(assignmentAgreementTable);

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        if (jointVentureTrans != null)
                        {
                            base.BulkInsert(jointVentureTrans.ToList(), true);
                        }
                        if (consortiumTrans != null)
                        {
                            base.BulkInsert(consortiumTrans.ToList(), true);
                        }
                        UnitOfWork.Commit(transaction);
                    }
                }
                return assignmentAgreementTable.ToAssignmentAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private AssignmentAgreementTable CreateNewAssignmentAgreement(AssignmentAgreementTableItemView model)
        {
            try
            {
                AssignmentAgreementTable assignmentAgreementTable = model.ToAssignmentAgreementTable();
                assignmentAgreementTable.AssignmentAgreementTableGUID = Guid.NewGuid();
                return assignmentAgreementTable;

            }
            catch (Exception)
            {

                throw;
            }
        }
        private AgreementTableInfo CreateAgreementTableInfo(AssignmentAgreementTable assignmentAgreementTable)
        {
            try
            {
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                ICompanyRepo companyRepo = new CompanyRepo(db);
                IContactTransRepo contactTransRepo = new ContactTransRepo(db);
                ICompanyBankRepo companyBankRepo = new CompanyBankRepo(db);
                AddressTrans customerAddressTrans = addressTransRepo.GetAddressTransByRefType((int)RefType.Customer, assignmentAgreementTable.CustomerTableGUID)
                    .Where(w => w.Primary).FirstOrDefault();
                AddressTrans buyerAddressTrans = addressTransRepo.GetAddressTransByRefType((int)RefType.Buyer, assignmentAgreementTable.BuyerTableGUID)
                    .Where(w => w.Primary).FirstOrDefault();
                Company company = companyRepo.GetCompanyByIdNoTracking(db.GetCompanyFilter().StringToGuid());
                AddressTrans companyAddressTrans = addressTransRepo.GetPrimaryAddressByReference(RefType.Branch, company.DefaultBranchGUID.GetValueOrDefault());
                ContactTrans phone = contactTransRepo.GetPrimaryContactTransByRefTypeAndContactType(company.DefaultBranchGUID.GetValueOrDefault(), (int)RefType.Branch, (int)ContactType.Phone);
                ContactTrans fax = contactTransRepo.GetPrimaryContactTransByRefTypeAndContactType(company.DefaultBranchGUID.GetValueOrDefault(), (int)RefType.Branch, (int)ContactType.Fax);
                CompanyBank companyBank = companyBankRepo.GetPrimaryCompanyBankByCompanyNoTracking(company.CompanyGUID);
                AgreementTableInfo agreementTableInfo = new AgreementTableInfo()
                {
                    AgreementTableInfoGUID = Guid.NewGuid(),
                    RefGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                    RefType = (int)RefType.AssignmentAgreement,
                    CustomerAddress1 = customerAddressTrans == null ? null : customerAddressTrans.Address1,
                    CustomerAddress2 = customerAddressTrans == null ? null : customerAddressTrans.Address2,
                    BuyerAddress1 = buyerAddressTrans == null ? null : buyerAddressTrans.Address1,
                    BuyerAddress2 = buyerAddressTrans == null ? null : buyerAddressTrans.Address2,
                    CompanyName = company.Name,
                    CompanyAltName = company.AltName,
                    CompanyAddress1 = companyAddressTrans == null ? null : companyAddressTrans.Address1,
                    CompanyAddress2 = companyAddressTrans == null ? null : companyAddressTrans.Address2,
                    CompanyPhone = phone != null ? phone.Description : null,
                    CompanyFax = fax != null ? fax.Description : null,
                    CompanyBankGUID = companyBank != null ? (Guid?)companyBank.CompanyBankGUID : null,
                    CompanyGUID = assignmentAgreementTable.CompanyGUID
                };
                return agreementTableInfo;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private IEnumerable<JointVentureTrans> CreateJointVentureTrans(AssignmentAgreementTable assignmentAgreementTable, Guid fromRefGUID, int fromRefType)
        {
            try
            {
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                if (assignmentAgreementTable.CustomerTableGUID == null)
                {
                    return null;
                }
                else
                {
                    IEnumerable<JointVentureTrans> jointVentureTrans = jointVentureTransService.CopyJointVentureTrans(fromRefGUID, assignmentAgreementTable.AssignmentAgreementTableGUID, fromRefType, (int)RefType.AssignmentAgreement);
                    jointVentureTrans.ToList().ForEach(item =>
                    {
                        item.RefType = (int)RefType.AssignmentAgreement;
                        item.RefGUID = assignmentAgreementTable.AssignmentAgreementTableGUID;
                    });
                    return jointVentureTrans;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IEnumerable<ConsortiumTrans> CreateConsortiumTrans(AssignmentAgreementTable assignmentAgreementTable)
        {
            try
            {
                IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                if (assignmentAgreementTable.ConsortiumTableGUID == null)
                {
                    return null;
                }
                else
                {
                    IEnumerable<ConsortiumTrans> consortiumTrans = consortiumTransService.CopyConsortiumTrans(assignmentAgreementTable.ConsortiumTableGUID.GetValueOrDefault(), assignmentAgreementTable.AssignmentAgreementTableGUID, (int)RefType.AssignmentAgreement);
                    consortiumTrans.ToList().ForEach(item =>
                    {
                        item.RefType = (int)RefType.AssignmentAgreement;
                        item.RefGUID = assignmentAgreementTable.AssignmentAgreementTableGUID;
                    });
                    return consortiumTrans;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AssignmentAgreementLineItemView GetAssignmentAgreementLineInitialDataByParentId(string id)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTableItemView assignmentAgreementTable = assignmentAgreementTableRepo.GetByIdvw(id.StringToGuid());

                IAssignmentAgreementLineRepo asignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);

                int lineNum = 0;
                IEnumerable<AssignmentAgreementLine> assignmentAgreementLines = asignmentAgreementLineRepo.GetAssignmentAgreementLineByAssignmentAgreementTableGuid(assignmentAgreementTable.AssignmentAgreementTableGUID.StringToGuid());
                if (assignmentAgreementLines.ToList().Count > 0)
                {
                    lineNum = assignmentAgreementLines.ToList().OrderByDescending(o => o.LineNum).FirstOrDefault().LineNum + 1;
                }
                else
                {
                    lineNum = 1;
                }
                AssignmentAgreementLineItemView assignmentAgreementLineItemView = new AssignmentAgreementLineItemView()
                {
                    AssignmentAgreementTable_StatusId = assignmentAgreementTable.DocumentStatus_StatusId,
                    AssignmentAgreementTable_BuyerTableGUID = assignmentAgreementTable.BuyerTableGUID,
                    AssignmentAgreementTable_InternalAssignmentAgreementId = assignmentAgreementTable.InternalAssignmentAgreementId,
                    AssignmentAgreementTable_AssignmentAgreementId = assignmentAgreementTable.AssignmentAgreementId,
                    AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                    LineNum = lineNum,
                    AssignmentAgreementTable_CustomerTableGUID = assignmentAgreementTable.CustomerTableGUID
                };
                return assignmentAgreementLineItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByAssignmentTableTable(string id)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTableItemView assignmentAgreementTable = assignmentAgreementTableRepo.GetByIdvw(id.StringToGuid());
                if (Int32.Parse(assignmentAgreementTable.DocumentStatus_StatusId) < (int)AssignmentAgreementStatus.Signed)
                {
                    accessModeView.CanCreate = true;
                    accessModeView.CanView = true;
                    accessModeView.CanDelete = true;
                }
                else
                {
                    accessModeView.CanCreate = false;
                    accessModeView.CanView = false;
                    accessModeView.CanDelete = false;
                }
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByAssignmentTableByBookmarkDocumentTrans(string id)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTableItemView assignmentAgreementTable = assignmentAgreementTableRepo.GetByIdvw(id.StringToGuid());
                if (Int32.Parse(assignmentAgreementTable.DocumentStatus_StatusId) <= (int)AssignmentAgreementStatus.Signed)
                {
                    accessModeView.CanCreate = true;
                    accessModeView.CanView = true;
                    accessModeView.CanDelete = true;
                }
                else
                {
                    accessModeView.CanCreate = false;
                    accessModeView.CanView = false;
                    accessModeView.CanDelete = false;
                }
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refGUID)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                string refId = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalAssignmentAgreementId;
                return bookmarkDocumentTransService.GetBookmarkDocumentTransInitialData(refId, refGUID, Models.Enum.RefType.AssignmentAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualByAssignmentAgreement(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.InternalAssignmentAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region GenerateNoticeOfCancellation

        public AssignmentAgreementTableItemView GetGenerateNoticeOfCancellationValidation(string assignmentGuid)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTableItemView assignmentAgreementTableItemView = assignmentAgreementTableRepo.GetByIdvw(assignmentGuid.StringToGuid());
                return GetGenerateNoticeOfCancellationValidation(assignmentAgreementTableItemView) ? assignmentAgreementTableItemView : null;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        public bool GetGenerateNoticeOfCancellationValidation(AssignmentAgreementTableItemView viewModel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(viewModel.AssignmentAgreementTableGUID.StringToGuid());
                List<DocumentStatus> assignmentStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.AssignmentAgreement).ToString()).ToList();
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Signed).ToString());
                DocumentStatus cancelStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Cancelled).ToString());
                DocumentStatus assignmentAgreementStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(assignmentAgreementTable.DocumentStatusGUID.GetValueOrDefault());
                List<AssignmentAgreementTable> refAssignmentAgreementTables = assignmentAgreementTableRepo.GetListByRefAssignmentAgreementTableGUID(assignmentAgreementTable.AssignmentAgreementTableGUID);

                if (assignmentAgreementTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.ASSIGNMENT_AGREEMENT" });
                }
                if (assignmentAgreementTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                var condition3 = (from refAssignment in refAssignmentAgreementTables
                                  join docStatus in assignmentStatus
                                   on refAssignment.DocumentStatusGUID equals docStatus.DocumentStatusGUID
                                   where refAssignment.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation
                                      && Convert.ToInt32(docStatus.StatusId) != (int)(AssignmentAgreementStatus.Cancelled)
                                  select refAssignment);

                if (condition3.Any())
                {
                    ex.AddData("ERROR.90004", new string[] { "ENUM.NOTICE_OF_CANCELLATION", SmartAppUtil.GetDropDownLabel(assignmentAgreementTable.InternalAssignmentAgreementId, assignmentAgreementTable.Description) });
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public GenAssignmentAgmNoticeOfCancelView GenerateNoticeOfCancellation(GenAssignmentAgmNoticeOfCancelView viewModel)
        {
            try
            {
                NotificationResponse success = new NotificationResponse();
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                List<AssignmentAgreementLine> dbAssignmentAgreementLines = assignmentAgreementLineRepo.GetAssignmentAgreementLineByAssignmentAgreementTableGuidNoTracking(viewModel.AssignmentAgreementTableItemView.AssignmentAgreementTableGUID.StringToGuid());
                List<AssignmentAgreementLine> assignmentAgreementLines = new List<AssignmentAgreementLine>();
                if (GetGenerateNoticeOfCancellationValidation(viewModel.AssignmentAgreementTableItemView))
                {
                    AssignmentAgreementTable assignmentAgreementTable = CreateAssignmentAgreementTableGenNoticeOfCancellation(viewModel);
                    dbAssignmentAgreementLines.ForEach(line =>
                    {
                        AssignmentAgreementLine assignmentAgreementLine = CreateAssignmentAgreementLineGenNoticeOfCancellation(assignmentAgreementTable, line);
                        assignmentAgreementLines.Add(assignmentAgreementLine);
                    });
                    CopyAgreementInfoView copyAgreementInfoView = agreementTableInfoService.CopyAgreementInfo(viewModel.AssignmentAgreementTableItemView.AssignmentAgreementTableGUID.StringToGuid(),
                                                                                                                assignmentAgreementTable.AssignmentAgreementTableGUID, 
                                                                                                                (int)RefType.AssignmentAgreement, 
                                                                                                                (int)RefType.AssignmentAgreement);
                    IEnumerable<JointVentureTrans> jointVentureTrans = CreateJointVentureTrans(assignmentAgreementTable, viewModel.AssignmentAgreementTableItemView.AssignmentAgreementTableGUID.StringToGuid(), (int)RefType.AssignmentAgreement);
                    IEnumerable<ConsortiumTrans> consortiumTrans = consortiumTransService.CopyConsortiumTrans(viewModel.AssignmentAgreementTableItemView.AssignmentAgreementTableGUID.StringToGuid(),
                                                                                                                assignmentAgreementTable.AssignmentAgreementTableGUID, 
                                                                                                                (int)RefType.AssignmentAgreement, 
                                                                                                                (int)RefType.AssignmentAgreement);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(new List<AssignmentAgreementTable> { assignmentAgreementTable });
                        if (assignmentAgreementLines.Count > 0)
                        {
                            this.BulkInsert(assignmentAgreementLines, true);
                        }
                        if (copyAgreementInfoView != null)
                        {
                            this.BulkInsert(new List<AgreementTableInfo> { copyAgreementInfoView.AgreementTableInfo });
                            if (copyAgreementInfoView.AgreementTableInfoText != null)
                            {
                                this.BulkInsert(new List<AgreementTableInfoText>() { copyAgreementInfoView.AgreementTableInfoText });
                            }
                        }
                        if (jointVentureTrans != null)
                        {
                            this.BulkInsert(jointVentureTrans.ToList(), true);
                        }
                        if (consortiumTrans != null)
                        {
                            base.BulkInsert(consortiumTrans.ToList(), true);
                        }
                        UnitOfWork.Commit(transaction);
                    }
                  success.AddData("SUCCESS.90019", new string[] { (AgreementDocType.NoticeOfCancellation).GetAttrCode(), "LABEL.ASSIGNMENT_AGREEMENT", viewModel.AssignmentAgreementTableItemView.AssignmentAgreementId });
                    viewModel.AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID.ToString();
                    viewModel.Notification = success;
                }
                return viewModel;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private AssignmentAgreementTable CreateAssignmentAgreementTableGenNoticeOfCancellation(GenAssignmentAgmNoticeOfCancelView viewModel)
        {
            try
            {
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus draftStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Draft).ToString());
                AssignmentAgreementTable assignmentAgreementTable = new AssignmentAgreementTable()
                {
                    AssignmentAgreementTableGUID = Guid.NewGuid(),
                    AssignmentAgreementId = null,
                    ReferenceAgreementId = viewModel.AssignmentAgreementTableItemView.ReferenceAgreementId,
                    AgreementDocType = (int)AgreementDocType.NoticeOfCancellation,
                    Description = viewModel.Description,
                    AssignmentMethodGUID = viewModel.AssignmentAgreementTableItemView.AssignmentMethodGUID.StringToGuid(),
                    AgreementDate = viewModel.CancelDate.StringToDate(),
                    SigningDate = null,
                    AssignmentAgreementAmount = viewModel.AssignmentAgreementTableItemView.AssignmentAgreementAmount,
                    AcceptanceName = viewModel.AssignmentAgreementTableItemView.AcceptanceName,
                    DocumentStatusGUID = draftStatus.DocumentStatusGUID,
                    DocumentReasonGUID = viewModel.DocumentReasonGUID.StringToGuid(),
                    Remark = viewModel.ReasonRemark,
                    CustomerTableGUID = viewModel.AssignmentAgreementTableItemView.CustomerTableGUID.StringToGuid(),
                    CustomerName = viewModel.AssignmentAgreementTableItemView.CustomerName,
                    CustomerAltName = viewModel.AssignmentAgreementTableItemView.CustomerAltName,
                    ConsortiumTableGUID = viewModel.AssignmentAgreementTableItemView.ConsortiumTableGUID.StringToGuidNull(),
                    CustBankGUID = viewModel.AssignmentAgreementTableItemView.CustBankGUID.StringToGuidNull(),
                    BuyerTableGUID = viewModel.AssignmentAgreementTableItemView.BuyerTableGUID.StringToGuid(),
                    BuyerName = viewModel.AssignmentAgreementTableItemView.BuyerName,
                    RefAssignmentAgreementTableGUID = viewModel.AssignmentAgreementTableItemView.AssignmentAgreementTableGUID.StringToGuidNull(),
                    CompanyGUID = db.GetCompanyFilter().StringToGuid(),
                    AssignmentProductDescription = viewModel.AssignmentAgreementTableItemView.AssignmentProductDescription,
                    ToWhomConcern = viewModel.AssignmentAgreementTableItemView.ToWhomConcern,
                    CancelAuthorityPersonID = viewModel.AssignmentAgreementTableItemView.CancelAuthorityPersonID,
                    CancelAuthorityPersonName = viewModel.AssignmentAgreementTableItemView.CancelAuthorityPersonName,
                    CancelAuthorityPersonAddress = viewModel.AssignmentAgreementTableItemView.CancelAuthorityPersonAddress,
                    RefCreditAppRequestTableGUID = viewModel.AssignmentAgreementTableItemView.RefCreditAppRequestTableGUID.StringToGuidNull(),
                    CreditAppReqAssignmentGUID = viewModel.AssignmentAgreementTableItemView.CreditAppReqAssignmentGUID.StringToGuidNull(),
                    CustRegisteredLocation = viewModel.AssignmentAgreementTableItemView.CustRegisteredLocation
                };
                if (IsManualByAssignmentAgreement(db.GetCompanyFilter()))
                {
                    assignmentAgreementTable.InternalAssignmentAgreementId = viewModel.NoticeOfCancellationInternalAssignmentAgreementId;
                }
                else
                {
                    INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.InternalAssignmentAgreement);
                    assignmentAgreementTable.InternalAssignmentAgreementId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
                return assignmentAgreementTable;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private AssignmentAgreementLine CreateAssignmentAgreementLineGenNoticeOfCancellation(AssignmentAgreementTable assignmentAgreementTable, AssignmentAgreementLine line)
        {
            try
            {
                AssignmentAgreementLine assignmentAgreementLine = new AssignmentAgreementLine()
                {
                    AssignmentAgreementLineGUID = Guid.NewGuid(),
                    AssignmentAgreementTableGUID = assignmentAgreementTable.AssignmentAgreementTableGUID,
                    BuyerAgreementTableGUID = line.BuyerAgreementTableGUID,
                    LineNum = line.LineNum,
                    CompanyGUID = db.GetCompanyFilter().StringToGuid()
                };
                return assignmentAgreementLine;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        #endregion        
        #endregion

        #region relatedinfo
        public ConsortiumTransItemView GetConsortiumTransInitialDataByAssignmentAgreementTable(string refGUID)
        {
            try
            {
                IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                string refId = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalAssignmentAgreementId;
                return consortiumTransService.GetConsortiumTransInitialData(refId, refGUID, Models.Enum.RefType.AssignmentAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refGUID)
        {
            try
            {
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                string refId = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalAssignmentAgreementId;
                return agreementTableInfoService.GetAgreementTableInfoInitialData(refId, refGUID, Models.Enum.RefType.AssignmentAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public JointVentureTransItemView GetJointVentureTransInitialData(string refGUID)
        {
            try
            {
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                string refId = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalAssignmentAgreementId;
                return jointVentureTransService.GetJointVentureTransInitialData(refId, refGUID, Models.Enum.RefType.AssignmentAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByAssignmentAgreementTable(string refGUID)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                string refId = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalAssignmentAgreementId;
                return serviceFeeTransService.GetServiceFeeTransInitialDataByAssignmentAgreementTable(refId, refGUID, Models.Enum.RefType.AssignmentAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Get AccessMode
        public AccessModeView GetAccessModeByStatusLessThanSigned(string assignmentAgreementId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(assignmentAgreementId.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(assignmentAgreementTable.DocumentStatusGUID.Value);
                bool isLessThanSigned = (Convert.ToInt32(documentStatus.StatusId) < (int)AssignmentAgreementStatus.Signed);
                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = isLessThanSigned;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Get AccessMode

        #region Shared
        public decimal GetAssignmentBalance(Guid assignmentAgreementGUID)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
                AssignmentAgreementTable assignmentAgmTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(assignmentAgreementGUID);
                List<AssignmentAgreementSettle> assignmentAgmSettles = assignmentAgreementSettleRepo.GetAssignmentAgreementSettleByAssignmentAgreementTableGUIDNoTracking(assignmentAgreementGUID);

                decimal assignmentAmount = assignmentAgmTable != null ? assignmentAgmTable.AssignmentAgreementAmount : 0.0m;
                return assignmentAmount - assignmentAgmSettles.Sum(su => su.SettledAmount);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public SearchResult<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstanding(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                SearchCondition conditions = search.GetRefTypeConditionValue();
                int refType = conditions == null ? 0 : int.Parse(conditions.Value);
                if (refType == (int)RefType.Buyer)
                {
                    search = search.GetParentCondition(AssignmentAgreementCondition.BuyerTableGUID);
                }
                else if (refType == (int)RefType.Customer)
                {
                    search = search.GetParentCondition(AssignmentAgreementCondition.CustomerTableGUID);
                }
                else if (refType == (int)RefType.AssignmentAgreement)
                {
                    search = search.GetParentCondition(AssignmentAgreementCondition.AssignmentAgreementTableGUID);
                }
                SearchResult<AssignmentAgreementOutstandingView> assignmentAgreementOutstanding = assignmentAgreementTableRepo.GetAssignmentAgreementOutstanding(search);
                return assignmentAgreementOutstanding;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        public List<AssignmentAgreementOutstandingView> GetAssignmentAgreementOutstanding(Guid refGUID, int refType)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                List<AssignmentAgreementOutstandingView> assignmentAgreementOutstanding = new List<AssignmentAgreementOutstandingView>();
                if (RefType.Customer == (RefType)refType)
                {
                    assignmentAgreementOutstanding = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByCustomer(refGUID);
                }
                else if (RefType.Buyer == (RefType)refType)
                {
                    assignmentAgreementOutstanding = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByBuyer(refGUID);
                }
                else if (RefType.AssignmentAgreement == (RefType)refType)
                {
                    assignmentAgreementOutstanding = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByAssignmentAgreement(refGUID);
                }
                return assignmentAgreementOutstanding;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }

        }
        public CancelAssignmentAgreementView GetCancelAssignmentAgreementValue(Guid refGuid, Guid bookmarkDocumentTransGUID)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                return assignmentAgreementTableRepo.GetCancelAssignmentAgreementValue(refGuid, bookmarkDocumentTransGUID);
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }

        #region AssignmentAgreementBookmark
        public AssignmentAgreementBookmarkView GetAssignmentAgreementBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                ISharedService sharedService = new SharedService(db);
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementBookmarkView assignmentAgreementBookmarkView = assignmentAgreementTableRepo.GetAssignmentAgreementBookmarkValue(refGuid, bookmarkDocumentTransGuid);
                if (assignmentAgreementBookmarkView != null)
                {
                    assignmentAgreementBookmarkView.AssignmentDate1 = sharedService.DateToWord(assignmentAgreementBookmarkView.AssignmentDate, TextConstants.TH, TextConstants.DMY);
                    assignmentAgreementBookmarkView.AssignmentDate2 = sharedService.DateToWord(assignmentAgreementBookmarkView.AssignmentDate, TextConstants.TH, TextConstants.DMY);
                    assignmentAgreementBookmarkView.AssignmentDate3 = sharedService.DateToWord(assignmentAgreementBookmarkView.AssignmentDate, TextConstants.TH, TextConstants.DMY);
                    assignmentAgreementBookmarkView.AssignmentDate4 = sharedService.DateToWord(assignmentAgreementBookmarkView.AssignmentDate, TextConstants.TH, TextConstants.DMY);
                    assignmentAgreementBookmarkView.AssignmentDate5 = sharedService.DateToWord(assignmentAgreementBookmarkView.AssignmentDate, TextConstants.TH, TextConstants.DMY);
                    assignmentAgreementBookmarkView.CustDateRegis1 = assignmentAgreementBookmarkView.CustDateRegis == null ? "" : sharedService.DateToWord(assignmentAgreementBookmarkView.CustDateRegis.Value, TextConstants.TH, TextConstants.DMY);
                }
                return assignmentAgreementBookmarkView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemAssignmentAgreementStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.AssignmentAgreement);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Migration
        public void CreateAssignmentAgreementTableCollection(IEnumerable<AssignmentAgreementTableItemView> assignmentAgreementTablesView)
        {
            try
            {
                if (assignmentAgreementTablesView.Any())
                {
                    IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                    List<AssignmentAgreementTable> assignmentAgreementTables = assignmentAgreementTablesView.ToAssignmentAgreementTable().ToList();
                    assignmentAgreementTableRepo.ValidateAdd(assignmentAgreementTables);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(assignmentAgreementTables, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateAssignmentAgreementLineCollection(IEnumerable<AssignmentAgreementLineItemView> assignmentAgreementLinesView)
        {
            try
            {
                if (assignmentAgreementLinesView.Any())
                {
                    IAssignmentAgreementLineRepo assignmentAgreementLineRepo = new AssignmentAgreementLineRepo(db);
                    List<AssignmentAgreementLine> assignmentAgreementLines = assignmentAgreementLinesView.ToAssignmentAgreementLine().ToList();
                    assignmentAgreementLineRepo.ValidateAdd(assignmentAgreementLines);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(assignmentAgreementLines, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetDropDownItemBuyerAgeementTableByCustomerAndBuyer(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search.GetParentCondition(new string[] { AssignmentAgreementCondition.CustomerTableGUID, AssignmentAgreementCondition.BuyerTableGUID });
                return buyerAgreementTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetUpdateAssignmentagreementAmountValidation(AssignmentAgreementTableItemView vmodel)
        {

            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(vmodel.AssignmentAgreementTableGUID.StringToGuid());
                DocumentStatus Signed = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)AssignmentAgreementStatus.Signed).ToString());

                if (assignmentAgreementTable.DocumentStatusGUID != Signed.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.ASSIGNMENT_AGREEMENT" });
                }

                if (assignmentAgreementTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90035", new string[] { "LABEL.ASSIGNMENT_AGREEMENT" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool AssignmentagreementAmountValidation(UpdateAssignmentAgreementTableAmountView assignmentAgreementTableView)
        {

            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(assignmentAgreementTableView.AssignmentAgreementTableGUID.StringToGuid());

                IAssignmentMethodRepo assignmentMethodRepo = new AssignmentMethodRepo(db);
                AssignmentMethod assignmentMethod = assignmentMethodRepo.GetAssignmentMethodByIdNoTracking(assignmentAgreementTable.AssignmentMethodGUID);

                IAssignmentAgreementSettleRepo assignmentAgreementSettleRepo = new AssignmentAgreementSettleRepo(db);
                List<AssignmentAgreementSettle> assignmentAgreementSettlesList = assignmentAgreementSettleRepo.GetAssignmentAgreementSettleByAssignmentAgreementTableGUIDNoTracking(assignmentAgreementTableView.AssignmentAgreementTableGUID.StringToGuid());

                decimal sumSettledAmount = 0;

                if(assignmentAgreementSettlesList.Count() > 0)
                {
                    sumSettledAmount = assignmentAgreementSettlesList.Sum(s => s.SettledAmount);
                }

                if (assignmentMethod.ValidateAssignmentBalance == true && ((assignmentAgreementTableView.NewAssignmentAgreementAmount - sumSettledAmount) < 0))
                {
                    ex.AddData("ERROR.90172", new string[] { "LABEL.SETTLED_AMOUNT", "LABEL.ASSIGNMENT_AGREEMENT_SETTLE", "LABEL.UPDATE_ASSIGNMENT_AGREEMENT_AMOUNT", "LABEL.NEW_ASSIGNMENT_AGREEMENT_AMOUNT" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }


        public UpdateAssignmentAgreementTableAmountResult UpdateAssignmentAgreementTableAmount(UpdateAssignmentAgreementTableAmountView assignmentAgreementTableView)
        {

            try
            {
                // Validate
                AssignmentagreementAmountValidation(assignmentAgreementTableView);

                UpdateAssignmentAgreementTableAmountResult result = new UpdateAssignmentAgreementTableAmountResult();
                NotificationResponse success = new NotificationResponse();
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTableItemView assignmentAgreementTableItemView = assignmentAgreementTableRepo.GetByIdvw(assignmentAgreementTableView.AssignmentAgreementTableGUID.StringToGuid());
                assignmentAgreementTableItemView.AssignmentAgreementAmount = assignmentAgreementTableView.NewAssignmentAgreementAmount;

                UpdateAssignmentAgreementTable(assignmentAgreementTableItemView);

                success.AddData("SUCCESS.90012", new string[] { "LABEL.ASSIGNMENT_AGREEMENT", SmartAppUtil.GetDropDownLabel(assignmentAgreementTableItemView.InternalAssignmentAgreementId, assignmentAgreementTableItemView.Description) });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        public IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search)
        {

            try
            {
                search.GetParentCondition(BookmarkDocumentCondition.BookmarkDocumentRefType);
                List<SearchCondition> searchCondition = SearchConditionService.GetBookmarkDocumentTemplateCondition(((int)BookmarkDocumentRefType.AssignmentAgreement).ToString());
                search.Conditions.AddRange(searchCondition);
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return sysDropDownService.GetDropDownItemBookmarkDocumentTemplateTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetAssignmentAgreementTableByCustomerDropDown(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                search = search.GetParentCondition(AssignmentAgreementCondition.CustomerTableGUID);
                List<SearchCondition> searchConditions = SearchConditionService.GetDraftStatusCondition(((int)AssignmentAgreementStatus.Draft).ToString(), DocumentProcessId.AssignmentAgreement);
                search.Conditions.AddRange(searchConditions);
                return assignmentAgreementTableRepo.GetDropDownItemByStatusAndProcessId(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetAssignmentAgreementTableByBuyerDropDown(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                search = search.GetParentCondition(AssignmentAgreementCondition.BuyerTableGUID);
                List<SearchCondition> searchConditions = SearchConditionService.GetDraftStatusCondition(((int)AssignmentAgreementStatus.Draft).ToString(), DocumentProcessId.AssignmentAgreement);
                search.Conditions.AddRange(searchConditions);
                return assignmentAgreementTableRepo.GetDropDownItemByStatusAndProcessId(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AssignmentAgreementTableItemView>> GetAssignmentAgreementTableDropDown(SearchParameter search)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                List<SearchCondition> searchConditions = SearchConditionService.GetDraftStatusCondition(((int)AssignmentAgreementStatus.Draft).ToString(), DocumentProcessId.AssignmentAgreement);
                search.Conditions.AddRange(searchConditions);
                return assignmentAgreementTableRepo.GetDropDownItemByStatusAndProcessId(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<DocumentReasonItemView>> GetDocumentReasonDropdown(SearchParameter search)
        {
            try
            {
                search = search.GetParentCondition(DocumentReasonCondition.RefType);
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return sysDropDownService.GetDropDownItemDocumentReason(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
