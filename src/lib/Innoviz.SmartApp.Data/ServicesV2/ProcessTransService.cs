using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IProcessTransService
	{
		ProcessTransItemView GetProcessTransById(string id);
		ProcessTransItemView CreateProcessTrans(ProcessTransItemView processTransView);
		ProcessTrans CreateProcessTrans(ProcessTrans processTrans);
		ProcessTransItemView UpdateProcessTrans(ProcessTransItemView processTransView);
		bool DeleteProcessTrans(string id);
		GenInterestRealizeProcessTransView GenInterestRealizeProcessTrans(GenInterestRealizeProcessTransView genInterestRealizeProcessTransView);
		GenInterestRealizeProcessTransViewMap InitGenInterestRealizeProcessTrans(GenInterestRealizeProcessTransView genInterestRealizeProcessTransView);
		bool GenInterestRealizeProcessTransValidation(GenInterestRealizeProcessTransView genInterestRealizeProcessTransView);

		#region Shared
		ProcessTrans GenProcessTransFromVendorPaymentTrans(VendorPaymentTrans vendorPaymentTrans, Guid customerTableGUID,
																	ProductType productType, string documentId);
		List<ProcessTrans> GenProcessTransFromVendorPaymentTrans(List<VendorPaymentTrans> vendorPaymentTransList, Guid customerTableGUID,
																ProductType productType, string documentId);
		GenProcessTransFromInterestRealizedTransResultView GenProcessTransFromInterestRealizedTrans(List<InterestRealizedTrans> interestRealizedTrans, DateTime transDate);
		List<ProcessTrans> GenProcessTransFromSuspensePaymentDetail(List<PaymentDetail> paymentDetails, ProductType productType, string documentId, DateTime transDate);
		#endregion
	}
    public class ProcessTransService : SmartAppService, IProcessTransService
	{
		public ProcessTransService(SmartAppDbContext context) : base(context) { }
		public ProcessTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ProcessTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProcessTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ProcessTransService() : base() { }

		public ProcessTransItemView GetProcessTransById(string id)
		{
			try
			{
				IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
				return processTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProcessTransItemView CreateProcessTrans(ProcessTransItemView processTransView)
		{
			try
			{
				ProcessTrans processTrans = processTransView.ToProcessTrans();
				processTrans = CreateProcessTrans(processTrans);
				UnitOfWork.Commit();
				return processTrans.ToProcessTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProcessTrans CreateProcessTrans(ProcessTrans processTrans)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				processTrans = accessLevelService.AssignOwnerBU(processTrans);
				IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
				processTrans = processTransRepo.CreateProcessTrans(processTrans);
				base.LogTransactionCreate<ProcessTrans>(processTrans);
				return processTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProcessTransItemView UpdateProcessTrans(ProcessTransItemView processTransView)
		{
			try
			{
				IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
				ProcessTrans inputProcessTrans = processTransView.ToProcessTrans();
				ProcessTrans dbProcessTrans = processTransRepo.Find(inputProcessTrans.ProcessTransGUID);
				dbProcessTrans = processTransRepo.UpdateProcessTrans(dbProcessTrans, inputProcessTrans);
				base.LogTransactionUpdate<ProcessTrans>(GetOriginalValues<ProcessTrans>(dbProcessTrans), dbProcessTrans);
				UnitOfWork.Commit();
				return dbProcessTrans.ToProcessTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteProcessTrans(string item)
		{
			try
			{
				IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
				Guid processTransGUID = new Guid(item);
				ProcessTrans processTrans = processTransRepo.Find(processTransGUID);
				processTransRepo.Remove(processTrans);
				base.LogTransactionDelete<ProcessTrans>(processTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public GenInterestRealizeProcessTransView GenInterestRealizeProcessTrans(GenInterestRealizeProcessTransView genInterestRealizeProcessTransView)
		{
			try
			{
				GenInterestRealizeProcessTransView result = new GenInterestRealizeProcessTransView();
				NotificationResponse success = new NotificationResponse();

				GenInterestRealizeProcessTransViewMap genInterestRealizeProcessTransViewMap = InitGenInterestRealizeProcessTrans(genInterestRealizeProcessTransView);

				using (var transaction = UnitOfWork.ContextTransaction())
				{
					this.BulkInsert(genInterestRealizeProcessTransViewMap.ProcessTrans, false);
					this.BulkUpdate(genInterestRealizeProcessTransViewMap.InterestRealizedTrans);
					UnitOfWork.Commit(transaction);
				}

				success.AddData("SUCCESS.90019", new string[] { "LABEL.PROCESS_TRANSACTIONS", "LABEL.INTEREST_REALIZATION_TRANSACTION", genInterestRealizeProcessTransView.AsofDate });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		public GenInterestRealizeProcessTransViewMap InitGenInterestRealizeProcessTrans(GenInterestRealizeProcessTransView genInterestRealizeProcessTransView)
		{
			try
			{
				GenInterestRealizeProcessTransViewMap result = new GenInterestRealizeProcessTransViewMap();

				IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
				List<InterestRealizedTrans> interestRealizedTrans = interestRealizedTransRepo.GetInterestRealizedTransForGenInterestRealizeProcessTrans(genInterestRealizeProcessTransView.ProductType.ToList(), genInterestRealizeProcessTransView.AsofDate.StringToDate());

				// 1. Call Shared method : Method116_GenProcessTransFromInterestRealizedTrans
				GenProcessTransFromInterestRealizedTransResultView genProcessTransFromInterestRealizedTransResultView = GenProcessTransFromInterestRealizedTrans(interestRealizedTrans, genInterestRealizeProcessTransView.TransDate.StringToDate());
				result.ProcessTrans.AddRange(genProcessTransFromInterestRealizedTransResultView.ProcessTrans);
				result.InterestRealizedTrans.AddRange(genProcessTransFromInterestRealizedTransResultView.InterestRealizedTrans);

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}

		}
		public bool GenInterestRealizeProcessTransValidation(GenInterestRealizeProcessTransView genInterestRealizeProcessTransView)
		{
			try
			{
				ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);

				if (!ledgerFiscalService.IsPeriodStatusOpen(GetCurrentCompany().StringToGuid(), genInterestRealizeProcessTransView.TransDate.StringToDate()))
				{

					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90048", genInterestRealizeProcessTransView.TransDate);
					throw ex;
				}
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region Shared
		public ProcessTrans GenProcessTransFromVendorPaymentTrans(VendorPaymentTrans vendorPaymentTrans, Guid customerTableGUID, 
																	ProductType productType, string documentId)
        {
            try
            {
				var result = GenProcessTransFromVendorPaymentTrans(vendorPaymentTransList: new List<VendorPaymentTrans>() { vendorPaymentTrans },
																	customerTableGUID: customerTableGUID,
																	productType: productType,
																	documentId: documentId);
				return result.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public List<ProcessTrans> GenProcessTransFromVendorPaymentTrans(List<VendorPaymentTrans> vendorPaymentTransList, Guid customerTableGUID,
																ProductType productType, string documentId)
        {
            try
            {
				IExchangeRateService exchangeRateService = new ExchangeRateService(db);
				IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);

				List<VendorTable> vendorTables = vendorTableRepo.GetVendorTableByIdNoTracking(
					vendorPaymentTransList.Where(w => w.VendorTableGUID.HasValue).Select(s => s.VendorTableGUID.Value).ToList());

				List<ProcessTrans> result =
					(from vendorPaymTrans in vendorPaymentTransList
					 join vendorTable in vendorTables
					 on vendorPaymTrans.VendorTableGUID equals vendorTable.VendorTableGUID into ljVendorPaymTransVendorTable

					 from vendorTable in ljVendorPaymTransVendorTable.DefaultIfEmpty()
					 select new ProcessTrans
					 {
						 CustomerTableGUID = customerTableGUID,
						 CreditAppTableGUID = vendorPaymTrans.CreditAppTableGUID,
						 ProductType = (int)productType,
						 TransDate = vendorPaymTrans.PaymentDate.Value,
						 ProcessTransType = (int)ProcessTransType.VendorPayment,
						 ARLedgerAccount = string.Empty,
						 Amount = vendorPaymTrans.TotalAmount,
						 TaxAmount = vendorPaymTrans.TaxAmount,
						 CurrencyGUID = (vendorTable != null) ? vendorTable.CurrencyGUID : Guid.Empty,
						 ExchangeRate = (vendorTable != null) ? exchangeRateService.GetExchangeRate(vendorTable.CurrencyGUID, vendorPaymTrans.PaymentDate.Value) : 1,
						 DocumentReasonGUID = null,
						 RefTaxInvoiceGUID = null,
						 TaxTableGUID = vendorPaymTrans.TaxTableGUID,
						 OrigTaxTableGUID = null,
						 RefType = (int)RefType.VendorPaymentTrans,
						 RefGUID = vendorPaymTrans.VendorPaymentTransGUID,
						 DocumentId = documentId,
						 PaymentHistoryGUID = null,
						 Revert = false,
						 RefProcessTransGUID = null,
						 StagingBatchId = string.Empty,
						 StagingBatchStatus = (int)StagingBatchStatus.None,
						 ProcessTransGUID = Guid.NewGuid(),
						 CompanyGUID = vendorPaymTrans.CompanyGUID,
						 SourceRefType = vendorPaymTrans.RefType,
						 SourceRefId = documentId
					 }).ToList();
				result = result.Select(s =>
				{
					s.AmountMST = s.Amount * s.ExchangeRate;
					s.TaxAmountMST = s.TaxAmount * s.ExchangeRate;
					return s;
				}).ToList();
				return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
		}
		public GenProcessTransFromInterestRealizedTransResultView GenProcessTransFromInterestRealizedTrans(List<InterestRealizedTrans> interestRealizedTrans, DateTime transDate)
		{
			try
			{
				ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
				IExchangeRateService exchangeRateService = new ExchangeRateService(db);
				IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
				IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
				IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
				IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				GenProcessTransFromInterestRealizedTransResultView result = new GenProcessTransFromInterestRealizedTransResultView();

                #region Validate
                if (!ledgerFiscalService.IsPeriodStatusOpen(GetCurrentCompany().StringToGuid(), transDate))
				{

					SmartAppException ex = new SmartAppException("ERROR.ERROR");
					ex.AddData("ERROR.90048", transDate.DateToString());
					throw ex;
				}
				#endregion Validate

				#region 1.Create ProcessTrans From InterestRealizedTrans
				#region RefType PurchaseLine
				List<InterestRealizedTrans> interestRealizedTransRefPurchaseLine = new List<InterestRealizedTrans>();
				interestRealizedTransRefPurchaseLine = interestRealizedTrans.Where(w => w.RefType == (int)RefType.PurchaseLine).ToList();
				List<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByIdNoTracking(interestRealizedTransRefPurchaseLine.Select(s => s.RefGUID).Distinct());
				List<PurchaseTable> purchaseTables = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseLines.Select(s => s.PurchaseTableGUID).Distinct());
				List<CustomerTable> customerTablesByPurchase = customerTableRepo.GetCustomerTableByIdNoTracking(purchaseTables.Select(s => s.CustomerTableGUID).Distinct());
				var exchangeRateByPurchase = customerTablesByPurchase.Select(s => new 
				{ 
					s.CustomerTableGUID,
					ExchangeRate = exchangeRateService.GetExchangeRate(s.CurrencyGUID, transDate)
				});

				List<ProcessTrans> processTransByPurchaseLine = new List<ProcessTrans>();
				purchaseTables.ForEach(purchaseTable => {
					IEnumerable<Guid> purchaseLineGUIDsByPurchaseTable = purchaseLines.Where(w => w.PurchaseTableGUID == purchaseTable.PurchaseTableGUID).Select(s => s.PurchaseLineGUID);
					processTransByPurchaseLine.AddRange(interestRealizedTransRefPurchaseLine.Where(w => purchaseLineGUIDsByPurchaseTable.Contains(w.RefGUID)).Select(s => new ProcessTrans
					{
                        #region 1.1
                        CustomerTableGUID = purchaseTable.CustomerTableGUID,
						CreditAppTableGUID = purchaseTable.CreditAppTableGUID,
						ProductType = purchaseTable.ProductType,
						CurrencyGUID = customerTablesByPurchase.Where(w=> w.CustomerTableGUID == purchaseTable.CustomerTableGUID).FirstOrDefault().CurrencyGUID,
						DocumentId = purchaseTable.PurchaseId,
						SourceRefType = (int)RefType.PurchaseTable,
						SourceRefId = purchaseTable.PurchaseId,
						#endregion 1.1
						#region 1.2
						TransDate = transDate,
						ProcessTransType = (int)ProcessTransType.IncomeRealization,
						ARLedgerAccount = string.Empty,
						ExchangeRate = exchangeRateByPurchase.Where(w => w.CustomerTableGUID == purchaseTable.CustomerTableGUID).FirstOrDefault().ExchangeRate,
						Amount = s.AccInterestAmount,
						AmountMST = s.AccInterestAmount * exchangeRateByPurchase.Where(w => w.CustomerTableGUID == purchaseTable.CustomerTableGUID).FirstOrDefault().ExchangeRate,
						TaxAmount = 0,
						TaxAmountMST = 0,
						DocumentReasonGUID = null,
						RefTaxInvoiceGUID = null,
						TaxTableGUID = null,
						OrigTaxTableGUID = null,
						RefType = (int)RefType.InterestRealizedTrans,
						RefGUID = s.InterestRealizedTransGUID,
						InvoiceTableGUID = null,
						PaymentHistoryGUID = null,
						Revert = false,
						RefProcessTransGUID = null,
						StagingBatchId = string.Empty,
						StagingBatchStatus = (int)StagingBatchStatus.None,
						ProcessTransGUID = Guid.NewGuid(),
						CompanyGUID = s.CompanyGUID
						#endregion 1.2

					}));
				});
				result.InterestRealizedTrans.AddRange(interestRealizedTransRefPurchaseLine);
				result.ProcessTrans.AddRange(processTransByPurchaseLine);
				#endregion RefType PurchaseLine
				#region RefType WithdrawalLine
				List<InterestRealizedTrans> interestRealizedTransRefWithdrawalLine = interestRealizedTrans.Where(w => w.RefType == (int)RefType.WithdrawalLine).ToList();
				List<WithdrawalLine> withdrawalLines = withdrawalLineRepo.GetWithdrawalLineByIdNoTracking(interestRealizedTransRefWithdrawalLine.Select(s => s.RefGUID).Distinct());
				List<WithdrawalTable> withdrawalTables = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalLines.Select(s => s.WithdrawalTableGUID).Distinct());
				List<CustomerTable> customerTablesByWithdrawal = customerTableRepo.GetCustomerTableByIdNoTracking(withdrawalTables.Select(s => s.CustomerTableGUID).Distinct());
				var exchangeRateByWithdrawal = customerTablesByWithdrawal.Select(s => new
				{
					s.CustomerTableGUID,
					ExchangeRate = exchangeRateService.GetExchangeRate(s.CurrencyGUID, transDate)
				});

				List<ProcessTrans> processTransByWithdrawalLine = new List<ProcessTrans>();
				withdrawalTables.ForEach(withdrawalTable => {
					IEnumerable<Guid> withdrawalLineGUIDsByWithdrawalTable = withdrawalLines.Where(w => w.WithdrawalTableGUID == withdrawalTable.WithdrawalTableGUID).Select(s => s.WithdrawalLineGUID);
					processTransByWithdrawalLine.AddRange(interestRealizedTransRefWithdrawalLine.Where(w => withdrawalLineGUIDsByWithdrawalTable.Contains(w.RefGUID)).Select(s => new ProcessTrans
					{
						#region 1.1
						CustomerTableGUID = withdrawalTable.CustomerTableGUID,
						CreditAppTableGUID = withdrawalTable.CreditAppTableGUID,
						ProductType = withdrawalTable.ProductType,
						CurrencyGUID = customerTablesByWithdrawal.Where(w => w.CustomerTableGUID == withdrawalTable.CustomerTableGUID).FirstOrDefault().CurrencyGUID,
						DocumentId = withdrawalTable.WithdrawalId,
						SourceRefType = (int)RefType.WithdrawalTable,
						SourceRefId = withdrawalTable.WithdrawalId,
						#endregion 1.1
						#region 1.2
						TransDate = transDate,
						ProcessTransType = (int)ProcessTransType.IncomeRealization,
						ARLedgerAccount = string.Empty,
						ExchangeRate = exchangeRateByWithdrawal.Where(w => w.CustomerTableGUID == withdrawalTable.CustomerTableGUID).FirstOrDefault().ExchangeRate,
						Amount = s.AccInterestAmount,
						AmountMST = s.AccInterestAmount * exchangeRateByWithdrawal.Where(w => w.CustomerTableGUID == withdrawalTable.CustomerTableGUID).FirstOrDefault().ExchangeRate,
						TaxAmount = 0,
						TaxAmountMST = 0,
						DocumentReasonGUID = null,
						RefTaxInvoiceGUID = null,
						TaxTableGUID = null,
						OrigTaxTableGUID = null,
						RefType = (int)RefType.InterestRealizedTrans,
						RefGUID = s.InterestRealizedTransGUID,
						InvoiceTableGUID = null,
						PaymentHistoryGUID = null,
						Revert = false,
						RefProcessTransGUID = null,
						StagingBatchId = string.Empty,
						StagingBatchStatus = (int)StagingBatchStatus.None,
						ProcessTransGUID = Guid.NewGuid(),
						CompanyGUID = s.CompanyGUID
						#endregion 1.2

					}));
				});
				result.InterestRealizedTrans.AddRange(interestRealizedTransRefWithdrawalLine);
				result.ProcessTrans.AddRange(processTransByWithdrawalLine);
				#endregion RefType WithdrawalLine
				#endregion 1.Create ProcessTrans From InterestRealizedTrans
				#region 2.Update InterestRealizedTrans From ProcessTrans
				result.InterestRealizedTrans.Select(s =>
				{
					s.RefProcessTransGUID = result.ProcessTrans.Where(w => w.RefGUID == s.InterestRealizedTransGUID).FirstOrDefault().ProcessTransGUID;
					return s;
				}).ToList();
				#endregion 2.Update InterestRealizedTrans From ProcessTrans

				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<ProcessTrans> GenProcessTransFromSuspensePaymentDetail(List<PaymentDetail> paymentDetails, ProductType productType, string documentId, DateTime transDate)
		{
			try
			{
				IExchangeRateService exchangeRateService = new ExchangeRateService(db);
				IInvoiceLineService invoiceLineService = new InvoiceLineService(db);
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);

				List<PaymentDetail> paymentDetailsSuspenseAndPaymentAmountNotZero = paymentDetails.Where(w => w.PaidToType == (int)PaidToType.Suspense && w.PaymentAmount != 0).ToList();
				List<CustomerTable> customerTables = customerTableRepo.GetCustomerTableByIdNoTracking(paymentDetailsSuspenseAndPaymentAmountNotZero.Where( w => w.CustomerTableGUID.HasValue).GroupBy(g => g.CustomerTableGUID.Value).Select(s => s.Key));
				List<InvoiceType> invoiceTypes = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(paymentDetailsSuspenseAndPaymentAmountNotZero.Where(w => w.InvoiceTypeGUID.HasValue).GroupBy(g => g.InvoiceTypeGUID.Value).Select(s => s.Key));
				var exchangeRateByCurrency = customerTables.GroupBy(g => g.CurrencyGUID).Select(s => new {
					CurrencyGUID = s.Key,
					ExchangeRate = exchangeRateService.GetExchangeRate(s.Key, transDate) // Shared method : Method029_ GetExchangeRate
				});

				List<ProcessTrans> result = new List<ProcessTrans>();
				paymentDetailsSuspenseAndPaymentAmountNotZero.ForEach(paymentDetail =>
				{
					CustomerTable customerTable = customerTables.Where(w => w.CustomerTableGUID == paymentDetail.CustomerTableGUID).FirstOrDefault();
					decimal exchangeRate = (customerTable != null) ? exchangeRateByCurrency.Where(w => w.CurrencyGUID == customerTable.CurrencyGUID).FirstOrDefault().ExchangeRate : 1;

					// Shared method : Method098_CalcInvoiceLineFromInvoiceRevenueType 
					InvoiceLineAmountParamView invoiceLineAmountParamView = new InvoiceLineAmountParamView
					{
						IssuedDate = transDate,
						InvoiceAmount = paymentDetail.PaymentAmount,
						IncludeTax = true,
						InvoiceRevenueTypeGUID = invoiceTypes.Where(w => w.InvoiceTypeGUID == paymentDetail.InvoiceTypeGUID).FirstOrDefault().AutoGenInvoiceRevenueTypeGUID
					};
					InvoiceLineAmountResultView invoiceLineAmountResultView = invoiceLineService.CalcInvoiceLineFromInvoiceRevenueType(invoiceLineAmountParamView);
					
					ProcessTrans processTrans = new ProcessTrans
					{
						CustomerTableGUID = paymentDetail.CustomerTableGUID.HasValue ? paymentDetail.CustomerTableGUID.Value : Guid.Empty,
						CreditAppTableGUID = null,
						ProductType = (int)productType,
						TransDate = transDate,
						ProcessTransType = (int)ProcessTransType.PaymentDetail,
						ARLedgerAccount = string.Empty,
						CurrencyGUID = (customerTable != null) ? customerTable.CurrencyGUID : Guid.Empty,
						ExchangeRate = exchangeRate,
						Amount = paymentDetail.PaymentAmount,
						AmountMST = (paymentDetail.PaymentAmount * exchangeRate).Round(),
						TaxAmount = invoiceLineAmountResultView.TaxAmount,
						TaxAmountMST = (invoiceLineAmountResultView.TaxAmount * exchangeRate).Round(),
						DocumentReasonGUID = null,
						RefTaxInvoiceGUID = null,
						TaxTableGUID = invoiceLineAmountResultView.TaxTableGUID,
						OrigTaxTableGUID = null,
						RefType = paymentDetail.RefType,
						RefGUID = paymentDetail.RefGUID,
						DocumentId = documentId,
						PaymentHistoryGUID = null,
						Revert = false,
						RefProcessTransGUID = null,
						StagingBatchId = string.Empty,
						StagingBatchStatus = (int)StagingBatchStatus.None,
						PaymentDetailGUID = paymentDetail.PaymentDetailGUID,
						ProcessTransGUID = Guid.NewGuid(),
						CompanyGUID = paymentDetail.CompanyGUID,
						SourceRefType = paymentDetail.RefType,
						SourceRefId = documentId
					};
					result.Add(processTrans);
				});
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
