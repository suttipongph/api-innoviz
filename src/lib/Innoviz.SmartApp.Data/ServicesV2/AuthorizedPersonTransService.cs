using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAuthorizedPersonTransService
	{
		AuthorizedPersonTransItemView GetAuthorizedPersonTransById(string id);
		AuthorizedPersonTransItemView CreateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView);
		AuthorizedPersonTransItemView UpdateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView);
		bool DeleteAuthorizedPersonTrans(string id);
		AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refId, string refGUID, RefType refType);
		IEnumerable<AuthorizedPersonTrans> GetActiveAuthorizedPersonTrans(Guid refGUID, int refType);
		IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByCustomer(SearchParameter search);
		IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByCreditApp(SearchParameter search);
		IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByBuyer(SearchParameter search);
		void CreateAuthorizedPersonTransCollection(List<AuthorizedPersonTransItemView> authorizedPersonTransItemViews);
		#region K2 function
		public IEnumerable<AuthorizedPersonTrans> CopyAuthorizedPersonTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, bool IsAmend = false, string owner = null, Guid? ownerBusinessUnitGUID = null);
		#endregion
	}
    public class AuthorizedPersonTransService : SmartAppService, IAuthorizedPersonTransService
    {
        public AuthorizedPersonTransService(SmartAppDbContext context) : base(context) { }
        public AuthorizedPersonTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public AuthorizedPersonTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public AuthorizedPersonTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public AuthorizedPersonTransService() : base() { }

		public AuthorizedPersonTransItemView GetAuthorizedPersonTransById(string id)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				return authorizedPersonTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonTransItemView CreateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				authorizedPersonTransView = accessLevelService.AssignOwnerBU(authorizedPersonTransView);
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				AuthorizedPersonTrans authorizedPersonTrans = authorizedPersonTransView.ToAuthorizedPersonTrans();
				authorizedPersonTrans = authorizedPersonTransRepo.CreateAuthorizedPersonTrans(authorizedPersonTrans);
				base.LogTransactionCreate<AuthorizedPersonTrans>(authorizedPersonTrans);
				UnitOfWork.Commit();
				return authorizedPersonTrans.ToAuthorizedPersonTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonTransItemView UpdateAuthorizedPersonTrans(AuthorizedPersonTransItemView authorizedPersonTransView)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				AuthorizedPersonTrans inputAuthorizedPersonTrans = authorizedPersonTransView.ToAuthorizedPersonTrans();
				AuthorizedPersonTrans dbAuthorizedPersonTrans = authorizedPersonTransRepo.Find(inputAuthorizedPersonTrans.AuthorizedPersonTransGUID);
				dbAuthorizedPersonTrans = authorizedPersonTransRepo.UpdateAuthorizedPersonTrans(dbAuthorizedPersonTrans, inputAuthorizedPersonTrans);
				base.LogTransactionUpdate<AuthorizedPersonTrans>(GetOriginalValues<AuthorizedPersonTrans>(dbAuthorizedPersonTrans), dbAuthorizedPersonTrans);
				UnitOfWork.Commit();
				return dbAuthorizedPersonTrans.ToAuthorizedPersonTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAuthorizedPersonTrans(string item)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				Guid authorizedPersonTransGUID = new Guid(item);
				AuthorizedPersonTrans authorizedPersonTrans = authorizedPersonTransRepo.Find(authorizedPersonTransGUID);
				authorizedPersonTransRepo.Remove(authorizedPersonTrans);
				base.LogTransactionDelete<AuthorizedPersonTrans>(authorizedPersonTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AuthorizedPersonTransItemView GetAuthorizedPersonTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new AuthorizedPersonTransItemView
				{
					AuthorizedPersonTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<AuthorizedPersonTrans> GetActiveAuthorizedPersonTrans(Guid refGUID, int refType)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				return authorizedPersonTransRepo.GetAuthorizedPersonTransByReference(refGUID, refType).Where(w => w.InActive == false);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region migration
		public void CreateAuthorizedPersonTransCollection(List<AuthorizedPersonTransItemView> authorizedPersonTransItemViews)
		{
			try
			{
				if (authorizedPersonTransItemViews.Any())
				{
					IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
					List<AuthorizedPersonTrans> authorizedPersonTrans = authorizedPersonTransItemViews.ToAuthorizedPersonTrans().ToList();
					authorizedPersonTransRepo.ValidateAdd(authorizedPersonTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(authorizedPersonTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion

		#region DropdownByCondition
		public IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByCustomer(SearchParameter search)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				search = search.GetParentCondition(AuthorizedPersonTransCondition.RefGUID);
				SearchCondition searchCondition = SearchConditionService.GetAuthorizedPersonTransRefTypeCondition((int)RefType.Customer);
				search.Conditions.Add(searchCondition);
				return authorizedPersonTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByCreditApp(SearchParameter search)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				search = search.GetParentCondition(AuthorizedPersonTransCondition.RefGUID);
				SearchCondition searchCondition = SearchConditionService.GetAuthorizedPersonTransRefTypeCondition((int)RefType.CreditAppTable);
				search.Conditions.Add(searchCondition);
				return authorizedPersonTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByBuyer(SearchParameter search)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				search = search.GetParentCondition(AuthorizedPersonTransCondition.RefGUID);
				SearchCondition searchCondition = SearchConditionService.GetAuthorizedPersonTransRefTypeCondition((int)RefType.Buyer);
				search.Conditions.Add(searchCondition);
				return authorizedPersonTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropdownByCondition
		#region K2 function
		public IEnumerable<AuthorizedPersonTrans> CopyAuthorizedPersonTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, bool IsAmend = false, string owner = null, Guid? ownerBusinessUnitGUID = null)
		{
			try
			{
				IAuthorizedPersonTransRepo authorizedPersonTransRepo = new AuthorizedPersonTransRepo(db);
				List<AuthorizedPersonTrans> authorizedPersonTrans = authorizedPersonTransRepo.GetAuthorizedPersonTransByReference(fromRefGUID, fromRefType).ToList();
				if (authorizedPersonTrans.Any())
				{
					if (fromInactive != null)
					{
						authorizedPersonTrans = authorizedPersonTrans.Where(w => w.InActive == fromInactive.Value).ToList();
					}
					int ordering = 1;
					authorizedPersonTrans.OrderBy(o => o.Ordering).ToList().ForEach(f =>
					{
						f.AuthorizedPersonTransGUID = Guid.NewGuid();
						f.RefGUID = toRefGUID;
						f.RefType = toRefType;
						f.Ordering = ordering;
						f.Owner = owner;
						f.OwnerBusinessUnitGUID = ownerBusinessUnitGUID;
						if (IsAmend)
						{
							f.RefAuthorizedPersonTransGUID = f.AuthorizedPersonTransGUID;
						}
						ordering++;
					});
				}
				return authorizedPersonTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
