using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface INationalityService
	{

		NationalityItemView GetNationalityById(string id);
		NationalityItemView CreateNationality(NationalityItemView nationalityView);
		NationalityItemView UpdateNationality(NationalityItemView nationalityView);
		bool DeleteNationality(string id);
	}
	public class NationalityService : SmartAppService, INationalityService
	{
		public NationalityService(SmartAppDbContext context) : base(context) { }
		public NationalityService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public NationalityService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public NationalityService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public NationalityService() : base() { }

		public NationalityItemView GetNationalityById(string id)
		{
			try
			{
				INationalityRepo nationalityRepo = new NationalityRepo(db);
				return nationalityRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NationalityItemView CreateNationality(NationalityItemView nationalityView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				nationalityView = accessLevelService.AssignOwnerBU(nationalityView);
				INationalityRepo nationalityRepo = new NationalityRepo(db);
				Nationality nationality = nationalityView.ToNationality();
				nationality = nationalityRepo.CreateNationality(nationality);
				base.LogTransactionCreate<Nationality>(nationality);
				UnitOfWork.Commit();
				return nationality.ToNationalityItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public NationalityItemView UpdateNationality(NationalityItemView nationalityView)
		{
			try
			{
				INationalityRepo nationalityRepo = new NationalityRepo(db);
				Nationality inputNationality = nationalityView.ToNationality();
				Nationality dbNationality = nationalityRepo.Find(inputNationality.NationalityGUID);
				dbNationality = nationalityRepo.UpdateNationality(dbNationality, inputNationality);
				base.LogTransactionUpdate<Nationality>(GetOriginalValues<Nationality>(dbNationality), dbNationality);
				UnitOfWork.Commit();
				return dbNationality.ToNationalityItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteNationality(string item)
		{
			try
			{
				INationalityRepo nationalityRepo = new NationalityRepo(db);
				Guid nationalityGUID = new Guid(item);
				Nationality nationality = nationalityRepo.Find(nationalityGUID);
				nationalityRepo.Remove(nationality);
				base.LogTransactionDelete<Nationality>(nationality);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
