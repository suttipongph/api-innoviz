using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IMessengerJobTableService
    {

        MessengerJobTableItemView GetMessengerJobByIdForFunction(string id);
        MessengerJobTableItemView GetMessengerJobTableById(string id);
        MessengerJobTableItemView CreateMessengerJobTable(MessengerJobTableItemView messengerJobTableView);
        MessengerJobTableItemView UpdateMessengerJobTable(MessengerJobTableItemView messengerJobTableView);
        bool DeleteMessengerJobTable(string id);
        bool GetNumberSeqParameter(string companyId);
        MessengerJobTableItemView GetMessengerJobInitialData(string companyGUID);
        MessengerJobTableItemView GetMessengerJobRequestInitialData(string refGUID);
        MessengerJobTableItemView GetMessengerJobRequestPuchaseLineInitialData(string refGUID, string userId, string companyId);
        MessengerJobTableItemView GetMessengerJobRequestWithdrawalInitialData(int refType, string refGUID);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByMessengerJobTable(string refGUID);
        AccessModeView GetMessengerJobTableAcessMode(string messengerjobtableId);
        IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTrans(SearchParameter search);
        IEnumerable<SelectItem<ContactPersonTransItemView>> GetContactPersonDropDown(SearchParameter search);
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusDocumentProcessDropdown(SearchParameter search);
        PrintMessengerJobView GetPrintMessengerJobById(string messengerJobTableGUID);
        #region function
        bool ValidateFunctionAssign(string id);
        bool ValidateFunctionPost(string id);
        bool ValidateFunctionCancel(string id);
        ResultBaseEntity UpdateCancelMessengerJob(string id);
        ResultBaseEntity UpdatePostMessengerJob(string id);
        ResultBaseEntity UpdateAssignMessengerJob(string id);
        #endregion function

    }
    public class MessengerJobTableService : SmartAppService, IMessengerJobTableService
    {
        public MessengerJobTableService(SmartAppDbContext context) : base(context) { }
        public MessengerJobTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public MessengerJobTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public MessengerJobTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public MessengerJobTableService() : base() { }

        public MessengerJobTableItemView GetMessengerJobTableById(string id)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                return messengerJobTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MessengerJobTableItemView GetMessengerJobByIdForFunction(string id)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                return messengerJobTableRepo.GetByIdvwBy(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MessengerJobTableItemView CreateMessengerJobTable(MessengerJobTableItemView messengerJobTableView)
        {
            try
            {

                if (!GetNumberSeqParameter(db.GetCompanyFilter()))
                {
                    INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(db.GetCompanyFilter().StringToGuid(), ReferenceId.MessengerJob);
                    messengerJobTableView.JobId = numberSequenceService.GetNumber(db.GetCompanyFilter().StringToGuid(), db.GetBranchFilter().StringToGuid(), numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());

                }
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);


                messengerJobTableView = accessLevelService.AssignOwnerBU(messengerJobTableView);
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);

                MessengerJobTable messengerJobTable = messengerJobTableView.ToMessengerJobTable();

                messengerJobTable = messengerJobTableRepo.CreateMessengerJobTable(messengerJobTable);
                base.LogTransactionCreate<MessengerJobTable>(messengerJobTable);
                UnitOfWork.Commit();
                return messengerJobTable.ToMessengerJobTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MessengerJobTableItemView UpdateMessengerJobTable(MessengerJobTableItemView messengerJobTableView)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                MessengerJobTable inputMessengerJobTable = messengerJobTableView.ToMessengerJobTable();
                MessengerJobTable dbMessengerJobTable = messengerJobTableRepo.Find(inputMessengerJobTable.MessengerJobTableGUID);
                dbMessengerJobTable = messengerJobTableRepo.UpdateMessengerJobTable(dbMessengerJobTable, inputMessengerJobTable);
                base.LogTransactionUpdate<MessengerJobTable>(GetOriginalValues<MessengerJobTable>(dbMessengerJobTable), dbMessengerJobTable);
                UnitOfWork.Commit();
                return dbMessengerJobTable.ToMessengerJobTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteMessengerJobTable(string item)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                Guid messengerJobTableGUID = new Guid(item);
                MessengerJobTable messengerJobTable = messengerJobTableRepo.Find(messengerJobTableGUID);
                messengerJobTableRepo.Remove(messengerJobTable);
                base.LogTransactionDelete<MessengerJobTable>(messengerJobTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetNumberSeqParameter(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(companyId.StringToGuid(), ReferenceId.MessengerJob);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public MessengerJobTableItemView GetMessengerJobRequestInitialData(string refGUID)
        {
            try
            {
                IWithdrawalLineRepo withdrawalLine = new WithdrawalLineRepo(db);
                WithdrawalTableItemViewMap withdrawalTableItemViewMap = withdrawalLine.GetWithdrawalTable(refGUID.StringToGuid());
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IDocumentService documentService = new DocumentService(db);
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)MessengerJobStatus.Draft);
                MessengerJobTableItemView messengerJobTableItem = new MessengerJobTableItemView();
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine creditAppLine = new CreditAppLine();
                if (withdrawalTableItemViewMap.CreditAppLineGUID != null)
                {
                    creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTrackingByAccessLevel((Guid)withdrawalTableItemViewMap.CreditAppLineGUID);

                }


                messengerJobTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                messengerJobTableItem.RefType = (int)RefType.WithdrawalLine;
                messengerJobTableItem.RefGUID = refGUID;
                messengerJobTableItem.ProductType = withdrawalTableItemViewMap.ProductType;
                messengerJobTableItem.BuyerTableGUID = withdrawalTableItemViewMap.BuyerTableGUID.GuidNullToString();
                messengerJobTableItem.CustomerTableGUID = withdrawalTableItemViewMap.CustomerTableGUID.GuidNullToString();
                messengerJobTableItem.RefCreditAppLineGUID = withdrawalTableItemViewMap.CreditAppLineGUID.GuidNullToString();
                messengerJobTableItem.RefID = withdrawalTableItemViewMap.WithdrawalLine_Values;
                messengerJobTableItem.RefCreditAppLine_Values = SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString());
                return messengerJobTableItem;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MessengerJobTableItemView GetMessengerJobRequestPuchaseLineInitialData(string refGUID, string userId, string companyId)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)MessengerJobStatus.Draft);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                PurchaseTable purchaseTable = new PurchaseTable();
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                EmployeeTableItemView employeeTable = employeeTableRepo.GetEmployeeTableByUserIdAndCompany(userId, companyId);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLineItemView creditAppLine = creditAppLineRepo.GetByIdvw(purchaseLine.CreditAppLineGUID);

                if (purchaseLine.PurchaseTableGUID != null)
                {
                    purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(purchaseLine.PurchaseTableGUID);
                }
                MessengerJobTableItemView messengerJobTableItem = new MessengerJobTableItemView();
                messengerJobTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                messengerJobTableItem.RequestorGUID = employeeTable.EmployeeTableGUID;
                messengerJobTableItem.RefType = (int)RefType.PurchaseLine;
                messengerJobTableItem.RefGUID = refGUID;
                messengerJobTableItem.ProductType = purchaseTable.ProductType;
                messengerJobTableItem.BuyerTableGUID = purchaseLine.BuyerTableGUID.GuidNullToString();
                messengerJobTableItem.CustomerTableGUID = purchaseTable.CustomerTableGUID.GuidNullToString();
                messengerJobTableItem.RefCreditAppLineGUID = purchaseLine.CreditAppLineGUID.GuidNullToString();
                messengerJobTableItem.RefCreditAppLine_Values = (creditAppLine != null) ? SmartAppUtil.GetDropDownLabel(creditAppLine.LineNum.ToString(), creditAppLine.ApprovedCreditLimitLine.ToString()) : null;
                messengerJobTableItem.RefID = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseLine.LineNum.ToString());
                return messengerJobTableItem;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MessengerJobTableItemView GetMessengerJobRequestWithdrawalInitialData(int refType, string refGUID)
        {
            try
            {
                IWithdrawalLineRepo withdrawalLine = new WithdrawalLineRepo(db);
                WithdrawalTableItemViewMap withdrawalTableItemViewMap = withdrawalLine.GetWithdrawalTable(refGUID.StringToGuid());
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IDocumentService documentService = new DocumentService(db);
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)MessengerJobStatus.Draft);
                MessengerJobTableItemView messengerJobTableItem = new MessengerJobTableItemView();
                messengerJobTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                messengerJobTableItem.RefType = refType;
                messengerJobTableItem.RefGUID = refGUID;
                messengerJobTableItem.ProductType = withdrawalTableItemViewMap.ProductType;
                messengerJobTableItem.BuyerTableGUID = withdrawalTableItemViewMap.BuyerTableGUID.GuidNullToString();
                messengerJobTableItem.CustomerTableGUID = withdrawalTableItemViewMap.CustomerTableGUID.GuidNullToString();
                messengerJobTableItem.RefCreditAppLineGUID = withdrawalTableItemViewMap.CreditAppLineGUID.GuidNullToString();
                messengerJobTableItem.RefID = withdrawalTableItemViewMap.WithdrawalLine_Values;
                return messengerJobTableItem;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MessengerJobTableItemView GetMessengerJobInitialData(string companyGUID)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IDocumentService documentService = new DocumentService(db);
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                var employee = employeeTableService.GetEmployeeTableByUserIdAndCompany(db.GetUserId(), companyGUID);
                DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)MessengerJobStatus.Draft);
                MessengerJobTableItemView messengerJobTableItem = new MessengerJobTableItemView();
                messengerJobTableItem.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                messengerJobTableItem.RequestorGUID = employee.EmployeeTableGUID;
                messengerJobTableItem.RefType = (int)RefType.MessengerJob;
                return messengerJobTableItem;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTrans(SearchParameter search)
        {
            ISysDropDownService sysDropDownService = new SysDropDownService(db);
            search = search.GetParentCondition(new string[] { AddressTransCondition.RefGUID, AddressTransCondition.RefType });
            return sysDropDownService.GetDropDownItemAddressTrans(search);
        }


        public IEnumerable<SelectItem<ContactPersonTransItemView>> GetContactPersonDropDown(SearchParameter search)
        {
            ISysDropDownService sysDropDownService = new SysDropDownService(db);
            search = search.GetParentCondition(new string[] { AddressTransCondition.RefGUID, AddressTransCondition.RefType });
            SearchCondition searchConditions = SearchConditionService.GetContactPersonTransItemByInActiveCondition(false);
            search.Conditions.Add(searchConditions);
            return sysDropDownService.GetDropDownItemContactPersonTrans(search);
        }

        public bool ValidateFunctionAssign(string id)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                var messengerJobTable = messengerJobTableRepo.GetByIdvw(id.StringToGuid());
                if (messengerJobTable.DocumentStatus_StatusId != ((int)MessengerJobStatus.Draft).ToString())
                {
                    ex.AddData("ERROR.90012", new string[] { "LABEL.MESSENGER_JOB" });
                }
                if (messengerJobTable.MessengerTableGUID == null)
                {
                    ex.AddData("ERROR.90042", new string[] { "LABEL.MESSENGER" });
                }
                if (messengerJobTable.JobDate == null)
                {
                    ex.AddData("ERROR.90042", new string[] { "LABEL.JOB_DATE" });
                }
                if (messengerJobTable.JobStartTime == null || messengerJobTable.JobEndTime == null)
                {
                    ex.AddData("ERROR.90156", new string[] { "LABEL.JOB_START_TIME", "LABEL.JOB_END_TIME" });

                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        public bool ValidateFunctionPost(string id)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                var messengerJobTable = messengerJobTableRepo.GetByIdvw(id.StringToGuid());
                if (messengerJobTable.DocumentStatus_StatusId != ((int)MessengerJobStatus.Assigned).ToString())
                {
                    ex.AddData("ERROR.90077", new string[] { "LABEL.MESSENGER_JOB" });



                }
                if (messengerJobTable.Result == (int)Result.None)
                {
                    ex.AddData("ERROR.90057", new string[] { "LABEL.RESULT" });



                }
                if (messengerJobTable.ResultRemark == null)
                {
                    ex.AddData("ERROR.90042", new string[] { "LABEL.RESULT_REMARK" });



                }
                // R06 remove validate
                //if (messengerJobTable.FeeAmount == 0)
                //{
                //    ex.AddData("ERROR.90042", new string[] { "LABEL.FEE_AMOUNT" });
                //}
                if (messengerJobTable.ContactTo == (int)ContactTo.Specific)
                {
                    var serviceFeeTransList = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.MessengerJob, id.StringToGuid());
                    if (ConditionService.IsNotZero(serviceFeeTransList.Count()))
                    {
                        ex.AddData("ERROR.90134", new string[] { "LABEL.MESSENGER_JOB", "LABEL.CONTACT_TO", "LABEL.SERVICE_FEE_TRANSACTIONS" });

                    }

                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool ValidateFunctionCancel(string id)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                var messengerJobTable = messengerJobTableRepo.GetByIdvw(id.StringToGuid());
                if (messengerJobTable.DocumentStatus_StatusId != ((int)MessengerJobStatus.Assigned).ToString() && messengerJobTable.DocumentStatus_StatusId != ((int)MessengerJobStatus.Draft).ToString())
                {
                    ex.AddData("ERROR.90076", new string[] { "LABEL.MESSENGER_JOB" });

                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ResultBaseEntity UpdateCancelMessengerJob(string id)
        {
            try
            {
                ValidateFunctionCancel(id);
                NotificationResponse success = new NotificationResponse();
                ResultBaseEntity result = new ResultBaseEntity();

                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var messengerJob = messengerJobTableService.GetMessengerJobTableById(id);
                var documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)MessengerJobStatus.Cancelled).ToString());
                messengerJob.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                var messengerJobUpdated = messengerJobTableService.UpdateMessengerJobTable(messengerJob);

                success.AddData("SUCCESS.90008", new string[] { SmartAppUtil.GetDropDownLabel(messengerJobUpdated.JobId, messengerJobUpdated.JobDate) });
                result.Notification = success;
                return result;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);

            }
        }

        public ResultBaseEntity UpdatePostMessengerJob(string id)
        {
            try
            {
                ValidateFunctionPost(id);
                NotificationResponse success = new NotificationResponse();
                List<string> labels = new List<string>();
                ResultBaseEntity result = new ResultBaseEntity();
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);



                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var messengerJob = messengerJobTableRepo.GetMessengerJobTableByIdNoTracking(id.StringToGuid());
                var documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)MessengerJobStatus.Posted).ToString());
                messengerJob.DocumentStatusGUID = documentStatus.DocumentStatusGUID;
                messengerJob.ModifiedBy = db.GetUserName();
                messengerJob.ModifiedDateTime = DateTime.Now;

                string creditAppTableGuid = null;
                if (messengerJob.RefCreditAppLineGUID != null)
                {
                    ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByLine(messengerJob.RefCreditAppLineGUID);
                    creditAppTableGuid = creditAppTable.CreditAppTableGUID.ToString();

                }

                IInvoiceService invoiceService = new InvoiceService(db);
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.MessengerJob, messengerJob.MessengerJobTableGUID);

                // get shared method: Method017_GenInvoiceFromServiceFeeTrans
                GenInvoiceFromServiceFeeTransResultView genInvoiceFromServiceFeeTransResultView = new GenInvoiceFromServiceFeeTransResultView();
                if (messengerJob.ContactTo != (int)ContactTo.Specific)
                {
                    GenInvoiceFromServiceFeeTransParamView genInvoiceFromServiceFeeTransView = new GenInvoiceFromServiceFeeTransParamView()
                    {
                        ServiceFeeTrans = serviceFeeTrans.ToList(),
                        CustomerTableGUID = messengerJob.CustomerTableGUID.Value,
                        IssuedDate = messengerJob.JobDate.Value,
                        DocumentId = messengerJob.JobId,
                        ProductType = (ProductType)messengerJob.ProductType,
                        CreditAppTableGUID = creditAppTableGuid.StringToGuidNull()
                    };
                    genInvoiceFromServiceFeeTransResultView = serviceFeeTransService.GenInvoiceFromServiceFeeTrans(genInvoiceFromServiceFeeTransView);
                }

                // get shared method: Method022_PostInvoice 
                PostInvoiceResultView postInvoiceResult = invoiceService.PostInvoice(genInvoiceFromServiceFeeTransResultView.InvoiceTable, genInvoiceFromServiceFeeTransResultView.InvoiceLine, RefType.MessengerJob, id.StringToGuid());

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkUpdate(messengerJob.FirstToList());
                    this.BulkInsert(genInvoiceFromServiceFeeTransResultView.IntercompanyInvoiceTable);
                    #region PostInvoice
                    this.BulkInsert(postInvoiceResult.InvoiceTables);
                    this.BulkInsert(postInvoiceResult.InvoiceLines);
                    this.BulkInsert(postInvoiceResult.TaxInvoiceTables);
                    this.BulkInsert(postInvoiceResult.TaxInvoiceLines);
                    this.BulkInsert(postInvoiceResult.CustTranses);
                    this.BulkInsert(postInvoiceResult.CreditAppTranses);
                    this.BulkInsert(postInvoiceResult.ProcessTranses);
                    this.BulkInsert(postInvoiceResult.RetentionTranses);
                    #endregion PostInvoice
                    UnitOfWork.Commit(transaction);
                }
                labels.Add("LABEL.MESSENGER_JOB");
                labels.Add(messengerJob.JobId);
                success.AddData("SUCCESS.90004", labels.ToArray());
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);

            }
        }

        public ResultBaseEntity UpdateAssignMessengerJob(string id)
        {
            try
            {
                ValidateFunctionAssign(id);
                NotificationResponse success = new NotificationResponse();
                List<string> labels = new List<string>();
                ResultBaseEntity result = new ResultBaseEntity();

                IMessengerJobTableService messengerJobTableService = new MessengerJobTableService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var messengerJob = messengerJobTableService.GetMessengerJobTableById(id);
                var documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(((int)MessengerJobStatus.Assigned).ToString());
                messengerJob.DocumentStatusGUID = documentStatus.DocumentStatusGUID.ToString();
                var messengerJobUpdated = messengerJobTableService.UpdateMessengerJobTable(messengerJob);

                labels.Add("LABEL.MESSENGER_JOB");
                labels.Add(SmartAppUtil.GetDropDownLabel(messengerJobUpdated.JobId, messengerJobUpdated.JobDate));
                success.AddData("SUCCESS.90013", labels.ToArray());

                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);

            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByMessengerJobTable(string refGUID)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                IMessengerJobTableRepo messengerTableRepo = new MessengerJobTableRepo(db);
                var result = messengerTableRepo.GetMessengerJobTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());

                var res = serviceFeeTransService.GetServiceFeeTransInitialDataByMessengerJobTable(result.JobId, refGUID, RefType.MessengerJob);
                res.TaxDate = result.JobDate.DateNullToString();
                return res;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetMessengerJobTableAcessMode(string messengerjobtableId)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                MessengerJobTable messengerJobTable = messengerJobTableRepo.GetMessengerJobTableByIdNoTracking(messengerjobtableId.StringToGuid());

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(messengerJobTable.DocumentStatusGUID);

                bool condition = Convert.ToInt32(documentStatus.StatusId) < Convert.ToInt32(MessengerJobStatus.Posted);
                return new AccessModeView
                {
                    CanCreate = condition,
                    CanDelete = condition,
                    CanView = condition,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDocumentStatusDocumentProcessDropdown(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.MessengerJob);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Report
        public PrintMessengerJobView GetPrintMessengerJobById(string messengerJobTableGUID)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                MessengerJobTableItemView messengerJobTable = messengerJobTableRepo.GetByIdvw(messengerJobTableGUID.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var doc_status = documentStatusRepo.GetDocumentStatusByIdNoTracking(messengerJobTable.DocumentStatusGUID.StringToGuid());
                IJobTypeRepo jobTypeRepo = new JobTypeRepo(db);
                var job_type = jobTypeRepo.GetByIdvw(messengerJobTable.JobTypeGUID.StringToGuid());
                IMessengerTableRepo messengerTableRepo = new MessengerTableRepo(db);
                MessengerTableItemView messenger_table = new MessengerTableItemView();
                VendorTableItemView vendor_table = new VendorTableItemView();
                if (messengerJobTable.MessengerTableGUID != null)
                {
                    messenger_table = messengerTableRepo.GetByIdvw(messengerJobTable.MessengerTableGUID.StringToGuid());
                    IVendorTableRepo vendorTableRepo = new VendorTableRepo(db);
                    if (messenger_table.VendorTableGUID != null)
                    {
                        vendor_table = vendorTableRepo.GetByIdvw(messenger_table.VendorTableGUID.StringToGuid());
                    }
                }
                PrintMessengerJobView result = new PrintMessengerJobView
                {
                    MessengerJobTableGUID = messengerJobTable.MessengerJobTableGUID,
                    MessengerJobTable_Values = SmartAppUtil.GetDropDownLabel(messengerJobTable.JobId, messengerJobTable.JobDate),
                    DocumentStatusGUID = messengerJobTable.DocumentStatusGUID,
                    DocumentStatus_Values = doc_status.Description,
                    JobDate = messengerJobTable.JobDate,
                    JobTime = messengerJobTable.JobStartTime,
                    JobEndTime = messengerJobTable.JobEndTime,
                    JobTypeGUID = messengerJobTable.JobTypeGUID,
                    JobType_Values = SmartAppUtil.GetDropDownLabel(job_type.JobTypeId, job_type.Description),
                    MessengerTableGUID = messengerJobTable.MessengerTableGUID,
                    MessengerTable_Values = SmartAppUtil.GetDropDownLabel(messenger_table.Name, vendor_table.VendorId),
                    JobDetail = messengerJobTable.JobDetail,
                    ContactTo = messengerJobTable.ContactTo,
                    ContactPersonName = messengerJobTable.ContactPersonName
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
    }
}
