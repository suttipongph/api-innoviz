using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IOwnershipService
	{

		OwnershipItemView GetOwnershipById(string id);
		OwnershipItemView CreateOwnership(OwnershipItemView ownershipView);
		OwnershipItemView UpdateOwnership(OwnershipItemView ownershipView);
		bool DeleteOwnership(string id);
	}
	public class OwnershipService : SmartAppService, IOwnershipService
	{
		public OwnershipService(SmartAppDbContext context) : base(context) { }
		public OwnershipService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public OwnershipService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public OwnershipService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public OwnershipService() : base() { }

		public OwnershipItemView GetOwnershipById(string id)
		{
			try
			{
				IOwnershipRepo ownershipRepo = new OwnershipRepo(db);
				return ownershipRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OwnershipItemView CreateOwnership(OwnershipItemView ownershipView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ownershipView = accessLevelService.AssignOwnerBU(ownershipView);
				IOwnershipRepo ownershipRepo = new OwnershipRepo(db);
				Ownership ownership = ownershipView.ToOwnership();
				ownership = ownershipRepo.CreateOwnership(ownership);
				base.LogTransactionCreate<Ownership>(ownership);
				UnitOfWork.Commit();
				return ownership.ToOwnershipItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OwnershipItemView UpdateOwnership(OwnershipItemView ownershipView)
		{
			try
			{
				IOwnershipRepo ownershipRepo = new OwnershipRepo(db);
				Ownership inputOwnership = ownershipView.ToOwnership();
				Ownership dbOwnership = ownershipRepo.Find(inputOwnership.OwnershipGUID);
				dbOwnership = ownershipRepo.UpdateOwnership(dbOwnership, inputOwnership);
				base.LogTransactionUpdate<Ownership>(GetOriginalValues<Ownership>(dbOwnership), dbOwnership);
				UnitOfWork.Commit();
				return dbOwnership.ToOwnershipItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteOwnership(string item)
		{
			try
			{
				IOwnershipRepo ownershipRepo = new OwnershipRepo(db);
				Guid ownershipGUID = new Guid(item);
				Ownership ownership = ownershipRepo.Find(ownershipGUID);
				ownershipRepo.Remove(ownership);
				base.LogTransactionDelete<Ownership>(ownership);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
