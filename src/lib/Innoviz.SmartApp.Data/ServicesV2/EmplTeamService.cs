using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IEmplTeamService
	{

		EmplTeamItemView GetEmplTeamById(string id);
		EmplTeamItemView CreateEmplTeam(EmplTeamItemView emplTeamView);
		EmplTeamItemView UpdateEmplTeam(EmplTeamItemView emplTeamView);
		bool DeleteEmplTeam(string id);
	}
	public class EmplTeamService : SmartAppService, IEmplTeamService
	{
		public EmplTeamService(SmartAppDbContext context) : base(context) { }
		public EmplTeamService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public EmplTeamService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public EmplTeamService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public EmplTeamService() : base() { }

		public EmplTeamItemView GetEmplTeamById(string id)
		{
			try
			{
				IEmplTeamRepo emplTeamRepo = new EmplTeamRepo(db);
				return emplTeamRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public EmplTeamItemView CreateEmplTeam(EmplTeamItemView emplTeamView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				emplTeamView = accessLevelService.AssignOwnerBU(emplTeamView);
				IEmplTeamRepo emplTeamRepo = new EmplTeamRepo(db);
				EmplTeam emplTeam = emplTeamView.ToEmplTeam();
				emplTeam = emplTeamRepo.CreateEmplTeam(emplTeam);
				base.LogTransactionCreate<EmplTeam>(emplTeam);
				UnitOfWork.Commit();
				return emplTeam.ToEmplTeamItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public EmplTeamItemView UpdateEmplTeam(EmplTeamItemView emplTeamView)
		{
			try
			{
				IEmplTeamRepo emplTeamRepo = new EmplTeamRepo(db);
				EmplTeam inputEmplTeam = emplTeamView.ToEmplTeam();
				EmplTeam dbEmplTeam = emplTeamRepo.Find(inputEmplTeam.EmplTeamGUID);
				dbEmplTeam = emplTeamRepo.UpdateEmplTeam(dbEmplTeam, inputEmplTeam);
				base.LogTransactionUpdate<EmplTeam>(GetOriginalValues<EmplTeam>(dbEmplTeam), dbEmplTeam);
				UnitOfWork.Commit();
				return dbEmplTeam.ToEmplTeamItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteEmplTeam(string item)
		{
			try
			{
				IEmplTeamRepo emplTeamRepo = new EmplTeamRepo(db);
				Guid emplTeamGUID = new Guid(item);
				EmplTeam emplTeam = emplTeamRepo.Find(emplTeamGUID);
				emplTeamRepo.Remove(emplTeam);
				base.LogTransactionDelete<EmplTeam>(emplTeam);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
