using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ILedgerFiscalService
	{

		LedgerFiscalPeriodItemView GetLedgerFiscalPeriodById(string id);
		LedgerFiscalPeriodItemView CreateLedgerFiscalPeriod(LedgerFiscalPeriodItemView ledgerFiscalPeriodView);
		LedgerFiscalPeriodItemView UpdateLedgerFiscalPeriod(LedgerFiscalPeriodItemView ledgerFiscalPeriodView);
		bool DeleteLedgerFiscalPeriod(string id);
		LedgerFiscalYearItemView GetLedgerFiscalYearById(string id);
		LedgerFiscalYearItemView CreateLedgerFiscalYear(LedgerFiscalYearItemView ledgerFiscalYearView);
		LedgerFiscalYearItemView UpdateLedgerFiscalYear(LedgerFiscalYearItemView ledgerFiscalYearView);
		bool DeleteLedgerFiscalYear(string id);
		LedgerFiscalPeriodItemView GetInitialLedgerFiscalYearData(string id);
		#region shared
		bool IsPeriodStatusOpen(Guid companyGUID, DateTime asOfDate);
		#endregion
	}
    public class LedgerFiscalService : SmartAppService, ILedgerFiscalService
	{
		public LedgerFiscalService(SmartAppDbContext context) : base(context) { }
		public LedgerFiscalService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public LedgerFiscalService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LedgerFiscalService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public LedgerFiscalService() : base() { }
		#region LedgerFiscalPeriod
		public LedgerFiscalPeriodItemView GetLedgerFiscalPeriodById(string id)
		{
			try
			{
				ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
				return ledgerFiscalPeriodRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalPeriodItemView CreateLedgerFiscalPeriod(LedgerFiscalPeriodItemView ledgerFiscalPeriodView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ledgerFiscalPeriodView = accessLevelService.AssignOwnerBU(ledgerFiscalPeriodView);
				ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
				LedgerFiscalPeriod ledgerFiscalPeriod = ledgerFiscalPeriodView.ToLedgerFiscalPeriod();
				ledgerFiscalPeriod = ledgerFiscalPeriodRepo.CreateLedgerFiscalPeriod(ledgerFiscalPeriod);
				base.LogTransactionCreate<LedgerFiscalPeriod>(ledgerFiscalPeriod);
				UnitOfWork.Commit();
				return ledgerFiscalPeriod.ToLedgerFiscalPeriodItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalPeriodItemView UpdateLedgerFiscalPeriod(LedgerFiscalPeriodItemView ledgerFiscalPeriodView)
		{
			try
			{
				ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
				LedgerFiscalPeriod inputLedgerFiscalPeriod = ledgerFiscalPeriodView.ToLedgerFiscalPeriod();
				LedgerFiscalPeriod dbLedgerFiscalPeriod = ledgerFiscalPeriodRepo.Find(inputLedgerFiscalPeriod.LedgerFiscalPeriodGUID);
				dbLedgerFiscalPeriod = ledgerFiscalPeriodRepo.UpdateLedgerFiscalPeriod(dbLedgerFiscalPeriod, inputLedgerFiscalPeriod);
				base.LogTransactionUpdate<LedgerFiscalPeriod>(GetOriginalValues<LedgerFiscalPeriod>(dbLedgerFiscalPeriod), dbLedgerFiscalPeriod);
				UnitOfWork.Commit();
				return dbLedgerFiscalPeriod.ToLedgerFiscalPeriodItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteLedgerFiscalPeriod(string item)
		{
			try
			{
				ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
				Guid ledgerFiscalPeriodGUID = new Guid(item);
				LedgerFiscalPeriod ledgerFiscalPeriod = ledgerFiscalPeriodRepo.Find(ledgerFiscalPeriodGUID);
				ledgerFiscalPeriodRepo.Remove(ledgerFiscalPeriod);
				base.LogTransactionDelete<LedgerFiscalPeriod>(ledgerFiscalPeriod);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalPeriodItemView GetInitialLedgerFiscalYearData(string id)
		{
			try
			{
				ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
				LedgerFiscalYear ledgerFiscalYear = ledgerFiscalYearRepo.GetLedgerFiscalYearByIdNoTrackingByAccessLevel(id.StringToGuid());
				return new LedgerFiscalPeriodItemView
				{
					LedgerFiscalYearGUID = ledgerFiscalYear.LedgerFiscalYearGUID.GuidNullToString(),
					LedgerFiscalYear_Values = SmartAppUtil.GetDropDownLabel(ledgerFiscalYear.FiscalYearId,ledgerFiscalYear.Description),
					//StartDate = ledgerFiscalYear.StartDate.DateToString(),
					//EndDate = ledgerFiscalYear.EndDate.DateToString()
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region LedgerFiscalYear
		public LedgerFiscalYearItemView GetLedgerFiscalYearById(string id)
		{
			try
			{
				ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
				return ledgerFiscalYearRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalYearItemView CreateLedgerFiscalYear(LedgerFiscalYearItemView ledgerFiscalYearView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ledgerFiscalYearView = accessLevelService.AssignOwnerBU(ledgerFiscalYearView);
				ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
				LedgerFiscalYear ledgerFiscalYear = ledgerFiscalYearView.ToLedgerFiscalYear();
				if (ValidateLedgerFiscalPeriod(ledgerFiscalYear) == true)
				{
					ledgerFiscalYear.LedgerFiscalYearGUID = Guid.NewGuid();
					List<LedgerFiscalPeriod> ledgerFiscalPeriods = GetLedgerFiscalYearPeriodUnit(ledgerFiscalYear);
					if (ledgerFiscalPeriods != null)
					{
						using (var transaction = UnitOfWork.ContextTransaction())
						{
							this.BulkInsert(ledgerFiscalYear.FirstToList(), false);
							this.BulkInsert(ledgerFiscalPeriods, false);
							UnitOfWork.Commit(transaction);
						}
					}
				}
				return ledgerFiscalYear.ToLedgerFiscalYearItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LedgerFiscalYearItemView UpdateLedgerFiscalYear(LedgerFiscalYearItemView ledgerFiscalYearView)
		{
			try
			{
				ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
				LedgerFiscalYear inputLedgerFiscalYear = ledgerFiscalYearView.ToLedgerFiscalYear();
				LedgerFiscalYear dbLedgerFiscalYear = ledgerFiscalYearRepo.Find(inputLedgerFiscalYear.LedgerFiscalYearGUID);
				dbLedgerFiscalYear = ledgerFiscalYearRepo.UpdateLedgerFiscalYear(dbLedgerFiscalYear, inputLedgerFiscalYear);
				base.LogTransactionUpdate<LedgerFiscalYear>(GetOriginalValues<LedgerFiscalYear>(dbLedgerFiscalYear), dbLedgerFiscalYear);
				UnitOfWork.Commit();
				return dbLedgerFiscalYear.ToLedgerFiscalYearItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteLedgerFiscalYear(string item)
		{
			try
			{
				ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
				Guid ledgerFiscalYearGUID = new Guid(item);
				LedgerFiscalYear ledgerFiscalYear = ledgerFiscalYearRepo.Find(ledgerFiscalYearGUID);
				ledgerFiscalYearRepo.Remove(ledgerFiscalYear);
				base.LogTransactionDelete<LedgerFiscalYear>(ledgerFiscalYear);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion

        #region shared
		public bool IsPeriodStatusOpen(Guid companyGUID, DateTime asOfDate)
        {
            try
            {
				ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
				LedgerFiscalPeriod ledgerFiscalPeriod = ledgerFiscalPeriodRepo.GetByDate(companyGUID, asOfDate);
				if (ledgerFiscalPeriod.PeriodStatus == (int)LedgerFiscalPeriodStatus.Open)
					return true;
				else
					return false;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}



		#endregion

		public List<LedgerFiscalPeriod> GetLedgerFiscalYearPeriodUnit(LedgerFiscalYear ledgerFiscalYear)
		{
			try
			{
				List<LedgerFiscalPeriod> ledgerFiscalPeriods = new List<LedgerFiscalPeriod>();
				DateTime endDate = ledgerFiscalYear.EndDate;
				DateTime startDate = ledgerFiscalYear.StartDate;
				int lengthPeriod = ledgerFiscalYear.LengthOfPeriod;
				int newPeriod = 0;
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				ILedgerFiscalPeriodRepo ledgerFiscalPeriodRepo = new LedgerFiscalPeriodRepo(db);
				ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);

				if (ledgerFiscalYear.PeriodUnit == Convert.ToInt16(LedgerFiscalPeriodUnit.Month))
				{
					for (int i = startDate.Month; i <= endDate.Month; i++)
					{
						LedgerFiscalPeriod ledgerFiscalPeriod = new LedgerFiscalPeriod();
						ledgerFiscalPeriod.LedgerFiscalPeriodGUID = Guid.NewGuid();
						ledgerFiscalPeriod.LedgerFiscalYearGUID = ledgerFiscalYear.LedgerFiscalYearGUID;
						ledgerFiscalPeriod.PeriodStatus = Convert.ToInt16(LedgerFiscalPeriodStatus.Closed);
						ledgerFiscalPeriod.StartDate = ledgerFiscalYear.StartDate.AddMonths(newPeriod);
						ledgerFiscalPeriod.CompanyGUID = ledgerFiscalYear.CompanyGUID;
						if (i == endDate.Month)
						{
							ledgerFiscalPeriod.EndDate = ledgerFiscalYear.EndDate;
						}
						else
						{
							ledgerFiscalPeriod.EndDate = ledgerFiscalYear.StartDate.AddMonths(newPeriod + lengthPeriod).AddDays(-1);
						}

						if (ledgerFiscalPeriod.StartDate <= endDate)
						{
							ledgerFiscalPeriods.Add(ledgerFiscalPeriod);
						}
						newPeriod += lengthPeriod;
					}

					ledgerFiscalPeriodRepo.ValidateAdd(ledgerFiscalPeriods);
				}
				if (ledgerFiscalYear.PeriodUnit == Convert.ToInt16(LedgerFiscalPeriodUnit.Year))
				{
					for (int i = startDate.Year; i < endDate.Year; i++)
					{
						LedgerFiscalPeriod ledgerFiscalPeriod = new LedgerFiscalPeriod();
						ledgerFiscalPeriod.LedgerFiscalPeriodGUID = Guid.NewGuid();
						ledgerFiscalPeriod.LedgerFiscalYearGUID = ledgerFiscalYear.LedgerFiscalYearGUID;
						ledgerFiscalPeriod.PeriodStatus = Convert.ToInt16(LedgerFiscalPeriodStatus.Closed);
						ledgerFiscalPeriod.StartDate = ledgerFiscalYear.StartDate.AddYears(newPeriod);
						ledgerFiscalPeriod.CompanyGUID = ledgerFiscalYear.CompanyGUID;

						if (i == (endDate.Year - 1))
						{
							ledgerFiscalPeriod.EndDate = ledgerFiscalYear.EndDate;
						}
						else
						{
							ledgerFiscalPeriod.EndDate = ledgerFiscalYear.StartDate.AddYears(newPeriod + lengthPeriod).AddDays(-1);
						}

						if (ledgerFiscalPeriod.StartDate <= endDate)
						{
							ledgerFiscalPeriods.Add(ledgerFiscalPeriod);
						}
						newPeriod += lengthPeriod;
					}

					ledgerFiscalPeriodRepo.ValidateAdd(ledgerFiscalPeriods);
				}
				return ledgerFiscalPeriods;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.ThrowDBException(ex);
			}
		}
		public bool ValidateLedgerFiscalPeriod(LedgerFiscalYear ledgerFiscalYear)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");

				if (!IsNumeric(ledgerFiscalYear))
				{
					ex.AddData("ERROR.00153");
				}
				if (!IsStartDateLessThanOrEqualEndDate(ledgerFiscalYear))
				{
					ex.AddData("ERROR.00154");
				}
				if (!IsYearStartDateValid(ledgerFiscalYear))
				{
					ex.AddData("ERROR.00155");
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				return true;
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.ThrowDBException(ex);
			}

		}

		public bool IsNumeric(LedgerFiscalYear ledgerFiscalYear)
		{
			object data = ledgerFiscalYear.LengthOfPeriod;
			bool isNum;
			double retNum;
			isNum = Double.TryParse(Convert.ToString(data), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
			if (isNum)
			{
				isNum = (ledgerFiscalYear.LengthOfPeriod > 0);
			}
			return isNum;
		}

		public bool IsStartDateLessThanOrEqualEndDate(LedgerFiscalYear ledgerFiscalYear)
		{
			try
			{
				if (ledgerFiscalYear.StartDate > ledgerFiscalYear.EndDate)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}

		public bool IsYearStartDateValid(LedgerFiscalYear ledgerFiscalYear)
		{
			try
			{
				ILedgerFiscalYearRepo ledgerFiscalYearRepo = new LedgerFiscalYearRepo(db);
				LedgerFiscalYear maxLedgerFiscalYear = ledgerFiscalYearRepo.GetMaxEndDate(ledgerFiscalYear.CompanyGUID);
				if (maxLedgerFiscalYear != null)
				{
					if (ledgerFiscalYear.StartDate <= maxLedgerFiscalYear.EndDate)
						return false;
				}

				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
	}
}
