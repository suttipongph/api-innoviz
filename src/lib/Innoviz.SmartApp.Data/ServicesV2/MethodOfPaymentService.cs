using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IMethodOfPaymentService
	{

		MethodOfPaymentItemView GetMethodOfPaymentById(string id);
		MethodOfPaymentItemView CreateMethodOfPayment(MethodOfPaymentItemView methodOfPaymentView);
		MethodOfPaymentItemView UpdateMethodOfPayment(MethodOfPaymentItemView methodOfPaymentView);
		bool DeleteMethodOfPayment(string id);
	}
	public class MethodOfPaymentService : SmartAppService, IMethodOfPaymentService
	{
		public MethodOfPaymentService(SmartAppDbContext context) : base(context) { }
		public MethodOfPaymentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public MethodOfPaymentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MethodOfPaymentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public MethodOfPaymentService() : base() { }

		public MethodOfPaymentItemView GetMethodOfPaymentById(string id)
		{
			try
			{
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				return methodOfPaymentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MethodOfPaymentItemView CreateMethodOfPayment(MethodOfPaymentItemView methodOfPaymentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				methodOfPaymentView = accessLevelService.AssignOwnerBU(methodOfPaymentView);
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				MethodOfPayment methodOfPayment = methodOfPaymentView.ToMethodOfPayment();
				methodOfPayment = methodOfPaymentRepo.CreateMethodOfPayment(methodOfPayment);
				base.LogTransactionCreate<MethodOfPayment>(methodOfPayment);
				UnitOfWork.Commit();
				return methodOfPayment.ToMethodOfPaymentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MethodOfPaymentItemView UpdateMethodOfPayment(MethodOfPaymentItemView methodOfPaymentView)
		{
			try
			{
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				MethodOfPayment inputMethodOfPayment = methodOfPaymentView.ToMethodOfPayment();
				MethodOfPayment dbMethodOfPayment = methodOfPaymentRepo.Find(inputMethodOfPayment.MethodOfPaymentGUID);
				dbMethodOfPayment = methodOfPaymentRepo.UpdateMethodOfPayment(dbMethodOfPayment, inputMethodOfPayment);
				base.LogTransactionUpdate<MethodOfPayment>(GetOriginalValues<MethodOfPayment>(dbMethodOfPayment), dbMethodOfPayment);
				UnitOfWork.Commit();
				return dbMethodOfPayment.ToMethodOfPaymentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteMethodOfPayment(string item)
		{
			try
			{
				IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
				Guid methodOfPaymentGUID = new Guid(item);
				MethodOfPayment methodOfPayment = methodOfPaymentRepo.Find(methodOfPaymentGUID);
				methodOfPaymentRepo.Remove(methodOfPayment);
				base.LogTransactionDelete<MethodOfPayment>(methodOfPayment);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
