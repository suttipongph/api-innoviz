using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems;
using Innoviz.SmartApp.Data.InterfaceWithOtherSystems.Services;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IVendorPaymentTransService
    {

        VendorPaymentTransItemView GetVendorPaymentTransById(string id);
        VendorPaymentTransItemView CreateVendorPaymentTrans(VendorPaymentTransItemView vendorPaymentTransView);
        VendorPaymentTrans CreateVendorPaymentTrans(VendorPaymentTrans vendorPaymentTrans);
        VendorPaymentTransItemView UpdateVendorPaymentTrans(VendorPaymentTransItemView vendorPaymentTransView);
        bool DeleteVendorPaymentTrans(string id);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemVendorPaymentTransStatus(SearchParameter search);
        #region function
        bool ValidateSendVendorInvoiceStagingMenuBtn(SendVendorInvoiceStagingParamView parm);
        SendVendorInvoiceStagingParamView GetSendVendorInvoiceStagingInitialData(SendVendorInvoiceStagingParamView parm);
        SendVendorInvoiceStagingResultView SendVendorInvoiceStaging(SendVendorInvoiceStagingParamView parm); 
        #endregion
    }
    public class VendorPaymentTransService : SmartAppService, IVendorPaymentTransService
    {
        public VendorPaymentTransService(List<DbContext> contexts, ISysTransactionLogService transactionLogService) : base(contexts, transactionLogService) 
        {
            
        }
        public VendorPaymentTransService(SmartAppDbContext context) : base(context) { }
        public VendorPaymentTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public VendorPaymentTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public VendorPaymentTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        
        public VendorPaymentTransService() : base() { }

        public VendorPaymentTransItemView GetVendorPaymentTransById(string id)
        {
            try
            {
                IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
                var result = vendorPaymentTransRepo.GetByIdvw(id.StringToGuid());
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VendorPaymentTransItemView CreateVendorPaymentTrans(VendorPaymentTransItemView vendorPaymentTransView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                vendorPaymentTransView = accessLevelService.AssignOwnerBU(vendorPaymentTransView);
                IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
                VendorPaymentTrans vendorPaymentTrans = vendorPaymentTransView.ToVendorPaymentTrans();
                vendorPaymentTrans = vendorPaymentTransRepo.CreateVendorPaymentTrans(vendorPaymentTrans);
                base.LogTransactionCreate<VendorPaymentTrans>(vendorPaymentTrans);
                UnitOfWork.Commit();
                return vendorPaymentTrans.ToVendorPaymentTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VendorPaymentTrans CreateVendorPaymentTrans(VendorPaymentTrans vendorPaymentTrans)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                vendorPaymentTrans = accessLevelService.AssignOwnerBU(vendorPaymentTrans);
                IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
                vendorPaymentTrans = vendorPaymentTransRepo.CreateVendorPaymentTrans(vendorPaymentTrans);
                base.LogTransactionCreate<VendorPaymentTrans>(vendorPaymentTrans);
                return vendorPaymentTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VendorPaymentTransItemView UpdateVendorPaymentTrans(VendorPaymentTransItemView vendorPaymentTransView)
        {
            try
            {
                IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
                VendorPaymentTrans inputVendorPaymentTrans = vendorPaymentTransView.ToVendorPaymentTrans();
                VendorPaymentTrans dbVendorPaymentTrans = vendorPaymentTransRepo.Find(inputVendorPaymentTrans.VendorPaymentTransGUID);
                dbVendorPaymentTrans = vendorPaymentTransRepo.UpdateVendorPaymentTrans(dbVendorPaymentTrans, inputVendorPaymentTrans);
                base.LogTransactionUpdate<VendorPaymentTrans>(GetOriginalValues<VendorPaymentTrans>(dbVendorPaymentTrans), dbVendorPaymentTrans);
                UnitOfWork.Commit();
                return dbVendorPaymentTrans.ToVendorPaymentTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteVendorPaymentTrans(string item)
        {
            try
            {
                IVendorPaymentTransRepo vendorPaymentTransRepo = new VendorPaymentTransRepo(db);
                Guid vendorPaymentTransGUID = new Guid(item);
                VendorPaymentTrans vendorPaymentTrans = vendorPaymentTransRepo.Find(vendorPaymentTransGUID);
                vendorPaymentTransRepo.Remove(vendorPaymentTrans);
                base.LogTransactionDelete<VendorPaymentTrans>(vendorPaymentTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemVendorPaymentTransStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.VendorPaymentTrans);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region function
        #region SendVendorInvoiceStaging
        public bool ValidateSendVendorInvoiceStagingMenuBtn(SendVendorInvoiceStagingParamView parm)
        {
            try
            {
                IStagingTableRepo stagingTableRepo = new StagingTableRepo(db);
                var stagingTableVendorPayment = stagingTableRepo.GetStagingTableVendorPaymentTransByRefTypeRefGUID(parm.RefType, parm.RefGUID.StringToGuid());
                if (stagingTableVendorPayment.Count() > 0)
                {
                    if (stagingTableVendorPayment.All(a => a.InterfaceStatus == (int)InterfaceStatus.Success))
                    {
                        SmartAppException ex = new SmartAppException("ERROR.ERROR");
                        ex.AddData("ERROR.90145", parm.CallerTableLabel);
                        throw ex;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SendVendorInvoiceStagingParamView GetSendVendorInvoiceStagingInitialData(SendVendorInvoiceStagingParamView parm)
        {
            try
            {
                IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
                parm.Number = processTransRepo.GetCountProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(parm.RefType, parm.RefGUID.StringToGuid());
                return parm;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SendVendorInvoiceStagingResultView SendVendorInvoiceStaging(SendVendorInvoiceStagingParamView parm)
        {
            try
            {
                SendVendorInvoiceStagingResultView result = new SendVendorInvoiceStagingResultView();
                IProcessTransRepo processTransRepo = new ProcessTransRepo(db);
                IInterfaceStagingService interfaceStagingService = new InterfaceStagingService(dbContexts, transactionLogService);

                var processTransRefGuids = processTransRepo.GetProcessTransVendorPaymentByVendorPaymentRefTypeRefGUID(parm.RefType, parm.RefGUID.StringToGuid())
                                                .Select(s => s.RefGUID.Value).ToList();
               
                var genStagingResult = GenStagingTableForSendVendorInvoiceStaging(processTransRefGuids);

                InterfaceVendorInvoiceView interfaceVendorParm = new InterfaceVendorInvoiceView
                {
                    InterfaceStatus = new List<int>() { (int)InterfaceStatus.None, (int)InterfaceStatus.Fail },
                    ProcessTransType = new List<int>() { (int)ProcessTransType.VendorPayment },
                    ListRefGUID = processTransRefGuids
                };
                interfaceStagingService.InterfaceVendorInvoice(interfaceVendorParm);

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.90020", new string[] { "LABEL.INTERFACE_VENDOR_INVOICE" });
                result.Notification = success;
                return result;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private GenStagingTableResult GenStagingTableForSendVendorInvoiceStaging(List<Guid> processTransRefGuids)
        {
            try
            {
                IStagingTableService stagingTableService = new StagingTableService(db, transactionLogService);

                GenStagingTableParameter genStagingTableParam = new GenStagingTableParameter
                {
                    ProcessTransType = new List<int>() { (int)ProcessTransType.VendorPayment },
                    StagingBatchStatus = new List<int>() { (int)StagingBatchStatus.None, (int)StagingBatchStatus.Fail },
                    RefGUID = processTransRefGuids
                };

                var genStagingResult = stagingTableService.GenStagingTableByProcessTransType(genStagingTableParam);

                #region insert & update
                bool checkCreateStagingTables = genStagingResult != null && genStagingResult.StagingTable != null && genStagingResult.StagingTable.Count > 0;
                bool checkCreateStagingTableVendorInfo = genStagingResult != null && genStagingResult.StagingTableVendorInfo != null && genStagingResult.StagingTableVendorInfo.Count > 0;
                bool checkUpdateProcessTrans = genStagingResult != null && genStagingResult.ProcessTrans != null && genStagingResult.ProcessTrans.Count > 0;
                
                if( checkCreateStagingTables || checkCreateStagingTableVendorInfo || checkUpdateProcessTrans)
                {
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(genStagingResult.StagingTable);
                        this.BulkInsert(genStagingResult.StagingTableVendorInfo);

                        this.BulkUpdate(genStagingResult.ProcessTrans);
                        UnitOfWork.Commit(transaction);
                    }
                }
                #endregion
                if (genStagingResult.GenStagingTableErrorList != null && genStagingResult.GenStagingTableErrorList.Count > 0)
                {
                    SmartAppException expectedError = 
                        stagingTableService.GetSmartAppExceptionFromGenStagingTableErrorList(genStagingResult.GenStagingTableErrorList);
                    throw expectedError;
                }

                return genStagingResult;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
    }
}
