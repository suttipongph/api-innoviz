﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface ISharedService
    {
        void InitializeStoredProceduresAndFunctions(string rootFolder, string connectionString);
        string DateToWord(DateTime date, string language, string format);
        IEnumerable<DateToWordResult> DateToWord(IEnumerable<DateTime> dates, string language, string format);
        string NumberToWords(decimal value);
        IEnumerable<NumberToWordsResult> NumberToWords(IEnumerable<decimal> values);
        string NumberToWordsTH(decimal value, string suffix = null);
        IEnumerable<NumberToWordsResult> NumberToWordsTH(IEnumerable<decimal> values, string suffix = null);
        Dictionary<string, StaticModelTypeLookupData> InitializeModelTypeLookupStaticData();
    }
    public class SharedService: SmartAppService, ISharedService
    {
        public SharedService(SmartAppDbContext context) : base(context) { }
        public SharedService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public SharedService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public SharedService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public SharedService() : base() { }

        #region init sp & fn
        public void InitializeStoredProceduresAndFunctions(string filePath, string connectionString)
        {
            try
            {
                SmartAppUtil.LogMessage(Serilog.Events.LogEventLevel.Information, "Initializing SQLStoredProcAndFunction...");
                DirectoryInfo fnDi = new DirectoryInfo(string.Concat(filePath, "/Function"));
                DirectoryInfo spDi = new DirectoryInfo(string.Concat(filePath, "/StoredProcedure"));
                var rgFiles = (fnDi.GetFiles("*.sql", SearchOption.AllDirectories))
                                .Concat(spDi.GetFiles("", SearchOption.AllDirectories));
               
                foreach (FileInfo fi in rgFiles)
                {
                    FileInfo fileInfo = new FileInfo(fi.FullName);
                    string script = File.ReadAllText(fi.ToString());
                    IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*Go\s*$", RegexOptions.Multiline | RegexOptions.IgnoreCase);

                    CreateStoredProcedureAndFunction(connectionString, commandStrings);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private void CreateStoredProcedureAndFunction(string connectionString, IEnumerable<string> commandStrings)
        {
            
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                foreach (string commandString in commandStrings)
                {
                    if (commandString.Trim() != "")
                    {
                        using (var command = new SqlCommand(commandString, connection))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
                connection.Close();
            }
        }
        #endregion
        #region DateToWords
        public string DateToWord(DateTime date, string language, string format)
        {
            try
            {
                var dateParm = date.FirstToList();
                var result = DateToWord(dateParm, language, format);
                return result.FirstOrDefault().ValueInWords;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<DateToWordResult> DateToWord(IEnumerable<DateTime> dates, string language, string format)
        {
            try
            {
                string datesString = string.Join(',', dates.Select(s => s.ToSQLDateString()));
                DateToWordParam parm = new DateToWordParam
                {
                    DateString = datesString,
                    Delimiter = ',',
                    Format = format,
                    Language = language
                };
                var result = DateToWords(parm);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IEnumerable<DateToWordResult> DateToWords(DateToWordParam input)
        {
            try
            {
                string connectionString = DataTypeHandler.GetConfigurationValue("ConnectionStrings:SmartApp");
                string procName = "LSS_BulkDateToWords";

                List<DateToWordResult> result = new List<DateToWordResult>();

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(procName, connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter date = new SqlParameter("@date", SqlDbType.NVarChar);
                    SqlParameter lang = new SqlParameter("@lang", SqlDbType.NVarChar, 2);
                    SqlParameter format = new SqlParameter("@format", SqlDbType.NVarChar);
                    SqlParameter delimiter = new SqlParameter("@delimiter", SqlDbType.NVarChar, 1);


                    date.Value = input.DateString;
                    lang.Value = input.Language;
                    format.Value = input.Format;
                    delimiter.Value = input.Delimiter;

                    command.Parameters.Add(date);
                    command.Parameters.Add(lang);
                    command.Parameters.Add(format);
                    command.Parameters.Add(delimiter);

                    connection.Open();

                    DbDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(
                            new DateToWordResult
                            {
                                ValueDate = reader["dateValue"].ToString().StringSQLToDate(),
                                ValueInWords = reader["dateWord"].ToString()
                            });
                    }
                    reader.Close();
                    connection.Close();
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region NumberToWords
        public string NumberToWords(decimal value)
        {
            try
            {
                var decimalParm = value.FirstToList();
                var result = NumberToWords(decimalParm);
                return result.FirstOrDefault().ValueInWords;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<NumberToWordsResult> NumberToWords(IEnumerable<decimal> values)
        {
            try
            {
                return NumberToWords(values, "EN");
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public string NumberToWordsTH(decimal value, string suffix = null)
        {
            try
            {
                var decimalParm = value.FirstToList();
                var result = NumberToWordsTH(decimalParm, suffix);
                return result.FirstOrDefault().ValueInWords;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<NumberToWordsResult> NumberToWordsTH(IEnumerable<decimal> values, string suffix = null)
        {
            try
            {
                return NumberToWords(values, "TH", suffix);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IEnumerable<NumberToWordsResult> NumberToWords(IEnumerable<decimal> values, string language, string suffix = null)
        {
            try
            {
                string decimalString = string.Join(',', values);
                NumberToWordsParam parm = new NumberToWordsParam
                {
                    DecimalString = decimalString,
                    Delimiter = ',',
                    Language = language,
                    Suffix = suffix
                };
                var result = NumberToWords(parm);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private IEnumerable<NumberToWordsResult> NumberToWords(NumberToWordsParam input)
        {
            try
            {
                string connectionString = DataTypeHandler.GetConfigurationValue("ConnectionStrings:SmartApp");
                string procName = "LSS_BulkNumberToWords";
                List<NumberToWordsResult> result = new List<NumberToWordsResult>();

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(procName, connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter decimalString = new SqlParameter("@DecimalString", SqlDbType.NVarChar);
                    SqlParameter delimiter = new SqlParameter("@Delimiter", SqlDbType.NVarChar, 1);
                    SqlParameter language = new SqlParameter("@Language", SqlDbType.NVarChar);
                    SqlParameter suffix = new SqlParameter("@Suffix", SqlDbType.NVarChar);

                    decimalString.Value = input.DecimalString;
                    delimiter.Value = input.Delimiter;
                    language.Value = input.Language;
                    suffix.Value = input.Suffix;

                    command.Parameters.Add(decimalString);
                    command.Parameters.Add(delimiter);
                    command.Parameters.Add(language);
                    command.Parameters.Add(suffix);

                    connection.Open();

                    DbDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(
                            new NumberToWordsResult
                            {
                                ValueDecimal = Convert.ToDecimal(reader["ValueDecimal"]),
                                ValueInWords = reader["ValueInWords"].ToString()
                            });
                    }
                    reader.Close();
                    connection.Close();
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public Dictionary<string, StaticModelTypeLookupData> InitializeModelTypeLookupStaticData()
        {
            try
            {
                Dictionary<string, StaticModelTypeLookupData> data = new Dictionary<string, StaticModelTypeLookupData>();
                #region add model type lookup data
                data.Add(
                    GetRelatedOwnerConst.CustomerTable, 
                    new StaticModelTypeLookupData { ModelType = typeof(CustomerTable), RefType = (int)RefType.Customer }
                );
                #endregion
                return data;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
