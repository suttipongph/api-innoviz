using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IFinancialCreditTransService
	{

		FinancialCreditTransItemView GetFinancialCreditTransById(string id);
		FinancialCreditTransItemView CreateFinancialCreditTrans(FinancialCreditTransItemView financialCreditTransView);
		FinancialCreditTransItemView UpdateFinancialCreditTrans(FinancialCreditTransItemView financialCreditTransView);
		bool DeleteFinancialCreditTrans(string id);
		FinancialCreditTransItemView GetFinancialCreditTransInitialData(string refId, string refGUID, RefType refType);
		void CreateFinancialCreditTransCollection(List<FinancialCreditTransItemView> financialCreditTransItemViews);
	}
	public class FinancialCreditTransService : SmartAppService, IFinancialCreditTransService
	{
		public FinancialCreditTransService(SmartAppDbContext context) : base(context) { }
		public FinancialCreditTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public FinancialCreditTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public FinancialCreditTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public FinancialCreditTransService() : base() { }

		public FinancialCreditTransItemView GetFinancialCreditTransById(string id)
		{
			try
			{
				IFinancialCreditTransRepo financialCreditTransRepo = new FinancialCreditTransRepo(db);
				return financialCreditTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialCreditTransItemView CreateFinancialCreditTrans(FinancialCreditTransItemView financialCreditTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				financialCreditTransView = accessLevelService.AssignOwnerBU(financialCreditTransView);
				IFinancialCreditTransRepo financialCreditTransRepo = new FinancialCreditTransRepo(db);
				FinancialCreditTrans financialCreditTrans = financialCreditTransView.ToFinancialCreditTrans();
				financialCreditTrans = financialCreditTransRepo.CreateFinancialCreditTrans(financialCreditTrans);
				base.LogTransactionCreate<FinancialCreditTrans>(financialCreditTrans);
				UnitOfWork.Commit();
				return financialCreditTrans.ToFinancialCreditTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialCreditTransItemView UpdateFinancialCreditTrans(FinancialCreditTransItemView financialCreditTransView)
		{
			try
			{
				IFinancialCreditTransRepo financialCreditTransRepo = new FinancialCreditTransRepo(db);
				FinancialCreditTrans inputFinancialCreditTrans = financialCreditTransView.ToFinancialCreditTrans();
				FinancialCreditTrans dbFinancialCreditTrans = financialCreditTransRepo.Find(inputFinancialCreditTrans.FinancialCreditTransGUID);
				dbFinancialCreditTrans = financialCreditTransRepo.UpdateFinancialCreditTrans(dbFinancialCreditTrans, inputFinancialCreditTrans);
				base.LogTransactionUpdate<FinancialCreditTrans>(GetOriginalValues<FinancialCreditTrans>(dbFinancialCreditTrans), dbFinancialCreditTrans);
				UnitOfWork.Commit();
				return dbFinancialCreditTrans.ToFinancialCreditTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteFinancialCreditTrans(string item)
		{
			try
			{
				IFinancialCreditTransRepo financialCreditTransRepo = new FinancialCreditTransRepo(db);
				Guid financialCreditTransGUID = new Guid(item);
				FinancialCreditTrans financialCreditTrans = financialCreditTransRepo.Find(financialCreditTransGUID);
				financialCreditTransRepo.Remove(financialCreditTrans);
				base.LogTransactionDelete<FinancialCreditTrans>(financialCreditTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialCreditTransItemView GetFinancialCreditTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new FinancialCreditTransItemView
				{
					FinancialCreditTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateFinancialCreditTransCollection(List<FinancialCreditTransItemView> financialCreditTransItemViews)
		{
			try
			{
				if (financialCreditTransItemViews.Any())
				{
					IFinancialCreditTransRepo financialCreditTransRepo = new FinancialCreditTransRepo(db);
					List<FinancialCreditTrans> financialCreditTrans = financialCreditTransItemViews.ToFinancialCreditTrans().ToList();
					financialCreditTransRepo.ValidateAdd(financialCreditTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(financialCreditTrans, false);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
	}
}
