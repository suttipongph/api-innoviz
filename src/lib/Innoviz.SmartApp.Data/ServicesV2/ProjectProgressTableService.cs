using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IProjectProgressTableService
	{

		ProjectProgressTableItemView GetProjectProgressTableById(string id);
		ProjectProgressTableItemView CreateProjectProgressTable(ProjectProgressTableItemView projectProgressTableView);
		ProjectProgressTableItemView UpdateProjectProgressTable(ProjectProgressTableItemView projectProgressTableView);
		bool DeleteProjectProgressTable(string id);
	
		ProjectProgressTableItemView ProjectProgressInitialData(string refId, string id , string transDate);
		void CreateProjectProgressTableCollection(List<ProjectProgressTableItemView> projectProgressTableItemViews);
		MemoTransItemView GetMemoTransInitialData(string refGUID);
	}
	public class ProjectProgressTableService : SmartAppService, IProjectProgressTableService
	{
		public ProjectProgressTableService(SmartAppDbContext context) : base(context) { }
		public ProjectProgressTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ProjectProgressTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ProjectProgressTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ProjectProgressTableService() : base() { }

		public ProjectProgressTableItemView GetProjectProgressTableById(string id)
		{
			try
			{
				IProjectProgressTableRepo projectProgressTableRepo = new ProjectProgressTableRepo(db);
				return projectProgressTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProjectProgressTableItemView CreateProjectProgressTable(ProjectProgressTableItemView projectProgressTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				projectProgressTableView = accessLevelService.AssignOwnerBU(projectProgressTableView);
				IProjectProgressTableRepo projectProgressTableRepo = new ProjectProgressTableRepo(db);
				ProjectProgressTable projectProgressTable = projectProgressTableView.ToProjectProgressTable();
				projectProgressTable = projectProgressTableRepo.CreateProjectProgressTable(projectProgressTable);
				base.LogTransactionCreate<ProjectProgressTable>(projectProgressTable);
				UnitOfWork.Commit();
				return projectProgressTable.ToProjectProgressTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ProjectProgressTableItemView UpdateProjectProgressTable(ProjectProgressTableItemView projectProgressTableView)
		{
			try
			{
				IProjectProgressTableRepo projectProgressTableRepo = new ProjectProgressTableRepo(db);
				ProjectProgressTable inputProjectProgressTable = projectProgressTableView.ToProjectProgressTable();
				ProjectProgressTable dbProjectProgressTable = projectProgressTableRepo.Find(inputProjectProgressTable.ProjectProgressTableGUID);
				dbProjectProgressTable = projectProgressTableRepo.UpdateProjectProgressTable(dbProjectProgressTable, inputProjectProgressTable);
				base.LogTransactionUpdate<ProjectProgressTable>(GetOriginalValues<ProjectProgressTable>(dbProjectProgressTable), dbProjectProgressTable);
				UnitOfWork.Commit();
				return dbProjectProgressTable.ToProjectProgressTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteProjectProgressTable(string item)
		{
			try
			{
				IProjectProgressTableRepo projectProgressTableRepo = new ProjectProgressTableRepo(db);
				Guid projectProgressTableGUID = new Guid(item);
				ProjectProgressTable projectProgressTable = projectProgressTableRepo.Find(projectProgressTableGUID);
				projectProgressTableRepo.Remove(projectProgressTable);
				base.LogTransactionDelete<ProjectProgressTable>(projectProgressTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		
		public ProjectProgressTableItemView ProjectProgressInitialData(string refId,string id, string transDate)
        {


            try {	


				IWithdrawalTableRepo withdrawaltableRepo = new WithdrawalTableRepo(db);
				var result = withdrawaltableRepo.GetByIdvw(id.StringToGuid());
				

				return new ProjectProgressTableItemView
					{
						ProjectProgressTableGUID = new Guid().GuidNullToString(),
						TransDate = transDate,
						RefId = refId,
						WithdrawalTableGUID = id,
						Withdrawal_Value=SmartAppUtil.GetDropDownLabel(result.WithdrawalId,result.Description)
					};
				}
				catch (Exception e)
				{
					throw SmartAppUtil.AddStackTrace(e);
				}
			
		}
		public MemoTransItemView GetMemoTransInitialData(string refGUID)
		{
			try
			{
				IMemoTransService memoTransService = new MemoTransService(db);
				IProjectProgressTableRepo progressTableRepo = new ProjectProgressTableRepo(db);
				string refId = progressTableRepo.GetProjectProgressTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).ProjectProgressId;
				return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.ProjectProgress);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region migration
		public void CreateProjectProgressTableCollection(List<ProjectProgressTableItemView> projectProgressTableItemViews)
		{
			try
			{
				if (projectProgressTableItemViews.Any())
				{
					IProjectProgressTableRepo projectProgressTableRepo = new ProjectProgressTableRepo(db);
					List<ProjectProgressTable> projectProgressTable = projectProgressTableItemViews.ToProjectProgressTable().ToList();
					projectProgressTableRepo.ValidateAdd(projectProgressTable);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(projectProgressTable, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion
	}
}
