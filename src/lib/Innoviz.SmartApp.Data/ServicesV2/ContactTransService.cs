using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IContactTransService
	{

		ContactTransItemView GetContactTransById(string id);
		ContactTransItemView CreateContactTrans(ContactTransItemView contactTransView);
		ContactTransItemView UpdateContactTrans(ContactTransItemView contactTransView);
		bool DeleteContactTrans(string id);
		ContactTransItemView GetContactTransInitialData(string refId, string refGUID, RefType refType);
		void CreateContactTransCollection(List<ContactTransItemView> contactTransItemViews);
	}
	public class ContactTransService : SmartAppService, IContactTransService
	{
		public ContactTransService(SmartAppDbContext context) : base(context) { }
		public ContactTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ContactTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ContactTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ContactTransService() : base() { }

		public ContactTransItemView GetContactTransById(string id)
		{
			try
			{
				IContactTransRepo contactTransRepo = new ContactTransRepo(db);
				return contactTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactTransItemView CreateContactTrans(ContactTransItemView contactTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				contactTransView = accessLevelService.AssignOwnerBU(contactTransView);
				IContactTransRepo contactTransRepo = new ContactTransRepo(db);
				ContactTrans contactTrans = contactTransView.ToContactTrans();
				contactTrans = contactTransRepo.CreateContactTrans(contactTrans);
				base.LogTransactionCreate<ContactTrans>(contactTrans);
				UnitOfWork.Commit();
				return contactTrans.ToContactTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactTransItemView UpdateContactTrans(ContactTransItemView contactTransView)
		{
			try
			{
				IContactTransRepo contactTransRepo = new ContactTransRepo(db);
				ContactTrans inputContactTrans = contactTransView.ToContactTrans();
				ContactTrans dbContactTrans = contactTransRepo.Find(inputContactTrans.ContactTransGUID);
				dbContactTrans = contactTransRepo.UpdateContactTrans(dbContactTrans, inputContactTrans);
				base.LogTransactionUpdate<ContactTrans>(GetOriginalValues<ContactTrans>(dbContactTrans), dbContactTrans);
				UnitOfWork.Commit();
				return dbContactTrans.ToContactTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteContactTrans(string item)
		{
			try
			{
				IContactTransRepo contactTransRepo = new ContactTransRepo(db);
				Guid contactTransGUID = new Guid(item);
				ContactTrans contactTrans = contactTransRepo.Find(contactTransGUID);
				contactTransRepo.Remove(contactTrans);
				base.LogTransactionDelete<ContactTrans>(contactTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactTransItemView GetContactTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new ContactTransItemView
				{
					ContactTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region migration
		public void CreateContactTransCollection(List<ContactTransItemView> contactTransItemViews)
		{
			try
			{
				if (contactTransItemViews.Any())
				{
					IContactTransRepo contactTransRepo = new ContactTransRepo(db);
					List<ContactTrans> contactTrans = contactTransItemViews.ToContactTrans().ToList();
					contactTransRepo.ValidateAdd(contactTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(contactTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion


	}
}
