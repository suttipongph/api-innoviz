using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IRetentionConditionTransService
	{

		RetentionConditionTransItemView GetRetentionConditionTransById(string id);
		RetentionConditionTransItemView CreateRetentionConditionTrans(RetentionConditionTransItemView retentionConditionTransView);
		RetentionConditionTransItemView UpdateRetentionConditionTrans(RetentionConditionTransItemView retentionConditionTransView);
		bool DeleteRetentionConditionTrans(string id);
		RetentionConditionTransItemView GetRetentionConditionTransInitialData(string refId, string refGUID, int refType, int productType);
		List<int> GetRetentionCalculateBase(RetentionConditionTransItemView retentionConditionTransView);
		List<int> GetRetentionCalculateMethod(RetentionConditionTransItemView model);
		void CreateRetentionConditionTransCollection(List<RetentionConditionTransItemView> retentionConditionTransItemViews);
		#region K2 function
		IEnumerable<RetentionConditionTrans> CopyRetentionConditionTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, string owner = null, Guid? ownerBusinessUnitGUID = null);
		IEnumerable<RetentionConditionTrans> GetRetentionConditionTransByReferenceAndMethod(Guid creditAppTableGUID, int retentionDeductionMethod);	
		#endregion
	}
    public class RetentionConditionTransService : SmartAppService, IRetentionConditionTransService
	{
		public RetentionConditionTransService(SmartAppDbContext context) : base(context) { }
		public RetentionConditionTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public RetentionConditionTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RetentionConditionTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public RetentionConditionTransService() : base() { }

		public RetentionConditionTransItemView GetRetentionConditionTransById(string id)
		{
			try
			{
				IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
				return retentionConditionTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionTransItemView CreateRetentionConditionTrans(RetentionConditionTransItemView retentionConditionTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				retentionConditionTransView = accessLevelService.AssignOwnerBU(retentionConditionTransView);
				IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
				RetentionConditionTrans retentionConditionTrans = retentionConditionTransView.ToRetentionConditionTrans();
				retentionConditionTrans = retentionConditionTransRepo.CreateRetentionConditionTrans(retentionConditionTrans);
				base.LogTransactionCreate<RetentionConditionTrans>(retentionConditionTrans);
				UnitOfWork.Commit();
				return retentionConditionTrans.ToRetentionConditionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionTransItemView UpdateRetentionConditionTrans(RetentionConditionTransItemView retentionConditionTransView)
		{
			try
			{
				IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
				RetentionConditionTrans inputRetentionConditionTrans = retentionConditionTransView.ToRetentionConditionTrans();
				RetentionConditionTrans dbRetentionConditionTrans = retentionConditionTransRepo.Find(inputRetentionConditionTrans.RetentionConditionTransGUID);
				dbRetentionConditionTrans = retentionConditionTransRepo.UpdateRetentionConditionTrans(dbRetentionConditionTrans, inputRetentionConditionTrans);
				base.LogTransactionUpdate<RetentionConditionTrans>(GetOriginalValues<RetentionConditionTrans>(dbRetentionConditionTrans), dbRetentionConditionTrans);
				UnitOfWork.Commit();
				return dbRetentionConditionTrans.ToRetentionConditionTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteRetentionConditionTrans(string item)
		{
			try
			{
				IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
				Guid retentionConditionTransGUID = new Guid(item);
				RetentionConditionTrans retentionConditionTrans = retentionConditionTransRepo.Find(retentionConditionTransGUID);
				retentionConditionTransRepo.Remove(retentionConditionTrans);
				base.LogTransactionDelete<RetentionConditionTrans>(retentionConditionTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionTransItemView GetRetentionConditionTransInitialData(string refId, string refGUID, int refType, int productType)
		{
			try
			{
				return new RetentionConditionTransItemView
				{
					RetentionConditionTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					ProductType = productType,
					RetentionPct = 0,
					RetentionAmount = 0
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<int> GetRetentionCalculateBase(RetentionConditionTransItemView model) 
		{
			try
			{
				IRetentionConditionSetupRepo retentionConditionSetupRepo = new RetentionConditionSetupRepo(db);
				List<int> output = new List<int>();
				IEnumerable<RetentionConditionSetup> byCompany = retentionConditionSetupRepo.GetRetentionConditionSetupByCompanyNoTracking(model.CompanyGUID.StringToGuid());
				IEnumerable<RetentionConditionSetup> byRetentionDeductionMethod = byCompany.Where(t => t.RetentionDeductionMethod == model.RetentionDeductionMethod &&
																									   t.ProductType == model.ProductType);
				foreach(RetentionConditionSetup item in byRetentionDeductionMethod)
                {
					if(!output.Contains(item.RetentionCalculateBase))
                    {
						output.Add(item.RetentionCalculateBase);
                    }
                }
				return output;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public List<int> GetRetentionCalculateMethod(RetentionConditionTransItemView model)
		{
			try
			{
				IRetentionConditionSetupRepo retentionConditionSetupRepo = new RetentionConditionSetupRepo(db);
				List<int> output = new List<int>();
				IEnumerable<RetentionConditionSetup> byCompany = retentionConditionSetupRepo.GetRetentionConditionSetupByCompanyNoTracking(model.CompanyGUID.StringToGuid());
				IEnumerable<RetentionConditionSetup> byRetentionDeductionMethod = byCompany.Where(t => t.ProductType == model.ProductType);
				foreach (RetentionConditionSetup item in byRetentionDeductionMethod)
				{
					if (!output.Contains(item.RetentionDeductionMethod))
					{
						output.Add(item.RetentionDeductionMethod);
					}
				}
				return output;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<RetentionConditionTrans> GetRetentionConditionTransByReferenceAndMethod(Guid creditAppTableGUID, int retentionDeductionMethod)
		{
			try
			{
				IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
				return retentionConditionTransRepo.GetRetentionConditionTransByReference(creditAppTableGUID,(int)RefType.CreditAppTable).Where(t => t.RetentionDeductionMethod == retentionDeductionMethod);
			}
			catch (Exception ex)
			{
				throw SmartAppUtil.AddStackTrace(ex);
			}
		}
		public void CreateRetentionConditionTransCollection(List<RetentionConditionTransItemView> retentionConditionTransItemViews)
		{
			try
			{
				if (retentionConditionTransItemViews.Any())
				{
					IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
					List<RetentionConditionTrans> retentionConditionTrans = retentionConditionTransItemViews.ToRetentionConditionTrans().ToList();
					retentionConditionTransRepo.ValidateAdd(retentionConditionTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(retentionConditionTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#region K2 function
		public IEnumerable<RetentionConditionTrans> CopyRetentionConditionTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, string owner = null, Guid? ownerBusinessUnitGUID = null)
		{
			try
			{
				IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
				List<RetentionConditionTrans> retentionConditionTrans = retentionConditionTransRepo.GetRetentionConditionTransByReference(fromRefGUID, fromRefType).ToList();
				if (retentionConditionTrans.Any())
				{
					retentionConditionTrans.ForEach(f =>
					{
						f.RetentionConditionTransGUID = Guid.NewGuid();
						f.RefGUID = toRefGUID;
						f.RefType = toRefType;
						f.Owner = owner;
						f.OwnerBusinessUnitGUID = ownerBusinessUnitGUID;
					});
				}
				return retentionConditionTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
