using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IInvoiceTableService
	{

		InvoiceLineItemView GetInvoiceLineById(string id);
		InvoiceLineItemView CreateInvoiceLine(InvoiceLineItemView invoiceLineView);
		InvoiceLineItemView UpdateInvoiceLine(InvoiceLineItemView invoiceLineView);
		bool DeleteInvoiceLine(string id);
		void CreateInvoiceLineCollection(List<InvoiceLineItemView> invoiceLineItemViews);
		InvoiceTableItemView GetInvoiceTableById(string id);
		InvoiceTableItemView CreateInvoiceTable(InvoiceTableItemView invoiceTableView);
		InvoiceTableItemView UpdateInvoiceTable(InvoiceTableItemView invoiceTableView);
		InvoiceTable CreateInvoiceTable(InvoiceTable invoiceTable);
		InvoiceTable UpdateInvoiceTable(InvoiceTable invoiceTable);
		bool DeleteInvoiceTable(string id);
		void CreateInvoiceTableCollection(List<InvoiceTableItemView> invoiceTableItemViews);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemInvoiceStatus(SearchParameter search);
		IEnumerable<SelectItem<InvoiceTableItemView>> GetRefInvoiceTableDropDown(SearchParameter search);
		IEnumerable<SelectItem<InvoiceTableItemView>> GetOriginalInvoiceDropDown(SearchParameter search);
		IEnumerable<SelectItem<InvoiceTableItemView>> GetOriginalInvoiceByServiceFeeTransDropDown(SearchParameter search);
	}
	public class InvoiceTableService : SmartAppService, IInvoiceTableService
	{
		public InvoiceTableService(SmartAppDbContext context) : base(context) { }
		public InvoiceTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public InvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InvoiceTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public InvoiceTableService() : base() { }

		public InvoiceLineItemView GetInvoiceLineById(string id)
		{
			try
			{
				IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
				return invoiceLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceLineItemView CreateInvoiceLine(InvoiceLineItemView invoiceLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				invoiceLineView = accessLevelService.AssignOwnerBU(invoiceLineView);
				IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
				InvoiceLine invoiceLine = invoiceLineView.ToInvoiceLine();
				invoiceLine = invoiceLineRepo.CreateInvoiceLine(invoiceLine);
				base.LogTransactionCreate<InvoiceLine>(invoiceLine);
				UnitOfWork.Commit();
				return invoiceLine.ToInvoiceLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceLineItemView UpdateInvoiceLine(InvoiceLineItemView invoiceLineView)
		{
			try
			{
				IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
				InvoiceLine inputInvoiceLine = invoiceLineView.ToInvoiceLine();
				InvoiceLine dbInvoiceLine = invoiceLineRepo.Find(inputInvoiceLine.InvoiceLineGUID);
				dbInvoiceLine = invoiceLineRepo.UpdateInvoiceLine(dbInvoiceLine, inputInvoiceLine);
				base.LogTransactionUpdate<InvoiceLine>(GetOriginalValues<InvoiceLine>(dbInvoiceLine), dbInvoiceLine);
				UnitOfWork.Commit();
				return dbInvoiceLine.ToInvoiceLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteInvoiceLine(string item)
		{
			try
			{
				IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
				Guid invoiceLineGUID = new Guid(item);
				InvoiceLine invoiceLine = invoiceLineRepo.Find(invoiceLineGUID);
				invoiceLineRepo.Remove(invoiceLine);
				base.LogTransactionDelete<InvoiceLine>(invoiceLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public InvoiceTableItemView GetInvoiceTableById(string id)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				return invoiceTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceTableItemView CreateInvoiceTable(InvoiceTableItemView invoiceTableView)
		{
			try
			{
				InvoiceTable invoiceTable = invoiceTableView.ToInvoiceTable();
				invoiceTable = CreateInvoiceTable(invoiceTable);
				UnitOfWork.Commit();
				return invoiceTable.ToInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceTable CreateInvoiceTable(InvoiceTable invoiceTable)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				invoiceTable = accessLevelService.AssignOwnerBU(invoiceTable);
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				invoiceTable = invoiceTableRepo.CreateInvoiceTable(invoiceTable);
				base.LogTransactionCreate<InvoiceTable>(invoiceTable);
				return invoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
		public InvoiceTableItemView UpdateInvoiceTable(InvoiceTableItemView invoiceTableView)
		{
			try
			{
				InvoiceTable invoiceTable = invoiceTableView.ToInvoiceTable();
				invoiceTable = UpdateInvoiceTable(invoiceTable);
				UnitOfWork.Commit();
				return invoiceTable.ToInvoiceTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceTable UpdateInvoiceTable(InvoiceTable invoiceTable)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				InvoiceTable dbInvoiceTable = invoiceTableRepo.Find(invoiceTable.InvoiceTableGUID);
				dbInvoiceTable = invoiceTableRepo.UpdateInvoiceTable(dbInvoiceTable, invoiceTable);
				base.LogTransactionUpdate<InvoiceTable>(GetOriginalValues<InvoiceTable>(dbInvoiceTable), dbInvoiceTable);
				return dbInvoiceTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteInvoiceTable(string item)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				Guid invoiceTableGUID = new Guid(item);
				InvoiceTable invoiceTable = invoiceTableRepo.Find(invoiceTableGUID);
				invoiceTableRepo.Remove(invoiceTable);
				base.LogTransactionDelete<InvoiceTable>(invoiceTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateInvoiceTableCollection(List<InvoiceTableItemView> invoiceTableItemViews)
		{
			try
			{
				if (invoiceTableItemViews.Any())
				{
					IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
					IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
					IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
					IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
					IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
					List<InvoiceTable> invoiceTables = invoiceTableItemViews.ToInvoiceTable()
						.Select(s =>
						{
							s.InvoiceTableGUID = Guid.NewGuid();
							return s;
						}).ToList();
					invoiceTableRepo.ValidateAdd(invoiceTables);

					List<InvoiceType> invoiceTypes = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTables.GroupBy(g => g.InvoiceTypeGUID).Select(s => s.Key));
					List<WithdrawalLine> withdrawalLines = withdrawalLineRepo.GetWithdrawalLineByIdNoTracking(invoiceTables.Where(w=> w.RefType == (int)RefType.WithdrawalLine).GroupBy(g => g.RefGUID).Select(s => s.Key));
					List<WithdrawalTable> withdrawalTables = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalLines.GroupBy(g => g.WithdrawalTableGUID).Select(s => s.Key));
					List<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByIdNoTracking(invoiceTables.Where(w => w.RefType == (int)RefType.PurchaseLine).GroupBy(g => g.RefGUID).Select(s => s.Key));
					#region GenCustTrans
					List<CustTrans> custTrans = invoiceTables.Select(s => new CustTrans
					{
						InvoiceTableGUID = s.InvoiceTableGUID,
						CustomerTableGUID = s.CustomerTableGUID,
						TransDate = s.IssuedDate,
						Description = invoiceTypes.Where(w => w.InvoiceTypeGUID == s.InvoiceTypeGUID).FirstOrDefault().Description + " " + s.DocumentId,
						DueDate = s.DueDate,
						CurrencyGUID = s.CurrencyGUID,
						TransAmount = s.InvoiceAmount,
						TransAmountMST = s.InvoiceAmountMST,
						LastSettleDate = null,
						SettleAmount = 0,
						SettleAmountMST = 0,
						ExchangeRate = s.ExchangeRate,
						CustTransStatus = (int)CustTransStatus.Open,
						InvoiceTypeGUID = s.InvoiceTypeGUID,
						CompanyGUID = s.CompanyGUID,
						BranchGUID = s.BranchGUID,
						ProductType = s.ProductType,
						CreditAppTableGUID = s.CreditAppTableGUID,
						CreditAppLineGUID = s.RefType == (int)RefType.WithdrawalLine 
												? withdrawalTables.Where(w => w.WithdrawalTableGUID == withdrawalLines.Where(w => w.WithdrawalLineGUID == s.RefGUID).FirstOrDefault().WithdrawalTableGUID).FirstOrDefault().CreditAppLineGUID
											: s.RefType == (int)RefType.PurchaseLine
												? (Guid?)purchaseLines.Where(w=> w.PurchaseLineGUID == s.RefGUID).FirstOrDefault().CreditAppLineGUID
												: null,
						BuyerTableGUID = s.BuyerTableGUID,
						CustTransGUID = Guid.NewGuid()
					}).ToList();
					#endregion GenCustTrans
					#region GenProcessTrans
					List<ProcessTrans> processTrans = invoiceTables.Select(s => new ProcessTrans
					{
						TransDate = s.IssuedDate,
						CreditAppTableGUID = s.CreditAppTableGUID,
						CustomerTableGUID = s.CustomerTableGUID,
						Revert = false,
						ProcessTransType = (int)ProcessTransType.Invoice,
						Amount = s.InvoiceAmount,
						AmountMST = s.InvoiceAmountMST,
						ExchangeRate = s.ExchangeRate,
						TaxAmount = s.TaxAmount,
						TaxAmountMST = s.TaxAmountMST,
						ARLedgerAccount = invoiceTypes.Where(w => w.InvoiceTypeGUID == s.InvoiceTypeGUID).FirstOrDefault().ARLedgerAccount,
						ProductType = s.ProductType,
						CurrencyGUID = s.CurrencyGUID,
						DocumentReasonGUID = null,
						RefTaxInvoiceGUID = null,
						TaxTableGUID = null,
						OrigTaxTableGUID = null,
						RefType = s.RefType,
						RefGUID = s.RefGUID,
						InvoiceTableGUID = s.InvoiceTableGUID,
						DocumentId = s.DocumentId,
						RefProcessTransGUID = null,
						StagingBatchId = "Migration",
						StagingBatchStatus = (int)StagingBatchStatus.Success,
						PaymentHistoryGUID = null,
						CompanyGUID = s.CompanyGUID,
						ProcessTransGUID = Guid.NewGuid()
					}).ToList();
					#endregion GenProcessTrans
					#region GenRetentionTrans
					List<RetentionTrans> retentionTrans = invoiceTables.Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.Retention).Select(s => new RetentionTrans
					{
						CustomerTableGUID = s.CustomerTableGUID,
						CreditAppTableGUID = s.CreditAppTableGUID.Value,
						ProductType = s.ProductType,
						TransDate = s.IssuedDate,
						Amount = s.InvoiceAmount * -1,
						BuyerTableGUID = s.BuyerTableGUID,
						BuyerAgreementTableGUID = s.BuyerAgreementTableGUID,
						CompanyGUID = s.CompanyGUID,
						RefType = s.RefType,
						RefGUID = s.RefGUID,
						DocumentId = s.DocumentId,
						RetentionTransGUID = Guid.NewGuid()
					}).ToList();
					#endregion GenRetentionTrans

					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(invoiceTables, false);

						BulkInsert(custTrans, false);
						BulkInsert(processTrans, false);
						if(ConditionService.IsNotZero(retentionTrans.Count()))
						{
							BulkInsert(retentionTrans, false);
						}
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public void CreateInvoiceLineCollection(List<InvoiceLineItemView> invoiceLineItemViews)
		{
			try
			{
				if (invoiceLineItemViews.Any())
				{
					IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
					List<InvoiceLine> invoiceLines = invoiceLineItemViews.ToInvoiceLine().ToList();
					invoiceLineRepo.ValidateAdd(invoiceLines);

					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(invoiceLines, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}

		#region DropDown
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemInvoiceStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Invoice);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceTableItemView>> GetRefInvoiceTableDropDown(SearchParameter search)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				search = search.GetParentCondition(InvoiceTableCondition.InvoiceTableGUID);
				return invoiceTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceTableItemView>> GetOriginalInvoiceDropDown(SearchParameter search)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				search = search.GetParentCondition(InvoiceTableCondition.CreditAppRequestTableGUID);
				return invoiceTableRepo.GetOriginalInvoiceDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceTableItemView>> GetOriginalInvoiceByServiceFeeTransDropDown(SearchParameter search)
		{
			try
			{
				IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
				search = search.GetParentCondition(InvoiceTableCondition.CustomerTableGUID);
				SearchCondition searchConditionSuspenseInvoiceType = SearchConditionService.GetInvoiceTableBySuspenseInvoiceType(SuspenseInvoiceType.None.GetAttrValue().ToString());
				SearchCondition searchConditionProductInvoice = SearchConditionService.GetInvoiceTableByProductInvoice(false.ToString());
				search.Conditions.Add(searchConditionSuspenseInvoiceType);
				search.Conditions.Add(searchConditionProductInvoice);
				return invoiceTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
