using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IRaceService
	{

		RaceItemView GetRaceById(string id);
		RaceItemView CreateRace(RaceItemView raceView);
		RaceItemView UpdateRace(RaceItemView raceView);
		bool DeleteRace(string id);
	}
	public class RaceService : SmartAppService, IRaceService
	{
		public RaceService(SmartAppDbContext context) : base(context) { }
		public RaceService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public RaceService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RaceService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public RaceService() : base() { }

		public RaceItemView GetRaceById(string id)
		{
			try
			{
				IRaceRepo raceRepo = new RaceRepo(db);
				return raceRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RaceItemView CreateRace(RaceItemView raceView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				raceView = accessLevelService.AssignOwnerBU(raceView);
				IRaceRepo raceRepo = new RaceRepo(db);
				Race race = raceView.ToRace();
				race = raceRepo.CreateRace(race);
				base.LogTransactionCreate<Race>(race);
				UnitOfWork.Commit();
				return race.ToRaceItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RaceItemView UpdateRace(RaceItemView raceView)
		{
			try
			{
				IRaceRepo raceRepo = new RaceRepo(db);
				Race inputRace = raceView.ToRace();
				Race dbRace = raceRepo.Find(inputRace.RaceGUID);
				dbRace = raceRepo.UpdateRace(dbRace, inputRace);
				base.LogTransactionUpdate<Race>(GetOriginalValues<Race>(dbRace), dbRace);
				UnitOfWork.Commit();
				return dbRace.ToRaceItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteRace(string item)
		{
			try
			{
				IRaceRepo raceRepo = new RaceRepo(db);
				Guid raceGUID = new Guid(item);
				Race race = raceRepo.Find(raceGUID);
				raceRepo.Remove(race);
				base.LogTransactionDelete<Race>(race);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
