using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDocumentTemplateTableService
	{

		Task<DocumentTemplateTableItemView> GetDocumentTemplateTableById(string id);
		DocumentTemplateTableItemView CreateDocumentTemplateTable(DocumentTemplateTableItemView documentTemplateTableView);
		DocumentTemplateTableItemView UpdateDocumentTemplateTable(DocumentTemplateTableItemView documentTemplateTableView);
		bool DeleteDocumentTemplateTable(string id);
		IEnumerable<SelectItem<DocumentTemplateTableItemView>> GetDocumentTemplateTableByDocumentType(SearchParameter search);
	}
	public class DocumentTemplateTableService : SmartAppService, IDocumentTemplateTableService
	{
		public DocumentTemplateTableService(SmartAppDbContext context) : base(context) { }
		public DocumentTemplateTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DocumentTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentTemplateTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DocumentTemplateTableService() : base() { }

		public async Task<DocumentTemplateTableItemView> GetDocumentTemplateTableById(string id)
		{
			try
			{
				IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
				return await documentTemplateTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentTemplateTableItemView CreateDocumentTemplateTable(DocumentTemplateTableItemView documentTemplateTableView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentTemplateTableView = accessLevelService.AssignOwnerBU(documentTemplateTableView);
				IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
				DocumentTemplateTable documentTemplateTable = documentTemplateTableView.ToDocumentTemplateTable();
				documentTemplateTable = documentTemplateTableRepo.CreateDocumentTemplateTable(documentTemplateTable);
				base.LogTransactionCreate<DocumentTemplateTable>(documentTemplateTable);
				UnitOfWork.Commit();
				return documentTemplateTable.ToDocumentTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentTemplateTableItemView UpdateDocumentTemplateTable(DocumentTemplateTableItemView documentTemplateTableView)
		{
			try
			{
				IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
				DocumentTemplateTable inputDocumentTemplateTable = documentTemplateTableView.ToDocumentTemplateTable();
				DocumentTemplateTable dbDocumentTemplateTable = documentTemplateTableRepo.Find(inputDocumentTemplateTable.DocumentTemplateTableGUID);
				dbDocumentTemplateTable = documentTemplateTableRepo.UpdateDocumentTemplateTable(dbDocumentTemplateTable, inputDocumentTemplateTable);
				base.LogTransactionUpdate<DocumentTemplateTable>(GetOriginalValues<DocumentTemplateTable>(dbDocumentTemplateTable), dbDocumentTemplateTable);
				UnitOfWork.Commit();
				return dbDocumentTemplateTable.ToDocumentTemplateTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentTemplateTable(string item)
		{
			try
			{
				IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
				Guid documentTemplateTableGUID = new Guid(item);
				DocumentTemplateTable documentTemplateTable = documentTemplateTableRepo.Find(documentTemplateTableGUID);
				documentTemplateTableRepo.Remove(documentTemplateTable);
				base.LogTransactionDelete<DocumentTemplateTable>(documentTemplateTable);
				UnitOfWork.Commit();
				documentTemplateTable.EnsureFileDeleted();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentTemplateTableItemView>> GetDocumentTemplateTableByDocumentType(SearchParameter search)
		{
			try
			{
				IDocumentTemplateTableRepo documentTemplateTableRepo = new DocumentTemplateTableRepo(db);
				search = search.GetParentCondition(DocumentTemplateTypeCondition.DocumentTemplateType);
				return documentTemplateTableRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
