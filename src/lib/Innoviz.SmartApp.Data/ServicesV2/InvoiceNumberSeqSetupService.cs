using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IInvoiceNumberSeqSetupService
	{

		InvoiceNumberSeqSetupItemView GetInvoiceNumberSeqSetupById(string id);
		InvoiceNumberSeqSetupItemView CreateInvoiceNumberSeqSetup(InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupView);
		InvoiceNumberSeqSetupItemView UpdateInvoiceNumberSeqSetup(InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupView);
		bool DeleteInvoiceNumberSeqSetup(string id);
		#region function
		InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByBranchAndProductTypeAndInvoiceType(string branchGuid, int productType, string invoiceTypeGuid);
		#endregion
	}
    public class InvoiceNumberSeqSetupService : SmartAppService, IInvoiceNumberSeqSetupService
	{
		public InvoiceNumberSeqSetupService(SmartAppDbContext context) : base(context) { }
		public InvoiceNumberSeqSetupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public InvoiceNumberSeqSetupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InvoiceNumberSeqSetupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public InvoiceNumberSeqSetupService() : base() { }

		public InvoiceNumberSeqSetupItemView GetInvoiceNumberSeqSetupById(string id)
		{
			try
			{
				IInvoiceNumberSeqSetupRepo invoiceNumberSeqSetupRepo = new InvoiceNumberSeqSetupRepo(db);
				return invoiceNumberSeqSetupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceNumberSeqSetupItemView CreateInvoiceNumberSeqSetup(InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				invoiceNumberSeqSetupView = accessLevelService.AssignOwnerBU(invoiceNumberSeqSetupView);
				IInvoiceNumberSeqSetupRepo invoiceNumberSeqSetupRepo = new InvoiceNumberSeqSetupRepo(db);
				InvoiceNumberSeqSetup invoiceNumberSeqSetup = invoiceNumberSeqSetupView.ToInvoiceNumberSeqSetup();
				invoiceNumberSeqSetup = invoiceNumberSeqSetupRepo.CreateInvoiceNumberSeqSetup(invoiceNumberSeqSetup);
				base.LogTransactionCreate<InvoiceNumberSeqSetup>(invoiceNumberSeqSetup);
				UnitOfWork.Commit();
				return invoiceNumberSeqSetup.ToInvoiceNumberSeqSetupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceNumberSeqSetupItemView UpdateInvoiceNumberSeqSetup(InvoiceNumberSeqSetupItemView invoiceNumberSeqSetupView)
		{
			try
			{
				IInvoiceNumberSeqSetupRepo invoiceNumberSeqSetupRepo = new InvoiceNumberSeqSetupRepo(db);
				InvoiceNumberSeqSetup inputInvoiceNumberSeqSetup = invoiceNumberSeqSetupView.ToInvoiceNumberSeqSetup();
				InvoiceNumberSeqSetup dbInvoiceNumberSeqSetup = invoiceNumberSeqSetupRepo.Find(inputInvoiceNumberSeqSetup.InvoiceNumberSeqSetupGUID);
				dbInvoiceNumberSeqSetup = invoiceNumberSeqSetupRepo.UpdateInvoiceNumberSeqSetup(dbInvoiceNumberSeqSetup, inputInvoiceNumberSeqSetup);
				base.LogTransactionUpdate<InvoiceNumberSeqSetup>(GetOriginalValues<InvoiceNumberSeqSetup>(dbInvoiceNumberSeqSetup), dbInvoiceNumberSeqSetup);
				UnitOfWork.Commit();
				return dbInvoiceNumberSeqSetup.ToInvoiceNumberSeqSetupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteInvoiceNumberSeqSetup(string item)
		{
			try
			{
				IInvoiceNumberSeqSetupRepo invoiceNumberSeqSetupRepo = new InvoiceNumberSeqSetupRepo(db);
				Guid invoiceNumberSeqSetupGUID = new Guid(item);
				InvoiceNumberSeqSetup invoiceNumberSeqSetup = invoiceNumberSeqSetupRepo.Find(invoiceNumberSeqSetupGUID);
				invoiceNumberSeqSetupRepo.Remove(invoiceNumberSeqSetup);
				base.LogTransactionDelete<InvoiceNumberSeqSetup>(invoiceNumberSeqSetup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region function
		public InvoiceNumberSeqSetup GetInvoiceNumberSeqSetupByBranchAndProductTypeAndInvoiceType(string branchGuid,int productType,string invoiceTypeGuid)
        {
			try
			{
				IInvoiceNumberSeqSetupRepo invoiceNumberSeqSetupRepo = new InvoiceNumberSeqSetupRepo(db);
				return invoiceNumberSeqSetupRepo.GetInvoiceNumberSeqSetupByBranchAndProductTypeAndInvoiceType(branchGuid.StringToGuid(), productType, invoiceTypeGuid.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

	}
}
