using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAgreementTableInfoService
	{

		AgreementTableInfoItemView GetAgreementTableInfoById(string id);
		AgreementTableInfoItemView CreateAgreementTableInfo(AgreementTableInfoItemView agreementTableInfoView);
		AgreementTableInfo CreateAgreementTableInfo(AgreementTableInfo agreementTableInfo);
		AgreementTableInfoItemView UpdateAgreementTableInfo(AgreementTableInfoItemView agreementTableInfoView);
		bool DeleteAgreementTableInfo(string id);
		CopyAgreementInfoView CopyAgreementInfo(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType);
		AgreementTableInfoItemView GetAgreementTableInfoByMainAgreement(string id);
		IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByAgreementInfo(SearchParameter search);
		AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refId, string refGUID, RefType refType);
		AgreementTableInfoItemView GetAgreementTableInfoByAssignmentAgreement(string id);
		AgreementTableInfoItemView GetAgreementTableInfoByBusinessCollateralAgm(string id);
		void CreateAgreementTableInfoCollection(IEnumerable<AgreementTableInfoItemView> agreementTableInfoItemViews);
	}
	public class AgreementTableInfoService : SmartAppService, IAgreementTableInfoService
	{
		public AgreementTableInfoService(SmartAppDbContext context) : base(context) { }
		public AgreementTableInfoService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AgreementTableInfoService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AgreementTableInfoService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AgreementTableInfoService() : base() { }

		public AgreementTableInfoItemView GetAgreementTableInfoById(string id)
		{
			try
			{
				IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
				return agreementTableInfoRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoItemView CreateAgreementTableInfo(AgreementTableInfoItemView agreementTableInfoView)
		{
			try
			{				
				AgreementTableInfo agreementTableInfo = agreementTableInfoView.ToAgreementTableInfo();
				agreementTableInfo = CreateAgreementTableInfo(agreementTableInfo);

				// Create AgreementInfoText
				CreateAgreementTableInfoText(agreementTableInfoView);

				UnitOfWork.Commit();
				return agreementTableInfo.ToAgreementTableInfoItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfo CreateAgreementTableInfo(AgreementTableInfo agreementTableInfo)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				agreementTableInfo = accessLevelService.AssignOwnerBU(agreementTableInfo);
				IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
				agreementTableInfo = agreementTableInfoRepo.CreateAgreementTableInfo(agreementTableInfo);
				base.LogTransactionCreate<AgreementTableInfo>(agreementTableInfo);				
				return agreementTableInfo;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoItemView UpdateAgreementTableInfo(AgreementTableInfoItemView agreementTableInfoView)
		{
			try
			{
				IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
				AgreementTableInfo inputAgreementTableInfo = agreementTableInfoView.ToAgreementTableInfo();
				AgreementTableInfo dbAgreementTableInfo = agreementTableInfoRepo.Find(inputAgreementTableInfo.AgreementTableInfoGUID);
				dbAgreementTableInfo = agreementTableInfoRepo.UpdateAgreementTableInfo(dbAgreementTableInfo, inputAgreementTableInfo);
				base.LogTransactionUpdate<AgreementTableInfo>(GetOriginalValues<AgreementTableInfo>(dbAgreementTableInfo), dbAgreementTableInfo);

				// agreementinfoText
				CreateAgreementTableInfoText(agreementTableInfoView);

				UnitOfWork.Commit();
				return dbAgreementTableInfo.ToAgreementTableInfoItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAgreementTableInfo(string item)
		{
			try
			{
				IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
				Guid agreementTableInfoGUID = new Guid(item);
				AgreementTableInfo agreementTableInfo = agreementTableInfoRepo.Find(agreementTableInfoGUID);
				agreementTableInfoRepo.Remove(agreementTableInfo);
				base.LogTransactionDelete<AgreementTableInfo>(agreementTableInfo);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public void CreateAgreementTableInfoText(AgreementTableInfoItemView agreementTableInfoView)
		{
			try
			{
				// Create AgreementInfoText
				IAgreementTableInfoTextService agreementTableInfoTextService = new AgreementTableInfoTextService(db);
				IAgreementTableInfoTextRepo agreementTableInfoTextRepo = new AgreementTableInfoTextRepo(db);
				AgreementTableInfoText agreementTableInfoText = agreementTableInfoTextRepo.GetAgreementTableInfoTextByAgreementTableInfoNoTracking(agreementTableInfoView.AgreementTableInfoGUID.StringToGuid());

				if (agreementTableInfoText == null)
				{
					if ((agreementTableInfoView.AgreementTableInfoText_Text1 != null && agreementTableInfoView.AgreementTableInfoText_Text1 != "") ||
						(agreementTableInfoView.AgreementTableInfoText_Text2 != null && agreementTableInfoView.AgreementTableInfoText_Text2 != "") ||
						(agreementTableInfoView.AgreementTableInfoText_Text3 != null && agreementTableInfoView.AgreementTableInfoText_Text3 != "") ||
						(agreementTableInfoView.AgreementTableInfoText_Text4 != null && agreementTableInfoView.AgreementTableInfoText_Text4 != ""))
					{
						AgreementTableInfoText newAgreementTableInfoText = new AgreementTableInfoText
						{
							Text1 = agreementTableInfoView.AgreementTableInfoText_Text1,
							Text2 = agreementTableInfoView.AgreementTableInfoText_Text2,
							Text3 = agreementTableInfoView.AgreementTableInfoText_Text3,
							Text4 = agreementTableInfoView.AgreementTableInfoText_Text4,
							CompanyGUID = agreementTableInfoView.CompanyGUID.StringToGuid(),
							AgreementTableInfoGUID = agreementTableInfoView.AgreementTableInfoGUID.StringToGuid(),
						};
						agreementTableInfoTextService.CreateAgreementTableInfoText(newAgreementTableInfoText);
					}
				}
				else
				{
					agreementTableInfoText.Text1 = agreementTableInfoView.AgreementTableInfoText_Text1;
					agreementTableInfoText.Text2 = agreementTableInfoView.AgreementTableInfoText_Text2;
					agreementTableInfoText.Text3 = agreementTableInfoView.AgreementTableInfoText_Text3;
					agreementTableInfoText.Text4 = agreementTableInfoView.AgreementTableInfoText_Text4;
					agreementTableInfoTextService.UpdateAgreementTableInfoText(agreementTableInfoText);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public CopyAgreementInfoView CopyAgreementInfo(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType)
		{
			try
			{
				IAgreementTableInfoRepo agreementInfoTableRepo = new AgreementTableInfoRepo(db);
				IAgreementTableInfoTextRepo agreementTableInfoTextRepo = new AgreementTableInfoTextRepo(db);
				AgreementTableInfo originAgreementTableInfo = agreementInfoTableRepo.GetAgreementTableInfoByReferenceTableNoTracking(fromRefGUID, fromRefType);
				if (originAgreementTableInfo != null)
				{
					CopyAgreementInfoView copyAgreementInfoView = new CopyAgreementInfoView();
					copyAgreementInfoView.AgreementTableInfo = new AgreementTableInfo
					{
						AgreementTableInfoGUID = Guid.NewGuid(),
						CompanyGUID = originAgreementTableInfo.CompanyGUID,
						RefType = toRefType,
						RefGUID = toRefGUID,
						GuarantorName = originAgreementTableInfo.GuarantorName,
						GuaranteeText = originAgreementTableInfo.GuaranteeText,
						CustomerAddress1 = originAgreementTableInfo.CustomerAddress1,
						CustomerAddress2 = originAgreementTableInfo.CustomerAddress2,
						AuthorizedPersonTransCustomer1GUID = originAgreementTableInfo.AuthorizedPersonTransCustomer1GUID,
						AuthorizedPersonTransCustomer2GUID = originAgreementTableInfo.AuthorizedPersonTransCustomer2GUID,
						AuthorizedPersonTransCustomer3GUID = originAgreementTableInfo.AuthorizedPersonTransCustomer3GUID,
						AuthorizedPersonTypeCustomerGUID = originAgreementTableInfo.AuthorizedPersonTypeCustomerGUID,
						WitnessCustomer1 = originAgreementTableInfo.WitnessCustomer1,
						WitnessCustomer2 = originAgreementTableInfo.WitnessCustomer2,
						BuyerAddress1 = originAgreementTableInfo.BuyerAddress1,
						BuyerAddress2 = originAgreementTableInfo.BuyerAddress2,
						AuthorizedPersonTransBuyer1GUID = originAgreementTableInfo.AuthorizedPersonTransBuyer1GUID,
						AuthorizedPersonTransBuyer2GUID = originAgreementTableInfo.AuthorizedPersonTransBuyer2GUID,
						AuthorizedPersonTransBuyer3GUID = originAgreementTableInfo.AuthorizedPersonTransBuyer3GUID,
						AuthorizedPersonTypeBuyerGUID = originAgreementTableInfo.AuthorizedPersonTypeBuyerGUID,
						CompanyName = originAgreementTableInfo.CompanyName,
						CompanyAltName = originAgreementTableInfo.CompanyAltName,
						CompanyAddress1 = originAgreementTableInfo.CompanyAddress1,
						CompanyAddress2 = originAgreementTableInfo.CompanyAddress2,
						CompanyPhone = originAgreementTableInfo.CompanyPhone,
						CompanyFax = originAgreementTableInfo.CompanyFax,
						CompanyBankGUID = originAgreementTableInfo.CompanyBankGUID,
						AuthorizedPersonTransCompany1GUID = originAgreementTableInfo.AuthorizedPersonTransCompany1GUID,
						AuthorizedPersonTransCompany2GUID = originAgreementTableInfo.AuthorizedPersonTransCompany2GUID,
						AuthorizedPersonTransCompany3GUID = originAgreementTableInfo.AuthorizedPersonTransCompany3GUID,
						AuthorizedPersonTypeCompanyGUID = originAgreementTableInfo.AuthorizedPersonTypeCompanyGUID,
						Owner = originAgreementTableInfo.Owner,
						OwnerBusinessUnitGUID = originAgreementTableInfo.OwnerBusinessUnitGUID,
						WitnessCompany1GUID = originAgreementTableInfo.WitnessCompany1GUID,
						WitnessCompany2GUID = originAgreementTableInfo.WitnessCompany2GUID
					};
					AgreementTableInfoText originAgreementTableInfoText = agreementTableInfoTextRepo.GetAgreementTableInfoTextByAgreementTableInfoNoTracking(originAgreementTableInfo.AgreementTableInfoGUID);
					if (originAgreementTableInfoText != null)
					{
						copyAgreementInfoView.AgreementTableInfoText = new AgreementTableInfoText
						{
							AgreementTableInfoTextGUID = Guid.NewGuid(),
							Owner = originAgreementTableInfoText.Owner,
							OwnerBusinessUnitGUID = originAgreementTableInfoText.OwnerBusinessUnitGUID,
							Text1 = originAgreementTableInfoText.Text1,
							Text2 = originAgreementTableInfoText.Text2,
							Text3 = originAgreementTableInfoText.Text3,
							Text4 = originAgreementTableInfoText.Text4,
							CompanyGUID = originAgreementTableInfoText.CompanyGUID,
							AgreementTableInfoGUID = copyAgreementInfoView.AgreementTableInfo.AgreementTableInfoGUID,
						};
					}
					return copyAgreementInfoView;
				}
				else
				{
					return null;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AgreementTableInfoItemView GetAgreementTableInfoByMainAgreement(string id)
		{
			try
			{
				IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
				IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
				MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(id.StringToGuid());
				AgreementTableInfo agreementTableInfo = agreementTableInfoRepo.GetAgreementTableInfoByReferenceTableNoTracking(mainAgreementTable.MainAgreementTableGUID, (int)RefType.MainAgreement);
				if(agreementTableInfo != null)
                {
					return agreementTableInfo.ToAgreementTableInfoItemView();
				}
				else
                {
					return null;
                }
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<AuthorizedPersonTransItemView>> GetDropDownItemAuthorizedPersonTransByAgreementInfo(SearchParameter search)
		{
			try
			{
				IAuthorizedPersonTransService authorizedPersonTransService = new AuthorizedPersonTransService(db);
				search = search.GetParentCondition(new string[] {AuthorizedPersonTransCondition.RefGUID, AuthorizedPersonTransCondition.RefGUID });
				List<SearchCondition> tempConditions = search.Conditions;
				search.Conditions = new SearchCondition[] { tempConditions[0] }.ToList();
				IEnumerable<SelectItem<AuthorizedPersonTransItemView>> tempList = authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCreditApp(search);
				if (tempList.Count() > 0)
				{
					return tempList;
				}
				else
                {
					search.Conditions = new SearchCondition[] { tempConditions[1] }.ToList();
					return authorizedPersonTransService.GetDropDownItemAuthorizedPersonTransByCustomer(search);
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new AgreementTableInfoItemView
				{
					AgreementTableInfoGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AgreementTableInfoItemView GetAgreementTableInfoByAssignmentAgreement(string id)
		{
			try
			{
				IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
				IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
				AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTracking(id.StringToGuid());
				AgreementTableInfo agreementTableInfo = agreementTableInfoRepo.GetAgreementTableInfoByReferenceTableNoTracking(assignmentAgreementTable.AssignmentAgreementTableGUID, (int)RefType.AssignmentAgreement);
				if (agreementTableInfo != null)
				{
					AgreementTableInfoItemView agreementTableInfoItemView = agreementTableInfo.ToAgreementTableInfoItemView();
					agreementTableInfoItemView.RefId = assignmentAgreementTable.InternalAssignmentAgreementId;
					return agreementTableInfoItemView;
				}
				else
				{
					return null;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AgreementTableInfoItemView GetAgreementTableInfoByBusinessCollateralAgm(string id)
		{
			try
			{
				IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
				IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
				BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(id.StringToGuid());
				AgreementTableInfo agreementTableInfo = agreementTableInfoRepo.GetAgreementTableInfoByReferenceTableNoTracking(businessCollateralAgmTable.BusinessCollateralAgmTableGUID, (int)RefType.BusinessCollateralAgreement);
				if (agreementTableInfo != null)
				{
					return agreementTableInfo.ToAgreementTableInfoItemView();
				}
				else
				{
					return null;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region Migration
		public void CreateAgreementTableInfoCollection(IEnumerable<AgreementTableInfoItemView> agreementTableInfoItemViews)
		{
			try
			{
				if (agreementTableInfoItemViews.Any())
				{
					IAgreementTableInfoRepo agreementTableInfoRepo = new AgreementTableInfoRepo(db);
					List<AgreementTableInfoText> agreementTableInfoTexts = new List<AgreementTableInfoText>();
					agreementTableInfoItemViews.ToList().ForEach(f =>
					{
						agreementTableInfoTexts.Add(new AgreementTableInfoText
						{
							AgreementTableInfoTextGUID = Guid.NewGuid(),
							AgreementTableInfoGUID = f.AgreementTableInfoGUID.StringToGuid(),
							Text1 = f.Text1,
							Text2 = f.Text2,
							Text3 = f.Text3,
							Text4 = f.Text4,
							CompanyGUID = f.CompanyGUID.StringToGuid(),
							Owner = f.Owner,
							OwnerBusinessUnitGUID = f.OwnerBusinessUnitGUID.StringToGuid(),
						});
					});
					List<AgreementTableInfo> agreementTableInfos = agreementTableInfoItemViews.ToAgreementTableInfo().ToList();

					agreementTableInfoRepo.ValidateAdd(agreementTableInfos);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(agreementTableInfos);
						BulkInsert(agreementTableInfoTexts);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

	}
}
