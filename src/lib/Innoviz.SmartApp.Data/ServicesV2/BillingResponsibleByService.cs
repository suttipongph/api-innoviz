using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBillingResponsibleByService
	{

		BillingResponsibleByItemView GetBillingResponsibleByById(string id);
		BillingResponsibleByItemView CreateBillingResponsibleBy(BillingResponsibleByItemView billingResponsibleByView);
		BillingResponsibleByItemView UpdateBillingResponsibleBy(BillingResponsibleByItemView billingResponsibleByView);
		bool DeleteBillingResponsibleBy(string id);
	}
	public class BillingResponsibleByService : SmartAppService, IBillingResponsibleByService
	{
		public BillingResponsibleByService(SmartAppDbContext context) : base(context) { }
		public BillingResponsibleByService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BillingResponsibleByService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BillingResponsibleByService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BillingResponsibleByService() : base() { }

		public BillingResponsibleByItemView GetBillingResponsibleByById(string id)
		{
			try
			{
				IBillingResponsibleByRepo billingResponsibleByRepo = new BillingResponsibleByRepo(db);
				return billingResponsibleByRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BillingResponsibleByItemView CreateBillingResponsibleBy(BillingResponsibleByItemView billingResponsibleByView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				billingResponsibleByView = accessLevelService.AssignOwnerBU(billingResponsibleByView);
				IBillingResponsibleByRepo billingResponsibleByRepo = new BillingResponsibleByRepo(db);
				BillingResponsibleBy billingResponsibleBy = billingResponsibleByView.ToBillingResponsibleBy();
				billingResponsibleBy = billingResponsibleByRepo.CreateBillingResponsibleBy(billingResponsibleBy);
				base.LogTransactionCreate<BillingResponsibleBy>(billingResponsibleBy);
				UnitOfWork.Commit();
				return billingResponsibleBy.ToBillingResponsibleByItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BillingResponsibleByItemView UpdateBillingResponsibleBy(BillingResponsibleByItemView billingResponsibleByView)
		{
			try
			{
				IBillingResponsibleByRepo billingResponsibleByRepo = new BillingResponsibleByRepo(db);
				BillingResponsibleBy inputBillingResponsibleBy = billingResponsibleByView.ToBillingResponsibleBy();
				BillingResponsibleBy dbBillingResponsibleBy = billingResponsibleByRepo.Find(inputBillingResponsibleBy.BillingResponsibleByGUID);
				dbBillingResponsibleBy = billingResponsibleByRepo.UpdateBillingResponsibleBy(dbBillingResponsibleBy, inputBillingResponsibleBy);
				base.LogTransactionUpdate<BillingResponsibleBy>(GetOriginalValues<BillingResponsibleBy>(dbBillingResponsibleBy), dbBillingResponsibleBy);
				UnitOfWork.Commit();
				return dbBillingResponsibleBy.ToBillingResponsibleByItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBillingResponsibleBy(string item)
		{
			try
			{
				IBillingResponsibleByRepo billingResponsibleByRepo = new BillingResponsibleByRepo(db);
				Guid billingResponsibleByGUID = new Guid(item);
				BillingResponsibleBy billingResponsibleBy = billingResponsibleByRepo.Find(billingResponsibleByGUID);
				billingResponsibleByRepo.Remove(billingResponsibleBy);
				base.LogTransactionDelete<BillingResponsibleBy>(billingResponsibleBy);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
