using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IGuarantorTransService
    {
		GuarantorTransItemView GetGuarantorTransById(string id);
		GuarantorTransItemView CreateGuarantorTrans(GuarantorTransItemView guarantorTransView);
		GuarantorTransItemView UpdateGuarantorTrans(GuarantorTransItemView guarantorTransView);
		bool DeleteGuarantorTrans(string id);
		GuarantorTransItemView GetGuarantorTransInitialData(string refId, string refGUID, RefType refType);
		IEnumerable<GuarantorTrans> GetActiveGuarantorTrans(Guid refGUID, int refType);
        void CreateGuarantorTransCollection(IEnumerable<GuarantorTransItemView> guarantorTransItemViews);
        #region K2 function
        IEnumerable<GuarantorTrans> CopyGuarantorTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, bool IsAmend = false, string owner = null, Guid? ownerBusinessUnitGUID = null);
		#endregion
	}
    public class GuarantorTransService : SmartAppService, IGuarantorTransService
    {
        public GuarantorTransService(SmartAppDbContext context) : base(context) { }
        public GuarantorTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public GuarantorTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public GuarantorTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public GuarantorTransService() : base() { }

        public GuarantorTransItemView GetGuarantorTransById(string id)
        {
            try
            {
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                return guarantorTransRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView CreateGuarantorTrans(GuarantorTransItemView guarantorTransView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                guarantorTransView = accessLevelService.AssignOwnerBU(guarantorTransView);
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                GuarantorTrans guarantorTrans = guarantorTransView.ToGuarantorTrans();
                guarantorTransRepo.ValidateAdd(guarantorTrans);
                guarantorTrans = guarantorTransRepo.CreateGuarantorTrans(guarantorTrans);
                base.LogTransactionCreate<GuarantorTrans>(guarantorTrans);
                UnitOfWork.Commit();
                return guarantorTrans.ToGuarantorTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView UpdateGuarantorTrans(GuarantorTransItemView guarantorTransView)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                GuarantorTrans inputGuarantorTrans = guarantorTransView.ToGuarantorTrans();
                GuarantorTrans dbGuarantorTrans = guarantorTransRepo.Find(inputGuarantorTrans.GuarantorTransGUID);
                guarantorTransRepo.ValidateUpdate(inputGuarantorTrans);
                dbGuarantorTrans = guarantorTransRepo.UpdateGuarantorTrans(dbGuarantorTrans, inputGuarantorTrans);
                base.LogTransactionUpdate<GuarantorTrans>(GetOriginalValues<GuarantorTrans>(dbGuarantorTrans), dbGuarantorTrans);
                UnitOfWork.Commit();
                return dbGuarantorTrans.ToGuarantorTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteGuarantorTrans(string item)
        {
            try
            {
                IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                Guid guarantorTransGUID = new Guid(item);
                GuarantorTrans guarantorTrans = guarantorTransRepo.Find(guarantorTransGUID);
                guarantorTransRepo.Remove(guarantorTrans);
                base.LogTransactionDelete<GuarantorTrans>(guarantorTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GuarantorTransItemView GetGuarantorTransInitialData(string refId, string refGUID, RefType refType)
        {
            try
            {
                return new GuarantorTransItemView
                {
                    GuarantorTransGUID = new Guid().GuidNullToString(),
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefID = refId,
                    InActive = false,
                    Affiliate = false
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public IEnumerable<GuarantorTrans> GetActiveGuarantorTrans(Guid refGUID, int refType)
		{
			try
			{
				IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
				return guarantorTransRepo.GetGuarantorTransByReference(refGUID, refType).Where(w => w.InActive == false);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region K2 function
		public IEnumerable<GuarantorTrans> CopyGuarantorTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, bool IsAmend = false, string owner = null, Guid? ownerBusinessUnitGUID = null)
		{
			try
			{
				IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                List<GuarantorTrans> guarantorTrans = guarantorTransRepo.GetGuarantorTransByReference(fromRefGUID, fromRefType).ToList();
				if (guarantorTrans.Any())
				{
					if (fromInactive != null)
					{
                        guarantorTrans = guarantorTrans.Where(w => w.InActive == fromInactive.Value).ToList();
					}
					int ordering = 1;
					guarantorTrans.OrderBy(o => o.Ordering).ToList().ForEach(f =>
					{
						f.GuarantorTransGUID = Guid.NewGuid();
						f.RefGUID = toRefGUID;
						f.RefType = toRefType;
						f.Ordering = ordering;
						f.Owner = owner;
						f.OwnerBusinessUnitGUID = ownerBusinessUnitGUID;
						if (IsAmend)
						{
							f.RefGuarantorTransGUID = f.GuarantorTransGUID;
						}
						ordering++;
					});
				}
				return guarantorTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion

        #region Migration
        public void CreateGuarantorTransCollection(IEnumerable<GuarantorTransItemView> guarantorTransItemViews)
        {
            try
            {
                if (guarantorTransItemViews.Any())
                {
                    IGuarantorTransRepo guarantorTransRepo = new GuarantorTransRepo(db);
                    List<GuarantorTrans> guarantorTranss = guarantorTransItemViews.ToGuarantorTrans().ToList();
                    guarantorTranss.ForEach(f => f.GuarantorTransGUID = Guid.NewGuid());
                    guarantorTransRepo.ValidateAdd(guarantorTranss);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(guarantorTranss);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Migration
    }
}
