using EFCore.BulkExtensions;
using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IMainAgreementTableService
    {

        MainAgreementTableItemView GetMainAgreementTableById(string id);
        MainAgreementTableItemView GetCopyServiceFeeCondCARequestInitialData(string id);
        
        MainAgreementTableItemView CreateMainAgreementTable(MainAgreementTableItemView mainAgreementTableView);
        MainAgreementTableItemView UpdateMainAgreementTable(MainAgreementTableItemView mainAgreementTableView);
        MainAgreementTable UpdateMainAgreementTable(MainAgreementTable mainAgreementTable);
        bool DeleteMainAgreementTable(string id);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemMainTainAgreementStatus(SearchParameter search);
        bool IsManualInternalMainAgreement(int productType);
        bool IsManualMainAgreement(int productType);
        MainAgreementTableItemView GetMainAgreementTableInitialData(int productType);
        MainAgreementTableItemView GetBuyerAgreementTableByBuyerTable(MainAgreementTableItemView input);
        BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refGUID);
        MemoTransItemView GetMemoTransInitialData(string refGUID);
        JointVentureTransItemView GetJointVentureTransInitialData(string refGUID);
        AccessModeView GetAccessModeByStatusLessThanSigned(string MainAgreementId);
        AccessModeView GetAccessModeByStatusLessThanEqSigned(string MainAgreementId);
        AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refGUID);
        ConsortiumTransItemView GetConsortiumTransInitialDataByMainAgreementTable(string refGUID);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByMainAgreementTable(string refGUID);
        MemoTransItemView GetMemoTransInitialDataByGuarantorAgreement(string refGUID);
        IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTable(string id);
        IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTablebyLoanRequest(string id);
        GenMainAgmAddendumView GetDatabyCreditAppRequestID(string id);
        GenMainAgmAddendumView GetGenMainAgmAddendumById(string id);
        AddendumMainAgreementView CreateGenMainAgmView(GenMainAgmAddendumView genMainAgmAddendumView);
        List<int> GetVisibleManageMenu(string mainAgreementTableGuid);
        GenMainAgmNoticeOfCancelView GetGenMainAgmNoticeOfCancelById(string id);
        bool GetGenerateAddendumValidation(MainAgreementTableItemView vmodel);
        bool GetGenerateNoticeOfCancellationValidation(MainAgreementTableItemView vmodel);
        bool getGenerateLoanRequestAGMValidation(MainAgreementTableItemView vmodel);
        GenMainAgmNoticeOfCancelResultView CreateGenMainAgmNoticeOfCancel(GenMainAgmNoticeOfCancelView genMainAgmNoticeOfCancelView);
        GenMainAgmLoanRequestResultView CreateGenMainAgmLoanRequest(GenMainAgmLoanRequestView genMainAgmLoanRequestView);

        MainAgreementPFBookmarkView GetMainAgreementPFBookmarkValue(Guid refGuid);
        GenMainAgreementBookmarkSharedView GetMainAgreementBookmarkShared(Guid refGuid, Guid bookmarkDocumentTransGuid);
        GenMainAgreementExpenceDetailView GenMainAgreementExpenceDetailView(Guid refGuid);
        GenMainAgreementBookmarkSharedJVView GetMainAgreementBookmarkSharedJVView(Guid refGuid, Guid bookmarkDocumentTransGuid);
        GenMainAgreementConsortiumBookmarkView GetMainAgreementConsortiumBookmarkView(Guid refGuid, Guid bookmarkDocumentTransGuid);
        GenMainAgmLoanRequestView GetGenMainAgmLoanRequestById(string id);
        MainAgreementPFBookmarkView GetMainAgreementPFAndSharedBookmarkValue(Guid refGuid, Guid bookmarkDocumentTransGuid);
        AddendumMainAgreementBookmarkView GetAddendumMainAgreementBookmarkView(Guid refGuid, Guid bookmarkDocumentTransGuid);
        void CreateMainAgreementTableCollection(IEnumerable<MainAgreementTableItemView> mainAgreementTableViews);
        IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search);
        IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTableByGuarantorAgreement(SearchParameter search);

    }
    public class MainAgreementTableService : SmartAppService, IMainAgreementTableService
    {
        public MainAgreementTableService(SmartAppDbContext context) : base(context) { }
        public MainAgreementTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public MainAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public MainAgreementTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public MainAgreementTableService() : base() { }

        public MainAgreementTableItemView GetMainAgreementTableById(string id)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                return mainAgreementTableRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MainAgreementTableItemView GetCopyServiceFeeCondCARequestInitialData(string id)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                var result =  mainAgreementTableRepo.GetByIdvw(id.StringToGuid());
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        public GenMainAgmAddendumView GetGenMainAgmAddendumById(string id)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                return mainAgreementTableRepo.GetGenMainAgmAddendumByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenMainAgmNoticeOfCancelView GetGenMainAgmNoticeOfCancelById(string id)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                return mainAgreementTableRepo.GetGenMainAgmNoticeOfCancelByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public MainAgreementTableItemView CreateMainAgreementTable(MainAgreementTableItemView mainAgreementTableView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                mainAgreementTableView = accessLevelService.AssignOwnerBU(mainAgreementTableView);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableView.ToMainAgreementTable();

                GenMainAgreementNumberSeqCode(mainAgreementTable);
                mainAgreementTable = mainAgreementTableRepo.CreateMainAgreementTable(mainAgreementTable);
                GenDataOnCreate(mainAgreementTable);
                return mainAgreementTable.ToMainAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MainAgreementTableItemView UpdateMainAgreementTable(MainAgreementTableItemView mainAgreementTableView)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableView.ToMainAgreementTable();
                mainAgreementTable = UpdateMainAgreementTable(mainAgreementTable);
                UnitOfWork.Commit();
                return mainAgreementTable.ToMainAgreementTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MainAgreementTable UpdateMainAgreementTable(MainAgreementTable mainAgreementTable)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementTable dbMainAgreementTable = mainAgreementTableRepo.Find(mainAgreementTable.MainAgreementTableGUID);
                dbMainAgreementTable = mainAgreementTableRepo.UpdateMainAgreementTable(dbMainAgreementTable, mainAgreementTable);
                base.LogTransactionUpdate<MainAgreementTable>(GetOriginalValues<MainAgreementTable>(dbMainAgreementTable), dbMainAgreementTable);
                return dbMainAgreementTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteMainAgreementTable(string item)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                Guid mainAgreementTableGUID = new Guid(item);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.Find(mainAgreementTableGUID);
                mainAgreementTableRepo.Remove(mainAgreementTable);
                base.LogTransactionDelete<MainAgreementTable>(mainAgreementTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemMainTainAgreementStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.MainAgreement);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualInternalMainAgreement(int productType)
        {
            try
            {
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);

                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalMainAgreementNumberSeqGUID(new Guid(GetCurrentCompany()), productType);
                return numberSeqTableRepo.GetByNumberSeqTableGUID(numberSeqTableGUID).Manual;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsManualMainAgreement(int productType)
        {
            try
            {
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);

                Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetMainAgreementNumberSeqGUID(new Guid(GetCurrentCompany()), productType);
                return numberSeqTableRepo.GetByNumberSeqTableGUID(numberSeqTableGUID).Manual;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MainAgreementTableItemView GetMainAgreementTableInitialData(int productType)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                ICompanyParameterService companyParameterService = new CompanyParameterService(db);
                DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)MainAgreementStatus.Draft);
                CompanyParameter companyParameter = companyParameterService.GetCompanyParameterByCompany(GetCurrentCompany()).ToCompanyParameter();
                return new MainAgreementTableItemView
                {
                    MainAgreementTableGUID = new Guid().GuidNullToString(),
                    AgreementDocType = (int)AgreementDocType.New,
                    ProductType = productType,
                    Agreementextension = 0,
                    DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
                    DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(documentStatus.Description),
                    DocumentStatus_StatusId = documentStatus.StatusId,
                    MaxInterestPct = companyParameter.MaxInterestPct,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MainAgreementTableItemView GetBuyerAgreementTableByBuyerTable(MainAgreementTableItemView input)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                var cl = creditAppLineRepo.GetCreditAppLineByCompanyIdNoTracking(input.CompanyGUID.StringToGuid());

                var result = (from buyerAgreementTrans in db.Set<BuyerAgreementTrans>()
                              join creditAppLine in cl on buyerAgreementTrans.RefGUID equals creditAppLine.CreditAppLineGUID
                              where buyerAgreementTrans.BuyerAgreementTableGUID != null &&
                                    buyerAgreementTrans.BuyerAgreementTableGUID != Guid.Empty &&
                                    creditAppLine.BuyerTableGUID == input.BuyerTableGUID.StringToGuid() &&
                                    creditAppLine.CreditAppTableGUID == input.CreditAppTableGUID.StringToGuid()
                              select new
                              {
                                  LineNum = creditAppLine.LineNum,
                                  BuyerAgreementTableGUID = buyerAgreementTrans.BuyerAgreementTableGUID,
                              }).FirstOrDefault();

                if (result != null)
                {
                    IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                    BuyerAgreementTable buyerAgreementTable = buyerAgreementTableRepo.GetBuyerAgreementTableByIdNoTracking(result.BuyerAgreementTableGUID);
                    input.BuyerAgreementDescription = buyerAgreementTable.Description;
                    input.BuyerAgreementReferenceId = buyerAgreementTable.ReferenceAgreementID;
                    return input;
                }
                else
                {
                    return input;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region NumberSeq
        public void GenMainAgreementNumberSeqCode(MainAgreementTable mainAgreementTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);

                bool isManual = IsManualInternalMainAgreement(mainAgreementTable.ProductType);
                if (!isManual)
                {
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalMainAgreementNumberSeqGUID(mainAgreementTable.CompanyGUID, mainAgreementTable.ProductType);
                    mainAgreementTable.InternalMainAgreementId = numberSequenceService.GetNumber(mainAgreementTable.CompanyGUID, null, numberSeqTableGUID);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public string GenInternalMainAgreementNumberSeqCode(MainAgreementTable mainAgreementTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);

                bool isManual = IsManualInternalMainAgreement(mainAgreementTable.ProductType);
                if (!isManual)
                {
                    Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalMainAgreementNumberSeqGUID(mainAgreementTable.CompanyGUID, mainAgreementTable.ProductType);
                    return numberSequenceService.GetNumber(mainAgreementTable.CompanyGUID, null, numberSeqTableGUID);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion NumberSeq

        public void GenDataOnCreate(MainAgreementTable mainAgreementTable)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);

                AgreementTableInfo agreementTableInfo = mainAgreementTableRepo.GenerateAgreementInfo(mainAgreementTable);
                agreementTableInfoService.CreateAgreementTableInfo(agreementTableInfo);

                List<JointVentureTrans> jointVentureTrans_CreateList = new List<JointVentureTrans>();
                List<ConsortiumTrans> consortiumTables_CreateList = new List<ConsortiumTrans>();

                if (mainAgreementTable.CustomerTableGUID != null)
                {
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IEnumerable<JointVentureTrans> jointVentureTrans = jointVentureTransService.CopyJointVentureTrans(mainAgreementTable.CustomerTableGUID, mainAgreementTable.MainAgreementTableGUID, (int)RefType.Customer, (int)RefType.MainAgreement);
                    jointVentureTrans_CreateList.AddRange(jointVentureTrans.ToList());
                }

                if (mainAgreementTable.ConsortiumTableGUID != null)
                {
                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    IEnumerable<ConsortiumTrans> consortiumTables = consortiumTransService.CopyConsortiumTrans(mainAgreementTable.ConsortiumTableGUID.Value, mainAgreementTable.MainAgreementTableGUID, (int)RefType.MainAgreement);
                    consortiumTables_CreateList.AddRange(consortiumTables.ToList());
                }

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkInsert(jointVentureTrans_CreateList, true);
                    this.BulkInsert(consortiumTables_CreateList, true);
                    base.LogTransactionCreate<MainAgreementTable>(mainAgreementTable);
                    UnitOfWork.Commit(transaction);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

        public List<int> GetVisibleManageMenu(string mainAgreementTableGuid)
        {
            try
            {
                List<int> result = new List<int>();
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(mainAgreementTableGuid.StringToGuid());
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(mainAgreementTable.DocumentStatusGUID);
                DocumentStatus completedBookmarkTrans = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)BookMarkDocumentStatus.Completed).ToString());
                IEnumerable<BookmarkDocumentTrans> bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByRefType((int)RefType.MainAgreement, mainAgreementTableGuid.StringToGuid());
                if ((int)MainAgreementStatus.Draft == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Post);
                }
                if ((int)MainAgreementStatus.Posted == int.Parse(documentStatus.StatusId))
                {
                    result.Add((int)ManageAgreementAction.Send);
                }
                if ((int)MainAgreementStatus.Sent == int.Parse(documentStatus.StatusId) && bookmarkDocumentTrans.All(a => a.DocumentStatusGUID == completedBookmarkTrans.DocumentStatusGUID))
                {
                    result.Add((int)ManageAgreementAction.Sign);
                }
                if ((int)MainAgreementStatus.Signed == int.Parse(documentStatus.StatusId) && (mainAgreementTable.AgreementDocType == (int)AgreementDocType.New || mainAgreementTable.AgreementDocType == (int)AgreementDocType.Addendum))
                {
                    result.Add((int)ManageAgreementAction.Close);
                }
                if (int.Parse(documentStatus.StatusId) < (int)MainAgreementStatus.Signed)
                {
                    result.Add((int)ManageAgreementAction.Cancel);
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region RelatedInfo
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string refId = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalMainAgreementId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.MainAgreement);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialDataByGuarantorAgreement(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                IGuarantorAgreementTableRepo guarantorAgreementTableRepo = new GuarantorAgreementTableRepo(db);
                string refId = guarantorAgreementTableRepo.GetGuarantorAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalGuarantorAgreementId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.GuarantorAgreement);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public JointVentureTransItemView GetJointVentureTransInitialData(string refGUID)
        {
            try
            {
                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string refId = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalMainAgreementId;
                return jointVentureTransService.GetJointVentureTransInitialData(refId, refGUID, Models.Enum.RefType.MainAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BookmarkDocumentTransItemView GetBookmarkDocumentTransInitialData(string refGUID)
        {
            try
            {
                IBookmarkDocumentTransService bookmarkDocumentTransService = new BookmarkDocumentTransService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string refId = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalMainAgreementId;
                return bookmarkDocumentTransService.GetBookmarkDocumentTransInitialData(refId, refGUID, Models.Enum.RefType.MainAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AgreementTableInfoItemView GetAgreementTableInfoInitialData(string refGUID)
        {
            try
            {
                IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string refId = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalMainAgreementId;
                return agreementTableInfoService.GetAgreementTableInfoInitialData(refId, refGUID, Models.Enum.RefType.MainAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ConsortiumTransItemView GetConsortiumTransInitialDataByMainAgreementTable(string refGUID)
        {
            try
            {
                IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string refId = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalMainAgreementId;
                return consortiumTransService.GetConsortiumTransInitialDataByMainAgreementTable(refId, refGUID, Models.Enum.RefType.MainAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByMainAgreementTable(string refGUID)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string refId = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).InternalMainAgreementId;
                return serviceFeeTransService.GetServiceFeeTransInitialDataByMainAgreementTable(refId, refGUID, Models.Enum.RefType.MainAgreement);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByStatusLessThanSigned(string MainAgreementId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string _MainAgreementStatus = GetStatusIdByMainAgreement(MainAgreementId);
                bool isLessThanSigned = (Convert.ToInt32(_MainAgreementStatus) < (int)MainAgreementStatus.Signed);
                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = isLessThanSigned;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetAccessModeByStatusLessThanEqSigned(string MainAgreementId)
        {
            try
            {
                AccessModeView accessModeView = new AccessModeView();
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                string _MainAgreementStatus = GetStatusIdByMainAgreement(MainAgreementId);
                bool isLessThanSigned = (Convert.ToInt32(_MainAgreementStatus) <= (int)MainAgreementStatus.Signed);
                accessModeView.CanCreate = isLessThanSigned;
                accessModeView.CanView = isLessThanSigned;
                accessModeView.CanDelete = isLessThanSigned;
                return accessModeView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public string GetStatusIdByMainAgreement(string mainAgreementId)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(mainAgreementId.StringToGuid());
                return documentStatusRepo.GetDocumentStatusByIdNoTracking(mainAgreementTable.DocumentStatusGUID).StatusId;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion RelatedInfo
        #region Function
        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTable(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetDropDownItemForFunctionAddendum(id);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<CreditAppRequestTableItemView>> GetDropDownItemCreditAppRequestTablebyLoanRequest(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetDropDownItemForFunctionLoanRequest(id);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenMainAgmAddendumView GetDatabyCreditAppRequestID(string id)
        {
            try
            {
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                return creditAppRequestTableRepo.GetDatabyCreditAppRequestID(id);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AddendumMainAgreementView CreateGenMainAgmView(GenMainAgmAddendumView genMainAgmAddendumView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                genMainAgmAddendumView = accessLevelService.AssignOwnerBU(genMainAgmAddendumView);
                AddendumMainAgreementView addendumMainAgreementView = new AddendumMainAgreementView();
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                List<DocumentStatus> agreementStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.MainAgreement).ToString()).ToList();
                MainAgreementTable mainAGMTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(genMainAgmAddendumView.MainAgreementTableGUID.StringToGuid());
                DocumentStatus documentStatus_mainAGMTable = agreementStatus.Where(w => w.DocumentStatusGUID == mainAGMTable.DocumentStatusGUID).FirstOrDefault();
                List<MainAgreementTable> addendumAgreementTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(mainAGMTable.MainAgreementTableGUID, AgreementDocType.Addendum);

                var Condition3 = (from mainAgreementTable in addendumAgreementTables
                                  join documentStatus in agreementStatus
                                  on mainAgreementTable.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                                  where Convert.ToInt32(documentStatus.StatusId) < (int)(MainAgreementStatus.Signed)
                                  select mainAgreementTable
                              ).FirstOrDefault();

                if (int.Parse(documentStatus_mainAGMTable.StatusId) != (int)MainAgreementStatus.Signed)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.MAIN_AGREEMENT" });
                }
                if (mainAGMTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                if (Condition3 != null)
                {
                    ex.AddData("ERROR.90030", new string[] { SmartAppUtil.GetDropDownLabel(mainAGMTable.InternalMainAgreementId, mainAGMTable.Description) });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    // Main agreement
                    DocumentStatus signStatus = agreementStatus.Where(w => w.StatusId == ((int)MainAgreementStatus.Signed).ToString()).FirstOrDefault();
                    ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                    IEnumerable<CreditAppRequestLine> creditAppRequestLines = creditAppRequestLineRepo.GetCreditAppRequestLineByCreditAppRequestTable(genMainAgmAddendumView.CreditAppRequestTableGUID.StringToGuid());

                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                    string internalMainAGM_tmp = null;
                    bool isManual = IsManualInternalMainAgreement(mainAGMTable.ProductType);
                    if (!isManual)
                    {
                        Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalMainAgreementNumberSeqGUID(mainAGMTable.CompanyGUID, mainAGMTable.ProductType);
                        internalMainAGM_tmp = numberSequenceService.GetNumber(mainAGMTable.CompanyGUID, null, numberSeqTableGUID);
                    }
                    else
                    {
                        internalMainAGM_tmp = genMainAgmAddendumView.AddendumInternalMainAgreementId;
                    }

                    ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(mainAGMTable.CreditAppTableGUID);
                    IMainAgreementTableService mainAgreementservice = new MainAgreementTableService(db);
                    MainAgreementTable tmp_MainAGM = new MainAgreementTable()
                    {
                        MainAgreementTableGUID = Guid.NewGuid(),
                        InternalMainAgreementId = internalMainAGM_tmp,
                        MainAgreementId = String.Empty,
                        CompanyGUID = mainAGMTable.CompanyGUID,
                        AgreementDocType = (int)AgreementDocType.Addendum,
                        Description = genMainAgmAddendumView.Description,
                        CustomerTableGUID = mainAGMTable.CustomerTableGUID,
                        CustomerName = mainAGMTable.CustomerName,
                        CustomerAltName = mainAGMTable.CustomerAltName,
                        AgreementDate = genMainAgmAddendumView.AgreementDate.StringToDate(),
                        DocumentStatusGUID = agreementStatus.Where(w => w.StatusId == ((int)MainAgreementStatus.Draft).ToString()).Select(s => s.DocumentStatusGUID).FirstOrDefault(),
                        StartDate = mainAGMTable.StartDate,
                        ExpiryDate = mainAGMTable.ExpiryDate,
                        AgreementYear = mainAGMTable.AgreementYear,
                        SigningDate = null,
                        MaxInterestPct = mainAGMTable.MaxInterestPct,
                        DocumentReasonGUID = null,
                        Remark = String.Empty,
                        ProductType = mainAGMTable.ProductType,
                        CreditAppTableGUID = mainAGMTable.CreditAppTableGUID,
                        CreditAppRequestTableGUID = genMainAgmAddendumView.CreditAppRequestTableGUID.StringToGuid(),
                        CreditLimitTypeGUID = mainAGMTable.CreditLimitTypeGUID,
                        ApprovedCreditLimit = creditAppTable.ApprovedCreditLimit,
                        TotalInterestPct = creditAppTable.TotalInterestPct,
                        ConsortiumTableGUID = mainAGMTable.ConsortiumTableGUID,
                        BuyerTableGUID = mainAGMTable.BuyerTableGUID,
                        BuyerName = mainAGMTable.BuyerName,
                        BuyerAgreementDescription = mainAGMTable.BuyerAgreementDescription,
                        BuyerAgreementReferenceId = mainAGMTable.BuyerAgreementReferenceId,
                        WithdrawalTableGUID = mainAGMTable.WithdrawalTableGUID,
                        RefMainAgreementTableGUID = mainAGMTable.MainAgreementTableGUID,
                        Agreementextension = addendumAgreementTables.Where(w => w.DocumentStatusGUID == signStatus.DocumentStatusGUID)
                                                                    .OrderByDescending(o => o.Agreementextension).Select(s => s.Agreementextension).FirstOrDefault() + 1,
                        ApprovedCreditLimitLine = creditAppRequestLines.Sum(s => s.ApprovedCreditLimitLineRequest)
                    };

                    // SharedMethod30:CopyAgreementInfo
                    IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                    CopyAgreementInfoView copyAgreementInfoView = agreementTableInfoService.CopyAgreementInfo(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    // Agreement joint venture transactions
                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IEnumerable<JointVentureTrans> copy_Join = jointVentureTransService.CopyJointVentureTrans(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    // Consortium transactions
                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    IEnumerable<ConsortiumTrans> Copy_Consortium = consortiumTransService.CopyConsortiumTrans(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(tmp_MainAGM.FirstToList());
                        if (copyAgreementInfoView != null)
                        {
                            this.BulkInsert(new List<AgreementTableInfo> { copyAgreementInfoView.AgreementTableInfo });
                            if (copyAgreementInfoView.AgreementTableInfoText != null)
                            {
                                this.BulkInsert(new List<AgreementTableInfoText>() { copyAgreementInfoView.AgreementTableInfoText });
                            }
                        }
                        if (Copy_Consortium.Count() > 0)
                        {
                            this.BulkInsert(Copy_Consortium.ToList());
                        }
                        this.BulkInsert(copy_Join.ToList());
                        UnitOfWork.Commit(transaction);
                    }

                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.90019", new string[] { (AgreementDocType.Addendum).GetAttrCode(), "LABEL.MAIN_AGREEMENT", mainAGMTable.InternalMainAgreementId });
                    addendumMainAgreementView.Notification = success;

                    return addendumMainAgreementView;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetGenerateAddendumValidation(MainAgreementTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(vmodel.MainAgreementTableGUID.StringToGuid());
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Signed).ToString());
                DocumentStatus cancelStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Cancelled).ToString());
                DocumentStatus mainAgreementTableStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(mainAgreementTable.DocumentStatusGUID);

                if (mainAgreementTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.MAIN_AGREEMENT" });
                }
                if (mainAgreementTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }

                List<DocumentStatus> agreementStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.MainAgreement).ToString()).ToList();

                List<MainAgreementTable> addendumAgreementTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(vmodel.MainAgreementTableGUID.StringToGuid(), AgreementDocType.Addendum);

                var Condition3 = (from mainAgreement in addendumAgreementTables
                                  join documentStatus in agreementStatus
                                  on mainAgreement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                                  where Convert.ToInt32(documentStatus.StatusId) < (int)(MainAgreementStatus.Signed)
                                  select mainAgreement
                              ).FirstOrDefault();
                if (Condition3 != null)
                {
                    ex.AddData("ERROR.90030", new string[] { SmartAppUtil.GetDropDownLabel(mainAgreementTable.InternalMainAgreementId, mainAgreementTable.Description) });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetGenerateNoticeOfCancellationValidation(MainAgreementTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(vmodel.MainAgreementTableGUID.StringToGuid());
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Signed).ToString());
                DocumentStatus cancelStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Cancelled).ToString());
                DocumentStatus mainAgreementTableStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(mainAgreementTable.DocumentStatusGUID);

                List<DocumentStatus> agreementStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.MainAgreement).ToString()).ToList();
                List<MainAgreementTable> notictOfCancelledTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(vmodel.MainAgreementTableGUID.StringToGuid(), AgreementDocType.NoticeOfCancellation);
                List<MainAgreementTable> addendumTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(vmodel.MainAgreementTableGUID.StringToGuid(), AgreementDocType.Addendum);

                var Condition3 = (from mainAgreement in notictOfCancelledTables
                                  join documentStatus in agreementStatus
                                  on mainAgreement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                                  where Convert.ToInt32(documentStatus.StatusId) != (int)(MainAgreementStatus.Cancelled)
                                  && mainAgreement.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation
                                  && mainAgreement.RefMainAgreementTableGUID.GuidNullOrEmptyToString() == vmodel.MainAgreementTableGUID
                                  select mainAgreement
              ).FirstOrDefault();
                var Condition4 = (from mainAgreement in addendumTables
                                  join documentStatus in agreementStatus
                                  on mainAgreement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                                  where Convert.ToInt32(documentStatus.StatusId) < (int)(MainAgreementStatus.Signed)
                                  && mainAgreement.RefMainAgreementTableGUID.GuidNullOrEmptyToString() == vmodel.MainAgreementTableGUID
                                  && mainAgreement.AgreementDocType == (int)AgreementDocType.Addendum
                                  select mainAgreement

               ).FirstOrDefault();
                if (mainAgreementTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.MAIN_AGREEMENT" });
                }
                if (mainAgreementTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                if (Condition3 != null)
                {
                    ex.AddData("ERROR.90004", new string[] { "LABEL.MAIN_AGREEMENT", SmartAppUtil.GetDropDownLabel(mainAgreementTable.InternalMainAgreementId, mainAgreementTable.Description) });
                }
                if (Condition4 != null)
                {
                    ex.AddData("ERROR.90030", new string[] { SmartAppUtil.GetDropDownLabel(mainAgreementTable.InternalMainAgreementId, mainAgreementTable.Description) });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenMainAgmNoticeOfCancelResultView CreateGenMainAgmNoticeOfCancel(GenMainAgmNoticeOfCancelView genMainAgmNoticeOfCancelView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                genMainAgmNoticeOfCancelView = accessLevelService.AssignOwnerBU(genMainAgmNoticeOfCancelView);
                GenMainAgmNoticeOfCancelResultView genMainAgmNoticeOfCancelResultView = new GenMainAgmNoticeOfCancelResultView();
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                var mainAGMTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(genMainAgmNoticeOfCancelView.MainAgreementTableGUID.StringToGuid());
                var documentStatus_mainAGMTable = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(mainAGMTable.DocumentStatusGUID);
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Signed).ToString());

                List<DocumentStatus> agreementStatus = documentStatusRepo.GetDocumentStatusByProcessId(((int)DocumentProcessStatus.MainAgreement).ToString()).ToList();
                List<MainAgreementTable> notictOfCancelledTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(mainAGMTable.MainAgreementTableGUID, AgreementDocType.NoticeOfCancellation);
                List<MainAgreementTable> addendumTables = mainAgreementTableRepo.GetListByRefMainAgreementTableAndAgreementDocType(mainAGMTable.MainAgreementTableGUID, AgreementDocType.Addendum);

                var Condition3 = (from mainAgreement in notictOfCancelledTables
                                  join documentStatus in agreementStatus
                                  on mainAgreement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                                  where Convert.ToInt32(documentStatus.StatusId) != (int)(MainAgreementStatus.Cancelled)
                                  && mainAgreement.AgreementDocType == (int)AgreementDocType.NoticeOfCancellation
                                  && mainAgreement.RefMainAgreementTableGUID.GuidNullOrEmptyToString() == mainAGMTable.MainAgreementTableGUID.ToString()
                                  select mainAgreement
              ).FirstOrDefault();
                var Condition4 = (from mainAgreement in addendumTables
                                  join documentStatus in agreementStatus
                                  on mainAgreement.DocumentStatusGUID equals documentStatus.DocumentStatusGUID
                                  where Convert.ToInt32(documentStatus.StatusId) < (int)(MainAgreementStatus.Signed)
                                  && mainAgreement.RefMainAgreementTableGUID.GuidNullOrEmptyToString() == mainAGMTable.MainAgreementTableGUID.ToString()
                                  && mainAgreement.AgreementDocType == (int)AgreementDocType.Addendum
                                  select mainAgreement

               ).FirstOrDefault();
                if (mainAGMTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.MAIN_AGREEMENT" });
                }
                if (mainAGMTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                if (Condition3 != null)
                {
                    ex.AddData("ERROR.90004", new string[] { "LABEL.MAIN_AGREEMENT", SmartAppUtil.GetDropDownLabel(mainAGMTable.InternalMainAgreementId, mainAGMTable.Description) });
                }
                if (Condition4 != null)
                {
                    ex.AddData("ERROR.90030", new string[] { SmartAppUtil.GetDropDownLabel(mainAGMTable.InternalMainAgreementId, mainAGMTable.Description) });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }


               
                else
                {
                    var documentStatus_tmp = (from documentStatus in db.Set<DocumentStatus>()
                                              where Convert.ToInt32(documentStatus.StatusId) == (int)MainAgreementStatus.Draft
                                              select documentStatus
                                         ).FirstOrDefault();
                    var approvedCreditLimitLine = (from creditAppLine in db.Set<CreditAppLine>()
                                                   where creditAppLine.CreditAppTableGUID == mainAGMTable.CreditAppTableGUID
                                                   select creditAppLine).Sum(m => m.ApprovedCreditLimitLine); ;

                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                    string internalMainAGM_tmp = null;
                    bool isManual = IsManualInternalMainAgreement(mainAGMTable.ProductType);
                    if (!isManual)
                    {
                        Guid numberSeqTableGUID = numberSeqSetupByProductTypeRepo.GetInternalMainAgreementNumberSeqGUID(mainAGMTable.CompanyGUID, mainAGMTable.ProductType);
                        internalMainAGM_tmp = numberSequenceService.GetNumber(mainAGMTable.CompanyGUID, null, numberSeqTableGUID);
                    }
                    else
                    {
                        internalMainAGM_tmp = genMainAgmNoticeOfCancelView.NoticeOfCancellationInternalMainAgreementId;
                    }

                    IMainAgreementTableService mainAgreementservice = new MainAgreementTableService(db);
                    MainAgreementTable tmp_MainAGM = new MainAgreementTable()
                    {
                        MainAgreementTableGUID = Guid.NewGuid(),
                        InternalMainAgreementId = internalMainAGM_tmp,
                        MainAgreementId = null,
                        CompanyGUID = mainAGMTable.CompanyGUID,
                        AgreementDocType = (int)AgreementDocType.NoticeOfCancellation,
                        Description = genMainAgmNoticeOfCancelView.Description,
                        CustomerTableGUID = mainAGMTable.CustomerTableGUID,
                        CustomerName = mainAGMTable.CustomerName,
                        CustomerAltName = mainAGMTable.CustomerAltName,
                        AgreementDate = genMainAgmNoticeOfCancelView.CancelDate.StringToDate(),
                        DocumentStatusGUID = documentStatus_tmp.DocumentStatusGUID,
                        
                        StartDate = null,
                        ExpiryDate = null,
                        AgreementYear = 0,
                        SigningDate = null,
                        MaxInterestPct = mainAGMTable.MaxInterestPct,
                        DocumentReasonGUID = genMainAgmNoticeOfCancelView.DocumentReasonGUID.StringToGuid(),
                        Remark = genMainAgmNoticeOfCancelView.ReasonRemark,
                        ProductType = mainAGMTable.ProductType,
                        CreditAppTableGUID = mainAGMTable.CreditAppTableGUID,
                        CreditAppRequestTableGUID = mainAGMTable.CreditAppRequestTableGUID,
                        CreditLimitTypeGUID = mainAGMTable.CreditLimitTypeGUID,
                        ApprovedCreditLimit = mainAGMTable.ApprovedCreditLimit,
                        ApprovedCreditLimitLine = approvedCreditLimitLine,
                        TotalInterestPct = mainAGMTable.TotalInterestPct,
                        ConsortiumTableGUID = mainAGMTable.ConsortiumTableGUID,
                        BuyerTableGUID = mainAGMTable.BuyerTableGUID,
                        BuyerName = mainAGMTable.BuyerName,
                        BuyerAgreementDescription = mainAGMTable.BuyerAgreementDescription,
                        BuyerAgreementReferenceId = mainAGMTable.BuyerAgreementReferenceId,
                        WithdrawalTableGUID = mainAGMTable.WithdrawalTableGUID,
                        RefMainAgreementTableGUID = mainAGMTable.MainAgreementTableGUID,
                        Agreementextension = 0,
                    };

                    // SharedMethod30:CopyAgreementInfo
                    IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                    CopyAgreementInfoView copyAgreementInfoView = agreementTableInfoService.CopyAgreementInfo(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IEnumerable<JointVentureTrans> copy_Join = jointVentureTransService.CopyJointVentureTrans(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    IEnumerable<ConsortiumTrans> Copy_Consortium = consortiumTransService.CopyConsortiumTrans(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(tmp_MainAGM.FirstToList());
                        if (copyAgreementInfoView != null)
                        {
                            this.BulkInsert(new List<AgreementTableInfo> { copyAgreementInfoView.AgreementTableInfo });
                            if (copyAgreementInfoView.AgreementTableInfoText != null)
                            {
                                this.BulkInsert(new List<AgreementTableInfoText>() { copyAgreementInfoView.AgreementTableInfoText });
                            }
                        }
                        if (Copy_Consortium.Count() > 0)
                        {
                            this.BulkInsert(Copy_Consortium.ToList());
                        }
                        this.BulkInsert(copy_Join.ToList());
                        UnitOfWork.Commit(transaction);
                    }

                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.90019", new string[] { (AgreementDocType.NoticeOfCancellation).GetAttrCode(), "LABEL.MAIN_AGREEMENT", mainAGMTable.InternalMainAgreementId });
                    genMainAgmNoticeOfCancelResultView.Notification = success;
                }

                return genMainAgmNoticeOfCancelResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool getGenerateLoanRequestAGMValidation(MainAgreementTableItemView vmodel)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(vmodel.MainAgreementTableGUID.StringToGuid());
                DocumentStatus signStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Signed).ToString());
                DocumentStatus cancelStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)MainAgreementStatus.Cancelled).ToString());
                DocumentStatus mainAgreementTableStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(mainAgreementTable.DocumentStatusGUID);
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking((Guid)mainAgreementTable.CreditLimitTypeGUID);

                if (mainAgreementTable.DocumentStatusGUID != signStatus.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.MAIN_AGREEMENT" });
                }
                if (mainAgreementTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                if ((mainAgreementTable.ProductType != (int)ProductType.ProjectFinance) || (creditLimitType.BuyerMatchingAndLoanRequest != true))
                {
                    ex.AddData("ERROR.90167", new string[]
                    {
                        "LABEL.MAIN_AGREEMENT" ,
                        "LABEL.BUYER_MATCHING__LOAN_REQUEST",
                        "LABEL.CREDIT_LIMIT_TYPE" ,
                        SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description)
                    });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenMainAgmLoanRequestView GetGenMainAgmLoanRequestById(string id)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                return mainAgreementTableRepo.GetGenMainAgmLoanRequestByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenMainAgmLoanRequestResultView CreateGenMainAgmLoanRequest(GenMainAgmLoanRequestView genMainAgmLoanRequestView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                genMainAgmLoanRequestView = accessLevelService.AssignOwnerBU(genMainAgmLoanRequestView);
                GenMainAgmLoanRequestResultView genMainAgmLoanRequestResultView = new GenMainAgmLoanRequestResultView();
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                var mainAGMTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(genMainAgmLoanRequestView.MainAgreementTableGUID.StringToGuid());
                var documentStatus_mainAGMTable = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(mainAGMTable.DocumentStatusGUID);
                var creditAppTable_tmp = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(genMainAgmLoanRequestView.CreditAppTableGUID.StringToGuid());
                var creditAppRequestLine_tmp = creditAppRequestLineRepo.GetCreditAppRequestLineByCreditAppRequestTable(genMainAgmLoanRequestView.CreditAppRequestTableGUID.StringToGuid());
                decimal approvedCreditLimitLineRequest_sum = creditAppRequestLine_tmp.Select(s => s.ApprovedCreditLimitLineRequest).Sum();
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(mainAGMTable.CreditLimitTypeGUID);

                if (int.Parse(documentStatus_mainAGMTable.StatusId) != (int)MainAgreementStatus.Signed)
                {
                    ex.AddData("ERROR.90015", new string[] { "LABEL.MAIN_AGREEMENT" });
                }
                if (mainAGMTable.AgreementDocType != (int)AgreementDocType.New)
                {
                    ex.AddData("ERROR.90029");
                }
                if(mainAGMTable.ProductType != (int)ProductType.ProjectFinance || creditLimitType.BuyerMatchingAndLoanRequest != true)
                {
                    ex.AddData("ERROR.90167", new string[] 
                    { 
                        "LABEL.MAIN_AGREEMENT" , 
                        "LABEL.BUYER_MATCHING__LOAN_REQUEST", 
                        "LABEL.CREDIT_LIMIT_TYPE" ,
                        SmartAppUtil.GetDropDownLabel(creditLimitType.CreditLimitTypeId, creditLimitType.Description)
                    });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    var documentStatus_tmp = (from documentStatus in db.Set<DocumentStatus>()
                                              where Convert.ToInt32(documentStatus.StatusId) == (int)MainAgreementStatus.Draft
                                              select documentStatus
                                         ).FirstOrDefault();
                    var InternalMainAGM_tmp = GenInternalMainAgreementNumberSeqCode(mainAGMTable) ?? genMainAgmLoanRequestView.LoanrequestInternalMainAgreementId;
                    ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                    CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(mainAGMTable.CompanyGUID);
                    IMainAgreementTableService mainAgreementservice = new MainAgreementTableService(db);
                    MainAgreementTable tmp_MainAGM = new MainAgreementTable()
                    {
                        MainAgreementTableGUID = Guid.NewGuid(),
                        InternalMainAgreementId = InternalMainAGM_tmp,
                        MainAgreementId = null,
                        CompanyGUID = mainAGMTable.CompanyGUID,
                        AgreementDocType = (int)AgreementDocType.New,
                        Description = genMainAgmLoanRequestView.Description,
                        CustomerTableGUID = mainAGMTable.CustomerTableGUID,
                        CustomerName = mainAGMTable.CustomerName,
                        CustomerAltName = mainAGMTable.CustomerAltName,
                        AgreementDate = genMainAgmLoanRequestView.AgreementDate.StringToDate(),
                        DocumentStatusGUID = documentStatus_tmp.DocumentStatusGUID,
                        StartDate = creditAppTable_tmp.StartDate,
                        ExpiryDate = creditAppTable_tmp.ExpiryDate,
                        AgreementYear = 0,
                        SigningDate = null,
                        MaxInterestPct = companyParameter.MaxInterestPct,
                        DocumentReasonGUID = null,
                        Remark = String.Empty,
                        ProductType = mainAGMTable.ProductType,
                        CreditAppTableGUID = mainAGMTable.CreditAppTableGUID,
                        CreditAppRequestTableGUID = genMainAgmLoanRequestView.CreditAppRequestTableGUID.StringToGuid(),
                        CreditLimitTypeGUID = mainAGMTable.CreditLimitTypeGUID,
                        ApprovedCreditLimit = creditAppTable_tmp.ApprovedCreditLimit,
                        ApprovedCreditLimitLine = approvedCreditLimitLineRequest_sum,
                        TotalInterestPct = creditAppTable_tmp.TotalInterestPct,
                        ConsortiumTableGUID = mainAGMTable.ConsortiumTableGUID,
                        BuyerTableGUID = mainAGMTable.BuyerTableGUID,
                        BuyerName = mainAGMTable.BuyerName,
                        BuyerAgreementDescription = mainAGMTable.BuyerAgreementDescription,
                        BuyerAgreementReferenceId = mainAGMTable.BuyerAgreementReferenceId,
                        WithdrawalTableGUID = null,
                        RefMainAgreementTableGUID = null,
                        Agreementextension = 0,
                    };

                    // SharedMethod30:CopyAgreementInfo
                    IAgreementTableInfoService agreementTableInfoService = new AgreementTableInfoService(db);
                    CopyAgreementInfoView copyAgreementInfoView = agreementTableInfoService.CopyAgreementInfo(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                    IEnumerable<JointVentureTrans> copy_Join = jointVentureTransService.CopyJointVentureTrans(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                    IEnumerable<ConsortiumTrans> Copy_Consortium = consortiumTransService.CopyConsortiumTrans(mainAGMTable.MainAgreementTableGUID, tmp_MainAGM.MainAgreementTableGUID, (int)RefType.MainAgreement, (int)RefType.MainAgreement);

                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        this.BulkInsert(tmp_MainAGM.FirstToList());
                        if (copyAgreementInfoView != null)
                        {
                            this.BulkInsert(new List<AgreementTableInfo> { copyAgreementInfoView.AgreementTableInfo });
                            if (copyAgreementInfoView.AgreementTableInfoText != null)
                            {
                                this.BulkInsert(new List<AgreementTableInfoText>() { copyAgreementInfoView.AgreementTableInfoText });
                            }
                        }
                        if (Copy_Consortium.Count() > 0)
                        {
                            this.BulkInsert(Copy_Consortium.ToList());
                        }
                        this.BulkInsert(copy_Join.ToList());
                        UnitOfWork.Commit(transaction);
                    }

                    NotificationResponse success = new NotificationResponse();
                    success.AddData("SUCCESS.90026", new string[] { "LABEL.LOAN_REQUEST_INTERNAL_MAIN_AGREEMENT_ID", SmartAppUtil.GetDropDownLabel(tmp_MainAGM.InternalMainAgreementId, tmp_MainAGM.Description) });
                    genMainAgmLoanRequestResultView.Notification = success;
                }

                return genMainAgmLoanRequestResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region BookmarkSharedView
        public GenMainAgreementBookmarkSharedView GetMainAgreementBookmarkShared(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                var mainAgreementShareView = mainAgreementTableRepo.MainAgreementBookmarkSharedView(refGuid);

                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                var bookmarkItem = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByIdNoTracking(bookmarkDocumentTransGuid);

                ISharedService sharedService = new SharedService(db);
                if (mainAgreementShareView != null)
                {
                    mainAgreementShareView.ContAmountSecond1 = sharedService.NumberToWordsTH(mainAgreementShareView.ContAmountFirst1);
                    mainAgreementShareView.AgreementDate1 = sharedService.DateToWord(mainAgreementShareView.AgreementDate, "TH", "DAY MONTH YEAR");
                    mainAgreementShareView.AgreementDate2 = mainAgreementShareView.AgreementDate1;
                    mainAgreementShareView.AgreementDate3 = mainAgreementShareView.AgreementDate1;
                    mainAgreementShareView.AgreementDate4 = mainAgreementShareView.AgreementDate1;
                    mainAgreementShareView.AgreementDate5 = mainAgreementShareView.AgreementDate1;
                    if (bookmarkItem != null) mainAgreementShareView.ManualNo1 = bookmarkItem.ReferenceExternalId;
                }

                return mainAgreementShareView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public GenMainAgreementBookmarkSharedJVView GetMainAgreementBookmarkSharedJVView(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                GenMainAgreementBookmarkSharedView mainBookmarkSharedView = GetMainAgreementBookmarkShared(refGuid, bookmarkDocumentTransGuid);

                IJointVentureTransService jointVentureTransService = new JointVentureTransService(db);
                MainAgreementJVBookmarkView mainAgreementJvBookmarkView = jointVentureTransService.GetMainAgreementJVBookmarkValue(refGuid);

                GenMainAgreementBookmarkSharedJVView genMainAgreementBookmarkSharedJvView =
                    new GenMainAgreementBookmarkSharedJVView
                    {                        
                        ContAmountFirst1 = mainBookmarkSharedView.ContAmountFirst1,
                        ContAmountSecond1 = mainBookmarkSharedView?.ContAmountSecond1,
                        AgreementDate1 = mainBookmarkSharedView?.AgreementDate1,
                        AgreementDate2 = mainBookmarkSharedView?.AgreementDate2,
                        AgreementDate3 = mainBookmarkSharedView?.AgreementDate3,
                        AgreementDate4 = mainBookmarkSharedView?.AgreementDate4,
                        AgreementDate5 = mainBookmarkSharedView?.AgreementDate5,
                        AgreementNo1 = mainBookmarkSharedView?.AgreementNo1,
                        AgreementNo2 = mainBookmarkSharedView?.AgreementNo2,
                        AgreementNo3 = mainBookmarkSharedView?.AgreementNo3,
                        AgreementNo4 = mainBookmarkSharedView?.AgreementNo4,
                        AgreementNo5 = mainBookmarkSharedView?.AgreementNo5,
                        InterestRate1 = mainBookmarkSharedView?.InterestRate1,
                        AliasNameCust1 = mainBookmarkSharedView?.AliasNameCust1,
                        AliasNameCust2 = mainBookmarkSharedView?.AliasNameCust2,
                        AliasNameCust3 = mainBookmarkSharedView?.AliasNameCust3,
                        AliasNameCust4 = mainBookmarkSharedView?.AliasNameCust4,
                        AliasNameCust5 = mainBookmarkSharedView?.AliasNameCust5,
                        AliasNameCust6 = mainBookmarkSharedView?.AliasNameCust6,
                        AliasNameCust7 = mainBookmarkSharedView?.AliasNameCust7,
                        AliasNameCust8 = mainBookmarkSharedView?.AliasNameCust8,
                        CustName1 = mainBookmarkSharedView?.CustName1,
                        CustName2 = mainBookmarkSharedView?.CustName2,
                        CustName3 = mainBookmarkSharedView?.CustName3,
                        CustName4 = mainBookmarkSharedView?.CustName4,
                        CustName5 = mainBookmarkSharedView?.CustName5,
                        CustName6 = mainBookmarkSharedView?.CustName6,
                        CustName7 = mainBookmarkSharedView?.CustName7,
                        CustName8 = mainBookmarkSharedView?.CustName8,
                        CustTaxID1 = mainBookmarkSharedView?.CustTaxID1,
                        CompanyWitnessFirst1 = mainBookmarkSharedView?.CompanyWitnessFirst1,
                        CompanyWitnessSecond1 = mainBookmarkSharedView?.CompanyWitnessSecond1,
                        Text1 = mainBookmarkSharedView?.Text1,
                        Text2 = mainBookmarkSharedView?.Text2,
                        Text3 = mainBookmarkSharedView?.Text3,
                        Text4 = mainBookmarkSharedView?.Text4,
                        AuthorityPersonFirst1 = mainBookmarkSharedView?.AuthorityPersonFirst1,
                        AuthorityPersonFirst2 = mainBookmarkSharedView?.AuthorityPersonFirst2,
                        AuthorityPersonSecond1 = mainBookmarkSharedView?.AuthorityPersonSecond1,
                        AuthorityPersonSecond2 = mainBookmarkSharedView?.AuthorityPersonSecond2,
                        AuthorityPersonThird1 = mainBookmarkSharedView?.AuthorityPersonThird1,
                        AuthorityPersonThird2 = mainBookmarkSharedView?.AuthorityPersonThird2,
                        AuthorizedBuyerFirst1 = mainBookmarkSharedView?.AuthorizedBuyerFirst1,
                        AuthorizedBuyerFirst2 = mainBookmarkSharedView?.AuthorizedBuyerFirst2,
                        AuthorizedBuyerSecond1 = mainBookmarkSharedView?.AuthorizedBuyerSecond1,
                        AuthorizedBuyerSecond2 = mainBookmarkSharedView?.AuthorizedBuyerSecond2,
                        AuthorizedBuyerThird1 = mainBookmarkSharedView?.AuthorizedBuyerThird1,
                        AuthorizedBuyerThird2 = mainBookmarkSharedView?.AuthorizedBuyerThird2,
                        AuthorizedCustFirst1 = mainBookmarkSharedView?.AuthorizedCustFirst1,
                        AuthorizedCustFirst2 = mainBookmarkSharedView?.AuthorizedCustFirst2,
                        AuthorizedCustSecond1 = mainBookmarkSharedView?.AuthorizedCustSecond1,
                        AuthorizedCustSecond2 = mainBookmarkSharedView?.AuthorizedCustSecond2,
                        AuthorizedCustThird1 = mainBookmarkSharedView?.AuthorizedCustThird1,
                        AuthorizedCustThird2 = mainBookmarkSharedView?.AuthorizedCustThird2,
                        CustWitnessFirst1 = mainBookmarkSharedView?.CustWitnessFirst1,
                        CustWitnessSecond1 = mainBookmarkSharedView?.CustWitnessSecond1,
                        PositionBuyer1 = mainBookmarkSharedView?.PositionBuyer1,
                        PositionCust1 = mainBookmarkSharedView?.PositionCust1,
                        PositionCust2 = mainBookmarkSharedView?.PositionCust2,
                        PositionCust3 = mainBookmarkSharedView?.PositionCust3,
                        PositionLIT1 = mainBookmarkSharedView?.PositionLIT1,
                        PositionLIT2 = mainBookmarkSharedView?.PositionLIT2,
                        PositionLIT3 = mainBookmarkSharedView?.PositionLIT3,
                        CustAddress1 = mainBookmarkSharedView?.CustAddress1,
                        ManualNo1 = mainBookmarkSharedView?.ManualNo1,

                        JointAddressFirst1 = mainAgreementJvBookmarkView?.JointAddressFirst1,
                        JointNameFirst1 = mainAgreementJvBookmarkView?.JointNameFirst1,
                        JointAddressSecond1 = mainAgreementJvBookmarkView?.JointAddressSecond1,
                        JointNameSecond1 = mainAgreementJvBookmarkView?.JointNameSecond1,
                        JointAddressThird1 = mainAgreementJvBookmarkView?.JointAddressThird1,
                        JointNameThird1 = mainAgreementJvBookmarkView?.JointNameThird1,
                        JointAddressFourth1 = mainAgreementJvBookmarkView?.JointAddressFourth1,
                        JointNameFourth1 = mainAgreementJvBookmarkView?.JointNameFourth1,
                        PositionJointFirst1 = mainAgreementJvBookmarkView?.PositionJointFirst1,
                        PositionJointSecond1 = mainAgreementJvBookmarkView?.PositionJointSecond1,
                        PositionJointThird1 = mainAgreementJvBookmarkView?.PositionJointThird1,
                        PositionJointFourth1 = mainAgreementJvBookmarkView?.PositionJointFourth1,
                        JointAddressFirst2 = mainAgreementJvBookmarkView?.JointAddressFirst2,
                        JointNameFirst2 = mainAgreementJvBookmarkView?.JointNameFirst2,
                        JointAddressSecond2 = mainAgreementJvBookmarkView?.JointAddressSecond2,
                        JointNameSecond2 = mainAgreementJvBookmarkView?.JointNameSecond2,
                        JointAddressThird2 = mainAgreementJvBookmarkView?.JointAddressThird2,
                        JointNameThird2 = mainAgreementJvBookmarkView?.JointNameThird2,
                        JointAddressFourth2 = mainAgreementJvBookmarkView?.JointAddressFourth2,
                        JointNameFourth2 = mainAgreementJvBookmarkView?.JointNameFourth2,
                        PositionJointFirst2 = mainAgreementJvBookmarkView?.PositionJointFirst2,
                        PositionJointSecond2 = mainAgreementJvBookmarkView?.PositionJointSecond2,
                        PositionJointThird2 = mainAgreementJvBookmarkView?.PositionJointThird2,
                        PositionJointFourth2 = mainAgreementJvBookmarkView?.PositionJointFourth2,
                        JointAddressFirst3 = mainAgreementJvBookmarkView?.JointAddressFirst3,
                        JointNameFirst3 = mainAgreementJvBookmarkView?.JointNameFirst3,
                        JointAddressSecond3 = mainAgreementJvBookmarkView?.JointAddressSecond3,
                        JointNameSecond3 = mainAgreementJvBookmarkView?.JointNameSecond3,
                        JointAddressThird3 = mainAgreementJvBookmarkView?.JointAddressThird3,
                        JointNameThird3 = mainAgreementJvBookmarkView?.JointNameThird3,
                        JointAddressFourth3 = mainAgreementJvBookmarkView?.JointAddressFourth3,
                        JointNameFourth3 = mainAgreementJvBookmarkView?.JointNameFourth3,
                        PositionJointFirst3 = mainAgreementJvBookmarkView?.PositionJointFirst3,
                        PositionJointSecond3 = mainAgreementJvBookmarkView?.PositionJointSecond3,
                        PositionJointThird3 = mainAgreementJvBookmarkView?.PositionJointThird3,
                        PositionJointFourth3 = mainAgreementJvBookmarkView?.PositionJointFourth3,
                        JointAddressFirst4 = mainAgreementJvBookmarkView?.JointAddressFirst4,
                        JointNameFirst4 = mainAgreementJvBookmarkView?.JointNameFirst4,
                        JointAddressSecond4 = mainAgreementJvBookmarkView?.JointAddressSecond4,
                        JointNameSecond4 = mainAgreementJvBookmarkView?.JointNameSecond4,
                        JointAddressThird4 = mainAgreementJvBookmarkView?.JointAddressThird4,
                        JointNameThird4 = mainAgreementJvBookmarkView?.JointNameThird4,
                        JointAddressFourth4 = mainAgreementJvBookmarkView?.JointAddressFourth4,
                        JointNameFourth4 = mainAgreementJvBookmarkView?.JointNameFourth4,
                        PositionJointFirst4 = mainAgreementJvBookmarkView?.PositionJointFirst4,
                        PositionJointSecond4 = mainAgreementJvBookmarkView?.PositionJointSecond4,
                        PositionJointThird4 = mainAgreementJvBookmarkView?.PositionJointThird4,
                        PositionJointFourth4 = mainAgreementJvBookmarkView?.PositionJointFourth4,
                        TextJVDetail1 = mainAgreementJvBookmarkView?.TextJVDetail1,
                        OperatedbyFirst1 = mainAgreementJvBookmarkView?.OperatedbyFirst1,
                        OperatedbySecond1 = mainAgreementJvBookmarkView?.OperatedbySecond1,
                        OperatedbyThird1 = mainAgreementJvBookmarkView?.OperatedbyThird1,
                        OperatedbyFourth1 = mainAgreementJvBookmarkView?.OperatedbyFourth1,
                    };

                return genMainAgreementBookmarkSharedJvView;

                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                var mainAgreementShareJVView = mainAgreementTableRepo.MainAgreementBookmarkSharedJVView(refGuid);

                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);
                var bookmarkItem = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByIdNoTracking(bookmarkDocumentTransGuid);

                ISharedService sharedService = new SharedService(db);
                if (mainAgreementShareJVView != null)
                {
                    mainAgreementShareJVView.ContAmountSecond1 = sharedService.NumberToWordsTH(mainAgreementShareJVView.ContAmountFirst1);
                    mainAgreementShareJVView.AgreementDate1 = sharedService.DateToWord(mainAgreementShareJVView.AgreementDate, "TH", "DAY MONTH YEAR");
                    mainAgreementShareJVView.AgreementDate2 = mainAgreementShareJVView.AgreementDate1;
                    mainAgreementShareJVView.AgreementDate3 = mainAgreementShareJVView.AgreementDate1;
                    mainAgreementShareJVView.AgreementDate4 = mainAgreementShareJVView.AgreementDate1;
                    mainAgreementShareJVView.AgreementDate5 = mainAgreementShareJVView.AgreementDate1;
                    if (bookmarkItem != null) mainAgreementShareJVView.ManualNo1 = bookmarkItem.ReferenceExternalId;
                }

                return mainAgreementShareJVView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region  MainAgreementExpenceDetail
        public GenMainAgreementExpenceDetailView GenMainAgreementExpenceDetailView(Guid refGuid)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                GenMainAgreementExpenceDetailView mainAgmExp = mainAgreementTableRepo.MainAgreementExpenceDetailView(refGuid);
                ISharedService sharedService = new SharedService(db);
                IServiceFeeTransService serviceFeeTrans = new ServiceFeeTransService(db);

                decimal totalServiceFee = mainAgmExp.ServiceFeeTrans.Sum(s => s.AmountIncludeTax - s.WHTAmount);
                decimal upcountryChequeFee = serviceFeeTrans.GetUpcountryChequeFee(totalServiceFee);
                decimal expTransTotalCheck = totalServiceFee + upcountryChequeFee;

                decimal upcountryChequeFeeSM1 = serviceFeeTrans.GetUpcountryChequeFee(mainAgmExp.ServiceFeeTransSM[0].AmountIncludeTax -
                    mainAgmExp.ServiceFeeTransSM[0].WHTAmount);
                decimal expTransTotalCheckSM1 = (mainAgmExp.ServiceFeeTransSM[0].AmountIncludeTax -
                    mainAgmExp.ServiceFeeTransSM[0].WHTAmount) + upcountryChequeFeeSM1;

                decimal upcountryChequeFeeSM2 = serviceFeeTrans.GetUpcountryChequeFee(mainAgmExp.ServiceFeeTransSM[1].AmountIncludeTax -
                    mainAgmExp.ServiceFeeTransSM[1].WHTAmount);
                decimal expTransTotalCheckSM2 = (mainAgmExp.ServiceFeeTransSM[1].AmountIncludeTax -
                    mainAgmExp.ServiceFeeTransSM[1].WHTAmount) + upcountryChequeFeeSM2;

                decimal upcountryChequeFeeSM3 = serviceFeeTrans.GetUpcountryChequeFee(mainAgmExp.ServiceFeeTransSM[2].AmountIncludeTax -
                    mainAgmExp.ServiceFeeTransSM[2].WHTAmount);
                decimal expTransTotalCheckSM3 = (mainAgmExp.ServiceFeeTransSM[2].AmountIncludeTax -
                    mainAgmExp.ServiceFeeTransSM[2].WHTAmount) + upcountryChequeFeeSM3;

                if (mainAgmExp != null)
                {
                    mainAgmExp.ContractDate1 = sharedService.DateToWord(mainAgmExp.AgreementDate, TextConstants.TH, TextConstants.DMY);
                    mainAgmExp.ContractDate2 = sharedService.DateToWord(mainAgmExp.AgreementDate, TextConstants.TH, TextConstants.DMY);
                    mainAgmExp.ContractDate3 = sharedService.DateToWord(mainAgmExp.AgreementDate, TextConstants.TH, TextConstants.DMY);
                    mainAgmExp.ContractDate4 = sharedService.DateToWord(mainAgmExp.AgreementDate, TextConstants.TH, TextConstants.DMY);
                    mainAgmExp.ContractDate5 = sharedService.DateToWord(mainAgmExp.AgreementDate, TextConstants.TH, TextConstants.DMY);
                    mainAgmExp.TotalServiceFee1 = totalServiceFee;
                    mainAgmExp.TotalServiceFee2 = totalServiceFee;
                    mainAgmExp.BankChargeCheck1 = upcountryChequeFee;
                    mainAgmExp.BankChargeCheck2 = upcountryChequeFee;
                    mainAgmExp.ExpTransTotalCheck1 = expTransTotalCheck;
                    mainAgmExp.SMBankChargeCheck1 = upcountryChequeFeeSM1;
                    mainAgmExp.SMExpTransTotalCheck1 = expTransTotalCheckSM1;
                    mainAgmExp.SMBankChargeCheck2 = upcountryChequeFeeSM2;
                    mainAgmExp.SMExpTransTotalCheck2 = expTransTotalCheckSM2;
                    mainAgmExp.SMBankChargeCheck3 = upcountryChequeFeeSM3;
                    mainAgmExp.SMExpTransTotalCheck3 = expTransTotalCheckSM3;
                }
                return mainAgmExp;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }

        #endregion

        #region BookmarkPF
        public MainAgreementPFBookmarkView GetMainAgreementPFBookmarkValue(Guid refGuid)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementPFBookmarkViewMap mainAgreementPFBookmarkViewMap = mainAgreementTableRepo.GetMainAgreementPFBookmarkValue(refGuid);

                ISharedService sharedService = new SharedService(db);
                string revoving = mainAgreementPFBookmarkViewMap.Revolving == true ? TextConstants.Checked : TextConstants.UnChecked;
                string nonRevoving = mainAgreementPFBookmarkViewMap.Revolving == false ? TextConstants.Checked :TextConstants.UnChecked;

                MainAgreementPFBookmarkView mainAgreementPFBookmarkView = new MainAgreementPFBookmarkView()
                {
                    TypeLoanFirst1 = revoving,
                    TypeLoanSecond1 = nonRevoving,
                    TermLoan1 = int.Parse( mainAgreementPFBookmarkViewMap.AgreementYear),
                    CreditStartDate1 = mainAgreementPFBookmarkViewMap.MainAgmStartDate == null ? "" : sharedService.DateToWord(mainAgreementPFBookmarkViewMap.MainAgmStartDate.Value, TextConstants.TH, TextConstants.DMY),
                    CreditEndDate1 = mainAgreementPFBookmarkViewMap.MainAgmExpiryDate == null? "" : sharedService.DateToWord(mainAgreementPFBookmarkViewMap.MainAgmExpiryDate.Value, TextConstants.TH, TextConstants.DMY),
                    WithdrawAmountFirst1 = mainAgreementPFBookmarkViewMap.WithdrawalAmount,
                    WithdrawAmountSecond1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.WithdrawalAmount),
                    WithdrawOutstandingFirst1 = mainAgreementPFBookmarkViewMap.ApprovedCreditLimit - mainAgreementPFBookmarkViewMap.WithdrawalAmount,
                    WithdrawOutstandingSecond1 = sharedService.NumberToWordsTH((mainAgreementPFBookmarkViewMap.ApprovedCreditLimit - mainAgreementPFBookmarkViewMap.WithdrawalAmount)),
                    EndDateContract1 = mainAgreementPFBookmarkViewMap.WithdrawalDueDate == null ? "" : sharedService.DateToWord(mainAgreementPFBookmarkViewMap.WithdrawalDueDate.Value, TextConstants.TH, TextConstants.DMY),
                    EndDateContract2 = mainAgreementPFBookmarkViewMap.WithdrawalDueDate == null ? "" : sharedService.DateToWord(mainAgreementPFBookmarkViewMap.WithdrawalDueDate.Value, TextConstants.TH, TextConstants.DMY),
                    ContAmountFirst2 = mainAgreementPFBookmarkViewMap.WithdrawalAmount,
                    ContAmountSecond2 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.WithdrawalAmount),
                    TextContractObjective1 = mainAgreementPFBookmarkViewMap.Description,
                    GuarantorNameFirst1 = mainAgreementPFBookmarkViewMap.GuarantorName,
                    TextContractGuarantee1 = mainAgreementPFBookmarkViewMap.GuaranteeText,
                    InterestNoCheckFirst1 = mainAgreementPFBookmarkViewMap.ChequeNo1,
                    InterestNoCheckSecond1 = mainAgreementPFBookmarkViewMap.ChequeNo2,
                    InterestNoCheckThird1 = mainAgreementPFBookmarkViewMap.ChequeNo3,
                    InterestNoCheckFourth1 = mainAgreementPFBookmarkViewMap.ChequeNo4,
                    InterestNoCheckFifth1 = mainAgreementPFBookmarkViewMap.ChequeNo5,
                    InterestNoCheckSixth1 = mainAgreementPFBookmarkViewMap.ChequeNo6,
                    InterestNoCheckSeventh1 = mainAgreementPFBookmarkViewMap.ChequeNo7,
                    InterestNoCheckEighth1 = mainAgreementPFBookmarkViewMap.ChequeNo8,
                    InterestNoCheckNineth1 = mainAgreementPFBookmarkViewMap.ChequeNo9,
                    InterestNoCheckTenth1 = mainAgreementPFBookmarkViewMap.ChequeNo10,
                    InterestNoCheckEleventh1 = mainAgreementPFBookmarkViewMap.ChequeNo11,
                    InterestNoCheckTwelfth1 = mainAgreementPFBookmarkViewMap.ChequeNo12,
                    InterestNoCheckThirteenth1 = mainAgreementPFBookmarkViewMap.ChequeNo13,
                    InterestNoCheckFourteenth1 = mainAgreementPFBookmarkViewMap.ChequeNo14,
                    InterestNoCheckFifteenth1 = mainAgreementPFBookmarkViewMap.ChequeNo15,
                    InterestNoCheckSixteenth1 = mainAgreementPFBookmarkViewMap.ChequeNo16,
                    InterestNoCheckSeventeenth1 = mainAgreementPFBookmarkViewMap.ChequeNo17,
                    InterestNoCheckEighteenth1 = mainAgreementPFBookmarkViewMap.ChequeNo18,
                    InterestNoCheckNineteenth1 = mainAgreementPFBookmarkViewMap.ChequeNo19,
                    InterestNoCheckTwentieth1 = mainAgreementPFBookmarkViewMap.ChequeNo20,
                    InterestNoCheckTwentyfirst1 = mainAgreementPFBookmarkViewMap.ChequeNo21,
                    InterestNoCheckTwentysecond1 = mainAgreementPFBookmarkViewMap.ChequeNo22,
                    InterestNoCheckTwentythird1 = mainAgreementPFBookmarkViewMap.ChequeNo23,
                    InterestNoCheckTwentyfourth1 = mainAgreementPFBookmarkViewMap.ChequeNo24,
                    InterestDateFirst1 = mainAgreementPFBookmarkViewMap.ChequeDate1 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate1.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateSecond1 = mainAgreementPFBookmarkViewMap.ChequeDate2 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate2.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateThird1 = mainAgreementPFBookmarkViewMap.ChequeDate3 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate3.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateFourth1 = mainAgreementPFBookmarkViewMap.ChequeDate4 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate4.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateFifth1 = mainAgreementPFBookmarkViewMap.ChequeDate5 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate5.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateSixth1 = mainAgreementPFBookmarkViewMap.ChequeDate6 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate6.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateSeventh1 = mainAgreementPFBookmarkViewMap.ChequeDate7 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate7.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateEighth1 = mainAgreementPFBookmarkViewMap.ChequeDate8 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate8.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateNineth1 = mainAgreementPFBookmarkViewMap.ChequeDate9 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate9.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateTenth1 = mainAgreementPFBookmarkViewMap.ChequeDate10 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate10.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateEleventh1 = mainAgreementPFBookmarkViewMap.ChequeDate11 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate11.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateTwelfth1 = mainAgreementPFBookmarkViewMap.ChequeDate12 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate12.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateThirteenth1 = mainAgreementPFBookmarkViewMap.ChequeDate13 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate13.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateFourteenth1 = mainAgreementPFBookmarkViewMap.ChequeDate14 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate14.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateFifteenth1 = mainAgreementPFBookmarkViewMap.ChequeDate15 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate15.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateSixteenth1 = mainAgreementPFBookmarkViewMap.ChequeDate16 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate16.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateSeventeenth1 = mainAgreementPFBookmarkViewMap.ChequeDate17 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate17.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateEighteenth1 = mainAgreementPFBookmarkViewMap.ChequeDate18 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate18.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateNineteenth1 = mainAgreementPFBookmarkViewMap.ChequeDate19 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate19.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateTwentieth1 = mainAgreementPFBookmarkViewMap.ChequeDate20 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate20.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateTwentyfirst1 = mainAgreementPFBookmarkViewMap.ChequeDate21 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate21.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateTwentysecond1 = mainAgreementPFBookmarkViewMap.ChequeDate22 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate22.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateTwentythird1 = mainAgreementPFBookmarkViewMap.ChequeDate23 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate23.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestDateTwentyfourth1 = mainAgreementPFBookmarkViewMap.ChequeDate24 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate24.Value, TextConstants.TH, TextConstants.DMY): "",
                    InterestAmountFirst1 = mainAgreementPFBookmarkViewMap.InterestAmount1,
                    InterestAmountSecond1 = mainAgreementPFBookmarkViewMap.InterestAmount2,
                    InterestAmountThird1 = mainAgreementPFBookmarkViewMap.InterestAmount3,
                    InterestAmountFourth1 = mainAgreementPFBookmarkViewMap.InterestAmount4,
                    InterestAmountFifth1 = mainAgreementPFBookmarkViewMap.InterestAmount5,
                    InterestAmountSixth1 = mainAgreementPFBookmarkViewMap.InterestAmount6,
                    InterestAmountSeventh1 = mainAgreementPFBookmarkViewMap.InterestAmount7,
                    InterestAmountEighth1 = mainAgreementPFBookmarkViewMap.InterestAmount8,
                    InterestAmountNineth1 = mainAgreementPFBookmarkViewMap.InterestAmount9,
                    InterestAmountTenth1 = mainAgreementPFBookmarkViewMap.InterestAmount10,
                    InterestAmountEleventh1 = mainAgreementPFBookmarkViewMap.InterestAmount11,
                    InterestAmountTwelfth1 = mainAgreementPFBookmarkViewMap.InterestAmount12,
                    InterestAmountThirteenth1 = mainAgreementPFBookmarkViewMap.InterestAmount13,
                    InterestAmountFourteenth1 = mainAgreementPFBookmarkViewMap.InterestAmount14,
                    InterestAmountFifteenth1 = mainAgreementPFBookmarkViewMap.InterestAmount15,
                    InterestAmountSixteenth1 = mainAgreementPFBookmarkViewMap.InterestAmount16,
                    InterestAmountSeventeenth1 = mainAgreementPFBookmarkViewMap.InterestAmount17,
                    InterestAmountEighteenth1 = mainAgreementPFBookmarkViewMap.InterestAmount18,
                    InterestAmountNineteenth1 = mainAgreementPFBookmarkViewMap.InterestAmount19,
                    InterestAmountTwentieth1 = mainAgreementPFBookmarkViewMap.InterestAmount20,
                    InterestAmountTwentyfirst1 = mainAgreementPFBookmarkViewMap.InterestAmount21,
                    InterestAmountTwentysecond1 = mainAgreementPFBookmarkViewMap.InterestAmount22,
                    InterestAmountTwentythird1 = mainAgreementPFBookmarkViewMap.InterestAmount23,
                    InterestAmountTwentyfourth1 = mainAgreementPFBookmarkViewMap.InterestAmount24,
                    InterestDateFirst2 = mainAgreementPFBookmarkViewMap.ChequeDate1 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate1.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateSecond2 = mainAgreementPFBookmarkViewMap.ChequeDate2 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate2.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateThird2 = mainAgreementPFBookmarkViewMap.ChequeDate3 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate3.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateFourth2 = mainAgreementPFBookmarkViewMap.ChequeDate4 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate4.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateFifth2 = mainAgreementPFBookmarkViewMap.ChequeDate5 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate5.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateSixth2 = mainAgreementPFBookmarkViewMap.ChequeDate6 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate6.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateSeventh2 = mainAgreementPFBookmarkViewMap.ChequeDate7 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate7.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateEighth2 = mainAgreementPFBookmarkViewMap.ChequeDate8 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate8.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateNineth2 = mainAgreementPFBookmarkViewMap.ChequeDate9 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate9.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateTenth2 = mainAgreementPFBookmarkViewMap.ChequeDate10 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate10.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateEleventh2 = mainAgreementPFBookmarkViewMap.ChequeDate11 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate11.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateTwelfth2 = mainAgreementPFBookmarkViewMap.ChequeDate12 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate12.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateThirteenth2 = mainAgreementPFBookmarkViewMap.ChequeDate13 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate13.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateFourteenth2 = mainAgreementPFBookmarkViewMap.ChequeDate14 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate14.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateFifteenth2 = mainAgreementPFBookmarkViewMap.ChequeDate15 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate15.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateSixteenth2 = mainAgreementPFBookmarkViewMap.ChequeDate16 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate16.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateSeventeenth2 = mainAgreementPFBookmarkViewMap.ChequeDate17 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate17.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateEighteenth2 = mainAgreementPFBookmarkViewMap.ChequeDate18 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate18.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateNineteenth2 = mainAgreementPFBookmarkViewMap.ChequeDate19 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate19.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateTwentieth2 = mainAgreementPFBookmarkViewMap.ChequeDate20 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate20.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateTwentyfirst2 = mainAgreementPFBookmarkViewMap.ChequeDate21 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate21.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateTwentysecond2 = mainAgreementPFBookmarkViewMap.ChequeDate22 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate22.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateTwentythird2 = mainAgreementPFBookmarkViewMap.ChequeDate23 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate23.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestDateTwentyfourth2 = mainAgreementPFBookmarkViewMap.ChequeDate24 != null ? sharedService.DateToWord(mainAgreementPFBookmarkViewMap.ChequeDate24.Value, TextConstants.TH, TextConstants.DMY) : "",
                    InterestAmountFirst2 = mainAgreementPFBookmarkViewMap.InterestAmount1,
                    InterestAmountSecond2 = mainAgreementPFBookmarkViewMap.InterestAmount2,
                    InterestAmountThird2 = mainAgreementPFBookmarkViewMap.InterestAmount3,
                    InterestAmountFourth2 = mainAgreementPFBookmarkViewMap.InterestAmount4,
                    InterestAmountFifth2 = mainAgreementPFBookmarkViewMap.InterestAmount5,
                    InterestAmountSixth2 = mainAgreementPFBookmarkViewMap.InterestAmount6,
                    InterestAmountSeventh2 = mainAgreementPFBookmarkViewMap.InterestAmount7,
                    InterestAmountEighth2 = mainAgreementPFBookmarkViewMap.InterestAmount8,
                    InterestAmountNineth2 = mainAgreementPFBookmarkViewMap.InterestAmount9,
                    InterestAmountTenth2 = mainAgreementPFBookmarkViewMap.InterestAmount10,
                    InterestAmountEleventh2 = mainAgreementPFBookmarkViewMap.InterestAmount11,
                    InterestAmountTwelfth2 = mainAgreementPFBookmarkViewMap.InterestAmount12,
                    InterestAmountThirteenth2 = mainAgreementPFBookmarkViewMap.InterestAmount13,
                    InterestAmountFourteenth2 = mainAgreementPFBookmarkViewMap.InterestAmount14,
                    InterestAmountFifteenth2 = mainAgreementPFBookmarkViewMap.InterestAmount15,
                    InterestAmountSixteenth2 = mainAgreementPFBookmarkViewMap.InterestAmount16,
                    InterestAmountSeventeenth2 = mainAgreementPFBookmarkViewMap.InterestAmount17,
                    InterestAmountEighteenth2 = mainAgreementPFBookmarkViewMap.InterestAmount18,
                    InterestAmountNineteenth2 = mainAgreementPFBookmarkViewMap.InterestAmount19,
                    InterestAmountTwentieth2 = mainAgreementPFBookmarkViewMap.InterestAmount20,
                    InterestAmountTwentyfirst2 = mainAgreementPFBookmarkViewMap.InterestAmount21,
                    InterestAmountTwentysecond2 = mainAgreementPFBookmarkViewMap.InterestAmount22,
                    InterestAmountTwentythird2 = mainAgreementPFBookmarkViewMap.InterestAmount23,
                    InterestAmountTwentyfourth2 = mainAgreementPFBookmarkViewMap.InterestAmount24,
                    InterestTotalAmountFirst1 = mainAgreementPFBookmarkViewMap.THSumInterestAmount,
                    InterestTotalAmountFirst2 = mainAgreementPFBookmarkViewMap.THSumInterestAmount,
                    InterestTotalAmountSecond1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.THSumInterestAmount),
                    InterestWordingFirst1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount1),
                    InterestWordingSecond1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount2),
                    InterestWordingThird1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount3),
                    InterestWordingFourth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount4),
                    InterestWordingFifth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount5),
                    InterestWordingSixth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount6),
                    InterestWordingSeventh1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount7),
                    InterestWordingEighth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount8),
                    InterestWordingNineth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount9),
                    InterestWordingTenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount10),
                    InterestWordingEleventh1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount11),
                    InterestWordingTwelfth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount12),
                    InterestWordingThirteenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount13),
                    InterestWordingFourteenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount14),
                    InterestWordingFifteenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount15),
                    InterestWordingSixteenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount16),
                    InterestWordingSeventeenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount17),
                    InterestWordingEighteenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount18),
                    InterestWordingNineteenth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount19),
                    InterestWordingTwentieth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount20),
                    InterestWordingTwentyfirst1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount21),
                    InterestWordingTwentysecond1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount22),
                    InterestWordingTwentythird1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount23),
                    InterestWordingTwentyfourth1 = sharedService.NumberToWordsTH(mainAgreementPFBookmarkViewMap.InterestAmount24)
                };

                return mainAgreementPFBookmarkView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        

        #region Query LIT1629, LIT1631
        public MainAgreementPFBookmarkView GetMainAgreementPFAndSharedBookmarkValue (Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                MainAgreementPFBookmarkView mainAgreementPFBookmarkView = new MainAgreementPFBookmarkView();
                mainAgreementPFBookmarkView = GetMainAgreementPFBookmarkValue(refGuid);

                GenMainAgreementBookmarkSharedView mainAgreementBookmarkSharedView = new MainAgreementPFBookmarkView();
                mainAgreementBookmarkSharedView = GetMainAgreementBookmarkShared(refGuid, bookmarkDocumentTransGuid);
                mainAgreementPFBookmarkView.ContAmountSecond1 = mainAgreementBookmarkSharedView.ContAmountSecond1;
                mainAgreementPFBookmarkView.AgreementDate1 = mainAgreementBookmarkSharedView.AgreementDate1;
                mainAgreementPFBookmarkView.AgreementDate2 = mainAgreementBookmarkSharedView.AgreementDate2;
                mainAgreementPFBookmarkView.AgreementDate3 = mainAgreementBookmarkSharedView.AgreementDate3;
                mainAgreementPFBookmarkView.AgreementDate4 = mainAgreementBookmarkSharedView.AgreementDate4;
                mainAgreementPFBookmarkView.AgreementDate5 = mainAgreementBookmarkSharedView.AgreementDate5;
                mainAgreementPFBookmarkView.ManualNo1 = mainAgreementBookmarkSharedView.ManualNo1;
                mainAgreementPFBookmarkView.ContAmountFirst1 = mainAgreementBookmarkSharedView.ContAmountFirst1;
                mainAgreementPFBookmarkView.AgreementDate = mainAgreementBookmarkSharedView.AgreementDate;

                mainAgreementPFBookmarkView.AgreementNo1 = mainAgreementBookmarkSharedView.AgreementNo1;
                mainAgreementPFBookmarkView.AgreementNo2 = mainAgreementBookmarkSharedView.AgreementNo2;
                mainAgreementPFBookmarkView.AgreementNo3 = mainAgreementBookmarkSharedView.AgreementNo3;
                mainAgreementPFBookmarkView.AgreementNo4 = mainAgreementBookmarkSharedView.AgreementNo4;
                mainAgreementPFBookmarkView.AgreementNo5 = mainAgreementBookmarkSharedView.AgreementNo5;
                mainAgreementPFBookmarkView.InterestRate1 = mainAgreementBookmarkSharedView.InterestRate1;
                mainAgreementPFBookmarkView.AliasNameCust1 = mainAgreementBookmarkSharedView.AliasNameCust1;
                mainAgreementPFBookmarkView.AliasNameCust2 = mainAgreementBookmarkSharedView.AliasNameCust2;
                mainAgreementPFBookmarkView.AliasNameCust3 = mainAgreementBookmarkSharedView.AliasNameCust3;
                mainAgreementPFBookmarkView.AliasNameCust4 = mainAgreementBookmarkSharedView.AliasNameCust4;
                mainAgreementPFBookmarkView.AliasNameCust5 = mainAgreementBookmarkSharedView.AliasNameCust5;
                mainAgreementPFBookmarkView.AliasNameCust6 = mainAgreementBookmarkSharedView.AliasNameCust6;
                mainAgreementPFBookmarkView.AliasNameCust7 = mainAgreementBookmarkSharedView.AliasNameCust7;
                mainAgreementPFBookmarkView.AliasNameCust8 = mainAgreementBookmarkSharedView.AliasNameCust8;
                mainAgreementPFBookmarkView.CustName1 = mainAgreementBookmarkSharedView.CustName1;
                mainAgreementPFBookmarkView.CustName2 = mainAgreementBookmarkSharedView.CustName2;
                mainAgreementPFBookmarkView.CustName3 = mainAgreementBookmarkSharedView.CustName3;
                mainAgreementPFBookmarkView.CustName4 = mainAgreementBookmarkSharedView.CustName4;
                mainAgreementPFBookmarkView.CustName5 = mainAgreementBookmarkSharedView.CustName5;
                mainAgreementPFBookmarkView.CustName6 = mainAgreementBookmarkSharedView.CustName6;
                mainAgreementPFBookmarkView.CustName7 = mainAgreementBookmarkSharedView.CustName7;
                mainAgreementPFBookmarkView.CustName8 = mainAgreementBookmarkSharedView.CustName8;
                mainAgreementPFBookmarkView.CustTaxID1 = mainAgreementBookmarkSharedView.CustTaxID1;
                mainAgreementPFBookmarkView.CompanyWitnessFirst1 = mainAgreementBookmarkSharedView.CompanyWitnessFirst1;
                mainAgreementPFBookmarkView.CompanyWitnessSecond1 = mainAgreementBookmarkSharedView.CompanyWitnessSecond1;
                mainAgreementPFBookmarkView.Text1 = mainAgreementBookmarkSharedView.Text1;
                mainAgreementPFBookmarkView.Text2 = mainAgreementBookmarkSharedView.Text2;
                mainAgreementPFBookmarkView.Text3 = mainAgreementBookmarkSharedView.Text3;
                mainAgreementPFBookmarkView.Text4 = mainAgreementBookmarkSharedView.Text4;
                mainAgreementPFBookmarkView.AuthorityPersonFirst1 = mainAgreementBookmarkSharedView.AuthorityPersonFirst1;
                mainAgreementPFBookmarkView.AuthorityPersonFirst2 = mainAgreementBookmarkSharedView.AuthorityPersonFirst2;
                mainAgreementPFBookmarkView.AuthorityPersonSecond1 = mainAgreementBookmarkSharedView.AuthorityPersonSecond1;
                mainAgreementPFBookmarkView.AuthorityPersonSecond2 = mainAgreementBookmarkSharedView.AuthorityPersonSecond2;
                mainAgreementPFBookmarkView.AuthorityPersonThird1 = mainAgreementBookmarkSharedView.AuthorityPersonThird1;
                mainAgreementPFBookmarkView.AuthorityPersonThird2 = mainAgreementBookmarkSharedView.AuthorityPersonThird2;
                mainAgreementPFBookmarkView.AuthorizedBuyerFirst1 = mainAgreementBookmarkSharedView.AuthorizedBuyerFirst1;
                mainAgreementPFBookmarkView.AuthorizedBuyerFirst2 = mainAgreementBookmarkSharedView.AuthorizedBuyerFirst2;
                mainAgreementPFBookmarkView.AuthorizedBuyerSecond1 = mainAgreementBookmarkSharedView.AuthorizedBuyerSecond1;
                mainAgreementPFBookmarkView.AuthorizedBuyerSecond2 = mainAgreementBookmarkSharedView.AuthorizedBuyerSecond2;
                mainAgreementPFBookmarkView.AuthorizedBuyerThird1 = mainAgreementBookmarkSharedView.AuthorizedBuyerThird1;
                mainAgreementPFBookmarkView.AuthorizedBuyerThird2 = mainAgreementBookmarkSharedView.AuthorizedBuyerThird2;
                mainAgreementPFBookmarkView.AuthorizedCustFirst1 = mainAgreementBookmarkSharedView.AuthorizedCustFirst1;
                mainAgreementPFBookmarkView.AuthorizedCustFirst2 = mainAgreementBookmarkSharedView.AuthorizedCustFirst2;
                mainAgreementPFBookmarkView.AuthorizedCustSecond1 = mainAgreementBookmarkSharedView.AuthorizedCustSecond1;
                mainAgreementPFBookmarkView.AuthorizedCustSecond2 = mainAgreementBookmarkSharedView.AuthorizedCustSecond2;
                mainAgreementPFBookmarkView.AuthorizedCustThird1 = mainAgreementBookmarkSharedView.AuthorizedCustThird1;
                mainAgreementPFBookmarkView.AuthorizedCustThird2 = mainAgreementBookmarkSharedView.AuthorizedCustThird2;
                mainAgreementPFBookmarkView.CustWitnessFirst1 = mainAgreementBookmarkSharedView.CustWitnessFirst1;
                mainAgreementPFBookmarkView.CustWitnessSecond1 = mainAgreementBookmarkSharedView.CustWitnessSecond1;
                mainAgreementPFBookmarkView.PositionBuyer1 = mainAgreementBookmarkSharedView.PositionBuyer1;
                mainAgreementPFBookmarkView.PositionCust1 = mainAgreementBookmarkSharedView.PositionCust1;
                mainAgreementPFBookmarkView.PositionCust2 = mainAgreementBookmarkSharedView.PositionCust2;
                mainAgreementPFBookmarkView.PositionCust3 = mainAgreementBookmarkSharedView.PositionCust3;
                mainAgreementPFBookmarkView.PositionLIT1 = mainAgreementBookmarkSharedView.PositionLIT1;
                mainAgreementPFBookmarkView.PositionLIT2 = mainAgreementBookmarkSharedView.PositionLIT2;
                mainAgreementPFBookmarkView.PositionLIT3 = mainAgreementBookmarkSharedView.PositionLIT3;
                mainAgreementPFBookmarkView.CustAddress1 = mainAgreementBookmarkSharedView.CustAddress1;
                mainAgreementPFBookmarkView.CustAddress2 = mainAgreementBookmarkSharedView.CustAddress2;

                return mainAgreementPFBookmarkView;
                 }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GetMainAgreementConsortiumBookmarkView
        public GenMainAgreementConsortiumBookmarkView GetMainAgreementConsortiumBookmarkView(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                IConsortiumTransService consortiumTransService = new ConsortiumTransService(db);
                MainAgreementConsortiumBookmarkView mainAgreementConsortiumBookmarkView = consortiumTransService.GetMainAgreementConsortiumBookmarkValue(refGuid);

                GenMainAgreementBookmarkSharedView mainAgreementBookmarkSharedView =
                    GetMainAgreementBookmarkShared(refGuid, bookmarkDocumentTransGuid);

                GenMainAgreementConsortiumBookmarkView result = 
                 new GenMainAgreementConsortiumBookmarkView
                {
                    ContAmountFirst1 = mainAgreementBookmarkSharedView.ContAmountFirst1,
                    ContAmountSecond1 = mainAgreementBookmarkSharedView.ContAmountSecond1,
                    AgreementDate = mainAgreementBookmarkSharedView.AgreementDate,
                    AgreementDate1 = mainAgreementBookmarkSharedView.AgreementDate1,
                    AgreementDate2 = mainAgreementBookmarkSharedView.AgreementDate2,
                    AgreementDate3 = mainAgreementBookmarkSharedView.AgreementDate3,
                    AgreementDate4 = mainAgreementBookmarkSharedView.AgreementDate4,
                    AgreementDate5 = mainAgreementBookmarkSharedView.AgreementDate5,
                    AgreementNo1 = mainAgreementBookmarkSharedView.AgreementNo1,
                    AgreementNo2 = mainAgreementBookmarkSharedView.AgreementNo2,
                    AgreementNo3 = mainAgreementBookmarkSharedView.AgreementNo3,
                    AgreementNo4 = mainAgreementBookmarkSharedView.AgreementNo4,
                    AgreementNo5 = mainAgreementBookmarkSharedView.AgreementNo5,
                    InterestRate1 = mainAgreementBookmarkSharedView.InterestRate1,
                    AliasNameCust1 = mainAgreementBookmarkSharedView.AliasNameCust1,
                    AliasNameCust2 = mainAgreementBookmarkSharedView.AliasNameCust2,
                    AliasNameCust3 = mainAgreementBookmarkSharedView.AliasNameCust3,
                    AliasNameCust4 = mainAgreementBookmarkSharedView.AliasNameCust4,
                    AliasNameCust5 = mainAgreementBookmarkSharedView.AliasNameCust5,
                    AliasNameCust6 = mainAgreementBookmarkSharedView.AliasNameCust6,
                    AliasNameCust7 = mainAgreementBookmarkSharedView.AliasNameCust7,
                    AliasNameCust8 = mainAgreementBookmarkSharedView.AliasNameCust8,
                    CustName1 = mainAgreementBookmarkSharedView.CustName1,
                    CustName2 = mainAgreementBookmarkSharedView.CustName2,
                    CustName3 = mainAgreementBookmarkSharedView.CustName3,
                    CustName4 = mainAgreementBookmarkSharedView.CustName4,
                    CustName5 = mainAgreementBookmarkSharedView.CustName5,
                    CustName6 = mainAgreementBookmarkSharedView.CustName6,
                    CustName7 = mainAgreementBookmarkSharedView.CustName7,
                    CustName8 = mainAgreementBookmarkSharedView.CustName8,
                    CustTaxID1 = mainAgreementBookmarkSharedView.CustTaxID1,
                    CompanyWitnessFirst1 = mainAgreementBookmarkSharedView.CompanyWitnessFirst1,
                    CompanyWitnessSecond1 = mainAgreementBookmarkSharedView.CompanyWitnessSecond1,
                    Text1 = mainAgreementBookmarkSharedView.Text1,
                    Text2 = mainAgreementBookmarkSharedView.Text2,
                    Text3 = mainAgreementBookmarkSharedView.Text3,
                    Text4 = mainAgreementBookmarkSharedView.Text4,
                    AuthorityPersonFirst1 = mainAgreementBookmarkSharedView.AuthorityPersonFirst1,
                    AuthorityPersonFirst2 = mainAgreementBookmarkSharedView.AuthorityPersonFirst2,
                    AuthorityPersonSecond1 = mainAgreementBookmarkSharedView.AuthorityPersonSecond1,
                    AuthorityPersonSecond2 = mainAgreementBookmarkSharedView.AuthorityPersonSecond2,
                    AuthorityPersonThird1 = mainAgreementBookmarkSharedView.AuthorityPersonThird1,
                    AuthorityPersonThird2 = mainAgreementBookmarkSharedView.AuthorityPersonThird2,
                    AuthorizedBuyerFirst1 = mainAgreementBookmarkSharedView.AuthorizedBuyerFirst1,
                    AuthorizedBuyerFirst2 = mainAgreementBookmarkSharedView.AuthorizedBuyerFirst2,
                    AuthorizedBuyerSecond1 = mainAgreementBookmarkSharedView.AuthorizedBuyerSecond1,
                    AuthorizedBuyerSecond2 = mainAgreementBookmarkSharedView.AuthorizedBuyerSecond2,
                    AuthorizedBuyerThird1 = mainAgreementBookmarkSharedView.AuthorizedBuyerThird1,
                    AuthorizedBuyerThird2 = mainAgreementBookmarkSharedView.AuthorizedBuyerThird2,
                    AuthorizedCustFirst1 = mainAgreementBookmarkSharedView.AuthorizedCustFirst1,
                    AuthorizedCustFirst2 = mainAgreementBookmarkSharedView.AuthorizedCustFirst2,
                    AuthorizedCustSecond1 = mainAgreementBookmarkSharedView.AuthorizedCustSecond1,
                    AuthorizedCustSecond2 = mainAgreementBookmarkSharedView.AuthorizedCustSecond2,
                    AuthorizedCustThird1 = mainAgreementBookmarkSharedView.AuthorizedCustThird1,
                    AuthorizedCustThird2 = mainAgreementBookmarkSharedView.AuthorizedCustThird2,
                    CustWitnessFirst1 = mainAgreementBookmarkSharedView.CustWitnessFirst1,
                    CustWitnessSecond1 = mainAgreementBookmarkSharedView.CustWitnessSecond1,
                    PositionBuyer1 = mainAgreementBookmarkSharedView.PositionBuyer1,
                    PositionCust1 = mainAgreementBookmarkSharedView.PositionCust1,
                    PositionCust2 = mainAgreementBookmarkSharedView.PositionCust2,
                    PositionCust3 = mainAgreementBookmarkSharedView.PositionCust3,
                    PositionLIT1 = mainAgreementBookmarkSharedView.PositionLIT1,
                    PositionLIT2 = mainAgreementBookmarkSharedView.PositionLIT2,
                    PositionLIT3 = mainAgreementBookmarkSharedView.PositionLIT3,
                    CustAddress1 = mainAgreementBookmarkSharedView.CustAddress1,
                    CustAddress2 = mainAgreementBookmarkSharedView.CustAddress2,
                    ManualNo1 = mainAgreementBookmarkSharedView.ManualNo1,

                    ConsName1 = mainAgreementConsortiumBookmarkView?.ConsName1,
                    ConsName2 = mainAgreementConsortiumBookmarkView?.ConsName2,
                    ConsName3 = mainAgreementConsortiumBookmarkView?.ConsName3,
                    ConsName4 = mainAgreementConsortiumBookmarkView?.ConsName4,
                    ConsName5 = mainAgreementConsortiumBookmarkView?.ConsName5,
                    ConsName6 = mainAgreementConsortiumBookmarkView?.ConsName6,
                    ConsName7 = mainAgreementConsortiumBookmarkView?.ConsName7,

                     ConsNameCustFirst1 = mainAgreementConsortiumBookmarkView?.ConsNameCustFirst1,
                    ConsNameCustFirst2 = mainAgreementConsortiumBookmarkView?.ConsNameCustFirst2,
                    ConsNameCustFirst3 = mainAgreementConsortiumBookmarkView?.ConsNameCustFirst3,
                    ConsNameCustFirst4 = mainAgreementConsortiumBookmarkView?.ConsNameCustFirst4,

                    ConsNameCustSecond1 = mainAgreementConsortiumBookmarkView?.ConsNameCustSecond1,
                    ConsNameCustSecond2 = mainAgreementConsortiumBookmarkView?.ConsNameCustSecond2,
                    ConsNameCustSecond3 = mainAgreementConsortiumBookmarkView?.ConsNameCustSecond3,
                    ConsNameCustSecond4 = mainAgreementConsortiumBookmarkView?.ConsNameCustSecond4,

                    ConsNameCustThird1 = mainAgreementConsortiumBookmarkView?.ConsNameCustThird1,
                    ConsNameCustThird2 = mainAgreementConsortiumBookmarkView?.ConsNameCustThird2,
                    ConsNameCustThird3 = mainAgreementConsortiumBookmarkView?.ConsNameCustThird3,
                    ConsNameCustThird4 = mainAgreementConsortiumBookmarkView?.ConsNameCustThird4,

                    ConsNameCustForth1 = mainAgreementConsortiumBookmarkView?.ConsNameCustForth1,
                    ConsNameCustForth2 = mainAgreementConsortiumBookmarkView?.ConsNameCustForth2,
                    ConsNameCustForth3 = mainAgreementConsortiumBookmarkView?.ConsNameCustForth3,
                    ConsNameCustForth4 = mainAgreementConsortiumBookmarkView?.ConsNameCustForth4,

                    ConsAddressCustFirst1 = mainAgreementConsortiumBookmarkView?.ConsAddressCustFirst1,
                    ConsAddressCustFirst2 = mainAgreementConsortiumBookmarkView?.ConsAddressCustFirst2,
                    ConsAddressCustFirst3 = mainAgreementConsortiumBookmarkView?.ConsAddressCustFirst3,
                    ConsAddressCustFirst4 = mainAgreementConsortiumBookmarkView?.ConsAddressCustFirst4,

                    ConsAddressCustSecond1 = mainAgreementConsortiumBookmarkView?.ConsAddressCustSecond1,
                    ConsAddressCustSecond2 = mainAgreementConsortiumBookmarkView?.ConsAddressCustSecond2,
                    ConsAddressCustSecond3 = mainAgreementConsortiumBookmarkView?.ConsAddressCustSecond3,
                    ConsAddressCustSecond4 = mainAgreementConsortiumBookmarkView?.ConsAddressCustSecond4,

                    ConsAddressCustThird1 = mainAgreementConsortiumBookmarkView?.ConsAddressCustThird1,
                    ConsAddressCustThird2 = mainAgreementConsortiumBookmarkView?.ConsAddressCustThird2,
                    ConsAddressCustThird3 = mainAgreementConsortiumBookmarkView?.ConsAddressCustThird3,
                    ConsAddressCustThird4 = mainAgreementConsortiumBookmarkView?.ConsAddressCustThird4,

                    ConsAddressCustForth1 = mainAgreementConsortiumBookmarkView?.ConsAddressCustForth1,
                    ConsAddressCustForth2 = mainAgreementConsortiumBookmarkView?.ConsAddressCustForth2,
                    ConsAddressCustForth3 = mainAgreementConsortiumBookmarkView?.ConsAddressCustForth3,
                    ConsAddressCustForth4 = mainAgreementConsortiumBookmarkView?.ConsAddressCustForth4,

                    ConOperatedbyFirst1 = mainAgreementConsortiumBookmarkView?.ConOperatedbyFirst1,
                    ConOperatedbyFirst2 = mainAgreementConsortiumBookmarkView?.ConOperatedbyFirst2,
                    ConOperatedbyFirst3 = mainAgreementConsortiumBookmarkView?.ConOperatedbyFirst3,
                    ConOperatedbyFirst4 = mainAgreementConsortiumBookmarkView?.ConOperatedbyFirst4,

                    ConOperatedbySecond1 = mainAgreementConsortiumBookmarkView?.ConOperatedbySecond1,
                    ConOperatedbySecond2 = mainAgreementConsortiumBookmarkView?.ConOperatedbySecond2,
                    ConOperatedbySecond3 = mainAgreementConsortiumBookmarkView?.ConOperatedbySecond3,
                    ConOperatedbySecond4 = mainAgreementConsortiumBookmarkView?.ConOperatedbySecond4,

                    ConOperatedbyThird1 = mainAgreementConsortiumBookmarkView?.ConOperatedbyThird1,
                    ConOperatedbyThird2 = mainAgreementConsortiumBookmarkView?.ConOperatedbyThird2,
                    ConOperatedbyThird3 = mainAgreementConsortiumBookmarkView?.ConOperatedbyThird3,
                    ConOperatedbyThird4 = mainAgreementConsortiumBookmarkView?.ConOperatedbyThird4,

                    ConOperatedbyForth1 = mainAgreementConsortiumBookmarkView?.ConOperatedbyForth1,
                    ConOperatedbyForth2 = mainAgreementConsortiumBookmarkView?.ConOperatedbyForth2,
                    ConOperatedbyForth3 = mainAgreementConsortiumBookmarkView?.ConOperatedbyForth3,
                    ConOperatedbyForth4 = mainAgreementConsortiumBookmarkView?.ConOperatedbyForth4,

                    PositionConsCustFirst1 = mainAgreementConsortiumBookmarkView?.PositionConsCustFirst1,
                    PositionConsCustFirst2 = mainAgreementConsortiumBookmarkView?.PositionConsCustFirst2,
                    PositionConsCustFirst3 = mainAgreementConsortiumBookmarkView?.PositionConsCustFirst3,
                    PositionConsCustFirst4 = mainAgreementConsortiumBookmarkView?.PositionConsCustFirst4,

                    PositionConsCustSecond1 = mainAgreementConsortiumBookmarkView?.PositionConsCustSecond1,
                    PositionConsCustSecond2 = mainAgreementConsortiumBookmarkView?.PositionConsCustSecond2,
                    PositionConsCustSecond3 = mainAgreementConsortiumBookmarkView?.PositionConsCustSecond3,
                    PositionConsCustSecond4 = mainAgreementConsortiumBookmarkView?.PositionConsCustSecond4,

                    PositionConsCustThird1 = mainAgreementConsortiumBookmarkView?.PositionConsCustThird1,
                    PositionConsCustThird2 = mainAgreementConsortiumBookmarkView?.PositionConsCustThird2,
                    PositionConsCustThird3 = mainAgreementConsortiumBookmarkView?.PositionConsCustThird3,
                    PositionConsCustThird4 = mainAgreementConsortiumBookmarkView?.PositionConsCustThird4,

                    PositionConsCustForth1 = mainAgreementConsortiumBookmarkView?.PositionConsCustForth1,
                    PositionConsCustForth2 = mainAgreementConsortiumBookmarkView?.PositionConsCustForth2,
                    PositionConsCustForth3 = mainAgreementConsortiumBookmarkView?.PositionConsCustForth3,
                    PositionConsCustForth4 = mainAgreementConsortiumBookmarkView?.PositionConsCustForth4,

                };


                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #region GetAddendumMainAgreementBookmarkView
        public AddendumMainAgreementBookmarkView GetAddendumMainAgreementBookmarkView(Guid refGuid, Guid bookmarkDocumentTransGuid)
        {
            try
            {
                ISharedService sharedService = new SharedService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IBookmarkDocumentTransRepo bookmarkDocumentTransRepo = new BookmarkDocumentTransRepo(db);

                BookmarkDocumentTrans bookmarkDocumentTrans = bookmarkDocumentTransRepo.GetBookmarkDocumentTransByIdNoTracking(bookmarkDocumentTransGuid);

                AddendumMainAgreementBookmarkView addendumMainAgreementBookmarkView = mainAgreementTableRepo.GetAddendumMainAgreementBookmarkView(refGuid);
                addendumMainAgreementBookmarkView.ApprovedDate1 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate1 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate2 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate3 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate4 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate5 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate6 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate7 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate8 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate9 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ContractDate10 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.DateOfCA1 = sharedService.DateToWord(addendumMainAgreementBookmarkView.RequestDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.DateOfCA2 = sharedService.DateToWord(addendumMainAgreementBookmarkView.RequestDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.DateOfCA3 = sharedService.DateToWord(addendumMainAgreementBookmarkView.RequestDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.DateOfCA4 = sharedService.DateToWord(addendumMainAgreementBookmarkView.RequestDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.DateOfCA5 = sharedService.DateToWord(addendumMainAgreementBookmarkView.RequestDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.DateOfCA6 = sharedService.DateToWord(addendumMainAgreementBookmarkView.RequestDate, TextConstants.TH, TextConstants.DMY);

                addendumMainAgreementBookmarkView.ApprovedDate2 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ApprovedDate3 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ApprovedDate4 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ApprovedDate5 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);
                addendumMainAgreementBookmarkView.ApprovedDate6 = sharedService.DateToWord(addendumMainAgreementBookmarkView.AgreementDate, TextConstants.TH, TextConstants.DMY);

                addendumMainAgreementBookmarkView.ManualNo1 = bookmarkDocumentTrans.ReferenceExternalId;

                return addendumMainAgreementBookmarkView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion
        #region Migration
        public void CreateMainAgreementTableCollection(IEnumerable<MainAgreementTableItemView> mainAgreementTableViews)
        {
            try
            {
                if(mainAgreementTableViews.Any())
                {
                    IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                    List<MainAgreementTable> mainAgreementTables = mainAgreementTableViews.ToMainAgreementTable().ToList();
                    mainAgreementTableRepo.ValidateAdd(mainAgreementTables);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(mainAgreementTables, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTable(SearchParameter search)
        {
            try
            {
                search.GetParentCondition(BookmarkDocumentCondition.BookmarkDocumentRefType);
                List<SearchCondition> searchCondition = SearchConditionService.GetBookmarkDocumentTemplateCondition(((int)BookmarkDocumentRefType.MainAgreement).ToString());
                search.Conditions.AddRange(searchCondition);
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return sysDropDownService.GetDropDownItemBookmarkDocumentTemplateTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public IEnumerable<SelectItem<BookmarkDocumentTemplateTableItemView>> GetDropDownItemBookmarkDocumentTemplateTableByGuarantorAgreement(SearchParameter search)
        {
            try
            {
                search.GetParentCondition(BookmarkDocumentCondition.BookmarkDocumentRefType);
                List<SearchCondition> searchCondition = SearchConditionService.GetBookmarkDocumentTemplateCondition(((int)BookmarkDocumentRefType.GuarantorAgreement).ToString());
                search.Conditions.AddRange(searchCondition);
                ISysDropDownService sysDropDownService = new SysDropDownService(db);
                return sysDropDownService.GetDropDownItemBookmarkDocumentTemplateTable(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}