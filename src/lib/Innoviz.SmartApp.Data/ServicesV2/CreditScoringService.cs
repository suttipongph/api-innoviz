using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICreditScoringService
	{

		CreditScoringItemView GetCreditScoringById(string id);
		CreditScoringItemView CreateCreditScoring(CreditScoringItemView creditScoringView);
		CreditScoringItemView UpdateCreditScoring(CreditScoringItemView creditScoringView);
		bool DeleteCreditScoring(string id);
	}
	public class CreditScoringService : SmartAppService, ICreditScoringService
	{
		public CreditScoringService(SmartAppDbContext context) : base(context) { }
		public CreditScoringService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CreditScoringService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditScoringService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CreditScoringService() : base() { }

		public CreditScoringItemView GetCreditScoringById(string id)
		{
			try
			{
				ICreditScoringRepo creditScoringRepo = new CreditScoringRepo(db);
				return creditScoringRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditScoringItemView CreateCreditScoring(CreditScoringItemView creditScoringView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				creditScoringView = accessLevelService.AssignOwnerBU(creditScoringView);
				ICreditScoringRepo creditScoringRepo = new CreditScoringRepo(db);
				CreditScoring creditScoring = creditScoringView.ToCreditScoring();
				creditScoring = creditScoringRepo.CreateCreditScoring(creditScoring);
				base.LogTransactionCreate<CreditScoring>(creditScoring);
				UnitOfWork.Commit();
				return creditScoring.ToCreditScoringItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditScoringItemView UpdateCreditScoring(CreditScoringItemView creditScoringView)
		{
			try
			{
				ICreditScoringRepo creditScoringRepo = new CreditScoringRepo(db);
				CreditScoring inputCreditScoring = creditScoringView.ToCreditScoring();
				CreditScoring dbCreditScoring = creditScoringRepo.Find(inputCreditScoring.CreditScoringGUID);
				dbCreditScoring = creditScoringRepo.UpdateCreditScoring(dbCreditScoring, inputCreditScoring);
				base.LogTransactionUpdate<CreditScoring>(GetOriginalValues<CreditScoring>(dbCreditScoring), dbCreditScoring);
				UnitOfWork.Commit();
				return dbCreditScoring.ToCreditScoringItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCreditScoring(string item)
		{
			try
			{
				ICreditScoringRepo creditScoringRepo = new CreditScoringRepo(db);
				Guid creditScoringGUID = new Guid(item);
				CreditScoring creditScoring = creditScoringRepo.Find(creditScoringGUID);
				creditScoringRepo.Remove(creditScoring);
				base.LogTransactionDelete<CreditScoring>(creditScoring);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
