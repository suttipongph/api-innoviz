using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICreditAppReqAssignmentService
	{

		CreditAppReqAssignmentItemView GetCreditAppReqAssignmentById(string id);
		CreditAppReqAssignmentItemView CreateCreditAppReqAssignment(CreditAppReqAssignmentItemView creditAppReqAssignmentView);
		CreditAppReqAssignmentItemView UpdateCreditAppReqAssignment(CreditAppReqAssignmentItemView creditAppReqAssignmentView);
		bool DeleteCreditAppReqAssignment(string id);
		IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItemCreditAppReqAssignmentByByCreditAppRequestTable(SearchParameter search);
		IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItemCreditAppReqAssignment(SearchParameter search);
	}
	public class CreditAppReqAssignmentService : SmartAppService, ICreditAppReqAssignmentService
	{
		public CreditAppReqAssignmentService(SmartAppDbContext context) : base(context) { }
		public CreditAppReqAssignmentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CreditAppReqAssignmentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppReqAssignmentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CreditAppReqAssignmentService() : base() { }

		public CreditAppReqAssignmentItemView GetCreditAppReqAssignmentById(string id)
		{
			try
			{
				ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
				return creditAppReqAssignmentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppReqAssignmentItemView CreateCreditAppReqAssignment(CreditAppReqAssignmentItemView creditAppReqAssignmentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				creditAppReqAssignmentView = accessLevelService.AssignOwnerBU(creditAppReqAssignmentView);
				ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
				CreditAppReqAssignment creditAppReqAssignment = creditAppReqAssignmentView.ToCreditAppReqAssignment();
				creditAppReqAssignment = creditAppReqAssignmentRepo.CreateCreditAppReqAssignment(creditAppReqAssignment);
				base.LogTransactionCreate<CreditAppReqAssignment>(creditAppReqAssignment);
				UnitOfWork.Commit();
				return creditAppReqAssignment.ToCreditAppReqAssignmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppReqAssignmentItemView UpdateCreditAppReqAssignment(CreditAppReqAssignmentItemView creditAppReqAssignmentView)
		{
			try
			{
				ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
				CreditAppReqAssignment inputCreditAppReqAssignment = creditAppReqAssignmentView.ToCreditAppReqAssignment();
				CreditAppReqAssignment dbCreditAppReqAssignment = creditAppReqAssignmentRepo.Find(inputCreditAppReqAssignment.CreditAppReqAssignmentGUID);
				dbCreditAppReqAssignment = creditAppReqAssignmentRepo.UpdateCreditAppReqAssignment(dbCreditAppReqAssignment, inputCreditAppReqAssignment);
				base.LogTransactionUpdate<CreditAppReqAssignment>(GetOriginalValues<CreditAppReqAssignment>(dbCreditAppReqAssignment), dbCreditAppReqAssignment);
				UnitOfWork.Commit();
				return dbCreditAppReqAssignment.ToCreditAppReqAssignmentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCreditAppReqAssignment(string item)
		{
			try
			{
				ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
				Guid creditAppReqAssignmentGUID = new Guid(item);
				CreditAppReqAssignment creditAppReqAssignment = creditAppReqAssignmentRepo.Find(creditAppReqAssignmentGUID);
				creditAppReqAssignmentRepo.Remove(creditAppReqAssignment);
				base.LogTransactionDelete<CreditAppReqAssignment>(creditAppReqAssignment);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region CreditAppReqAssignmentDropDown
		public IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItemCreditAppReqAssignmentByByCreditAppRequestTable(SearchParameter search)
		{
			try
			{
				ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
				search = search.GetParentCondition(CreditAppReqAssignmentCondition.CreditAppRequestTableGUID);
				SearchCondition searchCondition = SearchConditionService.GetCreditAppReqBusinessCollateralIsNewCondition(true);
				search.Conditions.Add(searchCondition);
				return creditAppReqAssignmentRepo.GetDropDownItem(search);

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<CreditAppReqAssignmentItemView>> GetDropDownItemCreditAppReqAssignment(SearchParameter search)
		{
			try
			{
				ICreditAppReqAssignmentRepo creditAppReqAssignmentRepo = new CreditAppReqAssignmentRepo(db);
				return creditAppReqAssignmentRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion CreditAppReqAssignmentDropDown

	}
}
