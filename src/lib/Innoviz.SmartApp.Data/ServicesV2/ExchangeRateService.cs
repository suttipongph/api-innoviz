using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IExchangeRateService
	{

		ExchangeRateItemView GetExchangeRateById(string id);
		ExchangeRateItemView CreateExchangeRate(ExchangeRateItemView exchangeRateView);
		ExchangeRateItemView UpdateExchangeRate(ExchangeRateItemView exchangeRateView);
		bool DeleteExchangeRate(string id);
		#region shared
		decimal GetExchangeRate(Guid currencyGuid, DateTime asOfDate);
        #endregion

    }
    public class ExchangeRateService : SmartAppService, IExchangeRateService
	{
		public ExchangeRateService(SmartAppDbContext context) : base(context) { }
		public ExchangeRateService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ExchangeRateService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ExchangeRateService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ExchangeRateService() : base() { }

		public ExchangeRateItemView GetExchangeRateById(string id)
		{
			try
			{
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				return exchangeRateRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExchangeRateItemView CreateExchangeRate(ExchangeRateItemView exchangeRateView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				exchangeRateView = accessLevelService.AssignOwnerBU(exchangeRateView);
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				ExchangeRate exchangeRate = exchangeRateView.ToExchangeRate();
				exchangeRate = exchangeRateRepo.CreateExchangeRate(exchangeRate);
				base.LogTransactionCreate<ExchangeRate>(exchangeRate);
				UnitOfWork.Commit();
				return exchangeRate.ToExchangeRateItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExchangeRateItemView UpdateExchangeRate(ExchangeRateItemView exchangeRateView)
		{
			try
			{
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				ExchangeRate inputExchangeRate = exchangeRateView.ToExchangeRate();
				ExchangeRate dbExchangeRate = exchangeRateRepo.Find(inputExchangeRate.ExchangeRateGUID);
				dbExchangeRate = exchangeRateRepo.UpdateExchangeRate(dbExchangeRate, inputExchangeRate);
				base.LogTransactionUpdate<ExchangeRate>(GetOriginalValues<ExchangeRate>(dbExchangeRate), dbExchangeRate);
				UnitOfWork.Commit();
				return dbExchangeRate.ToExchangeRateItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteExchangeRate(string item)
		{
			try
			{
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				Guid exchangeRateGUID = new Guid(item);
				ExchangeRate exchangeRate = exchangeRateRepo.Find(exchangeRateGUID);
				exchangeRateRepo.Remove(exchangeRate);
				base.LogTransactionDelete<ExchangeRate>(exchangeRate);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

        #region shared
		public decimal GetExchangeRate(Guid currencyGuid, DateTime asOfDate)
        {
            try
            {
				IExchangeRateRepo exchangeRateRepo = new ExchangeRateRepo(db);
				ExchangeRate exchangeRate = exchangeRateRepo.GetByCurrencyGUIDAndDateNoTracking(currencyGuid, asOfDate);
				if (exchangeRate == null)
                {
					return 1;
				}
                else
                {
					return exchangeRate.Rate;
				}
			}
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }



        #endregion

    }
}
