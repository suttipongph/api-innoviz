using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBlacklistStatusService
	{

		BlacklistStatusItemView GetBlacklistStatusById(string id);
		BlacklistStatusItemView CreateBlacklistStatus(BlacklistStatusItemView blacklistStatusView);
		BlacklistStatusItemView UpdateBlacklistStatus(BlacklistStatusItemView blacklistStatusView);
		bool DeleteBlacklistStatus(string id);
	}
	public class BlacklistStatusService : SmartAppService, IBlacklistStatusService
	{
		public BlacklistStatusService(SmartAppDbContext context) : base(context) { }
		public BlacklistStatusService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BlacklistStatusService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BlacklistStatusService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BlacklistStatusService() : base() { }

		public BlacklistStatusItemView GetBlacklistStatusById(string id)
		{
			try
			{
				IBlacklistStatusRepo blacklistStatusRepo = new BlacklistStatusRepo(db);
				return blacklistStatusRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BlacklistStatusItemView CreateBlacklistStatus(BlacklistStatusItemView blacklistStatusView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				blacklistStatusView = accessLevelService.AssignOwnerBU(blacklistStatusView);
				IBlacklistStatusRepo blacklistStatusRepo = new BlacklistStatusRepo(db);
				BlacklistStatus blacklistStatus = blacklistStatusView.ToBlacklistStatus();
				blacklistStatus = blacklistStatusRepo.CreateBlacklistStatus(blacklistStatus);
				base.LogTransactionCreate<BlacklistStatus>(blacklistStatus);
				UnitOfWork.Commit();
				return blacklistStatus.ToBlacklistStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BlacklistStatusItemView UpdateBlacklistStatus(BlacklistStatusItemView blacklistStatusView)
		{
			try
			{
				IBlacklistStatusRepo blacklistStatusRepo = new BlacklistStatusRepo(db);
				BlacklistStatus inputBlacklistStatus = blacklistStatusView.ToBlacklistStatus();
				BlacklistStatus dbBlacklistStatus = blacklistStatusRepo.Find(inputBlacklistStatus.BlacklistStatusGUID);
				dbBlacklistStatus = blacklistStatusRepo.UpdateBlacklistStatus(dbBlacklistStatus, inputBlacklistStatus);
				base.LogTransactionUpdate<BlacklistStatus>(GetOriginalValues<BlacklistStatus>(dbBlacklistStatus), dbBlacklistStatus);
				UnitOfWork.Commit();
				return dbBlacklistStatus.ToBlacklistStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBlacklistStatus(string item)
		{
			try
			{
				IBlacklistStatusRepo blacklistStatusRepo = new BlacklistStatusRepo(db);
				Guid blacklistStatusGUID = new Guid(item);
				BlacklistStatus blacklistStatus = blacklistStatusRepo.Find(blacklistStatusGUID);
				blacklistStatusRepo.Remove(blacklistStatus);
				base.LogTransactionDelete<BlacklistStatus>(blacklistStatus);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
