using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IMaritalStatusService
	{

		MaritalStatusItemView GetMaritalStatusById(string id);
		MaritalStatusItemView CreateMaritalStatus(MaritalStatusItemView maritalStatusView);
		MaritalStatusItemView UpdateMaritalStatus(MaritalStatusItemView maritalStatusView);
		bool DeleteMaritalStatus(string id);
	}
	public class MaritalStatusService : SmartAppService, IMaritalStatusService
	{
		public MaritalStatusService(SmartAppDbContext context) : base(context) { }
		public MaritalStatusService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public MaritalStatusService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public MaritalStatusService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public MaritalStatusService() : base() { }

		public MaritalStatusItemView GetMaritalStatusById(string id)
		{
			try
			{
				IMaritalStatusRepo maritalStatusRepo = new MaritalStatusRepo(db);
				return maritalStatusRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MaritalStatusItemView CreateMaritalStatus(MaritalStatusItemView maritalStatusView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				maritalStatusView = accessLevelService.AssignOwnerBU(maritalStatusView);
				IMaritalStatusRepo maritalStatusRepo = new MaritalStatusRepo(db);
				MaritalStatus maritalStatus = maritalStatusView.ToMaritalStatus();
				maritalStatus = maritalStatusRepo.CreateMaritalStatus(maritalStatus);
				base.LogTransactionCreate<MaritalStatus>(maritalStatus);
				UnitOfWork.Commit();
				return maritalStatus.ToMaritalStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public MaritalStatusItemView UpdateMaritalStatus(MaritalStatusItemView maritalStatusView)
		{
			try
			{
				IMaritalStatusRepo maritalStatusRepo = new MaritalStatusRepo(db);
				MaritalStatus inputMaritalStatus = maritalStatusView.ToMaritalStatus();
				MaritalStatus dbMaritalStatus = maritalStatusRepo.Find(inputMaritalStatus.MaritalStatusGUID);
				dbMaritalStatus = maritalStatusRepo.UpdateMaritalStatus(dbMaritalStatus, inputMaritalStatus);
				base.LogTransactionUpdate<MaritalStatus>(GetOriginalValues<MaritalStatus>(dbMaritalStatus), dbMaritalStatus);
				UnitOfWork.Commit();
				return dbMaritalStatus.ToMaritalStatusItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteMaritalStatus(string item)
		{
			try
			{
				IMaritalStatusRepo maritalStatusRepo = new MaritalStatusRepo(db);
				Guid maritalStatusGUID = new Guid(item);
				MaritalStatus maritalStatus = maritalStatusRepo.Find(maritalStatusGUID);
				maritalStatusRepo.Remove(maritalStatus);
				base.LogTransactionDelete<MaritalStatus>(maritalStatus);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
