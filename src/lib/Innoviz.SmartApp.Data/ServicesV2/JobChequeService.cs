using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IJobChequeService
	{

		JobChequeItemView GetJobChequeById(string id);
		JobChequeItemView CreateJobCheque(JobChequeItemView jobChequeView);
		JobChequeItemView UpdateJobCheque(JobChequeItemView jobChequeView);
		bool DeleteJobCheque(string id);
		JobChequeItemView InitialCreateJobChequeData(string id);
		
	}
	public class JobChequeService : SmartAppService, IJobChequeService
	{
		public JobChequeService(SmartAppDbContext context) : base(context) { }
		public JobChequeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public JobChequeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public JobChequeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public JobChequeService() : base() { }

		public JobChequeItemView GetJobChequeById(string id)
		{
			try
			{
				IJobChequeRepo jobChequeRepo = new JobChequeRepo(db);
				return jobChequeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		
			public JobChequeItemView InitialCreateJobChequeData(string id)
		{
			try
			{
				IMessengerJobTableService messengerJobTable = new MessengerJobTableService(db);
				var result = messengerJobTable.GetMessengerJobTableById(id);
				JobChequeItemView jobChequeItemView = new JobChequeItemView();
				jobChequeItemView.MessengerJobTableGUID = id;
				jobChequeItemView.MessengerJobTable_BuyerTableGUID = result.BuyerTableGUID;
				jobChequeItemView.MessengerJobTable_CustomerTableGUID = result.CustomerTableGUID;
				jobChequeItemView.MessengerJobTable_RefGUID = result.RefGUID;
				return jobChequeItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public JobChequeItemView CreateJobCheque(JobChequeItemView jobChequeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				jobChequeView = accessLevelService.AssignOwnerBU(jobChequeView);
				IJobChequeRepo jobChequeRepo = new JobChequeRepo(db);
				JobCheque jobCheque = jobChequeView.ToJobCheque();
				jobCheque = jobChequeRepo.CreateJobCheque(jobCheque);
				base.LogTransactionCreate<JobCheque>(jobCheque);
				UnitOfWork.Commit();
				return jobCheque.ToJobChequeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public JobChequeItemView UpdateJobCheque(JobChequeItemView jobChequeView)
		{
			try
			{
				IJobChequeRepo jobChequeRepo = new JobChequeRepo(db);
				JobCheque inputJobCheque = jobChequeView.ToJobCheque();
				JobCheque dbJobCheque = jobChequeRepo.Find(inputJobCheque.JobChequeGUID);
				dbJobCheque = jobChequeRepo.UpdateJobCheque(dbJobCheque, inputJobCheque);
				base.LogTransactionUpdate<JobCheque>(GetOriginalValues<JobCheque>(dbJobCheque), dbJobCheque);
				UnitOfWork.Commit();
				return dbJobCheque.ToJobChequeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteJobCheque(string item)
		{
			try
			{
				IJobChequeRepo jobChequeRepo = new JobChequeRepo(db);
				Guid jobChequeGUID = new Guid(item);
				JobCheque jobCheque = jobChequeRepo.Find(jobChequeGUID);
				jobChequeRepo.Remove(jobCheque);
				base.LogTransactionDelete<JobCheque>(jobCheque);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
