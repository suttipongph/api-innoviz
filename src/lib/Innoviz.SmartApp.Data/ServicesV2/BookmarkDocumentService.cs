using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBookmarkDocumentService
	{

		BookmarkDocumentItemView GetBookmarkDocumentById(string id);
		BookmarkDocumentItemView CreateBookmarkDocument(BookmarkDocumentItemView bookmarkDocumentView);
		BookmarkDocumentItemView UpdateBookmarkDocument(BookmarkDocumentItemView bookmarkDocumentView);
		bool DeleteBookmarkDocument(string id);
		IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByGuarantorAgreement(SearchParameter search);
		IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByMainAgreement(SearchParameter search);
		IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByCreditAppRequestTable(SearchParameter search);
		IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByAssignmentAgreement(SearchParameter search);
		IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByBusinessAGM(SearchParameter search);
	}
	public class BookmarkDocumentService : SmartAppService, IBookmarkDocumentService
	{
		public BookmarkDocumentService(SmartAppDbContext context) : base(context) { }
		public BookmarkDocumentService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BookmarkDocumentService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BookmarkDocumentService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BookmarkDocumentService() : base() { }

		public BookmarkDocumentItemView GetBookmarkDocumentById(string id)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				return bookmarkDocumentRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentItemView CreateBookmarkDocument(BookmarkDocumentItemView bookmarkDocumentView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				bookmarkDocumentView = accessLevelService.AssignOwnerBU(bookmarkDocumentView);
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				BookmarkDocument bookmarkDocument = bookmarkDocumentView.ToBookmarkDocument();
				bookmarkDocument = bookmarkDocumentRepo.CreateBookmarkDocument(bookmarkDocument);
				base.LogTransactionCreate<BookmarkDocument>(bookmarkDocument);
				UnitOfWork.Commit();
				return bookmarkDocument.ToBookmarkDocumentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BookmarkDocumentItemView UpdateBookmarkDocument(BookmarkDocumentItemView bookmarkDocumentView)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				BookmarkDocument inputBookmarkDocument = bookmarkDocumentView.ToBookmarkDocument();
				BookmarkDocument dbBookmarkDocument = bookmarkDocumentRepo.Find(inputBookmarkDocument.BookmarkDocumentGUID);
				dbBookmarkDocument = bookmarkDocumentRepo.UpdateBookmarkDocument(dbBookmarkDocument, inputBookmarkDocument);
				base.LogTransactionUpdate<BookmarkDocument>(GetOriginalValues<BookmarkDocument>(dbBookmarkDocument), dbBookmarkDocument);
				UnitOfWork.Commit();
				return dbBookmarkDocument.ToBookmarkDocumentItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBookmarkDocument(string item)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				Guid bookmarkDocumentGUID = new Guid(item);
				BookmarkDocument bookmarkDocument = bookmarkDocumentRepo.Find(bookmarkDocumentGUID);
				bookmarkDocumentRepo.Remove(bookmarkDocument);
				base.LogTransactionDelete<BookmarkDocument>(bookmarkDocument);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByGuarantorAgreement(SearchParameter search)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				List<SearchCondition> searchConditions =
					SearchConditionService.GetBookmarkDocumentByBookmarkDocumentRefTypeNotCondition
					(new string[] {
						Convert.ToInt32(BookmarkDocumentRefType.MainAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.CreditAppRequest).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.BusinessCollateralAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.AssignmentAgreement).ToString(),
					});
				foreach (SearchCondition item in searchConditions)
				{
					search.Conditions.Add(item);
				}
				return bookmarkDocumentRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByMainAgreement(SearchParameter search)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				List<SearchCondition> searchConditions =
					SearchConditionService.GetBookmarkDocumentByBookmarkDocumentRefTypeNotCondition
					(new string[] {
						Convert.ToInt32(BookmarkDocumentRefType.GuarantorAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.CreditAppRequest).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.BusinessCollateralAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.AssignmentAgreement).ToString(),
					});
				foreach (SearchCondition item in searchConditions)
				{
					search.Conditions.Add(item);
				}
				return bookmarkDocumentRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByBusinessAGM(SearchParameter search)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				List<SearchCondition> searchConditions =
					SearchConditionService.GetBookmarkDocumentByBookmarkDocumentRefTypeCondition
					(Convert.ToInt32(BookmarkDocumentRefType.BusinessCollateralAgreement).ToString());
				foreach (SearchCondition item in searchConditions)
				{
					search.Conditions.Add(item);
				}
				return bookmarkDocumentRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByCreditAppRequestTable(SearchParameter search)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				List<SearchCondition> searchConditions =
					SearchConditionService.GetBookmarkDocumentByBookmarkDocumentRefTypeNotCondition
					(new string[] {
						Convert.ToInt32(BookmarkDocumentRefType.GuarantorAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.MainAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.BusinessCollateralAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.AssignmentAgreement).ToString(),
					});
				foreach (SearchCondition item in searchConditions)
				{
					search.Conditions.Add(item);
				}
				return bookmarkDocumentRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public IEnumerable<SelectItem<BookmarkDocumentItemView>> GetDropDownItemBookmarkDocumentByAssignmentAgreement(SearchParameter search)
		{
			try
			{
				IBookmarkDocumentRepo bookmarkDocumentRepo = new BookmarkDocumentRepo(db);
				List<SearchCondition> searchConditions =
					SearchConditionService.GetBookmarkDocumentByBookmarkDocumentRefTypeNotCondition
					(new string[] {
						Convert.ToInt32(BookmarkDocumentRefType.GuarantorAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.CreditAppRequest).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.BusinessCollateralAgreement).ToString(),
						Convert.ToInt32(BookmarkDocumentRefType.MainAgreement).ToString(),
					});
				foreach (SearchCondition item in searchConditions)
				{
					search.Conditions.Add(item);
				}
				return bookmarkDocumentRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
