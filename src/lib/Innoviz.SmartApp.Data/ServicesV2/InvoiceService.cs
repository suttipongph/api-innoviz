using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IInvoiceService
    {

        InvoiceTable CreateInvoiceTable(InvoiceTable invoiceTable);
        InvoiceTable UpdateInvoiceTable(InvoiceTable invoiceTable);
        #region function
        #region shared genInvoiceLineText
        List<InvoiceLine> ReplaceInvoiceLineInvoiceText(List<InvoiceLine> invoiceLines);
        InvoiceLine ReplaceInvoiceLineInvoiceText(InvoiceLine invoiceLine);
        #endregion
        #region post invoice
        PostInvoiceResultView PostInvoice(List<InvoiceTable> invoiceTables, List<InvoiceLine> invoiceLines, RefType refType, Guid refGUID);
        #endregion
        InvoiceTable GetInvoiceTableByInvoiceId(string invoiceId);
        #endregion
        #region shared
        CalcSettleInvoiceAmountResultView CalcSettleInvoiceAmount(InvoiceTable invoiceTable, CustTrans custTrans, decimal settleAmountParam, decimal whtAmountParam, bool editWht);
        bool IsTaxUndue(Guid invoiceTableGuid);
        decimal GetSettleWHTBalance(Guid invoiceTableGuid);
        decimal GetSettleTaxBalance(Guid invoiceTableGuid);
        GenInvoiceResultView GenInvoice1LineSpecific(Invoice1LineSpecificParamView parm);
        GenInvoiceFromFreeTextInvoiceResult GenInvoiceFromFreeTextInvoice(List<FreeTextInvoiceTable> freeTextInvoiceTables);
        #endregion
    }
    public class InvoiceService : SmartAppService, IInvoiceService
    {
        public InvoiceService(SmartAppDbContext context) : base(context) { }
        public InvoiceService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public InvoiceService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public InvoiceService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public InvoiceService() : base() { }



        public InvoiceTable CreateInvoiceTable(InvoiceTable invoiceTable)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                invoiceTable = invoiceTableService.CreateInvoiceTable(invoiceTable);
                return invoiceTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InvoiceTable UpdateInvoiceTable(InvoiceTable invoiceTable)
        {
            try
            {
                IInvoiceTableService invoiceTableService = new InvoiceTableService(db);
                InvoiceTable result = invoiceTableService.UpdateInvoiceTable(invoiceTable);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region function
        #region shared genInvoiceLineText
        public List<InvoiceLine> ReplaceInvoiceLineInvoiceText(List<InvoiceLine> invoiceLines)
        {
            List<InvoiceLine> result = new List<InvoiceLine>();
            foreach (InvoiceLine item in invoiceLines)
            {
                result.Add(ReplaceInvoiceLineInvoiceText(item));
            }
            return result;
        }
        public InvoiceLine ReplaceInvoiceLineInvoiceText(InvoiceLine invoiceLine)
        {
            IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
            InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByIdNoTracking(invoiceLine.InvoiceRevenueTypeGUID);
            invoiceLine.InvoiceText = invoiceRevenueType.FeeInvoiceText;
            if (invoiceLine.InvoiceText.Contains("%1"))
            {
                invoiceLine.InvoiceText = invoiceLine.InvoiceText.Replace("%1","");
            }
            if (invoiceLine.InvoiceText.Contains("%2"))
            {
                invoiceLine.InvoiceText = invoiceLine.InvoiceText.Replace("%2", "");
            }
            if (invoiceLine.InvoiceText.Contains("%3"))
            {
                invoiceLine.InvoiceText = invoiceLine.InvoiceText.Replace("%3", "");
            }
            if (invoiceLine.InvoiceText.Contains("%4"))
            {
                invoiceLine.InvoiceText = invoiceLine.InvoiceText.Replace("%4", "");
            }
            if (invoiceLine.InvoiceText.Contains("%5"))
            {
                invoiceLine.InvoiceText = invoiceLine.InvoiceText.Replace("%5", "");
            }
            if (invoiceLine.InvoiceText.Contains("%6"))
            {
                invoiceLine.InvoiceText = invoiceLine.InvoiceText.Replace("%6", "");
            }
            return invoiceLine;
        }
        #endregion
        #endregion

        #region shared
        #region post invoice
        public PostInvoiceResultView PostInvoice(List<InvoiceTable> invoiceTables, List<InvoiceLine> invoiceLines, RefType refType, Guid refGUID)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                ICustTransService custTransService = new CustTransService(db);
                ICreditAppTransService creditAppTransService = new CreditAppTransService(db);
                ITaxInvoiceTableService taxInvoiceTableService = new TaxInvoiceTableService(db);
                IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
                IProcessTransService processTransService = new ProcessTransService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IRetentionTransService retentionTransService = new RetentionTransService(db);
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                PostInvoiceResultView PostInvoiceResultView = new PostInvoiceResultView();
                TaxInvoiceTable taxInvoiceTable = new TaxInvoiceTable();
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                invoiceTables.ForEach(invoice =>
                {
                    List<InvoiceLine> lines = invoiceLines.Where(w => w.InvoiceTableGUID == invoice.InvoiceTableGUID).ToList();
                    ex = PostInvoiceValidation(invoice, lines, ex);
                    //ex = InvoiceNumberSequenceValidation(invoice, ex);
                });
                ex = InvoiceNumberSequenceValidation(invoiceTables, ex);
                
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    invoiceTables.ForEach(invoice =>
                    {
                        List<InvoiceLine> lines = invoiceLines.Where(w => w.InvoiceTableGUID == invoice.InvoiceTableGUID).ToList();
                        PostInvoiceResultView.CustTranses.Add(CreateCustTransPostInvoice(invoice));
                        if (invoice.ProductInvoice)
                        {
                            CreditAppTrans creditAppTrans = GetCreditAppLineGuid(invoice);
                            PostInvoiceResultView.CreditAppTranses.Add(CreateCreditAppTransPostInvoice(invoice, creditAppTrans));
                        }
                        else
                        {
                            if (invoice.SuspenseInvoiceType == (int)SuspenseInvoiceType.Retention)
                            {
                                PostInvoiceResultView.RetentionTranses.Add(CreateRetentionTrans(invoice));
                            }
                            string taxInvoiceTableId = GenerateTaxInvoiceTablePostInvoice(invoice, lines);
                            if (!string.IsNullOrEmpty(taxInvoiceTableId))
                            {
                                taxInvoiceTable = CreateTaxInvoiceTablePostInvoice(invoice, taxInvoiceTableId, lines);
                                PostInvoiceResultView.TaxInvoiceTables.Add(taxInvoiceTable);
                                lines.ForEach(line =>
                                {
                                    if (line.TaxTableGUID != null)
                                    {
                                        TaxTable taxTable = taxTableRepo.GetTaxTableByIdNoTracking(line.TaxTableGUID.GetValueOrDefault());
                                        if (taxTable.PaymentTaxTableGUID == null)
                                        {
                                            PostInvoiceResultView.TaxInvoiceLines.Add(CreateTaxInvoiceLinePostInvoice(line, taxInvoiceTable.TaxInvoiceTableGUID));
                                        }
                                    }

                                });
                            }
                        }
                        PostInvoiceResultView.ProcessTranses.Add(CreateProcessTransPostInvoice(invoice, lines, taxInvoiceTable.TaxInvoiceTableGUID != Guid.Empty ? (Guid?)taxInvoiceTable.TaxInvoiceTableGUID : null, refType, refGUID));
                        DocumentStatus postedInvoice = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)InvoiceStatus.Posted).ToString());
                        invoice.DocumentStatusGUID = postedInvoice.DocumentStatusGUID;
                        invoice.ModifiedBy = db.GetUserName();
                        invoice.ModifiedDateTime = DateTime.Now;
                        PostInvoiceResultView.InvoiceTables.Add(invoice);
                        PostInvoiceResultView.InvoiceLines.AddRange(lines);
                    });

                }
                return PostInvoiceResultView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public SmartAppException PostInvoiceValidation(InvoiceTable invoiceTable, List<InvoiceLine> invoiceLines, SmartAppException ex)
        {
            try
            {
                ILedgerFiscalService ledgerFiscalService = new LedgerFiscalService(db);
                ITaxTableService taxTableService = new TaxTableService(db);
                IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);

                bool isPeriodStatusOpen = ledgerFiscalService.IsPeriodStatusOpen(db.GetCompanyFilter().StringToGuid(), invoiceTable.IssuedDate);
                // 1
                if (!isPeriodStatusOpen)
                {
                    ex.AddData("ERROR.90048", new string[] { invoiceTable.IssuedDate.DateToString() });
                }
                // 2
                if (invoiceTable.InvoiceAmount == 0)
                {
                    ex.AddData("ERROR.90049", new string[] { "LABEL.INVOICE_AMOUNT" });
                }
                // 3
                if ((invoiceTable.SuspenseInvoiceType == (int)SuspenseInvoiceType.None && invoiceTable.InvoiceAmount < 0 && (invoiceTable.CNReasonGUID == null
                    || string.IsNullOrEmpty(invoiceTable.OrigInvoiceId) || invoiceTable.OrigInvoiceAmount == 0)))
                {
                    ex.AddData("ERROR.00098");
                }
                if (invoiceTable.InvoiceAmount < 0 && invoiceTable.TaxAmount < 0 && (string.IsNullOrEmpty(invoiceTable.OrigTaxInvoiceId) || invoiceTable.OrigTaxInvoiceAmount == 0))
                {
                    ex.AddData("ERROR.90112");
                }
                // 4                
                var taxTableGroup = invoiceLines.Where(w => w.TaxTableGUID != null).GroupBy(item => item.TaxTableGUID.GetValueOrDefault(),
          (key) => new { TaxTableGUID = key }).ToList();
                if (taxTableGroup.Count > 1)
                {
                    ex.AddData("ERROR.90096");
                }
                else
                {
                    decimal taxValue = 0;
                    if (taxTableGroup.Count == 1)
                    {
                        // 4.1
                        taxValue = taxTableService.GetTaxValue(taxTableGroup.FirstOrDefault().Key, invoiceTable.IssuedDate);
                    }
                    if (taxTableGroup.Count == 0 || taxValue == 0)
                    {
                        if (invoiceTable.TaxAmount != 0)
                        {
                            ex.AddData("ERROR.90111", new string[] { "LABEL.TAX_AMOUNT", "LABEL.TAX_TABLE" });
                        }
                    }
                    else if (taxValue > 0)
                    {
                        if (invoiceTable.TaxAmount == 0)
                        {
                            ex.AddData("ERROR.90111", new string[] { "LABEL.TAX_AMOUNT", "LABEL.TAX_TABLE" });
                        }
                        if (ConditionService.IsNullOrEmpty(invoiceTable.TaxBranchId))
                        {
                            ex.AddData("ERROR.90042", new string[] { "LABEL.TAX_BRANCH_ID" });
                        }
                    }
                }

                // 5.1                
                var whtTaxTableGroup = invoiceLines.Where(w => w.WithholdingTaxTableGUID != null).GroupBy(item => item.WithholdingTaxTableGUID.GetValueOrDefault(),
          (key) => new { WithholdingTaxTableGUID = key }).ToList();

                if (whtTaxTableGroup.Count > 1)
                {
                    ex.AddData("ERROR.00097");
                }
                else
                {
                    decimal whtValue = 0;
                    if (whtTaxTableGroup.Count == 1)
                    {
                        // 5.2
                        whtValue = withholdingTaxTableService.GetWHTValue(whtTaxTableGroup.FirstOrDefault().Key, invoiceTable.IssuedDate);
                    }
                    if (whtTaxTableGroup.Count == 0 || whtValue == 0)
                    {
                        if (invoiceTable.WHTAmount != 0)
                        {
                            ex.AddData("ERROR.90111", new string[] { "LABEL.WITHHOLDING_TAX_AMOUNT", "LABEL.WITHHOLDING_TAX" });
                        }
                    }
                    else if (whtValue > 0)
                    {
                        if (invoiceTable.WHTAmount == 0)
                        {
                            ex.AddData("ERROR.90111", new string[] { "LABEL.WITHHOLDING_TAX_AMOUNT", "LABEL.WITHHOLDING_TAX" });
                        }
                    }

                }

                // 6
                InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTable.InvoiceTypeGUID);
                if (ConditionService.IsNullOrEmpty(invoiceType.ARLedgerAccount))
                {
                    ex.AddData("ERROR.90118", new string[] { "LABEL.AR_LEDGER_ACCOUNT", "LABEL.INVOICE_TYPE_ID", invoiceType.InvoiceTypeId });
                }

                //R07 7 & 8
                if (invoiceTable.ProductInvoice == true)
                {
                    if(invoiceTable.RefType == (int)RefType.WithdrawalLine)
                    {
                        IWithdrawalLineRepo withdrawalLineRepo = new WithdrawalLineRepo(db);
                        WithdrawalLine withdrawalLine = withdrawalLineRepo.GetWithdrawalLineByIdNoTracking(invoiceTable.RefGUID);
                        if (withdrawalLine != null)
                        {
                            IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                            WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTracking(withdrawalLine.WithdrawalTableGUID);
                            if(withdrawalTable.BuyerTableGUID != invoiceTable.BuyerTableGUID)
                                ex.AddData("ERROR.90168", new string[] { "LABEL.BUYER_ID", "LABEL.INVOICE", "LABEL.WITHDRAWAL" });
                        }
                    }
                    else if(invoiceTable.RefType == (int)RefType.PurchaseLine)
                    {
                        IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                        PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(invoiceTable.RefGUID);
                        if (purchaseLine != null)
                        {
                            if(purchaseLine.BuyerTableGUID != invoiceTable.BuyerTableGUID)
                                ex.AddData("ERROR.90168", new string[] { "LABEL.BUYER_ID", "LABEL.INVOICE", "LABEL.PURCHASE_LINE" });
                        }
                    }
                }
                
                // 9

                if(invoiceTable.ExchangeRate <= 0)
                {
                    ex.AddData("ERROR.90169", new string[] { "LABEL.EXCHANGE_RATE", "LABEL.INVOICE" });
                }

                return ex;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private SmartAppException InvoiceNumberSequenceValidation(List<InvoiceTable> invoiceTable, SmartAppException ex)
        {
            try
            {
                IInvoiceNumberSeqSetupRepo invoiceNumberSeqSetupRepo = new InvoiceNumberSeqSetupRepo(db);
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);

                List<InvoiceNumberSeqSetup> invoiceNumberSeqSetups = new List<InvoiceNumberSeqSetup>();
                
                var groupInvoice = invoiceTable.GroupBy(g => new { g.BranchGUID, g.InvoiceTypeGUID, g.ProductType });
                foreach (var group in groupInvoice)
                {
                    var invoiceNumberSeqSetup = invoiceNumberSeqSetupRepo.GetInvoiceNumberSeqSetupByBranchAndProductTypeAndInvoiceType(group.Key.BranchGUID, group.Key.ProductType, group.Key.InvoiceTypeGUID);
                    invoiceNumberSeqSetups.Add(invoiceNumberSeqSetup);
                }
                
                var invoiceNumberSeqSetupNumberSeqTableGuids = invoiceNumberSeqSetups.Select(s => s.NumberSeqTableGUID.HasValue ? s.NumberSeqTableGUID.Value : Guid.Empty).Distinct();
                var numberSeqTables = numberSeqTableRepo.GetNumberSeqTableByIdNoTracking(invoiceNumberSeqSetupNumberSeqTableGuids);
                if (numberSeqTables.Count() != invoiceNumberSeqSetupNumberSeqTableGuids.Count())
                {
                    SmartAppException exception = new SmartAppException("ERROR.ERROR");
                    exception.AddData("ERROR.90009");
                    throw exception;
                }
                
                var invoiceTableNumberSeqs =
                    (from invoice in invoiceTable
                     join invNumberSeqSetup in invoiceNumberSeqSetups
                     on new { invoice.BranchGUID, invoice.ProductType, invoice.InvoiceTypeGUID }
                     equals new { invNumberSeqSetup.BranchGUID, invNumberSeqSetup.ProductType, invNumberSeqSetup.InvoiceTypeGUID }
                     join numberseqTable in numberSeqTables
                     on invNumberSeqSetup.NumberSeqTableGUID equals numberseqTable.NumberSeqTableGUID
                     select new { invoice, numberseqTable });

                if(invoiceTableNumberSeqs.Any(a => a.numberseqTable.Manual && string.IsNullOrEmpty(a.invoice.InvoiceId)))
                {
                    ex.AddData("ERROR.90050", new string[] { "LABEL.INVOICE_ID" });
                }

                if(ex.MessageList.Count <= 1)
                {
                    // gen multiple number sequences
                    var numberSequences = invoiceTableNumberSeqs.Where(w => !w.numberseqTable.Manual).Select(s => new NumberSequences
                    {
                        Key = s.invoice.InvoiceTableGUID,
                        CompanyGUID = s.invoice.CompanyGUID,
                        BranchGUID = s.invoice.BranchGUID,
                        NumberSeqTableGUID = s.numberseqTable.NumberSeqTableGUID
                    }).ToList();
                    numberSequences = numberSequenceService.GetNumber(numberSequences);

                    var dbInvoices = invoiceTableRepo.GetInvoiceTableByInvoiceIdNoTracking(numberSequences.Select(s => s.GeneratedId));
                    if(dbInvoices.Count > 0)
                    {
                        ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.INVOICE_ID" });
                    }
                    else
                    {
                        foreach (var invoice in invoiceTable)
                        {
                            invoice.InvoiceId = numberSequences.Where(w => w.Key == invoice.InvoiceTableGUID).FirstOrDefault().GeneratedId;
                        }
                    }
                }

                return ex;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        
        private CustTrans CreateCustTransPostInvoice(InvoiceTable invoiceTable)
        {
            try
            {
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTable.InvoiceTypeGUID);
                CustTrans custTrans = new CustTrans()
                {
                    CustTransGUID = Guid.NewGuid(),
                    InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                    CustomerTableGUID = invoiceTable.CustomerTableGUID,
                    TransDate = invoiceTable.IssuedDate,
                    Description = string.Format("{0} {1}", invoiceType.Description, invoiceTable.DocumentId),
                    DueDate = invoiceTable.DueDate,
                    CurrencyGUID = invoiceTable.CurrencyGUID,
                    TransAmount = invoiceTable.InvoiceAmount,
                    TransAmountMST = invoiceTable.InvoiceAmountMST,
                    LastSettleDate = null,
                    SettleAmount = 0,
                    SettleAmountMST = 0,
                    ExchangeRate = invoiceTable.ExchangeRate,
                    CustTransStatus = (int)CustTransStatus.Open,
                    InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID,
                    BranchGUID = invoiceTable.BranchGUID,
                    ProductType = invoiceTable.ProductType,
                    CreditAppTableGUID = invoiceTable.CreditAppTableGUID,
                    CreditAppLineGUID = GetCreditAppLineGuid(invoiceTable).CreditAppLineGUID,
                    BuyerTableGUID = invoiceTable.BuyerTableGUID,
                    CompanyGUID = invoiceTable.CompanyGUID
                };
                return custTrans;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CalcSettleInvoiceAmountResultView CalcSettleInvoiceAmount(InvoiceTable invoiceTable, CustTrans custTrans, decimal settleAmountParam, decimal whtAmountParam, bool editWht)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
                CalcSettleInvoiceAmountResultView calcSettleInvoiceAmountResultView = new CalcSettleInvoiceAmountResultView();
                IEnumerable<PaymentHistory> paymentHistory = paymentHistoryRepo.GetPaymentHistoryByInvoiceTableGuid(invoiceTable.InvoiceTableGUID);
                decimal balanceAmount = custTrans.TransAmount - custTrans.SettleAmount;
                decimal settleAmount = settleAmountParam;
                decimal whtBalance = invoiceTable.WHTAmount == 0 ? 0 : invoiceTable.WHTAmount - paymentHistory.Sum(s => s.WHTAmount);
                decimal settleTaxBalance = invoiceTable.TaxAmount == 0 ? 0 : invoiceTable.TaxAmount - paymentHistory.Sum(s => s.PaymentTaxAmount);
                decimal maxSettleAmount = (balanceAmount - whtBalance).Round();
                decimal whtAmount = whtBalance == 0 ? 0 : (settleAmount * invoiceTable.WHTAmount) / (invoiceTable.InvoiceAmount - invoiceTable.WHTAmount);
                if (editWht == false && whtBalance != 0)
                {
                    if (settleAmount == maxSettleAmount)
                    {
                        whtAmount = whtBalance;
                    }
                    else
                    {
                        whtAmount = (settleAmount * invoiceTable.WHTAmount) / (invoiceTable.InvoiceAmount - invoiceTable.WHTAmount);
                    }
                }
                else
                {
                        whtAmount = whtAmountParam;
                }
                decimal settleInvoiceAmount = settleAmount + whtAmount;
                decimal settleTaxAmount = settleTaxBalance == 0 ? 0 : (settleInvoiceAmount * invoiceTable.TaxAmount) / invoiceTable.InvoiceAmount;
                settleTaxAmount = settleInvoiceAmount == balanceAmount ? settleTaxBalance : (settleTaxAmount).Round();
                calcSettleInvoiceAmountResultView.BalanceAmount = balanceAmount;
                calcSettleInvoiceAmountResultView.WHTAmount = whtAmount;
                calcSettleInvoiceAmountResultView.SettleAmount = settleAmount;
                calcSettleInvoiceAmountResultView.SettleInvoiceAmount = settleInvoiceAmount;
                calcSettleInvoiceAmountResultView.SettleTaxAmount = settleTaxAmount;
                return calcSettleInvoiceAmountResultView;

            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private CreditAppTrans CreateCreditAppTransPostInvoice(InvoiceTable invoiceTable, CreditAppTrans creditAppTransParam)
        {
            try
            {
                CreditAppTrans creditAppTrans = new CreditAppTrans()
                {
                    CreditAppTransGUID = Guid.NewGuid(),
                    CreditAppTableGUID = invoiceTable.CreditAppTableGUID.GetValueOrDefault(),
                    ProductType = invoiceTable.ProductType,
                    CustomerTableGUID = invoiceTable.CustomerTableGUID,
                    BuyerTableGUID = invoiceTable.BuyerTableGUID,
                    TransDate = invoiceTable.IssuedDate,
                    InvoiceAmount = invoiceTable.InvoiceAmount,
                    CreditDeductAmount = creditAppTransParam.CreditDeductAmount,
                    RefType = invoiceTable.RefType,
                    DocumentId = invoiceTable.DocumentId,
                    RefGUID = invoiceTable.RefGUID,
                    CreditAppLineGUID = creditAppTransParam.CreditAppLineGUID,
                    CompanyGUID = invoiceTable.CompanyGUID
                };
                return creditAppTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private CreditAppTrans GetCreditAppLineGuid(InvoiceTable invoiceTable)
        {
            try
            {
                CreditAppTrans creditAppTrans = new CreditAppTrans();
                if (invoiceTable.RefType == (int)RefType.WithdrawalLine)
                {
                    IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                    WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByInvoiceTableRefGuid(invoiceTable.RefGUID);
                    if (withdrawalTable != null)
                    {
                        creditAppTrans.CreditDeductAmount = withdrawalTable.WithdrawalAmount;
                        creditAppTrans.CreditAppLineGUID = withdrawalTable.CreditAppLineGUID;
                    }
                }
                else if (invoiceTable.RefType == (int)RefType.PurchaseLine)
                {
                    IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                    IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                    PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(invoiceTable.RefGUID);
                    if (purchaseLine != null)
                    {
                        creditAppTrans.CreditAppLineGUID = purchaseLine.CreditAppLineGUID;
                    }
                    creditAppTrans.CreditDeductAmount = purchaseTableService.GetPurchaseLineAmount(invoiceTable.RefGUID);
                }
                return creditAppTrans;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private TaxInvoiceTable CreateTaxInvoiceTablePostInvoice(InvoiceTable invoiceTable, string taxInvoiceId, List<InvoiceLine> invoiceLines)
        {
            try
            {
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus posted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)TaxInvoiceStatus.Posted).ToString());
                InvoiceLine invoiceLine = GetInvoiceLineForGenTaxTable(invoiceLines);

                TaxInvoiceTable taxInvoiceTable = new TaxInvoiceTable()
                {
                    TaxInvoiceTableGUID = Guid.NewGuid(),
                    TaxInvoiceId = taxInvoiceId,
                    IssuedDate = invoiceTable.IssuedDate,
                    DueDate = invoiceTable.DueDate,
                    CustomerName = invoiceTable.CustomerName,
                    InvoiceAmountBeforeTax = invoiceLine.TotalAmountBeforeTax,
                    InvoiceAmountBeforeTaxMST = invoiceLine.TotalAmountBeforeTax * invoiceTable.ExchangeRate,
                    TaxAmount = invoiceLine.TaxAmount,
                    TaxAmountMST = invoiceLine.TaxAmount * invoiceTable.ExchangeRate,
                    InvoiceAmount = invoiceLine.TotalAmountBeforeTax + invoiceLine.TaxAmount,
                    InvoiceAmountMST = invoiceTable.InvoiceAmountMST * invoiceTable.ExchangeRate,
                    Remark = invoiceTable.Remark,
                    InvoiceAddress1 = invoiceTable.InvoiceAddress1,
                    InvoiceAddress2 = invoiceTable.InvoiceAddress2,
                    MailingInvoiceAddress1 = invoiceTable.MailingInvoiceAddress1,
                    MailingInvoiceAddress2 = invoiceTable.MailingInvoiceAddress2,
                    OrigTaxInvoiceAmount = invoiceTable.OrigTaxInvoiceAmount,
                    ExchangeRate = invoiceTable.ExchangeRate,
                    CurrencyGUID = invoiceTable.CurrencyGUID,
                    CustomerTableGUID = invoiceTable.CustomerTableGUID,
                    CNReasonGUID = invoiceTable.CNReasonGUID,
                    DocumentStatusGUID = posted.DocumentStatusGUID,
                    InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID,
                    Dimension5GUID = invoiceTable.Dimension5GUID,
                    Dimension4GUID = invoiceTable.Dimension4GUID,
                    Dimension3GUID = invoiceTable.Dimension3GUID,
                    Dimension2GUID = invoiceTable.Dimension2GUID,
                    Dimension1GUID = invoiceTable.Dimension1GUID,
                    MethodOfPaymentGUID = invoiceTable.MethodOfPaymentGUID,
                    BranchGUID = invoiceTable.BranchGUID,
                    TaxBranchId = invoiceTable.TaxBranchId,
                    TaxBranchName = invoiceTable.TaxBranchName,
                    RefTaxInvoiceGUID = invoiceTable.RefTaxInvoiceGUID,
                    OrigTaxInvoiceId = invoiceTable.OrigTaxInvoiceId,
                    InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                    TaxInvoiceRefType = (int)TaxInvoiceRefType.Invoice,
                    TaxInvoiceRefGUID = invoiceTable.InvoiceTableGUID,
                    DocumentId = invoiceTable.DocumentId,
                    CompanyGUID = invoiceTable.CompanyGUID
                };
                return taxInvoiceTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        private InvoiceLine GetInvoiceLineForGenTaxTable(List<InvoiceLine> invoiceLines)
        {
            try
            {
                IInvoiceLineRepo taxTableRepo = new InvoiceLineRepo(db);
                IEnumerable<InvoiceLine> invoiceLinelist = taxTableRepo.GetInvoiceLinesForGenTaxTable(invoiceLines);

                InvoiceLine invoiceLine = invoiceLinelist.GroupBy(i => i.TaxTableGUID).Select(g => new InvoiceLine
                {
                    TotalAmountBeforeTax = g.Sum(tx => tx.TotalAmountBeforeTax),
                    TaxAmount = g.Sum(tx => tx.TaxAmount)

                }).FirstOrDefault();

                return invoiceLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private TaxInvoiceLine CreateTaxInvoiceLinePostInvoice(InvoiceLine invoiceLine, Guid taxInvoiceTableGuid)
        {
            try
            {
                ITaxInvoiceLineRepo taxInvoiceLineRepo = new TaxInvoiceLineRepo(db);
                int lineNum = 0;
                IEnumerable<TaxInvoiceLine> taxInvoiceLines = taxInvoiceLineRepo.GetTaxInvoiceLinesByTaxInvoiceTableGuid(taxInvoiceTableGuid);
                if (taxInvoiceLines.ToList().Count > 0)
                {
                    lineNum = taxInvoiceLines.ToList().OrderByDescending(o => o.LineNum).FirstOrDefault().LineNum + 1;
                }
                else
                {
                    lineNum = 1;
                }
                TaxInvoiceLine taxInvoiceLine = new TaxInvoiceLine()
                {
                    TaxInvoiceLineGUID = Guid.NewGuid(),
                    TaxInvoiceTableGUID = taxInvoiceTableGuid,
                    LineNum = lineNum,
                    InvoiceText = invoiceLine.InvoiceText,
                    Qty = invoiceLine.Qty,
                    UnitPrice = invoiceLine.UnitPrice,
                    TotalAmountBeforeTax = invoiceLine.TotalAmountBeforeTax,
                    TaxAmount = invoiceLine.TaxAmount,
                    TotalAmount = invoiceLine.TotalAmount,
                    InvoiceRevenueTypeGUID = invoiceLine.InvoiceRevenueTypeGUID,
                    Dimension1GUID = invoiceLine.Dimension1GUID,
                    Dimension2GUID = invoiceLine.Dimension2GUID,
                    Dimension3GUID = invoiceLine.Dimension3GUID,
                    Dimension4GUID = invoiceLine.Dimension4GUID,
                    Dimension5GUID = invoiceLine.Dimension5GUID,
                    ProdUnitGUID = invoiceLine.ProdUnitGUID,
                    TaxTableGUID = invoiceLine.TaxTableGUID.GetValueOrDefault(),
                    BranchGUID = invoiceLine.BranchGUID,
                    CompanyGUID = invoiceLine.CompanyGUID
                };
                return taxInvoiceLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private ProcessTrans CreateProcessTransPostInvoice(InvoiceTable invoiceTable, List<InvoiceLine> invoiceLines, Guid? taxInvoiceTableGuid, RefType refType, Guid refGUID)
        {
            try
            {
                IAttachmentService attachmentService = new AttachmentService(db);
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
                InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTable.InvoiceTypeGUID);
                InvoiceLine invoiceLineHasTaxTable = invoiceLines.Where(w => w.TaxTableGUID != null).FirstOrDefault();
                RefIdParm refIdParm = new RefIdParm 
                { 
                    RefGUID = refGUID.GuidNullToString(), 
                    RefType = (int)refType 
                };
                ProcessTrans processTrans = new ProcessTrans()
                {
                    ProcessTransGUID = Guid.NewGuid(),
                    TransDate = invoiceTable.IssuedDate,
                    CreditAppTableGUID = invoiceTable.CreditAppTableGUID,
                    CustomerTableGUID = invoiceTable.CustomerTableGUID,
                    Revert = false,
                    ProcessTransType = (int)ProcessTransType.Invoice,
                    Amount = invoiceTable.InvoiceAmount,
                    AmountMST = invoiceTable.InvoiceAmountMST,
                    ExchangeRate = invoiceTable.ExchangeRate,
                    TaxAmount = invoiceTable.TaxAmount,
                    TaxAmountMST = invoiceTable.TaxAmountMST,
                    ARLedgerAccount = invoiceType.ARLedgerAccount,
                    ProductType = invoiceTable.ProductType,
                    CurrencyGUID = invoiceTable.CurrencyGUID,
                    DocumentReasonGUID = null,
                    RefTaxInvoiceGUID = taxInvoiceTableGuid,
                    TaxTableGUID = invoiceLineHasTaxTable != null ? invoiceLineHasTaxTable.TaxTableGUID : null,
                    OrigTaxTableGUID = null,
                    RefType = (int)refType,
                    RefGUID = refGUID,
                    InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                    DocumentId = invoiceTable.DocumentId,
                    RefProcessTransGUID = null,
                    StagingBatchId = string.Empty,
                    StagingBatchStatus = (int)StagingBatchStatus.None,
                    SourceRefType = (int)refType,
                    SourceRefId = attachmentService.GetRefIdByRefTypeRefGUID(refIdParm),
                    PaymentHistoryGUID = null,
                    CompanyGUID = invoiceTable.CompanyGUID
                };
                return processTrans;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private RetentionTrans CreateRetentionTrans(InvoiceTable invoiceTable)
        {
            try
            {
                RetentionTrans retentionTrans = new RetentionTrans()
                {
                    RetentionTransGUID = Guid.NewGuid(),
                    CustomerTableGUID = invoiceTable.CustomerTableGUID,
                    CreditAppTableGUID = invoiceTable.CreditAppTableGUID.GetValueOrDefault(),
                    ProductType = invoiceTable.ProductType,
                    TransDate = invoiceTable.IssuedDate,
                    Amount = invoiceTable.InvoiceAmount * -1,
                    BuyerTableGUID = invoiceTable.BuyerTableGUID,
                    BuyerAgreementTableGUID = invoiceTable.BuyerAgreementTableGUID,
                    RefType = invoiceTable.RefType,
                    RefGUID = invoiceTable.RefGUID,
                    DocumentId = invoiceTable.DocumentId,
                    CompanyGUID = invoiceTable.CompanyGUID
                };
                return retentionTrans;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private string GenerateTaxInvoiceTablePostInvoice(InvoiceTable invoiceTable, List<InvoiceLine> invoiceLines)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                string taxInvoiceId = null;
                bool isTaxInvoiceDue = false;
                invoiceLines.ForEach(line =>
                {
                    if (line.TaxTableGUID != null)
                    {
                        TaxTable taxTable = taxTableRepo.GetTaxTableByIdNoTracking(line.TaxTableGUID.GetValueOrDefault());
                        if (taxTable.PaymentTaxTableGUID == null)
                        {
                            isTaxInvoiceDue = true;
                        }
                    }

                });
                if (isTaxInvoiceDue)
                {
                    INumberSeqSetupByProductTypeRepo numberSeqSetupByProductTypeRepo = new NumberSeqSetupByProductTypeRepo(db);
                    INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                    Guid taxInvoiceNumberSeqGuid = numberSeqSetupByProductTypeRepo.GetTaxInvoiceNumberSeqGUID(invoiceTable.CompanyGUID, invoiceTable.ProductType);
                    bool isManual = numberSequenceService.IsManualByNumberSeqTableGUID(taxInvoiceNumberSeqGuid);
                    if (isManual)
                    {
                        ex.AddData("ERROR.90050", new string[] { "LABEL.TAX_INVOICE_ID" });
                    }
                    else
                    {
                        ITaxInvoiceTableRepo taxInvoiceTableRepo = new TaxInvoiceTableRepo(db);
                        taxInvoiceId = numberSequenceService.GetNumber(invoiceTable.CompanyGUID, invoiceTable.BranchGUID, taxInvoiceNumberSeqGuid);
                        TaxInvoiceTable dbTaxInvoiceTable = taxInvoiceTableRepo.GetTaxInvoiceTableByTaxInvoiceId(taxInvoiceId);
                        if (dbTaxInvoiceTable != null)
                        {
                            ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.TAX_INVOICE_ID" });
                        }
                    }
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return taxInvoiceId;
                }
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsTaxUndue(Guid invoiceTableGuid)
        {
            try
            {
                IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
                IEnumerable<InvoiceLineItemView> invoiceLines = invoiceLineRepo.GetInvoiceLineItemViewByInvoiceTableGuid(invoiceTableGuid);
                return invoiceLines.ToList().Any(w => !string.IsNullOrEmpty(w.TaxTableGUID) && !string.IsNullOrEmpty(w.TaxTable_PaymentTaxTableGUID));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetSettleWHTBalance(Guid invoiceTableGuid)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
                InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceTableGuid);
                if (invoiceTable.WHTAmount != 0)
                {
                    IEnumerable<PaymentHistory> paymentHistories = paymentHistoryRepo.GetPaymentHistoryByInvoiceTableGuid(invoiceTableGuid);
                    return invoiceTable.WHTAmount - paymentHistories.Sum(s => s.WHTAmount);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public InvoiceTable GetInvoiceTableByInvoiceId(string invoiceId)
        {
            try
            {
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                return invoiceTableRepo.GetInvoiceTableByInvoiceIdNoTracking(invoiceId);
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetSettleTaxBalance(Guid invoiceTableGuid)
        {
            try
            {
                if (IsTaxUndue(invoiceTableGuid))
                {
                    IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                    IPaymentHistoryRepo paymentHistoryRepo = new PaymentHistoryRepo(db);
                    InvoiceTable invoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceTableGuid);
                    if (invoiceTable.TaxAmount != 0)
                    {
                        IEnumerable<PaymentHistory> paymentHistories = paymentHistoryRepo.GetPaymentHistoryByInvoiceTableGuid(invoiceTableGuid);
                        return invoiceTable.TaxAmount - paymentHistories.Sum(s => s.PaymentTaxAmount);
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region GenInvoice1LineSpecific
        private bool ValidateGenInvoice1LineSpecific(Invoice1LineSpecificParamView parm)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                if (!parm.AutoGenInvoiceRevenueType && parm.InvoiceRevenueTypeGUID == null)
                {
                    ex.AddData("ERROR.90042", "LABEL.SERVICE_FEE_TYPE_ID");
                }
                if (parm.RefGUID == null || parm.RefGUID == Guid.Empty)
                {
                    ex.AddData("ERROR.90121", "Reference GUID");
                }
                if (parm.IssuedDate == null || parm.IssuedDate == DateTime.MinValue)
                {
                    ex.AddData("ERROR.90121", "LABEL.ISSUED_DATE");
                }
                if (parm.DueDate == null || parm.DueDate == DateTime.MinValue)
                {
                    ex.AddData("ERROR.90121", "LABEL.DUE_DATE");
                }
                if (parm.CustomerTableGUID == null || parm.CustomerTableGUID == Guid.Empty)
                {
                    ex.AddData("ERROR.90121", "LABEL.CUSTOMER_ID");
                }
                if (parm.InvoiceTypeGUID == null || parm.InvoiceTypeGUID == Guid.Empty)
                {
                    ex.AddData("ERROR.90121", "LABEL.INVOICE_TYPE_ID");
                }
                if (parm.InvoiceAmount == 0)
                {
                    ex.AddData("ERROR.90121", "LABEL.INVOICE_AMOUNT");
                }
                if (parm.CompanyGUID == null || parm.CompanyGUID == Guid.Empty)
                {
                    ex.AddData("ERROR.90121", "LABEL.COMPANY_ID");
                }
                if (string.IsNullOrWhiteSpace(parm.DocumentId))
                {
                    ex.AddData("ERROR.90121", "LABEL.DOCUMENT_ID");
                }

                if (ex.MessageList.Count() > 1)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenInvoiceResultView GenInvoice1LineSpecific(Invoice1LineSpecificParamView parm)
        {
            try
            {
                GenInvoiceResultView result = new GenInvoiceResultView();
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                IInvoiceLineService invoiceLineService = new InvoiceLineService(db);

                InvoiceType parmInvoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(parm.InvoiceTypeGUID);

                if (ValidateGenInvoice1LineSpecific(parm))
                {
                    #region assign InvoiceRevenueTypeGUID
                    Guid? invoiceRevenueTypeGUID;
                    if (!parm.AutoGenInvoiceRevenueType)
                    {
                        invoiceRevenueTypeGUID = parm.InvoiceRevenueTypeGUID.Value;
                    }
                    else
                    {
                        invoiceRevenueTypeGUID = parmInvoiceType.AutoGenInvoiceRevenueTypeGUID;
                    }
                    #endregion
                    #region call Method098
                    InvoiceLineAmountParamView invoiceLineAmountParam = new InvoiceLineAmountParamView
                    {
                        IssuedDate = parm.IssuedDate,
                        InvoiceAmount = parm.InvoiceAmount,
                        IncludeTax = parm.IncludeTax,
                        InvoiceRevenueTypeGUID = invoiceRevenueTypeGUID,
                    };
                    InvoiceLineAmountResultView invoiceLineAmountResult = invoiceLineService.CalcInvoiceLineFromInvoiceRevenueType(invoiceLineAmountParam);
                    #endregion
                    #region create InvoiceTable & InvoiceLine
                    InvoiceTable invoiceTable = CreateInvoiceTableForGenInvoice1LineSpecific(parm, parmInvoiceType, invoiceLineAmountResult);
                    result.InvoiceTable = invoiceTable;

                    InvoiceLine invoiceLine = CreateInvoiceLineForGenInvoice1LineSpecific(parm, invoiceLineAmountResult, invoiceTable, invoiceRevenueTypeGUID);
                    result.InvoiceLine = invoiceLine;
                    #endregion
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private InvoiceTable CreateInvoiceTableForGenInvoice1LineSpecific(Invoice1LineSpecificParamView parm, InvoiceType parmInvoiceType,
                                                                        InvoiceLineAmountResultView invoiceLineAmountResult)
        {
            try
            {
                IExchangeRateService exchangeRateService = new ExchangeRateService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ICompanyRepo companyRepo = new CompanyRepo(db);

                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(parm.CustomerTableGUID);
                decimal exchangeRate = exchangeRateService.GetExchangeRate(customerTable.CurrencyGUID, parm.IssuedDate);

                AddressTrans invoiceAddress, mailingInvoiceAddress;
                if (parm.CreditAppTableGUID.HasValue)
                {
                    ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(parm.CreditAppTableGUID.Value);
                    
                    List<Guid> addressTransGuids = new List<Guid>();
                    if (creditAppTable.InvoiceAddressGUID.HasValue)
                        addressTransGuids.Add(creditAppTable.InvoiceAddressGUID.Value);
                    if (creditAppTable.MailingReceipAddressGUID.HasValue)
                        addressTransGuids.Add(creditAppTable.MailingReceipAddressGUID.Value);
                    
                    var addresses = addressTransRepo.GetAddressTransByIdNoTracking(addressTransGuids);
                    invoiceAddress = !creditAppTable.InvoiceAddressGUID.HasValue ? null :
                        addresses.Where(w => w.AddressTransGUID == creditAppTable.InvoiceAddressGUID.Value).FirstOrDefault();
                    mailingInvoiceAddress = !creditAppTable.MailingReceipAddressGUID.HasValue ? null :
                        addresses.Where(w => w.AddressTransGUID == creditAppTable.MailingReceipAddressGUID.Value).FirstOrDefault();
                }
                else
                {
                    invoiceAddress = (addressTransRepo.GetAddressTransByRefType((int)RefType.Customer, parm.CustomerTableGUID))
                                        .Where(w => w.Primary).FirstOrDefault();
                    mailingInvoiceAddress = invoiceAddress;
                }

                DocumentStatus invoiceDraftStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)InvoiceStatus.Draft).ToString());
                Company company = companyRepo.GetCompanyByIdNoTracking(parm.CompanyGUID);

                #region assign InvoiceTable values
                InvoiceTable result = new InvoiceTable
                {
                    InvoiceId = string.Empty,
                    IssuedDate = parm.IssuedDate,
                    DueDate = parm.DueDate,
                    CustomerTableGUID = parm.CustomerTableGUID,
                    CustomerName = customerTable.Name,
                    CurrencyGUID = customerTable.CurrencyGUID,
                    ExchangeRate = exchangeRate,
                    RefType = (int)parm.RefType,
                    RefGUID = parm.RefGUID,
                    ProductType = (int)parm.ProductType,
                    CreditAppTableGUID = parm.CreditAppTableGUID,
                    DocumentId = parm.DocumentId,
                    InvoiceAddress1 = (invoiceAddress != null ) ? invoiceAddress.Address1 : "",
                    InvoiceAddress2 = (invoiceAddress != null) ? invoiceAddress.Address2 : "",
                    TaxBranchId = (invoiceAddress != null) ? invoiceAddress.TaxBranchId : "",
                    TaxBranchName = (invoiceAddress != null) ? invoiceAddress.TaxBranchName : "",
                    MailingInvoiceAddress1 = (mailingInvoiceAddress != null) ? mailingInvoiceAddress.Address1 : "",
                    MailingInvoiceAddress2 = (mailingInvoiceAddress != null) ? mailingInvoiceAddress.Address2 : "",
                    DocumentStatusGUID = invoiceDraftStatus.DocumentStatusGUID,
                    InvoiceTypeGUID = parm.InvoiceTypeGUID,
                    ProductInvoice = parmInvoiceType.ProductInvoice,
                    SuspenseInvoiceType = parmInvoiceType.SuspenseInvoiceType,
                    BuyerTableGUID = parm.BuyerTableGUID,
                    BuyerAgreementTableGUID = parm.BuyerAgreementTableGUID,
                    InvoiceAmountBeforeTax = invoiceLineAmountResult.TotalAmountBeforeTax,
                    InvoiceAmountBeforeTaxMST = (invoiceLineAmountResult.TotalAmountBeforeTax * exchangeRate).Round(),
                    TaxAmount = invoiceLineAmountResult.TaxAmount,
                    TaxAmountMST = (invoiceLineAmountResult.TaxAmount * exchangeRate).Round(),
                    InvoiceAmount = invoiceLineAmountResult.TotalAmount,
                    InvoiceAmountMST = (invoiceLineAmountResult.TotalAmount * exchangeRate).Round(),
                    WHTAmount = invoiceLineAmountResult.WHTAmount,
                    WHTBaseAmount = invoiceLineAmountResult.WHTBaseAmount,
                    MarketingPeriod = 0,
                    AccountingPeriod = 0,
                    Remark = string.Empty,
                    OrigInvoiceAmount = 0.0m,
                    OrigTaxInvoiceAmount = 0.0m,
                    CNReasonGUID = null,
                    Dimension1GUID = parm.Dimension1GUID,
                    Dimension2GUID = parm.Dimension2GUID,
                    Dimension3GUID = parm.Dimension3GUID,
                    Dimension4GUID = parm.Dimension4GUID,
                    Dimension5GUID = parm.Dimension5GUID,
                    MethodOfPaymentGUID = parm.MethodOfPaymentGUID,
                    RefTaxInvoiceGUID = null,
                    OrigInvoiceId = string.Empty,
                    OrigTaxInvoiceId = string.Empty,
                    RefInvoiceGUID = null,
                    InvoiceTableGUID = Guid.NewGuid(),
                    CompanyGUID = parm.CompanyGUID,
                    BranchGUID = company.DefaultBranchGUID.Value,
                    ReceiptTempTableGUID = parm.ReceiptTempTableGUID
                };
                #endregion
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private InvoiceLine CreateInvoiceLineForGenInvoice1LineSpecific(Invoice1LineSpecificParamView parm, InvoiceLineAmountResultView invoiceLineAmountResult,
                                                                        InvoiceTable invoiceTable, Guid? invoiceRevenueTypeGUID)
        {
            try
            {
                InvoiceLine invoiceLine = new InvoiceLine
                {
                    InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                    LineNum = 1,
                    Qty = invoiceLineAmountResult.Qty,
                    UnitPrice = invoiceLineAmountResult.UnitPrice,
                    TotalAmountBeforeTax = invoiceLineAmountResult.TotalAmountBeforeTax,
                    TaxAmount = invoiceLineAmountResult.TaxAmount,
                    WHTAmount = invoiceLineAmountResult.WHTAmount,
                    TotalAmount = invoiceLineAmountResult.TotalAmount,
                    WHTBaseAmount = invoiceLineAmountResult.WHTBaseAmount,
                    InvoiceRevenueTypeGUID = invoiceRevenueTypeGUID.Value,
                    Dimension1GUID = parm.Dimension1GUID,
                    Dimension2GUID = parm.Dimension2GUID,
                    Dimension3GUID = parm.Dimension3GUID,
                    Dimension4GUID = parm.Dimension4GUID,
                    Dimension5GUID = parm.Dimension5GUID,
                    TaxTableGUID = invoiceLineAmountResult.TaxTableGUID,
                    WithholdingTaxTableGUID = invoiceLineAmountResult.WithholdingTaxTableGUID,
                    ProdUnitGUID = null,
                    InvoiceLineGUID = Guid.NewGuid(),
                    CompanyGUID = invoiceTable.CompanyGUID,
                    BranchGUID = invoiceTable.BranchGUID
                };
                invoiceLine = ReplaceInvoiceLineInvoiceText(invoiceLine);
                return invoiceLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region GenInvoiceFromFreeTextInvoice
        public bool ValidateGenInvoiceFromFreeTextInvoice(List<FreeTextInvoiceTable> freeTextInvoiceTables)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                // Validate No.1
                bool isFeeTextInvoiceTableInvoiceAmountZero = ConditionService.IsNotZero(freeTextInvoiceTables.Where(w => w.InvoiceAmount == 0).Count());
                // Validate No.2
                IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
                IEnumerable<Guid> invoiceRevenueTypeGUIDs = freeTextInvoiceTables.GroupBy(g => g.InvoiceRevenueTypeGUID).Select(s => s.Key);
                List<InvoiceRevenueType> invoiceRevenueTypesHasInterCompanyTable = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByIdNoTracking(invoiceRevenueTypeGUIDs).Where(w => w.IntercompanyTableGUID.HasValue).ToList();
                bool isInvoiceRevenueTypeHasInterCompanyTable = ConditionService.IsNotZero(invoiceRevenueTypesHasInterCompanyTable.Count());
                // Validate No.3
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                IEnumerable<Guid> invoiceTypeGUIDs  = freeTextInvoiceTables.GroupBy(g => g.InvoiceTypeGUID).Select(s => s.Key);
                List<InvoiceType> invoiceTypes = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTypeGUIDs);
                List<InvoiceType> invoiceTypesProductInvoiceEqualTrue = invoiceTypes.Where(w => w.ProductInvoice == true).ToList();
                bool isInvoiceTypesProductInvoiceEqualTrue = ConditionService.IsNotZero(invoiceTypesProductInvoiceEqualTrue.Count());
                // Validate No.4
                List<InvoiceType> invoiceTypesSuspenseInvoiceTypeNotEqualNone = invoiceTypes.Where(w => w.SuspenseInvoiceType != (int)SuspenseInvoiceType.None).ToList();
                bool isInvoiceTypesSuspenseInvoiceTypeNotEqualNone = ConditionService.IsNotZero(invoiceTypesSuspenseInvoiceTypeNotEqualNone.Count());

                if (isFeeTextInvoiceTableInvoiceAmountZero) // Validate No.1
                {
                    ex.AddData("ERROR.90049", new string[] { "LABEL.INVOICE_AMOUNT" });
                }
                if (isInvoiceRevenueTypeHasInterCompanyTable) // Validate No.2
                {
                    invoiceRevenueTypesHasInterCompanyTable.ForEach(invoiceRevenueType =>
                    {
                        ex.AddData("ERROR.90159", new string[] { "LABEL.INTERCOMPANY_ID", "LABEL.SERVICE_FEE_TYPE", invoiceRevenueType.RevenueTypeId });
                    });
                }
                if (isInvoiceTypesProductInvoiceEqualTrue) // Validate No.3
                {
                    invoiceTypesProductInvoiceEqualTrue.ForEach(invoiceType =>
                    {
                        ex.AddData("ERROR.90159", new string[] { "LABEL.PRODUCT_INVOICE", "LABEL.INVOICE_TYPE", invoiceType.InvoiceTypeId });
                    });
                }
                if (isInvoiceTypesSuspenseInvoiceTypeNotEqualNone) // Validate No.4
                {
                    invoiceTypesSuspenseInvoiceTypeNotEqualNone.ForEach(invoiceType =>
                    {
                        ex.AddData("ERROR.90159", new string[] { "LABEL.SUSPENSE_INVOICE_TYPE", "LABEL.INVOICE_TYPE", invoiceType.InvoiceTypeId });
                    });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public GenInvoiceFromFreeTextInvoiceResult GenInvoiceFromFreeTextInvoice(List<FreeTextInvoiceTable> freeTextInvoiceTables)
        {
            try
            {
                GenInvoiceFromFreeTextInvoiceResult genInvoiceFromFreeTextInvoiceResult = new GenInvoiceFromFreeTextInvoiceResult();
                if (ValidateGenInvoiceFromFreeTextInvoice(freeTextInvoiceTables))
                {
                    ICompanyRepo companyRepo = new CompanyRepo(db);
                    IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                    IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                    Company company = companyRepo.GetCompanyByIdNoTracking(freeTextInvoiceTables.Select(s => s.CompanyGUID).FirstOrDefault());
                    IEnumerable<Guid> invoiceTypeGUIDs = freeTextInvoiceTables.GroupBy(g => g.InvoiceTypeGUID).Select(s => s.Key);
                    List<InvoiceType> invoiceTypes = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTypeGUIDs);
                    Guid invoiceStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)InvoiceStatus.Draft).ToString()).DocumentStatusGUID;
                    List<FreeTextInvoiceTable> freeTextInvoiceTablesInvoiceAmountNotZero = freeTextInvoiceTables.Where(w => w.InvoiceAmount != 0).ToList();
                    freeTextInvoiceTablesInvoiceAmountNotZero.ForEach(freeTextInvoiceTable =>
                    {
                        InvoiceType invoiceType = invoiceTypes.Where(w => w.InvoiceTypeGUID == freeTextInvoiceTable.InvoiceTypeGUID).FirstOrDefault();
                        InvoiceTable invoiceTable = new InvoiceTable()
                        {
                            InvoiceId = null,
                            IssuedDate = freeTextInvoiceTable.IssuedDate,
                            DueDate = freeTextInvoiceTable.DueDate,
                            CustomerTableGUID = freeTextInvoiceTable.CustomerTableGUID,
                            CustomerName = freeTextInvoiceTable.CustomerName,
                            CurrencyGUID = freeTextInvoiceTable.CurrencyGUID,
                            ExchangeRate = freeTextInvoiceTable.ExchangeRate,
                            InvoiceAddress1 = freeTextInvoiceTable.InvoiceAddress1,
                            InvoiceAddress2 = freeTextInvoiceTable.InvoiceAddress2,
                            TaxBranchId = freeTextInvoiceTable.TaxBranchId,
                            TaxBranchName = freeTextInvoiceTable.TaxBranchName,
                            MailingInvoiceAddress1 = freeTextInvoiceTable.MailingInvoiceAddress1,
                            MailingInvoiceAddress2 = freeTextInvoiceTable.MailingInvoiceAddress2,
                            DocumentStatusGUID = invoiceStatusDraft,
                            InvoiceTypeGUID = freeTextInvoiceTable.InvoiceTypeGUID,
                            ProductInvoice = invoiceType.ProductInvoice,
                            SuspenseInvoiceType = invoiceType.SuspenseInvoiceType,
                            ProductType = freeTextInvoiceTable.ProductType,
                            CreditAppTableGUID = freeTextInvoiceTable.CreditAppTableGUID,
                            RefType = (int)RefType.FreeTextInvoice,
                            RefGUID = freeTextInvoiceTable.FreeTextInvoiceTableGUID,
                            DocumentId = freeTextInvoiceTable.FreeTextInvoiceId,
                            BuyerTableGUID = null,
                            BuyerInvoiceTableGUID = null,
                            BuyerAgreementTableGUID = null,
                            InvoiceAmountBeforeTax = freeTextInvoiceTable.InvoiceAmountBeforeTax,
                            InvoiceAmountBeforeTaxMST = (freeTextInvoiceTable.InvoiceAmountBeforeTax * freeTextInvoiceTable.ExchangeRate).Round(),
                            TaxAmount = freeTextInvoiceTable.TaxAmount,
                            TaxAmountMST = (freeTextInvoiceTable.TaxAmount*freeTextInvoiceTable.ExchangeRate).Round(),
                            InvoiceAmount = freeTextInvoiceTable.InvoiceAmount,
                            InvoiceAmountMST = (freeTextInvoiceTable.InvoiceAmount*freeTextInvoiceTable.ExchangeRate).Round(),
                            WHTAmount = freeTextInvoiceTable.WHTAmount,
                            WHTBaseAmount = freeTextInvoiceTable.WithholdingTaxTableGUID != null ? freeTextInvoiceTable.InvoiceAmountBeforeTax : 0,
                            MarketingPeriod = 0,
                            AccountingPeriod = 0,
                            Remark = freeTextInvoiceTable.Remark,
                            Dimension1GUID = freeTextInvoiceTable.Dimension1GUID,
                            Dimension2GUID = freeTextInvoiceTable.Dimension2GUID,
                            Dimension3GUID = freeTextInvoiceTable.Dimension3GUID,
                            Dimension4GUID = freeTextInvoiceTable.Dimension4GUID,
                            Dimension5GUID = freeTextInvoiceTable.Dimension5GUID,
                            MethodOfPaymentGUID = null,
                            RefTaxInvoiceGUID = null,
                            CNReasonGUID  = freeTextInvoiceTable.CNReasonGUID,
                            OrigInvoiceId = freeTextInvoiceTable.OrigInvoiceId,
                            OrigTaxInvoiceId = freeTextInvoiceTable.OrigTaxInvoiceId,
                            OrigInvoiceAmount = freeTextInvoiceTable.OrigInvoiceAmount,
                            OrigTaxInvoiceAmount = freeTextInvoiceTable.OrigTaxInvoiceAmount,
                            RefInvoiceGUID = null,
                            InvoiceTableGUID = Guid.NewGuid(),
                            CompanyGUID = freeTextInvoiceTable.CompanyGUID,
                            BranchGUID = company.DefaultBranchGUID.GetValueOrDefault()
                        };
                        
                        InvoiceLine invoiceLine = new InvoiceLine()
                        {
                            InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                            LineNum = 1,
                            Qty = 1,
                            UnitPrice = freeTextInvoiceTable.InvoiceAmountBeforeTax,
                            TotalAmountBeforeTax = freeTextInvoiceTable.InvoiceAmountBeforeTax,
                            TaxAmount = freeTextInvoiceTable.TaxAmount,
                            WHTAmount = freeTextInvoiceTable.WHTAmount,
                            TotalAmount = freeTextInvoiceTable.InvoiceAmount,
                            WHTBaseAmount = freeTextInvoiceTable.WithholdingTaxTableGUID != null ? freeTextInvoiceTable.InvoiceAmountBeforeTax : 0,
                            InvoiceRevenueTypeGUID  = freeTextInvoiceTable.InvoiceRevenueTypeGUID,
                            Dimension1GUID = freeTextInvoiceTable.Dimension1GUID,
                            Dimension2GUID = freeTextInvoiceTable.Dimension2GUID,
                            Dimension3GUID = freeTextInvoiceTable.Dimension3GUID,
                            Dimension4GUID = freeTextInvoiceTable.Dimension4GUID,
                            Dimension5GUID = freeTextInvoiceTable.Dimension5GUID,
                            TaxTableGUID = freeTextInvoiceTable.TaxTableGUID,
                            WithholdingTaxTableGUID = freeTextInvoiceTable.WithholdingTaxTableGUID,
                            ProdUnitGUID = null,
                            InvoiceText = freeTextInvoiceTable.InvoiceText,
                            InvoiceLineGUID = Guid.NewGuid(),
                            CompanyGUID = invoiceTable.CompanyGUID,
                            BranchGUID = invoiceTable.BranchGUID
                        };
                        invoiceLine = ReplaceInvoiceLineInvoiceText(invoiceLine); // Method018_GenInvoiceLine_InvoiceText
                        genInvoiceFromFreeTextInvoiceResult.InvoiceTable.Add(invoiceTable);
                        genInvoiceFromFreeTextInvoiceResult.InvoiceLine.Add(invoiceLine);
                    });
                }
                return genInvoiceFromFreeTextInvoiceResult;
            }
            catch (Exception e)
            {

                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion GenInvoiceFromFreeTextInvoice
        #endregion
    }
}
