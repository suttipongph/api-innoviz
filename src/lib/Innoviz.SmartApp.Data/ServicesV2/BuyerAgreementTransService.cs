using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IBuyerAgreementTransService
    {

        BuyerAgreementTransItemView GetBuyerAgreementTransById(string id);
        BuyerAgreementTransItemView CreateBuyerAgreementTrans(BuyerAgreementTransItemView buyerAgreementTransView);
        BuyerAgreementTransItemView UpdateBuyerAgreementTrans(BuyerAgreementTransItemView buyerAgreementTransView);
        bool DeleteBuyerAgreementTrans(string id);
        BuyerAgreementTransItemView GetBuyerAgreementTransInitialData(string refId, string refGUID, RefType refType);
        #region function
        CopyBuyerAgreementTransView GetCopyBuyerAgreementTransById(string guid);
        IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetBuyerAgreementByBuyerDropdown(SearchParameter search);
        bool GetCopyBuyerAgreementTransByRefTypeValidation(CopyBuyerAgreementTransView param);
        CopyFinancialStatementTransView CopyBuyerAgreementTrans(CopyBuyerAgreementTransView paramView);
        #endregion
        #region K2 Function
        IEnumerable<BuyerAgreementTrans> CopyBuyerAgreementTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, string owner = null, Guid? ownerBusinessUnitGUID = null);
        #endregion
        DateTime GetEndDate(IEnumerable<BuyerAgreementTrans> items);
        bool IsExistByWithdrawal(string withdrawalTableGUID);
        void CreateBuyerAgreementTransCollection(List<BuyerAgreementTransItemView> buyerAgreementTransItemViews);
    }
    public class BuyerAgreementTransService : SmartAppService, IBuyerAgreementTransService
    {
        public BuyerAgreementTransService(SmartAppDbContext context) : base(context) { }
        public BuyerAgreementTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public BuyerAgreementTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public BuyerAgreementTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public BuyerAgreementTransService() : base() { }

        public BuyerAgreementTransItemView GetBuyerAgreementTransById(string id)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                return buyerAgreementTransRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementTransItemView CreateBuyerAgreementTrans(BuyerAgreementTransItemView buyerAgreementTransView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                buyerAgreementTransView = accessLevelService.AssignOwnerBU(buyerAgreementTransView);
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                BuyerAgreementTrans buyerAgreementTrans = buyerAgreementTransView.ToBuyerAgreementTrans();
                buyerAgreementTrans = buyerAgreementTransRepo.CreateBuyerAgreementTrans(buyerAgreementTrans);
                base.LogTransactionCreate<BuyerAgreementTrans>(buyerAgreementTrans);
                UnitOfWork.Commit();
                return buyerAgreementTrans.ToBuyerAgreementTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementTransItemView UpdateBuyerAgreementTrans(BuyerAgreementTransItemView buyerAgreementTransView)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                BuyerAgreementTrans inputBuyerAgreementTrans = buyerAgreementTransView.ToBuyerAgreementTrans();
                BuyerAgreementTrans dbBuyerAgreementTrans = buyerAgreementTransRepo.Find(inputBuyerAgreementTrans.BuyerAgreementTransGUID);
                dbBuyerAgreementTrans = buyerAgreementTransRepo.UpdateBuyerAgreementTrans(dbBuyerAgreementTrans, inputBuyerAgreementTrans);
                base.LogTransactionUpdate<BuyerAgreementTrans>(GetOriginalValues<BuyerAgreementTrans>(dbBuyerAgreementTrans), dbBuyerAgreementTrans);
                UnitOfWork.Commit();
                return dbBuyerAgreementTrans.ToBuyerAgreementTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteBuyerAgreementTrans(string item)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                Guid buyerAgreementTransGUID = new Guid(item);
                BuyerAgreementTrans buyerAgreementTrans = buyerAgreementTransRepo.Find(buyerAgreementTransGUID);
                buyerAgreementTransRepo.Remove(buyerAgreementTrans);
                base.LogTransactionDelete<BuyerAgreementTrans>(buyerAgreementTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public BuyerAgreementTransItemView GetBuyerAgreementTransInitialData(string refId, string refGUID, RefType refType)
        {
            try
            {
                return new BuyerAgreementTransItemView
                {
                    BuyerAgreementTransGUID = new Guid().GuidNullToString(),
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region function
        public CopyBuyerAgreementTransView GetCopyBuyerAgreementTransById(string guid)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                CopyBuyerAgreementTransView copyBuyerAgreementTransView = buyerAgreementTransRepo.GetCopyBuyerAgreementTranByAppReqLineGUID(guid.StringToGuid());
                return copyBuyerAgreementTransView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<BuyerAgreementTableItemView>> GetBuyerAgreementByBuyerDropdown(SearchParameter search)
        {
            try
            {
                IBuyerAgreementTableRepo buyerAgreementTableRepo = new BuyerAgreementTableRepo(db);
                search = search.GetParentCondition(new string[] { BuyerAgreementTableCondition.BuyerTableGUID, BuyerAgreementTableCondition.CustomerTableGUID });
                return buyerAgreementTableRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool GetCopyBuyerAgreementTransByRefTypeValidation(CopyBuyerAgreementTransView param)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IEnumerable<BuyerAgreementTrans> buyerAgreementTrans = buyerAgreementTransRepo.GetCopyBuyerAgreementTransByReference(param.RefGUID.StringToGuid(), param.RefType);
                IEnumerable<BuyerAgreementLine> buyerAgreementLines = buyerAgreementLineRepo.GetBuyerAgreementLineByBuyerAgreementTableIdNoTracking(param.BuyerAgreementTableGUID.StringToGuid());
                if (buyerAgreementTrans.Any())
                {
                    ex.AddData("ERROR.90060", param.ResultLabel);
                }
                if(buyerAgreementLines.Count() == 0)
                {
                    ex.AddData("ERROR.90003");
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CopyFinancialStatementTransView CopyBuyerAgreementTrans(CopyBuyerAgreementTransView paramView)
        {
            try
            {
                IBuyerAgreementLineRepo buyerAgreementLineRepo = new BuyerAgreementLineRepo(db);
                CopyFinancialStatementTransView result = new CopyFinancialStatementTransView();
                NotificationResponse success = new NotificationResponse();
                IEnumerable<BuyerAgreementLine> buyerAgreementLines = buyerAgreementLineRepo.GetBuyerAgreementLineByBuyerAgreementTableIdNoTracking(paramView.BuyerAgreementTableGUID.StringToGuid());
                if (buyerAgreementLines.Count() > 0)
                {
                    List<BuyerAgreementTrans> createList = new List<BuyerAgreementTrans>();
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        try
                        {
                            buyerAgreementLines.ToList().ForEach(item =>
                            {
                                BuyerAgreementTrans createItem = new BuyerAgreementTrans()
                                {
                                    BuyerAgreementTransGUID = Guid.NewGuid(),
                                    BuyerAgreementTableGUID = item.BuyerAgreementTableGUID,
                                    BuyerAgreementLineGUID = item.BuyerAgreementLineGUID,
                                    RefType = paramView.RefType,
                                    RefGUID = paramView.RefGUID.StringToGuid(),
                                    CompanyGUID = db.GetCompanyFilter().StringToGuid()
                                };
                                createList.Add(createItem);
                            });
                            BulkInsert(createList);
                            UnitOfWork.Commit(transaction);
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            throw SmartAppUtil.AddStackTrace(e);
                        }
                    }
                }
                success.AddData("SUCCESS.90002", new string[] { paramView.ResultLabel[0] });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region K2 Function
        public IEnumerable<BuyerAgreementTrans> CopyBuyerAgreementTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, string owner = null, Guid? ownerBusinessUnitGUID = null)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                IEnumerable<BuyerAgreementTrans> buyerAgreementTrans = buyerAgreementTransRepo.GetCopyBuyerAgreementTransByReference(fromRefGUID, fromRefType);
                if (buyerAgreementTrans.Any())
                {
                    buyerAgreementTrans.ToList().ForEach(f =>
                    {
                        f.BuyerAgreementTransGUID = Guid.NewGuid();
                        f.RefGUID = toRefGUID;
                        f.RefType = toRefType;
                        f.Owner = owner;
                        f.OwnerBusinessUnitGUID = ownerBusinessUnitGUID;
                    });
                }
                return buyerAgreementTrans;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        public DateTime GetEndDate(IEnumerable<BuyerAgreementTrans> items)
        {
            try
            {
                var result = (from buyerAgreementTrans in items
                              join buyerAgreementTable in db.Set<BuyerAgreementTable>()
                              on buyerAgreementTrans.BuyerAgreementTableGUID equals buyerAgreementTable.BuyerAgreementTableGUID
                              where buyerAgreementTrans.RefType == (int)RefType.CreditAppLine
                              select buyerAgreementTable);
                

                return result.Any() ? result.Max(m => m.EndDate) : DateTime.MaxValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsExistByWithdrawal(string withdrawalTableGUID)
        {
            try
            {
                IBuyerAgreementTransRepo buyerAgreementTrans = new BuyerAgreementTransRepo(db);
                return buyerAgreementTrans.IsExistByWithdrawal(withdrawalTableGUID.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region migration
        public void CreateBuyerAgreementTransCollection(List<BuyerAgreementTransItemView> buyerAgreementTransItemViews)
        {
            try
            {
                if (buyerAgreementTransItemViews.Any())
                {
                    IBuyerAgreementTransRepo buyerAgreementTransRepo = new BuyerAgreementTransRepo(db);
                    List<BuyerAgreementTrans> buyerAgreementTrans = buyerAgreementTransItemViews.ToBuyerAgreementTrans().ToList();
                    buyerAgreementTransRepo.ValidateAdd(buyerAgreementTrans);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(buyerAgreementTrans, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion
    }
}
