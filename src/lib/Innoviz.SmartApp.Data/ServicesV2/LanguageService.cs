using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ILanguageService
	{

		LanguageItemView GetLanguageById(string id);
		LanguageItemView CreateLanguage(LanguageItemView languageView);
		LanguageItemView UpdateLanguage(LanguageItemView languageView);
		bool DeleteLanguage(string id);
	}
	public class LanguageService : SmartAppService, ILanguageService
	{
		public LanguageService(SmartAppDbContext context) : base(context) { }
		public LanguageService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public LanguageService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LanguageService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public LanguageService() : base() { }

		public LanguageItemView GetLanguageById(string id)
		{
			try
			{
				ILanguageRepo languageRepo = new LanguageRepo(db);
				return languageRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LanguageItemView CreateLanguage(LanguageItemView languageView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				languageView = accessLevelService.AssignOwnerBU(languageView);
				ILanguageRepo languageRepo = new LanguageRepo(db);
				Language language = languageView.ToLanguage();
				language = languageRepo.CreateLanguage(language);
				base.LogTransactionCreate<Language>(language);
				UnitOfWork.Commit();
				return language.ToLanguageItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LanguageItemView UpdateLanguage(LanguageItemView languageView)
		{
			try
			{
				ILanguageRepo languageRepo = new LanguageRepo(db);
				Language inputLanguage = languageView.ToLanguage();
				Language dbLanguage = languageRepo.Find(inputLanguage.LanguageGUID);
				dbLanguage = languageRepo.UpdateLanguage(dbLanguage, inputLanguage);
				base.LogTransactionUpdate<Language>(GetOriginalValues<Language>(dbLanguage), dbLanguage);
				UnitOfWork.Commit();
				return dbLanguage.ToLanguageItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteLanguage(string item)
		{
			try
			{
				ILanguageRepo languageRepo = new LanguageRepo(db);
				Guid languageGUID = new Guid(item);
				Language language = languageRepo.Find(languageGUID);
				languageRepo.Remove(language);
				base.LogTransactionDelete<Language>(language);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
