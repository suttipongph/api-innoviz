using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ILineOfBusinessService
	{

		LineOfBusinessItemView GetLineOfBusinessById(string id);
		LineOfBusinessItemView CreateLineOfBusiness(LineOfBusinessItemView lineOfBusinessView);
		LineOfBusinessItemView UpdateLineOfBusiness(LineOfBusinessItemView lineOfBusinessView);
		bool DeleteLineOfBusiness(string id);
	}
	public class LineOfBusinessService : SmartAppService, ILineOfBusinessService
	{
		public LineOfBusinessService(SmartAppDbContext context) : base(context) { }
		public LineOfBusinessService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public LineOfBusinessService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public LineOfBusinessService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public LineOfBusinessService() : base() { }

		public LineOfBusinessItemView GetLineOfBusinessById(string id)
		{
			try
			{
				ILineOfBusinessRepo lineOfBusinessRepo = new LineOfBusinessRepo(db);
				return lineOfBusinessRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LineOfBusinessItemView CreateLineOfBusiness(LineOfBusinessItemView lineOfBusinessView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				lineOfBusinessView = accessLevelService.AssignOwnerBU(lineOfBusinessView);
				ILineOfBusinessRepo lineOfBusinessRepo = new LineOfBusinessRepo(db);
				LineOfBusiness lineOfBusiness = lineOfBusinessView.ToLineOfBusiness();
				lineOfBusiness = lineOfBusinessRepo.CreateLineOfBusiness(lineOfBusiness);
				base.LogTransactionCreate<LineOfBusiness>(lineOfBusiness);
				UnitOfWork.Commit();
				return lineOfBusiness.ToLineOfBusinessItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public LineOfBusinessItemView UpdateLineOfBusiness(LineOfBusinessItemView lineOfBusinessView)
		{
			try
			{
				ILineOfBusinessRepo lineOfBusinessRepo = new LineOfBusinessRepo(db);
				LineOfBusiness inputLineOfBusiness = lineOfBusinessView.ToLineOfBusiness();
				LineOfBusiness dbLineOfBusiness = lineOfBusinessRepo.Find(inputLineOfBusiness.LineOfBusinessGUID);
				dbLineOfBusiness = lineOfBusinessRepo.UpdateLineOfBusiness(dbLineOfBusiness, inputLineOfBusiness);
				base.LogTransactionUpdate<LineOfBusiness>(GetOriginalValues<LineOfBusiness>(dbLineOfBusiness), dbLineOfBusiness);
				UnitOfWork.Commit();
				return dbLineOfBusiness.ToLineOfBusinessItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteLineOfBusiness(string item)
		{
			try
			{
				ILineOfBusinessRepo lineOfBusinessRepo = new LineOfBusinessRepo(db);
				Guid lineOfBusinessGUID = new Guid(item);
				LineOfBusiness lineOfBusiness = lineOfBusinessRepo.Find(lineOfBusinessGUID);
				lineOfBusinessRepo.Remove(lineOfBusiness);
				base.LogTransactionDelete<LineOfBusiness>(lineOfBusiness);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
