using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IInvoiceRevenueTypeService
	{

		InvoiceRevenueTypeItemView GetInvoiceRevenueTypeById(string id);
		InvoiceRevenueTypeItemView CreateInvoiceRevenueType(InvoiceRevenueTypeItemView invoiceRevenueTypeView);
		InvoiceRevenueTypeItemView UpdateInvoiceRevenueType(InvoiceRevenueTypeItemView invoiceRevenueTypeView);
		bool DeleteInvoiceRevenueType(string id);
		IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetInvoiceRevenueTypeByProductType(SearchParameter search);
		IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetInvoiceRevenueTypeByProductTypeAndDefaultNone(SearchParameter search);
		Decimal GetTaxValueByFeeTax(string taxTableGUID);

	}
	public class InvoiceRevenueTypeService : SmartAppService, IInvoiceRevenueTypeService
	{
		public InvoiceRevenueTypeService(SmartAppDbContext context) : base(context) { }
		public InvoiceRevenueTypeService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public InvoiceRevenueTypeService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public InvoiceRevenueTypeService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public InvoiceRevenueTypeService() : base() { }

		public InvoiceRevenueTypeItemView GetInvoiceRevenueTypeById(string id)
		{
			try
			{
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				return invoiceRevenueTypeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceRevenueTypeItemView CreateInvoiceRevenueType(InvoiceRevenueTypeItemView invoiceRevenueTypeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				invoiceRevenueTypeView = accessLevelService.AssignOwnerBU(invoiceRevenueTypeView);
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeView.ToInvoiceRevenueType();
				invoiceRevenueType = invoiceRevenueTypeRepo.CreateInvoiceRevenueType(invoiceRevenueType);
				base.LogTransactionCreate<InvoiceRevenueType>(invoiceRevenueType);
				UnitOfWork.Commit();
				return invoiceRevenueType.ToInvoiceRevenueTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public InvoiceRevenueTypeItemView UpdateInvoiceRevenueType(InvoiceRevenueTypeItemView invoiceRevenueTypeView)
		{
			try
			{
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				InvoiceRevenueType inputInvoiceRevenueType = invoiceRevenueTypeView.ToInvoiceRevenueType();
				InvoiceRevenueType dbInvoiceRevenueType = invoiceRevenueTypeRepo.Find(inputInvoiceRevenueType.InvoiceRevenueTypeGUID);
				dbInvoiceRevenueType = invoiceRevenueTypeRepo.UpdateInvoiceRevenueType(dbInvoiceRevenueType, inputInvoiceRevenueType);
				base.LogTransactionUpdate<InvoiceRevenueType>(GetOriginalValues<InvoiceRevenueType>(dbInvoiceRevenueType), dbInvoiceRevenueType);
				UnitOfWork.Commit();
				return dbInvoiceRevenueType.ToInvoiceRevenueTypeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteInvoiceRevenueType(string item)
		{
			try
			{
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				Guid invoiceRevenueTypeGUID = new Guid(item);
				InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeRepo.Find(invoiceRevenueTypeGUID);
				invoiceRevenueTypeRepo.Remove(invoiceRevenueType);
				base.LogTransactionDelete<InvoiceRevenueType>(invoiceRevenueType);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetInvoiceRevenueTypeByProductType(SearchParameter search)
		{
			try
			{
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				search = search.GetParentCondition(InvoiceRevenueTypeCondition.ProductType);
				return invoiceRevenueTypeRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<InvoiceRevenueTypeItemView>> GetInvoiceRevenueTypeByProductTypeAndDefaultNone(SearchParameter search)
		{
			try
			{
				IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
				var productType_Value = search.GetParentCondition(InvoiceRevenueTypeCondition.ProductType);
				List<string> productTypes = new List<string>() { ProductType.None.GetAttrValue().ToString() ,productType_Value.Conditions[0].Value };
				SearchCondition searchCondition = SearchConditionService.GetInvoiceRevenueTypeByProductTypeCondition(productTypes);
				search.Conditions = searchCondition.FirstToList();
				return invoiceRevenueTypeRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public Decimal GetTaxValueByFeeTax(string taxTableGUID)
		{
			try
			{
				ITaxValueRepo taxValueRepo = new TaxValueRepo(db);
				TaxValue taxValue = taxValueRepo.GetByTaxTableGUIDAndDateNoTracking(taxTableGUID.StringToGuid(), DateTime.Now,true);
				if(taxValue != null)
                {
					return taxValue.Value;
				}
				else
                {
					return 0;
                }
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
