using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IStagingTransTextService
	{

		StagingTransTextItemView GetStagingTransTextById(string id);
		StagingTransTextItemView CreateStagingTransText(StagingTransTextItemView stagingTransTextView);
		StagingTransTextItemView UpdateStagingTransText(StagingTransTextItemView stagingTransTextView);
		bool DeleteStagingTransText(string id);
	}
	public class StagingTransTextService : SmartAppService, IStagingTransTextService
	{
		public StagingTransTextService(SmartAppDbContext context) : base(context) { }
		public StagingTransTextService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public StagingTransTextService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public StagingTransTextService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public StagingTransTextService() : base() { }

		public StagingTransTextItemView GetStagingTransTextById(string id)
		{
			try
			{
				IStagingTransTextRepo stagingTransTextRepo = new StagingTransTextRepo(db);
				return stagingTransTextRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTransTextItemView CreateStagingTransText(StagingTransTextItemView stagingTransTextView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				stagingTransTextView = accessLevelService.AssignOwnerBU(stagingTransTextView);
				IStagingTransTextRepo stagingTransTextRepo = new StagingTransTextRepo(db);
				StagingTransText stagingTransText = stagingTransTextView.ToStagingTransText();
				stagingTransText = stagingTransTextRepo.CreateStagingTransText(stagingTransText);
				base.LogTransactionCreate<StagingTransText>(stagingTransText);
				UnitOfWork.Commit();
				return stagingTransText.ToStagingTransTextItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public StagingTransTextItemView UpdateStagingTransText(StagingTransTextItemView stagingTransTextView)
		{
			try
			{
				IStagingTransTextRepo stagingTransTextRepo = new StagingTransTextRepo(db);
				StagingTransText inputStagingTransText = stagingTransTextView.ToStagingTransText();
				StagingTransText dbStagingTransText = stagingTransTextRepo.Find(inputStagingTransText.StagingTransTextGUID);
				dbStagingTransText = stagingTransTextRepo.UpdateStagingTransText(dbStagingTransText, inputStagingTransText);
				base.LogTransactionUpdate<StagingTransText>(GetOriginalValues<StagingTransText>(dbStagingTransText), dbStagingTransText);
				UnitOfWork.Commit();
				return dbStagingTransText.ToStagingTransTextItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteStagingTransText(string item)
		{
			try
			{
				IStagingTransTextRepo stagingTransTextRepo = new StagingTransTextRepo(db);
				Guid stagingTransTextGUID = new Guid(item);
				StagingTransText stagingTransText = stagingTransTextRepo.Find(stagingTransTextGUID);
				stagingTransTextRepo.Remove(stagingTransText);
				base.LogTransactionDelete<StagingTransText>(stagingTransText);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
