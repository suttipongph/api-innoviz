using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IOwnerTransService
	{

		OwnerTransItemView GetOwnerTransById(string id);
		OwnerTransItemView CreateOwnerTrans(OwnerTransItemView ownerTransView);
		OwnerTransItemView UpdateOwnerTrans(OwnerTransItemView ownerTransView);
		bool DeleteOwnerTrans(string id);
		OwnerTransItemView GetOwnerTransInitialData(string refId, string refGUID, RefType refType);
		IEnumerable<OwnerTrans> GetActiveOwnerTrans(Guid refGUID, int refType);
		void CreateOwnerTransCollection(List<OwnerTransItemView> ownerTransItemViews);
		#region K2 function
		IEnumerable<OwnerTrans> CopyOwnerTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, string owner = null, Guid? ownerBusinessUnitGUID = null);
		#endregion
	}
    public class OwnerTransService : SmartAppService, IOwnerTransService
	{
		public OwnerTransService(SmartAppDbContext context) : base(context) { }
		public OwnerTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public OwnerTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public OwnerTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public OwnerTransService() : base() { }

		public OwnerTransItemView GetOwnerTransById(string id)
		{
			try
			{
				IOwnerTransRepo ownerTransRepo = new OwnerTransRepo(db);
				return ownerTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OwnerTransItemView CreateOwnerTrans(OwnerTransItemView ownerTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ownerTransView = accessLevelService.AssignOwnerBU(ownerTransView);
				IOwnerTransRepo ownerTransRepo = new OwnerTransRepo(db);
				OwnerTrans ownerTrans = ownerTransView.ToOwnerTrans();
				ownerTrans = ownerTransRepo.CreateOwnerTrans(ownerTrans);
				base.LogTransactionCreate<OwnerTrans>(ownerTrans);
				UnitOfWork.Commit();
				return ownerTrans.ToOwnerTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OwnerTransItemView UpdateOwnerTrans(OwnerTransItemView ownerTransView)
		{
			try
			{
				IOwnerTransRepo ownerTransRepo = new OwnerTransRepo(db);
				OwnerTrans inputOwnerTrans = ownerTransView.ToOwnerTrans();
				OwnerTrans dbOwnerTrans = ownerTransRepo.Find(inputOwnerTrans.OwnerTransGUID);
				dbOwnerTrans = ownerTransRepo.UpdateOwnerTrans(dbOwnerTrans, inputOwnerTrans);
				base.LogTransactionUpdate<OwnerTrans>(GetOriginalValues<OwnerTrans>(dbOwnerTrans), dbOwnerTrans);
				UnitOfWork.Commit();
				return dbOwnerTrans.ToOwnerTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteOwnerTrans(string item)
		{
			try
			{
				IOwnerTransRepo ownerTransRepo = new OwnerTransRepo(db);
				Guid ownerTransGUID = new Guid(item);
				OwnerTrans ownerTrans = ownerTransRepo.Find(ownerTransGUID);
				ownerTransRepo.Remove(ownerTrans);
				base.LogTransactionDelete<OwnerTrans>(ownerTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public OwnerTransItemView GetOwnerTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new OwnerTransItemView
				{
					OwnerTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<OwnerTrans> GetActiveOwnerTrans(Guid refGUID, int refType)
		{
			try
			{
				IOwnerTransRepo ownerTransRepo = new OwnerTransRepo(db);
				return ownerTransRepo.GetOwnerTransByReference(refGUID, refType).Where(w => w.InActive == false);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateOwnerTransCollection(List<OwnerTransItemView> ownerTransItemViews)
		{
			try
			{
				if (ownerTransItemViews.Any())
				{
					IOwnerTransRepo ownerTransRepo = new OwnerTransRepo(db);
					List<OwnerTrans> ownerTrans = ownerTransItemViews.ToOwnerTrans().ToList();
					ownerTransRepo.ValidateAdd(ownerTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(ownerTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#region K2 function
		public IEnumerable<OwnerTrans> CopyOwnerTrans(Guid fromRefGUID, Guid toRefGUID, int fromRefType, int toRefType, bool? fromInactive = null, string owner = null, Guid? ownerBusinessUnitGUID = null)
		{
			try
			{
				IOwnerTransRepo ownerTransRepo = new OwnerTransRepo(db);
				List<OwnerTrans> ownerTrans = ownerTransRepo.GetOwnerTransByReference(fromRefGUID, fromRefType).ToList();
				if (ownerTrans.Any())
				{
					if (fromInactive != null)
					{
						ownerTrans = ownerTrans.Where(w => w.InActive == fromInactive.Value).ToList();
					}
					int ordering = 1;
					ownerTrans.OrderBy(o => o.Ordering).ToList().ForEach(f =>
					{
						f.OwnerTransGUID = Guid.NewGuid();
						f.RefGUID = toRefGUID;
						f.RefType = toRefType;
						f.Ordering = ordering;
						f.Owner = owner;
						f.OwnerBusinessUnitGUID = ownerBusinessUnitGUID;
						ordering++;
					});
				}
				return ownerTrans;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
