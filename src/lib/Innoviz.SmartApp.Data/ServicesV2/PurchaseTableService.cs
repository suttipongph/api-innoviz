using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IPurchaseTableService
    {

        PurchaseLineItemView GetPurchaseLineById(string id);
        PurchaseLineItemView CreatePurchaseLine(PurchaseLineItemView purchaseLineView);
        PurchaseLineItemView UpdatePurchaseLine(PurchaseLineItemView purchaseLineView);
        bool DeletePurchaseLine(string id);
        PurchaseTableItemView GetPurchaseTableById(string id);
        PurchaseTableItemView CreatePurchaseTable(PurchaseTableItemView purchaseTableView);
        PurchaseTableItemView UpdatePurchaseTable(PurchaseTableItemView purchaseTableView);
        PurchaseTable UpdatePurchaseTable(PurchaseTable purchaseTable);
        bool DeletePurchaseTable(string id);
        Decimal CalcPurchaseNetPaidByPurchaseTable(Guid purchaseTableGUID);
        Decimal CalcInterestAmount(Guid CompanyGUID, decimal purchaseAmount, decimal totalInterestPct, decimal interestDay);
        IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemPurchaseTableStatus(SearchParameter search);
        PurchaseTableItemView GetPurchaseTableInitialDataByCreditAppTable(CreditAppTable creditAppTable);
        bool IsPurchaseLineByPurchaseTableEmpty(string purchaseTableGUID);
        RetentionConditionTransItemView GetRetentionConditionTransByPurchase(string creditAppTableGUID);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByPurchaseTable(string refGUID);
        bool IsManualByPurchaseTable(string companyId);
        PurchaseLineItemView GetPurchaseLineInitialDataByCreditAppTable(PurchaseTable purchaseTable, CreditAppTable creditAppTable);
        decimal GetCalcInterestAmount(PurchaseLineItemView purchaseLine);
        string GetInterestDate(PurchaseLineItemView purchaseLine);
        PurchaseLineItemView GetPurchaseLineByDueDate(PurchaseLineItemView purchaseLine);
        PurchaseLineItemView GetPurchaseLineByLinePurchasePct(PurchaseLineItemView input);
        PurchaseLineItemView GetPurchaseLineByBuyerInvoice(PurchaseLineItemView input);
        decimal CalcPurchaseLinePurchaseFeeAmount(PurchaseFeeCalculateBase purchaseFeeCalculateBase, decimal buyerInvoiceAmount, decimal purchaseFeePct, decimal linePurchaseAmount);
        decimal CalcPurchaseLineRetentionAmount(Guid purchaseTableGUID, decimal buyerInvoiceAmount, decimal linePurchaseAmount);
        PaymentDetailItemView GetPaymentDetailInitialData(string refGUID);
        #region Inquiry	Purchase line outstanding
        InquiryPurchaseLineOutstandingItemView GetInquiryPurchaseLineOutstandingById(string id);
        CollectionFollowUpItemView GetCollectionFollowUpInitialData(string refGUID);
        #endregion
        VerificationTransItemView GetVerificationTransInitialData(string refGUID);
        AccessModeView GetPurchaseTableAccessMode(string purchaseTableGUID, string isWorkflowMode);
        AccessModeView GetPurchaseTableAccessModeForPurchaseLine(string purchaseTableGUID, string isWorkflowMode);
        PurchaseLineItemView GetPurchaseLineRollBillInitialDataByCreditAppTable(PurchaseLineRollBillInitialDataParm input);
        InquiryRollbillPurchaseLineItemView GetInquiryRollBillPurchaseLineById(string id);
        AccessModeView GetPurchaseTablePurchaseAccessMode(string purchaselineGUID);
        #region k2
        void UpdatePurchaseTableK2(string purchaseTableGUID, string statusId, string processInstanceId,string documentReasonGUID);
        PurchaseTableItemView UpdatePurchaseWorkflowConditionK2(string purchaseTableGUID);
        #endregion
        #region shared
        decimal GetPurchaseLineAmount(Guid purchaseLineGuid);
        decimal GetPurchaseLineAmountBalance(Guid purchaseLineGuid);
        bool ValidatePurchaseAssignmentAmount(Guid purchaseTableGUID);
        bool ValidatePurchaseCustomerPDCAmountTolerance(Guid purchaseTableGUID);
        #endregion
        #region function
        #region Additional purchase
        AdditionalPurchaseParamView GetAdditionalPurchaseById(string purchaseLineGUID);
        bool ValidateAdditionalPurchaseMenu(PurchaseLineItemView model);
        bool ValidateMaxPurchasePct(string purchaseLineGUID, decimal inputPurchasePct);
        AdditionalPurchaseResultViewMap InitAdditionalPurchase(AdditionalPurchaseParamView parm);
        AdditionalPurchaseResultView AdditionalPurchase(AdditionalPurchaseParamView parm);

        #endregion
        #region send email
        SendPurchaseEmailView GetSendEmailPurchaseById(string purchaseTableGUID);
        Task<WorkflowResultView> ValidateAndStartWorkflowBySendEmailPurchase(SendPurchaseEmailView item);
        #endregion
        #region Post purchase
        PostPurchaseParamView GetPostPurchaseById(string purchaseTableGUID);
        PostPurchaseResultView PostPurchase(PostPurchaseParamView parm);
        PostPurchaseResultViewMap InitPostPurchase(Guid purchaseTableGUID);
        PostPurchaseResultViewMap InitPostPurchase(PurchaseTable purchaseTable);
        #endregion
        #region cancel purchase
        CancelPurchaseView GetCancelPurchaseById(string purchaseTableGUID);
        CancelPurchaseResultView CancelPurchase(CancelPurchaseView purchaseTable);
        #endregion
        #region update status rollbill purchase
        UpdateStatusRollbillPurchaseView GetUpdateStatusRollbillPurchaseById(string purchaseTableGUID);
        UpdateStatusRollbillPurchaseResultView UpdateStatusRollbillPurchase(UpdateStatusRollbillPurchaseView purchaseTable);
        #endregion
        #endregion

        #region WorkFlow
        WorkflowPurchaseView GetWorkFlowPurchaseById(string id);
        Task<WorkflowResultView> ValidateAndStartWorkflow(WorkflowPurchaseView item);
        Task<WorkflowResultView> ValidateAndActionWorkflow(WorkflowPurchaseView item);
        #endregion WorkFlow
        void CreatePurchaseTableCollection(IEnumerable<PurchaseTableItemView> purchaseTableItemView);
        void CreatePurchaseLineCollection(IEnumerable<PurchaseLineItemView> purchaseLineItemView);
        #region Report
        PrintPurchaseView GetPrintPurchaseById(string purchaseTableGUID);
        #endregion
        PrintPurchaseRollbillView GetPrintPurchaseRollbillById(string purchaseTableGUID);
        MemoTransItemView GetMemoTransInitialData(string refGUID);
    }
    public class PurchaseTableService : SmartAppService, IPurchaseTableService
    {
        public PurchaseTableService(SmartAppDbContext context) : base(context) { }
        public PurchaseTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public PurchaseTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public PurchaseTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public PurchaseTableService() : base() { }

        public PurchaseLineItemView GetPurchaseLineById(string id)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public InquiryRollbillPurchaseLineItemView GetInquiryRollBillPurchaseLineById(string id)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetInquiryRollBillPurchaseLineByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLineItemView CreatePurchaseLine(PurchaseLineItemView purchaseLineView)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                purchaseLineView = accessLevelService.AssignOwnerBU(purchaseLineView);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                PurchaseLine purchaseLine = purchaseLineView.ToPurchaseLine();
                purchaseLine = purchaseLineRepo.CreatePurchaseLine(purchaseLine);

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    // rollbill
                    if (purchaseLineView.RollBill && (purchaseLineView.OriginalPurchaseLineGUID != null || purchaseLineView.OriginalPurchaseLineGUID != ""))
                    {
                        IVerificationTransService verificationTransService = new VerificationTransService(db);
                        IEnumerable<VerificationTrans> verificationTrans = verificationTransService.CopyVerificationTransactions(purchaseLineView.OriginalPurchaseLineGUID, RefType.PurchaseLine, purchaseLine.PurchaseLineGUID.GuidNullToString(), RefType.PurchaseLine, purchaseLineView.PurchaseTable_PurchaseId);
                        base.BulkInsert(verificationTrans.ToList(), true);
                    }
                    base.LogTransactionCreate<PurchaseLine>(purchaseLine);
                    UnitOfWork.Commit(transaction);
                }

                return purchaseLine.ToPurchaseLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLineItemView UpdatePurchaseLine(PurchaseLineItemView purchaseLineView)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                PurchaseLine inputPurchaseLine = purchaseLineView.ToPurchaseLine();
                PurchaseLine dbPurchaseLine = purchaseLineRepo.Find(inputPurchaseLine.PurchaseLineGUID);
                dbPurchaseLine = purchaseLineRepo.UpdatePurchaseLine(dbPurchaseLine, inputPurchaseLine);
                base.LogTransactionUpdate<PurchaseLine>(GetOriginalValues<PurchaseLine>(dbPurchaseLine), dbPurchaseLine);
                UnitOfWork.Commit();
                return dbPurchaseLine.ToPurchaseLineItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeletePurchaseLine(string item)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                Guid purchaseLineGUID = new Guid(item);
                PurchaseLine purchaseLine = purchaseLineRepo.Find(purchaseLineGUID);

                // remove verificationTrans
                IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
                IEnumerable<VerificationTrans> verificationTransList = verificationTransRepo.GetVerificationTransByReferanceNoTracking(Convert.ToInt32(RefType.PurchaseLine), purchaseLine.PurchaseLineGUID);

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkDelete(new List<PurchaseLine>() { purchaseLine });
                    if (verificationTransList.Any())
                    {
                        this.BulkDelete(verificationTransList.ToList());
                    }
                    UnitOfWork.Commit(transaction);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTableItemView GetPurchaseTableById(string id)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTableItemView = purchaseTableRepo.GetByIdvw(id.StringToGuid());
                purchaseTableItemView.NetPaid = (purchaseTableItemView.Rollbill == false) ? CalcPurchaseNetPaidByPurchaseTable(purchaseTableItemView.PurchaseTableGUID.StringToGuid()) : 0;
                return purchaseTableItemView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTableItemView CreatePurchaseTable(PurchaseTableItemView purchaseTableView)
        {
            try
            {
                PurchaseTable purchaseTable = purchaseTableView.ToPurchaseTable();
                CreatePurchaseTable(purchaseTable);
                UnitOfWork.Commit();
                return purchaseTable.ToPurchaseTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTable CreatePurchaseTable(PurchaseTable purchaseTable)
        {
            try
            {
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                purchaseTable = accessLevelService.AssignOwnerBU(purchaseTable);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                GenPurchaseTableNumberSeqCode(purchaseTable);
                purchaseTable = purchaseTableRepo.CreatePurchaseTable(purchaseTable);
                base.LogTransactionCreate<PurchaseTable>(purchaseTable);
                return purchaseTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTableItemView UpdatePurchaseTable(PurchaseTableItemView purchaseTableView)
        {
            try
            {
                PurchaseTable inputPurchaseTable = purchaseTableView.ToPurchaseTable();
                PurchaseTable dbPurchaseTable = UpdatePurchaseTable(inputPurchaseTable);
                UnitOfWork.Commit();
                return dbPurchaseTable.ToPurchaseTableItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTable UpdatePurchaseTable(PurchaseTable inputPurchaseTable)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable dbPurchaseTable = purchaseTableRepo.Find(inputPurchaseTable.PurchaseTableGUID);
                dbPurchaseTable = purchaseTableRepo.UpdatePurchaseTable(dbPurchaseTable, inputPurchaseTable);
                base.LogTransactionUpdate<PurchaseTable>(GetOriginalValues<PurchaseTable>(dbPurchaseTable), dbPurchaseTable);
                return dbPurchaseTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeletePurchaseTable(string item)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                Guid purchaseTableGUID = new Guid(item);
                PurchaseTable purchaseTable = purchaseTableRepo.Find(purchaseTableGUID);
                purchaseTableRepo.Remove(purchaseTable);
                base.LogTransactionDelete<PurchaseTable>(purchaseTable);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemPurchaseTableStatus(SearchParameter search)
        {
            try
            {
                IDocumentService documentService = new DocumentService(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Purchase);
                search.Conditions.Add(searchCondition);
                return documentStatusRepo.GetDropDownItem(search);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region NumberSeq
        public bool IsManualByPurchaseTable(string companyId)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.Purchase);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void GenPurchaseTableNumberSeqCode(PurchaseTable purchaseTable)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                bool isManual = IsManualByPurchaseTable(purchaseTable.CompanyGUID.GuidNullToString());
                if (!isManual)
                {
                    NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(purchaseTable.CompanyGUID, ReferenceId.Purchase);
                    purchaseTable.PurchaseId = numberSequenceService.GetNumber(purchaseTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        #endregion

        public PurchaseTableItemView GetPurchaseTableInitialDataByCreditAppTable(CreditAppTable creditAppTable)
        {
            try
            {
                RetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
                RetentionConditionTransItemView retentionConditionTran = GetRetentionConditionTransByPurchase(creditAppTable.CreditAppTableGUID.GuidNullToString());

                DocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusId(Convert.ToInt32(PurchaseStatus.Draft).ToString());

                CustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(creditAppTable.CustomerTableGUID);

                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                string operReportSignatureGUID = null;
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(creditAppTable.CompanyGUID);
                if (companyParameter.OperReportSignatureGUID != null)
                {
                    EmployeeTable employeeTable = employeeTableRepo.GetByEmployeeGUIDNoTracking(companyParameter.OperReportSignatureGUID.Value);
                    if (employeeTable.InActive == false)
                    {
                        operReportSignatureGUID = employeeTable.EmployeeTableGUID.GuidNullToString();
                    }
                }
                return new PurchaseTableItemView
                {
                    PurchaseTableGUID = new Guid().GuidNullToString(),
                    RetentionFixedAmount = (retentionConditionTran != null) ? retentionConditionTran.RetentionAmount : 0,
                    ProductType = creditAppTable.ProductType,
                    CreditAppTableGUID = creditAppTable.CreditAppTableGUID.GuidNullToString(),
                    CreditAppTable_Values = SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description),
                    CustomerTableGUID = creditAppTable.CustomerTableGUID.GuidNullToString(),
                    CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                    PurchaseDate = DateTime.Now.DateToString(),
                    TotalInterestPct = creditAppTable.TotalInterestPct,
                    DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString(),
                    DocumentStatus_StatusId = documentStatus.StatusId,
                    RetentionCalculateBase = (retentionConditionTran != null) ? retentionConditionTran.RetentionCalculateBase : 0,
                    RetentionPct = (retentionConditionTran != null) ? retentionConditionTran.RetentionPct : 0,
                    Dimension1GUID = creditAppTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = creditAppTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = creditAppTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = creditAppTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = creditAppTable.Dimension5GUID.GuidNullToString(),
                    DiffChequeIssuedName = false,
                    OperReportSignatureGUID = operReportSignatureGUID
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public PurchaseLineItemView GetPurchaseLineInitialDataByCreditAppTable(PurchaseTable purchaseTable, CreditAppTable creditAppTable)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IProductSubTypeService productSubTypeService = new ProductSubTypeService(db);

                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking(purchaseTable.DocumentStatusGUID);

                PurchaseLine PreviousPurchaseLine = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTable.PurchaseTableGUID).OrderByDescending(o => o.CreatedDateTime).FirstOrDefault();
                DateTime? dueDate = (PreviousPurchaseLine != null ) ? (DateTime?) PreviousPurchaseLine.DueDate : null;
                return new PurchaseLineItemView
                {
                    PurchaseLineGUID = new Guid().GuidNullToString(),
                    PurchaseTableGUID = purchaseTable.PurchaseTableGUID.GuidNullToString(),
                    PurchaseTable_Values = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                    LineNum = GetPurchaseLineLastOrdering(purchaseTable.PurchaseTableGUID.GuidNullToString()),
                    ClosedForAdditionalPurchase = false,
                    Dimension1GUID = creditAppTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = creditAppTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = creditAppTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = creditAppTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = creditAppTable.Dimension5GUID.GuidNullToString(),
                    PurchaseTable_CustomerTableGUID = purchaseTable.CustomerTableGUID.GuidNullToString(),
                    PurchaseTable_CreditAppTableGUID = purchaseTable.CreditAppTableGUID.GuidNullToString(),
                    CreditLitmitType_ValidateBuyerAgreement = creditLimitType.ValidateBuyerAgreement,
                    PurchaseTable_PurchaseDate = purchaseTable.PurchaseDate.DateToString(),
                    PurchaseTable_PurchaseId = purchaseTable.PurchaseId,
                    DocumentStatus_StatusId = documentStatus.StatusId,
                    PurchaseTable_AdditionalPurchase = purchaseTable.AdditionalPurchase,
                    RollBill = purchaseTable.Rollbill,
                    RetentionCalculateBase = purchaseTable.RetentionCalculateBase,
                    RetentionPct = purchaseTable.RetentionPct,
                    DueDate = dueDate.DateNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLineItemView GetPurchaseLineRollBillInitialDataByCreditAppTable(PurchaseLineRollBillInitialDataParm input)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                ICustTransRepo custTransRepo = new CustTransRepo(db);

                PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(input.PurchaseLineGUID.StringToGuid());
                PurchaseTable parent_PurchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(input.PurchaseTableGUID.StringToGuid());
                MethodOfPayment methodOfPayment = (purchaseLine.MethodOfPaymentGUID != null) ? methodOfPaymentRepo.GetMethodOfPaymentByIdNoTracking(purchaseLine.MethodOfPaymentGUID.Value) : null;
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(purchaseLine.CompanyGUID);
                CustTrans custTrans = (purchaseLine.PurchaseLineInvoiceTableGUID != null) ? custTransRepo.GetCustTransByInvoiceTableNoTracking(purchaseLine.PurchaseLineInvoiceTableGUID.Value) : null;

                PurchaseLine refPurchaseLine = (purchaseLine.RefPurchaseLineGUID != null) ? purchaseLineRepo.GetPurchaseLineByIdNoTracking(purchaseLine.RefPurchaseLineGUID.Value) : null;
                PurchaseTable refPurchaseTable = (purchaseLine.RefPurchaseLineGUID != null) ? purchaseTableRepo.GetPurchaseTableByIdNoTracking(refPurchaseLine.PurchaseTableGUID) : null;

                //number of rollbill
                List<PurchaseLine> rollBillPurchaseLines = (purchaseLine.OriginalPurchaseLineGUID != null) ? purchaseLineRepo.GetPurchaseLineForMaxRollBillNoTracking(purchaseLine.OriginalPurchaseLineGUID.Value).ToList() : new List<PurchaseLine>();

                // onChange init
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(purchaseLine.CreditAppLineGUID);

                int rollbillInterestDay = Convert.ToInt32((parent_PurchaseTable.PurchaseDate - purchaseLine.InterestDate).TotalDays);
                decimal rollbillInterestPct = (rollbillInterestDay > companyParameter.FTMaxRollbillInterestDay) ? companyParameter.MaxInterestPct : parent_PurchaseTable.TotalInterestPct;

                decimal linePurchaseAmount = GetPurchaseLineAmountBalance(purchaseLine.PurchaseLineGUID);
                decimal outstanding = (custTrans != null) ? custTrans.TransAmount - custTrans.SettleAmount : 0;
                decimal rollbillInterestAmount = CalcInterestAmount(purchaseLine.CompanyGUID, linePurchaseAmount, rollbillInterestPct, rollbillInterestDay);

                PurchaseLine PreviousPurchaseLine = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(parent_PurchaseTable.PurchaseTableGUID).OrderByDescending(o => o.CreatedDateTime).FirstOrDefault();
                DateTime? dueDate = (PreviousPurchaseLine != null) ? (DateTime?)PreviousPurchaseLine.DueDate : null;

                return new PurchaseLineItemView
                {
                    RollbillPurchaseLineGUID = purchaseLine.PurchaseLineGUID.GuidNullToString(),
                    BuyerTableGUID = purchaseLine.BuyerTableGUID.GuidNullToString(),
                    CreditAppLineGUID = purchaseLine.CreditAppLineGUID.GuidNullToString(),
                    BuyerInvoiceTableGUID = purchaseLine.BuyerInvoiceTableGUID.GuidNullToString(),
                    BuyerInvoiceAmount = purchaseLine.BuyerInvoiceAmount,
                    BuyerAgreementTableGUID = purchaseLine.BuyerAgreementTableGUID.GuidNullToString(),
                    RefPurchaseLineGUID = null,
                    RefPurchaseLine_Values = (refPurchaseLine != null) ? SmartAppUtil.GetDropDownLabel(refPurchaseTable.PurchaseId, refPurchaseLine.LineNum.ToString()) : null,
                    PurchasePct = purchaseLine.PurchasePct,
                    LinePurchasePct = purchaseLine.LinePurchasePct,
                    PurchaseAmount = purchaseLine.PurchaseAmount,
                    LinePurchaseAmount = linePurchaseAmount,
                    PurchaseFeePct = purchaseLine.PurchaseFeePct,
                    PurchaseFeeCalculateBase = purchaseLine.PurchaseFeeCalculateBase,
                    AssignmentAgreementTableGUID = purchaseLine.AssignmentAgreementTableGUID.GuidNullToString(),
                    AssignmentAmount = purchaseLine.AssignmentAmount,
                    MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID.GuidNullToString(),
                    MethodOfPayment_Values = (methodOfPayment != null) ? SmartAppUtil.GetDropDownLabel(methodOfPayment.MethodOfPaymentId, methodOfPayment.Description) : null,
                    OriginalPurchaseLineGUID = (purchaseLine.OriginalPurchaseLineGUID != null) ? purchaseLine.OriginalPurchaseLineGUID.GuidNullToString() : purchaseLine.PurchaseLineGUID.GuidNullToString(),
                    OriginalInterestDate = purchaseLine.InterestDate.DateToString(),
                    RollbillInterestPct = rollbillInterestPct,
                    RollbillInterestAmount = rollbillInterestAmount,
                    NumberOfRollbill = (rollBillPurchaseLines.Count > 0) ? rollBillPurchaseLines.Max(m => m.NumberOfRollbill) + 1 : 1,
                    OutstandingBuyerInvoiceAmount = outstanding,
                    CreditAppLine_Values = creditAppLine.LineNum.ToString(),
                    RollbillInterestDay = rollbillInterestDay,
                    ReserveAmount = outstanding - linePurchaseAmount,
                    NetPurchaseAmount = linePurchaseAmount,
                    DueDate = dueDate.DateNullToString(),
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool IsPurchaseLineByPurchaseTableEmpty(string purchaseTableGUID)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IEnumerable<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTableGUID.StringToGuid());
                if (purchaseLines.Count() > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public RetentionConditionTransItemView GetRetentionConditionTransByPurchase(string creditAppTableGUID)
        {
            try
            {
                IRetentionConditionTransRepo retentionConditionTransRepo = new RetentionConditionTransRepo(db);
                IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                RetentionConditionTrans retentionConditionTrans = retentionConditionTransService.GetRetentionConditionTransByReferenceAndMethod(creditAppTableGUID.StringToGuid(), (int)RetentionDeductionMethod.Purchase).FirstOrDefault();
                if (retentionConditionTrans != null)
                {
                    return retentionConditionTrans.ToRetentionConditionTransItemView();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public int GetPurchaseLineLastOrdering(string purchaseTableGUID)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IEnumerable<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTableGUID.StringToGuid());
                if (purchaseLines.Count() > 0)
                {
                    return purchaseLines.Max(t => t.LineNum) + 1;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetCalcInterestAmount(PurchaseLineItemView purchaseLine)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(purchaseLine.PurchaseTableGUID.StringToGuid());
                decimal purchaseAmount = (purchaseLine.RefPurchaseLineGUID != null) ? purchaseLine.PurchaseAmount : purchaseLine.LinePurchaseAmount;
                return CalcInterestAmount(purchaseLine.CompanyGUID.StringToGuid(), purchaseAmount, purchaseTable.TotalInterestPct, purchaseLine.InterestDay);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public string GetInterestDate(PurchaseLineItemView purchaseLine)
        {
            try
            {
                IProductSubTypeService productSubTypeService = new ProductSubTypeService(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(purchaseLine.PurchaseTable_CreditAppTableGUID.StringToGuid());
                DateTime actualDueDate = productSubTypeService.FindActualDueDate(purchaseLine.DueDate.StringToDate(), creditAppTable.ProductSubTypeGUID);
                return productSubTypeService.FindActualInterestDate(actualDueDate, creditAppTable.ProductSubTypeGUID).DateToString();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public PurchaseLineItemView GetPurchaseLineByDueDate(PurchaseLineItemView purchaseLine)
        {
            try
            {
                DateTime interestDate = GetInterestDate(purchaseLine).StringToDate();
                purchaseLine.InterestDate = interestDate.DateToString();
                purchaseLine.InterestDay = Convert.ToInt32((interestDate - purchaseLine.PurchaseTable_PurchaseDate.StringToDate()).TotalDays);
                Decimal interestAmount = GetCalcInterestAmount(purchaseLine);
                purchaseLine.InterestAmount = interestAmount;

                // Purchase
                purchaseLine.PurchaseFeeAmount = CalcPurchaseLinePurchaseFeeAmount((PurchaseFeeCalculateBase)purchaseLine.PurchaseFeeCalculateBase, purchaseLine.BuyerInvoiceAmount, purchaseLine.PurchaseFeePct, purchaseLine.LinePurchaseAmount);
                return purchaseLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public PurchaseLineItemView GetPurchaseLineByLinePurchasePct(PurchaseLineItemView input)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(input.PurchaseTableGUID.StringToGuid());
                Guid postedPurchaseTableStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Posted).ToString()).DocumentStatusGUID;
                PurchaseLine purchaseLineByMaxPurchaseDate = purchaseLineRepo.GetPurchaseLineByMaxPurchaseDate(input.BuyerInvoiceTableGUID.StringToGuid());
                decimal purchaseLine_PurchasePct = (purchaseLineByMaxPurchaseDate != null) ? purchaseLineByMaxPurchaseDate.PurchasePct : 0;
                decimal purchaseLine_BuyerInvoiceAmount = (purchaseLineByMaxPurchaseDate != null) ? purchaseLineByMaxPurchaseDate.BuyerInvoiceAmount : 0;
                decimal purchaseLine_PurchaseAmount = (purchaseLineByMaxPurchaseDate != null) ? purchaseLineByMaxPurchaseDate.PurchaseAmount : 0;

                input.PurchasePct = input.LinePurchasePct + purchaseLine_PurchasePct;
                input.LinePurchaseAmount = (input.BuyerInvoiceAmount * (input.LinePurchasePct / 100)).Round();
                input.PurchaseAmount = input.LinePurchaseAmount + purchaseLine_PurchaseAmount;
                input.NetPurchaseAmount = input.LinePurchaseAmount - input.SettlePurchaseFeeAmount;

                // R05
                if (input.RefPurchaseLineGUID == null || input.RefPurchaseLineGUID == "" || input.RefPurchaseLineGUID == Guid.Empty.ToString()) {
                    input.InterestAmount = CalcInterestAmount(input.CompanyGUID.StringToGuid(), input.LinePurchaseAmount, purchaseTable.TotalInterestPct, input.InterestDay);
                }
                else
                {
                    input.InterestAmount = CalcInterestAmount(input.CompanyGUID.StringToGuid(), input.PurchaseAmount, purchaseTable.TotalInterestPct, input.InterestDay);
                }
                input.ReserveAmount = (input.RefPurchaseLineGUID != null) ? input.BuyerInvoiceAmount - input.PurchaseAmount : input.BuyerInvoiceAmount - input.LinePurchaseAmount;
                input.RetentionAmount = CalcPurchaseLineRetentionAmount(purchaseTable.PurchaseTableGUID, input.BuyerInvoiceAmount, input.LinePurchaseAmount);
                input.PurchaseFeeAmount = CalcPurchaseLinePurchaseFeeAmount((PurchaseFeeCalculateBase)input.PurchaseFeeCalculateBase, input.BuyerInvoiceAmount, input.PurchaseFeePct, input.LinePurchaseAmount);

                return input;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseLineItemView GetPurchaseLineByBuyerInvoice(PurchaseLineItemView input)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IBuyerInvoiceTableRepo buyerInvoiceTableRepo = new BuyerInvoiceTableRepo(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(input.PurchaseTableGUID.StringToGuid());
                BuyerInvoiceTable buyerInvoiceTable = buyerInvoiceTableRepo.GetBuyerInvoiceTableByIdNoTracking(input.BuyerInvoiceTableGUID.StringToGuid());
                PurchaseLine purchaseLineByMaxPurchaseDate = purchaseLineRepo.GetPurchaseLineByMaxPurchaseDate(input.BuyerInvoiceTableGUID.StringToGuid());

                CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(input.CreditAppLineGUID.StringToGuid());

                input.BuyerInvoiceAmount = buyerInvoiceTable.Amount;
                input.BuyerAgreementTableGUID = buyerInvoiceTable.BuyerAgreementTableGUID.GuidNullToString();
                input.PurchasePct = creditAppLine.MaxPurchasePct; // R10 remove input.LinePurchasePct + ((purchaseLineByMaxPurchaseDate != null) ? purchaseLineByMaxPurchaseDate.PurchasePct : 0);
                
                if(purchaseLineByMaxPurchaseDate != null)
                {
                    input.LinePurchasePct = (input.PurchasePct - purchaseLineByMaxPurchaseDate.PurchasePct) >= 0 ? input.PurchasePct - purchaseLineByMaxPurchaseDate.PurchasePct : 0;
                }
                else
                {
                    input.LinePurchasePct = 0;
                }

                input.LinePurchaseAmount = (input.BuyerInvoiceAmount * (input.LinePurchasePct / 100)).Round(2);
                input.AssignmentAmount = input.BuyerInvoiceAmount;
                input.PurchaseFeeAmount = CalcPurchaseLinePurchaseFeeAmount((PurchaseFeeCalculateBase)input.PurchaseFeeCalculateBase, input.BuyerInvoiceAmount, input.PurchaseFeePct, input.LinePurchaseAmount);
                input.RetentionAmount = CalcPurchaseLineRetentionAmount(input.PurchaseTableGUID.StringToGuid(), input.BuyerInvoiceAmount, input.LinePurchaseAmount);

                // R09
                decimal purchaseLine_PurchaseAmount = (purchaseLineByMaxPurchaseDate != null) ? purchaseLineByMaxPurchaseDate.PurchaseAmount : 0;
                input.PurchaseAmount = input.LinePurchaseAmount + purchaseLine_PurchaseAmount;
                input.NetPurchaseAmount = input.LinePurchaseAmount - input.SettlePurchaseFeeAmount;
                input.ReserveAmount = (input.RefPurchaseLineGUID != null) ? input.BuyerInvoiceAmount - input.PurchaseAmount : input.BuyerInvoiceAmount - input.LinePurchaseAmount;

                input.CollectionDate = buyerInvoiceTable.DueDate.DateToString(); ;
                if (input.RefPurchaseLineGUID == null || input.RefPurchaseLineGUID == "" || input.RefPurchaseLineGUID == Guid.Empty.ToString())
                {
                    input.InterestAmount = CalcInterestAmount(input.CompanyGUID.StringToGuid(), input.LinePurchaseAmount, purchaseTable.TotalInterestPct, input.InterestDay);
                }
                else
                {
                    input.InterestAmount = CalcInterestAmount(input.CompanyGUID.StringToGuid(), input.PurchaseAmount, purchaseTable.TotalInterestPct, input.InterestDay);
                }

                return input;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region CalcPurchaseNetPaidByPurchaseTable

        public Decimal CalcPurchaseNetPaidByPurchaseTable(Guid purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID);

                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IEnumerable<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTableGUID);

                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(RefType.PurchaseTable, purchaseTableGUID);

                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                List<InvoiceSettlementDetail> invoiceSettlementDetailByNone = invoiceSettlementDetailService.GetByReferenceAndSuspenseInvoiceType((int)RefType.PurchaseTable, purchaseTableGUID.GuidNullToString(), (int)SuspenseInvoiceType.None);
                List<InvoiceSettlementDetail> invoiceSettlementDetailBySuspenseAccount = invoiceSettlementDetailService.GetByReferenceAndSuspenseInvoiceType((int)RefType.PurchaseTable, purchaseTableGUID.GuidNullToString(), (int)SuspenseInvoiceType.SuspenseAccount);

                decimal sumLinePurchaseAmount = purchaseLines.Sum(t => t.LinePurchaseAmount);

                decimal sumSettlePurchaseFeeAmount = purchaseLines.Sum(t => t.SettlePurchaseFeeAmount);
                decimal sumSettleBillingFeeAmount = purchaseLines.Sum(t => t.SettleBillingFeeAmount);
                decimal sumSettleReceiptFeeAmount = purchaseLines.Sum(t => t.SettleReceiptFeeAmount);
                decimal sumSettleInterestAmount = purchaseLines.Sum(t => t.SettleInterestAmount);
                decimal sumRetentionAmount = purchaseLines.Sum(t => t.RetentionAmount);

                decimal sumSettleAmountByServiceFeeTrans = serviceFeeTrans.Sum(t => t.SettleAmount);
                decimal sumSettleAmountByInvoiceSettlement = invoiceSettlementDetailByNone.Sum(t => t.SettleAmount);
                decimal sumSettleAmountByInvoiceSettlementSuspenseAccount = invoiceSettlementDetailBySuspenseAccount.Sum(t => t.SettleAmount);

                decimal temp_1 = sumLinePurchaseAmount - (sumSettlePurchaseFeeAmount + sumSettleBillingFeeAmount + sumSettleReceiptFeeAmount +
                                                          sumSettleInterestAmount + sumRetentionAmount);
                decimal temp_2 = purchaseTable.RetentionFixedAmount;
                decimal temp_3 = sumSettleAmountByServiceFeeTrans;
                decimal temp_4 = sumSettleAmountByInvoiceSettlement;
                decimal temp_5 = sumSettleAmountByInvoiceSettlementSuspenseAccount;

                return temp_1 - (temp_2 + temp_3 + temp_4 + temp_5);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

        #region CalcInterestAmount
        public decimal CalcInterestAmount(Guid CompanyGUID, decimal purchaseAmount, decimal totalInterestPct, decimal interestDay)
        {
            try
            {
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(CompanyGUID);
                return ((purchaseAmount * (totalInterestPct / 100) * interestDay) / companyParameter.DaysPerYear).Round();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByPurchaseTable(string refGUID)
        {
            try
            {
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                string refId = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).PurchaseId;
                return serviceFeeTransService.GetServiceFeeTransInitialDataByPurchaseTable(refId, refGUID, Models.Enum.RefType.PurchaseTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public decimal CalcPurchaseLinePurchaseFeeAmount(PurchaseFeeCalculateBase purchaseFeeCalculateBase, decimal buyerInvoiceAmount, decimal purchaseFeePct, decimal linePurchaseAmount)
        {
            try
            {
                return purchaseFeeCalculateBase switch
                {
                    PurchaseFeeCalculateBase.InvoiceAmount => (buyerInvoiceAmount * purchaseFeePct / 100).Round(),
                    PurchaseFeeCalculateBase.PurchaseAmount => (linePurchaseAmount * purchaseFeePct / 100).Round(),
                    _ => 0,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public decimal CalcPurchaseLineRetentionAmount(Guid purchaseTableGUID, decimal buyerInvoiceAmount, decimal linePurchaseAmount)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(purchaseTableGUID);
                return CalcPurchaseLineRetentionAmount(purchaseTable, buyerInvoiceAmount, linePurchaseAmount);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal CalcPurchaseLineRetentionAmount(PurchaseTable purchaseTable, decimal buyerInvoiceAmount, decimal linePurchaseAmount)
        {
            try
            {
                return purchaseTable.RetentionCalculateBase switch
                {
                    (int)RetentionCalculateBase.InvoiceAmount => (buyerInvoiceAmount * (purchaseTable.RetentionPct / 100)).Round(),
                    (int)RetentionCalculateBase.PurchaseAmount => (linePurchaseAmount * (purchaseTable.RetentionPct / 100)).Round(),
                    _ => 0,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public PaymentDetailItemView GetPaymentDetailInitialData(string refGUID)
        {
            try
            {
                IPaymentDetailService paymentDetailService = new PaymentDetailService(db);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                string refId = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).PurchaseId;
                return paymentDetailService.GetPaymentDetailInitialData(refId, refGUID, Models.Enum.RefType.PurchaseTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public AccessModeView GetPurchaseTableAccessMode(string purchaseTableGUID, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID.StringToGuid());

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(purchaseTable.DocumentStatusGUID);

                bool isWorkflowModeCondition = Convert.ToBoolean(isWorkflowMode);

                bool condition1 = (documentStatus.StatusId == Convert.ToInt32(PurchaseStatus.Draft).ToString());
                bool condition2 = (documentStatus.StatusId == Convert.ToInt32(PurchaseStatus.WaitingForOperationStaff).ToString()) && isWorkflowModeCondition;

                return new AccessModeView
                {
                    CanCreate = condition1 || condition2,
                    CanDelete = condition1 || condition2,
                    CanView = condition1 || condition2,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetPurchaseTableAccessModeForPurchaseLine(string purchaseTableGUID, string isWorkflowMode)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID.StringToGuid());

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(purchaseTable.DocumentStatusGUID);

                bool isWorkflowModeCondition = Convert.ToBoolean(isWorkflowMode);

                bool condition1 = (documentStatus.StatusId == Convert.ToInt32(PurchaseStatus.Draft).ToString()) && (purchaseTable.AdditionalPurchase == false);
                bool condition2 = (documentStatus.StatusId == Convert.ToInt32(PurchaseStatus.WaitingForOperationStaff).ToString()) && isWorkflowModeCondition && (purchaseTable.AdditionalPurchase == false);

                return new AccessModeView
                {
                    CanCreate = condition1 || condition2,
                    CanDelete = condition1 || condition2,
                    CanView = true,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public VerificationTransItemView GetVerificationTransInitialData(string refGUID)
        {
            try
            {
                IVerificationTransService verificationTransService = new VerificationTransService(db);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(refGUID.StringToGuid());
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseLine.PurchaseTableGUID);
                string refId = purchaseLine.LineNum.ToString();
                string documentId = purchaseTable.PurchaseId;
                return verificationTransService.GetVerificationTransInitialData(refId, refGUID, Models.Enum.RefType.PurchaseLine, documentId);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AccessModeView GetPurchaseTablePurchaseAccessMode(string purchaselineGUID)
        {
            try
            {
                IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
               
                return new AccessModeView
                {
                    CanCreate = false,
                    CanDelete = false,
                    CanView = true,
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public MemoTransItemView GetMemoTransInitialData(string refGUID)
        {
            try
            {
                IMemoTransService memoTransService = new MemoTransService(db);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                string refId = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid()).PurchaseId;
                return memoTransService.GetMemoTransInitialData(refId, refGUID, Models.Enum.RefType.PurchaseTable);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region Inquiry	Purchase line outstanding
        public InquiryPurchaseLineOutstandingItemView GetInquiryPurchaseLineOutstandingById(string id)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                return purchaseLineRepo.GetInquiryPurchaseLineOutstandingByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public CollectionFollowUpItemView GetCollectionFollowUpInitialData(string refGUID)
        {
            try
            {
                ICollectionFollowUpService collectionFollowUpService = new CollectionFollowUpService(db);
                return collectionFollowUpService.GetCollectionFollowUpInitialDataByInquiryPurcaseLineOutstanding(refGUID.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region k2 method
        public void UpdatePurchaseTableK2(string purchaseTableGUID, string statusId, string processInstanceId,string documentReasonGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTable = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());

                // validate
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                if (processInstanceId == null || processInstanceId == "0" || processInstanceId == "")
                {
                    ex.AddData("ERROR.90128");
                }
                else if (purchaseTable.ProcessInstanceId != 0)
                {
                    if (Convert.ToInt32(processInstanceId) != purchaseTable.ProcessInstanceId)
                    {
                        ex.AddData("ERROR.90094");
                    }
                }

                if (statusId == null || statusId == "")
                {
                    ex.AddData("ERROR.90130");
                    throw ex;
                }
                else
                {
                    IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                    IDocumentProcessRepo documentProcessRepo = new DocumentProcessRepo(db);
                    DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(statusId);
                    DocumentProcess documentProcess = documentProcessRepo.GetDocumentProcessByIdNoTracking(documentStatus.DocumentProcessGUID);
                    if (documentProcess.ProcessId != Convert.ToInt32(DocumentProcessStatus.Purchase).ToString())
                    {
                        ex.AddData("ERROR.90130");
                        throw ex;
                    }
                    purchaseTable.DocumentStatusGUID = documentStatus.DocumentStatusGUID.GuidNullToString();
                }

                if (documentReasonGUID != null && documentReasonGUID != "")
                {
                    IDocumentReasonRepo documentReasonRepo = new DocumentReasonRepo(db);
                    DocumentReason documentReason = documentReasonRepo.GetDocumentReasonByIdNoTracking(documentReasonGUID.StringToGuid());
                    if(documentReason != null)
                    {
                        purchaseTable.DocumentReasonGUID = documentReason.DocumentReasonGUID.GuidNullToString();
                    }
                }

                if (ex.Data.Count > 0)
                {
                    throw ex;
                }

                if (processInstanceId != null || processInstanceId != "")
                {
                    IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                    purchaseTable.ProcessInstanceId = Convert.ToInt32(processInstanceId);
                }
                UpdatePurchaseTable(purchaseTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTableItemView UpdatePurchaseWorkflowConditionK2(string purchaseTableGUID)
        {
            try
            {
                PurchaseTableItemView updateResult = GetUpdatePurchaseWorkflowConditionK2(purchaseTableGUID);
                UpdatePurchaseTable(updateResult);
                return updateResult;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PurchaseTableItemView GetUpdatePurchaseWorkflowConditionK2(string purchaseTableGUID)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);

                PurchaseTableItemView main_PurchaseTable = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());
                string productType = ((ProductType)main_PurchaseTable.ProductType).ToString();
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(main_PurchaseTable.CustomerTableGUID.StringToGuid());
                IEnumerable<PurchaseLine> purchaseLineByPurchaseTable = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(main_PurchaseTable.PurchaseTableGUID.StringToGuid());

                Guid companyGUID = main_PurchaseTable.CompanyGUID.StringToGuid();

                decimal sumPurchasAmount = 0;
                decimal overCreditCA = 0;
                decimal overCreditCALine = 0;
                decimal overCreditBuyer = 0;
                bool diffChequeIssuedName = false;


                #region CalcSumPurchasAmount
                Guid purchaseTable_PostedStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Posted).ToString()).DocumentStatusGUID;

                List<PurchaseTable> sumPurchasAmount_PurchaseTableList = purchaseTableRepo.GetPurchaseTableByCompanyNoTracking(companyGUID)
                                                                                          .Where(t => t.CustomerTableGUID == main_PurchaseTable.CustomerTableGUID.StringToGuid() &&
                                                                                                      t.DocumentStatusGUID == purchaseTable_PostedStatus).ToList();
                var sumPurchasAmount_PurchaseLineList = purchaseLineRepo.GetPurchaseLineByCompanyIdNoTracking(companyGUID)
                                                                          .Where(t => t.ClosedForAdditionalPurchase == false &&
                                                                                      (((t.InterestDate != null) ? t.InterestDate : DateTime.MinValue) < main_PurchaseTable.PurchaseDate.StringToDate()))
                                                                          .Select(s => new
                                                                          {
                                                                              PurchaseLineGUID = s.PurchaseLineGUID,
                                                                              SelectPurchaseAmount = (s.RefPurchaseLineGUID != null) ? s.LinePurchaseAmount : s.PurchaseAmount,
                                                                              PurchaseTableGUID = s.PurchaseTableGUID,
                                                                              PurchaseLineInvoiceTableGUID = s.PurchaseLineInvoiceTableGUID
                                                                          }).ToList();

                List<InvoiceTable> sumPurchasAmount_InvoiceTableList = invoiceTableRepo.GetInvoiceTableByCustTransOpenStatus(companyGUID).ToList();

                var sumPurchaseSettleByPurchaseLine = productSettledTransRepo.GetProductSettledTransForUpdatePurchaseWorkflowConditionK2(companyGUID)
                    .GroupBy(g => new { g.RefGUID, g.RefType }).Select(r => new { PurchaseLineGUID = r.Key.RefGUID, sumPurchaseSettleByPurchaseLine = r.Sum(s => s.SettledPurchaseAmount) }).ToList();

                sumPurchasAmount = (from purchaseLine in sumPurchasAmount_PurchaseLineList

                                    join puchaseTable in sumPurchasAmount_PurchaseTableList
                                    on purchaseLine.PurchaseTableGUID equals puchaseTable.PurchaseTableGUID

                                    join invoiceTable in sumPurchasAmount_InvoiceTableList
                                    on purchaseLine.PurchaseLineInvoiceTableGUID equals invoiceTable.InvoiceTableGUID

                                    join sumPurchaseSettle in sumPurchaseSettleByPurchaseLine
                                    on purchaseLine.PurchaseLineGUID equals sumPurchaseSettle.PurchaseLineGUID into ljSumPurchaseSettleByPurchaseLine
                                    from sumPurchaseSettle in ljSumPurchaseSettleByPurchaseLine.DefaultIfEmpty()

                                    select new
                                    {
                                        calcPurchaseAmount = purchaseLine.SelectPurchaseAmount - ((sumPurchaseSettle != null) ? sumPurchaseSettle.sumPurchaseSettleByPurchaseLine : 0)
                                    }).Sum(f => f.calcPurchaseAmount);
                #endregion

                #region CalcOverCreditCA
                decimal sumPurchaseLine = purchaseLineByPurchaseTable.Sum(t => t.LinePurchaseAmount);
                decimal creditLimitBalance = creditAppTableService.GetCreditLimitBalanceByCreditConditionType(
                        main_PurchaseTable.CreditAppTableGUID.GuidNullToString(),
                        main_PurchaseTable.PurchaseDate
                    );

                overCreditCA = GetOverCredit(sumPurchaseLine, creditLimitBalance);
                #endregion

                #region CalcOverCreditCALine
                List<OverCreditCALineView> overCreditCALineList = purchaseLineByPurchaseTable.GroupBy(g => g.CreditAppLineGUID)
                    .Select(s => new OverCreditCALineView
                    {
                        CreditAppLineGUID = s.Key.GuidNullToString(),
                        OverCredit = 0,
                        SumPurchaseLine = s.Sum(s => s.LinePurchaseAmount),
                        CreditLimitBalance = creditAppTableService.GetCreditLimitBalanceByCreditAppLine(s.Key.GuidNullToString())
                    }).ToList();

                foreach (OverCreditCALineView item in overCreditCALineList)
                {
                    item.OverCredit = GetOverCredit(item.SumPurchaseLine, item.CreditLimitBalance);
                }

                overCreditCALine = (overCreditCALineList.Count > 0 ) ? overCreditCALineList.Max(m => m.OverCredit) : 0;
                #endregion

                #region CalcOverCreditBuyer
                IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);
                List<OverCreditBuyerView> overCreditBuyerList = purchaseLineByPurchaseTable.GroupBy(g => g.BuyerTableGUID)
                   .Select(s => new OverCreditBuyerView
                   {
                       BuyerTableGUID = s.Key.GuidNullToString(),
                       OverCredit = 0,
                       SumPurchaseLine = s.Sum(s => s.LinePurchaseAmount),
                       CreditLimitBalance = creditAppTableService.GetCreditLimitBalanceByProduct(s.Key.GuidNullToString(), (ProductType)main_PurchaseTable.ProductType),
                       isBuyerCreditLimitByProductExist = buyerCreditLimitByProductRepo.GetBuyerCreditLimitByProductByBuyer(s.Key)
                                                                                                       .Any(t => t.ProductType == Convert.ToInt32(main_PurchaseTable.ProductType))
                   }).ToList();

                foreach (OverCreditBuyerView item in overCreditBuyerList)
                {
                    if(item.isBuyerCreditLimitByProductExist == false)
                    {
                        BuyerTable buyerTable = buyerTableRepo.GetBuyerTableByIdNoTracking(item.BuyerTableGUID.StringToGuid());
                        ex.AddData("ERROR.90126", new string[] { "LABEL.BUYER_CREDIT_LIMIT_BY_PRODUCT", "LABEL.BUYER", buyerTable.BuyerId, "LABEL.PRODUCT_TYPE", productType });
                    }
                    item.OverCredit = GetOverCredit(item.SumPurchaseLine, item.CreditLimitBalance);
                }

                if (ex.Data.Count > 0)
                {
                    throw ex;
                }

                overCreditBuyer = (overCreditBuyerList.Count > 0) ? overCreditBuyerList.Max(m => m.OverCredit) : 0;
                #endregion

                #region GetDiffChequeIssuedName
                diffChequeIssuedName = chequeTableRepo.GetChequeTableByPurchaseLineCustomerPDCTable(companyGUID, main_PurchaseTable.PurchaseTableGUID.StringToGuid()).Any(t => t.IssuedName != customerTable.Name);
                #endregion

                main_PurchaseTable.SumPurchaseAmount = sumPurchasAmount;
                main_PurchaseTable.OverCreditCA = overCreditCA > MaxValue.PercentMaxValue ? MaxValue.PercentMaxValue : overCreditCA;
                main_PurchaseTable.OverCreditCALine = overCreditCALine > MaxValue.PercentMaxValue ? MaxValue.PercentMaxValue : overCreditCALine;
                main_PurchaseTable.OverCreditBuyer = overCreditBuyer > MaxValue.PercentMaxValue ? MaxValue.PercentMaxValue : overCreditBuyer;
                main_PurchaseTable.DiffChequeIssuedName = diffChequeIssuedName;

                return main_PurchaseTable;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public decimal GetOverCredit(decimal sumPurchaseLine, decimal creditLimitBalance)
        {
            try
            {
                if (creditLimitBalance == 0)
                {
                    return 0;
                }
                else
                {
                    decimal overCreditPct = ((sumPurchaseLine - creditLimitBalance) / creditLimitBalance * 100).Round();
                    if (overCreditPct < 0)
                    {
                        return 0;
                    }
                    else
                    {
                        return overCreditPct;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

        #region shared
        public decimal GetPurchaseLineAmount(Guid purchaseLineGuid)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(purchaseLineGuid);
                return GetPurchaseLineAmount(purchaseLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetPurchaseLineAmount(PurchaseLine purchaseLine)
        {
            try
            {
                return purchaseLine.RefPurchaseLineGUID == null ? purchaseLine.LinePurchaseAmount : purchaseLine.PurchaseAmount;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetPurchaseLineAmountBalance(Guid purchaseLineGuid)
        {
            try
            {
                IProductSettledTransRepo productSettledTransRepo = new ProductSettledTransRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus postedStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)TemporaryReceiptStatus.Posted).ToString());
                decimal purchaseLineAmount = GetPurchaseLineAmount(purchaseLineGuid);
                IEnumerable<ProductSettledTrans> productSettledTrans = productSettledTransRepo.GetProductSettledTransGetPurchaseLineAmountBalance(purchaseLineGuid, postedStatus.DocumentStatusGUID);
                return purchaseLineAmount - productSettledTrans.Sum(s => s.SettledPurchaseAmount);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool ValidatePurchaseAssignmentAmount(Guid purchaseTableGUID)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID);

                List<PurchaseLine> purchaseLineList = purchaseLineRepo.GetPurchaseLineByValidatePurchaseAssignmentAmount(purchaseTableGUID).ToList();

                ex = ValidatePurchaseAssignmentAmount(purchaseTable, purchaseLineList, ex);

                if (ex.MessageList.Count() > 1)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private SmartAppException ValidatePurchaseAssignmentAmount(PurchaseTable purchaseTable, List<PurchaseLine> purchaseLineList, SmartAppException ex)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);

                List<AssignmentAgreementTable> assignmentAgreementTableList = assignmentAgreementTableRepo.GetAssignmentAgreementTableByCompanyNoTracking(purchaseTable.CompanyGUID).ToList();

                var result = (from purchaseLine in purchaseLineList
                              join assignmentAgreement in assignmentAgreementTableList
                              on purchaseLine.AssignmentAgreementTableGUID equals assignmentAgreement.AssignmentAgreementTableGUID
                              group purchaseLine by new { purchaseLine.AssignmentAgreementTableGUID, assignmentAgreement.AssignmentAgreementId } into g
                              select new
                              {
                                  sumAssignmentAmount = g.Sum(s => s.AssignmentAmount),
                                  assignmentBalance = assignmentAgreementTableService.GetAssignmentBalance(g.Key.AssignmentAgreementTableGUID),
                                  assignmentAgreementId = g.Key.AssignmentAgreementId
                              });

                if (result.Any(t => t.sumAssignmentAmount > t.assignmentBalance))
                {
                    ex.AddData("ERROR.90100", new string[] { result.Count(t => t.sumAssignmentAmount > t.assignmentBalance).ToString(), String.Join(", ", result.Where(t => t.sumAssignmentAmount > t.assignmentBalance).Select(s => s.assignmentAgreementId)) });
                }
                return ex;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidatePurchaseCustomerPDCAmountTolerance(Guid purchaseTableGUID)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);

                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(purchaseTable.CompanyGUID);

                List<PurchaseLine> purchaseLineList = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTableGUID).ToList();

                ex = ValidatePurchaseCustomerPDCAmountTolerance(purchaseTable, companyParameter, purchaseLineList, ex);

                if (ex.MessageList.Count() > 1)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private SmartAppException ValidatePurchaseCustomerPDCAmountTolerance(PurchaseTable purchaseTable, CompanyParameter companyParameter,
                                                                             List<PurchaseLine> purchaseLineList, SmartAppException ex)
        {
            try
            {
                IChequeTableRepo chequeTableRepo = new ChequeTableRepo(db);

                List<ChequeTable> chequeTableList = chequeTableRepo.GetChequeTableByPurchaseLineCustomerPDCTable(purchaseTable.CompanyGUID, purchaseTable.PurchaseTableGUID).GroupBy(g => g.ChequeTableGUID).Select(s => s.FirstOrDefault()).ToList();

                var result = (from chequeTable in chequeTableList
                              join purchaseLine in purchaseLineList
                              on chequeTable.ChequeTableGUID equals purchaseLine.CustomerPDCTableGUID
                              group purchaseLine by new
                              {
                                  chequeTable.ChequeNo,
                                  purchaseLine.CustomerPDCTableGUID,
                                  purchaseLine.RefPurchaseLineGUID,
                                  chequeTable.Amount
                              } into g
                              select new
                              {
                                  sumPurchaseAmount = (g.Key.RefPurchaseLineGUID != null) ? g.Sum(s => s.PurchaseAmount) : g.Sum(s => s.LinePurchaseAmount),
                                  PDCAmount = g.Key.Amount,
                                  DiffAmount = Math.Abs(g.Key.Amount - ((g.Key.RefPurchaseLineGUID != null) ? g.Sum(s => s.PurchaseAmount) : g.Sum(s => s.LinePurchaseAmount))),
                                  ChequeNo = g.Key.ChequeNo
                              });

                if (result.Any(t => t.DiffAmount > companyParameter.ChequeToleranceAmount))
                {
                    ex.AddData("ERROR.90099", new string[] { result.Count(t => t.DiffAmount > companyParameter.ChequeToleranceAmount).ToString(), string.Join(", ", result.Where(t => t.DiffAmount > companyParameter.ChequeToleranceAmount).Select(s => s.ChequeNo)), "LABEL.PURCHASE_AMOUNT", companyParameter.ChequeToleranceAmount.ToString() });
                }

                return ex;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region function
        #region Additional purchase
        public AdditionalPurchaseParamView GetAdditionalPurchaseById(string purchaseLineGUID)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                var purchaseLineItem = purchaseLineRepo.GetByIdvw(purchaseLineGUID.StringToGuid());

                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(purchaseLineItem.CreditAppLineGUID.StringToGuid());

                decimal linePurchasePct = 0;
                if(creditAppLine.MaxPurchasePct - purchaseLineItem.PurchasePct >= 0)
                {
                    linePurchasePct = (creditAppLine.MaxPurchasePct - purchaseLineItem.PurchasePct);
                }

                AdditionalPurchaseParamView result = new AdditionalPurchaseParamView
                {
                    RefPurchaseId = purchaseLineItem.PurchaseTable_Values,
                    RefPurchaseDate = purchaseLineItem.PurchaseTable_PurchaseDate,
                    RefDueDate = purchaseLineItem.DueDate,
                    RefPurchasePct = purchaseLineItem.PurchasePct,
                    RefPurchaseAmount = purchaseLineItem.PurchaseAmount,
                    RefBuyerInvoiceAmount = purchaseLineItem.BuyerInvoiceAmount,
                    PurchaseLineGUID = purchaseLineItem.PurchaseLineGUID,
                    PurchasePct = creditAppLine.MaxPurchasePct,
                    LinePurchasePct = linePurchasePct
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region validate
        public bool ValidateAdditionalPurchaseMenu(PurchaseLineItemView model)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(model.PurchaseTableGUID.StringToGuid());
                //CustTrans custTrans = custTransRepo.GetCustTransByInvoiceTableNoTracking(model.PurchaseLineInvoiceTableGUID.StringToGuid());

                ex = ValidateRefPurchaseLineSettleAmount(ex, model.PurchaseLineInvoiceTableGUID.StringToGuidNull());

                if (model.ClosedForAdditionalPurchase)
                {
                    ex.AddData("ERROR.90083", "LABEL.PURCHASE_LINE");
                }
                if (purchaseTable.Rollbill)
                {
                    ex.AddData("ERROR.90084");
                }
                if (model.PurchaseAmount != model.LinePurchaseAmount)
                {
                    ex.AddData("ERROR.90088", "LABEL.PURCHASE_LINE");
                }

                if (ex.MessageList.Count() > 1)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private SmartAppException ValidateRefPurchaseLineSettleAmount(SmartAppException ex, Guid? purchaseLineInvoicetableGUID)
        {
            try
            {
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                CustTrans custTrans = purchaseLineInvoicetableGUID == null ? null :
                    custTransRepo.GetCustTransByInvoiceTableNoTracking(purchaseLineInvoicetableGUID.Value);
                if (custTrans == null)
                {
                    ex.AddData("ERROR.90120", "LABEL.CUSTOMER_TRANSACTION", "LABEL.INVOICE", "LABEL.REFERENCE_PURCHASE_LINE_ID");
                    return ex;
                }
                decimal refPurchaseLineSettleAmount = custTrans.SettleAmount;

                if (refPurchaseLineSettleAmount != 0)
                {
                    ex.AddData("ERROR.90082", "LABEL.PURCHASE");
                }
                return ex;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool ValidateMaxPurchasePct(string purchaseLineGUID, decimal inputPurchasePct)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(purchaseLineGUID.StringToGuid());
                return ValidateMaxPurchasePct(purchaseLine, inputPurchasePct);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool ValidateMaxPurchasePct(PurchaseLine purchaseLine, decimal inputPurchasePct, CreditAppLine creditAppLine = null)
        {
            try
            {
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                if(purchaseLine != null)
                {
                    creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(purchaseLine.CreditAppLineGUID);
                }
                
                if (inputPurchasePct > creditAppLine.MaxPurchasePct)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool ValidateAdditionalPurchase(AdditionalPurchaseParamView parm, PurchaseLine purchaseLine, PurchaseTable purchaseTable, 
                                                CreditAppTable creditAppTable, CreditAppLine creditAppLine)
        {
            try
            {
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);

                DocumentStatus purchaseTableStatus = documentStatusRepo.GetDocumentStatusByDocumentStatusGUIDNoTracking(purchaseTable.DocumentStatusGUID);
                int purchaseStatusId = Convert.ToInt32(purchaseTableStatus.StatusId);
                NumberSeqTable purchaseNumberSeq = numberSeqTableRepo.GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(purchaseLine.CompanyGUID, ReferenceId.Purchase);

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                if (string.IsNullOrWhiteSpace(parm.DueDate) || string.IsNullOrWhiteSpace(parm.PurchaseDate) ||
                    string.IsNullOrWhiteSpace(parm.RefDueDate) || string.IsNullOrWhiteSpace(parm.RefPurchaseDate) ||
                    parm.RefDueDate.StringNullToDateNull() == null || parm.RefPurchaseDate.StringNullToDateNull() == null ||
                    parm.DueDate.StringNullToDateNull() == null || parm.PurchaseDate.StringNullToDateNull() == null)
                {
                    ex.AddData("Invalid parameter values.");
                    throw ex;
                }
                if (creditAppTable.InactiveDate != null && parm.PurchaseDate.StringToDate() >= creditAppTable.InactiveDate)
                {
                    ex.AddData("ERROR.90108", "LABEL.PURCHASE_DATE", "LABEL.INACTIVE_DATE", "LABEL.CREDIT_APPLICATION");
                }
                if (creditAppTable.InactiveDate == null && parm.PurchaseDate.StringToDate() >= creditAppTable.ExpiryDate)
                {
                    ex.AddData("ERROR.90108", "LABEL.PURCHASE_DATE", "LABEL.EXPIRY_DATE", "LABEL.CREDIT_APPLICATION");
                }
                if (purchaseStatusId != (int)PurchaseStatus.Posted)
                {
                    ex.AddData("ERROR.90013", "LABEL.PURCHASE");
                }
                ex = ValidateRefPurchaseLineSettleAmount(ex, purchaseLine.PurchaseLineInvoiceTableGUID);
                if (purchaseLineRepo.AnyNotRejectedOrCancelledAdditionalPurchase(purchaseLine.PurchaseLineGUID))
                {
                    ex.AddData("ERROR.90085");
                }
                if (purchaseNumberSeq.Manual)
                {
                    ex.AddData("ERROR.00576", purchaseNumberSeq.NumberSeqCode);
                }
                // parm.PurchaseDate < parm.RefPurchaseDate
                if (parm.PurchaseDate.StringToDate() < parm.RefPurchaseDate.StringToDate())
                {
                    ex.AddData("ERROR.DATE_CANNOT_LESS_THAN", "LABEL.PURCHASE_DATE", "LABEL.REFERENCE_PURCHASE_DATE");
                }
                if (parm.DueDate.StringToDate() < parm.PurchaseDate.StringToDate())
                {
                    ex.AddData("ERROR.DATE_CANNOT_LESS_THAN", "LABEL.DUE_DATE", "LABEL.PURCHASE_DATE");
                }
                // parm.DueDate < parm.RefDueDate
                if (parm.DueDate.StringToDate() < parm.RefDueDate.StringToDate())
                {
                    ex.AddData("ERROR.DATE_CANNOT_LESS_THAN", "LABEL.DUE_DATE", "LABEL.REFERENCE_DUE_DATE");
                }
                if (!ValidateMaxPurchasePct(null, parm.PurchasePct, creditAppLine))
                {
                    ex.AddData("ERROR.90031", "LABEL.LINE_PURCHASE_PCT", "LABEL.MAXIMUM_PURCHASE_PCT");
                }
                if(parm.PurchaseDate.StringToDate() >= creditAppLine.ExpiryDate)
                {
                    ex.AddData("ERROR.90153", "LABEL.CREDIT_APPLICATION_LINE", "LABEL.ADDITIONAL_PURCHASE");
                }

                if (ex.MessageList.Count() > 1)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion 
        public AdditionalPurchaseResultView AdditionalPurchase(AdditionalPurchaseParamView parm)
        {
            try
            {
                AdditionalPurchaseResultView result = new AdditionalPurchaseResultView();
                AdditionalPurchaseResultViewMap additionalPurchase = InitAdditionalPurchase(parm);
                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    this.BulkInsert(new List<PurchaseTable> { additionalPurchase.PurchaseTable });
                    this.BulkInsert(additionalPurchase.PurchaseLine);
                    UnitOfWork.Commit(transaction);
                }

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.00075", new string[] { additionalPurchase.PurchaseTable.PurchaseId });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public AdditionalPurchaseResultViewMap InitAdditionalPurchase(AdditionalPurchaseParamView parm)
        {
            try
            {
                AdditionalPurchaseResultViewMap result = new AdditionalPurchaseResultViewMap();
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);

                PurchaseLine purchaseLine = purchaseLineRepo.GetPurchaseLineByIdNoTracking(parm.PurchaseLineGUID.StringToGuid());
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseLine.PurchaseTableGUID);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(purchaseTable.CreditAppTableGUID);
                DocumentStatus purchaseDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Draft).ToString());

                CreditAppLine creditAppLine = creditAppLineRepo.GetCreditAppLineByIdNoTracking(purchaseLine.CreditAppLineGUID);

                if (ValidateAdditionalPurchase(parm, purchaseLine, purchaseTable, creditAppTable, creditAppLine))
                {
                    #region create purchase table
                    IRetentionConditionTransService retentionConditionTransService = new RetentionConditionTransService(db);
                    RetentionConditionTrans retentionConditionTrans =
                        retentionConditionTransService.GetRetentionConditionTransByReferenceAndMethod(purchaseTable.CreditAppTableGUID,
                                                                                (int)RetentionDeductionMethod.Purchase).FirstOrDefault();
                    PurchaseTable newPurchaseTable = new PurchaseTable
                    {
                        PurchaseTableGUID = Guid.NewGuid(),
                        Description = parm.Description,
                        ProductType = purchaseTable.ProductType,
                        CreditAppTableGUID = purchaseTable.CreditAppTableGUID,
                        CustomerTableGUID = purchaseTable.CustomerTableGUID,
                        PurchaseDate = parm.PurchaseDate.StringToDate(),
                        TotalInterestPct = purchaseTable.TotalInterestPct,
                        DocumentStatusGUID = purchaseDraft.DocumentStatusGUID,
                        Rollbill = purchaseTable.Rollbill,
                        ReceiptTempTableGUID = null,
                        RetentionCalculateBase = retentionConditionTrans != null ? retentionConditionTrans.RetentionCalculateBase : (int)RetentionCalculateBase.None,
                        RetentionPct = retentionConditionTrans != null ? retentionConditionTrans.RetentionPct : 0.0m,
                        RetentionFixedAmount = (retentionConditionTrans != null
                            && retentionConditionTrans.RetentionCalculateBase == (int)RetentionCalculateBase.FixedAmount) ?
                            retentionConditionTrans.RetentionAmount : 0.0m,
                        Dimension1GUID = purchaseTable.Dimension1GUID,
                        Dimension2GUID = purchaseTable.Dimension2GUID,
                        Dimension3GUID = purchaseTable.Dimension3GUID,
                        Dimension4GUID = purchaseTable.Dimension4GUID,
                        Dimension5GUID = purchaseTable.Dimension5GUID,
                        SumPurchaseAmount = 0.0m,
                        OverCreditBuyer = 0.0m,
                        OverCreditCA = 0.0m,
                        OverCreditCALine = 0.0m,
                        DiffChequeIssuedName = false,
                        AdditionalPurchase = true,
                        CompanyGUID = purchaseLine.CompanyGUID,
                        Owner = GetUserName(),
                        OwnerBusinessUnitGUID = GetCurrentOwnerBusinessUnitGUID().StringToGuid(),
                        OperReportSignatureGUID = purchaseTable.OperReportSignatureGUID
                    };
                    GenPurchaseTableNumberSeqCode(newPurchaseTable);
                    result.PurchaseTable = newPurchaseTable;
                    #endregion
                    #region create purchase line
                    IInterestService interestService = new InterestService(db);
                    
                    DateTime interestDate = interestService.FindActualInterestDate(parm.DueDate.StringToDate(), creditAppTable.ProductSubTypeGUID);
                    int interestDay = Convert.ToInt32((interestDate - parm.PurchaseDate.StringToDate()).TotalDays);
                    decimal interestAmount = CalcInterestAmount(purchaseLine.CompanyGUID, parm.PurchaseAmount, newPurchaseTable.TotalInterestPct, interestDay);
                    decimal interestRefundAmount =
                        CalcInterestAmount(purchaseLine.CompanyGUID,
                                            purchaseLine.RefPurchaseLineGUID != null ? parm.PurchaseAmount : parm.LinePurchaseAmount,
                                            newPurchaseTable.TotalInterestPct,
                                            Convert.ToInt32((purchaseLine.InterestDate - newPurchaseTable.PurchaseDate).TotalDays * -1));
                    decimal purchaseFeeAmount =
                        CalcPurchaseLinePurchaseFeeAmount((PurchaseFeeCalculateBase)creditAppLine.PurchaseFeeCalculateBase, purchaseLine.BuyerInvoiceAmount,
                                                        creditAppLine.PurchaseFeePct, parm.LinePurchaseAmount);
                    decimal retentionAmount = CalcPurchaseLineRetentionAmount(newPurchaseTable, purchaseLine.BuyerInvoiceAmount, parm.LinePurchaseAmount);
                    // create purchase line
                    PurchaseLine newPurchaseLine = new PurchaseLine
                    {
                        PurchaseLineGUID = Guid.NewGuid(),
                        PurchaseTableGUID = result.PurchaseTable.PurchaseTableGUID,
                        LineNum = GetPurchaseLineLastOrdering(newPurchaseTable.PurchaseTableGUID.GuidNullToString()),
                        RefPurchaseLineGUID = purchaseLine.PurchaseLineGUID,
                        ClosedForAdditionalPurchase = false,
                        BuyerTableGUID = purchaseLine.BuyerTableGUID,
                        BuyerInvoiceTableGUID = purchaseLine.BuyerInvoiceTableGUID,
                        BuyerInvoiceAmount = purchaseLine.BuyerInvoiceAmount,
                        BuyerAgreementTableGUID = purchaseLine.BuyerAgreementTableGUID,
                        CreditAppLineGUID = purchaseLine.CreditAppLineGUID,
                        PurchaseLineInvoiceTableGUID = null,
                        DueDate = parm.DueDate.StringToDate(),
                        InterestDate = interestDate,
                        InterestDay = interestDay,
                        PurchasePct = parm.PurchasePct,
                        LinePurchasePct = parm.LinePurchasePct,
                        PurchaseAmount = parm.PurchaseAmount,
                        LinePurchaseAmount = parm.LinePurchaseAmount,
                        ReserveAmount = purchaseLine.BuyerInvoiceAmount - parm.PurchaseAmount,
                        NetPurchaseAmount = parm.LinePurchaseAmount - 0.0m,
                        InterestAmount = interestAmount,
                        SettleInterestAmount = 0.0m,
                        InterestRefundAmount = interestRefundAmount,
                        PurchaseFeePct = creditAppLine.PurchaseFeePct,
                        PurchaseFeeCalculateBase = creditAppLine.PurchaseFeeCalculateBase,
                        PurchaseFeeAmount = purchaseFeeAmount,
                        SettlePurchaseFeeAmount = 0.0m,
                        BillingFeeAmount = 0.0m,
                        SettleBillingFeeAmount = 0.0m,
                        ReceiptFeeAmount = 0.0m,
                        SettleReceiptFeeAmount = 0.0m,
                        RetentionAmount = retentionAmount,
                        AssignmentAgreementTableGUID = purchaseLine.AssignmentAgreementTableGUID,
                        AssignmentAmount = purchaseLine.BuyerInvoiceAmount,
                        CustomerPDCTableGUID = null,
                        BuyerPDCTableGUID = null,
                        BillingDate = null,
                        CollectionDate = parm.DueDate.StringToDate(),
                        MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID,
                        OutstandingBuyerInvoiceAmount = 0.0m,
                        RollbillPurchaseLineGUID = null,
                        OriginalPurchaseLineGUID = null,
                        OriginalInterestDate = null,
                        RollbillInterestDay = 0,
                        RollbillInterestPct = 0.0m,
                        RollbillInterestAmount = 0.0m,
                        SettleRollBillInterestAmount = 0.0m,
                        NumberOfRollbill = 0,
                        Dimension1GUID = newPurchaseTable.Dimension1GUID,
                        Dimension2GUID = newPurchaseTable.Dimension2GUID,
                        Dimension3GUID = newPurchaseTable.Dimension3GUID,
                        Dimension4GUID = newPurchaseTable.Dimension4GUID,
                        Dimension5GUID = newPurchaseTable.Dimension5GUID,
                        CompanyGUID = purchaseLine.CompanyGUID,
                        Owner = GetUserName(),
                        OwnerBusinessUnitGUID = GetCurrentOwnerBusinessUnitGUID().StringToGuid()
                    };
                    purchaseLineRepo.ValidateAdd(newPurchaseLine);
                    result.PurchaseLine = new List<PurchaseLine>() { newPurchaseLine };
                    #endregion
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

        #region Send E-mail
        public SendPurchaseEmailView GetSendEmailPurchaseById(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                IContactTransRepo contactTransRepo = new ContactTransRepo(db);
                ISysUserService sysUserService = new SysUserService(db);
                IActionHistoryRepo actionHistoryRepo = new ActionHistoryRepo(db);

                PurchaseTableItemView purchaseTableItem = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());

                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(purchaseTableItem.CustomerTableGUID.StringToGuid());
                SysUserTableItemView sysUserTable = null;
                string employee_name = "";
                if (customerTable.ResponsibleByGUID != null)
                {
                    EmployeeTable employeeTable = employeeTableRepo.GetByEmployeeGUIDNoTracking(customerTable.ResponsibleByGUID);
                    employee_name = employeeTable.Name;
                    if (employeeTable.UserId != null)
                    {
                        sysUserTable = sysUserService.GetSysUserTableById(employeeTable.UserId.GuidNullToString());
                    }
                }

                IEnumerable<ContactTrans> contactTransList = contactTransRepo.GetPrimaryContactTransByEmailType(purchaseTableItem.CustomerTableGUID.StringToGuid(), (int)RefType.Customer);

                ActionHistoryListView actionHistory = actionHistoryRepo.GetActionHistoryByRefGUID(purchaseTableItem.PurchaseTableGUID.StringToGuid()).Where(t => t.ActivityName == "Start workflow" &&
                                                                                                                                       t.ActionName == "Submit").FirstOrDefault();

                decimal netPaid = (purchaseTableItem.Rollbill == false) ? CalcPurchaseNetPaidByPurchaseTable(purchaseTableItem.PurchaseTableGUID.StringToGuid()) : 0;

                SendPurchaseEmailView result = new SendPurchaseEmailView
                {
                    AdditionalPurchase = purchaseTableItem.AdditionalPurchase,
                    CustomerId = purchaseTableItem.CustomerTable_Values,
                    DocumentStatus = SmartAppUtil.GetDropDownLabel(purchaseTableItem.DocumentStatus_StatusId, purchaseTableItem.Description),
                    PurchaseDate = purchaseTableItem.PurchaseDate,
                    PurchaseId = purchaseTableItem.PurchaseId,
                    PurchaseTableGUID = purchaseTableGUID,
                    RowVersion = purchaseTableItem.RowVersion,
                    // workflow
                    ParmFolio = "{{0}} : " + purchaseTableItem.PurchaseId + " {{1}} " + db.GetUserName() + " {{2}} " + customerTable.Name + " MKT : " + employee_name + " {{3}} " + purchaseTableItem.PurchaseDate + " " + String.Format("{0:n}", netPaid),
                    ProcName = "Workflow\\LIT.PurchaseEmail",
                    ParmCompany = purchaseTableItem.CompanyGUID,
                    ParmDocGUID = purchaseTableItem.PurchaseTableGUID,
                    ParmDocID = purchaseTableItem.PurchaseId,
                    ParmCustomerEmail = (contactTransList.Count() != 0) ? contactTransList.FirstOrDefault().ContactValue : null,
                    ParmMarketingUser = (sysUserTable != null) ? sysUserTable.UserName : null,
                    ParmSubmitPurchaseUser = (actionHistory != null) ? actionHistory.CreatedBy : null,
                    ParmCustomerName = customerTable.Name,
                    ParmPurchaseDate = purchaseTableItem.PurchaseDate,
                    ParmPurchaseId = purchaseTableItem.PurchaseId,
                    ActivityName = "Purchase E-mail",
                    ActionName = "Send purchase E-mail"
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private bool ValidateSendEmailPurchase(SendPurchaseEmailView input)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IContactTransRepo contactTransRepo = new ContactTransRepo(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(input.PurchaseTableGUID.StringToGuid());
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(purchaseTable.CustomerTableGUID);
                Guid postStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(PurchaseStatus.Posted).ToString()).DocumentStatusGUID;
                IEnumerable<ContactTrans> contactTransList = contactTransRepo.GetPrimaryContactTransByEmailType(purchaseTable.CustomerTableGUID, (int)RefType.Customer);

                IAttachmentRepo attachmentRepo = new AttachmentRepo(db);
                Attachment attachment = attachmentRepo.GetAttachmentByRefTypeNoTracking(purchaseTable.PurchaseTableGUID, Convert.ToInt32(RefType.PurchaseTable)).Where(t => t.FileName == purchaseTable.PurchaseId + ".pdf").FirstOrDefault();

                // check row version
                purchaseTable.CheckRowVersion(input);

                if (purchaseTable.DocumentStatusGUID != postStatus)
                {
                    ex.AddData("ERROR.90013", "LABEL.PURCHASE");
                }

                if (contactTransList.Count() == 0)
                {
                    ex.AddData("ERROR.90007", "LABEL.CUSTOMER", "ENUM.CONTACT_TYPE_EMAIL");
                }


                if (customerTable.ResponsibleByGUID == null)
                {
                    ex.AddData("ERROR.90047", "LABEL.RESPONSIBLE_BY", "LABEL.CUSTOMER");
                }

                if(attachment == null)
                {
                    ex.AddData("ERROR.90136", "LABEL.ATTACHMENT", "LABEL.FILE_NAME", purchaseTable.PurchaseId + ".pdf");
                }

                if (ex.MessageList.Count() > 1)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> ValidateAndStartWorkflowBySendEmailPurchase(SendPurchaseEmailView item)
        {
            try
            {
                ValidateSendEmailPurchase(item);
                IK2Service k2Service = new K2Service(db);
                return await k2Service.StartWorkflow(item.WorkflowInstance, item.ActivityName,item.ActionName);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Post purchase
        public PostPurchaseParamView GetPostPurchaseById(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                var purchaseTableItem = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());
                PostPurchaseParamView result = new PostPurchaseParamView
                {
                    PurchaseId = purchaseTableItem.PurchaseId,
                    PurchaseDate = purchaseTableItem.PurchaseDate,
                    DocumentStatus = purchaseTableItem.DocumentStatus_Description,
                    CustomerId = purchaseTableItem.CustomerTable_Values,
                    Rollbill = purchaseTableItem.Rollbill,
                    AdditionalPurchase = purchaseTableItem.AdditionalPurchase,
                    PurchaseTableGUID = purchaseTableItem.PurchaseTableGUID
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PostPurchaseResultView PostPurchase(PostPurchaseParamView parm)
        {
            try
            {
                PostPurchaseResultView result = new PostPurchaseResultView();
                PostPurchaseResultViewMap postPurchaseResult = InitPostPurchase(parm.PurchaseTableGUID.StringToGuid());

                using (var transaction = UnitOfWork.ContextTransaction())
                {
                    if (postPurchaseResult.ReceiptTempTable != null)
                    {
                        this.BulkInsert(new List<ReceiptTempTable>() { postPurchaseResult.ReceiptTempTable });
                    }
                    
                    this.BulkUpdate(postPurchaseResult.InterestRealizedTransUpdate);
                    this.BulkUpdate(postPurchaseResult.CustTransUpdate);

                    if(postPurchaseResult.CreditAppTable != null)
                    {
                        this.BulkUpdate(new List<CreditAppTable>() { postPurchaseResult.CreditAppTable });
                    }

                    this.BulkInsert(postPurchaseResult.ReceiptTempPaymDetail);

                    this.BulkInsert(postPurchaseResult.InvoiceTable);
                    this.BulkInsert(postPurchaseResult.InvoiceLine);

                    this.BulkInsert(postPurchaseResult.InvoiceSettlementDetail);

                    this.BulkInsert(postPurchaseResult.ReceiptTable);
                    this.BulkInsert(postPurchaseResult.ReceiptLine);

                    this.BulkInsert(postPurchaseResult.PaymentHistory);
                    this.BulkInsert(postPurchaseResult.VendorPaymentTrans);

                    this.BulkInsert(postPurchaseResult.TaxInvoiceTable);
                    this.BulkInsert(postPurchaseResult.TaxInvoiceLine);
                    
                    this.BulkInsert(postPurchaseResult.ProcessTrans);
                    this.BulkInsert(postPurchaseResult.CustTransCreate);
                    this.BulkInsert(postPurchaseResult.CreditAppTrans);
                    
                    this.BulkInsert(postPurchaseResult.RetentionTrans);

                    this.BulkInsert(postPurchaseResult.InterestRealizedTransCreate);

                    this.BulkInsert(postPurchaseResult.IntercompanyInvoiceTable);

                    this.BulkUpdate(postPurchaseResult.PurchaseLine);
                    this.BulkUpdate(new List<PurchaseTable>() { postPurchaseResult.PurchaseTable });

                    UnitOfWork.Commit(transaction);
                }

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.90004", new string[] { "LABEL.PURCHASE", SmartAppUtil.GetDropDownLabel(postPurchaseResult.PurchaseTable.PurchaseId,
                                                                                                    postPurchaseResult.PurchaseTable.Description) });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PostPurchaseResultViewMap InitPostPurchase(Guid purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                var purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID);
                return InitPostPurchase(purchaseTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PostPurchaseResultViewMap InitPostPurchase(PurchaseTable purchaseTable)
        {
            try
            {
                PostPurchaseResultViewMap result = new PostPurchaseResultViewMap();
                PostPurchaseParamViewMap parm = PreparePostPurchaseParam(purchaseTable);

                result.PurchaseLine = new List<PurchaseLine>();
                result.InvoiceTable = new List<InvoiceTable>();
                result.InvoiceLine = new List<InvoiceLine>();
                result.InvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                result.InterestRealizedTransCreate = new List<InterestRealizedTrans>();
                result.InterestRealizedTransUpdate = new List<InterestRealizedTrans>();
                result.VendorPaymentTrans = new List<VendorPaymentTrans>();
                result.ProcessTrans = new List<ProcessTrans>();
                result.ReceiptTempPaymDetail = new List<ReceiptTempPaymDetail>();
                result.CustTransCreate = new List<CustTrans>();
                result.CustTransUpdate = new List<CustTrans>();
                result.CreditAppTrans = new List<CreditAppTrans>();
                result.TaxInvoiceTable = new List<TaxInvoiceTable>();
                result.TaxInvoiceLine = new List<TaxInvoiceLine>();
                result.RetentionTrans = new List<RetentionTrans>();
                result.ReceiptTable = new List<ReceiptTable>();
                result.ReceiptLine = new List<ReceiptLine>();
                result.PaymentHistory = new List<PaymentHistory>();
                result.IntercompanyInvoiceTable = new List<IntercompanyInvoiceTable>();

                if (ValidatePostPurchase(parm))
                {
                    IInvoiceService invoiceService = new InvoiceService(db);
                    IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                    IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);
                    IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                    IInterestRealizedTransRepo interestRealizedTransRepo = new InterestRealizedTransRepo(db);
                    IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                    IProcessTransService processTransService = new ProcessTransService(db);
                    INumberSequenceService numberSeqService = new NumberSequenceService(db);
                    IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);

                    List<InvoiceTable> purchaseLineInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> purchaseLineInvoiceLineList = new List<InvoiceLine>();
                    List<InvoiceTable> purchaseLineInterestInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> purchaseLineInterestInvoiceLineList = new List<InvoiceLine>();
                    List<InvoiceTable> purchaseLinePurchaseFeeInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> purchaseLinePurchaseFeeInvoiceLineList = new List<InvoiceLine>();
                    List<InvoiceTable> purchaseLineReceiptFeeInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> purchaseLineReceiptFeeInvoiceLineList = new List<InvoiceLine>();
                    List<InvoiceTable> purchaseLineBillingFeeInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> purchaseLineBillingFeeInvoiceLineList = new List<InvoiceLine>();
                    List<InvoiceTable> rollbillInterestInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> rollbillInterestInvoiceLineList = new List<InvoiceLine>();

                    List<InvoiceSettlementDetail> newInvoiceSettlementDetailList = new List<InvoiceSettlementDetail>();

                    InvoiceType ftInvType = parm.InvoiceTypes.Where(w => w.InvoiceTypeGUID == parm.CompanyParameter.FTInvTypeGUID.Value).FirstOrDefault();

                    #region purchase invoiceTables (for post settlement)
                    List<Guid> invoiceTableGuids = new List<Guid>();
                    List<InvoiceTable> purchaseInvoiceTable = new List<InvoiceTable>();
                    List<InvoiceLine> purchaseInvoiceLine = new List<InvoiceLine>();
                    List<CustTrans> purchaseCustTrans = new List<CustTrans>();

                    #region purchase invoiceSettlementDetail invoice
                    var purchaseInvSettleDetailInvoiceTableGuids =
                        parm.PurchaseInvoiceSettlementDetail.Where(w => w.InvoiceTableGUID.HasValue).Select(s => s.InvoiceTableGUID.Value);
                    invoiceTableGuids.AddRange(purchaseInvSettleDetailInvoiceTableGuids);
                    #endregion
                    #region 7.2, 8.2 ref/rollbill purchaseLine invoice
                    List<InvoiceTable> refPurchaseLineRollBillInvoice = null;
                    if (parm.RefPurchaseLine != null && parm.RefPurchaseLine.Count > 0)
                    {
                        invoiceTableGuids.AddRange(parm.RefPurchaseLine.Where(w => w.PurchaseLineInvoiceTableGUID.HasValue)
                                                                                    .Select(s => s.PurchaseLineInvoiceTableGUID.Value));
                    }
                    if (parm.RollbillPurchaseLine != null && parm.RollbillPurchaseLine.Count > 0)
                    {
                        invoiceTableGuids.AddRange(parm.RollbillPurchaseLine.Where(w => w.PurchaseLineInvoiceTableGUID.HasValue)
                                                                                    .Select(s => s.PurchaseLineInvoiceTableGUID.Value));
                    }

                    #endregion
                    if (invoiceTableGuids.Count > 0)
                    {
                        purchaseInvoiceTable = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceTableGuids);
                        refPurchaseLineRollBillInvoice = purchaseInvoiceTable.Where(w => !purchaseInvSettleDetailInvoiceTableGuids.Contains(w.InvoiceTableGUID)).ToList();

                        purchaseInvoiceLine = invoiceLineRepo.GetInvoiceLinesByInvoiceTableGuid(invoiceTableGuids);

                        purchaseCustTrans = parm.CustTrans;
                    }
                    #endregion
                    #region refPurchaseLine InterestRealizedTrans
                    List<InterestRealizedTrans> refPurchaseLineInterestRealizedTransList =
                        interestRealizedTransRepo.GetInterestRealizedTransByReferenceNoTracking(RefType.PurchaseLine, parm.PurchaseLine
                                                                                                                        .Where(w => w.RefPurchaseLineGUID.HasValue)
                                                                                                                        .Select(s => s.RefPurchaseLineGUID.Value));
                    #endregion

                    // 1
                    result.ReceiptTempTable = CreatePostPurchaseReceiptTemp(parm);
                    
                    #region loop purchase lines 4 - 11
                    foreach (var purchaseLine in parm.PurchaseLine)
                    {
                        #region 4 create purchaseLine product invoice
                        #region 4.1 create purchase invoiceTable
                        var invoiceDraftStatus = parm.DocumentStatus.Where(w => w.StatusId == ((int)InvoiceStatus.Draft).ToString()).FirstOrDefault();
                        InvoiceTable purchaseLineInvoiceTable =
                            CreatePurchaseInvoiceTablePostPurchase(parm.PurchaseTable, purchaseLine, parm.CreditAppTable, invoiceDraftStatus,
                                                                parm.PurchaseCustomerTable, parm.PurchaseDateExchangeRate, ftInvType, parm.Company);
                        #endregion

                        #region 4.2 - 4.5 create purchase invoice line
                        InvoiceLine purchaseLineInvoiceLine1 =
                                CreatePurchaseInvoiceLinePostPurchase(purchaseLine, purchaseLineInvoiceTable, parm.PurchaseTable.PurchaseDate,
                                        purchaseLine.RefPurchaseLineGUID.HasValue ? purchaseLine.PurchaseAmount : purchaseLine.LinePurchaseAmount,
                                        ftInvType.AutoGenInvoiceRevenueTypeGUID, 1);
                        
                        purchaseLineInvoiceLineList.Add(purchaseLineInvoiceLine1);

                        InvoiceLine purchaseLineInvoiceLine2 =
                                CreatePurchaseInvoiceLinePostPurchase(purchaseLine, purchaseLineInvoiceTable, parm.PurchaseTable.PurchaseDate,
                                        purchaseLine.ReserveAmount, parm.CompanyParameter.FTReserveInvRevenueTypeGUID, 2);
                        
                        purchaseLineInvoiceLineList.Add(purchaseLineInvoiceLine2);
                        #endregion

                        // 4.6 get sumAmountInvoiceLine
                        var totalAmountBeforeTax = purchaseLineInvoiceLineList.Where(w => w.InvoiceTableGUID == purchaseLineInvoiceTable.InvoiceTableGUID)
                                                                            .Sum(s => s.TotalAmountBeforeTax);
                        var taxAmount = purchaseLineInvoiceLineList.Where(w => w.InvoiceTableGUID == purchaseLineInvoiceTable.InvoiceTableGUID)
                                                                .Sum(s => s.TaxAmount);
                        var totalAmount = purchaseLineInvoiceLineList.Where(w => w.InvoiceTableGUID == purchaseLineInvoiceTable.InvoiceTableGUID)
                                                                .Sum(s => s.TotalAmount);

                        // 4.7 update invoiceTable amount
                        purchaseLineInvoiceTable.InvoiceAmountBeforeTax = totalAmountBeforeTax;
                        purchaseLineInvoiceTable.InvoiceAmountBeforeTaxMST = (totalAmountBeforeTax * parm.PurchaseDateExchangeRate).Round();
                        purchaseLineInvoiceTable.TaxAmount = taxAmount;
                        purchaseLineInvoiceTable.TaxAmountMST = (taxAmount * parm.PurchaseDateExchangeRate).Round();
                        purchaseLineInvoiceTable.InvoiceAmount = totalAmount;
                        purchaseLineInvoiceTable.InvoiceAmountMST = (totalAmount * parm.PurchaseDateExchangeRate).Round();

                        purchaseLineInvoiceTableList.Add(purchaseLineInvoiceTable);

                        // 4.9 update purchaseLine invoiceTable
                        purchaseLine.PurchaseLineInvoiceTableGUID = purchaseLineInvoiceTable.InvoiceTableGUID;
                        #endregion

                        #region 5 create interest invoice
                        var outstandingInterest = (purchaseLine.InterestAmount + purchaseLine.InterestRefundAmount);
                        if (outstandingInterest > 0)
                        {
                            #region 5.1
                            var interestInvoiceResult =
                                    CreateInvoice1LineSpecificForPostPurchase(parm.PurchaseTable, parm.CompanyParameter.FTUnearnedInterestInvTypeGUID.Value,
                                                                                    outstandingInterest, true, null,
                                                                                    purchaseLine);
                            purchaseLineInterestInvoiceTableList.Add(interestInvoiceResult.InvoiceTable);
                            purchaseLineInterestInvoiceLineList.Add(interestInvoiceResult.InvoiceLine);
                            #endregion
                        }
                        #endregion
                        #region 6 create purchaseLine fee invoice
                        #region 6.1 - 6.2 create purchase fee amount invoice
                        if (purchaseLine.PurchaseFeeAmount > 0)
                        {
                            var purchaseFeeInvoiceResult =
                                    CreateInvoice1LineSpecificForPostPurchase(parm.PurchaseTable, parm.CompanyParameter.FTFeeInvTypeGUID.Value,
                                                                                    purchaseLine.PurchaseFeeAmount, true, null,
                                                                                    purchaseLine);
                            purchaseLinePurchaseFeeInvoiceTableList.Add(purchaseFeeInvoiceResult.InvoiceTable);
                            purchaseLinePurchaseFeeInvoiceLineList.Add(purchaseFeeInvoiceResult.InvoiceLine);
                        }
                        #endregion

                        #region 6.6 - 6.7 create receipt fee amount invoice
                        if (purchaseLine.ReceiptFeeAmount > 0)
                        {
                            var receiptFeeInvoiceResult =
                                    CreateInvoice1LineSpecificForPostPurchase(parm.PurchaseTable, parm.CompanyParameter.ServiceFeeInvTypeGUID.Value,
                                                            purchaseLine.ReceiptFeeAmount, false, parm.CompanyParameter.FTReceiptFeeInvRevenueTypeGUID.Value,
                                                            purchaseLine);
                            purchaseLineReceiptFeeInvoiceTableList.Add(receiptFeeInvoiceResult.InvoiceTable);
                            purchaseLineReceiptFeeInvoiceLineList.Add(receiptFeeInvoiceResult.InvoiceLine);
                        }
                        #endregion

                        #region 6.11 - 6.12 create billing fee amount invoice
                        if (purchaseLine.BillingFeeAmount > 0)
                        {
                            var billingFeeInvoiceResult =
                                    CreateInvoice1LineSpecificForPostPurchase(parm.PurchaseTable, parm.CompanyParameter.ServiceFeeInvTypeGUID.Value,
                                                                purchaseLine.BillingFeeAmount, false, parm.CompanyParameter.FTBillingFeeInvRevenueTypeGUID.Value,
                                                                purchaseLine);
                            purchaseLineBillingFeeInvoiceTableList.Add(billingFeeInvoiceResult.InvoiceTable);
                            purchaseLineBillingFeeInvoiceLineList.Add(billingFeeInvoiceResult.InvoiceLine);
                        }
                        #endregion
                        #endregion

                        #region 7 create invoice settlement detail ref purchaseLine
                        if (purchaseLine.RefPurchaseLineGUID.HasValue)
                        {
                            var refPurchaseLine = parm.RefPurchaseLine.Where(w => w.PurchaseLineGUID == purchaseLine.RefPurchaseLineGUID).FirstOrDefault();
                            // 7.2
                            var refPurchaseLineInvoice = 
                                    refPurchaseLineRollBillInvoice.Where(w => w.InvoiceTableGUID == refPurchaseLine.PurchaseLineInvoiceTableGUID)
                                                                    .FirstOrDefault();
                            // 7.3
                            var refPurchaseAmount = GetPurchaseLineAmount(refPurchaseLine);
                            // 7.4 - 7.5
                            var refPurchaseLineInvSettlementDetail =
                                CreateInvoiceSettlementDetailsPostPurchase(new List<InvoiceTable> { refPurchaseLineInvoice }, null,
                                                                            new List<PurchaseLine> { refPurchaseLine }, string.Empty,
                                                                            result.ReceiptTempTable.ReceiptTempTableGUID, parm.PurchaseTable.CompanyGUID,
                                                                            refPurchaseAmount);
                            newInvoiceSettlementDetailList.AddRange(refPurchaseLineInvSettlementDetail);
                            // 7.6
                            refPurchaseLine.ClosedForAdditionalPurchase = true;
                            result.PurchaseLine.Add(refPurchaseLine);

                            #region 7.7 - 7.8 create AdjustAccInterest interest realized trans
                            // create
                            var adjustAccInterestRealizedTrans =
                                    CreateAdjustAccInterestInterestRealizedTransPostPurchase(parm.PurchaseTable, purchaseLine,
                                                            refPurchaseLine, refPurchaseLineInterestRealizedTransList.Where(w => w.RefProcessTransGUID.HasValue));
                            result.InterestRealizedTransCreate.Add(adjustAccInterestRealizedTrans);
                            #endregion
                        }
                        #endregion
                        if (purchaseLine.RollbillPurchaseLineGUID.HasValue)
                        {
                            #region 8 create inovoice settlement detail rollbill

                            var rollbillPurchaseLine = parm.RollbillPurchaseLine.Where(w => w.PurchaseLineGUID == purchaseLine.RollbillPurchaseLineGUID)
                                                                                .FirstOrDefault();
                            // 8.2
                            var rollbillPurchaseLineInvoice =
                                refPurchaseLineRollBillInvoice.Where(w => w.InvoiceTableGUID == rollbillPurchaseLine.PurchaseLineInvoiceTableGUID).FirstOrDefault();
                            // 8.3 - 8.4
                            var rollbillPurchaseLineInvSettlementDetail =
                                    CreateInvoiceSettlementDetailsPostPurchase(new List<InvoiceTable> { rollbillPurchaseLineInvoice }, null,
                                                                            new List<PurchaseLine> { rollbillPurchaseLine }, string.Empty,
                                                                            result.ReceiptTempTable.ReceiptTempTableGUID, parm.PurchaseTable.CompanyGUID,
                                                                            purchaseLine.LinePurchaseAmount);
                            newInvoiceSettlementDetailList.AddRange(rollbillPurchaseLineInvSettlementDetail);
                            #endregion
                            #region 9 create rollbill interest invoice
                            if (purchaseLine.RollbillInterestAmount > 0)
                            {
                                #region 9.1 
                                var rollbillInterestInvoiceResult =
                                    CreateInvoice1LineSpecificForPostPurchase(parm.PurchaseTable, parm.CompanyParameter.FTRollbillUnearnedInterestInvTypeGUID.Value,
                                                                                purchaseLine.RollbillInterestAmount, true, null, purchaseLine);
                                rollbillInterestInvoiceTableList.Add(rollbillInterestInvoiceResult.InvoiceTable);
                                rollbillInterestInvoiceLineList.Add(rollbillInterestInvoiceResult.InvoiceLine);
                                #endregion

                            }
                            #endregion
                            // 10 update closedForAdditionalPurchase
                            rollbillPurchaseLine.ClosedForAdditionalPurchase = true;
                            result.PurchaseLine.Add(rollbillPurchaseLine);
                        }

                        #region 9.3, 11 create interest realized trans purchase line, rollbill
                        List<InterestRealizedTrans> interestRealizedTransPerPurchaseLine = 
                                CreateInterestRealizedTransPerPurchaseLinePostPurchase(purchaseLine, parm.PurchaseTable);
                        if (interestRealizedTransPerPurchaseLine.Count > 0)
                        {
                            result.InterestRealizedTransCreate.AddRange(interestRealizedTransPerPurchaseLine);
                        }
                        #endregion
                    }
                    #endregion loop purchase lines 4 - 11

                    #region 7.9 update cancelled interest realized trans w/o process trans
                    var updateCancelledInterestRealizedTrans =
                        refPurchaseLineInterestRealizedTransList.Where(w => !w.RefProcessTransGUID.HasValue)
                            .Select(s =>
                            {
                                s.Cancelled = true;
                                return s;
                            }).ToList();
                    if (updateCancelledInterestRealizedTrans.Count > 0)
                    {
                        result.InterestRealizedTransUpdate.AddRange(updateCancelledInterestRealizedTrans);
                    }
                    #endregion

                    #region 12.1 - 12.2 create serviceFeeTrans invoice
                    List<InvoiceTable> serviceFeeInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> serviceFeeInvoiceLineList = new List<InvoiceLine>();
                    if (parm.ServiceFeeTrans.Count > 0)
                    {
                        var issuedDate = parm.PurchaseTable.PurchaseDate;
                        var customerTableGUID = parm.PurchaseTable.CustomerTableGUID;
                        var productType = (ProductType)parm.PurchaseTable.ProductType;
                        var documentId = parm.PurchaseTable.PurchaseId;
                        var creditAppTableGUID = parm.PurchaseTable.CreditAppTableGUID;
                        GenInvoiceFromServiceFeeTransParamView serviceFeeTransInvoiceParam = new GenInvoiceFromServiceFeeTransParamView
                        {
                            ServiceFeeTrans = parm.ServiceFeeTrans,
                            IssuedDate = issuedDate,
                            CustomerTableGUID = customerTableGUID,
                            ProductType = productType,
                            DocumentId = documentId,
                            CreditAppTableGUID = creditAppTableGUID
                        };
                        var serviceFeeInvoiceResult = serviceFeeTransService.GenInvoiceFromServiceFeeTrans(serviceFeeTransInvoiceParam);
                        serviceFeeInvoiceTableList.AddRange(serviceFeeInvoiceResult.InvoiceTable);
                        serviceFeeInvoiceLineList.AddRange(serviceFeeInvoiceResult.InvoiceLine);

                        // R03
                        result.IntercompanyInvoiceTable.AddRange(serviceFeeInvoiceResult.IntercompanyInvoiceTable);
                    }
                    #endregion
                    #region 13.1 create retention invoice
                    InvoiceTable purchaseRetentionInvoiceTable = null;
                    InvoiceLine purchaseRetentionInvoiceLine = null;
                    if (parm.PurchaseRetentionAmount != 0)
                    {
                        var retentionInvoiceResult =
                                CreateInvoice1LineSpecificForPostPurchase(parm.PurchaseTable, parm.CompanyParameter.FTRetentionInvTypeGUID.Value,
                                                                            (parm.PurchaseRetentionAmount * -1), true, null);
                        purchaseRetentionInvoiceLine = retentionInvoiceResult.InvoiceLine;
                        purchaseRetentionInvoiceTable = retentionInvoiceResult.InvoiceTable;
                    }
                    #endregion
                    #region 14 create paymentDetail invoice & vendorPaymentTrans
                    // R04
                    var paymentDetailsList = parm.PurchasePaymentDetail.Where(w => w.PaymentAmount != 0);
                    List<InvoiceTable> paymentDetailInvoiceTableList = new List<InvoiceTable>();
                    List<InvoiceLine> paymentDetailInoiceLineList = new List<InvoiceLine>();
                    List<VendorPaymentTrans> vendorPaymentTransList = new List<VendorPaymentTrans>();
                    List<ProcessTrans> paymentDetailProcessTrans = new List<ProcessTrans>();

                    var settleMethodOfPayment = parm.MethodOfPayment.Where(w => w.MethodOfPaymentGUID == parm.CompanyParameter.SettlementMethodOfPaymentGUID.Value)
                                                                            .FirstOrDefault();
                    var confirmedVendorPaymentStatus = parm.DocumentStatus.Where(w => w.StatusId == ((int)VendorPaymentStatus.Confirmed).ToString()).FirstOrDefault();
                    foreach (var paymentDetails in paymentDetailsList)
                    {
                        if (paymentDetails.PaidToType == (int)PaidToType.Suspense)
                        {
                            #region 14.3 create paymentDetail invoice
                            var paymDetailInvType = parm.InvoiceTypes.Where(w => w.InvoiceTypeGUID == paymentDetails.InvoiceTypeGUID.Value).FirstOrDefault();
                            var suspensePaymentDetailInvoiceResult =
                                    CreateInvoice1LineSpecificForPostPurchase(parm.PurchaseTable, paymentDetails.InvoiceTypeGUID.Value,
                                                                                (paymentDetails.PaymentAmount * -1), true, null, null,
                                                                                paymentDetails, paymDetailInvType);
                            paymentDetailInvoiceTableList.Add(suspensePaymentDetailInvoiceResult.InvoiceTable);
                            paymentDetailInoiceLineList.Add(suspensePaymentDetailInvoiceResult.InvoiceLine);
                            #endregion
                        }
                        else if (paymentDetails.PaidToType == (int)PaidToType.Customer ||
                            paymentDetails.PaidToType == (int)PaidToType.Vendor)
                        {
                            #region 14.5 create vendor payment trans
                            VendorPaymentTrans vendorPaymentTrans = CreateVendorPaymentTransPostPurchase(parm.PurchaseTable, paymentDetails,
                                                                                                    settleMethodOfPayment, confirmedVendorPaymentStatus);
                            vendorPaymentTransList.Add(vendorPaymentTrans);
                            #endregion
                        }
                    }
                    #region 14.6 generate processTrans from vendorPaymentTrans
                    if (vendorPaymentTransList.Count > 0)
                    {
                        paymentDetailProcessTrans = 
                            processTransService.GenProcessTransFromVendorPaymentTrans(vendorPaymentTransList, parm.PurchaseTable.CustomerTableGUID,
                                                                                    (ProductType)parm.PurchaseTable.ProductType, parm.PurchaseTable.PurchaseId);

                        result.VendorPaymentTrans.AddRange(vendorPaymentTransList);
                        result.ProcessTrans.AddRange(paymentDetailProcessTrans);
                    }
                    #endregion
                    // R04
                    #region 14.7 generate processTrans from SuspensePaymDetail
                    var suspensePaymDetailList = paymentDetailsList.Where(w => w.PaidToType == (int)PaidToType.Suspense).ToList();
                    if(suspensePaymDetailList.Count() > 0)
                    {
                        var processTransSuspensePaymDetail =
                            processTransService.GenProcessTransFromSuspensePaymentDetail(suspensePaymDetailList,
                                                                                    (ProductType)parm.PurchaseTable.ProductType,
                                                                                    parm.PurchaseTable.PurchaseId,
                                                                                    parm.PurchaseTable.PurchaseDate);
                        result.ProcessTrans.AddRange(processTransSuspensePaymDetail);
                    }
                    #endregion
                    #endregion

                    #region 15 create purchase invoiceSettlementDetail
                    if (parm.PurchaseInvoiceSettlementDetail.Count > 0)
                    {
                        var invoiceSettlementDetailFromPurchaseInvSettleDetail =
                            invoiceSettlementDetailService
                                .GenInvoiceSettlementDetailFromInvoiceSettlementDetail(parm.PurchaseInvoiceSettlementDetail, RefType.ReceiptTemp,
                                                                                        result.ReceiptTempTable.ReceiptTempTableGUID);
                        newInvoiceSettlementDetailList.AddRange(invoiceSettlementDetailFromPurchaseInvSettleDetail);
                    }
                    #endregion

                    // InvoiceLine.InvoiceText (from 4.3, 4.5)
                    purchaseLineInvoiceLineList = invoiceService.ReplaceInvoiceLineInvoiceText(purchaseLineInvoiceLineList);
                    #region post invoice
                    List<InvoiceTable> allInvoiceTables = new List<InvoiceTable>();
                    List<InvoiceLine> allInvoiceLines = new List<InvoiceLine>();

                    #region 4.8
                    if (purchaseLineInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(purchaseLineInvoiceTableList);
                        allInvoiceLines.AddRange(purchaseLineInvoiceLineList);
                    }
                    #endregion
                    #region 5.2
                    if (purchaseLineInterestInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(purchaseLineInterestInvoiceTableList);
                        allInvoiceLines.AddRange(purchaseLineInterestInvoiceLineList);
                    }
                    #endregion
                    #region 6.3
                    if (purchaseLinePurchaseFeeInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(purchaseLinePurchaseFeeInvoiceTableList);
                        allInvoiceLines.AddRange(purchaseLinePurchaseFeeInvoiceLineList);
                    }
                    #endregion
                    #region 6.8
                    if (purchaseLineReceiptFeeInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(purchaseLineReceiptFeeInvoiceTableList);
                        allInvoiceLines.AddRange(purchaseLineReceiptFeeInvoiceLineList);
                    }
                    #endregion
                    #region 6.13
                    if (purchaseLineBillingFeeInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(purchaseLineBillingFeeInvoiceTableList);
                        allInvoiceLines.AddRange(purchaseLineBillingFeeInvoiceLineList);
                    }
                    #endregion
                    #region 9.2
                    if (rollbillInterestInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(rollbillInterestInvoiceTableList);
                        allInvoiceLines.AddRange(rollbillInterestInvoiceLineList);
                    }
                    #endregion
                    #region 12.3
                    if (serviceFeeInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(serviceFeeInvoiceTableList);
                        allInvoiceLines.AddRange(serviceFeeInvoiceLineList);
                    }
                    #endregion
                    #region 13.2
                    if (purchaseRetentionInvoiceTable != null)
                    {
                        allInvoiceTables.Add(purchaseRetentionInvoiceTable);
                        allInvoiceLines.Add(purchaseRetentionInvoiceLine);
                    }
                    #endregion
                    #region 14.4
                    if (paymentDetailInvoiceTableList.Count > 0)
                    {
                        allInvoiceTables.AddRange(paymentDetailInvoiceTableList);
                        allInvoiceLines.AddRange(paymentDetailInoiceLineList);
                    }
                    #endregion
                    var postInvoiceResult = invoiceService.PostInvoice(allInvoiceTables, allInvoiceLines, RefType.PurchaseTable, parm.PurchaseTable.PurchaseTableGUID);
                    #endregion
                    #region create InvoiceSettlementDetail from post invoice
                    #region 5.3 - 5.4
                    if (parm.PurchaseLine.Any(a => (a.InterestAmount + a.InterestRefundAmount) > 0 &&
                                                 a.SettleInterestAmount > 0))
                    {
                        var purchaseLineSettleInterests = parm.PurchaseLine.Where(w => (w.InterestAmount + w.InterestRefundAmount) > 0 &&
                                                                                    w.SettleInterestAmount > 0).ToList();
                        var invoiceTableSettleInterests = postInvoiceResult.InvoiceTables
                                                                .Where(w => purchaseLineInterestInvoiceTableList
                                                                .Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID))
                                                                .ToList();
                        var invoiceSettlementInterests = CreateInvoiceSettlementDetailsPostPurchase(invoiceTableSettleInterests, postInvoiceResult.CustTranses,
                                                                            purchaseLineSettleInterests, PurchaseLineCondition.SettleInterestAmount,
                                                                            result.ReceiptTempTable.ReceiptTempTableGUID, parm.PurchaseTable.CompanyGUID);
                        newInvoiceSettlementDetailList.AddRange(invoiceSettlementInterests);
                    }
                    #endregion
                    #region 6.4 - 6.5
                    if (parm.PurchaseLine.Any(a => a.PurchaseFeeAmount > 0 && a.SettlePurchaseFeeAmount > 0))
                    {
                        var purchaseLineSettlePurchaseFees = parm.PurchaseLine.Where(w => w.PurchaseFeeAmount > 0 && w.SettlePurchaseFeeAmount > 0).ToList();
                        var invoiceTableSettlePurchaseFees = postInvoiceResult.InvoiceTables
                                                                .Where(w => purchaseLinePurchaseFeeInvoiceTableList
                                                                .Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID))
                                                                .ToList();
                        var invoiceSettlementPurchaseFees = 
                                CreateInvoiceSettlementDetailsPostPurchase(invoiceTableSettlePurchaseFees, postInvoiceResult.CustTranses,
                                                                        purchaseLineSettlePurchaseFees, PurchaseLineCondition.SettlePurchaseFeeAmount,
                                                                        result.ReceiptTempTable.ReceiptTempTableGUID, parm.PurchaseTable.CompanyGUID);
                        newInvoiceSettlementDetailList.AddRange(invoiceSettlementPurchaseFees);
                    }
                    #endregion
                    #region 6.9 - 6.10
                    if (parm.PurchaseLine.Any(a => a.ReceiptFeeAmount > 0 && a.SettleReceiptFeeAmount > 0))
                    {
                        var purchaseLineSettleReceiptFees = parm.PurchaseLine.Where(w => w.ReceiptFeeAmount > 0 && w.SettleReceiptFeeAmount > 0).ToList();
                        var invoiceTableSettleReceiptFees = postInvoiceResult.InvoiceTables
                                                                .Where(w => purchaseLineReceiptFeeInvoiceTableList
                                                                .Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID))
                                                                .ToList();
                        var invoiceSettlementReceiptFees = 
                            CreateInvoiceSettlementDetailsPostPurchase(invoiceTableSettleReceiptFees, postInvoiceResult.CustTranses,
                                                                        purchaseLineSettleReceiptFees, PurchaseLineCondition.SettleReceiptFeeAmount,
                                                                        result.ReceiptTempTable.ReceiptTempTableGUID, parm.PurchaseTable.CompanyGUID);
                        newInvoiceSettlementDetailList.AddRange(invoiceSettlementReceiptFees);
                    }
                    #endregion
                    #region 6.14 - 6.15
                    if (parm.PurchaseLine.Any(a => a.BillingFeeAmount > 0 && a.SettleBillingFeeAmount > 0))
                    {
                        var purchaseLineSettleBillingFees = parm.PurchaseLine.Where(w => w.BillingFeeAmount > 0 && w.SettleBillingFeeAmount > 0).ToList();
                        var invoiceTableSettleBillingFees = postInvoiceResult.InvoiceTables
                                                                .Where(w => purchaseLineBillingFeeInvoiceTableList
                                                                .Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID))
                                                                .ToList();
                        var invoiceSettlementBillingFees = 
                                CreateInvoiceSettlementDetailsPostPurchase(invoiceTableSettleBillingFees, postInvoiceResult.CustTranses,
                                                                            purchaseLineSettleBillingFees, PurchaseLineCondition.SettleBillingFeeAmount,
                                                                            result.ReceiptTempTable.ReceiptTempTableGUID, parm.PurchaseTable.CompanyGUID);
                        newInvoiceSettlementDetailList.AddRange(invoiceSettlementBillingFees);
                    }
                    #endregion
                    #region 9.4 - 9.5
                    if (parm.PurchaseLine.Any(a => a.RollbillInterestAmount > 0 && a.SettleRollBillInterestAmount > 0))
                    {
                        var purchaseLineSettleRollBillInterests = 
                            parm.PurchaseLine.Where(w => w.RollbillInterestAmount > 0 && w.SettleRollBillInterestAmount > 0).ToList();
                        var invoiceTableSettleRollBillInterests = postInvoiceResult.InvoiceTables
                                                                .Where(w => rollbillInterestInvoiceTableList
                                                                .Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID))
                                                                .ToList();
                        var invoiceSettlementRollBillInterests = 
                                CreateInvoiceSettlementDetailsPostPurchase(invoiceTableSettleRollBillInterests, postInvoiceResult.CustTranses,
                                                                            purchaseLineSettleRollBillInterests, PurchaseLineCondition.SettleRollBillInterestAmount,
                                                                            result.ReceiptTempTable.ReceiptTempTableGUID, parm.PurchaseTable.CompanyGUID);
                        newInvoiceSettlementDetailList.AddRange(invoiceSettlementRollBillInterests);
                    }
                    #endregion
                    #region 12.4 - 12.5
                    var serviceFeeTransSettleAmountList = parm.ServiceFeeTrans.Where(w => w.SettleAmount > 0).ToList();
                    if (serviceFeeTransSettleAmountList.Count > 0)
                    {
                        var invoiceServiceFeeTransList = postInvoiceResult.InvoiceTables.Where(w => w.RefType == (int)RefType.ServiceFeeTrans).ToList();

                        var serviceFeeInvoiceSettlementDetails =
                            invoiceSettlementDetailService
                                .GenInvoiceSettlementDetailFromServiceFeeTrans(serviceFeeTransSettleAmountList, invoiceServiceFeeTransList,
                                                                                parm.PurchaseTable.PurchaseId, RefType.ReceiptTemp,
                                                                                result.ReceiptTempTable.ReceiptTempTableGUID);
                        newInvoiceSettlementDetailList.AddRange(serviceFeeInvoiceSettlementDetails);
                    }
                    #endregion
                    #endregion
                    #region 16 create receiptTempPaymDetail
                    #region 16.1 - 16.2 create receiptTempPaymDetail invoiceSettlementDetail
                    var collectionInvoiceSettlementDetail = newInvoiceSettlementDetailList.Where(w => w.RefType == (int)RefType.ReceiptTemp &&
                                                                                                    w.RefGUID == result.ReceiptTempTable.ReceiptTempTableGUID);
                    if (collectionInvoiceSettlementDetail.Count() > 0)
                    {
                        ReceiptTempPaymDetail settlement = new ReceiptTempPaymDetail
                        {
                            ReceiptTempTableGUID = result.ReceiptTempTable.ReceiptTempTableGUID,
                            MethodOfPaymentGUID = parm.CompanyParameter.SettlementMethodOfPaymentGUID.Value,
                            ReceiptAmount = collectionInvoiceSettlementDetail.Sum(s => s.SettleAmount),
                            ChequeTableGUID = null,
                            TransferReference = string.Empty,
                            ReceiptTempPaymDetailGUID = Guid.NewGuid(),
                            CompanyGUID = parm.PurchaseTable.CompanyGUID
                        };
                        result.ReceiptTempPaymDetail.Add(settlement);
                    }
                    #endregion
                    #region 16.3 - 16.5 create receiptTempPaymDetail suspenseSettlementDetail
                    var collectionSuspenseSettlementDetail = 
                        newInvoiceSettlementDetailList.Where(w => w.RefType == (int)RefType.ReceiptTemp &&
                                                                w.RefGUID == result.ReceiptTempTable.ReceiptTempTableGUID &&
                                                                w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount);
                    if (collectionSuspenseSettlementDetail.Count() > 0)
                    {
                        ReceiptTempPaymDetail suspense = new ReceiptTempPaymDetail
                        {
                            ReceiptTempTableGUID = result.ReceiptTempTable.ReceiptTempTableGUID,
                            MethodOfPaymentGUID = parm.CompanyParameter.SuspenseMethodOfPaymentGUID.Value,
                            ReceiptAmount = collectionSuspenseSettlementDetail.Sum(s => s.SettleAmount) * (-1),
                            ChequeTableGUID = null,
                            TransferReference = string.Empty,
                            ReceiptTempPaymDetailGUID = Guid.NewGuid(),
                            CompanyGUID = parm.PurchaseTable.CompanyGUID
                        };
                        result.ReceiptTempPaymDetail.Add(suspense);
                        foreach (var suspenseInvSettle in collectionSuspenseSettlementDetail)
                        {
                            suspenseInvSettle.RefReceiptTempPaymDetailGUID = suspense.ReceiptTempPaymDetailGUID;
                        }
                    }
                    #endregion
                    // 16.6 update receiptTempTable
                    if (collectionSuspenseSettlementDetail.Count() > 0 || collectionInvoiceSettlementDetail.Count() > 0)
                    {
                        result.ReceiptTempTable.ReceiptAmount = result.ReceiptTempPaymDetail.Sum(s => s.ReceiptAmount);
                        result.ReceiptTempTable.SettleAmount = 
                            collectionInvoiceSettlementDetail.Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                                                                .Sum(s => s.SettleAmount);
                    }
                    #endregion
                    #region 17 post settlement
                    ViewPostSettlementResult postSettlementResult = null;
                    if (result.ReceiptTempTable != null)
                    {
                        result.ReceiptTempTable.ReceiptTempId = numberSeqService.GetNumber(parm.PurchaseTable.CompanyGUID, null,
                                                                        parm.ReceiptTempNumberSeqTable.NumberSeqTableGUID);

                        #region invoiceTable, invoiceLine, custTrans from post invoice
                        List<Guid> notPostSettlementInvoiceGuids = new List<Guid>();
                        #region not from 13.2, 14.4
                        if (purchaseRetentionInvoiceTable != null)
                        {
                            notPostSettlementInvoiceGuids.Add(purchaseRetentionInvoiceTable.InvoiceTableGUID);
                        }
                        
                        if (paymentDetailInvoiceTableList.Count > 0)
                        {
                            notPostSettlementInvoiceGuids.AddRange(paymentDetailInvoiceTableList.Select(s => s.InvoiceTableGUID));
                        }
                        #endregion

                        var invoiceTablePostInvoice = postInvoiceResult.InvoiceTables.Where(w => !notPostSettlementInvoiceGuids.Contains(w.InvoiceTableGUID));
                        var invoiceLinePostInvoice = postInvoiceResult.InvoiceLines.Where(w => !notPostSettlementInvoiceGuids.Contains(w.InvoiceTableGUID));
                        var custTransPostInvoice = postInvoiceResult.CustTranses.Where(w => !notPostSettlementInvoiceGuids.Contains(w.InvoiceTableGUID));

                        // not post settlement new custtrans
                        var custTransNotPostSettlement = postInvoiceResult.CustTranses.Where(w => notPostSettlementInvoiceGuids.Contains(w.InvoiceTableGUID));
                        result.CustTransCreate.AddRange(custTransNotPostSettlement);
                        #endregion

                        ViewPostSettlementParameter postSettlementParm = new ViewPostSettlementParameter
                        {
                            RefType = RefType.ReceiptTemp,
                            ReceiptTempTable = result.ReceiptTempTable,
                            InvoiceSettlementDetails = newInvoiceSettlementDetailList,
                            InvoiceTables = purchaseInvoiceTable.Concat(invoiceTablePostInvoice).ToList(),
                            InvoiceLines = purchaseInvoiceLine.Concat(invoiceLinePostInvoice).ToList(),
                            CustTrans = purchaseCustTrans.Concat(custTransPostInvoice).ToList(),
                            SourceRefId = parm.PurchaseTable.PurchaseId,
                            SourceRefType = RefType.PurchaseTable
                        };
                        postSettlementResult = receiptTempTableService.PostSettlement(postSettlementParm);

                    }
                    #endregion

                    #region 18 - 19 & assign results
                    // 18
                    parm.PurchaseTable.DocumentStatusGUID = parm.DocumentStatus
                                                                .Where(w => w.StatusId == ((int)PurchaseStatus.Posted).ToString())
                                                                .FirstOrDefault()
                                                                .DocumentStatusGUID;
                    parm.PurchaseTable.ReceiptTempTableGUID = result.ReceiptTempTable != null ? (Guid?)result.ReceiptTempTable.ReceiptTempTableGUID : null;
                    result.PurchaseTable = parm.PurchaseTable;

                    // 19
                    parm.CreditAppTable = UpdateCreditAppTablePostPurchase(parm.CreditAppTable, parm.PurchaseTable);
                    result.CreditAppTable = parm.CreditAppTable;

                    result.PurchaseLine.AddRange(parm.PurchaseLine);
                    result.InvoiceTable.AddRange(postInvoiceResult.InvoiceTables);
                    result.InvoiceLine.AddRange(postInvoiceResult.InvoiceLines);
                    //result.CustTransCreate.AddRange(postInvoiceResult.CustTranses);
                    result.CreditAppTrans.AddRange(postInvoiceResult.CreditAppTranses);
                    result.ProcessTrans.AddRange(postInvoiceResult.ProcessTranses);
                    result.TaxInvoiceTable.AddRange(postInvoiceResult.TaxInvoiceTables);
                    result.TaxInvoiceLine.AddRange(postInvoiceResult.TaxInvoiceLines);
                    result.RetentionTrans.AddRange(postInvoiceResult.RetentionTranses);

                    result.InvoiceSettlementDetail.AddRange(newInvoiceSettlementDetailList);

                    #region post settlement result
                    if(postSettlementResult != null)
                    {
                        if(postSettlementResult.UpdateCustTrans != null && postSettlementResult.UpdateCustTrans.Count > 0)
                        {
                            result.CustTransCreate.AddRange(postSettlementResult.UpdateCustTrans
                                                                .Where(w => !purchaseCustTrans.Any(a => a.CustTransGUID == w.CustTransGUID)));
                            result.CustTransUpdate.AddRange(postSettlementResult.UpdateCustTrans
                                                                    .Where(w => purchaseCustTrans.Any(a => a.CustTransGUID == w.CustTransGUID)));
                        }
                        if(postSettlementResult.CreateCreditAppTrans != null && postSettlementResult.CreateCreditAppTrans.Count > 0)
                            result.CreditAppTrans.AddRange(postSettlementResult.CreateCreditAppTrans);
                        if(postSettlementResult.CreateReceiptTable != null && postSettlementResult.CreateReceiptTable.Count > 0)
                            result.ReceiptTable.AddRange(postSettlementResult.CreateReceiptTable);
                        if(postSettlementResult.CreateReceiptLine != null && postSettlementResult.CreateReceiptLine.Count > 0)
                            result.ReceiptLine.AddRange(postSettlementResult.CreateReceiptLine);
                        if(postSettlementResult.CreateTaxInvoiceTable != null && postSettlementResult.CreateTaxInvoiceTable.Count > 0)
                            result.TaxInvoiceTable.AddRange(postSettlementResult.CreateTaxInvoiceTable);
                        if(postSettlementResult.CreateTaxInvoiceLine != null && postSettlementResult.CreateTaxInvoiceLine.Count > 0)
                            result.TaxInvoiceLine.AddRange(postSettlementResult.CreateTaxInvoiceLine);
                        if(postSettlementResult.CreatePaymentHistory != null && postSettlementResult.CreatePaymentHistory.Count > 0)
                            result.PaymentHistory.AddRange(postSettlementResult.CreatePaymentHistory);
                        if(postSettlementResult.CreateProcessTrans != null && postSettlementResult.CreateProcessTrans.Count > 0)
                            result.ProcessTrans.AddRange(postSettlementResult.CreateProcessTrans);
                    }
                    else
                    {
                        result.CustTransCreate.AddRange(postInvoiceResult.CustTranses);
                    }
                    #endregion
                    #endregion
                }

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private PostPurchaseParamViewMap PreparePostPurchaseParam(PurchaseTable purchaseTable)
        {
            try
            {
                PostPurchaseParamViewMap parm = new PostPurchaseParamViewMap();
                parm.PurchaseTable = purchaseTable;

                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                ICompanyRepo companyRepo = new CompanyRepo(db);
                IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                ICustTransRepo custTransRepo = new CustTransRepo(db);
                IMethodOfPaymentRepo methodOfPaymentRepo = new MethodOfPaymentRepo(db);
                IVerificationTransRepo verificationTransRepo = new VerificationTransRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IExchangeRateService exchangeRateService = new ExchangeRateService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);


                List<Guid> invoiceTableGuids = new List<Guid>();
                List<Guid> invoiceTypeGuids = new List<Guid>();
                List<Guid> methodOfPaymentGuids = new List<Guid>();
                List<string> statusIds = new List<string>();

                // get document status
                statusIds.Add(((int)PurchaseStatus.Approved).ToString());
                statusIds.Add(((int)TemporaryReceiptStatus.Posted).ToString());
                statusIds.Add(((int)InvoiceStatus.Draft).ToString());
                statusIds.Add(((int)VendorPaymentStatus.Confirmed).ToString());
                statusIds.Add(((int)PurchaseStatus.Posted).ToString());

                parm.DocumentStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(statusIds.ToArray()).ToList();

                var purchaseLines = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTable.PurchaseTableGUID);
                parm.PurchaseLine = purchaseLines.ToList();

                if (purchaseTable.AdditionalPurchase)
                {
                    parm.PurchaseLineAdditionalPurchase = purchaseLines.OrderBy(o => o.LineNum).FirstOrDefault();
                    parm.RefPurchaseLine = purchaseLineRepo
                                            .GetPurchaseLineByIdNoTracking(purchaseLines.Where(w => w.RefPurchaseLineGUID.HasValue)
                                                                                        .Select(s => s.RefPurchaseLineGUID.Value)).ToList();
                    invoiceTableGuids.AddRange(parm.RefPurchaseLine.Where(w => w.PurchaseLineInvoiceTableGUID.HasValue)
                                                                    .Select(s => s.PurchaseLineInvoiceTableGUID.Value));
                }

                parm.CreditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(purchaseTable.CreditAppTableGUID);
                parm.RetentionBalance = creditAppTableService.GetRetentionBalanceByCreditAppTableGUID(parm.CreditAppTable);
                parm.PurchaseRetentionAmount = purchaseTable.RetentionFixedAmount + purchaseLines.Sum(s => s.RetentionAmount);

                parm.PurchaseInvoiceSettlementDetail = invoiceSettlementDetailRepo.GetByReference(RefType.PurchaseTable, purchaseTable.PurchaseTableGUID);

                if (purchaseTable.Rollbill)
                {
                    parm.SumSuspenseSettleAmount = parm.PurchaseInvoiceSettlementDetail
                                                            .Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount)
                                                            .Sum(s => s.SettleAmount);
                    parm.RollbillPurchaseLine = purchaseLineRepo
                                                .GetPurchaseLineByIdNoTracking(purchaseLines.Where(w => w.RollbillPurchaseLineGUID.HasValue)
                                                                                            .Select(s => s.RollbillPurchaseLineGUID.Value)).ToList();
                    invoiceTableGuids.AddRange(parm.RollbillPurchaseLine.Where(w => w.PurchaseLineInvoiceTableGUID.HasValue)
                                                                        .Select(s => s.PurchaseLineInvoiceTableGUID.Value));
                }

                parm.SumPurchaseLineSettleAmount = purchaseLines.Sum(s => (s.SettleInterestAmount +
                                                                            s.SettlePurchaseFeeAmount +
                                                                            s.SettleBillingFeeAmount +
                                                                            s.SettleReceiptFeeAmount +
                                                                            s.SettleRollBillInterestAmount));
                var serviceFeeTrans = serviceFeeTransRepo
                                                    .GetServiceFeeTransByReferenceNoTracking(RefType.PurchaseTable, purchaseTable.PurchaseTableGUID);
                parm.ServiceFeeTrans = serviceFeeTrans.ToList();
                parm.SumServiceFeeSettleAmount = serviceFeeTrans.Sum(s => s.SettleAmount);

                var companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(purchaseTable.CompanyGUID);
                parm.CompanyParameter = companyParameter;
                parm.Company = companyRepo.GetCompanyByIdNoTracking(purchaseTable.CompanyGUID);

                var paymentDetails = paymentDetailRepo.GetPaymentDetailByReferanceNoTracking(purchaseTable.PurchaseTableGUID, (int)RefType.PurchaseTable);
                parm.PurchasePaymentDetail = paymentDetails.ToList();
                parm.SumPaymentAmount = paymentDetails.Sum(s => s.PaymentAmount);

                // get custTrans 
                invoiceTableGuids.AddRange(parm.PurchaseInvoiceSettlementDetail.Where(w => w.InvoiceTableGUID.HasValue)
                                                                                .Select(s => s.InvoiceTableGUID.Value));
                parm.CustTrans = custTransRepo.GetCustTransByInvoiceTableNoTracking(invoiceTableGuids).ToList();
                
                if (parm.RefPurchaseLine != null && parm.RefPurchaseLine.Count > 0)
                {
                    var additionalPurchaseCustTrans = parm.CustTrans.Where(w => parm.RefPurchaseLine.Any(a =>
                                                                                a.PurchaseLineInvoiceTableGUID == w.InvoiceTableGUID)).FirstOrDefault();
                    parm.RefPurchaseLineSettleAmount = additionalPurchaseCustTrans?.SettleAmount;
                }

                // get invoiceType
                var paymentDetailPaidToSuspense = parm.PurchasePaymentDetail.Where(w => w.PaymentAmount > 0 && w.PaidToType == (int)PaidToType.Suspense);
                invoiceTypeGuids.AddRange(paymentDetailPaidToSuspense.Where(w => w.InvoiceTypeGUID.HasValue).Select(s => s.InvoiceTypeGUID.Value));
                
                if (companyParameter.FTRollbillUnearnedInterestInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTRollbillUnearnedInterestInvTypeGUID.Value);
                if (companyParameter.FTInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTInvTypeGUID.Value);
                if (companyParameter.FTUnearnedInterestInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTUnearnedInterestInvTypeGUID.Value);
                if (companyParameter.FTFeeInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTFeeInvTypeGUID.Value);
                if (companyParameter.FTRetentionInvTypeGUID.HasValue) invoiceTypeGuids.Add(companyParameter.FTRetentionInvTypeGUID.Value);

                parm.InvoiceTypes = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(invoiceTypeGuids);

                // get method of payments
                if (companyParameter.SettlementMethodOfPaymentGUID.HasValue) methodOfPaymentGuids.Add(companyParameter.SettlementMethodOfPaymentGUID.Value);
                if (companyParameter.SuspenseMethodOfPaymentGUID.HasValue) methodOfPaymentGuids.Add(companyParameter.SuspenseMethodOfPaymentGUID.Value);

                parm.MethodOfPayment = methodOfPaymentRepo.GetMethodOfPaymentByIdNoTracking(methodOfPaymentGuids);

                // get approved verification trans
                parm.PurchaseLineApprovedVerificationTrans = verificationTransRepo.GetVerificationTransByReferenceAndVerificationStatusNoTracking((int)RefType.PurchaseLine,
                                                                                            parm.PurchaseLine.Select(s => s.PurchaseLineGUID),
                                                                                            VerificationDocumentStatus.Approved).ToList();
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                NumberSeqTable receiptTempNumberSeq =
                    numberSeqTableRepo.GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(parm.PurchaseTable.CompanyGUID,
                                                                                        ReferenceId.ReceiptTemp);
                parm.ReceiptTempNumberSeqTable = receiptTempNumberSeq;

                bool normalCaseSettleAmountCondition = !(purchaseTable.AdditionalPurchase || purchaseTable.Rollbill) &&
                                                (parm.PurchaseLine.Any(a => a.SettleInterestAmount > 0 ||
                                                                            a.SettlePurchaseFeeAmount > 0 ||
                                                                            a.SettleBillingFeeAmount > 0 ||
                                                                            a.SettleReceiptFeeAmount > 0 ||
                                                                            a.SettleRollBillInterestAmount > 0) ||
                                                parm.ServiceFeeTrans.Any(a => a.SettleAmount > 0) ||
                                                parm.PurchaseInvoiceSettlementDetail.Count > 0);
                parm.NormalCaseSettleAmountCondition = normalCaseSettleAmountCondition;

                parm.PurchaseCustomerTable = customerTableRepo.GetCustomerTableByIdNoTracking(parm.PurchaseTable.CustomerTableGUID);
                parm.PurchaseDateExchangeRate = exchangeRateService.GetExchangeRate(parm.PurchaseCustomerTable.CurrencyGUID,
                                                                                    parm.PurchaseTable.PurchaseDate);
                return parm;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region validation
        private bool ValidatePostPurchase(PostPurchaseParamViewMap parm)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                // 1-2
                if (parm.PurchaseTable.AdditionalPurchase)
                {
                    if (parm.RefPurchaseLineSettleAmount == null)
                    {
                        ex.AddData("ERROR.90120", "LABEL.CUSTOMER_TRANSACTION", "LABEL.INVOICE", "LABEL.REFERENCE_PURCHASE_LINE_ID");
                    }
                    else if (Convert.ToDecimal(parm.RefPurchaseLineSettleAmount) != 0)
                    {
                        ex.AddData("ERROR.90109", "LABEL.PURCHASE");
                    }

                    if (parm.RefPurchaseLine.Any(a => a.ClosedForAdditionalPurchase))
                    {
                        ex.AddData("ERROR.90103", "LABEL.REFERENCE_PURCHASE_LINE_ID");
                    }
                }
                // 3
                var approvedPurchaseStatus = parm.DocumentStatus.Where(w => w.StatusId == ((int)PurchaseStatus.Approved).ToString()).FirstOrDefault();
                if (approvedPurchaseStatus.DocumentStatusGUID != parm.PurchaseTable.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90101", "LABEL.PURCHASE");
                }
                // 4
                var netPaid = parm.PurchaseTable.Rollbill ? 0 : CalcPurchaseNetPaidByPurchaseTable(parm.PurchaseTable.PurchaseTableGUID);
                if (netPaid != parm.SumPaymentAmount)
                {
                    ex.AddData("ERROR.90040", "LABEL.NET_PAID");
                }
                // 5
                var approvedVerificationTrans = parm.PurchaseLineApprovedVerificationTrans;
                    
                var groupVerificationTrans =
                    (from purchaseLine in parm.PurchaseLine
                     join verificationTrans in approvedVerificationTrans
                     on purchaseLine.PurchaseLineGUID equals verificationTrans.RefGUID into lj
                     from verificationTrans in lj.DefaultIfEmpty()
                     select new
                     {
                         purchaseLine,
                         verificationTrans
                     }).GroupBy(g => g.purchaseLine.PurchaseLineGUID).Select(s => new { s.Key, Count = s.Select(ss => ss.verificationTrans).Count() });
                if(groupVerificationTrans.Any(a => a.Count == 0))
                {
                    ex.AddData("ERROR.90098", "LABEL.PURCHASE_LINE");
                }
                // 6
                if(parm.PurchaseLine.Any(a => !a.CustomerPDCTableGUID.HasValue))
                {
                    ex.AddData("ERROR.90042", "LABEL.CUSTOMER_PDC_ID");
                }
                // 7
                ex = ValidatePurchaseCustomerPDCAmountTolerance(parm.PurchaseTable, parm.CompanyParameter, parm.PurchaseLine, ex);
                // 8
                IMainAgreementTableRepo mainAgmTableRepo = new MainAgreementTableRepo(db);
                var signedMainAgm = mainAgmTableRepo.GetMainAgreementTableByCreditAppAndMainAgmTableStatus(parm.PurchaseTable.CreditAppTableGUID, MainAgreementStatus.Signed);
                if(signedMainAgm.Count == 0)
                {
                    ex.AddData("ERROR.90074");
                }
                // 9-10
                if(parm.PurchaseInvoiceSettlementDetail.Any(a => a.SuspenseInvoiceType != (int)SuspenseInvoiceType.None &&
                                                                a.SuspenseInvoiceType != (int)SuspenseInvoiceType.SuspenseAccount))
                {
                    ex.AddData("There are InvoiceSettlementDetails with SuspenseInvoiceType other than 'None' and 'SuspenseAccount' for this Purchase.");
                }
                var custTransList = parm.CustTrans.Where(w => 
                                        (parm.PurchaseInvoiceSettlementDetail
                                            .Where(w => w.InvoiceTableGUID.HasValue)
                                            .Select(s => s.InvoiceTableGUID.Value)).Contains(w.InvoiceTableGUID));
                var checkBalanceAmount =
                    (from invSettle in parm.PurchaseInvoiceSettlementDetail
                     join custTrans in custTransList
                     on invSettle.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                     select new
                     {
                         invSettle.BalanceAmount,
                         invSettle.SuspenseInvoiceType,
                         RemainingAmount = custTrans.TransAmount - custTrans.SettleAmount,
                         custTrans.InvoiceTableGUID
                     }).Where(w => w.BalanceAmount != w.RemainingAmount);
                
                if(checkBalanceAmount.Count() > 0)
                {
                    IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                    var invoiceTables = invoiceTableRepo.GetInvoiceTableByIdNoTracking(checkBalanceAmount.Select(s => s.InvoiceTableGUID));
                    var invoiceIds =
                        (from checkBal in checkBalanceAmount
                         join invoiceTable in invoiceTables
                         on checkBal.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID
                         select new { checkBal.SuspenseInvoiceType, invoiceTable.InvoiceId }).OrderBy(o => o.SuspenseInvoiceType);
                    foreach (var inv in invoiceIds)
                    {
                        if(inv.SuspenseInvoiceType == (int)SuspenseInvoiceType.None)
                        {
                            ex.AddData("ERROR.90046", "LABEL.INVOICE_SETTLEMENT", inv.InvoiceId, "LABEL.POST");
                        }
                        else
                        {
                            ex.AddData("ERROR.90046", "LABEL.SUSPENSE_SETTLEMENT", inv.InvoiceId, "LABEL.POST");
                        }
                    }
                }
                // 11
                ex = ValidatePurchaseAssignmentAmount(parm.PurchaseTable, parm.PurchaseLine, ex);
                // 12
                if(parm.SumServiceFeeSettleAmount == 0 && parm.SumPurchaseLineSettleAmount == 0 &&
                    parm.PurchaseInvoiceSettlementDetail.Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.None).Count() == 0 && 
                    parm.PurchaseInvoiceSettlementDetail.Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount).Count() != 0)
                {
                    ex.AddData("ERROR.90110");
                }
                // 13
                if (parm.CreditAppTable.MaxRetentionAmount > 0)
                {
                    if(parm.RetentionBalance < parm.PurchaseRetentionAmount)
                    {
                        ex.AddData("ERROR.90104");
                    }
                }
                // 14.1
                var paymentDetailPaidToSuspense = parm.PurchasePaymentDetail.Where(w => w.PaymentAmount > 0 && w.PaidToType == (int)PaidToType.Suspense);
                if(paymentDetailPaidToSuspense.Any(a => !a.InvoiceTypeGUID.HasValue))
                {
                    var missingInvoicetype = paymentDetailPaidToSuspense.Where(a => !a.InvoiceTypeGUID.HasValue);
                    foreach (var invType in missingInvoicetype)
                    {
                        ex.AddData("ERROR.90059", "LABEL.INVOICE_TYPE", "Payment detail Paid to type : Suspense");
                    }
                }
                // 14.2
                var paidToSuspenseInvoiceType = parm.InvoiceTypes.Where(w => 
                                                    paymentDetailPaidToSuspense.Any(a => a.InvoiceTypeGUID.HasValue && 
                                                                                        a.InvoiceTypeGUID.Value == w.InvoiceTypeGUID));
                if(paidToSuspenseInvoiceType.Any(a => !a.AutoGenInvoiceRevenueTypeGUID.HasValue))
                {
                    var missingAutoGenInvRevenueType = paidToSuspenseInvoiceType.Where(w => !w.AutoGenInvoiceRevenueTypeGUID.HasValue);
                    foreach (var invType in missingAutoGenInvRevenueType)
                    {
                        ex.AddData("ERROR.90118", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.INVOICE_TYPE", invType.InvoiceTypeId);
                    }
                }
                // 15-17
                if(parm.PurchaseTable.Rollbill)
                {
                    if(parm.RollbillPurchaseLine.Any(a => a.ClosedForAdditionalPurchase))
                    {
                        ex.AddData("ERROR.90103", "LABEL.ROLL_BILL_PURCHASE_LINE");
                    }
                    var sum = parm.SumSuspenseSettleAmount + parm.SumPurchaseLineSettleAmount + parm.SumServiceFeeSettleAmount + parm.PurchaseRetentionAmount;
                    if(sum != 0)
                    {
                        ex.AddData("ERROR.90045", "LABEL.SETTLE_AMOUNT", "LABEL.SUSPENSE_SETTLEMENT", 
                            "Total purchase line settle amount", "Total retention amount", "LABEL.SETTLE_AMOUNT", "LABEL.SERVICE_FEE");
                    }
                    // 17
                    if(parm.RollbillPurchaseLine.Count > 0)
                    {
                        var rollbillOutstanding =
                            (from purchaseLine in parm.PurchaseLine.Where(w => w.RollbillPurchaseLineGUID.HasValue)
                             join rollbillLine in parm.RollbillPurchaseLine.Where(w => w.PurchaseLineInvoiceTableGUID.HasValue)
                             on purchaseLine.RollbillPurchaseLineGUID.Value equals rollbillLine.PurchaseLineGUID
                             join custTrans in parm.CustTrans
                             on rollbillLine.PurchaseLineInvoiceTableGUID.Value equals custTrans.InvoiceTableGUID
                             select new
                             {
                                 RemainingAmount = custTrans.TransAmount - custTrans.SettleAmount,
                                 purchaseLine.OutstandingBuyerInvoiceAmount
                             }).Where(w => w.RemainingAmount != w.OutstandingBuyerInvoiceAmount);
                        if(rollbillOutstanding.Count() > 0)
                        {
                            ex.AddData("ERROR.90106", "LABEL.OUTSTANDING_BUYER_INVOICE_AMOUNT", "LABEL.POST");
                        }
                    }
                }
                // validate CompanyParameter
                ex = ValidatePostPurchaseCompanyParameter(parm, ex);

                if (ex.MessageList.Count > 1)
                {
                    throw ex;
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private SmartAppException ValidatePostPurchaseCompanyParameter(PostPurchaseParamViewMap parm, SmartAppException ex)
        {
            try
            {
                
                if (parm.PurchaseTable.AdditionalPurchase || parm.PurchaseTable.Rollbill)
                {
                    // 18, 20
                    if (parm.ReceiptTempNumberSeqTable.Manual)
                    {
                        ex.AddData("ERROR.00576", parm.ReceiptTempNumberSeqTable.NumberSeqCode);
                    }
                    // 19, 21
                    if(!parm.CompanyParameter.SettlementMethodOfPaymentGUID.HasValue)
                    {
                        ex.AddData("ERROR.90047", "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER");
                    }
                    
                    // 22-23
                    if (parm.PurchaseTable.Rollbill)
                    {
                        if(!parm.CompanyParameter.FTRollbillUnearnedInterestInvTypeGUID.HasValue)
                        {
                            ex.AddData("ERROR.90047", "LABEL.FT_UNEARNED_INTEREST_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                        }
                        else
                        {
                            var rollbillUnearnedInvType = parm.InvoiceTypes.Where(a => 
                                                            a.InvoiceTypeGUID == parm.CompanyParameter.FTRollbillUnearnedInterestInvTypeGUID.Value)
                                                            .FirstOrDefault();
                            if(!rollbillUnearnedInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
                            {
                                ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_UNEARNED_INTEREST_INVOICE_TYPE_ID");
                            }
                        }
                    }
                }
                // 24-25
                else
                {
                    var outstandingCondition = parm.NormalCaseSettleAmountCondition;
                    if (outstandingCondition && parm.ReceiptTempNumberSeqTable.Manual)
                    {
                        ex.AddData("ERROR.00576", parm.ReceiptTempNumberSeqTable.NumberSeqCode);
                    }
                    if(outstandingCondition && !parm.CompanyParameter.SettlementMethodOfPaymentGUID.HasValue)
                    {
                        ex.AddData("ERROR.90047", "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER");
                    }
                }
                // 26-27
                if (!parm.CompanyParameter.FTInvTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                else
                {
                    var purchaseInvType = parm.InvoiceTypes.Where(a => a.InvoiceTypeGUID == parm.CompanyParameter.FTInvTypeGUID.Value).FirstOrDefault();
                    if (!purchaseInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
                    {
                        ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_INV_TYPE_ID");
                    }
                }
                // 28
                if(!parm.CompanyParameter.FTReserveInvRevenueTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_RESERVE_INV_REVENUE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                // 29-30
                if (!parm.CompanyParameter.FTUnearnedInterestInvTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_UNEARNED_INTEREST_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                else
                {
                    var unearedInterestInvType = parm.InvoiceTypes.Where(a => 
                                                    a.InvoiceTypeGUID == parm.CompanyParameter.FTUnearnedInterestInvTypeGUID.Value).FirstOrDefault();
                    if (!unearedInterestInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
                    {
                        ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_UNEARNED_INTEREST_INV_TYPE_ID");
                    }
                }
                // 31-32
                if (!parm.CompanyParameter.FTFeeInvTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_FEE_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                else
                {
                    var feeInvType = parm.InvoiceTypes.Where(a =>
                                                    a.InvoiceTypeGUID == parm.CompanyParameter.FTFeeInvTypeGUID.Value).FirstOrDefault();
                    if (!feeInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
                    {
                        ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_FEE_INV_TYPE_ID");
                    }
                }
                // 33
                if(parm.PurchaseLine.Any(a => a.ReceiptFeeAmount != 0 ||
                                            a.BillingFeeAmount != 0) &&
                    !parm.CompanyParameter.ServiceFeeInvTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.SERVICE_FEE_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                // 34
                if(parm.PurchaseLine.Any(a => a.ReceiptFeeAmount != 0) &&
                    !parm.CompanyParameter.FTReceiptFeeInvRevenueTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_RECEIPT_FEE_INV_REVENUE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                // 35
                if (parm.PurchaseLine.Any(a => a.BillingFeeAmount != 0) &&
                    !parm.CompanyParameter.FTBillingFeeInvRevenueTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_BILLING_FEE_INV_REVENUE_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                // 36-38
                if (!parm.CompanyParameter.FTRetentionInvTypeGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.FT_RETENTION_INV_TYPE_ID", "LABEL.COMPANY_PARAMETER");
                }
                else
                {
                    var retentionInvType = parm.InvoiceTypes.Where(a =>
                                                    a.InvoiceTypeGUID == parm.CompanyParameter.FTRetentionInvTypeGUID.Value).FirstOrDefault();
                    // 37
                    if (!retentionInvType.AutoGenInvoiceRevenueTypeGUID.HasValue)
                    {
                        ex.AddData("ERROR.90047", "LABEL.AUTOGEN_INVOICE_REVENUETYPE", "LABEL.FT_RETENTION_INV_TYPE_ID");
                    }
                    // 38
                    if (parm.PurchaseRetentionAmount != 0 && retentionInvType.SuspenseInvoiceType != (int)SuspenseInvoiceType.Retention)
                    {
                        ex.AddData("ERROR.90114", "LABEL.FT_RETENTION_INV_TYPE_ID", "LABEL.SUSPENSE_INVOICE_TYPE", "ENUM.RETENTION");
                    }
                }
                // 39
                var settleMOP = parm.MethodOfPayment.Where(w => parm.CompanyParameter.SettlementMethodOfPaymentGUID.HasValue &&
                                                    w.MethodOfPaymentGUID == parm.CompanyParameter.SettlementMethodOfPaymentGUID.Value).FirstOrDefault();
                if(parm.PurchasePaymentDetail.Any(a => a.VendorTableGUID.HasValue && 
                                                    (a.PaidToType == (int)PaidToType.Customer ||
                                                    a.PaidToType == (int)PaidToType.Vendor)) &&
                    (settleMOP == null || (settleMOP != null && settleMOP.AccountNum == null)))
                {
                    ex.AddData("ERROR.90059", "LABEL.ACCOUNT_NUMBER", "LABEL.SETTLEMENT_METHOD_OF_PAYMENT_ID");
                }
                // 40
                if(parm.PurchaseInvoiceSettlementDetail.Any(a => a.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount) &&
                    !parm.CompanyParameter.SuspenseMethodOfPaymentGUID.HasValue)
                {
                    ex.AddData("ERROR.90047", "LABEL.SUSPENSE_METHOD_OF_PAYMENT_ID", "LABEL.COMPANY_PARAMETER");
                }
                return ex;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        private ReceiptTempTable CreatePostPurchaseReceiptTemp(PostPurchaseParamViewMap parm)
        {
            try
            {
                
                ReceiptTempTable result = null;
                if((parm.PurchaseTable.AdditionalPurchase || parm.PurchaseTable.Rollbill) ||
                    parm.NormalCaseSettleAmountCondition)
                {
                    result = new ReceiptTempTable
                    {
                        ReceiptTempId = null,
                        ReceiptDate = parm.PurchaseTable.PurchaseDate,
                        TransDate = parm.PurchaseTable.PurchaseDate,
                        ProductType = parm.PurchaseTable.ProductType,
                        ReceivedFrom = (int)ReceivedFrom.Customer,
                        CustomerTableGUID = parm.PurchaseTable.CustomerTableGUID,
                        BuyerTableGUID = null,
                        BuyerReceiptTableGUID = null,
                        DocumentStatusGUID = parm.DocumentStatus
                                                    .Where(w => w.StatusId == ((int)TemporaryReceiptStatus.Posted).ToString())
                                                    .FirstOrDefault()
                                                    .DocumentStatusGUID,
                        DocumentReasonGUID = null,
                        Remark = string.Empty,
                        ReceiptAmount = 0,
                        SettleFeeAmount = 0,
                        SettleAmount = 0,
                        CurrencyGUID = parm.PurchaseCustomerTable.CurrencyGUID,
                        ExchangeRate = Convert.ToDecimal(parm.PurchaseDateExchangeRate),
                        SuspenseInvoiceTypeGUID = null,
                        SuspenseAmount = 0,
                        CreditAppTableGUID = null,
                        ReceiptTempRefType = (int)ReceiptTempRefType.Product,
                        RefReceiptTempGUID = null,
                        Dimension1GUID = parm.PurchaseTable.Dimension1GUID,
                        Dimension2GUID = parm.PurchaseTable.Dimension2GUID,
                        Dimension3GUID = parm.PurchaseTable.Dimension3GUID,
                        Dimension4GUID = parm.PurchaseTable.Dimension4GUID,
                        Dimension5GUID = parm.PurchaseTable.Dimension5GUID,
                        MainReceiptTempGUID = null,
                        CompanyGUID = parm.PurchaseTable.CompanyGUID,
                        ReceiptTempTableGUID = Guid.NewGuid()
                    };
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #region create post purchase product invoice
        private InvoiceTable CreatePurchaseInvoiceTablePostPurchase(PurchaseTable purchaseTable, PurchaseLine purchaseLine, 
                                                                    CreditAppTable creditAppTable, DocumentStatus invoiceDraftStatus,
                                                                    CustomerTable customerTable, decimal exchangeRate, InvoiceType ftInvType,
                                                                    Company company)
        {
            try
            {
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                #region addressTrans
                List<Guid> addressTransGuids = new List<Guid>();
                if (creditAppTable.InvoiceAddressGUID.HasValue)
                    addressTransGuids.Add(creditAppTable.InvoiceAddressGUID.Value);
                if (creditAppTable.MailingReceipAddressGUID.HasValue)
                    addressTransGuids.Add(creditAppTable.MailingReceipAddressGUID.Value);

                var addresses = addressTransRepo.GetAddressTransByIdNoTracking(addressTransGuids);
                var invoiceAddress = !creditAppTable.InvoiceAddressGUID.HasValue ? null :
                    addresses.Where(w => w.AddressTransGUID == creditAppTable.InvoiceAddressGUID.Value).FirstOrDefault();
                var mailingInvoiceAddress = !creditAppTable.MailingReceipAddressGUID.HasValue ? null :
                    addresses.Where(w => w.AddressTransGUID == creditAppTable.MailingReceipAddressGUID.Value).FirstOrDefault();
                #endregion
                
                InvoiceTable result = new InvoiceTable
                {
                    InvoiceId = string.Empty,
                    IssuedDate = purchaseTable.PurchaseDate,
                    DueDate = purchaseLine.DueDate,
                    CustomerTableGUID = purchaseTable.CustomerTableGUID,
                    CustomerName = customerTable.Name,
                    CurrencyGUID = customerTable.CurrencyGUID,
                    ExchangeRate = exchangeRate,
                    InvoiceAddress1 = invoiceAddress?.Address1,
                    InvoiceAddress2 = invoiceAddress?.Address2,
                    TaxBranchId = invoiceAddress?.TaxBranchId,
                    TaxBranchName = invoiceAddress?.TaxBranchName,
                    MailingInvoiceAddress1 = mailingInvoiceAddress?.Address1,
                    MailingInvoiceAddress2 = mailingInvoiceAddress?.Address2,
                    DocumentStatusGUID = invoiceDraftStatus.DocumentStatusGUID,
                    InvoiceTypeGUID = ftInvType.InvoiceTypeGUID,
                    ProductInvoice = ftInvType.ProductInvoice,
                    SuspenseInvoiceType = ftInvType.SuspenseInvoiceType,
                    ProductType = purchaseTable.ProductType,
                    CreditAppTableGUID = purchaseTable.CreditAppTableGUID,
                    RefType = (int)RefType.PurchaseLine,
                    RefGUID = purchaseLine.PurchaseLineGUID,
                    DocumentId = purchaseTable.PurchaseId,
                    BuyerTableGUID = purchaseLine.BuyerTableGUID,
                    BuyerInvoiceTableGUID = purchaseLine.BuyerInvoiceTableGUID,
                    BuyerAgreementTableGUID = purchaseLine.BuyerAgreementTableGUID,
                    InvoiceAmountBeforeTax = 0,
                    InvoiceAmountBeforeTaxMST = 0,
                    TaxAmount = 0,
                    TaxAmountMST = 0,
                    InvoiceAmount = 0,
                    InvoiceAmountMST = 0,
                    MarketingPeriod = 0,
                    AccountingPeriod = 0,
                    Remark = string.Empty,
                    OrigInvoiceAmount = 0,
                    OrigTaxInvoiceAmount = 0,
                    CNReasonGUID = null,
                    Dimension1GUID = purchaseLine.Dimension1GUID,
                    Dimension2GUID = purchaseLine.Dimension2GUID,
                    Dimension3GUID = purchaseLine.Dimension3GUID,
                    Dimension4GUID = purchaseLine.Dimension4GUID,
                    Dimension5GUID = purchaseLine.Dimension5GUID,
                    MethodOfPaymentGUID = purchaseLine.MethodOfPaymentGUID,
                    RefTaxInvoiceGUID = null,
                    OrigInvoiceId = string.Empty,
                    OrigTaxInvoiceId = string.Empty,
                    RefInvoiceGUID = null,
                    InvoiceTableGUID = Guid.NewGuid(),
                    CompanyGUID = purchaseTable.CompanyGUID,
                    BranchGUID = company.DefaultBranchGUID.Value
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private InvoiceLine CreatePurchaseInvoiceLinePostPurchase(PurchaseLine purchaseLine, InvoiceTable purchaseLineInvoiceTable,
                                                                    DateTime purchaseDate, decimal invoiceAmount, Guid? invoiceRevenueType, 
                                                                    int lineNum)
        {
            try
            {
                IInvoiceLineService invoiceLineService = new InvoiceLineService(db);
                // 4.2, 4.4
                InvoiceLineAmountParamView invoiceLineAmountParam = new InvoiceLineAmountParamView
                {
                    IssuedDate = purchaseDate,
                    InvoiceAmount = invoiceAmount,
                    IncludeTax = true,
                    InvoiceRevenueTypeGUID = invoiceRevenueType
                };
                var invoiceLineAmountResult = invoiceLineService.CalcInvoiceLineFromInvoiceRevenueType(invoiceLineAmountParam);
                
                // 4.3, 4.5
                InvoiceLine result = new InvoiceLine
                {
                    InvoiceTableGUID = purchaseLineInvoiceTable.InvoiceTableGUID,
                    LineNum = lineNum,
                    Qty = invoiceLineAmountResult.Qty,
                    UnitPrice = invoiceLineAmountResult.UnitPrice,
                    TotalAmountBeforeTax = invoiceLineAmountResult.TotalAmountBeforeTax,
                    TaxAmount = invoiceLineAmountResult.TaxAmount,
                    WHTAmount = invoiceLineAmountResult.WHTAmount,
                    TotalAmount = invoiceLineAmountResult.TotalAmount,
                    WHTBaseAmount = invoiceLineAmountResult.WHTBaseAmount,
                    InvoiceRevenueTypeGUID = invoiceRevenueType.Value,
                    Dimension1GUID = purchaseLine.Dimension1GUID,
                    Dimension2GUID = purchaseLine.Dimension2GUID,
                    Dimension3GUID = purchaseLine.Dimension3GUID,
                    Dimension4GUID = purchaseLine.Dimension4GUID,
                    Dimension5GUID = purchaseLine.Dimension5GUID,
                    TaxTableGUID = invoiceLineAmountResult.TaxTableGUID,
                    WithholdingTaxTableGUID = invoiceLineAmountResult.WithholdingTaxTableGUID,
                    ProdUnitGUID = null,
                    InvoiceLineGUID = Guid.NewGuid(),
                    CompanyGUID = purchaseLineInvoiceTable.CompanyGUID,
                    BranchGUID = purchaseLineInvoiceTable.BranchGUID
                };

                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region create  Post purchase InterstRealizedTrans
        private List<InterestRealizedTrans> CreateInterestRealizedTransPerPurchaseLinePostPurchase(PurchaseLine purchaseLine, PurchaseTable purchaseTable)
        {
            try
            {
                List<InterestRealizedTrans> interestRealizedTransPerPurchaseLine = new List<InterestRealizedTrans>();
                if(purchaseLine.RollbillPurchaseLineGUID.HasValue && purchaseLine.RollbillInterestAmount > 0)
                {
                    var rollbillInterestRealizedTrans = CreateInterestRealizedTransPurchaseLinePostPurchase(purchaseLine,
                                                                                                            purchaseTable.PurchaseId,
                                                                                                            purchaseTable.ProductType,
                                                                                                            purchaseLine.OriginalInterestDate.Value,
                                                                                                            purchaseTable.PurchaseDate,
                                                                                                            purchaseLine.RollbillInterestAmount,
                                                                                                            purchaseLine.RollbillInterestDay,
                                                                                                            purchaseLine.PurchaseLineGUID,
                                                                                                            purchaseTable.CompanyGUID);
                    interestRealizedTransPerPurchaseLine.AddRange(rollbillInterestRealizedTrans);
                }
                if(purchaseLine.InterestAmount > 0)
                {
                    int initLineNum = interestRealizedTransPerPurchaseLine.Count > 0 ? interestRealizedTransPerPurchaseLine.Max(m => m.LineNum) + 1 : 1;
                    var purchaseLineInterestRealizedTrans = CreateInterestRealizedTransPurchaseLinePostPurchase(purchaseLine,
                                                                                                                purchaseTable.PurchaseId,
                                                                                                                purchaseTable.ProductType,
                                                                                                                purchaseTable.PurchaseDate,
                                                                                                                purchaseLine.InterestDate,
                                                                                                                purchaseLine.InterestAmount,
                                                                                                                purchaseLine.InterestDay,
                                                                                                                purchaseLine.PurchaseLineGUID,
                                                                                                                purchaseTable.CompanyGUID,
                                                                                                                initLineNum);
                    interestRealizedTransPerPurchaseLine.AddRange(purchaseLineInterestRealizedTrans);
                }
                
                return interestRealizedTransPerPurchaseLine;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<InterestRealizedTrans> CreateInterestRealizedTransPurchaseLinePostPurchase(PurchaseLine purchaseLine, string documentId, int productType, 
                                                                                    DateTime startDate, DateTime interestDate, 
                                                                                    decimal interestAmount, int interestDay,
                                                                                    Guid purchaseLineGUID, Guid companyGUID, int initLineNum = 1)
        {
            try
            {
                List<InterestRealizedTrans> interestRealizedTransList = new List<InterestRealizedTrans>();
                //var interestRealizedTransPurchaseLine = purchaseLineInterestRealizedTransList.Where(w => w.RefGUID == purchaseLine.PurchaseLineGUID);
                //int lineNum = interestRealizedTransPurchaseLine.Count() > 0 ? interestRealizedTransPurchaseLine.Max(m => m.LineNum) + 1 : 1;
                //var interestDate = purchaseLine.InterestDate;
                //var startDate = parm.PurchaseTable.PurchaseDate;
                int lineNum = initLineNum;
                var endDate = DateTime.MinValue;
                var interestRealizedTransInterestPerDay = (interestAmount / interestDay).Round();

                while (endDate != interestDate.AddDays(-1))
                {
                    var endOfMonth = startDate.GetEndOfMonth();
                    endDate = endOfMonth < interestDate ? endOfMonth : interestDate.AddDays(-1);
                    var interestRealizedTransInterestDay = Convert.ToInt32((endDate - startDate).TotalDays) + 1;
                    InterestRealizedTrans interestRealizedTrans = new InterestRealizedTrans
                    {
                        DocumentId = documentId,
                        ProductType = productType,
                        LineNum = lineNum,
                        StartDate = startDate,
                        EndDate = endDate,
                        AccountingDate = endOfMonth,
                        InterestDay = Convert.ToInt32((endDate - startDate).TotalDays) + 1,
                        InterestPerDay = interestRealizedTransInterestPerDay,
                        AccInterestAmount = endDate == interestDate.AddDays(-1) ? 
                            interestAmount - interestRealizedTransList.Sum(s => s.AccInterestAmount) :
                            (interestRealizedTransInterestPerDay * interestRealizedTransInterestDay).Round(),
                        Accrued = false,
                        RefType = (int)RefType.PurchaseLine,
                        RefGUID = purchaseLineGUID,
                        InterestRealizedTransGUID = Guid.NewGuid(),
                        CompanyGUID = companyGUID,
                        Cancelled = false,
                        RefProcessTransGUID = null,
                        Dimension1GUID = purchaseLine.Dimension1GUID,
                        Dimension2GUID = purchaseLine.Dimension2GUID,
                        Dimension3GUID = purchaseLine.Dimension3GUID,
                        Dimension4GUID = purchaseLine.Dimension4GUID,
                        Dimension5GUID = purchaseLine.Dimension5GUID,
                    };
                    interestRealizedTransList.Add(interestRealizedTrans);
                    startDate = endDate.AddDays(1);
                    lineNum += 1;
                }
                return interestRealizedTransList;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private InterestRealizedTrans CreateAdjustAccInterestInterestRealizedTransPostPurchase(PurchaseTable purchaseTable, PurchaseLine purchaseLine,
                                                                                                PurchaseLine refPurchaseLine, 
                                                                                                IEnumerable<InterestRealizedTrans> refPurchaseLineIntRealTrans)
        {
            try
            {
                #region 7.7 - 7.8 create adjustAccInterest interest realized trans
                var refPurchaseInterestAmount = refPurchaseLine.InterestAmount;
                var refPurchaseActualInterestAmount = refPurchaseInterestAmount + purchaseLine.InterestRefundAmount;
                var sumInterestRealizedProcessTrans = refPurchaseLineIntRealTrans.Sum(s => s.AccInterestAmount);
                var adjustAccInterest = refPurchaseActualInterestAmount - sumInterestRealizedProcessTrans;

                DateTime startDate = purchaseTable.PurchaseDate;
                DateTime endOfMonth = startDate.GetEndOfMonth();
                DateTime endDate = endOfMonth < purchaseLine.InterestDate ? endOfMonth : purchaseLine.InterestDate.AddDays(-1);
                InterestRealizedTrans result = new InterestRealizedTrans
                {
                    DocumentId = purchaseTable.PurchaseId,
                    ProductType = purchaseTable.ProductType,
                    LineNum = 1,
                    StartDate = startDate,
                    EndDate = endDate,
                    AccountingDate = endDate,
                    InterestDay = 0,
                    InterestPerDay = 0,
                    AccInterestAmount = adjustAccInterest,
                    Accrued = false,
                    RefType = (int)RefType.PurchaseLine,
                    RefGUID = purchaseLine.PurchaseLineGUID,
                    InterestRealizedTransGUID = Guid.NewGuid(),
                    CompanyGUID = purchaseTable.CompanyGUID,
                    Cancelled = false,
                    RefProcessTransGUID = null,
                    Dimension1GUID = purchaseLine.Dimension1GUID,
                    Dimension2GUID = purchaseLine.Dimension2GUID,
                    Dimension3GUID = purchaseLine.Dimension3GUID,
                    Dimension4GUID = purchaseLine.Dimension4GUID,
                    Dimension5GUID = purchaseLine.Dimension5GUID,
                };
                return result;
                #endregion
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region create post purchase InvoiceSettlementDetail 
        private List<InvoiceSettlementDetail> CreateInvoiceSettlementDetailsPostPurchase(List<InvoiceTable> invoiceTableList, List<CustTrans> custTransList,
                                                                                                List<PurchaseLine> purchaseLines, string settleAmountProp,
                                                                                                Guid receiptTempTableGUID, Guid companyGUID, 
                                                                                                decimal? settleAmountRefPurchaseRollbill = null)
        {
            try
            {
                IInvoiceSettlementDetailService invSettlementDetailService = new InvoiceSettlementDetailService(db);
                IInvoiceService invoiceService = new InvoiceService(db);
                List<InvoiceSettlementDetail> result = new List<InvoiceSettlementDetail>();
                
                foreach (var purchaseLine in purchaseLines)
                {
                    var invoiceTables = (invoiceTableList.Count == 1) ? 
                            invoiceTableList : invoiceTableList.Where(w => w.RefGUID == purchaseLine.PurchaseLineGUID).ToList();
                    var custTransByInvoices = custTransList != null ? 
                            custTransList.Where(w => invoiceTables.Any(a => a.InvoiceTableGUID == w.InvoiceTableGUID)).ToList() : null;
                    var settleAmountValue = GetSettleAmountForCreateInvSettlementDetailPurchaseLinePostPurchase(settleAmountProp, purchaseLine);

                    for (int i=0; i< invoiceTables.Count; i++)
                    {
                        var invoiceTable = invoiceTables[i];
                        var custTrans = custTransByInvoices != null ? custTransByInvoices[i] : null;
                        CalcSettleInvoiceAmountResultView calcSettleInvoiceAmountResult = null;
                        CalcSettlePurchaseAmountResultView calcSettlePurchaseAmountResult = null;
                        // 7 - 8
                        if(settleAmountRefPurchaseRollbill.HasValue)
                        {
                            calcSettlePurchaseAmountResult = 
                                invSettlementDetailService.CalcSettlePurchaseAmount(invoiceTable.InvoiceTableGUID, settleAmountRefPurchaseRollbill.Value);
                        }
                        // 5, 6, 9
                        else
                        {
                            calcSettleInvoiceAmountResult = invoiceService.CalcSettleInvoiceAmount(invoiceTable, custTrans, settleAmountValue, 0, false);
                        }
                        
                        InvoiceSettlementDetail invoiceSettlementDetail = new InvoiceSettlementDetail
                        {
                            ProductType = invoiceTable.ProductType,
                            DocumentId = invoiceTable.DocumentId,
                            InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                            InvoiceDueDate = invoiceTable.DueDate,
                            InvoiceTypeGUID = invoiceTable.InvoiceTypeGUID,
                            SuspenseInvoiceType = invoiceTable.SuspenseInvoiceType,
                            InvoiceAmount = invoiceTable.InvoiceAmount,
                            BalanceAmount = settleAmountRefPurchaseRollbill.HasValue ? 
                                    calcSettlePurchaseAmountResult.BalanceAmount : calcSettleInvoiceAmountResult.BalanceAmount,
                            SettleAmount = settleAmountRefPurchaseRollbill.HasValue ?
                                    settleAmountRefPurchaseRollbill.Value : calcSettleInvoiceAmountResult.SettleAmount,
                            WHTAmount = settleAmountRefPurchaseRollbill.HasValue ? 
                                    calcSettlePurchaseAmountResult.WHTAmount : calcSettleInvoiceAmountResult.WHTAmount,
                            SettleInvoiceAmount = settleAmountRefPurchaseRollbill.HasValue ? 
                                    calcSettlePurchaseAmountResult.SettleInvoiceAmount : calcSettleInvoiceAmountResult.SettleInvoiceAmount,
                            SettleTaxAmount = settleAmountRefPurchaseRollbill.HasValue ? 
                                    calcSettlePurchaseAmountResult.SettleTaxAmount : calcSettleInvoiceAmountResult.SettleTaxAmount,
                            WHTSlipReceivedByCustomer = false,
                            WHTSlipReceivedByBuyer = false,
                            BuyerAgreementTableGUID = settleAmountRefPurchaseRollbill.HasValue ? invoiceTable.BuyerAgreementTableGUID: null,
                            AssignmentAgreementTableGUID = null,
                            SettleAssignmentAmount = 0,
                            PurchaseAmount = settleAmountRefPurchaseRollbill.HasValue ? calcSettlePurchaseAmountResult.PurchaseAmount : 0,
                            PurchaseAmountBalance = settleAmountRefPurchaseRollbill.HasValue ? calcSettlePurchaseAmountResult.PurchaseAmountBalance : 0,
                            SettlePurchaseAmount = settleAmountRefPurchaseRollbill.HasValue ? calcSettlePurchaseAmountResult.SettlePurchaseAmount : 0,
                            SettleReserveAmount = settleAmountRefPurchaseRollbill.HasValue ? calcSettlePurchaseAmountResult.SettleReserveAmount : 0,
                            InterestCalculationMethod = (int)InterestCalculationMethod.Minimum,
                            RetentionAmountAccum = 0,
                            RefType = (int)RefType.ReceiptTemp,
                            RefGUID = receiptTempTableGUID,
                            RefReceiptTempPaymDetailGUID = null,
                            InvoiceSettlementDetailGUID = Guid.NewGuid(),
                            CompanyGUID = companyGUID
                        };
                        result.Add(invoiceSettlementDetail);
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private decimal GetSettleAmountForCreateInvSettlementDetailPurchaseLinePostPurchase(string fieldName, PurchaseLine purchaseLine)
        {
            try
            {
                switch(fieldName)
                {
                    case PurchaseLineCondition.SettleBillingFeeAmount: return purchaseLine.SettleBillingFeeAmount;
                    case PurchaseLineCondition.SettleInterestAmount: return purchaseLine.SettleInterestAmount;
                    case PurchaseLineCondition.SettlePurchaseFeeAmount: return purchaseLine.SettlePurchaseFeeAmount;
                    case PurchaseLineCondition.SettleReceiptFeeAmount: return purchaseLine.SettleReceiptFeeAmount;
                    case PurchaseLineCondition.SettleRollBillInterestAmount: return purchaseLine.SettleRollBillInterestAmount;
                    default: return 0;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region create post purchase Invoice1LineSpecific
        private GenInvoiceResultView CreateInvoice1LineSpecificForPostPurchase(PurchaseTable purchaseTable, Guid invoiceTypeGuid,
                                                                                    decimal invoiceAmount, bool autoGenInvRevenueType, 
                                                                                    Guid? invoiceRevenueTypeGuid, PurchaseLine purchaseLine = null,
                                                                                    PaymentDetail paymentDetail = null, InvoiceType paymDetailInvoiceType = null)
        {
            try
            {
                IInvoiceService invoiceService = new InvoiceService(db);
                
                RefType refType = purchaseLine != null ? RefType.PurchaseLine : (paymentDetail != null ? RefType.PaymentDetail : RefType.PurchaseTable);
                Guid refGuid = purchaseLine != null ? purchaseLine.PurchaseLineGUID : 
                        (paymentDetail != null ? paymentDetail.PaymentDetailGUID : purchaseTable.PurchaseTableGUID);
                Guid customerTableGuid = paymentDetail != null ? 
                        (paymentDetail.CustomerTableGUID.HasValue ? paymentDetail.CustomerTableGUID.Value : Guid.Empty) : purchaseTable.CustomerTableGUID;
                Guid? creditAppTableGuid = (paymentDetail != null && paymentDetail.SuspenseTransfer) ? null : (Guid?)purchaseTable.CreditAppTableGUID;

                Invoice1LineSpecificParamView invoice1LineSpecificParam = new Invoice1LineSpecificParamView
                {
                    RefType = refType,
                    RefGUID = refGuid,
                    IssuedDate = purchaseTable.PurchaseDate,
                    DueDate = purchaseTable.PurchaseDate,
                    CustomerTableGUID = customerTableGuid,
                    ProductType = paymentDetail != null ? (ProductType)paymDetailInvoiceType.ProductType : (ProductType)purchaseTable.ProductType,
                    InvoiceTypeGUID = invoiceTypeGuid,
                    AutoGenInvoiceRevenueType = autoGenInvRevenueType,
                    InvoiceAmount = invoiceAmount,
                    IncludeTax = true,
                    CompanyGUID = purchaseTable.CompanyGUID,
                    InvoiceRevenueTypeGUID = invoiceRevenueTypeGuid,
                    DocumentId = purchaseTable.PurchaseId,
                    CreditAppTableGUID = creditAppTableGuid,
                    BuyerTableGUID = null,
                    Dimension1GUID = purchaseLine != null ? purchaseLine.Dimension1GUID : purchaseTable.Dimension1GUID,
                    Dimension2GUID = purchaseLine != null ? purchaseLine.Dimension2GUID : purchaseTable.Dimension2GUID,
                    Dimension3GUID = purchaseLine != null ? purchaseLine.Dimension3GUID : purchaseTable.Dimension3GUID,
                    Dimension4GUID = purchaseLine != null ? purchaseLine.Dimension4GUID : purchaseTable.Dimension4GUID,
                    Dimension5GUID = purchaseLine != null ? purchaseLine.Dimension5GUID : purchaseTable.Dimension5GUID,
                    MethodOfPaymentGUID = null,
                    BuyerInvoiceTableGUID = null,
                    BuyerAgreementTableGUID = null,
                };
                var result = invoiceService.GenInvoice1LineSpecific(invoice1LineSpecificParam);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region update credit app table post purchase
        private CreditAppTable UpdateCreditAppTablePostPurchase(CreditAppTable creditAppTable, PurchaseTable purchaseTable)
        {
            try
            {
                ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
                
                CreditLimitType creditLimitType = creditLimitTypeRepo.GetCreditLimitTypeByIdNoTracking(creditAppTable.CreditLimitTypeGUID);
                
                if(creditLimitType.CreditLimitExpiration == (int)CreditLimitExpiration.ByTransaction)
                {
                    creditAppTable.InactiveDate = purchaseTable.PurchaseDate.AddDays(creditLimitType.InactiveDay);
                    creditAppTable.ExpiryDate = purchaseTable.PurchaseDate.AddDays(creditLimitType.CloseDay);

                    return creditAppTable;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region create vendor payment trans post purchase
        private VendorPaymentTrans CreateVendorPaymentTransPostPurchase(PurchaseTable purchaseTable, PaymentDetail paymDetail, 
                                                                        MethodOfPayment settleMethodOfPayment, DocumentStatus confirmedVendorPaymentStatus)
        {
            try
            {
                VendorPaymentTrans result = new VendorPaymentTrans
                {
                    CreditAppTableGUID = purchaseTable.CreditAppTableGUID,
                    VendorTableGUID = paymDetail.VendorTableGUID,
                    PaymentDate = purchaseTable.PurchaseDate,
                    OffsetAccount = settleMethodOfPayment.AccountNum.ToString(),
                    TaxTableGUID = null,
                    AmountBeforeTax = paymDetail.PaymentAmount,
                    TaxAmount = 0,
                    TotalAmount = paymDetail.PaymentAmount,
                    DocumentStatusGUID = confirmedVendorPaymentStatus.DocumentStatusGUID,
                    Dimension1 = purchaseTable.Dimension1GUID,
                    Dimension2 = purchaseTable.Dimension2GUID,
                    Dimension3 = purchaseTable.Dimension3GUID,
                    Dimension4 = purchaseTable.Dimension4GUID,
                    Dimension5 = purchaseTable.Dimension5GUID,
                    RefType = (int)RefType.PurchaseTable,
                    RefGUID = purchaseTable.PurchaseTableGUID,
                    VendorPaymentTransGUID = Guid.NewGuid(),
                    CompanyGUID = purchaseTable.CompanyGUID,

                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion
        #region cancel purchase
        public CancelPurchaseView GetCancelPurchaseById(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTable = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());

                CancelPurchaseView cancelPurchaseView = new CancelPurchaseView
                {
                    PurchaseTableGUID = purchaseTable.PurchaseTableGUID.GuidNullToString(),
                    CustomerTable_Values = purchaseTable.CustomerTable_Values,
                    AdditionalPurchase = purchaseTable.AdditionalPurchase,
                    DocumentStatus_Values = SmartAppUtil.GetDropDownLabel(purchaseTable.DocumentStatus_StatusId, purchaseTable.DocumentStatus_Description),
                    PurchaseDate = purchaseTable.PurchaseDate,
                    Purchase_Values = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                    Rollbill = purchaseTable.Rollbill
                };
                return cancelPurchaseView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetCancelPurchaseValidation(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID.StringToGuid());

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus purchaseStatusDraft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)PurchaseStatus.Draft).ToString());
                DocumentStatus purchaseStatusApprove = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)PurchaseStatus.Approved).ToString());

                if (purchaseTable.DocumentStatusGUID != purchaseStatusDraft.DocumentStatusGUID && purchaseTable.DocumentStatusGUID != purchaseStatusApprove.DocumentStatusGUID)
                {
                    ex.AddData("ERROR.90133", new string[] { "LABEL.PURCHASE" });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public CancelPurchaseResultView CancelPurchase(CancelPurchaseView purchaseTable)
        {
            try
            {
                // validate
                GetCancelPurchaseValidation(purchaseTable.PurchaseTableGUID);
                CancelPurchaseResultView result = new CancelPurchaseResultView();
                NotificationResponse success = new NotificationResponse();
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTableItemView = purchaseTableRepo.GetByIdvw(purchaseTable.PurchaseTableGUID.StringToGuid());

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus purchaseStatusCancel = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)PurchaseStatus.Cancelled).ToString());

                purchaseTableItemView.DocumentStatusGUID = purchaseStatusCancel.DocumentStatusGUID.GuidNullToString();
                purchaseTableItemView.DocumentReasonGUID = purchaseTable.DocumentReasonGUID;

                UpdatePurchaseTable(purchaseTableItemView);

                success.AddData("SUCCESS.90008", new string[] { "LABEL.PURCHASE", SmartAppUtil.GetDropDownLabel(purchaseTableItemView.PurchaseId, purchaseTableItemView.Description) });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region UpdateStatusRollbillPurchase
        public UpdateStatusRollbillPurchaseView GetUpdateStatusRollbillPurchaseById(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTable = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());

                UpdateStatusRollbillPurchaseView updateStatusRollbillPurchaseView = new UpdateStatusRollbillPurchaseView
                {
                    PurchaseTableGUID = purchaseTable.PurchaseTableGUID.GuidNullToString(),
                    PurchaseId = SmartAppUtil.GetDropDownLabel(purchaseTable.PurchaseId, purchaseTable.Description),
                };
                return updateStatusRollbillPurchaseView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public bool GetUpdateStatusRollbillPurchaseValidation(string purchaseTableGUID, int ApprovedResult)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(purchaseTableGUID.StringToGuid());

                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                #region condition 1
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                Guid verificaitonApproveStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(VerificationDocumentStatus.Approved).ToString()).DocumentStatusGUID;
                List<VerificationTable> verificationTables = verificationTableRepo.GetVerificationTableByPurchaseTableNoTracking(purchaseTable.PurchaseTableGUID).ToList();

                if (!verificationTables.Any(t => t.DocumentStatusGUID == verificaitonApproveStatus))
                {
                    ex.AddData("ERROR.90098", "LABEL.PURCHASE_LINE");
                }
                #endregion

                #region condition 2
                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                List<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTable.PurchaseTableGUID).ToList();
                if (purchaseLines.Any(t => t.CustomerPDCTableGUID == null))
                {
                    ex.AddData("ERROR.90042", "LABEL.CUSTOMER_PDC_ID");
                }
                #endregion

                #region condition 3
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(purchaseTable.CompanyGUID);
                ex = ValidatePurchaseCustomerPDCAmountTolerance(purchaseTable, companyParameter, purchaseLines, ex);
                #endregion

                #region condition 4
                if (purchaseTable.Rollbill)
                {
                    IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                    IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);

                    decimal purchaseRetentionAmount = purchaseTable.RetentionFixedAmount + purchaseLines.Sum(s => s.RetentionAmount);
                    decimal sumSuspenseSettleAmount = 0;
                    decimal sumPurchaseLineSettleAmount = purchaseLines.Sum(s => (s.SettleInterestAmount +
                                                                s.SettlePurchaseFeeAmount +
                                                                s.SettleBillingFeeAmount +
                                                                s.SettleReceiptFeeAmount +
                                                                s.SettleRollBillInterestAmount));
                    var serviceFeeTrans = serviceFeeTransRepo
                                        .GetServiceFeeTransByReferenceNoTracking(RefType.PurchaseTable, purchaseTable.PurchaseTableGUID);
                    decimal sumServiceFeeSettleAmount = serviceFeeTrans.Sum(s => s.SettleAmount);
                    decimal sum = sumSuspenseSettleAmount + sumPurchaseLineSettleAmount + sumServiceFeeSettleAmount + purchaseRetentionAmount;

                    List<InvoiceSettlementDetail> purchaseInvoiceSettlementDetail = invoiceSettlementDetailRepo.GetByReference(RefType.PurchaseTable, purchaseTable.PurchaseTableGUID);

                    sumSuspenseSettleAmount = purchaseInvoiceSettlementDetail
                                                            .Where(w => w.SuspenseInvoiceType == (int)SuspenseInvoiceType.SuspenseAccount)
                                                            .Sum(s => s.SettleAmount);
                    if (sum != 0)
                    {
                        ex.AddData("ERROR.90045", "LABEL.SETTLE_AMOUNT", "LABEL.SUSPENSE_SETTLEMENT",
                            "Total purchase line settle amount", "Total retention amount", "LABEL.SETTLE_AMOUNT", "LABEL.SERVICE_FEE");
                    }
                }
                #endregion

                #region condition 5
                if(ApprovedResult == (int)ApprovalDecision.None)
                {
                    ex.AddData("ERROR.90057", "LABEL.APPROVED_RESULT");
                }
                #endregion

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public UpdateStatusRollbillPurchaseResultView UpdateStatusRollbillPurchase(UpdateStatusRollbillPurchaseView input)
        {
            try
            {
                // validate
                GetUpdateStatusRollbillPurchaseValidation(input.PurchaseTableGUID, (int)input.ApprovedResult);
                UpdateStatusRollbillPurchaseResultView result = new UpdateStatusRollbillPurchaseResultView();
                NotificationResponse success = new NotificationResponse();
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTableItemView = purchaseTableRepo.GetByIdvw(input.PurchaseTableGUID.StringToGuid());

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus purchaseStatusApproved = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)PurchaseStatus.Approved).ToString());
                DocumentStatus purchaseStatusRejected = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)PurchaseStatus.Rejected).ToString());

                if(input.ApprovedResult == ApprovalDecision.Approved)
                {
                    purchaseTableItemView.DocumentStatusGUID = purchaseStatusApproved.DocumentStatusGUID.GuidNullToString();
                    UpdatePurchaseTable(purchaseTableItemView);
                }
                else if(input.ApprovedResult == ApprovalDecision.Rejected)
                {
                    purchaseTableItemView.DocumentStatusGUID = purchaseStatusRejected.DocumentStatusGUID.GuidNullToString();
                    UpdatePurchaseTable(purchaseTableItemView);
                }

                success.AddData("SUCCESS.00075", new string[] { purchaseTableItemView.PurchaseId });
                result.Notification = success;
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion

        #region workflow
        public WorkflowPurchaseView GetWorkFlowPurchaseById(string id)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                IEmployeeTableService employeeTableService = new EmployeeTableService(db);
                PurchaseTableItemView item = purchaseTableRepo.GetByIdvw(id.StringToGuid());
                string userId = db.GetUserId();
                WorkflowPurchaseView workFlowPurchaseView = new WorkflowPurchaseView();
                workFlowPurchaseView.CompanyGUID = item.CompanyGUID;
                workFlowPurchaseView.CompanyId = item.CompanyId;
                workFlowPurchaseView.Owner = item.Owner;
                workFlowPurchaseView.OwnerBusinessUnitGUID = item.OwnerBusinessUnitGUID;
                workFlowPurchaseView.OwnerBusinessUnitId = item.OwnerBusinessUnitId;
                workFlowPurchaseView.CreatedBy = item.CreatedBy;
                workFlowPurchaseView.CreatedDateTime = item.CreatedDateTime;
                workFlowPurchaseView.ModifiedBy = item.ModifiedBy;
                workFlowPurchaseView.ModifiedDateTime = item.ModifiedDateTime;
                workFlowPurchaseView.PurchaseTableGUID = item.PurchaseTableGUID;
                workFlowPurchaseView.ProcessInstanceId = item.ProcessInstanceId;
                workFlowPurchaseView.RowVersion = item.RowVersion;

                PurchaseTableItemView resultUpdatePurchaseK2 = GetUpdatePurchaseWorkflowConditionK2(item.PurchaseTableGUID);

                // parm
                workFlowPurchaseView.ParmRollbill = item.Rollbill;
                workFlowPurchaseView.ParmSumPurchaseAmount = resultUpdatePurchaseK2.SumPurchaseAmount;
                workFlowPurchaseView.ParmOverCreditBuyer = resultUpdatePurchaseK2.OverCreditBuyer;
                workFlowPurchaseView.ParmOverCreditCA = resultUpdatePurchaseK2.OverCreditCA;
                workFlowPurchaseView.ParmOverCreditCALine = resultUpdatePurchaseK2.OverCreditCALine;
                workFlowPurchaseView.ParmDiffChequeIssuedName = resultUpdatePurchaseK2.DiffChequeIssuedName;
                workFlowPurchaseView.ParmCreditAppTableGUID = item.CreditAppTableGUID;
                workFlowPurchaseView.ParmDocID = item.PurchaseId;
                workFlowPurchaseView.ParmProductType = ConditionService.EnumToList<ProductType>(true)
                                                                               .Find(f => f.code == item.ProductType).status;

                // item view
                workFlowPurchaseView.CreditApplicationId = item.CreditAppTable_Values;
                workFlowPurchaseView.CustomerId = item.CustomerTable_Values;
                workFlowPurchaseView.CreditApplicationId = item.CreditAppTable_Values;
                workFlowPurchaseView.Rollbill = item.Rollbill;
                workFlowPurchaseView.PurchaseId = SmartAppUtil.GetDropDownLabel(item.PurchaseId,item.Description);
                workFlowPurchaseView.DocumentStatus_StatusId = item.DocumentStatus_StatusId;
                workFlowPurchaseView.NetPaid = (item.Rollbill == false) ? CalcPurchaseNetPaidByPurchaseTable(item.PurchaseTableGUID.StringToGuid()) : 0;

                // for ParmFolio
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                IEmployeeTableRepo employeeTableRepo = new EmployeeTableRepo(db);
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(item.CustomerTableGUID.StringToGuid());
                string employee_name = "";
                if (customerTable.ResponsibleByGUID != null)
                {
                    EmployeeTable employeeTable = employeeTableRepo.GetByEmployeeGUIDNoTracking(customerTable.ResponsibleByGUID);
                    employee_name = employeeTable.Name;
                }
                workFlowPurchaseView.ParmFolio = "{{0}} : " + item.PurchaseId + " {{1}} " + db.GetUserName() + " {{2}} " + customerTable.Name + " MKT : " + employee_name + " {{3}} " + item.PurchaseDate + " " + String.Format("{0:n}", workFlowPurchaseView.NetPaid);

                return workFlowPurchaseView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> ValidateAndStartWorkflow(WorkflowPurchaseView item)
        {
            try
            {
                ValidatetStartWorkflow(item);
                ValidateActionWorkflow(item);
                UpdatePurchaseWorkflowConditionK2(item.PurchaseTableGUID);
                IK2Service k2Service = new K2Service(db);
                return await k2Service.StartWorkflow(item.WorkflowInstance);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidatetStartWorkflow(WorkflowPurchaseView item)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(item.PurchaseTableGUID.StringToGuid());

                // check row version
                purchaseTable.CheckRowVersion(item);

                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                Guid draft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)PurchaseStatus.Draft).ToString()).DocumentStatusGUID;

                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(purchaseTable.CompanyGUID);

                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(purchaseTable.CustomerTableGUID);

                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");

                if (!(purchaseTable.DocumentStatusGUID == draft))
                {
                    smartAppException.AddData("ERROR.90012", "LABEL.PURCHASE");
                }

                if (customerTable.ResponsibleByGUID == null)
                {
                    smartAppException.AddData("ERROR.90047", "LABEL.RESPONSIBLE_BY", "LABEL.CUSTOMER");
                }

                if (smartAppException.Data.Count > 0)
                {
                    throw smartAppException;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> ValidateAndActionWorkflow(WorkflowPurchaseView item)
        {
            try
            {
                ValidateActionWorkflow(item);
                IK2Service k2Service = new K2Service(db);
                return await k2Service.ActionWorkflow(item.WorkflowInstance);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void ValidateActionWorkflow(WorkflowPurchaseView item)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTracking(item.PurchaseTableGUID.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                DocumentStatus documentStatus = documentStatusRepo.GetDocumentStatusByIdNoTracking((purchaseTable.DocumentStatusGUID));
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(purchaseTable.CompanyGUID);
                SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
                int status = Convert.ToInt32(documentStatus.StatusId);
                int waitingForMarketingStaff = (int)PurchaseStatus.WaitingForOperationStaff;

                IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                List<PurchaseLine> purchaseLines = purchaseLineRepo.GetPurchaseLineByPurchaseTableNoTracking(purchaseTable.PurchaseTableGUID).ToList();

                IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);

                // check row version
                purchaseTable.CheckRowVersion(item);

                // 100 - 110
                if (status <= waitingForMarketingStaff)
                {
                    #region condition 1
                    decimal sumPaymentAmount = 0;
                    IPaymentDetailRepo paymentDetailRepo = new PaymentDetailRepo(db);
                    List<PaymentDetail> paymentDetails = paymentDetailRepo.GetPaymentDetailByReferanceNoTracking(purchaseTable.PurchaseTableGUID, (int)RefType.PurchaseTable).ToList();
                    if(paymentDetails.Count() > 0 )
                    {
                        sumPaymentAmount = paymentDetails.Sum(t => t.PaymentAmount);
                    }

                    if(sumPaymentAmount != item.NetPaid)
                    {
                        smartAppException.AddData("ERROR.90040", "LABEL.NET_PAID");
                    }
                    #endregion condition 1

                    #region condition 2
                    IVerificationTableRepo verificationTableRepo = new VerificationTableRepo(db);
                    Guid verificaitonApproveStatus = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(Convert.ToInt32(VerificationDocumentStatus.Approved).ToString()).DocumentStatusGUID;
                    List<VerificationTable> verificationTables = verificationTableRepo.GetVerificationTableByPurchaseTableNoTracking(purchaseTable.PurchaseTableGUID).ToList();

                    if(!verificationTables.Any(t => t.DocumentStatusGUID == verificaitonApproveStatus))
                    {
                        smartAppException.AddData("ERROR.90098", "LABEL.PURCHASE_LINE");
                    }
                    #endregion condition 2

                    #region condition 3
                    if(purchaseLines.Any(t => t.CustomerPDCTableGUID == null))
                    {
                        smartAppException.AddData("ERROR.90042", "LABEL.CUSTOMER_PDC_ID");
                    }
                    #endregion

                    #region condition 4
                    smartAppException = ValidatePurchaseCustomerPDCAmountTolerance(purchaseTable, companyParameter, purchaseLines, smartAppException);
                    #endregion
                    #region condition 5
                    smartAppException = ValidatePurchaseAssignmentAmount(purchaseTable, purchaseLines, smartAppException);
                    #endregion

                    #region condition 6
                    if (purchaseLines.Sum(t => t.LinePurchaseAmount) <= 0 )
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.TOTAL_LINE_PURCHASE_AMOUNT");
                    }
                    #endregion
                    #region condition 7
                    if (purchaseLines.Any(t => t.BuyerInvoiceAmount <= 0))
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.BUYER_INVOICE_AMOUNT");
                    }
                    #endregion
                    #region condition 8
                    if (purchaseLines.Any(t => t.PurchaseAmount <= 0))
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.PURCHASE_AMOUNT");
                    }
                    #endregion
                    #region condition 9
                    if (purchaseLines.Any(t => t.LinePurchaseAmount <= 0))
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.LINE_PURCHASE_AMOUNT");
                    }
                    #endregion
                    #region condition 10
                    if (paymentDetails.Any(t => t.PaymentAmount <= 0))
                    {
                        smartAppException.AddData("ERROR.GREATER_THAN_ZERO", "LABEL.PAYMENT_AMOUNT");
                    }
                    #endregion
                    #region condition 10
                    IBuyerCreditLimitByProductRepo buyerCreditLimitByProductRepo = new BuyerCreditLimitByProductRepo(db);

                    IEnumerable<PurchaseLine> purchaseLineByBuyerCreditLimitList = purchaseLineRepo.GetPurchaseLineByBuyerCreditLimitBuyer(purchaseTable.PurchaseTableGUID, (int)ProductType.Factoring);

                    foreach(PurchaseLine line in purchaseLines)
                    {
                        if (!purchaseLineByBuyerCreditLimitList.Any(t => t.PurchaseLineGUID == line.PurchaseLineGUID))
                        {
                            BuyerTable buyerTable = buyerTableRepo.GetBuyerTableByIdNoTracking(line.BuyerTableGUID);
                            smartAppException.AddData("ERROR.90126", "LABEL.BUYER_CREDIT_LIMIT_BY_PRODUCT", buyerTable.BuyerId, ProductType.Factoring.ToString() , ProductType.Factoring.ToString());
                        }
                    }
                    #endregion
                }
                if (smartAppException.Data.Count > 0)
                {
                    throw smartAppException;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region Migration
        public void CreatePurchaseTableCollection(IEnumerable<PurchaseTableItemView> purchaseTableItemView)
        {
            try
            {
                if (purchaseTableItemView.Any())
                {
                    IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                    List<PurchaseTable> purchaseTables = purchaseTableItemView.ToPurchaseTable().ToList();
                    purchaseTableRepo.ValidateAdd(purchaseTables);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(purchaseTables, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreatePurchaseLineCollection(IEnumerable<PurchaseLineItemView> purchaseLineItemView)
        {
            try
            {
                if (purchaseLineItemView.Any())
                {
                    IPurchaseLineRepo purchaseLineRepo = new PurchaseLineRepo(db);
                    List<PurchaseLine> purchaseLines = purchaseLineItemView.ToPurchaseLine().ToList();
                    purchaseLineRepo.ValidateAdd(purchaseLines);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(purchaseLines, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion Migration
        #region Report
        public PrintPurchaseView GetPrintPurchaseById(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTable = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTableItemView customerTable = customerTableRepo.GetByIdvw(purchaseTable.CustomerTableGUID.StringToGuid());

                PrintPurchaseView printPurchaseView = new PrintPurchaseView
                {
                    PurchaseId = purchaseTable.PurchaseId,
                    AdditionalPurchase = purchaseTable.AdditionalPurchase,
                    DocumentStatus = purchaseTable.DocumentStatus_Description,
                    PurchaseDate = purchaseTable.PurchaseDate,
                    CustomerId = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                    Rollbill = purchaseTable.Rollbill,
                    PurchaseTableGUID = purchaseTable.PurchaseTableGUID
                };
                return printPurchaseView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public PrintPurchaseRollbillView GetPrintPurchaseRollbillById(string purchaseTableGUID)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTableItemView purchaseTable = purchaseTableRepo.GetByIdvw(purchaseTableGUID.StringToGuid());
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                var doc_Status = documentStatusRepo.GetDocumentStatusByIdNoTracking(purchaseTable.DocumentStatusGUID.StringToGuid());
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTableItemView customerTable = customerTableRepo.GetByIdvw(purchaseTable.CustomerTableGUID.StringToGuid());

                PrintPurchaseRollbillView printPurchaseRollbillView = new PrintPurchaseRollbillView
                {
                    PurchaseTableGUID = purchaseTable.PurchaseTableGUID,
                    PurchaseId = purchaseTable.PurchaseId,
                    PurchaseDate = purchaseTable.PurchaseDate,
                    DocumentStatus = doc_Status.Description,
                    CustomerId = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
                    Rollbill = purchaseTable.Rollbill,
                    AdditionalPurchase = purchaseTable.AdditionalPurchase
                };
                return printPurchaseRollbillView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion

    }
}
