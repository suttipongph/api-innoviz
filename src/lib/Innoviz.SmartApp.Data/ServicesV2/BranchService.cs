using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IBranchService
	{

		BranchItemView GetBranchById(string id);
		BranchItemView CreateBranch(BranchItemView branchView);
		BranchItemView UpdateBranch(BranchItemView branchView);
		bool DeleteBranch(string id);
		IEnumerable<SelectItem<BranchItemView>> GetDropDownItemNotFiltered(SearchParameter search);
		IEnumerable<SelectItem<BranchItemView>> GetTaxBranchDropDown(SearchParameter search);
		AddressTransItemView GetAddressTransInitialData(string refGUID);
		ContactTransItemView GetContactTransInitialData(string refGUID);
	}
	public class BranchService : SmartAppService, IBranchService
	{
		public BranchService(SmartAppDbContext context) : base(context) { }
		public BranchService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public BranchService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public BranchService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public BranchService() : base() { }

		public BranchItemView GetBranchById(string id)
		{
			try
			{
				IBranchRepo branchRepo = new BranchRepo(db);
				return branchRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BranchItemView CreateBranch(BranchItemView branchView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				branchView = accessLevelService.AssignOwnerBU(branchView);
				IBranchRepo branchRepo = new BranchRepo(db);
				Branch branch = branchView.ToBranch();
				branch = branchRepo.CreateBranch(branch);
				base.LogTransactionCreate<Branch>(branch);
				UnitOfWork.Commit();
				return branch.ToBranchItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public BranchItemView UpdateBranch(BranchItemView branchView)
		{
			try
			{
				IBranchRepo branchRepo = new BranchRepo(db);
				Branch inputBranch = branchView.ToBranch();
				Branch dbBranch = branchRepo.Find(inputBranch.BranchGUID);
				dbBranch = branchRepo.UpdateBranch(dbBranch, inputBranch);
				base.LogTransactionUpdate<Branch>(GetOriginalValues<Branch>(dbBranch), dbBranch);
				UnitOfWork.Commit();
				return dbBranch.ToBranchItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteBranch(string item)
		{
			try
			{
				IBranchRepo branchRepo = new BranchRepo(db);
				Guid branchGUID = new Guid(item);
				Branch branch = branchRepo.Find(branchGUID);
				branchRepo.Remove(branch);
				base.LogTransactionDelete<Branch>(branch);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<BranchItemView>> GetDropDownItemNotFiltered(SearchParameter search)
        {
            try
            {
				IBranchRepo branchRepo = new BranchRepo(db);
				return branchRepo.GetDropDownItemNotFiltered();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		public IEnumerable<SelectItem<BranchItemView>> GetTaxBranchDropDown(SearchParameter search)
		{
			try
			{
				IBranchRepo branchRepo = new BranchRepo(db);
				return branchRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressTransItemView GetAddressTransInitialData(string refGUID)
		{
			try
			{
				IAddressTransService addressTransService = new AddressTransService(db);
				IBranchRepo branchRepo = new BranchRepo(db);
				string refId = branchRepo.GetBranchByIdNoTracking(refGUID.StringToGuid()).BranchId;
				return addressTransService.GetAddressTransInitialData(refId, refGUID, Models.Enum.RefType.Branch);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ContactTransItemView GetContactTransInitialData(string refGUID)
		{
			try
			{
				IContactTransService ContactTransService = new ContactTransService(db);
				IBranchRepo branchRepo = new BranchRepo(db);
				string refId = branchRepo.GetBranchByIdNoTracking(refGUID.StringToGuid()).BranchId;
				return ContactTransService.GetContactTransInitialData(refId, refGUID, Models.Enum.RefType.Branch);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
