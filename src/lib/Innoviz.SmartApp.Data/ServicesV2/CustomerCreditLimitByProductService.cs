using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICustomerCreditLimitByProductService
	{

		CustomerCreditLimitByProductItemView GetCustomerCreditLimitByProductById(string id);
		CustomerCreditLimitByProductItemView CreateCustomerCreditLimitByProduct(CustomerCreditLimitByProductItemView customerCreditLimitByProductView);
		CustomerCreditLimitByProductItemView UpdateCustomerCreditLimitByProduct(CustomerCreditLimitByProductItemView customerCreditLimitByProductView);
		bool DeleteCustomerCreditLimitByProduct(string id);
		void CreateCustomerCreditLimitByProductCollection(List<CustomerCreditLimitByProductItemView> customerCreditLimitByProductItemViews);
		CustomerCreditLimitByProductItemView GetCustomerCreditLimitByProductInitialDataByCustomerTable(string customerTableGUID);
	}
	public class CustomerCreditLimitByProductService : SmartAppService, ICustomerCreditLimitByProductService
	{
		public CustomerCreditLimitByProductService(SmartAppDbContext context) : base(context) { }
		public CustomerCreditLimitByProductService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CustomerCreditLimitByProductService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustomerCreditLimitByProductService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CustomerCreditLimitByProductService() : base() { }

		public CustomerCreditLimitByProductItemView GetCustomerCreditLimitByProductById(string id)
		{
			try
			{
				ICustomerCreditLimitByProductRepo customerCreditLimitByProductRepo = new CustomerCreditLimitByProductRepo(db);
				ICreditAppTableService creditAppTableService = new CreditAppTableService(db);
				CustomerCreditLimitByProductItemView customerCreditLimitByProductItem = customerCreditLimitByProductRepo.GetByIdvw(id.StringToGuid());
				customerCreditLimitByProductItem.CreditLimitBalance = creditAppTableService.GetCustomerCreditLimitBalanceByProduct(customerCreditLimitByProductItem.CustomerTableGUID, customerCreditLimitByProductItem.ProductType, customerCreditLimitByProductItem.CreditLimit);

				return customerCreditLimitByProductItem;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerCreditLimitByProductItemView CreateCustomerCreditLimitByProduct(CustomerCreditLimitByProductItemView customerCreditLimitByProductView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				customerCreditLimitByProductView = accessLevelService.AssignOwnerBU(customerCreditLimitByProductView);
				ICustomerCreditLimitByProductRepo customerCreditLimitByProductRepo = new CustomerCreditLimitByProductRepo(db);
				CustomerCreditLimitByProduct customerCreditLimitByProduct = customerCreditLimitByProductView.ToCustomerCreditLimitByProduct();
				customerCreditLimitByProduct = customerCreditLimitByProductRepo.CreateCustomerCreditLimitByProduct(customerCreditLimitByProduct);
				base.LogTransactionCreate<CustomerCreditLimitByProduct>(customerCreditLimitByProduct);
				UnitOfWork.Commit();
				return customerCreditLimitByProduct.ToCustomerCreditLimitByProductItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustomerCreditLimitByProductItemView UpdateCustomerCreditLimitByProduct(CustomerCreditLimitByProductItemView customerCreditLimitByProductView)
		{
			try
			{
				ICustomerCreditLimitByProductRepo customerCreditLimitByProductRepo = new CustomerCreditLimitByProductRepo(db);
				CustomerCreditLimitByProduct inputCustomerCreditLimitByProduct = customerCreditLimitByProductView.ToCustomerCreditLimitByProduct();
				CustomerCreditLimitByProduct dbCustomerCreditLimitByProduct = customerCreditLimitByProductRepo.Find(inputCustomerCreditLimitByProduct.CustomerCreditLimitByProductGUID);
				dbCustomerCreditLimitByProduct = customerCreditLimitByProductRepo.UpdateCustomerCreditLimitByProduct(dbCustomerCreditLimitByProduct, inputCustomerCreditLimitByProduct);
				base.LogTransactionUpdate<CustomerCreditLimitByProduct>(GetOriginalValues<CustomerCreditLimitByProduct>(dbCustomerCreditLimitByProduct), dbCustomerCreditLimitByProduct);
				UnitOfWork.Commit();
				return dbCustomerCreditLimitByProduct.ToCustomerCreditLimitByProductItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCustomerCreditLimitByProduct(string item)
		{
			try
			{
				ICustomerCreditLimitByProductRepo customerCreditLimitByProductRepo = new CustomerCreditLimitByProductRepo(db);
				Guid customerCreditLimitByProductGUID = new Guid(item);
				CustomerCreditLimitByProduct customerCreditLimitByProduct = customerCreditLimitByProductRepo.Find(customerCreditLimitByProductGUID);
				customerCreditLimitByProductRepo.Remove(customerCreditLimitByProduct);
				base.LogTransactionDelete<CustomerCreditLimitByProduct>(customerCreditLimitByProduct);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateCustomerCreditLimitByProductCollection(List<CustomerCreditLimitByProductItemView> customerCreditLimitByProductItemViews)
		{
			try
			{
				if (customerCreditLimitByProductItemViews.Any())
				{
					ICustomerCreditLimitByProductRepo customerCreditLimitByProductRepo = new CustomerCreditLimitByProductRepo(db);
					List<CustomerCreditLimitByProduct> customerCreditLimitByProduct = customerCreditLimitByProductItemViews.ToCustomerCreditLimitByProduct().ToList();
					customerCreditLimitByProductRepo.ValidateAdd(customerCreditLimitByProduct);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(customerCreditLimitByProduct, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}

		public CustomerCreditLimitByProductItemView GetCustomerCreditLimitByProductInitialDataByCustomerTable(string customerTableGUID)
		{
			try
			{
				ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
				CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(customerTableGUID.StringToGuid());
				return new CustomerCreditLimitByProductItemView { 
					CustomerTableGUID = customerTableGUID,
					CustomerTable_Values = SmartAppUtil.GetDropDownLabel(customerTable.CustomerId, customerTable.Name),
					CustomerCreditLimitByProductGUID = Guid.Empty.GuidNullToString()
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
