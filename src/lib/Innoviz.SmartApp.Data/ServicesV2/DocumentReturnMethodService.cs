using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IDocumentReturnMethodService
	{

		DocumentReturnMethodItemView GetDocumentReturnMethodById(string id);
		DocumentReturnMethodItemView CreateDocumentReturnMethod(DocumentReturnMethodItemView documentReturnMethodView);
		DocumentReturnMethodItemView UpdateDocumentReturnMethod(DocumentReturnMethodItemView documentReturnMethodView);
		bool DeleteDocumentReturnMethod(string id);
	}
	public class DocumentReturnMethodService : SmartAppService, IDocumentReturnMethodService
	{
		public DocumentReturnMethodService(SmartAppDbContext context) : base(context) { }
		public DocumentReturnMethodService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public DocumentReturnMethodService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public DocumentReturnMethodService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public DocumentReturnMethodService() : base() { }

		public DocumentReturnMethodItemView GetDocumentReturnMethodById(string id)
		{
			try
			{
				IDocumentReturnMethodRepo documentReturnMethodRepo = new DocumentReturnMethodRepo(db);
				return documentReturnMethodRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnMethodItemView CreateDocumentReturnMethod(DocumentReturnMethodItemView documentReturnMethodView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				documentReturnMethodView = accessLevelService.AssignOwnerBU(documentReturnMethodView);
				IDocumentReturnMethodRepo documentReturnMethodRepo = new DocumentReturnMethodRepo(db);
				DocumentReturnMethod documentReturnMethod = documentReturnMethodView.ToDocumentReturnMethod();
				documentReturnMethod = documentReturnMethodRepo.CreateDocumentReturnMethod(documentReturnMethod);
				base.LogTransactionCreate<DocumentReturnMethod>(documentReturnMethod);
				UnitOfWork.Commit();
				return documentReturnMethod.ToDocumentReturnMethodItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentReturnMethodItemView UpdateDocumentReturnMethod(DocumentReturnMethodItemView documentReturnMethodView)
		{
			try
			{
				IDocumentReturnMethodRepo documentReturnMethodRepo = new DocumentReturnMethodRepo(db);
				DocumentReturnMethod inputDocumentReturnMethod = documentReturnMethodView.ToDocumentReturnMethod();
				DocumentReturnMethod dbDocumentReturnMethod = documentReturnMethodRepo.Find(inputDocumentReturnMethod.DocumentReturnMethodGUID);
				dbDocumentReturnMethod = documentReturnMethodRepo.UpdateDocumentReturnMethod(dbDocumentReturnMethod, inputDocumentReturnMethod);
				base.LogTransactionUpdate<DocumentReturnMethod>(GetOriginalValues<DocumentReturnMethod>(dbDocumentReturnMethod), dbDocumentReturnMethod);
				UnitOfWork.Commit();
				return dbDocumentReturnMethod.ToDocumentReturnMethodItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteDocumentReturnMethod(string item)
		{
			try
			{
				IDocumentReturnMethodRepo documentReturnMethodRepo = new DocumentReturnMethodRepo(db);
				Guid documentReturnMethodGUID = new Guid(item);
				DocumentReturnMethod documentReturnMethod = documentReturnMethodRepo.Find(documentReturnMethodGUID);
				documentReturnMethodRepo.Remove(documentReturnMethod);
				base.LogTransactionDelete<DocumentReturnMethod>(documentReturnMethod);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
