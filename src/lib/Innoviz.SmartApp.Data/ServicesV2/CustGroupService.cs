using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICustGroupService
	{

		CustGroupItemView GetCustGroupById(string id);
		CustGroupItemView CreateCustGroup(CustGroupItemView custGroupView);
		CustGroupItemView UpdateCustGroup(CustGroupItemView custGroupView);
		bool DeleteCustGroup(string id);
	}
	public class CustGroupService : SmartAppService, ICustGroupService
	{
		public CustGroupService(SmartAppDbContext context) : base(context) { }
		public CustGroupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CustGroupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CustGroupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CustGroupService() : base() { }

		public CustGroupItemView GetCustGroupById(string id)
		{
			try
			{
				ICustGroupRepo custGroupRepo = new CustGroupRepo(db);
				return custGroupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustGroupItemView CreateCustGroup(CustGroupItemView custGroupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				custGroupView = accessLevelService.AssignOwnerBU(custGroupView);
				ICustGroupRepo custGroupRepo = new CustGroupRepo(db);
				CustGroup custGroup = custGroupView.ToCustGroup();
				custGroup = custGroupRepo.CreateCustGroup(custGroup);
				base.LogTransactionCreate<CustGroup>(custGroup);
				UnitOfWork.Commit();
				return custGroup.ToCustGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CustGroupItemView UpdateCustGroup(CustGroupItemView custGroupView)
		{
			try
			{
				ICustGroupRepo custGroupRepo = new CustGroupRepo(db);
				CustGroup inputCustGroup = custGroupView.ToCustGroup();
				CustGroup dbCustGroup = custGroupRepo.Find(inputCustGroup.CustGroupGUID);
				dbCustGroup = custGroupRepo.UpdateCustGroup(dbCustGroup, inputCustGroup);
				base.LogTransactionUpdate<CustGroup>(GetOriginalValues<CustGroup>(dbCustGroup), dbCustGroup);
				UnitOfWork.Commit();
				return dbCustGroup.ToCustGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCustGroup(string item)
		{
			try
			{
				ICustGroupRepo custGroupRepo = new CustGroupRepo(db);
				Guid custGroupGUID = new Guid(item);
				CustGroup custGroup = custGroupRepo.Find(custGroupGUID);
				custGroupRepo.Remove(custGroup);
				base.LogTransactionDelete<CustGroup>(custGroup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
