using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IRetentionConditionSetupService
	{

		RetentionConditionSetupItemView GetRetentionConditionSetupById(string id);
		RetentionConditionSetupItemView CreateRetentionConditionSetup(RetentionConditionSetupItemView retentionConditionSetupView);
		RetentionConditionSetupItemView UpdateRetentionConditionSetup(RetentionConditionSetupItemView retentionConditionSetupView);
		bool DeleteRetentionConditionSetup(string id);
	}
	public class RetentionConditionSetupService : SmartAppService, IRetentionConditionSetupService
	{
		public RetentionConditionSetupService(SmartAppDbContext context) : base(context) { }
		public RetentionConditionSetupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public RetentionConditionSetupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public RetentionConditionSetupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public RetentionConditionSetupService() : base() { }

		public RetentionConditionSetupItemView GetRetentionConditionSetupById(string id)
		{
			try
			{
				IRetentionConditionSetupRepo retentionConditionSetupRepo = new RetentionConditionSetupRepo(db);
				return retentionConditionSetupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionSetupItemView CreateRetentionConditionSetup(RetentionConditionSetupItemView retentionConditionSetupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				retentionConditionSetupView = accessLevelService.AssignOwnerBU(retentionConditionSetupView);
				IRetentionConditionSetupRepo retentionConditionSetupRepo = new RetentionConditionSetupRepo(db);
				RetentionConditionSetup retentionConditionSetup = retentionConditionSetupView.ToRetentionConditionSetup();
				retentionConditionSetup = retentionConditionSetupRepo.CreateRetentionConditionSetup(retentionConditionSetup);
				base.LogTransactionCreate<RetentionConditionSetup>(retentionConditionSetup);
				UnitOfWork.Commit();
				return retentionConditionSetup.ToRetentionConditionSetupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public RetentionConditionSetupItemView UpdateRetentionConditionSetup(RetentionConditionSetupItemView retentionConditionSetupView)
		{
			try
			{
				IRetentionConditionSetupRepo retentionConditionSetupRepo = new RetentionConditionSetupRepo(db);
				RetentionConditionSetup inputRetentionConditionSetup = retentionConditionSetupView.ToRetentionConditionSetup();
				RetentionConditionSetup dbRetentionConditionSetup = retentionConditionSetupRepo.Find(inputRetentionConditionSetup.RetentionConditionSetupGUID);
				dbRetentionConditionSetup = retentionConditionSetupRepo.UpdateRetentionConditionSetup(dbRetentionConditionSetup, inputRetentionConditionSetup);
				base.LogTransactionUpdate<RetentionConditionSetup>(GetOriginalValues<RetentionConditionSetup>(dbRetentionConditionSetup), dbRetentionConditionSetup);
				UnitOfWork.Commit();
				return dbRetentionConditionSetup.ToRetentionConditionSetupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteRetentionConditionSetup(string item)
		{
			try
			{
				IRetentionConditionSetupRepo retentionConditionSetupRepo = new RetentionConditionSetupRepo(db);
				Guid retentionConditionSetupGUID = new Guid(item);
				RetentionConditionSetup retentionConditionSetup = retentionConditionSetupRepo.Find(retentionConditionSetupGUID);
				retentionConditionSetupRepo.Remove(retentionConditionSetup);
				base.LogTransactionDelete<RetentionConditionSetup>(retentionConditionSetup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
