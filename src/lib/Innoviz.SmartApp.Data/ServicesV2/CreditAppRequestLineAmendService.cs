﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICreditAppRequestLineAmendService
	{

		CreditAppRequestLineAmendItemView GetCreditAppRequestLineAmendById(string id);
		CreditAppRequestLineAmendItemView CreateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		CreditAppRequestLineAmendItemView UpdateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		bool DeleteCreditAppRequestLineAmend(string id);
		#region InitialData
		CreditAppRequestLineAmendItemView GetCreditAppRequestLineAmendInitialData(string creditApplineGUID);
		DocumentConditionTransItemView GetDocumentConditionTransInitialListData(string amendCaLineId);
		DocumentConditionTransItemView GetDocumentConditionTransChildInitialData(string amendCaLineId);
		#endregion InitialData
		#region CrditAppRequestLine
		CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendRate(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendAssignmentMethod(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendBillingInformation(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendReceiptInformation(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		void UpdateCreditAppRequestLineByAmendBillingDocumentCondition(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		void UpdateCreditAppRequestLineByAmendReceiptDocumentCondition(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView);
		#endregion CrditAppRequestLine
		#region Function
		#region UpdateStatusAmendBuyerInfo
		UpdateStatusAmendBuyerInfoResultView UpdateStatusAmendBuyerInfo(UpdateStatusAmendBuyerInfoView model);
		#endregion UpdateStatusAmendBuyerInfo
		#endregion Function


	}
	public class CreditAppRequestLineAmendService : SmartAppService, ICreditAppRequestLineAmendService
	{
		public CreditAppRequestLineAmendService(SmartAppDbContext context) : base(context) { }
		public CreditAppRequestLineAmendService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CreditAppRequestLineAmendService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditAppRequestLineAmendService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CreditAppRequestLineAmendService() : base() { }

		#region CRUD
		public CreditAppRequestLineAmendItemView GetCreditAppRequestLineAmendById(string id)
		{
			try
			{
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				return creditAppRequestLineAmendRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestLineAmendItemView CreateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
				ICreditLimitTypeRepo creditLimitTypeRepo = new CreditLimitTypeRepo(db);
				creditAppRequestLineAmendView = accessLevelService.AssignOwnerBU(creditAppRequestLineAmendView);
				ValidateCreateCreditAppRequestLineAmend(creditAppRequestLineAmendView);
				List<CAReqCreditOutStanding> caReqCreditOutStandings = new List<CAReqCreditOutStanding>();
				List<CAReqAssignmentOutstanding> caReqAssignmentOutstanding = new List<CAReqAssignmentOutstanding>();
				List<CAReqBuyerCreditOutstanding> caReqBuyerCreditOutstanding = new List<CAReqBuyerCreditOutstanding>();
				string userName = db.GetUserName();
				#region Create CreditAppRequestTable
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.CopyCreditAppRequestForCreateCreditAppRequestLineAmend(creditAppRequestLineAmendView);
				creditAppRequestTable.CACondition = creditAppRequestLineAmendView.CreditAppRequestTable_CACondition;
				creditAppRequestTableService.GenCreditAppRequestNumberSeqCode(creditAppRequestTable);
				#endregion Create CreditAppRequestTable
				#region Create CreditAppRequestLine
				ICustTransService custTransService = new CustTransService(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.CopyCreditAppRequestLineForCreateCreditAppRequestLineAmend(creditAppRequestLineAmendView, creditAppRequestTable);
				creditAppRequestLine.CustomerBuyerOutstanding = creditAppRequestLineAmendView.CreditAppRequestTable_CustomerCreditLimit;
				creditAppRequestLine.AllCustomerBuyerOutstanding = custTransService.GetAllCustomerBuyerOutstanding(creditAppRequestTable.ProductType, creditAppRequestLine.BuyerTableGUID);
				CreditLimitType creditLimitType = creditLimitTypeRepo.Find(creditAppRequestTable.CreditLimitTypeGUID);
				creditAppRequestLineRepo.ValidateAllowAddLine(creditLimitType);
				#endregion Create CreditAppRequestLine
				#region 3. Copy ข้อมูล Credit application line
				CreditAppRequestLineAmend creditAppRequestLineAmend = creditAppRequestLineAmendView.ToCreditAppRequestLineAmend();
				CreditAppLine creditAppLine = creditAppLineRepo.Find(creditAppRequestLine.RefCreditAppLineGUID);
				creditAppRequestLineAmend.CreditAppRequestLineAmendGUID = Guid.NewGuid();
				creditAppRequestLineAmend.CreditAppRequestLineGUID = creditAppRequestLine.CreditAppRequestLineGUID;
				creditAppRequestLineAmend.OriginalCreditLimitLineRequest = creditAppLine.ApprovedCreditLimitLine;
				creditAppRequestLineAmend.OriginalMaxPurchasePct = creditAppLine.MaxPurchasePct;
				creditAppRequestLineAmend.OriginalPurchaseFeePct = creditAppLine.PurchaseFeePct;
				creditAppRequestLineAmend.OriginalPurchaseFeeCalculateBase = creditAppLine.PurchaseFeeCalculateBase;
				creditAppRequestLineAmend.OriginalBillingAddressGUID = creditAppLine.BillingAddressGUID;
				creditAppRequestLineAmend.OriginalMethodOfPaymentGUID = creditAppLine.MethodOfPaymentGUID;
				creditAppRequestLineAmend.OriginalReceiptRemark = creditAppLine.ReceiptRemark;
				creditAppRequestLineAmend.OriginalReceiptAddressGUID = creditAppLine.ReceiptAddressGUID;
				creditAppRequestLineAmend.OriginalAssignmentMethodGUID = creditAppLine.AssignmentMethodGUID;
				creditAppRequestLineAmend.OriginalAssignmentMethodRemark = creditAppLine.AssignmentMethodRemark;
				#endregion 3. Copy ข้อมูล Credit application line
				#region 4. Create Credit outstanding
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				List<CreditOutstandingViewMap> creditOustandingView = creditAppTableRepo.GetCreditOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID, creditAppRequestTable.RequestDate);
				if (creditOustandingView.Any())
				{
					creditOustandingView.ForEach(f =>
					{
						caReqCreditOutStandings.Add(new CAReqCreditOutStanding
						{
							CAReqCreditOutStandingGUID = Guid.NewGuid(),
							CustomerTableGUID = f.CustomerTableGUID,
							ProductType = f.ProductType,
							CreditLimitTypeGUID = f.CreditLimitTypeGUID,
							AccumRetentionAmount = f.AccumRetentionAmount,
							AsOfDate = f.AsOfDate,
							CreditAppTableGUID = f.CreditAppTableGUID,
							CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
							ApprovedCreditLimit = f.ApprovedCreditLimit,
							CreditLimitBalance = f.CreditLimitBalance,
							ARBalance = f.ARBalance,
							ReserveToBeRefund = f.ReserveToBeRefund,
							CompanyGUID = creditAppRequestTable.CompanyGUID,
							Owner = userName,
							OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						});
					});
				}
				#endregion 4. Create Credit outstanding
				#region 5. Create Assignment agreement outstanding 
				IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
				List<AssignmentAgreementOutstandingView> assignmentAgreementOutstandingViews = assignmentAgreementTableRepo.GetAssignmentAgreementOutstandingByCustomer(creditAppRequestTable.CustomerTableGUID);
				if (assignmentAgreementOutstandingViews.Any())
				{
					assignmentAgreementOutstandingViews.ForEach(f =>
					{
						caReqAssignmentOutstanding.Add(new CAReqAssignmentOutstanding
						{
							CAReqAssignmentOutstandingGUID = Guid.NewGuid(),
							CustomerTableGUID = f.CustomerTableGUID.StringToGuid(),
							AssignmentAgreementTableGUID = f.AssignmentAgreementTableGUID.StringToGuid(),
							BuyerTableGUID = f.BuyerTableGUID.StringToGuid(),
							AssignmentAgreementAmount = f.AssignmentAgreementAmount,
							SettleAmount = f.SettledAmount,
							RemainingAmount = f.RemainingAmount,
							CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
							CompanyGUID = creditAppRequestTable.CompanyGUID,
							Owner = userName,
							OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
						});
					});
				}
				#endregion 5. Create Assignment agreement outstanding 
				#region 6. Create CA buyer credit outstanding
				List<CALineOutstandingViewMap> caLineOutstandingViewMap = new List<CALineOutstandingViewMap>();
				if (creditAppRequestTable.RefCreditAppTableGUID.HasValue)
				{
					caLineOutstandingViewMap = creditAppLineRepo.GetCALineOutstandingByCA((int)RefType.CreditAppTable, creditAppRequestTable.RefCreditAppTableGUID.Value, true, creditAppRequestTable.RequestDate).ToList();
					if (caLineOutstandingViewMap.Any())
					{
						caLineOutstandingViewMap.ForEach(f =>
						{
							caReqBuyerCreditOutstanding.Add(new CAReqBuyerCreditOutstanding
							{
								CAReqBuyerCreditOutstandingGUID = Guid.NewGuid(),
								CreditAppRequestTableGUID = creditAppRequestTable.CreditAppRequestTableGUID,
								CreditAppTableGUID = f.CreditAppTableGUID,
								CreditAppLineGUID = f.CreditAppLineGUID,
								LineNum = f.LineNum,
								BuyerTableGUID = f.BuyerTableGUID,
								Status = f.Status,
								ApprovedCreditLimitLine = f.ApprovedCreditLimitLine,
								ARBalance = f.ARBalance,
								AssignmentMethodGUID = f.AssignmentMethodGUID,
								BillingResponsibleByGUID = f.BillingResponsibleByGUID,
								MethodOfPaymentGUID = f.MethodOfPaymentGUID,
								ProductType = f.ProductType,
								MaxPurchasePct = f.MaxPurchasePct,
								CompanyGUID = creditAppRequestTable.CompanyGUID,
								Owner = userName,
								OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID,
							});
						});
					}
				}
				#endregion 6. Create CA buyer credit outstanding
				#region BulkInsert
				using (var transaction = UnitOfWork.ContextTransaction())
				{
					try
					{
						BulkInsert(creditAppRequestTable.FirstToList());
						BulkInsert(creditAppRequestLine.FirstToList());
						BulkInsert(creditAppRequestLineAmend.FirstToList());
                        if (caReqCreditOutStandings.Any())
                        {
							BulkInsert(caReqCreditOutStandings);
                        }
						if (caReqAssignmentOutstanding.Any())
						{
							BulkInsert(caReqAssignmentOutstanding);
						}
						if (caReqBuyerCreditOutstanding.Any())
						{
							BulkInsert(caReqBuyerCreditOutstanding);
						}
						UnitOfWork.Commit(transaction);
					}
					catch (Exception e)
					{
						transaction.Rollback();
						throw SmartAppUtil.AddStackTrace(e);
					}
				}
				#endregion
				return creditAppRequestLineAmend.ToCreditAppRequestLineAmendItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestLineAmendItemView UpdateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend inputCreditAppRequestLineAmend = creditAppRequestLineAmendView.ToCreditAppRequestLineAmend();
				CreditAppRequestLineAmend dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(inputCreditAppRequestLineAmend.CreditAppRequestLineAmendGUID);
				#region Update CreditAppRequestLine
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid());
				UpdateCreditAppRequestByAmendCaLine(creditAppRequestLine, creditAppRequestLineAmendView);
				#endregion Update CreditAppRequestLine
				dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.UpdateCreditAppRequestLineAmend(dbCreditAppRequestLineAmend, inputCreditAppRequestLineAmend);
				base.LogTransactionUpdate<CreditAppRequestLineAmend>(GetOriginalValues<CreditAppRequestLineAmend>(dbCreditAppRequestLineAmend), dbCreditAppRequestLineAmend);
				UnitOfWork.Commit();
				return dbCreditAppRequestLineAmend.ToCreditAppRequestLineAmendItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCreditAppRequestLineAmend(string item)
		{
			try
			{
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				Guid creditAppRequestLineAmendGUID = new Guid(item);
				CreditAppRequestLineAmend creditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(creditAppRequestLineAmendGUID);
				creditAppRequestLineAmendRepo.Remove(creditAppRequestLineAmend);
				base.LogTransactionDelete<CreditAppRequestLineAmend>(creditAppRequestLineAmend);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void ValidateCreateCreditAppRequestLineAmend(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				CreditAppTable creditAppTable = creditAppTableRepo.Find(creditAppRequestLineAmendView.RefCreditAppTableGUID.StringToGuid());
				ValidateExpired(creditAppRequestLineAmendView.CreditAppRequestTable_RequestDate.StringToDate(), creditAppTable.ExpiryDate);
				CreditAppLine creditAppLine = new CreditAppLine();
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByRefCreditAppLineAndCreditAppRequestType(creditAppRequestLineAmendView.CreditAppRequestLine_RefCreditAppLineGUID.StringToGuid(), creditAppRequestLineAmendView.CreditAppRequestTable_CreditAppRequestType);
				if (creditAppRequestTable != null)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90004", new string[]
					{
						"LABEL.AMEND_CREDIT_APPLICATION_LINE",
						SmartAppUtil.GetDropDownLabel(creditAppTable.CreditAppId, creditAppTable.Description)
					});
					throw smartAppException;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void ValidateExpired(DateTime requestDate, DateTime expiryDate)
		{
			try
			{
				if (requestDate >= expiryDate)
				{
					SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
					smartAppException.AddData("ERROR.90038");
					throw smartAppException;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion CRUD
		#region InitialData
		public CreditAppRequestLineAmendItemView GetCreditAppRequestLineAmendInitialData(string creditApplineGUID)
		{
			try
			{
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmendItemView creditAppRequestLineAmendItemView = creditAppRequestLineAmendRepo.GetCreditAppRequestLineAmendInitialData(creditApplineGUID.StringToGuid());
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(creditAppRequestLineAmendItemView.RefCreditAppTableGUID.StringToGuid());
				creditAppRequestLineAmendItemView.CreditAppRequestTable_CustomerCreditLimit = creditAppTableRepo.GetCustomerCreditLimit(creditAppTable.CustomerTableGUID, creditAppTable.ProductType, creditAppRequestLineAmendItemView.CreditAppRequestTable_RequestDate.StringToDate());
				return creditAppRequestLineAmendItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTransItemView GetDocumentConditionTransInitialListData(string amendCaLineId)
		{
			try
			{
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend creditAppRequestLineAmend = creditAppRequestLineAmendRepo.GetCreditAppRequestLineAmendByIdNoTrackingByAccessLevel(amendCaLineId.StringToGuid());
				ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
				DocumentConditionTransItemView documentConditionTransItemView = new DocumentConditionTransItemView();
				documentConditionTransItemView.RefGUID = creditAppRequestLineAmend.CreditAppRequestLineGUID.GuidNullToString();
				documentConditionTransItemView.RefType = (int)RefType.CreditAppRequestLine;
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				// GetAccessModeByCreditAppRequestTableStatusIsLessThanWaitingForMarketingStaff
				AccessModeView accessModeView = creditAppRequestTableService.GetAccessModeByCreditAppRequestLine(amendCaLineId);
				accessModeView.CanView = true;
				documentConditionTransItemView.AccessModeView = accessModeView;
				return documentConditionTransItemView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public DocumentConditionTransItemView GetDocumentConditionTransChildInitialData(string amendCaLineId)
		{
			try
			{
				IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend creditAppRequestLineAmend = creditAppRequestLineAmendRepo
					.GetCreditAppRequestLineAmendByIdNoTrackingByAccessLevel(amendCaLineId.StringToGuid());
				int lineNum = creditAppRequestLineRepo
					.GetCreditAppRequestLineByIdNoTrackingByAccessLevel(creditAppRequestLineAmend.CreditAppRequestLineGUID)
					.LineNum;
				return documentConditionTransService
					.GetDocumentConditionTransInitialData(lineNum.ToString(),
														  creditAppRequestLineAmend.CreditAppRequestLineGUID.GuidNullToString(),
														  Models.Enum.RefType.CreditAppRequestLine);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion InitialData
		#region CrditAppRequestLine
		public void UpdateCreditAppRequestByAmendCaLine(CreditAppRequestLine creditAppRequestLine, CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				if (creditAppRequestLineAmendView.CreditAppRequestTable_CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerBuyerCreditLimit)
				{
					creditAppRequestLine.CreditLimitLineRequest = creditAppRequestLineAmendView.CreditAppRequestLine_NewCreditLimitLineRequest;

				}
				else if (creditAppRequestLineAmendView.CreditAppRequestTable_CreditAppRequestType == (int)CreditAppRequestType.AmendBuyerInfo)
				{
					UpdateCreditAppRequestLineByAmendRate(creditAppRequestLine, creditAppRequestLineAmendView);
					UpdateCreditAppRequestLineByAmendAssignmentMethod(creditAppRequestLine, creditAppRequestLineAmendView);
					UpdateCreditAppRequestLineByAmendBillingInformation(creditAppRequestLine, creditAppRequestLineAmendView);
					UpdateCreditAppRequestLineByAmendReceiptInformation(creditAppRequestLine, creditAppRequestLineAmendView);

				}
				creditAppRequestLine.ApprovedCreditLimitLineRequest = creditAppRequestLineAmendView.CreditAppRequestLine_ApprovedCreditLimitLineRequest;
				creditAppRequestLine.MarketingComment = creditAppRequestLineAmendView.CreditAppRequestLine_MarketingComment;
				creditAppRequestLine.CreditComment = creditAppRequestLineAmendView.CreditAppRequestLine_CreditComment;
				creditAppRequestLine.ApproverComment = creditAppRequestLineAmendView.CreditAppRequestLine_ApproverComment;
				creditAppRequestTableService.UpdateCreditAppRequestLine(creditAppRequestLine);

				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.Find(creditAppRequestLine.CreditAppRequestTableGUID);
				creditAppRequestTable.Description = creditAppRequestLineAmendView.CreditAppRequestTable_Description;
				creditAppRequestTable.Remark = creditAppRequestLineAmendView.CreditAppRequestTable_Remark;
				creditAppRequestTable.RequestDate = creditAppRequestLineAmendView.CreditAppRequestTable_RequestDate.StringToDate();
				creditAppRequestTable.ApprovedDate = creditAppRequestLineAmendView.CreditAppRequestTable_ApprovedDate.StringNullToDateNull();
				creditAppRequestTable.CACondition = creditAppRequestLineAmendView.CreditAppRequestTable_CACondition;
				creditAppRequestTable.BlacklistStatusGUID = creditAppRequestLineAmendView.CreditAppRequestLine_BlacklistStatusGUID.StringToGuidNull();
				creditAppRequestTable.KYCGUID = creditAppRequestLineAmendView.CreditAppRequestLine_KYCSetupGUID.StringToGuidNull();
				creditAppRequestTable.CreditScoringGUID = creditAppRequestLineAmendView.CreditAppRequestLine_CreditScoringGUID.StringToGuidNull();
				creditAppRequestTableService.UpdateCreditAppRequestTable(creditAppRequestTable);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendRate(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid());
				UpdateCreditAppRequestLineByAmendRate(creditAppRequestLine, creditAppRequestLineAmendView);
				creditAppRequestTableService.UpdateCreditAppRequestLine(creditAppRequestLine);
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend inputCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineAmendGUID.StringToGuid());
				inputCreditAppRequestLineAmend.AmendRate = creditAppRequestLineAmendView.AmendRate;
				CreditAppRequestLineAmend dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(inputCreditAppRequestLineAmend.CreditAppRequestLineAmendGUID);
				dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.UpdateCreditAppRequestLineAmend(dbCreditAppRequestLineAmend, inputCreditAppRequestLineAmend);
				base.LogTransactionUpdate<CreditAppRequestLineAmend>(GetOriginalValues<CreditAppRequestLineAmend>(dbCreditAppRequestLineAmend), dbCreditAppRequestLineAmend);
				UnitOfWork.Commit();
				return creditAppRequestLine.ToCreditAppRequestLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendAssignmentMethod(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid());
				UpdateCreditAppRequestLineByAmendAssignmentMethod(creditAppRequestLine, creditAppRequestLineAmendView);
				creditAppRequestTableService.UpdateCreditAppRequestLine(creditAppRequestLine);
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend inputCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineAmendGUID.StringToGuid());
				inputCreditAppRequestLineAmend.AmendAssignmentMethod = creditAppRequestLineAmendView.AmendAssignmentMethod;
				CreditAppRequestLineAmend dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(inputCreditAppRequestLineAmend.CreditAppRequestLineAmendGUID);
				dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.UpdateCreditAppRequestLineAmend(dbCreditAppRequestLineAmend, inputCreditAppRequestLineAmend);
				base.LogTransactionUpdate<CreditAppRequestLineAmend>(GetOriginalValues<CreditAppRequestLineAmend>(dbCreditAppRequestLineAmend), dbCreditAppRequestLineAmend);
				UnitOfWork.Commit();
				return creditAppRequestLine.ToCreditAppRequestLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendBillingInformation(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid());
				UpdateCreditAppRequestLineByAmendBillingInformation(creditAppRequestLine, creditAppRequestLineAmendView);
				creditAppRequestTableService.UpdateCreditAppRequestLine(creditAppRequestLine);
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend inputCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineAmendGUID.StringToGuid());
				inputCreditAppRequestLineAmend.AmendBillingInformation = creditAppRequestLineAmendView.AmendBillingInformation;
				CreditAppRequestLineAmend dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(inputCreditAppRequestLineAmend.CreditAppRequestLineAmendGUID);
				dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.UpdateCreditAppRequestLineAmend(dbCreditAppRequestLineAmend, inputCreditAppRequestLineAmend);
				base.LogTransactionUpdate<CreditAppRequestLineAmend>(GetOriginalValues<CreditAppRequestLineAmend>(dbCreditAppRequestLineAmend), dbCreditAppRequestLineAmend);
				UnitOfWork.Commit();
				return creditAppRequestLine.ToCreditAppRequestLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditAppRequestLineItemView UpdateCreditAppRequestLineByAmendReceiptInformation(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid());
				UpdateCreditAppRequestLineByAmendReceiptInformation(creditAppRequestLine, creditAppRequestLineAmendView);
				creditAppRequestTableService.UpdateCreditAppRequestLine(creditAppRequestLine);
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend inputCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineAmendGUID.StringToGuid());
				inputCreditAppRequestLineAmend.AmendReceiptInformation = creditAppRequestLineAmendView.AmendReceiptInformation;
				CreditAppRequestLineAmend dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(inputCreditAppRequestLineAmend.CreditAppRequestLineAmendGUID);
				dbCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.UpdateCreditAppRequestLineAmend(dbCreditAppRequestLineAmend, inputCreditAppRequestLineAmend);
				UnitOfWork.Commit();
				return creditAppRequestLine.ToCreditAppRequestLineItemView(); ;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestLineByAmendBillingDocumentCondition(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend inputCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineAmendGUID.StringToGuid());
				inputCreditAppRequestLineAmend.AmendBillingDocumentCondition = creditAppRequestLineAmendView.AmendBillingDocumentCondition;

				IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				List<DocumentConditionTrans> documentConditionTrans = new List<DocumentConditionTrans>();
				if (inputCreditAppRequestLineAmend.AmendBillingDocumentCondition)
				{
					documentConditionTrans = documentConditionTransService
					.CopyDocumentConditionTrans(new DocConVerifyType[] { DocConVerifyType.Billing },
												creditAppRequestLineAmendView.CreditAppRequestLine_RefCreditAppLineGUID.StringToGuid(),
												creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid(),
												(int)RefType.CreditAppLine, (int)RefType.CreditAppRequestLine, false, true)
					.ToList();

				}
				else
				{
					documentConditionTrans = documentConditionTransRepo
						.GetDocumentConditionTransByReference(creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid(),
															  (int)RefType.CreditAppRequestLine).Where(w => w.DocConVerifyType == (int)DocConVerifyType.Billing)
						.ToList();
				}

				#region BulkInsert
				using (var transaction = UnitOfWork.ContextTransaction())
				{
					try
					{
						BulkUpdate(inputCreditAppRequestLineAmend.FirstToList());
						if (documentConditionTrans.Any())
						{
							if (inputCreditAppRequestLineAmend.AmendBillingDocumentCondition)
							{
								BulkInsert(documentConditionTrans);
							}
							else
							{
								BulkDelete(documentConditionTrans);
							}
						}
						UnitOfWork.Commit(transaction);
					}
					catch (Exception e)
					{
						transaction.Rollback();
						throw SmartAppUtil.AddStackTrace(e);
					}
				}
				#endregion
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestLineByAmendReceiptDocumentCondition(CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				ICreditAppRequestLineAmendRepo creditAppRequestLineAmendRepo = new CreditAppRequestLineAmendRepo(db);
				CreditAppRequestLineAmend inputCreditAppRequestLineAmend = creditAppRequestLineAmendRepo.Find(creditAppRequestLineAmendView.CreditAppRequestLineAmendGUID.StringToGuid());
				inputCreditAppRequestLineAmend.AmendReceiptDocumentCondition = creditAppRequestLineAmendView.AmendReceiptDocumentCondition;

				IDocumentConditionTransService documentConditionTransService = new DocumentConditionTransService(db);
				IDocumentConditionTransRepo documentConditionTransRepo = new DocumentConditionTransRepo(db);
				List<DocumentConditionTrans> documentConditionTrans = new List<DocumentConditionTrans>();
				if (inputCreditAppRequestLineAmend.AmendReceiptDocumentCondition)
				{
					documentConditionTrans = documentConditionTransService
					.CopyDocumentConditionTrans(new DocConVerifyType[] { DocConVerifyType.Receipt },
												creditAppRequestLineAmendView.CreditAppRequestLine_RefCreditAppLineGUID.StringToGuid(),
												creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid(),
												(int)RefType.CreditAppLine, (int)RefType.CreditAppRequestLine, false, true)
					.ToList();

				}
				else
				{
					documentConditionTrans = documentConditionTransRepo
						.GetDocumentConditionTransByReference(creditAppRequestLineAmendView.CreditAppRequestLineGUID.StringToGuid(),
															  (int)RefType.CreditAppRequestLine).Where(w => w.DocConVerifyType == (int)DocConVerifyType.Receipt)
						.ToList();
				}

				#region BulkInsert
				using (var transaction = UnitOfWork.ContextTransaction())
				{
					try
					{
						BulkUpdate(inputCreditAppRequestLineAmend.FirstToList());
						if (documentConditionTrans.Any())
						{
							if (inputCreditAppRequestLineAmend.AmendReceiptDocumentCondition)
							{
								BulkInsert(documentConditionTrans);
							}
							else
							{
								BulkDelete(documentConditionTrans);
							}
						}
						UnitOfWork.Commit(transaction);
					}
					catch (Exception e)
					{
						transaction.Rollback();
						throw SmartAppUtil.AddStackTrace(e);
					}
				}
				#endregion
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestLineByAmendRate(CreditAppRequestLine creditAppRequestLine, CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				if (creditAppRequestLineAmendView.AmendRate)
				{
					creditAppRequestLine.MaxPurchasePct = creditAppRequestLineAmendView.CreditAppRequestLine_NewMaxPurchasePct;
					creditAppRequestLine.PurchaseFeePct = creditAppRequestLineAmendView.CreditAppRequestLine_NewPurchaseFeePct;
					creditAppRequestLine.PurchaseFeeCalculateBase = creditAppRequestLineAmendView.CreditAppRequestLine_NewPurchaseFeeCalculateBase;
				}
				else
				{
					creditAppRequestLine.MaxPurchasePct = creditAppRequestLineAmendView.OriginalMaxPurchasePct;
					creditAppRequestLine.PurchaseFeePct = creditAppRequestLineAmendView.OriginalPurchaseFeePct;
					creditAppRequestLine.PurchaseFeeCalculateBase = creditAppRequestLineAmendView.OriginalPurchaseFeeCalculateBase;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestLineByAmendAssignmentMethod(CreditAppRequestLine creditAppRequestLine, CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				if (creditAppRequestLineAmendView.AmendAssignmentMethod)
				{
					creditAppRequestLine.AssignmentMethodGUID = creditAppRequestLineAmendView.CreditAppRequestLine_AssignmentMethodGUID.StringToGuidNull();
					creditAppRequestLine.AssignmentMethodRemark = creditAppRequestLineAmendView.CreditAppRequestLine_NewAssignmentMethodRemark;
				}
				else
				{
					creditAppRequestLine.AssignmentMethodGUID = creditAppRequestLineAmendView.OriginalAssignmentMethodGUID.StringToGuidNull();
					creditAppRequestLine.AssignmentMethodRemark = creditAppRequestLineAmendView.OriginalAssignmentMethodRemark;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestLineByAmendBillingInformation(CreditAppRequestLine creditAppRequestLine, CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				if (creditAppRequestLineAmendView.AmendBillingInformation)
				{
					creditAppRequestLine.BillingAddressGUID = creditAppRequestLineAmendView.CreditAppRequestLine_NewBillingAddressGUID.StringToGuidNull();
				}
				else
				{
					creditAppRequestLine.BillingAddressGUID = creditAppRequestLineAmendView.OriginalBillingAddressGUID.StringToGuidNull();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void UpdateCreditAppRequestLineByAmendReceiptInformation(CreditAppRequestLine creditAppRequestLine, CreditAppRequestLineAmendItemView creditAppRequestLineAmendView)
		{
			try
			{
				if (creditAppRequestLineAmendView.AmendReceiptInformation)
				{
					creditAppRequestLine.MethodOfPaymentGUID = creditAppRequestLineAmendView.CreditAppRequestLine_NewMethodOfPaymentGUID.StringToGuidNull();
					creditAppRequestLine.ReceiptRemark = creditAppRequestLineAmendView.CreditAppRequestLine_NewReceiptRemark;
					creditAppRequestLine.ReceiptAddressGUID = creditAppRequestLineAmendView.CreditAppRequestLine_NewReceiptAddressGUID.StringToGuidNull();
				}
				else
				{
					creditAppRequestLine.MethodOfPaymentGUID = creditAppRequestLineAmendView.OriginalMethodOfPaymentGUID.StringToGuidNull();
					creditAppRequestLine.ReceiptRemark = creditAppRequestLineAmendView.OriginalReceiptRemark;
					creditAppRequestLine.ReceiptAddressGUID = creditAppRequestLineAmendView.OriginalReceiptAddressGUID.StringToGuidNull();
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
		#region Function
		#region UpdateStatusAmendBuyerInfo
		public UpdateStatusAmendBuyerInfoResultView UpdateStatusAmendBuyerInfo(UpdateStatusAmendBuyerInfoView model)
		{
			try
			{
				ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(model.CreditAppRequestTableGUID.StringToGuid());
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTracking(model.UpdateStatusAmendBuyerInfoGUID.StringToGuid());
				VaidateUpdateStatusAmendBuyerInfo(creditAppRequestTable, creditAppRequestLine);
				if(model.ApprovalDecision != (int)ApprovalDecision.None)
                {
					switch (model.ApprovalDecision)
					{
						case (int)ApprovalDecision.Approved:
							creditAppRequestTable.ApprovedDate = model.ApprovedDate.StringNullToDateNull();
							ApproveAmendBuyerInformationK2ParmView param = new ApproveAmendBuyerInformationK2ParmView
							{
								ParmDocGUID = model.CreditAppRequestTableGUID,
								CompanyGUID = GetCurrentCompany(),
								WorkflowName = "None",
								Owner = creditAppRequestTable.Owner,
								OwnerBusinessUnitGUID = creditAppRequestTable.OwnerBusinessUnitGUID.GuidNullToString()
							};
							creditAppRequestTableService.ApproveAmendBuyerInformationK2(param);
							break;
						case (int)ApprovalDecision.Rejected:
							creditAppRequestTable.ApprovedDate = model.ApprovedDate.StringNullToDateNull();
							IDocumentService documentService = new DocumentService(db);
							DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)CreditAppRequestStatus.Rejected);
							creditAppRequestTable.DocumentStatusGUID = documentStatus.DocumentStatusGUID;
							creditAppRequestTableService.UpdateCreditAppRequestTable(creditAppRequestTable);
							break;
						default:
							break;
					}
					UnitOfWork.Commit();
					NotificationResponse notificationResponse = new NotificationResponse();
					notificationResponse.AddData("SUCCESS.90021", new string[] { creditAppRequestTable.CreditAppRequestId, SystemStaticData.GetTranslatedMessage(((ApprovalDecision)model.ApprovalDecision).GetAttrCode()) });
					return new UpdateStatusAmendBuyerInfoResultView
					{
						Notification = notificationResponse
					};
				}
				return null;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void VaidateUpdateStatusAmendBuyerInfo(CreditAppRequestTable creditAppRequestTable, CreditAppRequestLine creditAppRequestLine)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				DocumentStatus documentStatus = documentService.GetDocumentStatusByStatusId((int)CreditAppRequestStatus.Draft);
				SmartAppException smartAppException = new SmartAppException("ERROR.ERROR");
				if(creditAppRequestTable.DocumentStatusGUID != documentStatus.DocumentStatusGUID) 
				{
					smartAppException.AddData("ERROR.90012", "LABEL.CREDIT_APPLICATION_REQUEST");
				}
                if (string.IsNullOrWhiteSpace(creditAppRequestLine.MarketingComment))
                {
					smartAppException.AddData("ERROR.00755", "LABEL.MARKETING_COMMENT");
				}
				if (string.IsNullOrWhiteSpace(creditAppRequestLine.CreditComment))
				{
					smartAppException.AddData("ERROR.00755", "LABEL.CREDIT_COMMENT");
				}
				if (string.IsNullOrWhiteSpace(creditAppRequestLine.ApproverComment))
				{
					smartAppException.AddData("ERROR.00755", "LABEL.APPROVER_COMMENT");
				}
                if (smartAppException.Data.Count > 0)
                {
					throw SmartAppUtil.AddStackTrace(smartAppException);
                }

			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion UpdateStatusAmendBuyerInfo
		#endregion Function
	}
}
