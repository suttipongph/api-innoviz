﻿using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IK2Service
    {
        Task<TaskItem> GetTaskBySerialNumber(WorkflowInstance workflowInstance);
        Task<WorkflowResultView> StartWorkflow(WorkflowInstance workflowInstance, string activityName = null, string actionName = null);
        Task<WorkflowResultView> ActionWorkflow(WorkflowInstance workflowInstance);
        List<DataField> GetDataFieldFromParam<T>(T param);
        Task<SearchResult<TaskItem>> GetTaskList(TaskListParm param);
        Task<WorkflowResultView> ReleaseTask(WorkflowInstance workflowInstance);
    }
    public class K2Service : SmartAppService, IK2Service
    {
        public K2Service(SmartAppDbContext context) : base(context) { }
        public K2Service(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public K2Service(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public K2Service(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public K2Service() : base() { }

        public async Task<TaskItem> GetTaskBySerialNumber(WorkflowInstance workflowInstance)
        {
            try
            {
                HttpHelper helper = new HttpHelper(GetSystemParameter());
                var result = await helper.CallConnectApi<WorkflowInstance, TaskItem>(workflowInstance, "Workflow/GetTaskBySerialNumber");
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> StartWorkflow(WorkflowInstance workflowInstance,string activityName = null,string actionName = null)
        {
            try
            {
                HttpHelper helper = new HttpHelper(GetSystemParameter());
                var result = await helper.CallConnectApi<WorkflowInstance, int>(workflowInstance, "Workflow/StartWorkflow");

                IActionHistoryService actionHistoryService = new ActionHistoryService(db, transactionLogService);
                actionHistoryService.CreateActionHistoryStartWorkflow(workflowInstance, activityName, actionName);

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.00033", new string[] { result.ToString() });
                WorkflowResultView retVal = new WorkflowResultView
                {
                    ProcessInstanceId = result,
                    Notification = success
                };
                return retVal;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> ActionWorkflow(WorkflowInstance workflowInstance)
        {
            try
            {
                HttpHelper helper = new HttpHelper(GetSystemParameter());
                var result = await helper.CallConnectApi<WorkflowInstance, bool>(workflowInstance, "Workflow/ActionWorkflow");

                IActionHistoryService actionHistoryService = new ActionHistoryService(db, transactionLogService);
                actionHistoryService.CreateActionHistoryActionWorkflow(workflowInstance);

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.00034");
                WorkflowResultView retVal = new WorkflowResultView
                {
                    Notification = success
                };
                return retVal;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public List<DataField> GetDataFieldFromParam<T>(T param)
        {
            try
            {
                List<DataField> result = new List<DataField>();
                PropertyInfo[] props = typeof(T).GetType().GetProperties();
                result = props.Select(prop => new DataField
                {
                    Name = prop.Name,
                    Value = prop.GetValue(param)?.ToString()
                }).ToList();
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<SearchResult<TaskItem>> GetTaskList(TaskListParm param)
        {
            try
            {
                HttpHelper helper = new HttpHelper(GetSystemParameter());
                var result = await helper.CallConnectApi<TaskListParm, SearchResult<TaskItem>>(param, "Workflow/GetTasks");
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public async Task<WorkflowResultView> ReleaseTask(WorkflowInstance workflowInstance)
        {
            try
            {
                HttpHelper helper = new HttpHelper(GetSystemParameter());
                var result = await helper.CallConnectApi<WorkflowInstance, bool>(workflowInstance, "Workflow/ReleaseTask");

                NotificationResponse success = new NotificationResponse();
                success.AddData("SUCCESS.90018");
                WorkflowResultView retVal = new WorkflowResultView
                {
                    Notification = success
                };
                return retVal;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
    }
}
