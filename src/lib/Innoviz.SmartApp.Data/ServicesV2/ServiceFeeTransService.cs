﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;
using static Innoviz.SmartApp.Data.Models.EnumsDocumentProcessStatus;

namespace Innoviz.SmartApp.Data.ServicesV2
{
    public interface IServiceFeeTransService
    {

        ServiceFeeTransItemView GetServiceFeeTransById(string id);
        ServiceFeeTransItemView CreateServiceFeeTrans(ServiceFeeTransItemView serviceFeeTransView);
        ServiceFeeTransItemView UpdateServiceFeeTrans(ServiceFeeTransItemView serviceFeeTransView);
        bool DeleteServiceFeeTrans(string id);
        ServiceFeeTransCalculateFieldView GetCalculateField(ServiceFeeTransItemView serviceFeeTransView);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByMainAgreementTable(string refId, string refGUID, RefType refType);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByAssignmentAgreementTable(string refId, string refGUID, RefType refType);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(string refId, string refGUID, RefType refType);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByPurchaseTable(string refId, string refGUID, RefType refType);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByWithdrawalTable(string refId, string refGUID, RefType refType);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByMessengerJobTable(string refId, string refGUID, RefType refType);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByCustomerRefundTable(string refId, string refGUID, RefType refType);
        ServiceFeeTransItemView GetServiceFeeTransInitialDataByReceiptTempTable(string refId, string refGUID, RefType refType);
        void CreateServiceFeeTransCollection(List<ServiceFeeTransItemView> serviceFeeTransItemViews);
        bool ValidateServiceFeeTrans(GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm);
        decimal GetTaxValue(GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm);
        decimal GetWHTValue(GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm);

        #region function
        #region shared GenInvoiceFromServiceFeeTrans
        GenInvoiceFromServiceFeeTransResultView GenInvoiceFromServiceFeeTrans(GenInvoiceFromServiceFeeTransParamView view);
        #endregion
        #region shared CalcTaxAmount
        decimal CalcTaxAmount(decimal feeAmount, decimal taxValue, bool includeTax);
        #endregion
        #region shared CalcWHTAmount
        decimal CalcWHTAmount(decimal amountBeforeTax, decimal whTaxValue);
        #endregion
        #region copy service fee from CA
        ManageAgreementView CreateCopyServiceFeeCondCARequest(MainAgreementTableItemView view);
        #endregion copy service fee from CA
        #region GetTotalAmountByReference
        decimal GetTotalAmountByReference(RefType refType, string refGUID);
        #endregion
        #region GetUpcountryChequeFee

        decimal GetUpcountryChequeFee(decimal totalServiceFee);

        #endregion

        #endregion
    }
    public class ServiceFeeTransService : SmartAppService, IServiceFeeTransService
    {
        public ServiceFeeTransService(SmartAppDbContext context) : base(context) { }
        public ServiceFeeTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
        public ServiceFeeTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
        public ServiceFeeTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
        public ServiceFeeTransService() : base() { }

        public ServiceFeeTransItemView GetServiceFeeTransById(string id)
        {
            try
            {
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                return serviceFeeTransRepo.GetByIdvw(id.StringToGuid());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView CreateServiceFeeTrans(ServiceFeeTransItemView serviceFeeTransView)
        {
            try
            {
                ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
                serviceFeeTransView = accessLevelService.AssignOwnerBU(serviceFeeTransView);
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                ServiceFeeTrans serviceFeeTrans = serviceFeeTransView.ToServiceFeeTrans();
                serviceFeeTrans = serviceFeeTransRepo.CreateServiceFeeTrans(serviceFeeTrans);
                customerRefundTableService.UpdateCustomerRefundTableServiceFeeAmount(serviceFeeTransView);
                receiptTempTableService.UpdateReceiptTempTableSettleFeeAmount(serviceFeeTransView);
                base.LogTransactionCreate<ServiceFeeTrans>(serviceFeeTrans);
                UnitOfWork.Commit();
                return serviceFeeTrans.ToServiceFeeTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView UpdateServiceFeeTrans(ServiceFeeTransItemView serviceFeeTransView)
        {
            try
            {
                ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                ServiceFeeTrans inputServiceFeeTrans = serviceFeeTransView.ToServiceFeeTrans();
                ServiceFeeTrans dbServiceFeeTrans = serviceFeeTransRepo.Find(inputServiceFeeTrans.ServiceFeeTransGUID);
                dbServiceFeeTrans = serviceFeeTransRepo.UpdateServiceFeeTrans(dbServiceFeeTrans, inputServiceFeeTrans);
                customerRefundTableService.UpdateCustomerRefundTableServiceFeeAmount(serviceFeeTransView);
                receiptTempTableService.UpdateReceiptTempTableSettleFeeAmount(serviceFeeTransView);
                base.LogTransactionUpdate<ServiceFeeTrans>(GetOriginalValues<ServiceFeeTrans>(dbServiceFeeTrans), dbServiceFeeTrans);
                UnitOfWork.Commit();
                return dbServiceFeeTrans.ToServiceFeeTransItemView();
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public bool DeleteServiceFeeTrans(string item)
        {
            try
            {
                ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                Guid serviceFeeTransGUID = new Guid(item);
                ServiceFeeTrans serviceFeeTrans = serviceFeeTransRepo.Find(serviceFeeTransGUID);
                serviceFeeTransRepo.Remove(serviceFeeTrans);
                customerRefundTableService.UpdateCustomerRefundTableServiceFeeAmountOnRemove(serviceFeeTrans);
                receiptTempTableService.UpdateReceiptTempTableSettleFeeAmountOnRemove(serviceFeeTrans);
                base.LogTransactionDelete<ServiceFeeTrans>(serviceFeeTrans);
                UnitOfWork.Commit();
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByMainAgreementTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                MainAgreementTable mainAgreementTable = mainAgreementTableRepo.GetMainAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(mainAgreementTable.CreditAppTableGUID);
                return new ServiceFeeTransItemView
                {
                    ServiceFeeTransGUID = new Guid().GuidNullToString(),
                    IncludeTax = false,
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                    Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                    Dimension1GUID = creditAppTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = creditAppTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = creditAppTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = creditAppTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = creditAppTable.Dimension5GUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByAssignmentAgreementTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                IAssignmentAgreementTableRepo assignmentAgreementTableRepo = new AssignmentAgreementTableRepo(db);
                AssignmentAgreementTable assignmentAgreementTable = assignmentAgreementTableRepo.GetAssignmentAgreementTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTrackingByAccessLevel(assignmentAgreementTable.CustomerTableGUID);
                return new ServiceFeeTransItemView
                {
                    ServiceFeeTransGUID = new Guid().GuidNullToString(),
                    IncludeTax = false,
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                    Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                    Dimension1GUID = customerTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = customerTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = customerTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = customerTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = customerTable.Dimension5GUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByBusinessCollateralAgmTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                IBusinessCollateralAgmTableRepo businessCollateralAgmTableRepo = new BusinessCollateralAgmTableRepo(db);
                BusinessCollateralAgmTable businessCollateralAgmTable = businessCollateralAgmTableRepo.GetBusinessCollateralAgmTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                CreditAppTable creditAppTable = businessCollateralAgmTable.CreditAppTableGUID == null ? null : creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(businessCollateralAgmTable.CreditAppTableGUID.Value);
                return new ServiceFeeTransItemView
                {
                    ServiceFeeTransGUID = new Guid().GuidNullToString(),
                    IncludeTax = false,
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                    Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                    Dimension1GUID = creditAppTable == null ? null : creditAppTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = creditAppTable == null ? null : creditAppTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = creditAppTable == null ? null : creditAppTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = creditAppTable == null ? null : creditAppTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = creditAppTable == null ? null : creditAppTable.Dimension5GUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByPurchaseTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                IPurchaseTableRepo purchaseTableRepo = new PurchaseTableRepo(db);
                PurchaseTable purchaseTable = purchaseTableRepo.GetPurchaseTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                return new ServiceFeeTransItemView
                {
                    ServiceFeeTransGUID = new Guid().GuidNullToString(),
                    IncludeTax = false,
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                    Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                    Dimension1GUID = purchaseTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = purchaseTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = purchaseTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = purchaseTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = purchaseTable.Dimension5GUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByWithdrawalTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                IWithdrawalTableRepo withdrawalTableRepo = new WithdrawalTableRepo(db);
                WithdrawalTable withdrawalTable = withdrawalTableRepo.GetWithdrawalTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                return new ServiceFeeTransItemView
                {
                    ServiceFeeTransGUID = new Guid().GuidNullToString(),
                    IncludeTax = false,
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                    Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                    Dimension1GUID = withdrawalTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = withdrawalTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = withdrawalTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = withdrawalTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = withdrawalTable.Dimension5GUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public int GetServiceFeeTransLastOrdering(RefType refType, string refGUID)
        {
            try
            {
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                IEnumerable<ServiceFeeTrans> serviceFeeTranss = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(refType, refGUID.StringToGuid());
                if (serviceFeeTranss.Count() > 0)
                {
                    return serviceFeeTranss.Max(t => t.Ordering) + 1;
                }
                else
                {
                    return 1;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByMessengerJobTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                IMessengerJobTableRepo messengerJobTableRepo = new MessengerJobTableRepo(db);
                MessengerJobTable messengerJobTable = messengerJobTableRepo.GetMessengerJobTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                ICreditAppLineRepo creditAppLineRepo = new CreditAppLineRepo(db);
                var creditappline = new CreditAppLine();
                if (messengerJobTable.RefCreditAppLineGUID != null)
                {
                    creditappline = creditAppLineRepo.GetCreditAppLineByIdNoTrackingByAccessLevel((Guid)messengerJobTable.RefCreditAppLineGUID);
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTrackingByAccessLevel(creditappline.CreditAppTableGUID);

                    return new ServiceFeeTransItemView
                    {
                        ServiceFeeTransGUID = new Guid().GuidNullToString(),
                        IncludeTax = false,
                        RefGUID = refGUID,
                        RefType = (int)refType,
                        RefId = refId,
                        Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                        Dimension1GUID = creditAppTable.Dimension1GUID.GuidNullToString(),
                        Dimension2GUID = creditAppTable.Dimension2GUID.GuidNullToString(),
                        Dimension3GUID = creditAppTable.Dimension3GUID.GuidNullToString(),
                        Dimension4GUID = creditAppTable.Dimension4GUID.GuidNullToString(),
                        Dimension5GUID = creditAppTable.Dimension5GUID.GuidNullToString()
                    };
                }
                else
                {
                    return new ServiceFeeTransItemView
                    {
                        ServiceFeeTransGUID = new Guid().GuidNullToString(),
                        IncludeTax = false,
                        RefGUID = refGUID,
                        RefType = (int)refType,
                        RefId = refId,
                        Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                        Dimension1GUID = null,
                        Dimension2GUID = null,
                        Dimension3GUID = null,
                        Dimension4GUID = null,
                        Dimension5GUID = null
                    };
                }



            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByCustomerRefundTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                ICustomerRefundTableRepo customerRefundTableRepo = new CustomerRefundTableRepo(db);
                CustomerRefundTable customerRefundTable = customerRefundTableRepo.GetCustomerRefundTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                return new ServiceFeeTransItemView
                {
                    ServiceFeeTransGUID = new Guid().GuidNullToString(),
                    IncludeTax = false,
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                    Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                    Dimension1GUID = customerRefundTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = customerRefundTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = customerRefundTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = customerRefundTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = customerRefundTable.Dimension5GUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransItemView GetServiceFeeTransInitialDataByReceiptTempTable(string refId, string refGUID, RefType refType)
        {
            try
            {
                IReceiptTempTableRepo receiptTempTableRepo = new ReceiptTempTableRepo(db);
                ReceiptTempTable receiptTempTable = receiptTempTableRepo.GetReceiptTempTableByIdNoTrackingByAccessLevel(refGUID.StringToGuid());
                return new ServiceFeeTransItemView
                {
                    ServiceFeeTransGUID = new Guid().GuidNullToString(),
                    IncludeTax = false,
                    RefGUID = refGUID,
                    RefType = (int)refType,
                    RefId = refId,
                    Ordering = GetServiceFeeTransLastOrdering(refType, refGUID),
                    Dimension1GUID = receiptTempTable.Dimension1GUID.GuidNullToString(),
                    Dimension2GUID = receiptTempTable.Dimension2GUID.GuidNullToString(),
                    Dimension3GUID = receiptTempTable.Dimension3GUID.GuidNullToString(),
                    Dimension4GUID = receiptTempTable.Dimension4GUID.GuidNullToString(),
                    Dimension5GUID = receiptTempTable.Dimension5GUID.GuidNullToString()
                };
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ServiceFeeTransCalculateFieldView GetCalculateField(ServiceFeeTransItemView serviceFeeTransView)
        {
            try
            {
                ITaxTableService taxTableService = new TaxTableService(db);
                IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                ServiceFeeTransCalculateFieldView serviceFeeTransCalculateFieldView = new ServiceFeeTransCalculateFieldView();
                NotificationResponse error = new NotificationResponse();

                decimal maxSettleWHTAmount = 0;
                if (serviceFeeTransView.IncludeTax == false) //3
                {
                    serviceFeeTransCalculateFieldView.AmountBeforeTax = serviceFeeTransView.FeeAmount; //3.1
                    serviceFeeTransCalculateFieldView.TaxAmount = CalcTaxAmount(serviceFeeTransView.FeeAmount, serviceFeeTransView.TaxValue, serviceFeeTransView.IncludeTax); //3.2
                    serviceFeeTransCalculateFieldView.AmountIncludeTax = serviceFeeTransView.FeeAmount + serviceFeeTransCalculateFieldView.TaxAmount; //3.3
                }
                else if (serviceFeeTransView.IncludeTax == true) //4
                {
                    serviceFeeTransCalculateFieldView.AmountIncludeTax = serviceFeeTransView.FeeAmount; //4.1
                    serviceFeeTransCalculateFieldView.TaxAmount = CalcTaxAmount(serviceFeeTransView.FeeAmount, serviceFeeTransView.TaxValue, serviceFeeTransView.IncludeTax); //4.2
                    serviceFeeTransCalculateFieldView.AmountBeforeTax = serviceFeeTransView.FeeAmount - serviceFeeTransCalculateFieldView.TaxAmount; //4.3
                }
                serviceFeeTransCalculateFieldView.WHTAmount = CalcWHTAmount(serviceFeeTransCalculateFieldView.AmountBeforeTax, serviceFeeTransView.WHTaxValue); //5
                maxSettleWHTAmount = serviceFeeTransCalculateFieldView.AmountIncludeTax - serviceFeeTransCalculateFieldView.WHTAmount; //6
                if (serviceFeeTransView.SettleAmount == maxSettleWHTAmount) //7
                {
                    serviceFeeTransCalculateFieldView.SettleWHTAmount = serviceFeeTransCalculateFieldView.WHTAmount; //7.1
                }
                else if (serviceFeeTransView.SettleAmount < maxSettleWHTAmount) //8
                {
                    serviceFeeTransCalculateFieldView.SettleWHTAmount = ((serviceFeeTransCalculateFieldView.WHTAmount * serviceFeeTransView.SettleAmount) / maxSettleWHTAmount).Round(2); //8.1
                }
                else if (serviceFeeTransView.SettleAmount > maxSettleWHTAmount) //9
                {
                    serviceFeeTransCalculateFieldView.SettleWHTAmount = 0;
                }
                serviceFeeTransCalculateFieldView.SettleInvoiceAmount = serviceFeeTransView.SettleAmount + serviceFeeTransCalculateFieldView.SettleWHTAmount; //10

                return serviceFeeTransCalculateFieldView;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void CreateServiceFeeTransCollection(List<ServiceFeeTransItemView> serviceFeeTransItemViews)
        {
            try
            {
                if (serviceFeeTransItemViews.Any())
                {
                    IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                    List<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransItemViews.ToServiceFeeTrans().ToList();
                    serviceFeeTransRepo.ValidateAdd(serviceFeeTrans);
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(serviceFeeTrans, true);
                        UnitOfWork.Commit(transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }
        public bool ValidateServiceFeeTrans(GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                ITaxTableService taxTableService = new TaxTableService(db);
                IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                if (getTaxValueAndWHTValueParm.TaxTableGUID != null) //1
                {
                    try
                    {
                        decimal taxValue = taxTableService.GetTaxValue(getTaxValueAndWHTValueParm.TaxTableGUID.StringToGuid(), getTaxValueAndWHTValueParm.TaxDate.StringToDate());
                    }
                    catch
                    {
                        TaxTable taxTable = taxTableRepo.GetTaxTableByIdNoTracking(getTaxValueAndWHTValueParm.TaxTableGUID.StringToGuid());
                        ex.AddData("ERROR.90007", new string[] { "LABEL.TAX_VALUE", SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description) });
                    }
                }
                if (getTaxValueAndWHTValueParm.WithholdingTaxTableGUID != null) //1
                {
                    try
                    {
                        decimal whTaxValue = withholdingTaxTableService.GetWHTValue(getTaxValueAndWHTValueParm.WithholdingTaxTableGUID.StringToGuid(), getTaxValueAndWHTValueParm.TaxDate.StringToDate());
                    }
                    catch
                    {
                        WithholdingTaxTable withholdingTaxTable = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(getTaxValueAndWHTValueParm.WithholdingTaxTableGUID.StringToGuid());
                        ex.AddData("ERROR.90007", new string[] { "LABEL.WITHHOLDING_TAX_VALUE", SmartAppUtil.GetDropDownLabel(withholdingTaxTable.WHTCode, withholdingTaxTable.Description) });
                    }
                }
                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetTaxValue(GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                ITaxTableService taxTableService = new TaxTableService(db);
                ITaxTableRepo taxTableRepo = new TaxTableRepo(db);
                decimal taxValue = 0;
                if (getTaxValueAndWHTValueParm.TaxTableGUID != null) //1
                {
                    try
                    {
                        taxValue = taxTableService.GetTaxValue(getTaxValueAndWHTValueParm.TaxTableGUID.StringToGuid(), getTaxValueAndWHTValueParm.TaxDate.StringToDate());
                    }
                    catch
                    {
                        SmartAppException ex = new SmartAppException("ERROR.ERROR");
                        TaxTable taxTable = taxTableRepo.GetTaxTableByIdNoTracking(getTaxValueAndWHTValueParm.TaxTableGUID.StringToGuid());
                        ex.AddData("ERROR.90007", new string[] { "LABEL.TAX_VALUE", SmartAppUtil.GetDropDownLabel(taxTable.TaxCode, taxTable.Description) });
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                }
                return taxValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public decimal GetWHTValue(GetTaxValueAndWHTValueParm getTaxValueAndWHTValueParm)
        {
            try
            {
                IWithholdingTaxTableService withholdingTaxTableService = new WithholdingTaxTableService(db);
                IWithholdingTaxTableRepo withholdingTaxTableRepo = new WithholdingTaxTableRepo(db);
                decimal whTaxValue = 0;
                if (getTaxValueAndWHTValueParm.WithholdingTaxTableGUID != null) //1
                {
                    try
                    {
                        whTaxValue = withholdingTaxTableService.GetWHTValue(getTaxValueAndWHTValueParm.WithholdingTaxTableGUID.StringToGuid(), getTaxValueAndWHTValueParm.TaxDate.StringToDate());
                    }
                    catch
                    {
                        SmartAppException ex = new SmartAppException("ERROR.ERROR");
                        WithholdingTaxTable withholdingTaxTable = withholdingTaxTableRepo.GetWithholdingTaxTableByIdNoTracking(getTaxValueAndWHTValueParm.WithholdingTaxTableGUID.StringToGuid());
                        ex.AddData("ERROR.90007", new string[] { "LABEL.WITHHOLDING_TAX_VALUE", SmartAppUtil.GetDropDownLabel(withholdingTaxTable.WHTCode, withholdingTaxTable.Description) });
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                }
                return whTaxValue;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #region function
        #region shared GenInvoiceFromServiceFeeTrans
        public GenInvoiceFromServiceFeeTransResultView GenInvoiceFromServiceFeeTrans(GenInvoiceFromServiceFeeTransParamView view)
        {
            try
            {
                GenInvoiceFromServiceFeeTransResultView genInvoiceFromServiceFeeTransResultView = new GenInvoiceFromServiceFeeTransResultView
                {
                    InvoiceTable = new List<InvoiceTable>(),
                    InvoiceLine = new List<InvoiceLine>(),
                    IntercompanyInvoiceTable = new List<IntercompanyInvoiceTable>()
                };
                IInvoiceRevenueTypeRepo invoiceRevenueTypeRepo = new InvoiceRevenueTypeRepo(db);
                ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
                CreditAppTable creditAppTable = new CreditAppTable();
                AddressTrans invoiceAddressTrans = new AddressTrans();

                #region set Variable
                if (view.CreditAppTableGUID != null)
                {
                    creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(view.CreditAppTableGUID.Value);
                    if (creditAppTable.InvoiceAddressGUID != null)
                    {
                        invoiceAddressTrans = addressTransRepo.GetAddressTransByIdNoTracking(creditAppTable.InvoiceAddressGUID.Value);
                    }
                }
                else
                {
                    invoiceAddressTrans = addressTransRepo.GetPrimaryAddressByReference(RefType.Customer, view.CustomerTableGUID);
                }
                CompanyParameter companyParameter = ConditionService.IsNotZero(view.ServiceFeeTrans.Count()) ? companyParameterRepo.GetCompanyParameterByCompanyNoTracking(view.ServiceFeeTrans.FirstOrDefault().CompanyGUID) : null;
                #endregion set Variable

                List<InvoiceRevenueType> invoiceRevenueTypeList = invoiceRevenueTypeRepo.GetInvoiceRevenueTypeByIdNoTracking(view.ServiceFeeTrans.Select(w => w.InvoiceRevenueTypeGUID));

                if (GenInvoiceFromServiceFeeTransValidation(companyParameter, invoiceAddressTrans, view.ServiceFeeTrans, invoiceRevenueTypeList))
                {
                    List<ServiceFeeTrans> parmServiceFeeTransGenInvoiceList =
                    (from serviceFeeTrans in view.ServiceFeeTrans.Where(w => w.FeeAmount != 0)
                     join invoiceRevenueType in invoiceRevenueTypeList.Where(w => !w.IntercompanyTableGUID.HasValue)
                     on serviceFeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                     select serviceFeeTrans).ToList();
                    if (parmServiceFeeTransGenInvoiceList.Count > 0)
                    {
                        genInvoiceFromServiceFeeTransResultView =
                            CreateInvoiceTableInvoiceLineFromServiceFeeTrans(genInvoiceFromServiceFeeTransResultView, view, companyParameter, parmServiceFeeTransGenInvoiceList, invoiceAddressTrans);
                    }

                    List<ServiceFeeTrans> parmServiceFeeTransGenIntercompanyInvoiceList =
                    (from serviceFeeTrans in view.ServiceFeeTrans.Where(w => w.FeeAmount != 0)
                     join invoiceRevenueType in invoiceRevenueTypeList.Where(w => w.IntercompanyTableGUID.HasValue)
                     on serviceFeeTrans.InvoiceRevenueTypeGUID equals invoiceRevenueType.InvoiceRevenueTypeGUID
                     select serviceFeeTrans).ToList();
                    if (parmServiceFeeTransGenIntercompanyInvoiceList.Count > 0)
                    {
                        genInvoiceFromServiceFeeTransResultView.IntercompanyInvoiceTable = CreateIntercompanyInvoiceTableFromServiceFeeTrans(view, companyParameter, parmServiceFeeTransGenIntercompanyInvoiceList, invoiceAddressTrans, invoiceRevenueTypeList);
                    }
                }

                return genInvoiceFromServiceFeeTransResultView;
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private bool GenInvoiceFromServiceFeeTransValidation(CompanyParameter companyParameter, AddressTrans invoiceAddressTrans, List<ServiceFeeTrans> serviceFeeTrans, List<InvoiceRevenueType> invoiceRevenueTypeList)
        {
            try
            {
                IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
                SmartAppException ex = new SmartAppException("ERROR.ERROR");

                bool isServiceFeeTrans = ConditionService.IsNotZero(serviceFeeTrans.Count());
                if (isServiceFeeTrans && companyParameter.ServiceFeeInvTypeGUID == null)
                {
                    ex.AddData("ERROR.90047", new string[] { "LABEL.SERVICE_FEE_INVOICE_TYPE_ID", "LABEL.COMPANY_PARAMETER" });
                }

                bool isServiceFeeTransTaxTableGUID = ConditionService.IsNotZero(serviceFeeTrans.Where(w => w.TaxTableGUID != null).Count());
                bool isNotInvoiceAddressTranTaxBranchId = invoiceAddressTrans != null ? ConditionService.IsNullOrEmpty(invoiceAddressTrans.TaxBranchId) : true;
                if (isServiceFeeTransTaxTableGUID && isNotInvoiceAddressTranTaxBranchId)
                {
                    ex.AddData("ERROR.90148", new string[] { "LABEL.TAX_BRANCH_ID", "LABEL.ADDRESS_TRANSACTIONS", (invoiceAddressTrans != null && ConditionService.IsNotNullAndNotEmpty(invoiceAddressTrans.Name) ? invoiceAddressTrans.Name : " "), "LABEL.SERVICE_FEE_TRANSACTIONS", "LABEL.TAX_CODE" });
                }

                List<InvoiceRevenueType> invoicRevenueTypeHasIntercomPanyAndServiceFeeTransSettleAmountNotZero = invoiceRevenueTypeList.Where(w => w.IntercompanyTableGUID.HasValue && serviceFeeTrans.Where(w => w.SettleAmount != 0).Select(s => s.InvoiceRevenueTypeGUID).Contains(w.InvoiceRevenueTypeGUID)).ToList();
                bool isInvoicRevenueTypeIntercomPanyAndServiceFeeTransSettleAmountNotZero = ConditionService.IsNotZero(invoicRevenueTypeHasIntercomPanyAndServiceFeeTransSettleAmountNotZero.Count());
                if (isInvoicRevenueTypeIntercomPanyAndServiceFeeTransSettleAmountNotZero)
                {
                    List<Intercompany> intercompanys = intercompanyRepo.GetIntercompanyByIdNoTracking(invoicRevenueTypeHasIntercomPanyAndServiceFeeTransSettleAmountNotZero.Select(s => s.IntercompanyTableGUID.Value));
                    invoicRevenueTypeHasIntercomPanyAndServiceFeeTransSettleAmountNotZero.ForEach(invoicRevenueType =>
                    {
                        string intercompanyId = intercompanys.Where(w => w.IntercompanyGUID == invoicRevenueType.IntercompanyTableGUID).FirstOrDefault().IntercompanyId;
                        ex.AddData("ERROR.90149", new string[] { "LABEL.SETTLEMENT", "LABEL.SERVICE_FEE_TYPE_ID", invoicRevenueType.RevenueTypeId, "LABEL.INTERCOMPANY_ID", intercompanyId });
                    });
                }

                if (ex.Data.Count > 0)
                {
                    throw SmartAppUtil.AddStackTrace(ex);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw SmartAppUtil.AddStackTrace(ex);
            }
        }
        private GenInvoiceFromServiceFeeTransResultView CreateInvoiceTableInvoiceLineFromServiceFeeTrans(GenInvoiceFromServiceFeeTransResultView result,
                                                                                                        GenInvoiceFromServiceFeeTransParamView view,
                                                                                                        CompanyParameter companyParameter,
                                                                                                        List<ServiceFeeTrans> parmServiceFeeTransList,
                                                                                                        AddressTrans invoiceAddressTrans)
        {
            try
            {
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                IExchangeRateService exchangeRateService = new ExchangeRateService(db);
                IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
                ICreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IInvoiceTypeRepo invoiceTypeRepo = new InvoiceTypeRepo(db);
                ICompanyRepo companyRepo = new CompanyRepo(db);
                IInvoiceService invoiceService = new InvoiceService(db);

                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(view.CustomerTableGUID);
                DocumentStatus draft = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)InvoiceStatus.Draft).ToString());
                InvoiceType invoiceType = invoiceTypeRepo.GetInvoiceTypeByIdNoTracking(companyParameter.ServiceFeeInvTypeGUID.Value);
                Company company = companyRepo.GetCompanyByIdNoTracking(companyParameter.CompanyGUID);
                decimal exchangeRate = exchangeRateService.GetExchangeRate(customerTable.CurrencyGUID, view.IssuedDate);

                #region addressTrans
                AddressTrans mailingAddr;
                if (view.CreditAppTableGUID.HasValue)
                {
                    CreditAppTable creditAppTable = creditAppTableRepo.GetCreditAppTableByIdNoTracking(view.CreditAppTableGUID.Value);
                    if (creditAppTable.MailingReceipAddressGUID.HasValue)
                    {
                        mailingAddr = addressTransRepo.GetAddressTransByIdNoTracking(creditAppTable.MailingReceipAddressGUID.Value);
                    }
                    else
                    {
                        mailingAddr = null;
                    }
                }
                else
                {
                    mailingAddr = invoiceAddressTrans;
                }
                #endregion

                foreach (var serviceFeeTrans in parmServiceFeeTransList)
                {
                    #region invoiceTable
                    InvoiceTable invoiceTable = new InvoiceTable()
                    {
                        InvoiceTableGUID = Guid.NewGuid(),
                        InvoiceId = string.Empty,
                        IssuedDate = view.IssuedDate,
                        DueDate = view.IssuedDate,
                        CustomerTableGUID = view.CustomerTableGUID,
                        CustomerName = customerTable.Name,
                        CurrencyGUID = customerTable.CurrencyGUID,
                        ExchangeRate = exchangeRate,
                        InvoiceAddress1 = invoiceAddressTrans?.Address1,
                        InvoiceAddress2 = invoiceAddressTrans?.Address2,
                        TaxBranchId = invoiceAddressTrans != null ? invoiceAddressTrans.TaxBranchId : string.Empty,
                        TaxBranchName = invoiceAddressTrans != null ? invoiceAddressTrans.TaxBranchName : string.Empty,
                        MailingInvoiceAddress1 = mailingAddr?.Address1,
                        MailingInvoiceAddress2 = mailingAddr?.Address2,
                        DocumentStatusGUID = draft.DocumentStatusGUID,
                        InvoiceTypeGUID = companyParameter.ServiceFeeInvTypeGUID.Value,
                        ProductInvoice = invoiceType.ProductInvoice,
                        SuspenseInvoiceType = invoiceType.SuspenseInvoiceType,
                        ProductType = (int)view.ProductType,
                        CreditAppTableGUID = view.CreditAppTableGUID,
                        RefType = (int)RefType.ServiceFeeTrans,
                        RefGUID = serviceFeeTrans.ServiceFeeTransGUID,
                        DocumentId = view.DocumentId,
                        BuyerTableGUID = null,
                        BuyerInvoiceTableGUID = null,
                        BuyerAgreementTableGUID = null,
                        InvoiceAmountBeforeTax = serviceFeeTrans.AmountBeforeTax,
                        InvoiceAmountBeforeTaxMST = (serviceFeeTrans.AmountBeforeTax * exchangeRate).Round(),
                        TaxAmount = serviceFeeTrans.TaxAmount,
                        TaxAmountMST = (serviceFeeTrans.TaxAmount * exchangeRate).Round(),
                        InvoiceAmount = serviceFeeTrans.AmountIncludeTax,
                        InvoiceAmountMST = (serviceFeeTrans.AmountIncludeTax * exchangeRate).Round(),
                        WHTAmount = serviceFeeTrans.WHTAmount,
                        WHTBaseAmount = serviceFeeTrans.WithholdingTaxTableGUID.HasValue ? (serviceFeeTrans.WHTAmount * exchangeRate).Round() : 0,
                        MarketingPeriod = 0,
                        AccountingPeriod = 0,
                        Remark = string.Empty,
                        OrigInvoiceAmount = serviceFeeTrans.OrigInvoiceAmount,
                        OrigTaxInvoiceAmount = serviceFeeTrans.OrigTaxInvoiceAmount,
                        CNReasonGUID = serviceFeeTrans.CNReasonGUID,
                        Dimension1GUID = serviceFeeTrans.Dimension1GUID,
                        Dimension2GUID = serviceFeeTrans.Dimension2GUID,
                        Dimension3GUID = serviceFeeTrans.Dimension3GUID,
                        Dimension4GUID = serviceFeeTrans.Dimension4GUID,
                        Dimension5GUID = serviceFeeTrans.Dimension5GUID,
                        MethodOfPaymentGUID = null,
                        RefTaxInvoiceGUID = null,
                        OrigInvoiceId = serviceFeeTrans.OrigInvoiceId,
                        OrigTaxInvoiceId = serviceFeeTrans.OrigTaxInvoiceId,
                        RefInvoiceGUID = null,
                        CompanyGUID = serviceFeeTrans.CompanyGUID,
                        BranchGUID = company.DefaultBranchGUID.HasValue ? company.DefaultBranchGUID.Value : Guid.Empty,
                    };
                    result.InvoiceTable.Add(invoiceTable);
                    #endregion
                    #region invoiceLine
                    InvoiceLine invoiceLine = new InvoiceLine()
                    {
                        InvoiceLineGUID = Guid.NewGuid(),
                        InvoiceTableGUID = invoiceTable.InvoiceTableGUID,
                        LineNum = 1,
                        Qty = 1,
                        UnitPrice = serviceFeeTrans.AmountBeforeTax,
                        TotalAmountBeforeTax = serviceFeeTrans.AmountBeforeTax,
                        TaxAmount = serviceFeeTrans.TaxAmount,
                        WHTAmount = serviceFeeTrans.WHTAmount,
                        TotalAmount = serviceFeeTrans.AmountIncludeTax,
                        WHTBaseAmount = serviceFeeTrans.WithholdingTaxTableGUID.HasValue ? serviceFeeTrans.AmountBeforeTax : 0,
                        InvoiceRevenueTypeGUID = serviceFeeTrans.InvoiceRevenueTypeGUID,
                        Dimension1GUID = serviceFeeTrans.Dimension1GUID,
                        Dimension2GUID = serviceFeeTrans.Dimension2GUID,
                        Dimension3GUID = serviceFeeTrans.Dimension3GUID,
                        Dimension4GUID = serviceFeeTrans.Dimension4GUID,
                        Dimension5GUID = serviceFeeTrans.Dimension5GUID,
                        TaxTableGUID = serviceFeeTrans.TaxTableGUID,
                        WithholdingTaxTableGUID = serviceFeeTrans.WithholdingTaxTableGUID,
                        ProdUnitGUID = null,
                        InvoiceText = serviceFeeTrans.Description,
                        CompanyGUID = serviceFeeTrans.CompanyGUID,
                        BranchGUID = invoiceTable.BranchGUID,
                    };
                    result.InvoiceLine.Add(invoiceLine);
                    #endregion
                }
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private List<IntercompanyInvoiceTable> CreateIntercompanyInvoiceTableFromServiceFeeTrans(GenInvoiceFromServiceFeeTransParamView view,
                                                                                     CompanyParameter companyParameter,
                                                                                     List<ServiceFeeTrans> parmServiceFeeTransList,
                                                                                     AddressTrans invoiceAddressTrans,
                                                                                     List<InvoiceRevenueType> invoiceRevenueTypeHasIntercompanyList)
        {
            try
            {
                INumberSequenceService numberSequenceService = new NumberSequenceService(db);
                IExchangeRateService exchangeRateService = new ExchangeRateService(db);
                ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
                IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
                IIntercompanyInvoiceTableRepo intercompanyInvoiceTableRepo = new IntercompanyInvoiceTableRepo(db);
                INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);

                CustomerTable customerTable = customerTableRepo.GetCustomerTableByIdNoTracking(view.CustomerTableGUID);
                DocumentStatus posted = documentStatusRepo.GetDocumentStatusByStatusIdNoTracking(((int)IntercompanyInvoiceStatus.Posted).ToString());
                decimal exchangeRate = exchangeRateService.GetExchangeRate(customerTable.CurrencyGUID, view.IssuedDate);

                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                List<IntercompanyInvoiceTable> intercompanyInvoiceTables = new List<IntercompanyInvoiceTable>();
                bool isManual = numberSequenceService.IsManualByReferenceId(parmServiceFeeTransList.FirstOrDefault().CompanyGUID, ReferenceId.IntercompanyInvoice);

                if (isManual)
                {
                    ex.AddData("ERROR.90050", "LABEL.INTERCOMPANY_ID");
                    throw SmartAppUtil.AddStackTrace(ex);
                }

                foreach (var serviceFeeTrans in parmServiceFeeTransList)
                {
                    InvoiceRevenueType invoiceRevenueType = invoiceRevenueTypeHasIntercompanyList.Where(w => w.InvoiceRevenueTypeGUID == serviceFeeTrans.InvoiceRevenueTypeGUID).FirstOrDefault();
                    IntercompanyInvoiceTable intercompanyInvoiceTable = new IntercompanyInvoiceTable()
                    {
                        IntercompanyInvoiceTableGUID = Guid.NewGuid(),
                        IntercompanyInvoiceId = string.Empty,
                        IssuedDate = view.IssuedDate,
                        DueDate = view.IssuedDate,
                        CustomerTableGUID = view.CustomerTableGUID,
                        CustomerName = customerTable.Name,
                        CurrencyGUID = customerTable.CurrencyGUID,
                        ExchangeRate = exchangeRate,
                        InvoiceAddressTransGUID = invoiceAddressTrans.AddressTransGUID,
                        TaxBranchId = invoiceAddressTrans != null ? invoiceAddressTrans.TaxBranchId : string.Empty,
                        TaxBranchName = invoiceAddressTrans != null ? invoiceAddressTrans.TaxBranchName : string.Empty,
                        DocumentStatusGUID = posted.DocumentStatusGUID,
                        InvoiceTypeGUID = companyParameter.ServiceFeeInvTypeGUID.Value,
                        ProductType = (int)view.ProductType,
                        CreditAppTableGUID = view.CreditAppTableGUID,
                        RefType = (int)RefType.ServiceFeeTrans,
                        RefGUID = serviceFeeTrans.ServiceFeeTransGUID,
                        DocumentId = view.DocumentId,
                        InvoiceRevenueTypeGUID = serviceFeeTrans.InvoiceRevenueTypeGUID,
                        FeeLedgerAccount = invoiceRevenueType.FeeLedgerAccount,
                        InvoiceText = serviceFeeTrans.Description,
                        IntercompanyGUID = invoiceRevenueType.IntercompanyTableGUID.Value,
                        TaxTableGUID = serviceFeeTrans.TaxTableGUID,
                        WithholdingTaxTableGUID = serviceFeeTrans.WithholdingTaxTableGUID,
                        InvoiceAmount = serviceFeeTrans.AmountIncludeTax,
                        OrigInvoiceAmount = serviceFeeTrans.OrigInvoiceAmount,
                        OrigTaxInvoiceAmount = serviceFeeTrans.OrigTaxInvoiceAmount,
                        OrigInvoiceId = serviceFeeTrans.OrigInvoiceId,
                        OrigTaxInvoiceId = serviceFeeTrans.OrigTaxInvoiceId,
                        CNReasonGUID = serviceFeeTrans.CNReasonGUID,
                        Dimension1GUID = serviceFeeTrans.Dimension1GUID,
                        Dimension2GUID = serviceFeeTrans.Dimension2GUID,
                        Dimension3GUID = serviceFeeTrans.Dimension3GUID,
                        Dimension4GUID = serviceFeeTrans.Dimension4GUID,
                        Dimension5GUID = serviceFeeTrans.Dimension5GUID,
                        CompanyGUID = serviceFeeTrans.CompanyGUID,
                    };
                    intercompanyInvoiceTables.Add(intercompanyInvoiceTable);
                }

                #region gen multiple number sequences
                NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(parmServiceFeeTransList.FirstOrDefault().CompanyGUID, ReferenceId.IntercompanyInvoice);
                var numberSequences = intercompanyInvoiceTables.Select(s => new NumberSequences
                {
                    Key = s.IntercompanyInvoiceTableGUID,
                    CompanyGUID = s.CompanyGUID,
                    NumberSeqTableGUID = numberSeqParameter.NumberSeqTableGUID.Value
                }).ToList();
                numberSequences = numberSequenceService.GetNumber(numberSequences);

                List<IntercompanyInvoiceTable> dbIntercompanyInvoiceTable = intercompanyInvoiceTableRepo.GetIntercompanyInvoiceTableByIntercompanyInvoiceIdNoTracking(numberSequences.Select(s => s.GeneratedId));
                if (dbIntercompanyInvoiceTable.Count > 0)
                {
                    ex.AddData("ERROR.DUPLICATE", new string[] { "LABEL.INTERCOMPANY_INVOICE_ID" });
                }
                else
                {
                    intercompanyInvoiceTables.Select(s =>
                    {
                        s.IntercompanyInvoiceId = numberSequences.Where(w => w.Key == s.IntercompanyInvoiceTableGUID).FirstOrDefault().GeneratedId;
                        return s;
                    }).ToList();
                }
                #endregion gen multiple number sequences

                return intercompanyInvoiceTables;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region shared CalcTaxAmount
        public decimal CalcTaxAmount(decimal feeAmount, decimal taxValue, bool includeTax)
        {
            try
            {
                decimal taxAmount = 0;
                if (includeTax == true)
                {
                    taxAmount = ((feeAmount * taxValue) / (100 + taxValue)).Round(2);
                }
                else
                {
                    taxAmount = (feeAmount * (taxValue / 100)).Round(2);
                }
                return taxAmount;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region shared CalcWHTAmount
        public decimal CalcWHTAmount(decimal amountBeforeTax, decimal whTaxValue)
        {
            try
            {
                decimal whtAmount = 0;
                whtAmount = (amountBeforeTax * (whTaxValue / 100)).Round();
                return whtAmount;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region copy service fee from CA
        private bool ValidationCopyServiceFeeCondCARequest(string mainAgreementTableGUID, string creditAppRequestTableGUID)
        {
            try
            {
                SmartAppException ex = new SmartAppException("ERROR.ERROR");
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
                var result = mainAgreementTableRepo.GetByIdvw(mainAgreementTableGUID.StringToGuid());
                var serviceFeeTransConditionList = serviceFeeConditionTransRepo.GetServiceFeeConditionTransViewMapByRefGUIDAndTypeNoTracking(creditAppRequestTableGUID.StringToGuid(), (int)RefType.CreditAppRequestTable);
                if (result.DocumentStatus_StatusId != null)
                {
                    if (Int32.Parse(result.DocumentStatus_StatusId) >= (int)MainAgreementStatus.Signed)
                    {
                        ex.AddData("ERROR.90089", new string[] { "LABEL.MAIN_AGREEMENT" });
                    }
                    if (ConditionService.IsEqualZero(serviceFeeTransConditionList.Count()))
                    {
                        ex.AddData("ERROR.90003", new string[] { });
                    }
                    if (ex.Data.Count > 0)
                    {
                        throw SmartAppUtil.AddStackTrace(ex);
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public ManageAgreementView CreateCopyServiceFeeCondCARequest(MainAgreementTableItemView view)
        {
            try
            {
                ValidationCopyServiceFeeCondCARequest(view.MainAgreementTableGUID, view.CreditAppRequestTableGUID);

                NotificationResponse success = new NotificationResponse();
                List<string> labels = new List<string>();
                ManageAgreementView resultReturn = new ManageAgreementView();
                string userName = db.GetUserName();
                DateTime systemDate = DateTime.Now;
                IServiceFeeConditionTransRepo serviceFeeConditionTransRepo = new ServiceFeeConditionTransRepo(db);
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                List<ServiceFeeTrans> serviceFeeList = new List<ServiceFeeTrans>();
                int orderserviceFeeTrans = serviceFeeTransRepo.GetMaxOrdering(view.MainAgreementTableGUID.StringToGuid(), (int)RefType.MainAgreement);
                ITaxTableService taxTableService = new TaxTableService(db);
                IWithholdingTaxTableService whtService = new WithholdingTaxTableService(db);
                IServiceFeeTransService serviceFeeTransService = new ServiceFeeTransService(db);
                IMainAgreementTableRepo mainAgreementTableRepo = new MainAgreementTableRepo(db);
                ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
                ICreditAppRequestTableAmendRepo creditAppRequestTableAmendRepo = new CreditAppRequestTableAmendRepo(db);
                var mainAgreement = mainAgreementTableRepo.GetMainAgreementTableByIdNoTracking(view.MainAgreementTableGUID.StringToGuid());
                CreditAppRequestTable creditappRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(view.CreditAppRequestTableGUID.StringToGuid());
                CreditAppRequestTableAmend creditAppRequestTableAmend = creditAppRequestTableAmendRepo.GetCreditAppRequestTableAmendByCreditAppRequestTable(view.CreditAppRequestTableGUID.StringToGuid());
                var serviceFeeTransConditionList = serviceFeeConditionTransRepo.GetServiceFeeConditionTransViewMapByRefGUIDAndTypeNoTracking(view.CreditAppRequestTableGUID.StringToGuid(), (int)RefType.CreditAppRequestTable);
                if (ConditionService.IsNotZero(serviceFeeTransConditionList.Count()))
                {
                    serviceFeeTransConditionList.ForEach(f =>
                    {
                        var taxValue = f.InvoiceRevenueType_FeeTaxGUID != null ? taxTableService.GetTaxValue(f.InvoiceRevenueType_FeeTaxGUID.GuidNullOrEmptyToString().StringToGuid(), mainAgreement.AgreementDate) : 0;
                        var whtValue = f.InvoiceRevenueType_FeeWHTGUID != null ? whtService.GetWHTValue(f.InvoiceRevenueType_FeeWHTGUID.GuidNullOrEmptyToString().StringToGuid(), mainAgreement.AgreementDate) : 0;
                        orderserviceFeeTrans += 1;
                        ServiceFeeTrans newServiceFeeTrans = new ServiceFeeTrans();
                        newServiceFeeTrans.ServiceFeeTransGUID = Guid.NewGuid();
                        newServiceFeeTrans.Ordering = orderserviceFeeTrans;
                        newServiceFeeTrans.InvoiceRevenueTypeGUID = f.InvoiceRevenueTypeGUID;
                        newServiceFeeTrans.Description = f.Description;
                        newServiceFeeTrans.IncludeTax = false;
                        newServiceFeeTrans.FeeAmount = f.AmountBeforeTax;
                        newServiceFeeTrans.AmountBeforeTax = f.AmountBeforeTax;
                        newServiceFeeTrans.TaxTableGUID = f.InvoiceRevenueType_FeeTaxGUID;
                        newServiceFeeTrans.TaxAmount = serviceFeeTransService.CalcTaxAmount(newServiceFeeTrans.AmountBeforeTax, taxValue, newServiceFeeTrans.IncludeTax);
                        newServiceFeeTrans.AmountIncludeTax = newServiceFeeTrans.FeeAmount + newServiceFeeTrans.TaxAmount;
                        newServiceFeeTrans.WithholdingTaxTableGUID = f.InvoiceRevenueType_FeeWHTGUID;
                        newServiceFeeTrans.WHTAmount = serviceFeeTransService.CalcWHTAmount(f.AmountBeforeTax, whtValue);
                        newServiceFeeTrans.SettleAmount = 0;
                        newServiceFeeTrans.SettleInvoiceAmount = 0;
                        newServiceFeeTrans.SettleWHTAmount = 0;
                        newServiceFeeTrans.RefType = (int)RefType.MainAgreement;
                        newServiceFeeTrans.RefGUID = view.MainAgreementTableGUID.StringToGuid();
                        newServiceFeeTrans.CreatedBy = userName;
                        newServiceFeeTrans.CreatedDateTime = systemDate;
                        newServiceFeeTrans.Owner = userName;
                        newServiceFeeTrans.OwnerBusinessUnitGUID = view.OwnerBusinessUnitGUID.StringToGuid();
                        newServiceFeeTrans.CompanyGUID = view.CompanyGUID.StringToGuid();
                        if (creditappRequestTable.CreditAppRequestType == (int)CreditAppRequestType.ActiveAmendCustomerCreditLimit
                            || creditappRequestTable.CreditAppRequestType == (int)CreditAppRequestType.AmendCustomerInfo)
                        {
                            newServiceFeeTrans.CNReasonGUID = creditAppRequestTableAmend.CNReasonGUID;
                            newServiceFeeTrans.OrigInvoiceId = creditAppRequestTableAmend.OrigInvoiceId;
                            newServiceFeeTrans.OrigInvoiceAmount = creditAppRequestTableAmend.OrigInvoiceAmount;
                            newServiceFeeTrans.OrigTaxInvoiceId = creditAppRequestTableAmend.OrigTaxInvoiceId;
                            newServiceFeeTrans.OrigTaxInvoiceAmount = creditAppRequestTableAmend.OrigTaxInvoiceAmount;
                        }
                        serviceFeeList.Add(newServiceFeeTrans);
                    });
                    using (var transaction = UnitOfWork.ContextTransaction())
                    {
                        BulkInsert(serviceFeeList);
                        UnitOfWork.Commit(transaction);
                    }
                }
                labels.Add("LABEL.SERVICE_FEE_TRANSACTIONS");
                success.AddData("SUCCESS.90001", labels.ToArray());
                resultReturn.Notification = success;
                return resultReturn;

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }

        }
        #endregion copy service fee from CA

        #region GetTotalAmountByReference
        public decimal GetTotalAmountByReference(RefType refType, string refGUID)
        {
            try
            {
                IServiceFeeTransRepo serviceFeeTransRepo = new ServiceFeeTransRepo(db);
                IEnumerable<ServiceFeeTrans> serviceFeeTrans = serviceFeeTransRepo.GetServiceFeeTransByReferenceNoTracking(refType, refGUID.StringToGuid());

                List<ServiceFeeTrans> serviceFeeTransEnumerable = serviceFeeTrans.ToList();
                decimal sumAmountIncludeTax = serviceFeeTransEnumerable.Select(x => x.AmountIncludeTax).Sum();
                decimal sumWhtAmount = serviceFeeTransEnumerable.Select(x => x.WHTAmount).Sum();
                decimal totalServiceFee = sumAmountIncludeTax - sumWhtAmount;
                return totalServiceFee;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

        #region GetUpcountryChequeFee        
        public decimal GetUpcountryChequeFee(decimal totalServiceFee)
        {
            try
            {
                decimal upcountryChequeFee = 0;
                if (totalServiceFee > 0)
                {
                    Guid companyGuid = GetCurrentCompany().StringToGuid();
                    CompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
                    CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(companyGuid);
                    decimal value = Math.Round((totalServiceFee * companyParameter.UpcountryChequeFeePct) / 100, 2, MidpointRounding.AwayFromZero);
                    upcountryChequeFee = value <= companyParameter.MinUpcountryChequeFeeAmount ? companyParameter.MinUpcountryChequeFeeAmount : value;
                }

                return upcountryChequeFee;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }

        #endregion

        #endregion
    }
}
