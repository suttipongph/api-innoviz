using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IFinancialStatementTransService
	{

		FinancialStatementTransItemView GetFinancialStatementTransById(string id);
		FinancialStatementTransItemView CreateFinancialStatementTrans(FinancialStatementTransItemView financialStatementTransView);
		FinancialStatementTransItemView UpdateFinancialStatementTrans(FinancialStatementTransItemView financialStatementTransView);
		bool DeleteFinancialStatementTrans(string id);
		bool DeleteFinancialStatementTrans(FinancialStatementTrans financialStatementTrans);
		FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refId, string refGUID, RefType refType);
		void CreateFinancialStatementTransCollection(List<FinancialStatementTransItemView> financialStatementTransItemViews);
		#region function
		CopyFinancialStatementTransView CopyFinancialStatementTrans(CopyFinancialStatementTransView paramView, IEnumerable<FinancialStatementTrans> copyFinancialStatementTrans);
		bool GetCopyFinancialStatementTransByRefTypeValidationCART(CopyFinancialStatementTransView paramView);
		public bool GetCopyFinancialStatementTransByRefTypeValidationCARL(CopyFinancialStatementTransView paramView);
		#endregion

		#region ImportFinancialStatement
		ImportFinancialStatementResultView ImportFinancialStatement(ImportFinancialStatementView importFinancialStatementView);
		ImportFinancialStatementView GetFinancialStatementTransRefId(string refGUID, int refType);
		#endregion

	}
    public class FinancialStatementTransService : SmartAppService, IFinancialStatementTransService
	{
		public FinancialStatementTransService(SmartAppDbContext context) : base(context) { }
		public FinancialStatementTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public FinancialStatementTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public FinancialStatementTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public FinancialStatementTransService() : base() { }

		public FinancialStatementTransItemView GetFinancialStatementTransById(string id)
		{
			try
			{
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				return financialStatementTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialStatementTransItemView CreateFinancialStatementTrans(FinancialStatementTransItemView financialStatementTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				financialStatementTransView = accessLevelService.AssignOwnerBU(financialStatementTransView);
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				FinancialStatementTrans financialStatementTrans = financialStatementTransView.ToFinancialStatementTrans();
				financialStatementTrans = financialStatementTransRepo.CreateFinancialStatementTrans(financialStatementTrans);
				base.LogTransactionCreate<FinancialStatementTrans>(financialStatementTrans);
				UnitOfWork.Commit();
				return financialStatementTrans.ToFinancialStatementTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialStatementTransItemView UpdateFinancialStatementTrans(FinancialStatementTransItemView financialStatementTransView)
		{
			try
			{
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				FinancialStatementTrans inputFinancialStatementTrans = financialStatementTransView.ToFinancialStatementTrans();
				FinancialStatementTrans dbFinancialStatementTrans = financialStatementTransRepo.Find(inputFinancialStatementTrans.FinancialStatementTransGUID);
				dbFinancialStatementTrans = financialStatementTransRepo.UpdateFinancialStatementTrans(dbFinancialStatementTrans, inputFinancialStatementTrans);
				base.LogTransactionUpdate<FinancialStatementTrans>(GetOriginalValues<FinancialStatementTrans>(dbFinancialStatementTrans), dbFinancialStatementTrans);
				UnitOfWork.Commit();
				return dbFinancialStatementTrans.ToFinancialStatementTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteFinancialStatementTrans(string item)
		{
			try
			{
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				Guid financialStatementTransGUID = new Guid(item);
				FinancialStatementTrans financialStatementTrans = financialStatementTransRepo.Find(financialStatementTransGUID);
				financialStatementTransRepo.Remove(financialStatementTrans);
				base.LogTransactionDelete<FinancialStatementTrans>(financialStatementTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteFinancialStatementTrans(FinancialStatementTrans financialStatementTrans)
		{
			try
			{
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);				
				financialStatementTransRepo.Remove(financialStatementTrans);
				base.LogTransactionDelete<FinancialStatementTrans>(financialStatementTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public FinancialStatementTransItemView GetFinancialStatementTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new FinancialStatementTransItemView
				{
					FinancialStatementTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void CreateFinancialStatementTransCollection(List<FinancialStatementTransItemView> financialStatementTransItemViews)
		{
			try
			{
				if (financialStatementTransItemViews.Any())
				{
					IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
					List<FinancialStatementTrans> financialStatementTrans = financialStatementTransItemViews.ToFinancialStatementTrans().ToList();
					financialStatementTransRepo.ValidateAdd(financialStatementTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(financialStatementTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#region function
		public bool GetCopyFinancialStatementTransByRefTypeValidationCART(CopyFinancialStatementTransView paramView)
		{
			try
			{
				ICreditAppRequestTableRepo creditAppRequestTableRepo = new CreditAppRequestTableRepo(db);
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				CreditAppRequestTable creditAppRequestTable = creditAppRequestTableRepo.GetCreditAppRequestTableByIdNoTracking(paramView.RefGUID.StringToGuid());
				IEnumerable<FinancialStatementTrans> copyFinancialStatementTranses = financialStatementTransRepo.GetCopyFinancialStatementTranByRefTypes(creditAppRequestTable.CustomerTableGUID, (int)RefType.Customer);
				return GetCopyFinancialStatementTransByRefTypeValidation(paramView, copyFinancialStatementTranses);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetCopyFinancialStatementTransByRefTypeValidationCARL(CopyFinancialStatementTransView paramView)
		{
			try
			{
				ICreditAppRequestLineRepo creditAppRequestLineRepo = new CreditAppRequestLineRepo(db);
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				CreditAppRequestLine creditAppRequestLine = creditAppRequestLineRepo.GetCreditAppRequestLineByIdNoTracking(paramView.RefGUID.StringToGuid());
				IEnumerable<FinancialStatementTrans> copyFinancialStatementTranses = financialStatementTransRepo.GetCopyFinancialStatementTranByRefTypes(creditAppRequestLine.BuyerTableGUID, (int)RefType.Buyer);
				return GetCopyFinancialStatementTransByRefTypeValidation(paramView, copyFinancialStatementTranses);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool GetCopyFinancialStatementTransByRefTypeValidation(CopyFinancialStatementTransView param, IEnumerable<FinancialStatementTrans> copyList)
		{
			try
			{
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				ICompanyParameterRepo companyParameterRepo = new CompanyParameterRepo(db);
				CompanyParameter companyParameter = companyParameterRepo.GetCompanyParameterByCompanyNoTracking(db.GetCompanyFilter().StringToGuid());
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				
				if (param.Year < 1)
				{
					ex.AddData("ERROR.GREATER_THAN_ZERO", new string[] { "LABEL.COPY_FINANCIAL_STATEMENT_TRANS" });
				}
				if (param.Year > companyParameter.MaxCopyFinancialStatementNumOfYear)
				{
					ex.AddData("ERROR.90031", new string[] { param.Year.ToString(), companyParameter.MaxCopyFinancialStatementNumOfYear.ToString() });
				}
				if(copyList.Count() == 0)
                {
					ex.AddData("ERROR.90003");
				}
				else
                {
					int startYear = 0;
					FinancialStatementTrans financialStatementTrans = copyList.OrderByDescending(m => m.Year).FirstOrDefault();
					startYear = financialStatementTrans.Year - param.Year;
					if(copyList.Where(w => w.Year > startYear).Count() == 0)
                    {
						ex.AddData("ERROR.90003");
					}
				}
				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				IEnumerable<FinancialStatementTrans> financialStatementTranses = financialStatementTransRepo.GetCopyFinancialStatementTranByRefTypes(param.RefGUID.StringToGuid(), param.RefType);
				return financialStatementTranses.Any() ? false : true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CopyFinancialStatementTransView CopyFinancialStatementTrans(CopyFinancialStatementTransView paramView, IEnumerable<FinancialStatementTrans> copyFinancialStatementTrans)
        {
            try
            {
				IFinancialStatementTransRepo financialStatementTransRepo = new FinancialStatementTransRepo(db);
				CopyFinancialStatementTransView result = new CopyFinancialStatementTransView();
				NotificationResponse success = new NotificationResponse();

				int startYear = 0;
				List<FinancialStatementTrans> deleteFinancialStatementTranses = financialStatementTransRepo.GetCopyFinancialStatementTranByRefTypes(paramView.RefGUID.StringToGuid(), paramView.RefType).ToList();
				
				List<FinancialStatementTrans> createFinancialStatementTranses = new List<FinancialStatementTrans>();
				if (ConditionService.IsNotZero(copyFinancialStatementTrans.Count()))
				{
					FinancialStatementTrans financialStatementTrans = copyFinancialStatementTrans.OrderByDescending(m => m.Year).FirstOrDefault();
					startYear = financialStatementTrans.Year - paramView.Year;
					createFinancialStatementTranses = copyFinancialStatementTrans.Where(w => w.Year > startYear).Select(s => new FinancialStatementTrans()
					{
						FinancialStatementTransGUID = Guid.NewGuid(),
						Year = s.Year,
						Ordering = s.Ordering,
						Description = s.Description,
						Amount = s.Amount,
						RefType = paramView.RefType,
						RefGUID = paramView.RefGUID.StringToGuid(),
						CompanyGUID = s.CompanyGUID
				}).ToList();
				}
				using (var transaction = UnitOfWork.ContextTransaction())
				{
					if (ConditionService.IsNotZero(deleteFinancialStatementTranses.Count()))
					{
						this.BulkDelete(deleteFinancialStatementTranses);
					}
					if (ConditionService.IsNotZero(createFinancialStatementTranses.Count()))
					{
						this.BulkInsert(createFinancialStatementTranses);
					}
					UnitOfWork.Commit(transaction);
				}
				success.AddData("SUCCESS.90002", new string[] { paramView.ResultLabel });
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion

		#region ImportFinancialStatement

		public ImportFinancialStatementView GetFinancialStatementTransRefId(string refGUID, int refType)
		{
			try
			{
				ImportFinancialStatementView importFinancialStatementView = new ImportFinancialStatementView();
				if (RefType.Customer == (RefType)refType)
				{
					ICustomerTableRepo customerTableRepo = new CustomerTableRepo(db);
					CustomerTableItemView customerTableItemView = customerTableRepo.GetByIdvw(refGUID.StringToGuid());
					importFinancialStatementView.RefId = customerTableItemView.CustomerId;
					importFinancialStatementView.Name = customerTableItemView.Name;
				}
				else if (RefType.Buyer == (RefType)refType)
				{
					IBuyerTableRepo buyerTableRepo = new BuyerTableRepo(db);
					BuyerTableItemView buyerTableItemView = buyerTableRepo.GetByIdvw(refGUID.StringToGuid());
					importFinancialStatementView.RefId = buyerTableItemView.BuyerId;
					importFinancialStatementView.Name = buyerTableItemView.BuyerName;
				}
			
				return importFinancialStatementView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ImportFinancialStatementResultView ImportFinancialStatement(ImportFinancialStatementView importFinancialStatementView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				FileUpload fileUpload = new FileUpload { FileInfos = new List<FileInformation>() { importFinancialStatementView.FileInfo } };
				FileHelper fileHelper = new FileHelper();
				IEnumerable<ImportFinancialStatementView> importFinancialStatementViews = fileHelper.MapInputExcelFileToSingleEntityType<ImportFinancialStatementView>(fileUpload, ImportFinancialStatementView.GetMapper());
				if (importFinancialStatementViews.Count() > 0)
				{
					GetImportFinancialStatementValidation(importFinancialStatementViews);
					List<FinancialStatementTrans> financialStatementTrans = new List<FinancialStatementTrans>();
					foreach (var item in importFinancialStatementViews)
					{
						financialStatementTrans.Add(new FinancialStatementTrans()
						{
							CompanyGUID = db.GetCompanyFilter().StringToGuid(),
							RefGUID = importFinancialStatementView.RefGUID.StringToGuid(),
							RefType = Convert.ToInt32(importFinancialStatementView.RefType),
							Year = Convert.ToInt32(item.Year),
							Ordering = Convert.ToInt32(item.Ordering),
							Description = item.Description,
							Amount = Convert.ToDecimal(item.Amount)
						});
					}
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(financialStatementTrans.ToList(), true);
						UnitOfWork.Commit(transaction);
					}
				}

				NotificationResponse success = new NotificationResponse();
				ImportFinancialStatementResultView importFinancialStatementResultView = new ImportFinancialStatementResultView();
				success.AddData("SUCCESS.90017", new string[] { "LABEL.FINANCIAL_STATEMENT_TRANSACTION" });
				importFinancialStatementResultView.Notification = success;

				return importFinancialStatementResultView;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		private bool GetImportFinancialStatementValidation(IEnumerable<ImportFinancialStatementView> financialStatementTransItemViews)
		{
			try
			{
				SmartAppException ex = new SmartAppException("ERROR.ERROR");
				List<ImportFinancialStatementView> financialStatementTransItemViewList = financialStatementTransItemViews.ToList();
				foreach (var item in financialStatementTransItemViewList)
				{
					
					int index = financialStatementTransItemViewList.IndexOf(item) + 1;
					if (string.IsNullOrEmpty(item.Year) || string.IsNullOrEmpty(item.Ordering) || string.IsNullOrEmpty(item.Amount) || string.IsNullOrEmpty(item.Description))
					{
						ex.AddData("ERROR.90163", index);
					}
					if (item.Year != null && Convert.ToInt32(item.Year) > 9999)
					{
						ex.AddData("ERROR.90144", index, new string[] { "LABEL.YEAR" });
					}
					if (item.Year != null && Convert.ToInt32(item.Year) <= 0)
					{
						ex.AddData("ERROR.GREATER_THAN_ZERO", index, new string[] { "LABEL.YEAR" });
					}
				}

				if (ex.Data.Count > 0)
				{
					throw SmartAppUtil.AddStackTrace(ex);
				}
				else
				{
					return true;
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion
	}
}
