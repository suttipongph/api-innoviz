using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IExposureGroupService
	{

		ExposureGroupItemView GetExposureGroupById(string id);
		ExposureGroupItemView CreateExposureGroup(ExposureGroupItemView exposureGroupView);
		ExposureGroupItemView UpdateExposureGroup(ExposureGroupItemView exposureGroupView);
		bool DeleteExposureGroup(string id);
		ExposureGroupByProductItemView GetExposureGroupByProductById(string id);
		ExposureGroupByProductItemView CreateExposureGroupByProduct(ExposureGroupByProductItemView exposureGroupByProductView);
		ExposureGroupByProductItemView UpdateExposureGroupByProduct(ExposureGroupByProductItemView exposureGroupByProductView);
		bool DeleteExposureGroupByProduct(string id);
		ExposureGroupByProductItemView GetExposureGroupByProductInitialData(string exposureGroupGUID);
	}
	public class ExposureGroupService : SmartAppService, IExposureGroupService
	{
		public ExposureGroupService(SmartAppDbContext context) : base(context) { }
		public ExposureGroupService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ExposureGroupService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ExposureGroupService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ExposureGroupService() : base() { }

		public ExposureGroupItemView GetExposureGroupById(string id)
		{
			try
			{
				IExposureGroupRepo exposureGroupRepo = new ExposureGroupRepo(db);
				return exposureGroupRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExposureGroupItemView CreateExposureGroup(ExposureGroupItemView exposureGroupView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				exposureGroupView = accessLevelService.AssignOwnerBU(exposureGroupView);
				IExposureGroupRepo exposureGroupRepo = new ExposureGroupRepo(db);
				ExposureGroup exposureGroup = exposureGroupView.ToExposureGroup();
				exposureGroup = exposureGroupRepo.CreateExposureGroup(exposureGroup);
				base.LogTransactionCreate<ExposureGroup>(exposureGroup);
				UnitOfWork.Commit();
				return exposureGroup.ToExposureGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExposureGroupItemView UpdateExposureGroup(ExposureGroupItemView exposureGroupView)
		{
			try
			{
				IExposureGroupRepo exposureGroupRepo = new ExposureGroupRepo(db);
				ExposureGroup inputExposureGroup = exposureGroupView.ToExposureGroup();
				ExposureGroup dbExposureGroup = exposureGroupRepo.Find(inputExposureGroup.ExposureGroupGUID);
				dbExposureGroup = exposureGroupRepo.UpdateExposureGroup(dbExposureGroup, inputExposureGroup);
				base.LogTransactionUpdate<ExposureGroup>(GetOriginalValues<ExposureGroup>(dbExposureGroup), dbExposureGroup);
				UnitOfWork.Commit();
				return dbExposureGroup.ToExposureGroupItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteExposureGroup(string item)
		{
			try
			{
				IExposureGroupRepo exposureGroupRepo = new ExposureGroupRepo(db);
				Guid exposureGroupGUID = new Guid(item);
				ExposureGroup exposureGroup = exposureGroupRepo.Find(exposureGroupGUID);
				exposureGroupRepo.Remove(exposureGroup);
				base.LogTransactionDelete<ExposureGroup>(exposureGroup);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public ExposureGroupByProductItemView GetExposureGroupByProductById(string id)
		{
			try
			{
				IExposureGroupByProductRepo exposureGroupByProductRepo = new ExposureGroupByProductRepo(db);
				return exposureGroupByProductRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExposureGroupByProductItemView CreateExposureGroupByProduct(ExposureGroupByProductItemView exposureGroupByProductView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				exposureGroupByProductView = accessLevelService.AssignOwnerBU(exposureGroupByProductView);
				IExposureGroupByProductRepo exposureGroupByProductRepo = new ExposureGroupByProductRepo(db);
				ExposureGroupByProduct exposureGroupByProduct = exposureGroupByProductView.ToExposureGroupByProduct();
				exposureGroupByProduct = exposureGroupByProductRepo.CreateExposureGroupByProduct(exposureGroupByProduct);
				base.LogTransactionCreate<ExposureGroupByProduct>(exposureGroupByProduct);
				UnitOfWork.Commit();
				return exposureGroupByProduct.ToExposureGroupByProductItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExposureGroupByProductItemView UpdateExposureGroupByProduct(ExposureGroupByProductItemView exposureGroupByProductView)
		{
			try
			{
				IExposureGroupByProductRepo exposureGroupByProductRepo = new ExposureGroupByProductRepo(db);
				ExposureGroupByProduct inputExposureGroupByProduct = exposureGroupByProductView.ToExposureGroupByProduct();
				ExposureGroupByProduct dbExposureGroupByProduct = exposureGroupByProductRepo.Find(inputExposureGroupByProduct.ExposureGroupByProductGUID);
				dbExposureGroupByProduct = exposureGroupByProductRepo.UpdateExposureGroupByProduct(dbExposureGroupByProduct, inputExposureGroupByProduct);
				base.LogTransactionUpdate<ExposureGroupByProduct>(GetOriginalValues<ExposureGroupByProduct>(dbExposureGroupByProduct), dbExposureGroupByProduct);
				UnitOfWork.Commit();
				return dbExposureGroupByProduct.ToExposureGroupByProductItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteExposureGroupByProduct(string item)
		{
			try
			{
				IExposureGroupByProductRepo exposureGroupByProductRepo = new ExposureGroupByProductRepo(db);
				Guid exposureGroupByProductGUID = new Guid(item);
				ExposureGroupByProduct exposureGroupByProduct = exposureGroupByProductRepo.Find(exposureGroupByProductGUID);
				exposureGroupByProductRepo.Remove(exposureGroupByProduct);
				base.LogTransactionDelete<ExposureGroupByProduct>(exposureGroupByProduct);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ExposureGroupByProductItemView GetExposureGroupByProductInitialData(string exposureGroupGUID)
		{
			try
			{
				IExposureGroupRepo exposureGroupRepo = new ExposureGroupRepo(db);
				ExposureGroup exposureGroup = exposureGroupRepo.GetExposureGroupByIdNoTracking(exposureGroupGUID.StringToGuid());
				return new ExposureGroupByProductItemView
				{
					ExposureGroupByProductGUID = new Guid().GuidNullToString(),
					ExposureGroupGUID = exposureGroupGUID,
					ExposureGroup_Values = SmartAppUtil.GetDropDownLabel(exposureGroup.ExposureGroupId, exposureGroup.Description),
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
	}
}
