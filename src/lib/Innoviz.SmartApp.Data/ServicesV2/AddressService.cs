using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAddressService
	{

		AddressCountryItemView GetAddressCountryById(string id);
		AddressCountryItemView CreateAddressCountry(AddressCountryItemView addressCountryView);
		AddressCountryItemView UpdateAddressCountry(AddressCountryItemView addressCountryView);
		bool DeleteAddressCountry(string id);
		AddressDistrictItemView GetAddressDistrictById(string id);
		AddressDistrictItemView CreateAddressDistrict(AddressDistrictItemView addressDistrictView);
		AddressDistrictItemView UpdateAddressDistrict(AddressDistrictItemView addressDistrictView);
		bool DeleteAddressDistrict(string id);
		AddressPostalCodeItemView GetAddressPostalCodeById(string id);
		AddressPostalCodeItemView CreateAddressPostalCode(AddressPostalCodeItemView addressPostalCodeView);
		AddressPostalCodeItemView UpdateAddressPostalCode(AddressPostalCodeItemView addressPostalCodeView);
		bool DeleteAddressPostalCode(string id);
		AddressProvinceItemView GetAddressProvinceById(string id);
		AddressProvinceItemView CreateAddressProvince(AddressProvinceItemView addressProvinceView);
		AddressProvinceItemView UpdateAddressProvince(AddressProvinceItemView addressProvinceView);
		bool DeleteAddressProvince(string id);
		AddressSubDistrictItemView GetAddressSubDistrictById(string id);
		AddressSubDistrictItemView CreateAddressSubDistrict(AddressSubDistrictItemView addressSubDistrictView);
		AddressSubDistrictItemView UpdateAddressSubDistrict(AddressSubDistrictItemView addressSubDistrictView);
		bool DeleteAddressSubDistrict(string id);
	}
	public class AddressService : SmartAppService, IAddressService
	{
		public AddressService(SmartAppDbContext context) : base(context) { }
		public AddressService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AddressService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AddressService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AddressService() : base() { }

		public AddressCountryItemView GetAddressCountryById(string id)
		{
			try
			{
				IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
				return addressCountryRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressCountryItemView CreateAddressCountry(AddressCountryItemView addressCountryView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				addressCountryView = accessLevelService.AssignOwnerBU(addressCountryView);
				IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
				AddressCountry addressCountry = addressCountryView.ToAddressCountry();
				addressCountry = addressCountryRepo.CreateAddressCountry(addressCountry);
				base.LogTransactionCreate<AddressCountry>(addressCountry);
				UnitOfWork.Commit();
				return addressCountry.ToAddressCountryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressCountryItemView UpdateAddressCountry(AddressCountryItemView addressCountryView)
		{
			try
			{
				IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
				AddressCountry inputAddressCountry = addressCountryView.ToAddressCountry();
				AddressCountry dbAddressCountry = addressCountryRepo.Find(inputAddressCountry.AddressCountryGUID);
				dbAddressCountry = addressCountryRepo.UpdateAddressCountry(dbAddressCountry, inputAddressCountry);
				base.LogTransactionUpdate<AddressCountry>(GetOriginalValues<AddressCountry>(dbAddressCountry), dbAddressCountry);
				UnitOfWork.Commit();
				return dbAddressCountry.ToAddressCountryItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAddressCountry(string item)
		{
			try
			{
				IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
				Guid addressCountryGUID = new Guid(item);
				AddressCountry addressCountry = addressCountryRepo.Find(addressCountryGUID);
				addressCountryRepo.Remove(addressCountry);
				base.LogTransactionDelete<AddressCountry>(addressCountry);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AddressDistrictItemView GetAddressDistrictById(string id)
		{
			try
			{
				IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
				return addressDistrictRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressDistrictItemView CreateAddressDistrict(AddressDistrictItemView addressDistrictView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				addressDistrictView = accessLevelService.AssignOwnerBU(addressDistrictView);
				IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
				AddressDistrict addressDistrict = addressDistrictView.ToAddressDistrict();
				addressDistrict = addressDistrictRepo.CreateAddressDistrict(addressDistrict);
				base.LogTransactionCreate<AddressDistrict>(addressDistrict);
				UnitOfWork.Commit();
				return addressDistrict.ToAddressDistrictItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressDistrictItemView UpdateAddressDistrict(AddressDistrictItemView addressDistrictView)
		{
			try
			{
				IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
				AddressDistrict inputAddressDistrict = addressDistrictView.ToAddressDistrict();
				AddressDistrict dbAddressDistrict = addressDistrictRepo.Find(inputAddressDistrict.AddressDistrictGUID);
				dbAddressDistrict = addressDistrictRepo.UpdateAddressDistrict(dbAddressDistrict, inputAddressDistrict);
				base.LogTransactionUpdate<AddressDistrict>(GetOriginalValues<AddressDistrict>(dbAddressDistrict), dbAddressDistrict);
				UnitOfWork.Commit();
				return dbAddressDistrict.ToAddressDistrictItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAddressDistrict(string item)
		{
			try
			{
				IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
				Guid addressDistrictGUID = new Guid(item);
				AddressDistrict addressDistrict = addressDistrictRepo.Find(addressDistrictGUID);
				addressDistrictRepo.Remove(addressDistrict);
				base.LogTransactionDelete<AddressDistrict>(addressDistrict);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AddressPostalCodeItemView GetAddressPostalCodeById(string id)
		{
			try
			{
				IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
				return addressPostalCodeRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressPostalCodeItemView CreateAddressPostalCode(AddressPostalCodeItemView addressPostalCodeView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				addressPostalCodeView = accessLevelService.AssignOwnerBU(addressPostalCodeView);
				IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
				AddressPostalCode addressPostalCode = addressPostalCodeView.ToAddressPostalCode();
				addressPostalCode = addressPostalCodeRepo.CreateAddressPostalCode(addressPostalCode);
				base.LogTransactionCreate<AddressPostalCode>(addressPostalCode);
				UnitOfWork.Commit();
				return addressPostalCode.ToAddressPostalCodeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressPostalCodeItemView UpdateAddressPostalCode(AddressPostalCodeItemView addressPostalCodeView)
		{
			try
			{
				IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
				AddressPostalCode inputAddressPostalCode = addressPostalCodeView.ToAddressPostalCode();
				AddressPostalCode dbAddressPostalCode = addressPostalCodeRepo.Find(inputAddressPostalCode.AddressPostalCodeGUID);
				dbAddressPostalCode = addressPostalCodeRepo.UpdateAddressPostalCode(dbAddressPostalCode, inputAddressPostalCode);
				base.LogTransactionUpdate<AddressPostalCode>(GetOriginalValues<AddressPostalCode>(dbAddressPostalCode), dbAddressPostalCode);
				UnitOfWork.Commit();
				return dbAddressPostalCode.ToAddressPostalCodeItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAddressPostalCode(string item)
		{
			try
			{
				IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
				Guid addressPostalCodeGUID = new Guid(item);
				AddressPostalCode addressPostalCode = addressPostalCodeRepo.Find(addressPostalCodeGUID);
				addressPostalCodeRepo.Remove(addressPostalCode);
				base.LogTransactionDelete<AddressPostalCode>(addressPostalCode);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AddressProvinceItemView GetAddressProvinceById(string id)
		{
			try
			{
				IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
				return addressProvinceRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressProvinceItemView CreateAddressProvince(AddressProvinceItemView addressProvinceView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				addressProvinceView = accessLevelService.AssignOwnerBU(addressProvinceView);
				IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
				AddressProvince addressProvince = addressProvinceView.ToAddressProvince();
				addressProvince = addressProvinceRepo.CreateAddressProvince(addressProvince);
				base.LogTransactionCreate<AddressProvince>(addressProvince);
				UnitOfWork.Commit();
				return addressProvince.ToAddressProvinceItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressProvinceItemView UpdateAddressProvince(AddressProvinceItemView addressProvinceView)
		{
			try
			{
				IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
				AddressProvince inputAddressProvince = addressProvinceView.ToAddressProvince();
				AddressProvince dbAddressProvince = addressProvinceRepo.Find(inputAddressProvince.AddressProvinceGUID);
				dbAddressProvince = addressProvinceRepo.UpdateAddressProvince(dbAddressProvince, inputAddressProvince);
				base.LogTransactionUpdate<AddressProvince>(GetOriginalValues<AddressProvince>(dbAddressProvince), dbAddressProvince);
				UnitOfWork.Commit();
				return dbAddressProvince.ToAddressProvinceItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAddressProvince(string item)
		{
			try
			{
				IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
				Guid addressProvinceGUID = new Guid(item);
				AddressProvince addressProvince = addressProvinceRepo.Find(addressProvinceGUID);
				addressProvinceRepo.Remove(addressProvince);
				base.LogTransactionDelete<AddressProvince>(addressProvince);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public AddressSubDistrictItemView GetAddressSubDistrictById(string id)
		{
			try
			{
				IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
				return addressSubDistrictRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressSubDistrictItemView CreateAddressSubDistrict(AddressSubDistrictItemView addressSubDistrictView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				addressSubDistrictView = accessLevelService.AssignOwnerBU(addressSubDistrictView);
				IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
				AddressSubDistrict addressSubDistrict = addressSubDistrictView.ToAddressSubDistrict();
				addressSubDistrict = addressSubDistrictRepo.CreateAddressSubDistrict(addressSubDistrict);
				base.LogTransactionCreate<AddressSubDistrict>(addressSubDistrict);
				UnitOfWork.Commit();
				return addressSubDistrict.ToAddressSubDistrictItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressSubDistrictItemView UpdateAddressSubDistrict(AddressSubDistrictItemView addressSubDistrictView)
		{
			try
			{
				IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
				AddressSubDistrict inputAddressSubDistrict = addressSubDistrictView.ToAddressSubDistrict();
				AddressSubDistrict dbAddressSubDistrict = addressSubDistrictRepo.Find(inputAddressSubDistrict.AddressSubDistrictGUID);
				dbAddressSubDistrict = addressSubDistrictRepo.UpdateAddressSubDistrict(dbAddressSubDistrict, inputAddressSubDistrict);
				base.LogTransactionUpdate<AddressSubDistrict>(GetOriginalValues<AddressSubDistrict>(dbAddressSubDistrict), dbAddressSubDistrict);
				UnitOfWork.Commit();
				return dbAddressSubDistrict.ToAddressSubDistrictItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAddressSubDistrict(string item)
		{
			try
			{
				IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
				Guid addressSubDistrictGUID = new Guid(item);
				AddressSubDistrict addressSubDistrict = addressSubDistrictRepo.Find(addressSubDistrictGUID);
				addressSubDistrictRepo.Remove(addressSubDistrict);
				base.LogTransactionDelete<AddressSubDistrict>(addressSubDistrict);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
