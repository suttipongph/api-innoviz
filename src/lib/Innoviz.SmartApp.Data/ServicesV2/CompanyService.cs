﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.Repositories;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICompanyService
	{

		CompanyItemView GetCompanyById(string id);
		CompanyItemView CreateCompany(CompanyItemView companyView);
		CompanyItemView UpdateCompany(CompanyItemView companyView);
		bool DeleteCompany(string id);
		string GetCompanyGUIDByCompanyId(string companyId);
		CreateCompanyResultView CreateCompany(CreateCompanyParamView createCompanyView);
	}
	public class CompanyService : SmartAppService, ICompanyService
	{
		public CompanyService(SmartAppDbContext context) : base(context) { }
		public CompanyService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CompanyService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CompanyService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CompanyService() : base() { }

		public CompanyItemView GetCompanyById(string id)
		{
			try
			{
				ICompanyRepo companyRepo = new CompanyRepo(db);
				return companyRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyItemView CreateCompany(CompanyItemView companyView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				companyView = accessLevelService.AssignOwnerBU(companyView);
				ICompanyRepo companyRepo = new CompanyRepo(db);
				Company company = companyView.ToCompany();
				company = companyRepo.CreateCompany(company);
				base.LogTransactionCreate<Company>(company);
				UnitOfWork.Commit();
				return company.ToCompanyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanyItemView UpdateCompany(CompanyItemView companyView)
		{
			try
			{
				ICompanyRepo companyRepo = new CompanyRepo(db);
				Company inputCompany = companyView.ToCompany();
				Company dbCompany = companyRepo.Find(inputCompany.CompanyGUID);
				dbCompany = companyRepo.UpdateCompany(dbCompany, inputCompany);
				base.LogTransactionUpdate<Company>(GetOriginalValues<Company>(dbCompany), dbCompany);
				UnitOfWork.Commit();
				return dbCompany.ToCompanyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCompany(string item)
		{
			try
			{
				ICompanyRepo companyRepo = new CompanyRepo(db);
				Guid companyGUID = new Guid(item);
				Company company = companyRepo.Find(companyGUID);
				companyRepo.Remove(company);
				base.LogTransactionDelete<Company>(company);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public string GetCompanyGUIDByCompanyId(string companyId)
        {
            try
            {
				ICompanyRepo companyRepo = new CompanyRepo(db);
				var company = companyRepo.GetCompanyByCompanyIdNoTracking(companyId);
				return company != null ? company.CompanyGUID.GuidNullToString() : null;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#region LIT CreateCompany 
		public CreateCompanyResultView CreateCompany(CreateCompanyParamView createCompanyView)
		{
			try
			{
				CreateCompanyResultView result = new CreateCompanyResultView();
				CreateCompanyViewMap parm = InitCreateCompany(createCompanyView);

				using (var transaction = UnitOfWork.ContextTransaction())
                {
					this.BulkInsert(new List<Company> { parm.Company });
					this.BulkInsert(new List<BusinessUnit> { parm.BusinessUnit });
					this.BulkInsert(new List<EmployeeTable> { parm.EmployeeTable });
					this.BulkInsert(new List<Branch> { parm.Branch });

					this.BulkInsert(new List<Currency> { parm.Currency });
					this.BulkInsert(new List<ExchangeRate> { parm.ExchangeRate });
					this.BulkInsert(new List<CalendarGroup> { parm.CalendarGroup });
					this.BulkInsert(new List<CompanyParameter> { parm.CompanyParameter });

					this.BulkInsert(parm.SysRoleTable);
					this.BulkInsert(parm.SysScopeRole);
					this.BulkInsert(parm.SysControllerRole);
					this.BulkInsert(parm.SysFeatureGroupRole);
					this.BulkInsert(parm.SysUserRoles);
					this.BulkInsert(parm.SysUserCompanyMapping);

					this.BulkInsert(parm.NumberSeqParameter);
					this.BulkInsert(parm.CreditLimitType);
					this.BulkInsert(parm.RetentionConditionSetup);

					UnitOfWork.Commit(transaction);
				}
				NotificationResponse success = new NotificationResponse();
				success.AddData("Create company & initialize data successfully.", NotiType.SUCCESSING);
				result.Notification = success;
				return result;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		private bool ValidateCreateCompany(CreateCompanyParamView createCompanyView)
        {
            try
            {
				SmartAppException ex = new SmartAppException("Error create Company.");
				ISysUserTableRepo sysUserTableRepo = new SysUserTableRepo(db);
				ICompanyRepo companyRepo = new CompanyRepo(db);
				IBranchRepo branchRepo = new BranchRepo(db);

				var companyList = companyRepo.GetListCompanyNoTracking();
				var branchList = branchRepo.GetBranchByCompanyIdNoTracking(createCompanyView.CompanyId);
				
				if(!sysUserTableRepo.HasAnyUser())
                {
					ex.AddData("Cannot create company without any existing users.");
					throw ex;
                }
				if(companyList.Count != 0)
                {
					ex.AddData("Only one company is allowed in the system.");
					throw ex;
                }
				if(branchList.Any(a => a.BranchId == createCompanyView.BranchId))
                {
					ex.AddData("BranchId {{0}} already exist for CompanyId {{1}}", createCompanyView.BranchId, createCompanyView.CompanyId);
					throw ex;
                }
				return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private CreateCompanyViewMap InitCreateCompany(CreateCompanyParamView createCompanyView)
        {
            try
            {
				CreateCompanyViewMap param = new CreateCompanyViewMap();
				
				if(ValidateCreateCompany(createCompanyView))
                {
                    #region Company
                    param.Company = new Company
					{
						CompanyGUID = Guid.NewGuid(),
						CompanyId = createCompanyView.CompanyId,
						Name = createCompanyView.Company_Name,
						SecondName = createCompanyView.SecondName,
						CreatedDateTime = DateTime.Now,
						CreatedBy = createCompanyView.UserName,
						ModifiedBy = createCompanyView.UserName,
						ModifiedDateTime = DateTime.Now
					};
					#endregion
					// BusinessUnit, EmployeeTable, SysRoleTable, SysUserRoles, SysScopeRole, SysControllerRole, SysFeatureGroupRole
					param = InitSecurityCreateCompany(param, createCompanyView);

					// Branch, NumberSeqParameter, Currency, ExchangeRate, CalendarGroup,
					// ComopanyParameter, CreditLimitType, RetentionConditionSetup, AgingReportSetup
					param = InitSetupCreateCompany(param, createCompanyView);

					#region SysUserCompanyMapping
					param.SysUserCompanyMapping = new List<SysUserCompanyMapping>()
					{
						new SysUserCompanyMapping
						{
							CompanyGUID = param.Company.CompanyGUID,
							BranchGUID = param.Branch.BranchGUID,
							UserGUID = createCompanyView.UserId.StringToGuid(),
							CreatedDateTime = DateTime.Now,
							CreatedBy = createCompanyView.UserName,
							ModifiedBy = createCompanyView.UserName,
							ModifiedDateTime = DateTime.Now
						}
					};
					#endregion

					param.Company.DefaultBranchGUID = param.Branch.BranchGUID;
				}
				return param;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private CreateCompanyViewMap InitSecurityCreateCompany(CreateCompanyViewMap param, CreateCompanyParamView createCompanyView)
        {
            try
            {
				ISysAccessRightService sysAccessRightService = new SysAccessRightService(db);

				Guid companyGuid = param.Company.CompanyGUID;
				Guid rootBusinessUnitGuid = Guid.NewGuid();
				string username = createCompanyView.UserName;

				#region BusinessUnit
				param.BusinessUnit = new BusinessUnit
				{
					BusinessUnitGUID = rootBusinessUnitGuid,
					BusinessUnitId = param.Company.CompanyId,
					Description = param.Company.CompanyId,
					ParentBusinessUnitGUID = null,
					CompanyGUID = companyGuid,
					CreatedDateTime = DateTime.Now,
					CreatedBy = username,
					ModifiedBy = username,
					ModifiedDateTime = DateTime.Now,
					Owner = username,
					OwnerBusinessUnitGUID = rootBusinessUnitGuid
				};
				#endregion
				#region EmployeeTable
				param.EmployeeTable = new EmployeeTable
				{
					EmployeeTableGUID = Guid.NewGuid(),
					EmployeeId = username,
					Name = username,
					UserId = createCompanyView.UserId.StringToGuid(),
					InActive = false,
					BusinessUnitGUID = rootBusinessUnitGuid,
					CompanyGUID = companyGuid,
					CreatedDateTime = DateTime.Now,
					CreatedBy = username,
					ModifiedBy = username,
					ModifiedDateTime = DateTime.Now,
					Owner = username,
					OwnerBusinessUnitGUID = rootBusinessUnitGuid
				};
				#endregion
				
				// SysRoleTable, SysControllerRole, SysFeatureGroupRole, SysUserRoles, SysScopeRole
                param = sysAccessRightService.InitAdminRolesForNewCompany(param, createCompanyView);

				return param;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		private CreateCompanyViewMap InitSetupCreateCompany(CreateCompanyViewMap param, CreateCompanyParamView createCompanyView)
        {
            try
            {
				string username = createCompanyView.UserName;
				Guid companyGuid = param.Company.CompanyGUID;
				Guid ownerBusinessUnitGuid = param.BusinessUnit.BusinessUnitGUID;
				Guid branchGuid = Guid.NewGuid();
                #region Branch
                param.Branch = 
				new Branch
				{
					BranchGUID = branchGuid,
					BranchId = createCompanyView.BranchId,
					Name = createCompanyView.Branch_Name,
					TaxBranchGUID = branchGuid,
					CompanyId = param.Company.CompanyId,
					CompanyGUID = companyGuid,
					CreatedBy = username,
					CreatedDateTime = DateTime.Now,
					ModifiedBy = username,
					ModifiedDateTime = DateTime.Now,
					Owner = username,
					OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
				};
				#endregion
				#region Currency
				param.Currency =
				new Currency
				{
					CurrencyGUID = Guid.NewGuid(),
					CurrencyId = "THB",
					Name = "Thai baht",
					CompanyGUID = companyGuid,
					CreatedBy = username,
					CreatedDateTime = DateTime.Now,
					ModifiedBy = username,
					ModifiedDateTime = DateTime.Now,
					Owner = username,
					OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
				};
				#endregion
				#region ExchangeRate
				param.ExchangeRate =
				new ExchangeRate
				{
					ExchangeRateGUID = Guid.NewGuid(),
					CurrencyGUID = param.Currency.CurrencyGUID,
					HomeCurrencyGUID = param.Currency.CurrencyGUID,
					Rate = 1m,
					CompanyGUID = companyGuid,
					EffectiveFrom = "01/01/2021 00:00:00".StringToDateTime(),
					CreatedBy = username,
					CreatedDateTime = DateTime.Now,
					ModifiedBy = username,
					ModifiedDateTime = DateTime.Now,
					Owner = username,
					OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
				};
				#endregion
				#region CalendarGroup
				param.CalendarGroup =
				new CalendarGroup
				{
					CalendarGroupGUID = Guid.NewGuid(),
					CalendarGroupId = "LIT",
					Description = "LIT calendar",
					WorkingDay = 0,
					CompanyGUID = companyGuid,
					CreatedBy = username,
					CreatedDateTime = DateTime.Now,
					ModifiedBy = username,
					ModifiedDateTime = DateTime.Now,
					Owner = username,
					OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
				};
				#endregion
				#region CompanyParameter
				param.CompanyParameter =
				new CompanyParameter
				{
					CompanyGUID = companyGuid,
					CalcSheetIncTax = false,
					VehicleTaxServiceFee = 0m,
					CalcSheetIncAdditionalCost = false,
					CollectionFeeMethod = 1,
					CollectionFeeMinBalCal = 0m,
					ReceiptGenBy = 0,
					InvoiceIssuingDay = 1,
					InvoiceIssuing = 0,
					PenaltyBase = 1,
					PaymStructureRefNum = 0,
					EarlyToleranceOverAmount = 0m,
					EarlyToleranceUnderAmount = 0m,
					NPLMethod = 0,
					BOTRealization = false,
					ProvisionMethod = 0,
					ProvisionRateBy = 1,
					HomeCurrencyGUID = param.Currency.CurrencyGUID,
					CompanyParameterGUID = Guid.NewGuid(),
					CollectionOverdueDay = 0,
					MaximumPennyDifference = 0m,
					OverdueType = 0,
					PenaltyCalculationMethod = 0,
					SettleByPeriod = false,
					SettlementGraceDay = 0,
					AgreementType = false,
					LeaseSubType = false,
					LeaseType = false,
					Product = false,
					ProductGroup = false,
					RecordType = false,
					BuyerReviewedDay = 365,
					ChequeToleranceAmount = 0.1m,
					CompanyCalendarGroupGUID = param.CalendarGroup.CalendarGroupGUID,
					DaysPerYear = 365,
					MaxCopyFinancialStatementNumOfYear = 3,
					MaxCopyTaxReportNumOfMonth = 6,
					MaxInterestPct = 15m,
					MinUpcountryChequeFeeAmount = 10m,
					UpcountryChequeFeePct = 0m,
					FTMaxRollbillInterestDay = 0,
					FTMinInterestDay = 15,
					FTRollbillInterestDay = 0,
					PFInterestCutDay = 21,
					PFTermExtensionDaysPerFeeAmount = 30m,
					PFMaxPostponeCollectionDay = 0,
					CreatedBy = username,
					CreatedDateTime = DateTime.Now,
					ModifiedBy = username,
					ModifiedDateTime = DateTime.Now,
					Owner = username,
					OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
				};
				#endregion
				#region NumberSeqParameter
				param.NumberSeqParameter =
				new List<NumberSeqParameter>()
				{
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.AssignmentAgreement,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.BookmarkDocumentTemplateTable,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.BusinessCollateralAgreement,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.Buyer,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.BuyerAgreement,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.BuyerReceipt,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.Consortium,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.CustomerBusinessCollateral,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.DocumentConditionTemplateTable,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.DocumentReturn,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.Employee,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.InternalAssignmentAgreement,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.InternalBusinessCollateralAgreement,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.MessengerJob,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.Purchase,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.ReceiptTemp,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.RelatedPerson,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.ReserveRefund,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.RetentionRefund,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.SuspenseRefund,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.Vendor,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.Verification,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.Withdrawal,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.IntercompanyInvoice,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.IntercompanyInvoiceSettlement,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new NumberSeqParameter
					{
						NumberSeqParameterGUID = Guid.NewGuid(),
						ReferenceId = ReferenceId.FreeTextInvoice,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
				};
                #endregion
                #region CreditLimitType
                Guid revolvingByCustomerCreditLimitTypeGuid = Guid.NewGuid();
				param.CreditLimitType =
				new List<CreditLimitType>()
				{
					new CreditLimitType
					{
						CreditLimitTypeGUID = revolvingByCustomerCreditLimitTypeGuid,
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = true,
						CloseDay = 365,
						CreditLimitConditionType = 0,
						CreditLimitExpiration = 0,
						CreditLimitExpiryDay = 90,
						CreditLimitRemark = "ตามเงื่อนไขบริษัท",
						CreditLimitTypeId = "FT001",
						Description = "Revolving by customer",
						InactiveDay = 180,
						ProductType = 1,
						Revolving = true,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = true,
						BookmarkOrdering = 1,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 2,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "มีอายุตาม buyer agreement",
						CreditLimitTypeId = "FT002",
						Description = "Non-revolving by buyer agreement",
						InactiveDay = 0,
						ProductType = 1,
						Revolving = false,
						ValidateBuyerAgreement = true,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 2,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = false,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 0,
						CreditLimitExpiration = 3,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "User กำหนดเอง",
						CreditLimitTypeId = "FT003",
						Description = "Revolving by customer with period",
						InactiveDay = 0,
						ParentCreditLimitTypeGUID = revolvingByCustomerCreditLimitTypeGuid,
						ProductType = 1,
						Revolving = true,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 3,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 2,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "มีอายุตาม buyer agreement",
						CreditLimitTypeId = "PF001",
						Description = "Non revolving by CA",
						InactiveDay = 0,
						ProductType = 2,
						Revolving = false,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 4,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = true,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 2,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "มีอายุตาม buyer agreement",
						CreditLimitTypeId = "PF002",
						Description = "Non revolving by buyer agreement",
						InactiveDay = 0,
						ProductType = 2,
						Revolving = false,
						ValidateBuyerAgreement = true,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 5,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = false,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 0,
						CreditLimitExpiration = 1,
						CreditLimitExpiryDay = 365,
						CreditLimitRemark = "365 วัน",
						CreditLimitTypeId = "PF003",
						Description = "Revolving by customer",
						InactiveDay = 0,
						ProductType = 2,
						Revolving = true,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = true,
						BookmarkOrdering = 6,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 2,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "มีอายุตาม buyer agreement",
						CreditLimitTypeId = "PF004",
						Description = "Revolving by buyer agreement",
						InactiveDay = 0,
						ProductType = 2,
						Revolving = true,
						ValidateBuyerAgreement = true,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 7,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 4,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "ไม่มีอายุ",
						CreditLimitTypeId = "LS001",
						Description = "Leasing",
						InactiveDay = 0,
						ProductType = 3,
						Revolving = false,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 8,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 4,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "ไม่มีอายุ",
						CreditLimitTypeId = "HP001",
						Description = "Hire purchase",
						InactiveDay = 0,
						ProductType = 4,
						Revolving = false,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 9,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 3,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "User กำหนดเอง",
						CreditLimitTypeId = "BB001",
						Description = "Bond",
						InactiveDay = 0,
						ProductType = 5,
						Revolving = true,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 10,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new CreditLimitType
					{
						CreditLimitTypeGUID = Guid.NewGuid(),
						AllowAddLine = true,
						BuyerMatchingAndLoanRequest = false,
						CloseDay = 0,
						CreditLimitConditionType = 1,
						CreditLimitExpiration = 4,
						CreditLimitExpiryDay = 0,
						CreditLimitRemark = "ไม่มีอายุ",
						CreditLimitTypeId = "LC001",
						Description = "LC/DLC",
						InactiveDay = 0,
						ProductType = 6,
						Revolving = false,
						ValidateBuyerAgreement = false,
						ValidateCreditLimitType = false,
						BookmarkOrdering = 11,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
				};
				#endregion
				#region RetentionConditionSetup
				param.RetentionConditionSetup =
				new List<RetentionConditionSetup>()
				{
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 1,
						RetentionCalculateBase = 1,
						RetentionDeductionMethod = 0,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 1,
						RetentionCalculateBase = 2,
						RetentionDeductionMethod = 0,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 1,
						RetentionCalculateBase = 3,
						RetentionDeductionMethod = 0,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 1,
						RetentionCalculateBase = 3,
						RetentionDeductionMethod = 2,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 1,
						RetentionCalculateBase = 4,
						RetentionDeductionMethod = 2,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 1,
						RetentionCalculateBase = 0,
						RetentionDeductionMethod = 3,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 2,
						RetentionCalculateBase = 5,
						RetentionDeductionMethod = 1,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new RetentionConditionSetup
					{
						RetentionConditionSetupGUID = Guid.NewGuid(),
						ProductType = 2,
						RetentionCalculateBase = 3,
						RetentionDeductionMethod = 2,
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
				};
				#endregion
				#region AgingReportSetup
				new List<AgingReportSetup>()
				{
					new AgingReportSetup
					{
						AgingReportSetupGUID = Guid.NewGuid(),
						LineNum = 1,
						MaximumDays = 0,
						Description = "<= 0 Days",
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new AgingReportSetup
					{
						AgingReportSetupGUID = Guid.NewGuid(),
						LineNum = 2,
						MaximumDays = 30,
						Description = "1-30 Days",
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new AgingReportSetup
					{
						AgingReportSetupGUID = Guid.NewGuid(),
						LineNum = 3,
						MaximumDays = 90,
						Description = "31-90 Days",
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new AgingReportSetup
					{
						AgingReportSetupGUID = Guid.NewGuid(),
						LineNum = 4,
						MaximumDays = 180,
						Description = "91-180 Days",
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new AgingReportSetup
					{
						AgingReportSetupGUID = Guid.NewGuid(),
						LineNum = 5,
						MaximumDays = 365,
						Description = "181-365 Days",
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					},
					new AgingReportSetup
					{
						AgingReportSetupGUID = Guid.NewGuid(),
						LineNum = 6,
						MaximumDays = 99999999,
						Description = ">365 Days",
						CompanyGUID = companyGuid,
						CreatedBy = username,
						CreatedDateTime = DateTime.Now,
						ModifiedBy = username,
						ModifiedDateTime = DateTime.Now,
						Owner = username,
						OwnerBusinessUnitGUID = ownerBusinessUnitGuid,
					}
				};
				#endregion
				return param;
			}
			catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
		#endregion
	}
}
