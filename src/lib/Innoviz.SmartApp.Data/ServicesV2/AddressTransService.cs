using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IAddressTransService
	{

		AddressTransItemView GetAddressTransById(string id);
		AddressTransItemView CreateAddressTrans(AddressTransItemView addressTransView);
		AddressTransItemView UpdateAddressTrans(AddressTransItemView addressTransView);
		bool DeleteAddressTrans(string id);
		AddressTransItemView GetAddressTransInitialData(string refId, string refGUID, RefType refType);
		IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItemAddressProvince(SearchParameter search);
		IEnumerable<SelectItem<AddressDistrictItemView>> GetDropDownItemAddressDistrict(SearchParameter search);
		IEnumerable<SelectItem<AddressSubDistrictItemView>> GetDropDownItemAddressSubDistrict(SearchParameter search);
		IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTransByCustomer(SearchParameter search);
		IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTransByBuyer(SearchParameter search);
		IEnumerable<SelectItem<AddressPostalCodeItemView>> GetDropDownItemAddressPostalCode(SearchParameter search);
		void CreateAddressTransCollection(List<AddressTransItemView> addressTransItemViews);
	}
		
	public class AddressTransService : SmartAppService, IAddressTransService
	{
		public AddressTransService(SmartAppDbContext context) : base(context) { }
		public AddressTransService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public AddressTransService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public AddressTransService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public AddressTransService() : base() { }

		public AddressTransItemView GetAddressTransById(string id)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				return addressTransRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressTransItemView CreateAddressTrans(AddressTransItemView addressTransView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				addressTransView = accessLevelService.AssignOwnerBU(addressTransView);
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				AddressTrans addressTrans = addressTransView.ToAddressTrans();
				SetAddress2(addressTrans);
				addressTrans = addressTransRepo.CreateAddressTrans(addressTrans);
				base.LogTransactionCreate<AddressTrans>(addressTrans);
				UnitOfWork.Commit();
				return addressTrans.ToAddressTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressTransItemView UpdateAddressTrans(AddressTransItemView addressTransView)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				AddressTrans inputAddressTrans = addressTransView.ToAddressTrans();
				AddressTrans dbAddressTrans = addressTransRepo.Find(inputAddressTrans.AddressTransGUID);
				SetAddress2(inputAddressTrans);
				dbAddressTrans = addressTransRepo.UpdateAddressTrans(dbAddressTrans, inputAddressTrans);
				base.LogTransactionUpdate<AddressTrans>(GetOriginalValues<AddressTrans>(dbAddressTrans), dbAddressTrans);
				UnitOfWork.Commit();
				return dbAddressTrans.ToAddressTransItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteAddressTrans(string item)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				Guid addressTransGUID = new Guid(item);
				AddressTrans addressTrans = addressTransRepo.Find(addressTransGUID);
				addressTransRepo.Remove(addressTrans);
				base.LogTransactionDelete<AddressTrans>(addressTrans);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public AddressTransItemView GetAddressTransInitialData(string refId, string refGUID, RefType refType)
		{
			try
			{
				return new AddressTransItemView
				{
					AddressTransGUID = new Guid().GuidNullToString(),
					RefGUID = refGUID,
					RefType = (int)refType,
					RefId = refId,
					Primary = false,
					CurrentAddress = false,
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AddressProvinceItemView>> GetDropDownItemAddressProvince(SearchParameter search)
		{
			try
			{
				IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
				search = search.GetParentCondition(AddressCountryCondition.AddressCountryGUID);
				return addressProvinceRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AddressDistrictItemView>> GetDropDownItemAddressDistrict(SearchParameter search)
		{
			try
			{
				IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
				search = search.GetParentCondition(AddressProvinceCondition.AddressProvinceGUID);
				return addressDistrictRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AddressSubDistrictItemView>> GetDropDownItemAddressSubDistrict(SearchParameter search)
		{
			try
			{ 
				IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
				search = search.GetParentCondition(AddressDistrictCondition.AddressDistrictGUID);
				return addressSubDistrictRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		public void SetAddress2(AddressTrans addressTrans)
		{
			try
			{
				IAddressCountryRepo addressCountryRepo = new AddressCountryRepo(db);
				IAddressSubDistrictRepo addressSubDistrictRepo = new AddressSubDistrictRepo(db);
				IAddressDistrictRepo addressDistrictRepo = new AddressDistrictRepo(db);
				IAddressProvinceRepo addressProvinceRepo = new AddressProvinceRepo(db);
				IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);

				string subDistrictName = addressSubDistrictRepo.GetAddressSubDistrictByIdNoTracking(addressTrans.AddressSubDistrictGUID).Name;
				string districtName = addressDistrictRepo.GetAddressDistrictByIdNoTracking(addressTrans.AddressDistrictGUID).Name;
				string provinceName = addressProvinceRepo.GetAddressProvinceByIdNoTracking(addressTrans.AddressProvinceGUID).Name;
				string countryName = addressCountryRepo.GetAddressCountryByIdNoTracking(addressTrans.AddressCountryGUID).Name;
				string postalCode = addressPostalCodeRepo.GetAddressPostalCodeByIdNoTracking(addressTrans.AddressPostalCodeGUID).PostalCode;

				addressTrans.Address2 = subDistrictName + " " + districtName + " " + provinceName + " " + countryName + " " + postalCode;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#region DropdownByCondition
		public IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTransByCustomer(SearchParameter search)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				search = search.GetParentCondition(AddressTransCondition.RefGUID);
				SearchCondition searchCondition = SearchConditionService.GetAddressTransRefTypeCondition((int)RefType.Customer);
				search.Conditions.Add(searchCondition);
				return addressTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<AddressTransItemView>> GetDropDownItemAddressTransByBuyer(SearchParameter search)
		{
			try
			{
				IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
				search = search.GetParentCondition(AddressTransCondition.RefGUID);
				SearchCondition searchCondition = SearchConditionService.GetAddressTransRefTypeCondition((int)RefType.Buyer);
				search.Conditions.Add(searchCondition);
				return addressTransRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropdownByCondition
		#region migration
		public void CreateAddressTransCollection(List<AddressTransItemView> addressTransItemViews)
		{
			try
			{
				if (addressTransItemViews.Any())
				{
					IAddressTransRepo addressTransRepo = new AddressTransRepo(db);
					List<AddressTrans> addressTrans = addressTransItemViews.ToAddressTrans().ToList();
					addressTransRepo.ValidateAdd(addressTrans);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(addressTrans, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}

        public IEnumerable<SelectItem<AddressPostalCodeItemView>> GetDropDownItemAddressPostalCode(SearchParameter search)
        {
			try
			{
				IAddressPostalCodeRepo addressPostalCodeRepo = new AddressPostalCodeRepo(db);
				search = search.GetParentCondition(AddressPostalCodeCondition.AddressSubDistrictGUID);
				return addressPostalCodeRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        #endregion
    }
}
