using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IConsortiumTableService
	{

		ConsortiumLineItemView GetConsortiumLineById(string id);
		ConsortiumLineItemView CreateConsortiumLine(ConsortiumLineItemView consortiumLineView);
		ConsortiumLineItemView UpdateConsortiumLine(ConsortiumLineItemView consortiumLineView);
		bool DeleteConsortiumLine(string id);
		ConsortiumTableItemView GetConsortiumTableById(string id);
		ConsortiumTableItemView CreateConsortiumTable(ConsortiumTableItemView consortiumTableView);
		ConsortiumTable CreateConsortiumTable(ConsortiumTable consortiumTable);
		ConsortiumTableItemView UpdateConsortiumTable(ConsortiumTableItemView consortiumTableView);
		bool DeleteConsortiumTable(string id);
		bool IsManualByConsortium(string companyId);
		ConsortiumLineItemView GetConsortiumLineInitialData(string consortiumTableGUID);
		IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemConsortiumTableStatus(SearchParameter search);
		IEnumerable<ConsortiumTrans> CopyConsortiumLine(Guid consortiumTableGUID, Guid toRefGUID, int toRefType);
		IEnumerable<SelectItem<ConsortiumLineItemView>> GetDropDownItemConsortiumLineByConsortiumTable(SearchParameter search);
		void CreateConsortiumTableCollection(List<ConsortiumTableItemView> consortiumTableItemViews);
		void CreateConsortiumLineCollection(List<ConsortiumLineItemView> consortiumLineItemViews);
	}
	public class ConsortiumTableService : SmartAppService, IConsortiumTableService
	{
		public ConsortiumTableService(SmartAppDbContext context) : base(context) { }
		public ConsortiumTableService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public ConsortiumTableService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public ConsortiumTableService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public ConsortiumTableService() : base() { }

		public ConsortiumLineItemView GetConsortiumLineById(string id)
		{
			try
			{
				IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
				return consortiumLineRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumLineItemView CreateConsortiumLine(ConsortiumLineItemView consortiumLineView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				consortiumLineView = accessLevelService.AssignOwnerBU(consortiumLineView);
				IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
				ConsortiumLine consortiumLine = consortiumLineView.ToConsortiumLine();
				consortiumLine = consortiumLineRepo.CreateConsortiumLine(consortiumLine);
				base.LogTransactionCreate<ConsortiumLine>(consortiumLine);
				UnitOfWork.Commit();
				return consortiumLine.ToConsortiumLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumLineItemView UpdateConsortiumLine(ConsortiumLineItemView consortiumLineView)
		{
			try
			{
				IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
				ConsortiumLine inputConsortiumLine = consortiumLineView.ToConsortiumLine();
				ConsortiumLine dbConsortiumLine = consortiumLineRepo.Find(inputConsortiumLine.ConsortiumLineGUID);
				dbConsortiumLine = consortiumLineRepo.UpdateConsortiumLine(dbConsortiumLine, inputConsortiumLine);
				base.LogTransactionUpdate<ConsortiumLine>(GetOriginalValues<ConsortiumLine>(dbConsortiumLine), dbConsortiumLine);
				UnitOfWork.Commit();
				return dbConsortiumLine.ToConsortiumLineItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteConsortiumLine(string item)
		{
			try
			{
				IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
				Guid consortiumLineGUID = new Guid(item);
				ConsortiumLine consortiumLine = consortiumLineRepo.Find(consortiumLineGUID);
				consortiumLineRepo.Remove(consortiumLine);
				base.LogTransactionDelete<ConsortiumLine>(consortiumLine);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTableItemView GetConsortiumTableById(string id)
		{
			try
			{
				IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
				return consortiumTableRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTableItemView CreateConsortiumTable(ConsortiumTableItemView consortiumTableView)
		{
			try
			{
				ConsortiumTable consortiumTable = consortiumTableView.ToConsortiumTable();
				consortiumTable = CreateConsortiumTable(consortiumTable);
				UnitOfWork.Commit();
				return consortiumTable.ToConsortiumTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTable CreateConsortiumTable(ConsortiumTable consortiumTable)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				consortiumTable = accessLevelService.AssignOwnerBU(consortiumTable);
				IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
				GenNumberSeqCode(consortiumTable);
				consortiumTable = consortiumTableRepo.CreateConsortiumTable(consortiumTable);
				base.LogTransactionCreate<ConsortiumTable>(consortiumTable);
				return consortiumTable;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public ConsortiumTableItemView UpdateConsortiumTable(ConsortiumTableItemView consortiumTableView)
		{
			try
			{
				IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
				ConsortiumTable inputConsortiumTable = consortiumTableView.ToConsortiumTable();
				ConsortiumTable dbConsortiumTable = consortiumTableRepo.Find(inputConsortiumTable.ConsortiumTableGUID);
				dbConsortiumTable = consortiumTableRepo.UpdateConsortiumTable(dbConsortiumTable, inputConsortiumTable);
				base.LogTransactionUpdate<ConsortiumTable>(GetOriginalValues<ConsortiumTable>(dbConsortiumTable), dbConsortiumTable);
				UnitOfWork.Commit();
				return dbConsortiumTable.ToConsortiumTableItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteConsortiumTable(string item)
		{
			try
			{
				IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
				Guid consortiumTableGUID = new Guid(item);
				ConsortiumTable consortiumTable = consortiumTableRepo.Find(consortiumTableGUID);
				consortiumTableRepo.Remove(consortiumTable);
				base.LogTransactionDelete<ConsortiumTable>(consortiumTable);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool IsManualByConsortium(string companyId)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				return numberSequenceService.IsManualByReferenceId(new Guid(companyId), ReferenceId.Consortium);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public void GenNumberSeqCode(ConsortiumTable consortiumTable)
		{
			try
			{
				INumberSequenceService numberSequenceService = new NumberSequenceService(db);
				INumberSeqParameterRepo numberSeqParameterRepo = new NumberSeqParameterRepo(db);
				NumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
				bool isManual = numberSequenceService.IsManualByReferenceId(consortiumTable.CompanyGUID, ReferenceId.Consortium);
				if (!isManual)
				{
					NumberSeqParameter numberSeqParameter = numberSeqParameterRepo.GetByReferenceId(consortiumTable.CompanyGUID, ReferenceId.Consortium);
					consortiumTable.ConsortiumId = numberSequenceService.GetNumber(consortiumTable.CompanyGUID, null, numberSeqParameter.NumberSeqTableGUID.GetValueOrDefault());
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public ConsortiumLineItemView GetConsortiumLineInitialData(string consortiumTableGUID)
		{
			try
			{
				IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
				ConsortiumTable consortiumTable = consortiumTableRepo.GetConsortiumTableByIdNoTracking(consortiumTableGUID.StringToGuid());
				return new ConsortiumLineItemView
				{
					ConsortiumLineGUID = new Guid().GuidNullToString(),
					ConsortiumTableGUID = consortiumTableGUID,
					ConsortiumTable_Values = SmartAppUtil.GetDropDownLabel(consortiumTable.ConsortiumId, consortiumTable.Description),
				};
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<DocumentStatusItemView>> GetDropDownItemConsortiumTableStatus(SearchParameter search)
		{
			try
			{
				IDocumentService documentService = new DocumentService(db);
				IDocumentStatusRepo documentStatusRepo = new DocumentStatusRepo(db);
				SearchCondition searchCondition = documentService.GetSearchConditionByPrecessId(DocumentProcessId.Consortium);
				search.Conditions.Add(searchCondition);
				return documentStatusRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<SelectItem<ConsortiumLineItemView>> GetDropDownItemConsortiumLineByConsortiumTable(SearchParameter search)
		{
			try
			{
				IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
				search = search.GetParentCondition(ConsortiumLineCondition.ConsortiumTableGUID);
				return consortiumLineRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IEnumerable<ConsortiumTrans> CopyConsortiumLine(Guid consortiumTableGUID,Guid toRefGUID,int toRefType)
		{
			try
			{
				IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
				IEnumerable<ConsortiumLine> originConsortiumLineList = consortiumLineRepo.GetConsortiumLineByConsortiumTableNoTracking(consortiumTableGUID);
				List<ConsortiumTrans> copyConsortiumTransList = new List<ConsortiumTrans>();
				if (originConsortiumLineList.Count() != 0)
				{
					foreach(ConsortiumLine item in originConsortiumLineList)
                    {
						copyConsortiumTransList.Add(new ConsortiumTrans
						{
							RefType = toRefType,
							RefGUID = toRefGUID,
							Ordering = item.Ordering,
							CustomerName = item.CustomerName,
							AuthorizedPersonTypeGUID = item.AuthorizedPersonTypeGUID,
							OperatedBy = item.OperatedBy,
							Address = item.Address,
							Remark = item.Remark
						});
					}

				}
				return copyConsortiumTransList;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

		#region migration
		public void CreateConsortiumTableCollection(List<ConsortiumTableItemView> consortiumTableItemViews)
		{
			try
			{
				if (consortiumTableItemViews.Any())
				{
					IConsortiumTableRepo consortiumTableRepo = new ConsortiumTableRepo(db);
					List<ConsortiumTable> consortiumTable = consortiumTableItemViews.ToConsortiumTable().ToList();
					consortiumTableRepo.ValidateAdd(consortiumTable);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(consortiumTable, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		public void CreateConsortiumLineCollection(List<ConsortiumLineItemView> consortiumLineItemViews)
		{
			try
			{
				if (consortiumLineItemViews.Any())
				{
					IConsortiumLineRepo consortiumLineRepo = new ConsortiumLineRepo(db);
					List<ConsortiumLine> consortiumLine = consortiumLineItemViews.ToConsortiumLine().ToList();
					consortiumLineRepo.ValidateAdd(consortiumLine);
					using (var transaction = UnitOfWork.ContextTransaction())
					{
						BulkInsert(consortiumLine, true);
						UnitOfWork.Commit(transaction);
					}
				}
			}
			catch (Exception e)
			{
				throw SmartAppUtil.ThrowDBException(e);
			}
		}
		#endregion
	}
}
