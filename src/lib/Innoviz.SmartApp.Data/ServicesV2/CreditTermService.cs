using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICreditTermService
	{

		CreditTermItemView GetCreditTermById(string id);
		CreditTermItemView CreateCreditTerm(CreditTermItemView creditTermView);
		CreditTermItemView UpdateCreditTerm(CreditTermItemView creditTermView);
		bool DeleteCreditTerm(string id);
	}
	public class CreditTermService : SmartAppService, ICreditTermService
	{
		public CreditTermService(SmartAppDbContext context) : base(context) { }
		public CreditTermService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CreditTermService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CreditTermService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CreditTermService() : base() { }

		public CreditTermItemView GetCreditTermById(string id)
		{
			try
			{
				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				return creditTermRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditTermItemView CreateCreditTerm(CreditTermItemView creditTermView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				creditTermView = accessLevelService.AssignOwnerBU(creditTermView);
				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				CreditTerm creditTerm = creditTermView.ToCreditTerm();
				creditTerm = creditTermRepo.CreateCreditTerm(creditTerm);
				base.LogTransactionCreate<CreditTerm>(creditTerm);
				UnitOfWork.Commit();
				return creditTerm.ToCreditTermItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CreditTermItemView UpdateCreditTerm(CreditTermItemView creditTermView)
		{
			try
			{
				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				CreditTerm inputCreditTerm = creditTermView.ToCreditTerm();
				CreditTerm dbCreditTerm = creditTermRepo.Find(inputCreditTerm.CreditTermGUID);
				dbCreditTerm = creditTermRepo.UpdateCreditTerm(dbCreditTerm, inputCreditTerm);
				base.LogTransactionUpdate<CreditTerm>(GetOriginalValues<CreditTerm>(dbCreditTerm), dbCreditTerm);
				UnitOfWork.Commit();
				return dbCreditTerm.ToCreditTermItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCreditTerm(string item)
		{
			try
			{
				ICreditTermRepo creditTermRepo = new CreditTermRepo(db);
				Guid creditTermGUID = new Guid(item);
				CreditTerm creditTerm = creditTermRepo.Find(creditTermGUID);
				creditTermRepo.Remove(creditTerm);
				base.LogTransactionDelete<CreditTerm>(creditTerm);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
