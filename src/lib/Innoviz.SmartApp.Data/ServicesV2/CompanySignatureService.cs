using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface ICompanySignatureService
	{

		CompanySignatureItemView GetCompanySignatureById(string id);
		CompanySignatureItemView CreateCompanySignature(CompanySignatureItemView companySignatureView);
		CompanySignatureItemView UpdateCompanySignature(CompanySignatureItemView companySignatureView);
		bool DeleteCompanySignature(string id);
        int Getmaxorderingbyreftypeandbranch(int reftype, string branch);
		CompanySignatureItemView GetInitialDataBranch();
		IEnumerable<SelectItem<CompanySignatureItemView>> GetDropDownItemCompanySignatureByBranch(SearchParameter search);
	}
    public class CompanySignatureService : SmartAppService, ICompanySignatureService
	{
		public CompanySignatureService(SmartAppDbContext context) : base(context) { }
		public CompanySignatureService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public CompanySignatureService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public CompanySignatureService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public CompanySignatureService() : base() { }

		public CompanySignatureItemView GetCompanySignatureById(string id)
		{
			try
			{
				ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
				return companySignatureRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanySignatureItemView CreateCompanySignature(CompanySignatureItemView companySignatureView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				companySignatureView = accessLevelService.AssignOwnerBU(companySignatureView);
				ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
				CompanySignature companySignature = companySignatureView.ToCompanySignature();
				companySignature = companySignatureRepo.CreateCompanySignature(companySignature);
				base.LogTransactionCreate<CompanySignature>(companySignature);
				UnitOfWork.Commit();
				return companySignature.ToCompanySignatureItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public CompanySignatureItemView UpdateCompanySignature(CompanySignatureItemView companySignatureView)
		{
			try
			{
				ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
				CompanySignature inputCompanySignature = companySignatureView.ToCompanySignature();
				CompanySignature dbCompanySignature = companySignatureRepo.Find(inputCompanySignature.CompanySignatureGUID);
				dbCompanySignature = companySignatureRepo.UpdateCompanySignature(dbCompanySignature, inputCompanySignature);
				base.LogTransactionUpdate<CompanySignature>(GetOriginalValues<CompanySignature>(dbCompanySignature), dbCompanySignature);
				UnitOfWork.Commit();
				return dbCompanySignature.ToCompanySignatureItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteCompanySignature(string item)
		{
			try
			{
				ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
				Guid companySignatureGUID = new Guid(item);
				CompanySignature companySignature = companySignatureRepo.Find(companySignatureGUID);
				companySignatureRepo.Remove(companySignature);
				base.LogTransactionDelete<CompanySignature>(companySignature);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
        public int Getmaxorderingbyreftypeandbranch(int reftype, string branch)
        {
            try
            {
                ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
				IQueryable<CompanySignature> companySignatures = companySignatureRepo.GetByRefTypeAndBranch(reftype, branch);
				return (companySignatures.Any()) ? companySignatures.Max(m => m.Ordering) : 0;
				//return 1;

			}
            catch (Exception e)
            {
				throw SmartAppUtil.ThrowDBException(e, "ERROR.ERROR");
			}
        }
        public CompanySignatureItemView GetInitialDataBranch()
        {
            try
            {
                ICompanyRepo companyRepo = new CompanyRepo(db);
                Guid companyGuid = GetCurrentCompany().StringToGuid();
                Company company = companyRepo.GetCompanyByIdNoTracking(companyGuid);
				IBranchRepo branchRepo = new BranchRepo(db);
				Branch branch = branchRepo.GetBranchByIdNoTracking(company.DefaultBranchGUID.Value);
                return new CompanySignatureItemView
                {
                    BranchGUID = company.DefaultBranchGUID.GuidNullToString(),
					Branch_Values = SmartAppUtil.GetDropDownLabel(branch.BranchId,branch.Name),
					DefaultBranchGUID = company.DefaultBranchGUID.GuidNullToString()
				};
            }
            catch (Exception e)
            {
                throw SmartAppUtil.ThrowDBException(e);
            }
        }

		#region DropdownByCondition
		public IEnumerable<SelectItem<CompanySignatureItemView>> GetDropDownItemCompanySignatureByBranch(SearchParameter search)
		{
			try
			{
				ICompanySignatureRepo companySignatureRepo = new CompanySignatureRepo(db);
				search = search.GetParentCondition(CompanySignatureCondition.BranchGUID);
				return companySignatureRepo.GetDropDownItem(search);
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		#endregion DropdownByCondition

	}
}
