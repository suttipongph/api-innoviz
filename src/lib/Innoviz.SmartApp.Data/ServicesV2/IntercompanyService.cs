using Innoviz.SmartApp.Core.Repositories;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.Services;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ServicesV2
{
	public interface IIntercompanyService
	{

		IntercompanyItemView GetIntercompanyById(string id);
		IntercompanyItemView CreateIntercompany(IntercompanyItemView intercompanyView);
		IntercompanyItemView UpdateIntercompany(IntercompanyItemView intercompanyView);
		bool DeleteIntercompany(string id);
	}
	public class IntercompanyService : SmartAppService, IIntercompanyService
	{
		public IntercompanyService(SmartAppDbContext context) : base(context) { }
		public IntercompanyService(SmartAppDbContext context, ISysTransactionLogService transactionLogService) : base(context, transactionLogService) { }
		public IntercompanyService(SmartAppDbContext context, SystemParameter systemParameter) : base(context, systemParameter) { }
		public IntercompanyService(SmartAppDbContext context, SystemParameter systemParameter, ISysTransactionLogService transactionLogService) : base(context, systemParameter, transactionLogService) { }
		public IntercompanyService() : base() { }

		public IntercompanyItemView GetIntercompanyById(string id)
		{
			try
			{
				IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
				return intercompanyRepo.GetByIdvw(id.StringToGuid());
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyItemView CreateIntercompany(IntercompanyItemView intercompanyView)
		{
			try
			{
				ISysAccessLevelService accessLevelService = new SysAccessLevelService(db);
				intercompanyView = accessLevelService.AssignOwnerBU(intercompanyView);
				IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
				Intercompany intercompany = intercompanyView.ToIntercompany();
				intercompany = intercompanyRepo.CreateIntercompany(intercompany);
				base.LogTransactionCreate<Intercompany>(intercompany);
				UnitOfWork.Commit();
				return intercompany.ToIntercompanyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public IntercompanyItemView UpdateIntercompany(IntercompanyItemView intercompanyView)
		{
			try
			{
				IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
				Intercompany inputIntercompany = intercompanyView.ToIntercompany();
				Intercompany dbIntercompany = intercompanyRepo.Find(inputIntercompany.IntercompanyGUID);
				dbIntercompany = intercompanyRepo.UpdateIntercompany(dbIntercompany, inputIntercompany);
				base.LogTransactionUpdate<Intercompany>(GetOriginalValues<Intercompany>(dbIntercompany), dbIntercompany);
				UnitOfWork.Commit();
				return dbIntercompany.ToIntercompanyItemView();
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}
		public bool DeleteIntercompany(string item)
		{
			try
			{
				IIntercompanyRepo intercompanyRepo = new IntercompanyRepo(db);
				Guid intercompanyGUID = new Guid(item);
				Intercompany intercompany = intercompanyRepo.Find(intercompanyGUID);
				intercompanyRepo.Remove(intercompany);
				base.LogTransactionDelete<Intercompany>(intercompany);
				UnitOfWork.Commit();
				return true;
			}
			catch (Exception e)
			{
				throw SmartAppUtil.AddStackTrace(e);
			}
		}

	}
}
