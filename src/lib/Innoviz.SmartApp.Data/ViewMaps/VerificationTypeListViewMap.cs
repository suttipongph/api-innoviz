using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VerificationTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VerificationTypeGUID { get; set; }
		public string VerificationTypeId { get; set; }
		public string Description { get; set; }
		public int VerifyType { get; set; }
	}
}
