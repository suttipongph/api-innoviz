﻿using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class CustomerAgingViewMap : ViewCompanyBaseEntityMap
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string BuyerId { get; set; }
        public string BuyerName { get; set; }
        public int ProductType { get; set; }
        public string InvoiceTypeId { get; set; }
        public string InvoiceId { get; set; }
        public string DocumentId { get; set; }
        public string BuyerInvoiceId { get; set; }
        public DateTime? BuyerInvoiceDate { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? WithdrawalDate { get; set; }
        public DateTime InterestDate { get; set; }
        public DateTime AsOfDate { get; set; }
        public int Days { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PurchaseAmount { get; set; }
        public decimal ReserveAmount { get; set; }
        public decimal AROutstanding { get; set; }
        public decimal PurchaseOutstanding { get; set; }
        public decimal UnearnedInterestBalance { get; set; }
        public decimal AccruedIntBalance { get; set; }
        public decimal FeeBalance { get; set; }
        public decimal NetAR { get; set; }
        public decimal NetARBucket1 { get; set; }
        public decimal NetARBucket2 { get; set; }
        public decimal NetARBucket3 { get; set; }
        public decimal NetARBucket4 { get; set; }
        public decimal NetARBucket5 { get; set; }
        public decimal NetARBucket6 { get; set; }
        public string ResponsibleName { get; set; }
        public string BusinessUnitId { get; set; }
        public string BusinessTypeDesc { get; set; }
        public string CreditAppId { get; set; }
        public string MainAgreementId { get; set; }

        public Guid InvoiceTableGUID { get; set; }
        public Guid InvoiceTable_RefGUID { get; set; }
        public bool ProductInvoice { get; set; }
        public decimal CustTrans_TransAmount { get; set; }
        public decimal CustTrans_SettledAmount { get; set; }
    }
}
