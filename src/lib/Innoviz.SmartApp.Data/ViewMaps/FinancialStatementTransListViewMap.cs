using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class FinancialStatementTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid FinancialStatementTransGUID { get; set; }
		public int Year { get; set; }
		public int Ordering { get; set; }
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public Guid RefGUID { get; set; }
	}
}
