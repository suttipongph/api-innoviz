using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PurchaseTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PurchaseTableGUID { get; set; }
		public string PurchaseId { get; set; }
		public string Description { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public DateTime PurchaseDate { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public bool RollBill { get; set; }
		public string DocumentStatus_Values { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
