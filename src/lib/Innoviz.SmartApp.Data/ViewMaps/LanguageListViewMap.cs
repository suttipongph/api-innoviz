using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class LanguageListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid LanguageGUID { get; set; }
	}
}
