using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BranchItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BranchGUID { get; set; }
		public Guid? ApplicationNumberSeqGUID { get; set; }
		public string BranchId { get; set; }
		public string Name { get; set; }
		public Guid? TaxBranchGUID { get; set; }
		public string TaxBranch_Values { get; set; }
	}
}
