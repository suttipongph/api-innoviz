using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentReturnLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentReturnLineGUID { get; set; }
		public string DocumentReturnTableGUID { get; set; }
		public int LineNum { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string ContactPersonName { get; set; }
		public Guid? DocumentTypeGUID { get; set; }
		public string DocumentNo { get; set; }
		public string BuyerTable_Values { get; set; }
		public string DocumentType_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string DocumentType_DocumentTypeId { get; set; }
	}
}
