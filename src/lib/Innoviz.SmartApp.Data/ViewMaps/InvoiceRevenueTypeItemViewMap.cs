using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceRevenueTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public int AssetFeeType { get; set; }
		public string Description { get; set; }
		public decimal FeeAmount { get; set; }
		public string FeeInvoiceText { get; set; }
		public string FeeLedgerAccount { get; set; }
		public decimal FeeTaxAmount { get; set; }
		public Guid? FeeTaxGUID { get; set; }
		public Guid? FeeWHTGUID { get; set; }
		public Guid? IntercompanyTableGUID { get; set; }
		public int ProductType { get; set; }
		public string RevenueTypeId { get; set; }
		public int ServiceFeeCategory { get; set; }
		public Guid? ServiceFeeRevenueTypeGUID { get; set; }
	}
}
