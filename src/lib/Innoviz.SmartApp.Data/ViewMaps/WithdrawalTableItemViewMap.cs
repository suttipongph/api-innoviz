using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithdrawalTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithdrawalTableGUID { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public decimal AssignmentAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public string CreditComment { get; set; }
		public bool CreditMarkComment { get; set; }
		public Guid CreditTermGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime? ExtendInterestDate { get; set; }
		public Guid? ExtendWithdrawalTableGUID { get; set; }
		public int InterestCutDay { get; set; }
		public string MarketingComment { get; set; }
		public bool MarketingMarkComment { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public int NumberOfExtension { get; set; }
		public string OperationComment { get; set; }
		public bool OperationMarkComment { get; set; }
		public Guid? OriginalWithdrawalTableGUID { get; set; }
		public int ProductType { get; set; }
		public Guid? ReceiptTempTableGUID { get; set; }
		public string Remark { get; set; }
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public decimal SettleTermExtensionFeeAmount { get; set; }
		public bool TermExtension { get; set; }
		public decimal TermExtensionFeeAmount { get; set; }
		public Guid? TermExtensionInvoiceRevenueTypeGUID { get; set; }
		public decimal TotalInterestPct { get; set; }
		public decimal WithdrawalAmount { get; set; }
		public DateTime WithdrawalDate { get; set; }
		public string WithdrawalId { get; set; }
		public int CreditLimitType_CreditLimitConditionType { get; set; }
		public bool CreditLimitType_ValidateBuyerAgreement { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string ExtendWithdrawalTable_Values { get; set; }
		public string OriginalWithdrawalTable_Values { get; set; }
		public string WithdrawalLine_Values { get; set; }
		public decimal NetPaid { get; set; }

		// MainAgreementTable
		public string DocumentStatus_StatusId { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid OperReportSignatureGUID { get; set; }
		public string CreditTerm_Values { get; set; }
		public Guid? InvoiceRevenueType_IntercompanyTableGUID { get; set; }
	}
}
