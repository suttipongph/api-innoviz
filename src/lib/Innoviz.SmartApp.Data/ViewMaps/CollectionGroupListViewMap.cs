using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CollectionGroupListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CollectionGroupGUID { get; set; }
	}
}
