using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InquiryWithdrawalLineOutstandItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithdrawalLineGUID { get; set; }
		public string WithdrawalTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerAgreementTrans_Values { get; set; }
		public decimal WithdrawalAmount { get; set; }
		public decimal Outstanding { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime? ChequeTable_ChequeDate { get; set; }
		public DateTime? BillingDate { get; set; }
		public DateTime CollectionDate { get; set; }
		public string CreditAppTableBankAccountControl_BankAccountName { get; set; }
		public string CreditAppTablePDCBank_BankAccountName { get; set; }
		public int WithdrawalLineType { get; set; }
		public string CreditAppLine_Values { get; set; }
	}
}
