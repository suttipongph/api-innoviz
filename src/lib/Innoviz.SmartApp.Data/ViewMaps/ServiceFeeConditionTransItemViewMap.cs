using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ServiceFeeConditionTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ServiceFeeConditionTransGUID { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public string Description { get; set; }
		public bool Inactive { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public int Ordering { get; set; }
		public Guid RefGUID { get; set; }
		public Guid? RefServiceFeeConditionTransGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public int InvoiceRevenueType_ProductType { get; set; }
		public int InvoiceRevenueType_ServiceFeeCategory { get; set; }
		public Guid? InvoiceRevenueType_FeeTaxGUID { get; set; }
		public Guid? InvoiceRevenueType_FeeWHTGUID { get; set; }
	}
}
