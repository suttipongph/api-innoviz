using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ExposureGroupByProductItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ExposureGroupByProductGUID { get; set; }
		public decimal ExposureAmount { get; set; }
		public Guid ExposureGroupGUID { get; set; }
		public int ProductType { get; set; }
		public string ExposureGroup_Values { get; set; }
	}
}
