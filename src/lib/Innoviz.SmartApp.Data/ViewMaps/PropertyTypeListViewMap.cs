using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PropertyTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PropertyTypeGUID { get; set; }
		public string Description { get; set; }
		public string PropertyTypeId { get; set; }
	}
}
