using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BookmarkDocumentTemplateTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BookmarkDocumentTemplateTableGUID { get; set; }
		public int BookmarkDocumentRefType { get; set; }
		public string BookmarkDocumentTemplateId { get; set; }
		public string Description { get; set; }
	}
}
