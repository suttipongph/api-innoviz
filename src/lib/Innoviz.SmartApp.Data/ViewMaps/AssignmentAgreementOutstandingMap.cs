﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class AssignmentAgreementOutstandingMap : ViewCompanyBaseEntityMap
    {
        public string InternalAssignmentAgreementId { get; set; }
        public Guid CustomerTableGUID { get; set; }
        public Guid AssignmentAgreementTableGUID { get; set; }
        public Guid BuyerTableGUID { get; set; }
        public decimal AssignmentAgreementAmount { get; set; }
        public decimal SettledAmount { get; set; }
        public decimal RemainingAmount { get; set; }
        public string CustomerName { get; set; }
        public string BuyerName { get; set; }
        public string CustomerTable_CustomerId { get; set; }
        public string BuyerTable_BuyerId { get; set; }
        public int RefType { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public string AssignmentAgreementId { get; set; }
        public Guid? DocumentStatusGUID { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public Guid AssignmentMethodGUID { get; set; }
        public string AssignmentMethod_Values { get; set; }
        public string AssignmentMethod_InternalAssignmentAgreementId { get; set; }
        public int AgreementDocType { get; set; }
        public DateTime? AgreementDate { get; set; }
        public string Description { get; set; }
        public string ReferenceAgreementId { get; set; }
    }
}
