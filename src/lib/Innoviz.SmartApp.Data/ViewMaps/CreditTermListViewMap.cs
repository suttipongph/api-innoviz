using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditTermListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditTermGUID { get; set; }
		public string CreditTermId { get; set; }
		public string Description { get; set; }
		public int NumberOfDays { get; set; }
	}
}
