using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ContactTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ContactTransGUID { get; set; }
		public int ContactType { get; set; }
		public string ContactValue { get; set; }
		public string Description { get; set; }
		public string Extension { get; set; }
		public bool PrimaryContact { get; set; }
		public Guid RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
	}
}
