using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentTemplateTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentTemplateTableGUID { get; set; }
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
		public string TemplateId { get; set; }
	}
}
