using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppReqAssignmentListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppReqAssignmentGUID { get; set; }
		public bool IsNew { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public Guid AssignmentMethodGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string AssignmentMethod_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string AssignmentAgreementTable_InternalAssignmentAgreementId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
	}
}
