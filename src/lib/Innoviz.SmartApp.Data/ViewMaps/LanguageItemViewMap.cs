using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class LanguageItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid LanguageGUID { get; set; }
		public string LanguageId { get; set; }
		public string Name { get; set; }
	}
}
