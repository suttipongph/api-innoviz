using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InterestTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InterestTypeGUID { get; set; }
		public string InterestTypeId { get; set; }
		public string Description { get; set; }
	}
}
