using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NCBTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NCBTransGUID { get; set; }
		public Guid BankGroupGUID { get; set; }
		public decimal CreditLimit { get; set; }
		public Guid CreditTypeGUID { get; set; }
		public DateTime EndDate { get; set; }
		public decimal MonthlyRepayment { get; set; }
		public Guid NCBAccountStatusGUID { get; set; }
		public decimal OutstandingAR { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
	}
}
