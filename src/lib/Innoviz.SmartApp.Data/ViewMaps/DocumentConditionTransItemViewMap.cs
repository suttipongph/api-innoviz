using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentConditionTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentConditionTransGUID { get; set; }
		public int DocConVerifyType { get; set; }
		public Guid DocumentTypeGUID { get; set; }
		public bool Mandatory { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public Guid? RefDocumentConditionTransGUID { get; set; }
		public bool Inactive { get; set; }
	}
}
