using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppReqBusinessCollateralListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppReqBusinessCollateralGUID { get; set; }
		public bool IsNew { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public decimal CapitalValuation { get; set; }
		public Guid? CustBusinessCollateralGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public string Description { get; set; }
		public string RefAgreementId { get; set; }
		public DateTime? RefAgreementDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CustBusinessCollateral_Values { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public string BusinessCollateralSubType_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CustBusinessCollateral_CustBusinessCollateralId { get; set; }
		public string BusinessCollateralType_BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralSubType_BusinessCollateralSubTypeId { get; set; }
	}
}
