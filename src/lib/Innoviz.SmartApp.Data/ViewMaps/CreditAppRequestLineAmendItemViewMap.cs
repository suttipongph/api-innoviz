using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestLineAmendItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestLineAmendGUID { get; set; }
		public bool AmendAssignmentMethod { get; set; }
		public bool AmendBillingDocumentCondition { get; set; }
		public bool AmendBillingInformation { get; set; }
		public bool AmendRate { get; set; }
		public bool AmendReceiptDocumentCondition { get; set; }
		public bool AmendReceiptInformation { get; set; }
		public Guid CreditAppRequestLineGUID { get; set; }
		public Guid? OriginalAssignmentMethodGUID { get; set; }
		public string OriginalAssignmentMethodRemark { get; set; }
		public Guid? OriginalBillingAddressGUID { get; set; }
		public decimal OriginalCreditLimitLineRequest { get; set; }
		public decimal OriginalMaxPurchasePct { get; set; }
		public Guid? OriginalMethodOfPaymentGUID { get; set; }
		public int OriginalPurchaseFeeCalculateBase { get; set; }
		public decimal OriginalPurchaseFeePct { get; set; }
		public Guid? OriginalReceiptAddressGUID { get; set; }
		public string OriginalReceiptRemark { get; set; }
		public string OriginalAssignmentMethod_Values { get; set; }
		public string OriginalBillingAddress_Values { get; set; }
		public string OriginalMethodOfPayment_Values { get; set; }
		public string OriginalReceiptAddress_Values { get; set; }
		public string UnboundOriginalBillingAddress { get; set; }
		public string UnboundOriginalReceiptAddress { get; set; }

		#region CreditAppRequest
		public Guid? CreditAppRequestTable_CustomerTableGUID { get; set; }
		public Guid? CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public DateTime? CreditAppRequestTable_RequestDate { get; set; }
		public decimal CreditAppRequestTable_CustomerCreditLimit { get; set; }
		public string CreditAppRequestTable_Remark { get; set; }
		public DateTime? CreditAppRequestTable_ApprovedDate { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid? CreditAppRequestTable_DocumentStatusGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public Guid? RefCreditAppTableGUID { get; set; }
		public string CreditAppRequestTable_CACondition { get; set; }
		#endregion CreditAppRequest
		#region CreditAppRequestLine
		public decimal CreditAppRequestLine_NewCreditLimitLineRequest { get; set; }
		public decimal CreditAppRequestLine_BuyerCreditLimit { get; set; }
		public decimal CreditAppRequestLine_ApprovedCreditLimitLineRequest { get; set; }
		public decimal CreditAppRequestLine_NewMaxPurchasePct { get; set; }
		public decimal CreditAppRequestLine_NewPurchaseFeePct { get; set; }
		public int CreditAppRequestLine_NewPurchaseFeeCalculateBase { get; set; }
		public Guid? CreditAppRequestLine_AssignmentMethodGUID { get; set; }
		public string CreditAppRequestLine_NewAssignmentMethodRemark { get; set; }
		public string CreditAppRequestLine_MarketingComment { get; set; }
		public string CreditAppRequestLine_CreditComment { get; set; }
		public string CreditAppRequestLine_ApproverComment { get; set; }
		public Guid? CreditAppRequestLine_NewMethodOfPaymentGUID { get; set; }
		public string CreditAppRequestLine_NewReceiptRemark { get; set; }
		public Guid? CreditAppRequestLine_NewReceiptAddressGUID { get; set; }
		public Guid? CreditAppRequestLine_RefCreditAppLineGUID { get; set; }
		public Guid? CreditAppRequestLine_NewBillingAddressGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public Guid? CreditAppRequestLine_BuyerTableGUID { get; set; }
		public string UnboundNewBillingAddress { get; set; }
		public string unboundNewReceiptAddress { get; set; }
		public Guid? CreditAppRequestLine_BlacklistStatusGUID { get; set; }
		public Guid? CreditAppRequestLine_CreditScoringGUID { get; set; }
		public Guid? CreditAppRequestLine_KYCSetupGUID { get; set; }
		public decimal CreditAppRequestLine_CustomerBuyerOutstanding { get; set; }
		public decimal CreditAppRequestLine_AllCustomerBuyerOutstanding { get; set; }

		#endregion CreditAppRequestLine
		public int ProcessInstanceId { get; set; }
	}
}
