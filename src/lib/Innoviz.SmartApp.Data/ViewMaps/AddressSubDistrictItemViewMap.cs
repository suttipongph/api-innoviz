using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressSubDistrictItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressSubDistrictGUID { get; set; }
		public Guid AddressDistrictGUID { get; set; }
		public Guid addressProvinceGUID { get; set; }
		public string Name { get; set; }
		public string SubDistrictId { get; set; }
		public string addressDistrict_DistrictId { get; set; }
		public string addressDistrict_Name { get; set; }
		public string addressProvince_ProvinceId { get; set; }
	}
}
