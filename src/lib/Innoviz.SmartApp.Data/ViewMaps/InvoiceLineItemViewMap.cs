using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceLineItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid InvoiceLineGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? InvoiceRevenueTypeGUID { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public string InvoiceText { get; set; }
		public int LineNum { get; set; }
		public Guid? ProdUnitGUID { get; set; }
		public decimal Qty { get; set; }
		public decimal TaxAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public decimal TotalAmount { get; set; }
		public decimal TotalAmountBeforeTax { get; set; }
		public decimal UnitPrice { get; set; }
		public decimal WHTAmount { get; set; }
		public decimal WHTBaseAmount { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
	}
}
