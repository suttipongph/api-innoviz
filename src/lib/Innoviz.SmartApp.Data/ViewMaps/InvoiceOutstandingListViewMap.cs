﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class InvoiceOutstandingListViewMap : ViewBranchCompanyBaseEntityMap
    {
        public decimal BalanceAmount { get; set; }
        public Guid? BuyerAgreementTableGUID { get; set; }
        public Guid? BuyerInvoiceTableGUID { get; set; }
        public Guid? BuyerTableGUID { get; set; }
        public string DocumentId { get; set; }
        public DateTime DueDate { get; set; }
        public Guid? RefGUID { get; set; }
        public int RefType { get; set; }
        public decimal InvoiceAmount { get; set; }
        public Guid InvoiceTableGUID { get; set; }
        public Guid InvoiceTypeGUID { get; set; }
        public bool ProductInvoice { get; set; }
        public int ProductType { get; set; }
        public string InvoiceTable_Values { get; set; }
        public string InvoiceTable_InvoiceId { get; set; }
        public Guid? InvoiceTable_CustomerTableGUID { get; set; }
        public int SuspenseInvoiceType { get; set; }
        public string InvoiceType_Values { get; set; }
        public string InvoiceType_InvoiceTypeId { get; set; }
        public string BuyerInvoiceTable_Values { get; set; }
        public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
        public string BuyerAgreementTable_Values { get; set; }
        public string BuyerAgreementTable_BuyerAgreementId { get; set; }
        public string BuyerTable_Values { get; set; }
        public string BuyerTable_BuyerId { get; set; }
        public string CreditAppTable_Values { get; set; }
        public string Currency_Values { get; set; }        
        public Guid? RefReceiptTempPaymDetailGUID { get; set; }
        #region Unbound 
        public decimal MaxSettleAmount { get; set; }
        public bool ExitsInvoice { get; set; }
        #endregion Unbound
    }
}
