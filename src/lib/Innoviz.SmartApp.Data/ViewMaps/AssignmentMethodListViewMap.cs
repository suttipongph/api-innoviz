using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AssignmentMethodListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AssignmentMethodGUID { get; set; }
		public string AssignmentMethodId { get; set; }
		public string Description { get; set; }
		public bool ValidateAssignmentBalance { get; set; }
	}
}
