using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditLimitTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditLimitTypeGUID { get; set; }
		public string CreditLimitTypeId { get; set; }
		public string Description { get; set; }
		public int ProductType { get; set; }
		public bool Revolving { get; set; }
		public int CreditLimitExpiration { get; set; }
		public int CreditLimitExpiryDay { get; set; }
		public int InactiveDay { get; set; }
		public int CloseDay { get; set; }
		public int BookmarkOrdering { get; set; }
	}
}
