using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class KYCSetupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid KYCSetupGUID { get; set; }
		public string Description { get; set; }
		public string KYCId { get; set; }
	}
}
