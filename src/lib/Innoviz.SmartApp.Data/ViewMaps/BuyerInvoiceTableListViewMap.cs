using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerInvoiceTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerInvoiceTableGUID { get; set; }
		public string BuyerInvoiceId { get; set; }
		public DateTime InvoiceDate { get; set; }
		public DateTime DueDate { get; set; }
		public decimal Amount { get; set; }
		public Guid CreditAppLineGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? BuyerAgreementLineGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string BuyerAgreementLine_Values { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
		public string BuyerAgreementLine_Period { get; set; }
		public bool AccessModeCanDelete { get; set; }

	}
}
