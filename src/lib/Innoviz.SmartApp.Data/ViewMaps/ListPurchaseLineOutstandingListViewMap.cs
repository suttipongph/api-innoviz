using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ListPurchaseLineOutstandingListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ListPurchaseLineOutstandingGUID { get; set; }
		public DateTime? BillingDate { get; set; }
		public string BuyerTable_Values { get; set; }
		public DateTime CollectionDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public DateTime DueDate { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string PurchaseTable_Values { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal Outstanding { get; set; }
		public decimal PurchaseAmount { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public Guid PurchaseTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public string CustomerId { get; set; }
		public string MethodOfPaymentId { get; set; }
		public string PurchaseId { get; set; }
		public string BuyerId { get; set; }
		public DateTime InterestDate { get; set; }
		public bool ClosedForAdditionalPurchase { get; set; }
	}
}
