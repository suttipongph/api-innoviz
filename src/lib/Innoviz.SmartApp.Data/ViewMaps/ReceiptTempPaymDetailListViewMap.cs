using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptTempPaymDetailListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ReceiptTempPaymDetailGUID { get; set; }
		public Guid MethodOfPaymentGUID { get; set; }
		public Guid ReceiptTempTableGUID { get; set; }
		public decimal ReceiptAmount { get; set; }
		public Guid? ChequeTableGUID { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string ChequeTable_Values { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public string ChequeTable_ChequeNo { get; set; }
		public Guid? InvoiceSettlementDetail_RefReceiptTempPaymDetailGUID { get; set; }
	}
}
