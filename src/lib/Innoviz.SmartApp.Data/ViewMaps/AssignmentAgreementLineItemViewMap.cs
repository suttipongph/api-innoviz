using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AssignmentAgreementLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AssignmentAgreementLineGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		public int LineNum { get; set; }
		public string ReferenceAgreementID { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public string AssignmentAgreementTable_StatusId { get; set; }
		public string AssignmentAgreementTable_InternalAssignmentAgreementId { get; set; }
		public string AssignmentAgreementTable_AssignmentAgreementId { get; set; }
		public Guid AssignmentAgreementTable_CustomerTableGUID { get; set; }
		public Guid AssignmentAgreementTable_BuyerTableGUID { get; set; }
		public string AssignmentAgreementTable_Description { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
	}
}
