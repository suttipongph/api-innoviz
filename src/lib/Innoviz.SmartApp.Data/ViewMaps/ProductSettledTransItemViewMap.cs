using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProductSettledTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProductSettledTransGUID { get; set; }
		public string DocumentId { get; set; }
		public decimal InterestCalcAmount { get; set; }
		public DateTime InterestDate { get; set; }
		public int InterestDay { get; set; }
		public Guid InvoiceSettlementDetailGUID { get; set; }
		public DateTime? LastReceivedDate { get; set; }
		public string OriginalDocumentId { get; set; }
		public decimal OriginalInterestCalcAmount { get; set; }
		public Guid? OriginalRefGUID  { get; set; }
		public decimal PDCInterestOutstanding { get; set; }
		public decimal PostedInterestAmount { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public decimal ReserveToBeRefund { get; set; }
		public DateTime SettledDate { get; set; }
		public decimal SettledInvoiceAmount { get; set; }
		public decimal SettledPurchaseAmount { get; set; }
		public decimal SettledReserveAmount { get; set; }
		public decimal TotalRefundAdditionalIntAmount { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
		public string RefId { get; set; }
	}
}
