using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentTypeGUID { get; set; }
		public string DocumentTypeId { get; set; }
		public string Description { get; set; }
	}
}
