using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerCreditLimitByProductItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerCreditLimitByProductGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public decimal CreditLimit { get; set; }
		public int ProductType { get; set; }
		public decimal CreditLimitBalance { get; set; }
	}
}
