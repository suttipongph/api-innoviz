using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CompanyItemViewMap : ViewCompanyBaseEntityMap
	{
		public string CompanyLogo { get; set; }
		public string Name { get; set; }
		public string SecondName { get; set; }
		public string AltName { get; set; }
		public Guid? DefaultBranchGUID { get; set; }
		public string TaxId { get; set; }
	}
}
