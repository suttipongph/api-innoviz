using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxInvoiceLineItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid TaxInvoiceLineGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceText { get; set; }
		public int LineNum { get; set; }
		public decimal Qty { get; set; }
		public decimal TaxAmount { get; set; }
		public Guid TaxInvoiceTableGUID { get; set; }
		public Guid TaxTableGUID { get; set; }
		public decimal TotalAmount { get; set; }
		public decimal TotalAmountBeforeTax { get; set; }
		public decimal UnitPrice { get; set; }
		public string TaxInvoiceTable_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string ProdUnit_Values { get; set; }
		public string TaxTable_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public Guid ProdUnitGUID { get; set; }

	}
}
