using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppTransGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public decimal CreditDeductAmount { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string DocumentId { get; set; }
		public decimal InvoiceAmount { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public DateTime TransDate { get; set; }
		public string ProductType_Values { get; set; }
		public string RefType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppLine_Values { get; set; }
	}
}
