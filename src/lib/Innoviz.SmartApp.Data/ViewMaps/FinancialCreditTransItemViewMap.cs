using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class FinancialCreditTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid FinancialCreditTransGUID { get; set; }
		public decimal Amount { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid CreditTypeGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
	}
}
