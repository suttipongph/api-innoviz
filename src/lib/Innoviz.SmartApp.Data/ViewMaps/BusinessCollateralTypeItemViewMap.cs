using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralTypeGUID { get; set; }
		public int AgreementOrdering { get; set; }
		public string BusinessCollateralTypeId { get; set; }
		public string Description { get; set; }
	}
}
