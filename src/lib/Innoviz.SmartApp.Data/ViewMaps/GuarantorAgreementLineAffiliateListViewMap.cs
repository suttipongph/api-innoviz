using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorAgreementLineAffiliateListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorAgreementLineAffiliateGUID { get; set; }
		public int Ordering { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Customer_Values { get; set; }
		public Guid GuarantorAgreementLineGUID { get; set; }
}
}
