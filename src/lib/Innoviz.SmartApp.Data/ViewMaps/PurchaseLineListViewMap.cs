using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PurchaseLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PurchaseLineGUID { get; set; }
		public Guid PurchaseTableGUID { get; set; }
		public int LineNum { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid BuyerInvoiceTableGUID { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime? BillingDate { get; set; }
		public DateTime CollectionDate { get; set; }
		public decimal BuyerInvoiceAmount { get; set; }
		public decimal PurchaseAmount { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public string BuyerInvoiceTable_Values { get; set; }
		public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
	}
}
