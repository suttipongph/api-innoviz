using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerAgreementTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementId { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public DateTime EndDate { get; set; }
		public decimal MaximumCreditLimit { get; set; }
		public string Penalty { get; set; }
		public string ReferenceAgreementID { get; set; }
		public string Remark { get; set; }
		public DateTime StartDate { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public Guid CreditAppRequestLine_CreditAppRequestLineGUID { get; set; }
		public Guid BuyerAgreementTrans_RefGUID { get; set; }
		public int BuyerAgreementTrans_RefType { get; set; }
		public Guid CreditAppTable_CreditAppTableGUID { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public string BuyerAgreementLine_Description { get; set; }
	}
}
