﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class InvoiceLineAmountParamView
    {
        public DateTime IssuedDate { get; set; }
        public decimal InvoiceAmount { get; set; }
        public bool IncludeTax { get; set; }
        public Guid? InvoiceRevenueTypeGUID { get; set; }
    }
    public class InvoiceLineAmountResultView
    {
        public int Qty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalAmountBeforeTax { get; set; }
        public Guid? TaxTableGUID { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public Guid? WithholdingTaxTableGUID { get; set; }
        public decimal WHTBaseAmount { get; set; }
        public decimal WHTAmount { get; set; }
    }
}
