using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class StagingTableVendorInfoListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid StagingTableVendorInfoGUID { get; set; }
		public int RecordType { get; set; }
		public string VendorId { get; set; }
		public string Name { get; set; }
		public string CurrencyId { get; set; }
		public string VendGroupId { get; set; }
		public string BankAccountName { get; set; }
		public string BankAccount { get; set; }
		public string BankGroupId { get; set; }
		public Guid? ProcessTransGUID { get; set; }
	}
}
