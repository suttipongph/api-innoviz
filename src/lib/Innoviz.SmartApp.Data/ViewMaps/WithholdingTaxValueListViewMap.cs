using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithholdingTaxValueListViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid WithholdingTaxValueGUID { get; set; }
		public Guid WithholdingTaxTableGUID { get; set; }
		public decimal Value { get; set; }
	}
}
