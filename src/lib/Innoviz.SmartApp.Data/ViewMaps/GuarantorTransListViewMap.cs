using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorTransGUID { get; set; }
		public int Ordering { get; set; }
		public Guid GuarantorTypeGUID { get; set; }
		public bool Affiliate { get; set; }
		public bool InActive { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid? RefGuarantorTransGUID { get; set; }

		public string GuarantorType_Values { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string GuarantorType_GuarantorTypeId { get; set; }
		public decimal MaximumGuaranteeAmount { get; set; }

	}
}
