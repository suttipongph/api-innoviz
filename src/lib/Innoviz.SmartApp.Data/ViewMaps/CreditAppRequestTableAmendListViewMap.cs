using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestTableAmendListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestTableAmendGUID { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public DateTime? CreditAppRequestTable_RequestDate { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public Guid? CreditAppRequestTable_RefCreditAppTableGUID { get; set; }
	}
}
