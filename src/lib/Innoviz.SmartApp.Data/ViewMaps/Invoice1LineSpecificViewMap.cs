﻿using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class Invoice1LineSpecificParamView
    {
        public RefType RefType { get; set; }
        public Guid RefGUID { get; set; }
        public DateTime IssuedDate { get; set; }
        public DateTime DueDate { get; set; }
        public Guid CustomerTableGUID { get; set; }
        public ProductType ProductType { get; set; }
        public Guid InvoiceTypeGUID { get; set; }
        public bool AutoGenInvoiceRevenueType { get; set; }
        public decimal InvoiceAmount { get; set; }
        public bool IncludeTax { get; set; }
        public Guid CompanyGUID { get; set; }
        public string DocumentId { get; set; }
        public Guid? InvoiceRevenueTypeGUID { get; set; }
        public Guid? CreditAppTableGUID { get; set; }
        public Guid? BuyerTableGUID { get; set; }
        public Guid? Dimension1GUID { get; set; }
        public Guid? Dimension2GUID { get; set; }
        public Guid? Dimension3GUID { get; set; }
        public Guid? Dimension4GUID { get; set; }
        public Guid? Dimension5GUID { get; set; }
        public Guid? MethodOfPaymentGUID { get; set; }
        public Guid? BuyerInvoiceTableGUID { get; set; }
        public Guid? BuyerAgreementTableGUID { get; set; }
        public Guid? ReceiptTempTableGUID { get; set; }
    }
}
