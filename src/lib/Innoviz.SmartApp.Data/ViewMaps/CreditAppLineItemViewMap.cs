using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class CreditAppLineItemViewMap : ViewCompanyBaseEntityMap
    {
        public Guid CreditAppLineGUID { get; set; }
        public bool AcceptanceDocument { get; set; }
        public string AcceptanceDocumentDescription { get; set; }
        public decimal ApprovedCreditLimitLine { get; set; }
        public Guid? AssignmentAgreementTableGUID { get; set; }
        public Guid? AssignmentMethodGUID { get; set; }
        public string AssignmentMethodRemark { get; set; }
        public Guid? BillingAddressGUID { get; set; }
        public Guid? BillingContactPersonGUID { get; set; }
        public int BillingDay { get; set; }
        public string BillingDescription { get; set; }
        public string BillingRemark { get; set; }
        public Guid? BillingResponsibleByGUID { get; set; }
        public Guid BuyerTableGUID { get; set; }
        public Guid CreditAppTableGUID { get; set; }
        public string CreditTermDescription { get; set; }
        public Guid? CreditTermGUID { get; set; }
        public DateTime ExpiryDate { get; set; }
        public decimal InsuranceCreditLimit { get; set; }
        public Guid? InvoiceAddressGUID { get; set; }
        public int LineNum { get; set; }
        public Guid? MailingReceiptAddressGUID { get; set; }
        public decimal MaxPurchasePct { get; set; }
        public int MethodOfBilling { get; set; }
        public Guid? MethodOfPaymentGUID { get; set; }
        public string PaymentCondition { get; set; }
        public int PurchaseFeeCalculateBase { get; set; }
        public decimal PurchaseFeePct { get; set; }
        public Guid? ReceiptAddressGUID { get; set; }
        public Guid? ReceiptContactPersonGUID { get; set; }
        public int ReceiptDay { get; set; }
        public string ReceiptDescription { get; set; }
        public string ReceiptRemark { get; set; }
        public Guid? RefCreditAppRequestLineGUID { get; set; }
        public DateTime? ReviewDate { get; set; }
        public string UnboundBillingAddress { get; set; }
        public string UnboundReceiptAddress { get; set; }
        public string UnboundMailingReceiptAddress { get; set; }
        public string UnboundInvoiceAddress { get; set; }
        public string AssignmentAgreementTable_Values { get; set; }
        public string AssignmentMethod_Values { get; set; }
        public string BillingAddress_Values { get; set; }
        public string BillingContactPerson_Values { get; set; }
        public string BillingResponsibleBy_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public string CreditAppTable_Values { get; set; }
        public string CreditTerm_Values { get; set; }
        public string InvoiceAddress_Values { get; set; }
        public string MailingReceiptAddress_Values { get; set; }
        public string MethodOfPayment_Values { get; set; }
        public string ReceiptAddress_Values { get; set; }
        public string ReceiptContactPerson_Values { get; set; }
        public string RefCreditAppRequestLine_Values { get; set; }
        public string RefCreditAppRequestTable_Values { get; set; }
        public string BusinessSegment_Values { get; set; }
        public string BusinessType_Values { get; set; }
        public string LineOfBusiness_Values { get; set; }
        public DateTime? DateOfEstablish { get; set; }
        public string CreditAppTable_ProductType { get; set; }
        public string LineCondition { get; set; }

    }
}
