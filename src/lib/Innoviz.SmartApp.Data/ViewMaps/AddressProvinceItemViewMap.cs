using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressProvinceItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressProvinceGUID { get; set; }
		public Guid AddressCountryGUID { get; set; }
		public string Name { get; set; }
		public string ProvinceId { get; set; }
		public string CountryId { get; set; }
		public string AddressCountry_Name { get; set; }
	}
}
