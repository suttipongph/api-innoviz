using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class JobTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid JobTypeGUID { get; set; }
		public string Description { get; set; }
		public string JobTypeId { get; set; }
		public int ShowDocConVerifyType { get; set; }
		public bool Assignment { get; set; }
	}
}
