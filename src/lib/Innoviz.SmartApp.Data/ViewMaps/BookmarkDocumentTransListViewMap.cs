using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BookmarkDocumentTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BookmarkDocumentTransGUID { get; set; }
		public Guid BookmarkDocumentGUID { get; set; }
		public Guid DocumentTemplateTableGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public string ReferenceExternalId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string BookmarkDocument_Values { get; set; }
		public string DocumentTemplateTable_Values { get; set; }
		public string BookmarkDocument_BookmarkDocumentId { get; set; }
		public string DocumentTemplateTable_TemplateId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public int BookmarkDocument_BookmarkDocumentRefType { get; set; }
		public int BookmarkDocument_DocumentTemplateType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string BookmarkDocumentId { get; set; }
	}
}
