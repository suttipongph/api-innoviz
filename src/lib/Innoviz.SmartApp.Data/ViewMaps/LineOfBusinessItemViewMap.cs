using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class LineOfBusinessItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid LineOfBusinessGUID { get; set; }
		public string Description { get; set; }
		public string LineOfBusinessId { get; set; }
	}
}
