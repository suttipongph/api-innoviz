using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RelatedPersonTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RelatedPersonTableGUID { get; set; }
		public string RelatedPersonId { get; set; }
		public string Name { get; set; }
		public string OperatedBy { get; set; }
		public int RecordType { get; set; }
		public string TaxId { get; set; }
		public string PassportId { get; set; }
		public string Position { get; set; }
		public string CompanyName { get; set; }
	}
}
