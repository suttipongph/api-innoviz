using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BookmarkDocumentTemplateLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BookmarkDocumentTemplateLineGUID { get; set; }
		public Guid BookmarkDocumentGUID { get; set; }
		public Guid DocumentTemplateTableGUID { get; set; }
		public Guid? BookmarkDocumentTemplateTableGUID { get; set; }
		public string BookmarkDocumentId { get; set; }
		public string TemplateId { get; set; }
		public string BookmarkDocument_Values { get; set; }
		public string DocumentTemplateTable_Values { get; set; }
		public int DocumentTemplateType { get; set; }
	}
}
