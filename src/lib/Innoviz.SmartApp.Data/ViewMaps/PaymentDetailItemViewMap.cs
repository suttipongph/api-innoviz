using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PaymentDetailItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PaymentDetailGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid? InvoiceTypeGUID { get; set; }
		public int PaidToType { get; set; }
		public decimal PaymentAmount { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public bool SuspenseTransfer { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public string RefId { get; set; }
	}
}
