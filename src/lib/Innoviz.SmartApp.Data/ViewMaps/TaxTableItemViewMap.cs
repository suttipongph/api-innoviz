using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid TaxTableGUID { get; set; }
		public string Description { get; set; }
		public string InputTaxLedgerAccount { get; set; }
		public string OutputTaxLedgerAccount { get; set; }
		public Guid? PaymentTaxTableGUID { get; set; }
		public string TaxCode { get; set; }
	}
}
