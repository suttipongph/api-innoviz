using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestTableGUID { get; set; }
		public string CreditAppRequestId { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public int CreditAppRequestType { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		public DateTime RequestDate { get; set; }
		public decimal CreditLimitRequest { get; set; }
		public decimal ApprovedCreditLimitRequest { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid? RefCreditAppTableGUID { get; set; }
        #region For ReviewCreditAppRequest
        public Guid? CreditAppLineGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppRequestLine_BuyerId { get; set; }
		public Guid? CreditAppRequestLine_BuyerTableGUID { get; set; }
		public string RefCreditAppTable_CreditAppId { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public decimal BuyerCreditLimit { get; set; }
		public decimal CreditLimitLineRequest { get; set; }
		#endregion For ReviewCreditAppRequest

	}
}
