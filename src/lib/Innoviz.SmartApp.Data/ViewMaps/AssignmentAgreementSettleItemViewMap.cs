using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AssignmentAgreementSettleItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AssignmentAgreementSettleGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public string DocumentId { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public Guid? RefAssignmentAgreementSettleGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public decimal SettledAmount { get; set; }
		public DateTime SettledDate { get; set; }
		public string AssignmentAgreementTable_Value { get; set; }
		public string AssignmentAgreementTable_AssignmentAgreementId { get; set; }
		public string BuyerTable_Value { get; set; }
		public string DocumentReason_ReasonId { get; set; }
		public string RefAssignTable_Value { get; set; }
		public string InvoiceTable_Values { get; set; }
	}
}
