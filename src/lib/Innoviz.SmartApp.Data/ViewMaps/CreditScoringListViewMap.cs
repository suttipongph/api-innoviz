using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditScoringListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditScoringGUID { get; set; }
		public string CreditScoringId { get; set; }
		public string Description { get; set; }
	}
}
