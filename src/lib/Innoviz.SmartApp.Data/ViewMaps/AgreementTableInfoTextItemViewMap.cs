using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AgreementTableInfoTextItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AgreementTableInfoTextGUID { get; set; }
		public	Guid AgreementTableInfoGUID { get; set; }
		public string Text1 { get; set; }
		public string Text2 { get; set; }
		public string Text3 { get; set; }
		public string Text4 { get; set; }
	}
}
