using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MainAgreementPFBookmarkViewMap : ViewCompanyBaseEntityMap
	{
		public int RowNumber { get; set; }
		public int LineNum { get; set; }
		public bool Revolving { get; set; }
		public string Description { get; set; }
		public string AgreementYear { get; set; }
		public string GuarantorName { get; set; }
		public string GuaranteeText { get; set; }
		public string CreditLimitType { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? ExpiryDate { get; set; }
		public decimal WithdrawalAmount { get; set; }
		public DateTime? DueDate { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public string ChequeNo { get; set; }
		public string ChequeNo1 { get; set; }
		public string ChequeNo2 { get; set; }
		public string ChequeNo3 { get; set; }
		public string ChequeNo4 { get; set; }
		public string ChequeNo5 { get; set; }
		public string ChequeNo6 { get; set; }
		public string ChequeNo7 { get; set; }
		public string ChequeNo8 { get; set; }
		public string ChequeNo9 { get; set; }
		public string ChequeNo10 { get; set; }
		public string ChequeNo11 { get; set; }
		public string ChequeNo12 { get; set; }
		public string ChequeNo13 { get; set; }
		public string ChequeNo14 { get; set; }
		public string ChequeNo15 { get; set; }
		public string ChequeNo16 { get; set; }
		public string ChequeNo17 { get; set; }
		public string ChequeNo18 { get; set; }
		public string ChequeNo19 { get; set; }
		public string ChequeNo20 { get; set; }
		public string ChequeNo21 { get; set; }
		public string ChequeNo22 { get; set; }
		public string ChequeNo23 { get; set; }
		public string ChequeNo24 { get; set; }
		public DateTime? ChequeDate { get; set; }
		public DateTime? ChequeDate1 { get; set; }
		public DateTime? ChequeDate2 { get; set; }
		public DateTime? ChequeDate3 { get; set; }
		public DateTime? ChequeDate4 { get; set; }
		public DateTime? ChequeDate5 { get; set; }
		public DateTime? ChequeDate6 { get; set; }
		public DateTime? ChequeDate7 { get; set; }
		public DateTime? ChequeDate8 { get; set; }
		public DateTime? ChequeDate9 { get; set; }
		public DateTime? ChequeDate10 { get; set; }
		public DateTime? ChequeDate11 { get; set; }
		public DateTime? ChequeDate12 { get; set; }
		public DateTime? ChequeDate13 { get; set; }
		public DateTime? ChequeDate14 { get; set; }
		public DateTime? ChequeDate15 { get; set; }
		public DateTime? ChequeDate16 { get; set; }
		public DateTime? ChequeDate17 { get; set; }
		public DateTime? ChequeDate18 { get; set; }
		public DateTime? ChequeDate19 { get; set; }
		public DateTime? ChequeDate20 { get; set; }
		public DateTime? ChequeDate21 { get; set; }
		public DateTime? ChequeDate22 { get; set; }
		public DateTime? ChequeDate23 { get; set; }
		public DateTime? ChequeDate24 { get; set; }
		public decimal InterestAmount { get; set; }
		public decimal InterestAmount1 { get; set; }
		public decimal InterestAmount2 { get; set; }
		public decimal InterestAmount3 { get; set; }
		public decimal InterestAmount4 { get; set; }
		public decimal InterestAmount5 { get; set; }
		public decimal InterestAmount6 { get; set; }
		public decimal InterestAmount7 { get; set; }
		public decimal InterestAmount8 { get; set; }
		public decimal InterestAmount9 { get; set; }
		public decimal InterestAmount10 { get; set; }
		public decimal InterestAmount11 { get; set; }
		public decimal InterestAmount12 { get; set; }
		public decimal InterestAmount13 { get; set; }
		public decimal InterestAmount14 { get; set; }
		public decimal InterestAmount15 { get; set; }
		public decimal InterestAmount16 { get; set; }
		public decimal InterestAmount17 { get; set; }
		public decimal InterestAmount18 { get; set; }
		public decimal InterestAmount19 { get; set; }
		public decimal InterestAmount20 { get; set; }
		public decimal InterestAmount21 { get; set; }
		public decimal InterestAmount22 { get; set; }
		public decimal InterestAmount23 { get; set; }
		public decimal InterestAmount24 { get; set; }

		public Guid MainAgreementTableGUID { get; set; }
		public Guid? WithdrawalTableGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }

		public DateTime? MainAgmStartDate { get; set; }
		public DateTime? MainAgmExpiryDate { get; set; }
		public DateTime? WithdrawalDueDate { get; set; }
		public decimal THSumInterestAmount { get; set; }
	}
}
