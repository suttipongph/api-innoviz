using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RaceItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RaceGUID { get; set; }
		public string Description { get; set; }
		public string RaceId { get; set; }
	}
}
