using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ExposureGroupByProductListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ExposureGroupByProductGUID { get; set; }
		public Guid ExposureGroupGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ExposureAmount { get; set; }
		public string DocumentConditionTemplateTable_Values { get; set; }
		public string ExposureGroup_ExposureGroupId { get; set; }

	}
}
