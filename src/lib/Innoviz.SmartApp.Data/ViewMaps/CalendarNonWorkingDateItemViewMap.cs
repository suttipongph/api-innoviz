using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CalendarNonWorkingDateItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CalendarNonWorkingDateGUID { get; set; }
		public DateTime CalendarDate { get; set; }
		public Guid CalendarGroupGUID { get; set; }
		public string Description { get; set; }
		public int HolidayType { get; set; }
	}
}
