using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProjectProgressTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProjectProgressTableGUID { get; set; }
		public string ProjectProgressId { get; set; }
		public DateTime TransDate { get; set; }
		public string Description { get; set; }
		public Guid WithdrawalTableGUID { get; set; }
	}
}
