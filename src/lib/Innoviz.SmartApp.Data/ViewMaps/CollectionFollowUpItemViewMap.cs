using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CollectionFollowUpItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CollectionFollowUpGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public decimal ChequeAmount { get; set; }
		public string ChequeBankAccNo { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public int ChequeCollectionResult { get; set; }
		public DateTime? ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public decimal CollectionAmount { get; set; }
		public DateTime CollectionDate { get; set; }
		public int CollectionFollowUpResult { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public string DocumentId { get; set; }
		public Guid MethodOfPaymentGUID { get; set; }
		public DateTime? NewCollectionDate { get; set; }
		public int ReceivedFrom { get; set; }
		public string RecipientName { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string RefId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppLine_Values { get; set; }
		public string MethodOfPayment_PaymentType { get; set; }
	}
}
