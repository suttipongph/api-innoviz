﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class SysRoleTableListViewMap : ViewCompanyBaseEntityMap
    {
        public Guid Id { get; set; }
        public string DisplayName { get; set; }

        public int SiteLoginType { get; set; }
    }
}
