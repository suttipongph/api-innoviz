using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptTableItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid ReceiptTableGUID { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public DateTime? ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceSettlementDetailGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public decimal OverUnderAmount { get; set; }
		public string ReceiptAddress1 { get; set; }
		public string ReceiptAddress2 { get; set; }
		public DateTime ReceiptDate { get; set; }
		public string ReceiptId { get; set; }
		
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAmountMST { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleBaseAmountMST { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleTaxAmountMST { get; set; }
		public DateTime TransDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string ReceiptTempTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string  Methodopayment_Values { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
		public string RefId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public string ReceiptAddress_Values { get; set; }
		

	}
}
