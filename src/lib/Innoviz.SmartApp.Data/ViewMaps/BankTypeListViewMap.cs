using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BankTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BankTypeGUID { get; set; }
		public string BankTypeId { get; set; }
		public string Description { get; set; }
	}
}
