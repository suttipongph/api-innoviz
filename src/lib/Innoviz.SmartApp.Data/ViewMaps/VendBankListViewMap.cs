using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VendBankListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VendBankGUID { get; set; }
		public string BankAccount { get; set; }
		public string BankAccountName { get; set; }
		public string BankBranch { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid VendorTableGUID { get; set; }
		public string BankGroup_BankGroupId { get; set; }
		public string BankGroup_Description { get; set; }
		public bool Primary { get; set; }

	}
}
