using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AgreementTableInfoItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AgreementTableInfoGUID { get; set; }
		public Guid? AuthorizedPersonTransBuyer1GUID { get; set; }
		public Guid? AuthorizedPersonTransBuyer2GUID { get; set; }
		public Guid? AuthorizedPersonTransBuyer3GUID { get; set; }
		public Guid? AuthorizedPersonTransCompany1GUID { get; set; }
		public Guid? AuthorizedPersonTransCompany2GUID { get; set; }
		public Guid? AuthorizedPersonTransCompany3GUID { get; set; }
		public Guid? AuthorizedPersonTransCustomer1GUID { get; set; }
		public Guid? AuthorizedPersonTransCustomer2GUID { get; set; }
		public Guid? AuthorizedPersonTransCustomer3GUID { get; set; }
		public Guid? AuthorizedPersonTypeBuyerGUID { get; set; }
		public Guid? AuthorizedPersonTypeCompanyGUID { get; set; }
		public Guid? AuthorizedPersonTypeCustomerGUID { get; set; }
		public string BuyerAddress1 { get; set; }
		public string BuyerAddress2 { get; set; }
		public string CompanyAddress1 { get; set; }
		public string CompanyAddress2 { get; set; }
		public string CompanyAltName { get; set; }
		public Guid? CompanyBankGUID { get; set; }
		public string CompanyFax { get; set; }
		public string CompanyName { get; set; }
		public string CompanyPhone { get; set; }
		public string CustomerAddress1 { get; set; }
		public string CustomerAddress2 { get; set; }
		public string GuarantorName { get; set; }
		public string GuaranteeText { get; set; }
		public Guid RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string AgreementTableInfoText_Text1 { get; set; }
		public string AgreementTableInfoText_Text2 { get; set; }
		public string AgreementTableInfoText_Text3 { get; set; }
		public string AgreementTableInfoText_Text4 { get; set; }
		public Guid? WitnessCompany1GUID { get; set; }
		public Guid? WitnessCompany2GUID { get; set; }
		public string WitnessCustomer1 { get; set; }
		public string WitnessCustomer2 { get; set; }
		public int ParentTable_ProductType { get; set; }
		public Guid? ParentTable_CustomerTableGUID { get; set; }
		public Guid? ParentTable_CreditAppTableGUID { get; set; }
		public Guid? ParentTable_BuyerTableGUID { get; set; }
		public Guid? Company_DefaultBranchGUID { get; set; }
		public string UnboundBuyerAddress { get; set; }
		public string UnboundCompanyAddress { get; set; }
		public string UnboundCustomerAddress { get; set; }
	}
}
