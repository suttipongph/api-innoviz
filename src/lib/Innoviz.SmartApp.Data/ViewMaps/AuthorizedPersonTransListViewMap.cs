using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AuthorizedPersonTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AuthorizedPersonTransGUID { get; set; }
		public int Ordering { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		public bool InActive { get; set; }
		public string AuthorizedPersonType_Values { get; set; }
		public Guid RefGUID { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
		public Guid? RefAuthorizedPersonTransGUID { get; set; }
		public int RefType { get; set; }

		#region Unbound 
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string AuthorizedPersonType_AuthorizedPersonTypeId { get; set; }
		#endregion Unbound


	}
}
