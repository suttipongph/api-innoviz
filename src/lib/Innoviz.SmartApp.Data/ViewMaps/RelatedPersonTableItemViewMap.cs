using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RelatedPersonTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RelatedPersonTableGUID { get; set; }
		public string BackgroundSummary { get; set; }
		public string CompanyName { get; set; }
		public string OperatedBy { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public DateTime? DateOfEstablishment { get; set; }
		public DateTime? DateOfExpiry { get; set; }
		public DateTime? DateOfIssue { get; set; }
		public string Email { get; set; }
		public string Extension { get; set; }
		public string Fax { get; set; }
		public Guid? GenderGUID { get; set; }
		public int IdentificationType { get; set; }
		public decimal Income { get; set; }
		public string IssuedBy { get; set; }
		public string LineId { get; set; }
		public Guid? MaritalStatusGUID { get; set; }
		public string Mobile { get; set; }
		public string Name { get; set; }
		public Guid? NationalityGUID { get; set; }
		public Guid? OccupationGUID { get; set; }
		public decimal OtherIncome { get; set; }
		public string OtherSourceIncome { get; set; }
		public decimal PaidUpCapital { get; set; }
		public string PassportId { get; set; }
		public string Phone { get; set; }
		public string Position { get; set; }
		public Guid? RaceGUID { get; set; }
		public int RecordType { get; set; }
		public decimal RegisteredCapital { get; set; }
		public Guid? RegistrationTypeGUID { get; set; }
		public string RelatedPersonId { get; set; }
		public string SpouseName { get; set; }
		public string TaxId { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		public string WorkPermitId { get; set; }
		public string Nationality_NationalityId { get; set; }
		public string Race_RaceId { get; set; }
		public int Age { get; set; }
		public Guid CustomerTable_customerTableGUID { get; set; }

		#region GetRelatedPersonTableByCreditAppTableDropDown
		public Guid CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public int Reference_RefType { get; set; }
		public bool Reference_Inactive { get; set; }
		public string Nationality_Values { get; set; }
		public string Race_Values { get; set; }

		#endregion GetRelatedPersonTableByCreditAppTableDropDown
	}
}
