using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ServiceFeeCondTemplateTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ServiceFeeCondTemplateTableGUID { get; set; }
		public string Description { get; set; }
		public int ProductType { get; set; }
		public string ServiceFeeCondTemplateId { get; set; }
	}
}
