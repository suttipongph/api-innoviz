using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestLineGUID { get; set; }
		public int LineNum { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public decimal CreditLimitLineRequest { get; set; }
		public decimal ApprovedCreditLimitLineRequest { get; set; }
		public int ApprovalDecision { get; set; }
		public Guid CreditapprequesttableGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public Guid BusinessSegmentGUID { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BusinessSegment_BusinessSegmentId { get; set; }

	}
}
