using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VendorPaymentTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VendorPaymentTransGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public DateTime? PaymentDate { get; set; }
		public string OffsetAccount { get; set; }
		public decimal TotalAmount { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string VendorTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public Guid? RefGUID { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string VendorTable_VendorId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
