using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerReceiptTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerReceiptTableGUID { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public string BuyerReceiptAddress { get; set; }
		public decimal BuyerReceiptAmount { get; set; }
		public DateTime BuyerReceiptDate { get; set; }
		public string BuyerReceiptId { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public bool Cancel { get; set; }
		public DateTime? ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public bool ShowRemarkOnly { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string CompanyBankId { get; set; }
		public string refId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }

}
}
