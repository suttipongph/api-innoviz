using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntercompanyInvoiceSettlementListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntercompanyInvoiceSettlementGUID { get; set; }
		public DateTime TransDate { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleWHTAmount { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public string IntercompanyInvoiceSettlementId { get; set; }
		
		public string DocumentStatus_Values { get; set; }
		public Guid? IntercompanyInvoiceTableGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
	
	} 
}
