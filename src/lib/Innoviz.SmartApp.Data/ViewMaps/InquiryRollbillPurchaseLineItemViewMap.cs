﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewModelsV2
{
	public class InquiryRollbillPurchaseLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PurchaseLineGUID { get; set; }
		public int LineNum { get; set; }
		public DateTime DueDate { get; set; }
		public int NumberOfRollbill { get; set; }
		public string PurchaseTable_PurchaseId { get; set; }
		public decimal OutstandingBuyerInvoiceAmount { get; set; }
		public DateTime PurchaseTable_PurchaseDate { get; set; }
		public DateTime? InterestDate { get; set; }
		public decimal LinePurchaseAmount { get; set; }
		public string DocumentStatus_Description { get; set; }
	}
}
