using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InquiryWithdrawalLineOutstandListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithdrawalLineGUID { get; set; }
		public DateTime? BillingDate { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BuyerTable_Values { get; set; }
		public DateTime CollectionDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CustomerTable_Values { get; set; }
		public DateTime DueDate { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public decimal Outstanding { get; set; }
		public decimal WithdrawalAmount { get; set; }
		public Guid WithdrawalTableGUID { get; set; }
		public string WithdrawalTable_WithdrawalId { get; set; }
		public string WithdrawalTable_Values { get; set; }
	}
}
