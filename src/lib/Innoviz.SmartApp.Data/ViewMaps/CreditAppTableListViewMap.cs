using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppTableGUID { get; set; }
		public string CreditAppId { get; set; }
		public DateTime ExpiryDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		public DateTime ApprovedDate { get; set; }
		public Guid RefCreditAppRequestTableGUID { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public DateTime? InactiveDate { get; set; }
		public DateTime StartDate { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string RefCreditAppRequestTable_Values { get; set; }
		public string RefCreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
	}
}
