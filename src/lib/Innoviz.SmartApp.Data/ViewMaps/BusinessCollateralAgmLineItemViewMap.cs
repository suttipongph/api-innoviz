using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralAgmLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralAgmLineGUID { get; set; }
		public string AccountNumber { get; set; }
		public Guid? BankGroupGUID { get; set; }
		public Guid? BankTypeGUID { get; set; }
		public Guid BusinessCollateralAgmTableGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public string BuyerName { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string BuyerTaxIdentificationId { get; set; }
		public decimal CapitalValuation { get; set; }
		public string ChassisNumber { get; set; }
		public Guid CreditAppReqBusinessCollateralGUID { get; set; }
		public DateTime? DateOfValuation { get; set; }
		public string Description { get; set; }
		public decimal GuaranteeAmount { get; set; }
		public string Lessee { get; set; }
		public string Lessor { get; set; }
		public int LineNum { get; set; }
		public string MachineNumber { get; set; }
		public string MachineRegisteredStatus { get; set; }
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		public string ProjectName { get; set; }
		public decimal Quantity { get; set; }
		public DateTime? RefAgreementDate { get; set; }
		public string RefAgreementId { get; set; }
		public string RegisteredPlace { get; set; }
		public string RegistrationPlateNumber { get; set; }
		public string TitleDeedDistrict { get; set; }
		public string TitleDeedNumber { get; set; }
		public string TitleDeedProvince { get; set; }
		public string TitleDeedSubDistrict { get; set; }
		public string Unit { get; set; }
		public string ValuationCommittee { get; set; }
		public string BusinessCollateralAgmTable_BusinessCollateralAgmId { get; set; }
		public string BusinessCollateralAgmTable_InternalBusinessCollateralAgmId { get; set; }
		public string BusinessCollateralAgmTable_Values { get; set; }
		public Guid BusinessCollateralAgmTable_CreditAppRequestTableGUID { get; set; }
		public int BusinessCollateralAgmTable_AgreementDocType { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string Product { get; set; }
		public Guid? OriginalBusinessCollateralAgreementLineGUID { get; set; }
		public Guid? OriginalBusinessCollateralAgreementTableGUID { get; set; }
		public bool Cancelled { get; set; }
		public Guid? BusinessCollateralStatusGUID { get; set; }
		public string BusinessCollateralStatus_Values { get; set; }
		public string AttachmentText { get; set; }
	}
}
