using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustomerTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustomerTableGUID { get; set; }
		public int Age { get; set; }

		public string AltName { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		public string BOTRating { get; set; }
		public Guid? BusinessSegmentGUID { get; set; }
		public Guid? BusinessSizeGUID { get; set; }
		public Guid? BusinessTypeGUID { get; set; }
		public string CompanyName { get; set; }
		public string CompanyWebsite { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid CustGroupGUID { get; set; }
		public string CustomerId { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public DateTime? DateOfEstablish { get; set; }
		public DateTime? DateOfExpiry { get; set; }
		public DateTime? DateOfIssue { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public int DueDay { get; set; }
		public Guid? ExposureGroupGUID { get; set; }
		public decimal FixedAsset { get; set; }
		public Guid? GenderGUID { get; set; }
		public Guid? GradeClassificationGUID { get; set; }
		public int IdentificationType { get; set; }
		public decimal Income { get; set; }
		public Guid? IntroducedByGUID { get; set; }
		public string IntroducedByRemark { get; set; }
		public int InvoiceIssuingDay { get; set; }
		public string IssuedBy { get; set; }
		public Guid? KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		public Guid? LineOfBusinessGUID { get; set; }
		public Guid? MaritalStatusGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string Name { get; set; }
		public Guid? NationalityGUID { get; set; }
		public Guid? NCBAccountStatusGUID { get; set; }
		public Guid? OccupationGUID { get; set; }
		public decimal OtherIncome { get; set; }
		public string OtherSourceIncome { get; set; }
		public decimal PaidUpCapital { get; set; }
		public Guid? ParentCompanyGUID { get; set; }
		public string PassportID { get; set; }
		public string Position { get; set; }
		public decimal PrivateARPct { get; set; }
		public decimal PublicARPct { get; set; }
		public Guid? RaceGUID { get; set; }
		public int RecordType { get; set; }
		public string ReferenceId { get; set; }
		public decimal RegisteredCapital { get; set; }
		public Guid? RegistrationTypeGUID { get; set; }
		public Guid? ResponsibleByGUID { get; set; }
		public string SigningCondition { get; set; }
		public string SpouseName { get; set; }
		public string TaxID { get; set; }
		public Guid? TerritoryGUID { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public Guid? WithholdingTaxGroupGUID { get; set; }
		public int WorkExperienceMonth { get; set; }
		public int WorkExperienceYear { get; set; }
		public string WorkPermitID { get; set; }
		public string LineOfBusiness_Values { get; set; }
		public string IntroducedBy_Values { get; set; }
		public string BusinessType_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid? GuarantorAgreement_GuarantorAgreementGUID { get; set; }
		public bool IsPassportIdDuplicate { get; set; }
		public bool IsTaxIdDuplicate { get; set; }
		public string PassportDuplicateEmployeeName { get; set; }
	}
}
