using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NationalityItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NationalityGUID { get; set; }
		public string Description { get; set; }
		public string NationalityId { get; set; }
	}
}
