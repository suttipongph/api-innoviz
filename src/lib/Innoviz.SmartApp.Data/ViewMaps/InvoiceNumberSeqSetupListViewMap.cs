using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceNumberSeqSetupListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InvoiceNumberSeqSetupGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public string InvoiceType_Values { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public Guid? NumberSeqTableGUID { get; set; }
		public string NumberSeqTable_Values { get; set; }
		public string NumberSeqTable_NumberSeqCode { get; set; }
		public Guid BranchGUID { get; set; }
		public string Branch_Values { get; set; }
		public string Branch_BranchId { get; set; }
		public int ProductType { get; set; }
	}
}
