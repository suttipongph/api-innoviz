using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid TaxTableGUID { get; set; }
		public string TaxCode { get; set; }
		public string Description { get; set; }
	}
}
