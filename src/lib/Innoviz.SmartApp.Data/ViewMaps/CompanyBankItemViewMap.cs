using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CompanyBankItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CompanyBankGUID { get; set; }
		public string AccountNumber { get; set; }
		public string BankAccountName { get; set; }
		public string BankBranch { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid BankTypeGUID { get; set; }
		public bool InActive { get; set; }
		public bool Primary { get; set; }
		public string BankGroup_BankGroupId { get; set; }
	}
}
