using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BlacklistStatusItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BlacklistStatusGUID { get; set; }
		public string BlacklistStatusId { get; set; }
		public string Description { get; set; }
	}
}
