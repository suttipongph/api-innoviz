using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid? ApplicationTableGUID { get; set; }
		public decimal ApprovedCreditLimitRequest { get; set; }
		public string ApproverComment { get; set; }
		public Guid? BankAccountControlGUID { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public Guid? BillingContactPersonGUID { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		public string CACondition { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public string CreditAppRequestId { get; set; }
		public int CreditAppRequestType { get; set; }
		public string CreditComment { get; set; }
		public int CreditLimitExpiration { get; set; }
		public string CreditLimitRemark { get; set; }
		public decimal CreditLimitRequest { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		public decimal CreditRequestFeeAmount { get; set; }
		public decimal CreditRequestFeePct { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		public Guid? CreditTermGUID { get; set; }
		public decimal CustomerCreditLimit { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime? ExpiryDate { get; set; }
		public Guid? ExtensionServiceFeeCondTemplateGUID { get; set; }
		public string FinancialCheckedBy { get; set; }
		public DateTime? FinancialCreditCheckedDate { get; set; }
		public decimal InterestAdjustment { get; set; }
		public Guid InterestTypeGUID { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? KYCGUID { get; set; }
		public Guid? MailingReceiptAddressGUID { get; set; }
		public string MarketingComment { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public decimal MaxRetentionAmount { get; set; }
		public decimal MaxRetentionPct { get; set; }
		public string NCBCheckedBy { get; set; }
		public DateTime? NCBCheckedDate { get; set; }
		public Guid? PDCBankGUID { get; set; }
		public Guid? ProductSubTypeGUID { get; set; }
		public int ProductType { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		public decimal PurchaseFeePct { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public Guid? ReceiptContactPersonGUID { get; set; }
		public Guid? RefCreditAppTableGUID { get; set; }
		public Guid RegisteredAddressGUID { get; set; }
		public string Remark { get; set; }
		public DateTime RequestDate { get; set; }
		public decimal SalesAvgPerMonth { get; set; }
		public string SigningCondition { get; set; }
		public DateTime? StartDate { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string UnboundRegisteredAddress { get; set; }
		public string UnboundBillingAddress { get; set; }
		public string UnboundReceiptAddress { get; set; }
		public string UnboundMailingReceiptAddress { get; set; }
		public string UnboundInvoiceAddress { get; set; }
		public int? CustomerTable_IdentificationType { get; set; }
		public int NumberOfRegisteredBuyer { get; set; }
		public decimal TotalInterestPct { get; set; }
		public string TaxIdentificationId { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string ConsortiumTable_ConsortiumId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string ConsortiumTable_Values { get; set; }
		//R02
		public decimal CustomerAllBuyerOutstanding { get; set; }
		public int ProcessInstanceId { get; set; }
		public DateTime? ApprovedDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string BusinessTypeId { get; set; }
		public string IntroducedById { get; set; }
		public string LineOfBusinessId { get; set; }
		public DateTime? DateOfEstablish { get; set; }
		#region For ReviewCreditAppRequest
		public Guid? BuyerTableGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public decimal CreditLimitLineRequest { get; set; }
		public Guid? RefCreditAppLineGUID { get; set; }
		public string RefCreditAppLine_Values { get; set; }
		public decimal BuyerCreditLimit { get; set; }
		public decimal CustomerBuyerOutstanding { get; set; }
		public string DocumentReason_Values { get; set; }
		#endregion For ReviewCreditAppRequest
		public Guid? CreditAppRequestLine_CreditAppRequestLineGuid { get; set; }
		public int CreditAppRequestLine_LineNum { get; set; }
		public Guid? CreditlimitType_ParentCreditLimitTypeGUID { get; set; }
		public string CustomerEmail { get; set; }
		public string PurchaseWithdrawalCondition { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
	}
}
