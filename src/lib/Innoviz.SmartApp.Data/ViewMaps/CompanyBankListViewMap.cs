using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CompanyBankListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CompanyBankGUID { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid BankTypeGUID { get; set; }
		public string AccountNumber { get; set; }
		public string BankAccountName { get; set; }
		public bool Primary { get; set; }
		public bool InActive { get; set; }
		public string BankGroup_Values { get; set; }
		public string BankType_Values { get; set; }
		public string BankGroup_BankGroupId { get; set; }
		public string BankType_BankTypeId { get; set; }
	}
}
