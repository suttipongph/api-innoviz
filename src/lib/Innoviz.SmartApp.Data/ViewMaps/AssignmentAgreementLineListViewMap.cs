using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AssignmentAgreementLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AssignmentAgreementLineGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string ReferenceAgreementID { get; set; }
		public int LineNum { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
	}
}
