using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustomerCreditLimitByProductItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustomerCreditLimitByProductGUID { get; set; }
		public decimal CreditLimit { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public decimal CreditLimitBalance { get; set; }
		public string CustomerTable_Values { get; set; }
	}
}
