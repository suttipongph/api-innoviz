using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProcessTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProcessTransGUID { get; set; }
		public decimal Amount { get; set; }
		public decimal AmountMST { get; set; }
		public string ARLedgerAccount { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string DocumentId { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public decimal ExchangeRate { get; set; }
		public Guid? OrigTaxTableGUID { get; set; }
		public Guid? PaymentHistoryGUID { get; set; }
		public Guid? PaymentDetailGUID { get; set; }
		public int ProcessTransType { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public string RefId { get; set; }
		public Guid? RefProcessTransGUID { get; set; }
		public string RefProcessTrans_Values { get; set; }
		public Guid? RefTaxInvoiceGUID { get; set; }
		public string RefTaxInvoice_Values { get; set; }
		public int RefType { get; set; }
		public bool Revert { get; set; }
		public string StagingBatchId { get; set; }
		public int StagingBatchStatus { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TaxAmountMST { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public DateTime TransDate { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CurrencyTable_Values { get;set;}
		public string ReasonTable_Values { get; set; }
		public string RefTexTable_Values { get; set; }
		public string TexCode_Values { get; set; }
		public string OriginalTax_Values { get; set; }
		public string History_Values { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public string InvoiceTable_Values { get; set; }

		public string ProcessTrans_Values { get; set; }
		public int SourceRefType { get; set; }
		public string SourceRefId { get; set; }
		public string PaymentDetail_Values{ get; set; }

}
}
