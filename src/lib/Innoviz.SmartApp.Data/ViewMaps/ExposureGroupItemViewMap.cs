using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ExposureGroupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ExposureGroupGUID { get; set; }
		public string Description { get; set; }
		public string ExposureGroupId { get; set; }
	}
}
