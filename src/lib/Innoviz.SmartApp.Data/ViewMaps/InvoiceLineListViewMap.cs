using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceLineListViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid InvoiceTableGUID { get; set; }
		public Guid InvoiceLineGUID { get; set; }
		public int LineNum { get; set; }
		public Guid? InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceText { get; set; }
		public decimal TotalAmountBeforeTax { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TotalAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public string InvoiceRevenueType_RevenueTypeId { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string TaxTable_taxTabled { get; set; }
		public string TaxTable_Values { get; set; }
	}
}
