using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InterestRealizedTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InterestRealizedTransGUID { get; set; }
		public decimal AccInterestAmount { get; set; }
		public DateTime AccountingDate { get; set; }
		public bool Accrued { get; set; }
		public string DocumentId { get; set; }
		public DateTime EndDate { get; set; }
		public int InterestDay { get; set; }
		public decimal InterestPerDay { get; set; }
		public int LineNum { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public DateTime StartDate { get; set; }
		public string RefID { get; set; }
		public bool Cancelled { get; set; }
		public Guid? RefProcessTransGUID { get; set; }
		public string ProcessTrans_Value { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public DateTime? ProcessTrans_TransDate { get; set; }
	}
}
