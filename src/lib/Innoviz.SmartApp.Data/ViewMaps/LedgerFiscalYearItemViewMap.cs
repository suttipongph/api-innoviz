using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class LedgerFiscalYearItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid LedgerFiscalYearGUID { get; set; }
		public string Description { get; set; }
		public DateTime EndDate { get; set; }
		public string FiscalYearId { get; set; }
		public int LengthOfPeriod { get; set; }
		public int PeriodUnit { get; set; }
		public DateTime StartDate { get; set; }
	}
}
