using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntercompanyInvoiceTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		public string IntercompanyInvoiceId { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public string DocumentId { get; set; }
		public DateTime IssuedDate { get; set; }
		public DateTime DueDate { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public decimal Balance { get; set; }
		public decimal SettleAmount { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string InvoiceRevenueType_InvoiceRevenueTypeId { get; set; }
	}
	public class IntercompanyInvoiceTableSettleInvoiceAmount : ViewCompanyBaseEntityMap
    {
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		public decimal SumSettleInvoiceAmount { get; set; }

	}
}
