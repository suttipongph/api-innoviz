using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustGroupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustGroupGUID { get; set; }
		public string CustGroupId { get; set; }
		public string Description { get; set; }
		public Guid NumberSeqTableGUID { get; set; }
	}
}
