using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RetentionConditionSetupListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RetentionConditionSetupGUID { get; set; }
		public int ProductType { get; set; }
		public int RetentionDeductionMethod { get; set; }
		public int RetentionCalculateBase { get; set; }
	}
}
