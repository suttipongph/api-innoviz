using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentStatusListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentStatusGUID { get; set; }
		public string StatusId { get; set; }
		public string Description { get; set; }
		public Guid DocumentProcessGUID { get; set; }


	}
}
