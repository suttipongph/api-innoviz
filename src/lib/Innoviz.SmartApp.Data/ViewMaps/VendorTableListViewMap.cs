using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VendorTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VendorTableGUID { get; set; }
		public string VendorId { get; set; }
		public string Name { get; set; }
		public string AltName { get; set; }
		public string ExternalCode { get; set; }
		public string TaxId { get; set; }
		public int RecordType { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid VendGroupGUID { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string VendGroup_VendGroupId { get; set; }
		public string VendGroup_Values { get; set; }
		public string Currency_Values { get; set; }

	}
}
