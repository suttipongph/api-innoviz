using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InterestTypeValueItemViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid InterestTypeValueGUID { get; set; }
		public Guid InterestTypeGUID { get; set; }
		public decimal Value { get; set; }
		public string InterestType_Values { get; set; }
	}
}
