using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ConsortiumLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ConsortiumLineGUID { get; set; }
		public string CustomerName { get; set; }
		public string OperatedBy { get; set; }
		public string Position { get; set; }
		public decimal ProportionOfShareholderPct { get; set; }
		public bool IsMain { get; set; }
		public Guid ConsortiumTableGUID { get; set; }
		public int Ordering { get; set; }
	}
}
