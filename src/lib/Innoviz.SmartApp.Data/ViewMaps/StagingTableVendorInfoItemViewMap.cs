using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class StagingTableVendorInfoItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid StagingTableVendorInfoGUID { get; set; }
		public string Address { get; set; }
		public string AddressName { get; set; }
		public string AltName { get; set; }
		public string BankAccount { get; set; }
		public string BankAccountName { get; set; }
		public string BankBranch { get; set; }
		public string BankGroupId { get; set; }
		public string CountryId { get; set; }
		public string CurrencyId { get; set; }
		public string DistrictId { get; set; }
		public string Name { get; set; }
		public string PostalCode { get; set; }
		public Guid? ProcessTransGUID { get; set; }
		public string ProvinceId { get; set; }
		public int RecordType { get; set; }
		public string SubDistrictId { get; set; }
		public string TaxBranchId { get; set; }
		public string VendGroupId { get; set; }
		public string VendorId { get; set; }
		public Guid? VendorPaymentTransGUID { get; set; }
		public string VendorTaxId { get; set; }
		public string StagingTableCompanyId { get; set; }
	}
}
