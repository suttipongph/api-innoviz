using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppReqAssignmentItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppReqAssignmentGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid AssignmentMethodGUID { get; set; }
		public decimal BuyerAgreementAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public bool IsNew { get; set; }
		public string ReferenceAgreementId { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
	}
}
