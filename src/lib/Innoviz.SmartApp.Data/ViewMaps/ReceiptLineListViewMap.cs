using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptLineListViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid ReceiptLineGUID { get; set; }
		public int LineNum { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal WHTAmount { get; set; }
		public string InvoiceTable_Values { get; set; }
		public Guid? ReceiptTableGUID { get; set; }

		public string InvoiceTable_InvoiceId { get; set; }
	}
}
