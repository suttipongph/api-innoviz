using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditLimitTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditLimitTypeGUID { get; set; }
		public bool AllowAddLine { get; set; }
		public bool BuyerMatchingAndLoanRequest { get; set; }
		public int CloseDay { get; set; }
		public int CreditLimitConditionType { get; set; }
		public int CreditLimitExpiration { get; set; }
		public int CreditLimitExpiryDay { get; set; }
		public string CreditLimitRemark { get; set; }
		public string CreditLimitTypeId { get; set; }
		public string Description { get; set; }
		public int InactiveDay { get; set; }
		public Guid? ParentCreditLimitTypeGUID { get; set; }
		public int ProductType { get; set; }
		public bool Revolving { get; set; }
		public bool ValidateBuyerAgreement { get; set; }
		public bool ValidateCreditLimitType { get; set; }
		public int BookmarkOrdering { get; set; }
	}
}
