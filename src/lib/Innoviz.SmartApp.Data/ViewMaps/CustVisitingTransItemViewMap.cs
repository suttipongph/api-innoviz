using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustVisitingTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustVisitingTransGUID { get; set; }
		public DateTime CustVisitingDate { get; set; }
		public string CustVisitingMemo { get; set; }
		public string Description { get; set; }
		public Guid? RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public Guid ResponsibleByGUID { get; set; }
	}
}
