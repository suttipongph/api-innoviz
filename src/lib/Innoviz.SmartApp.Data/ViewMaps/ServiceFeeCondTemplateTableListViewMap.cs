using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ServiceFeeCondTemplateTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ServiceFeeCondTemplateTableGUID { get; set; }
		public int ProductType { get; set; }
		public string ServiceFeeCondTemplateId { get; set; }
		public string Description { get; set; }
	}
}
