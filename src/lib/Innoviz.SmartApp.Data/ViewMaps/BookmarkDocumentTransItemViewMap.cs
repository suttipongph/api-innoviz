using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BookmarkDocumentTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BookmarkDocumentTransGUID { get; set; }
		public Guid BookmarkDocumentGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public Guid DocumentTemplateTableGUID { get; set; }
		public string ReferenceExternalId { get; set; }
		public string ReferenceExternalDate { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string RefId { get; set; }
		public int BookmarkDocument_BookmarkDocumentRefType { get; set; }
		public int BookmarkDocument_DocumentTemplateType { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
