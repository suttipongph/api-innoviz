using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerAgreementLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerAgreementLineGUID { get; set; }
		public int Period { get; set; }
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }

	}
}
