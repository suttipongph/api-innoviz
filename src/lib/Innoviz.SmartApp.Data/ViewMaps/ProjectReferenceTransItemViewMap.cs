using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProjectReferenceTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProjectReferenceTransGUID { get; set; }
		public string ProjectCompanyName { get; set; }
		public int ProjectCompletion { get; set; }
		public string ProjectName { get; set; }
		public string ProjectStatus { get; set; }
		public decimal ProjectValue { get; set; }
		public Guid? RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
	}
}
