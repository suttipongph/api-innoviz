using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class OwnershipItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid OwnershipGUID { get; set; }
		public string Description { get; set; }
		public string OwnershipId { get; set; }
	}
}
