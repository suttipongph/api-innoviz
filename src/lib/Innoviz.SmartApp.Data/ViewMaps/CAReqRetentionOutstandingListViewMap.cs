using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqRetentionOutstandingListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqRetentionOutstandingGUID { get; set; }
		public int ProductType { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public decimal MaximumRetention { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }

	}
}
