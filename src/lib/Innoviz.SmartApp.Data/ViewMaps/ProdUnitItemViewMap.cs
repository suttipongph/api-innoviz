using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProdUnitItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProdUnitGUID { get; set; }
		public string Description { get; set; }
		public string UnitId { get; set; }
	}
}
