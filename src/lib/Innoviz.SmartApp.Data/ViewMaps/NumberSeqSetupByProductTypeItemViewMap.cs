using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NumberSeqSetupByProductTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NumberSeqSetupByProductTypeGUID { get; set; }
		public Guid? CreditAppNumberSeqGUID { get; set; }
		public Guid? CreditAppRequestNumberSeqGUID { get; set; }
		public Guid? GuarantorAgreementNumberSeqGUID { get; set; }
		public Guid? InternalGuarantorAgreementNumberSeqGUID { get; set; }
		public Guid? InternalMainAgreementNumberSeqGUID { get; set; }
		public Guid? MainAgreementNumberSeqGUID { get; set; }
		public Guid? TaxInvoiceNumberSeqGUID { get; set; }
		public Guid? TaxCreditNoteNumberSeqGUID { get; set; }
		public Guid? ReceiptNumberSeqGUID { get; set; }
		public int ProductType { get; set; }
	}
}
