using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CalendarGroupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CalendarGroupGUID { get; set; }
		public string CalendarGroupId { get; set; }
		public string Description { get; set; }
		public int WorkingDay { get; set; }
	}
}
