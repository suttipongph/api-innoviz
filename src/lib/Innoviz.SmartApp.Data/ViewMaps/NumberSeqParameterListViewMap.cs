using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NumberSeqParameterListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NumberSeqParameterGUID { get; set; }
		public Guid? NumberSeqTableGUID { get; set; }
		public string ReferenceId { get; set; }
		public string NumberSeqTable_Values { get; set; }
        //public string NumberSeqTable_Description { get; set; }
        public string Numberseqtable_numberseqcode { get; set; }

    }
}
