using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AssignmentAgreementTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AssignmentAgreementTableGUID { get; set; }
		public string InternalAssignmentAgreementId { get; set; }
		public string AssignmentAgreementId { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid AssignmentMethodGUID { get; set; }
		public DateTime AgreementDate { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public int AgreementDocType { get; set; }
		public string AssignmentMethod_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public Guid? RefAssignmentAgreementTableGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public Guid? RefCreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppReqAssignmentGUID { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string Description { get; set; }

	}
}
