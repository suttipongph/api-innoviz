using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqBuyerCreditOutstandingItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqBuyerCreditOutstandingGUID { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public decimal ARBalance { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		public Guid? BillingResponsibleByGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public int LineNum { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public int ProductType { get; set; }
		public string Status { get; set; }
	}
}
