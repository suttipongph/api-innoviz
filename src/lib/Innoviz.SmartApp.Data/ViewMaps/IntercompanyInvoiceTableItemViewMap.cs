using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntercompanyInvoiceTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public string DocumentId { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public DateTime DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public Guid IntercompanyGUID { get; set; }
		public string IntercompanyInvoiceId { get; set; }
		public Guid InvoiceAddressTransGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public string InvoiceText { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public DateTime IssuedDate { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		public string Customer_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string InterCompany_InterCompanyId{ get; set; }
		public string InterCompany_Values { get; set; }
		public string  CreditAppTable_Values { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string Currency_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string TaxTable_TaxCode { get; set; }
		public string TaxTable_Values { get; set; }
		public string WithholdingTaxTable_Values { get; set; }
		public string CNReason_ReasonId { get; set; }
		public string CNReason_Values { get; set; }
		public string Dimension1_DimensionCode { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_DimensionCode { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_DimensionCode { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_DimensionCode { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_DimensionCode { get; set; }
		public string Dimension5_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public decimal Balance { get; set; }
		public decimal SettleAmount { get; set; }
		public string RefId { get; set; }
		public string FeeLedgerAccount { get; set; }
		public string UnboundInvoiceAddress { get; set; }
		public string InvoiceAddressTrans_Values { get; set; }
	}
}
