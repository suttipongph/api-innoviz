using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppTableGUID { get; set; }
		public Guid? ApplicationTableGUID { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public DateTime ApprovedDate { get; set; }
		public Guid? BankAccountControlGUID { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public Guid? BillingContactPersonGUID { get; set; }
		public string CACondition { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public string CreditAppId { get; set; }
		public int CreditLimitExpiration { get; set; }
		public string CreditLimitRemark { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		public decimal CreditRequestFeePct { get; set; }
		public Guid? CreditTermGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public DateTime? ExpectedAgreementSigningDate { get; set; }
		public DateTime ExpiryDate { get; set; }
		public Guid? ExtensionServiceFeeCondTemplateGUID { get; set; }
		public DateTime? InactiveDate { get; set; }
		public decimal InterestAdjustment { get; set; }
		public Guid InterestTypeGUID { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? MailingReceipAddressGUID { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public decimal MaxRetentionAmount { get; set; }
		public decimal MaxRetentionPct { get; set; }
		public Guid? PDCBankGUID { get; set; }
		public Guid ProductSubTypeGUID { get; set; }
		public int ProductType { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		public decimal PurchaseFeePct { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public Guid? ReceiptContactPersonGUID { get; set; }
		public Guid RefCreditAppRequestTableGUID { get; set; }
		public Guid? RegisteredAddressGUID { get; set; }
		public string Remark { get; set; }
		public decimal SalesAvgPerMonth { get; set; }
		public DateTime StartDate { get; set; }
		public decimal TotalInterestPct { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string UnboundRegisteredAddress { get; set; }
		public string UnboundBillingAddress { get; set; }
		public string UnboundReceiptAddress { get; set; }
		public string UnboundMailingReceiptAddress { get; set; }
		public string UnboundInvoiceAddress { get; set; }
		public int NumberOfRegisteredBuyer { get; set; }
		public string TaxIdentificationId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string ApplicationTable_Values { get; set; }
		public string BankAccountControl_Values { get; set; }
		public string BillingAddress_Values { get; set; }
		public string BillingContactPerson_Values { get; set; }
		public string ConsortiumTable_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string CreditTerm_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string DocumentReason_Values { get; set; }
		public string ExtensionServiceFeeCondTemplate_Values { get; set; }
		public string InterestType_Values { get; set; }
		public string InvoiceAddress_Values { get; set; }
		public string MailingReceipAddress_Values { get; set; }
		public string PDCBank_Values { get; set; }
		public string ProductSubType_Values { get; set; }
		public string ReceiptAddress_Values { get; set; }
		public string ReceiptContactPerson_Values { get; set; }
		public string RefCreditAppRequestTable_Values { get; set; }
		public string RegisteredAddress_Values { get; set; }
		public string IntroducedBy_Values { get; set; }
		public string BusinessType_Values { get; set; }
		public string LineOfBusiness_Values { get; set; }
		public DateTime? DateOfEstablish { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
		public int MainAgreementTable_AgreementDocType { get; set; }
		public decimal SumApprovedCreditLimitLine { get; set; }
		public bool CreditLimitType_BuyerMatchingAndLoanRequest { get; set; }
		public Guid? MainAgreementTable_MainAgreementTableGUID { get; set; }
		public Guid? CreditLimitType_ParentCreditLimitTypeGUID { get; set; }
		public string PurchaseWithdrawalCondition { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		public string AssignmentMethod_Values { get; set; }


	}
}
