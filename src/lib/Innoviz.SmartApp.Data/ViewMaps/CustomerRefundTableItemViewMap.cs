using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustomerRefundTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustomerRefundTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public string CustomerRefundId { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public string DocumentReason_Values { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public decimal ExchangeRate { get; set; }
		public Guid OperReportSignatureGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public int ProductType { get; set; }
		public decimal RetentionAmount { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public decimal ServiceFeeAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SuspenseAmount { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public DateTime TransDate { get; set; }
		public decimal NetAmount { get; set; }
		public string Remark { get; set; }
	}
}
