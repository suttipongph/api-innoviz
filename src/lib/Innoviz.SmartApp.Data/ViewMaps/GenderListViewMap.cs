using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GenderListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GenderGUID { get; set; }
		public string Description { get; set; }
		public string GenderId { get; set; }
	}
}
