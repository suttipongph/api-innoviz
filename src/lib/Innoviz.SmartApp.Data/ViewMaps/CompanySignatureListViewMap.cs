using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CompanySignatureListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CompanySignatureGUID { get; set; }
		public Guid EmployeeTableGUID { get; set; }
		public Guid BranchGUID { get; set; }
		public int Ordering { get; set; }
		public int RefType { get; set; }
		public string Branch_Values { get; set; }
		public string Branch_BranchId { get; set; }
		public string EmployeeTable_EmployeeId { get; set; }
		public string EmployeeTable_Name { get; set; }
		
	}
}
