using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerAgreementLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerAgreementLineGUID { get; set; }
		public bool AlreadyInvoiced { get; set; }
		public decimal Amount { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		public string Description { get; set; }
		public DateTime? DueDate { get; set; }
		public int Period { get; set; }
		public string Remark { get; set; }
		public string BuyerAgreementTable_Values { get; set; }

	}
}
