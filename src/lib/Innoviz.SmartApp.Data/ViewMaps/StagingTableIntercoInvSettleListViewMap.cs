using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class StagingTableIntercoInvSettleListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid StagingTableIntercoInvSettleGUID { get; set; }
		public string IntercompanyInvoiceSettlementId { get; set; }
		public int InterfaceStatus { get; set; }
		public DateTime InvoiceDate { get; set; }
		public DateTime DueDate { get; set; }
		public string MethodOfPaymentId { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public string FeeLedgerAccount { get; set; }
		public string CustomerId { get; set; }
		public string Name { get; set; }
		public string InterfaceStagingBatchId { get; set; }
		public Guid IntercompanyInvoiceSettlementGUID { get; set; }
	}
}
