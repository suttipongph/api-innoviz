using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceSettlementDetailItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InvoiceSettlementDetailGUID { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public decimal BalanceAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public string DocumentId { get; set; }
		public int InterestCalculationMethod { get; set; }
		public decimal InvoiceAmount { get; set; }
		public DateTime? InvoiceDueDate { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public Guid? InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal PurchaseAmountBalance { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? RefReceiptTempPaymDetailGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public decimal RetentionAmountAccum { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAssignmentAmount { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public decimal SettlePurchaseAmount { get; set; }
		public decimal SettleReserveAmount { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public decimal WHTAmount { get; set; }
		public bool WHTSlipReceivedByBuyer { get; set; }
		public string InvoiceType_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string AssignmentAgreementTable_Values { get; set; }
		#region InvoiceTable
		public string InvoiceTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public decimal InvoiceTable_WHTBalance { get; set; }
		public decimal InvoiceTable_SettleTaxBalance { get; set; }
		public decimal InvoiceTable_WHTAmount { get; set; }
		public int? InvoiceTable_ProductType { get; set; }
		public bool InvoiceTable_ProductInvoice { get; set; }
		#endregion InvoiceTable
		public int ReceivedFrom { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }

	}
}
