using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorTypeGUID { get; set; }
		public string Description { get; set; }
		public string GuarantorTypeId { get; set; }
	}
}
