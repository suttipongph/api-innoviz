using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VerificationTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VerificationTableGUID { get; set; }
		public string VerificationId { get; set; }
		public string Description { get; set; }
		public DateTime VerificationDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
	}
}
