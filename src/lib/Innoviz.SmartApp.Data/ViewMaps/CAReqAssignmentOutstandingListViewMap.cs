using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqAssignmentOutstandingListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqAssignmentOutstandingGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string AssignmentAgreement_Values { get; set; }
		public string AssignmentAgreement_InternalAssignmentAgreementId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
	}
}
