using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RaceListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RaceGUID { get; set; }
		public string RaceId { get; set; }
		public string Description { get; set; }
	}
}
