using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.ViewModelsV2;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CompanySignatureItemViewMap : ViewCompanyBaseEntityMap
	{
        public Guid CompanySignatureGUID { get; set; }
        public int RefType { get; set; }
        public int Ordering { get; set; }
        public Guid EmployeeTableGUID { get; set; }
        public string Branch_Values { get; set; }
        public string EmployeeTable_EmployeeId { get; set; }
        public string EmployeeTable_Name { get; set; }
        public Guid? BranchGUID { get; set; }
        public Guid? DefaultBranchGUID { get; set; }
    }
}
