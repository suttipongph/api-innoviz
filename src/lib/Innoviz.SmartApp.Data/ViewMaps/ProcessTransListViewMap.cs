using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProcessTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProcessTransGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int StagingBatchStatus { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public int ProductType { get; set; }
		public DateTime TransDate { get; set; }
		public int ProcessTransType { get; set; }
		public decimal Amount { get; set; }
		public decimal TaxAmount { get; set; }
		public bool Revert { get; set; }
		public string StagingBatchId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string RefId { get; set; }
		public string RefType { get; set; }
		public string InvoiceTable_InvoiceId { get; set; }
		public string CreditAppTable_CreditappId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string RefGUID { get; set; }
	}
}
