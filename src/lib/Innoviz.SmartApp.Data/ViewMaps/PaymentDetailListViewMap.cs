using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PaymentDetailListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PaymentDetailGUID { get; set; }
		public int PaidToType { get; set; }
		public Guid? InvoiceTypeGUID { get; set; }
		public bool SuspenseTransfer { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public string InvoiceType_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string VendorTable_Values { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string VendorTable_VendorId { get; set; }
		public Guid? RefGUID { get; set; }
	}
}
