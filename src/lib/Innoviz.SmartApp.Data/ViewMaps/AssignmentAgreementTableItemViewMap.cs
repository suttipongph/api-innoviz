using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AssignmentAgreementTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AssignmentAgreementTableGUID { get; set; }
		public string AcceptanceName { get; set; }
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public string AssignmentAgreementId { get; set; }
		public Guid AssignmentMethodGUID { get; set; }
		public string BuyerName { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public Guid? CustBankGUID { get; set; }
		public string CustomerAltName { get; set; }
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public string InternalAssignmentAgreementId { get; set; }
		public Guid? RefAssignmentAgreementTableGUID { get; set; }
		public string ReferenceAgreementId { get; set; }
		public string Remark { get; set; }
		public DateTime? SigningDate { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string AssignmentProductDescription { get; set; }
		public string ToWhomConcern { get; set; }
		public string CancelAuthorityPersonID { get; set; }
		public string CancelAuthorityPersonName { get; set; }
		public string CancelAuthorityPersonAddress { get; set; }
		public string CustRegisteredLocation { get; set; }
		public Guid? RefCreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppReqAssignmentGUID { get; set; }
		public string DocumentReason_Values { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public string DocumentProcess_ProcessId { get; set; }
		public string AssignmentAgreementTable_InternalAssignmentAgreementId { get; set; }
		public string AssignmentAgreementTable_AssignmentAgreementId { get; set; }
	}
}
