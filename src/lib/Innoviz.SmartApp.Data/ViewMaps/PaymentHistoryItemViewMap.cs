using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PaymentHistoryItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid PaymentHistoryGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public bool Cancel { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid CustTransGUID { get; set; }
		public DateTime? DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public Guid? InvoiceSettlementDetailGUID { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public Guid? OrigTaxTableGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public decimal PaymentAmountMST { get; set; }
		public decimal PaymentBaseAmount { get; set; }
		public decimal PaymentBaseAmountMST { get; set; }
		public DateTime PaymentDate { get; set; }
		public decimal PaymentTaxAmount { get; set; }
		public decimal PaymentTaxAmountMST { get; set; }
		public decimal PaymentTaxBaseAmount { get; set; }
		public decimal PaymentTaxBaseAmountMST { get; set; }
		public int ProductType { get; set; }
		public Guid? ReceiptTableGUID { get; set; }
		public int ReceivedFrom { get; set; }
		public DateTime ReceivedDate { get; set; }
		public decimal WHTAmount { get; set; }
		public decimal WHTAmountMST { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string Currency_Values { get; set; }

		public string CustomerTable_Values { get; set; }

		public string InvoiceTable_Values { get; set; }

		public string WithholdingTaxTable_Values { get; set; }
		public string ReceiptTable_Values { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
		public string OrigTaxTable_Values { get; set; }

		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefID { get; set; }
	}
}
