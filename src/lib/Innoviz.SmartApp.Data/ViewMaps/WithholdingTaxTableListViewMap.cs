using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithholdingTaxTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithholdingTaxTableGUID { get; set; }
		public string WHTCode { get; set; }
		public string Description { get; set; }
	}
}
