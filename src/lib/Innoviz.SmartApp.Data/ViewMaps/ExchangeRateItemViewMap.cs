using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ExchangeRateItemViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid ExchangeRateGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid? HomeCurrencyGUID { get; set; }
		public string HomeCurrencyId{ get; set; }
		public string CurrencyId { get; set; }
		public decimal Rate { get; set; }
		public string Currency_Values { get; set; }
		public string HomeCurrency_Values { get; set; }

	}
}
