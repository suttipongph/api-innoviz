﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class NumberSequences
    {
        public Guid Key { get; set; }
        public Guid NumberSeqTableGUID { get; set; }
        public Guid? CompanyGUID { get; set; }
        public Guid? BranchGUID { get; set; }
        public string GeneratedId { get; set; }
    }
}
