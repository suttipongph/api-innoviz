using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceRevenueTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public string RevenueTypeId { get; set; }
		public string Description { get; set; }
		public decimal FeeAmount { get; set; }
		public Guid? FeeTaxGUID { get; set; }
		public decimal FeeTaxAmount { get; set; }
		public Guid? FeeWHTGUID { get; set; }
		public Guid? ServiceFeeRevenueTypeGUID { get; set; }
		public int AssetFeeType { get; set; }
		public string WithholdingTaxTable_Values { get; set; }
		public string TaxTable_Values { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string WithholdingTaxTable_WHTCode { get; set; }
		public string TaxTable_TaxCode { get; set; }
		public string InvoiceRevenueType_revenueTypeId { get; set; }
		public int ServiceFeeCategory { get; set; }
	}
}
