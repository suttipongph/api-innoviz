using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressPostalCodeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressPostalCodeGUID { get; set; }
		public Guid AddressSubDistrictGUID { get; set; }
		public string PostalCode { get; set; }
		public string AddressSubDistrict_Value { get; set; }
		
	}
}
