using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VendGroupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VendGroupGUID { get; set; }
		public string Description { get; set; }
		public string VendGroupId { get; set; }
	}
}
