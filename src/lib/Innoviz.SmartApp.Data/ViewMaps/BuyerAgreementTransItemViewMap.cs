using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerAgreementTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerAgreementTransGUID { get; set; }
		public Guid BuyerAgreementLineGUID { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string RefId { get; set; }
		public DateTime? BuyerAgreementLine_DueDate { get; set; }
		public decimal BuyerAgreementLine_Amount { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
		public string BuyerAgreementLine_Description { get; set; }
	}
}
