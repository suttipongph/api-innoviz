using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NumberSeqSegmentItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NumberSeqSegmentGUID { get; set; }
		public Guid NumberSeqTableGUID { get; set; }
		public int Ordering { get; set; }
		public int SegmentType { get; set; }
		public string SegmentValue { get; set; }
		public string numberSeqTable_Values { get; set; }
	}
}
