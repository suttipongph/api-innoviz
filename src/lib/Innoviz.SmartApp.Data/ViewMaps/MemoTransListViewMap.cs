using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MemoTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MemoTransGUID { get; set; }
		public string Topic { get; set; }
		public string Memo { get; set; }
		public Guid RefGUID { get; set; }

	}
}
