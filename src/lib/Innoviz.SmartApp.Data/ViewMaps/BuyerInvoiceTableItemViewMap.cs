using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerInvoiceTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerInvoiceTableGUID { get; set; }
		public decimal Amount { get; set; }
		public Guid? BuyerAgreementLineGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public string BuyerInvoiceId { get; set; }
		public Guid CreditAppLineGUID { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime InvoiceDate { get; set; }
		public string Remark { get; set; }
		public string BuyerId { get; set; }
		public string CreditAppId { get; set; }
		public int LineNum { get; set; }
		public Guid PurchaseLineGUID { get; set; }
		public Guid PurchaseTableGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid CreditAppLine_CreditAppTableGUID { get; set; }
		public Guid CreditAppLine_BuyerTableGUID { get; set; }
		public Guid CreditAppTable_CustomerTableGUID { get; set; }
	}
}
