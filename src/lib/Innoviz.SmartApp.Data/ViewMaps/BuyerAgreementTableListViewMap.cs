using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerAgreementTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerAgreementTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string BuyerAgreementId { get; set; }
		public string ReferenceAgreementID { get; set; }
		public string Description { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public decimal buyerAgreementAmount { get; set; }
		public Guid BuyerTableGUID { get; set; }

	}
}
