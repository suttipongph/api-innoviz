using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DepartmentItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DepartmentGUID { get; set; }
		public string DepartmentId { get; set; }
		public string Description { get; set; }
	}
}
