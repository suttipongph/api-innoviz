using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PurchaseTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PurchaseTableGUID { get; set; }
		public bool AdditionalPurchase { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public bool DiffChequeIssuedName { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public decimal OverCreditBuyer { get; set; }
		public decimal OverCreditCA { get; set; }
		public decimal OverCreditCALine { get; set; }
		public int ProductType { get; set; }
		public DateTime PurchaseDate { get; set; }
		public string PurchaseId { get; set; }
		public Guid? ReceiptTempTableGUID { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionFixedAmount { get; set; }
		public decimal RetentionPct { get; set; }
		public bool Rollbill { get; set; }
		public decimal SumPurchaseAmount { get; set; }
		public decimal TotalInterestPct { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public decimal NetPaid { get; set; }
		public int ProcessInstanceId { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid OperReportSignatureGUID { get; set; }
		public string Remark { get; set; }
		public string DocumentReason_Values { get; set; }
	}
}
