using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class OwnershipListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid OwnershipGUID { get; set; }
		public string OwnershipId { get; set; }
		public string Description { get; set; }
	}
}
