using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxInvoiceTableListViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid TaxInvoiceTableGUID { get; set; }
		public string TaxInvoiceId { get; set; }
		public DateTime IssuedDate { get; set; }
		public DateTime DueDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string CustomerName { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal InvoiceAmount { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public int TaxInvoiceRefType { get; set; }
		public Guid TaxInvoiceRefGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid MethodOfPaymentGUID { get; set; }
		public Guid CNReasonGUID { get; set; }
		public Guid Dimension1GUID { get; set; }
		public Guid Dimension2GUID { get; set; }
		public Guid Dimension3GUID { get; set; }
		public Guid Dimension4GUID { get; set; }
		public Guid Dimension5GUID { get; set; }
		public Guid RefTaxInvoiceGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string Currency_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string CNReason_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string RefTaxInvoice_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
	}
}
