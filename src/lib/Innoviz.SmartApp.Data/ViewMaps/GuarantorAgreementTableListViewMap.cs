using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorAgreementTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorAgreementTableGUID { get; set; }
		public string InternalGuarantorAgreementId { get; set; }
		public string GuarantorAgreementId { get; set; }
		public int AgreementDocType { get; set; }
		public string Description { get; set; }
		public DateTime AgreementDate { get; set; }
		public DateTime ExpiryDate { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public bool Affiliate { get; set; }
		public Guid? ParentCompanyGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public int creditApp_ProductType { get; set; }
		public string company_CompanyId { get; set; }
		public string MainAgreementTable_MainAgreementId { get; set; }
		public string MainAgreementTable_MainAgreementGUID { get; set; }
		public Guid? RefGuarantorAgreementTableGUID { get; set; }

	}
}
