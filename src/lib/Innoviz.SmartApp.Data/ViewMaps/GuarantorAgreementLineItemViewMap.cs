using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorAgreementLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorAgreementLineGUID { get; set; }
		public int Age { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public DateTime? DateOfIssue { get; set; }
		public string GuaranteeLand { get; set; }
		public Guid GuarantorAgreementTableGUID { get; set; }
		public Guid GuarantorTransGUID { get; set; }
		public string Name { get; set; }
		public Guid? NationalityGUID { get; set; }
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		public string PassportId { get; set; }
		public string PrimaryAddress1 { get; set; }
		public string PrimaryAddress2 { get; set; }
		public Guid? RaceGUID { get; set; }
		public string TaxId { get; set; }
		public string WorkPermitId { get; set; }
		public string GuarantorAgreementTable_InternalGuarantorAgreementId { get; set; }
		public string GuarantorAgreementTable_GuarantorAgreementId { get; set; }
		public bool Affiliate { get; set; }
		public string CreditAppTable_CreditAppTableGUID { get; set; }
		public int Mainagreement_AgreementDoctype { get; set; }
		public string GuarantorTransTable_RefGUID { get; set; }
		public bool GuarantorTransTable_InActive { get; set; }
		public string CreditAppRequestTable_creditAppRequestTableGUID { get; set; }
		public decimal MaximumGuaranteeAmount { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid MainAgreement_CreditAppTableGUID { get; set; }

	}
}
