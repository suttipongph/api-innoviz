using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RegistrationTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RegistrationTypeGUID { get; set; }
		public string Description { get; set; }
		public string RegistrationTypeId { get; set; }
	}
}
