using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressPostalCodeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressPostalCodeGUID { get; set; }
		public string PostalCode { get; set; }
		public Guid AddressSubDistrictGUID { get; set; }
		public string AddressSubDistrict_Value { get; set; }
		


	}
}
