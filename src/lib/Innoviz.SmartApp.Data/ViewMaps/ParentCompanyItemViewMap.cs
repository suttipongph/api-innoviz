using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ParentCompanyItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ParentCompanyGUID { get; set; }
		public string Description { get; set; }
		public string ParentCompanyId { get; set; }
	}
}
