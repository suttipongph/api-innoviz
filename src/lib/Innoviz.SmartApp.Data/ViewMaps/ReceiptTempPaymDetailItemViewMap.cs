using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptTempPaymDetailItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ReceiptTempPaymDetailGUID { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public Guid? ChequeTableGUID { get; set; }
		public Guid MethodOfPaymentGUID { get; set; }
		public decimal ReceiptAmount { get; set; }
		public Guid ReceiptTempTableGUID { get; set; }
		public string TransferReference { get; set; }
		public int MethodOfPayment_PaymentType { get; set; }
		public int ReceiptTempTable_ReceivedFrom { get; set; }
		public Guid? ReceiptTempTable_CustomerTableGUID { get; set; }
		public Guid? ReceiptTempTable_BuyerTableGUID { get; set; }
		public string CompanyBank_Values { get; set; }
		public int ReceiptTempTable_ProductType { get; set; }
		public string ReceiptTempTable_ReceiptTempId { get; set; }
	}
}
