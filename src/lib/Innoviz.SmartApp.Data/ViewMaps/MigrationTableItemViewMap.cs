﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class MigrationTableItemViewMap : ViewBaseEntityMap
    {
        public Guid MigrationTableGUID { get; set; }
        public string GroupName { get; set; }
        public int GroupOrder { get; set; }
        public string MigrationName { get; set; }
        public int MigrationOrder { get; set; }
        public string MigrationResult { get; set; }
        public string PackageName { get; set; }
    }
}
