using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentTypeGUID { get; set; }
		public string Description { get; set; }
		public string DocumentTypeId { get; set; }
	}
}
