using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ApplicationTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ApplicationTableGUID { get; set; }
	}
}
