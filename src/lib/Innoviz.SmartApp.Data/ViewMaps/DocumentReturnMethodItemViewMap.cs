using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentReturnMethodItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentReturnMethodGUID { get; set; }
		public string Description { get; set; }
		public string DocumentReturnMethodId { get; set; }
	}
}
