using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class FinancialStatementTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid FinancialStatementTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string Description { get; set; }
		public int Ordering { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
		public string RefId { get; set; }
	}
}
