using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ServiceFeeConditionTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ServiceFeeConditionTransGUID { get; set; }
		public int Ordering { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public string Description { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public bool Inactive { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public string InvoiceRevenueType_revenueTypeId { get; set; }
		public Guid RefGUID { get; set; }
		public int InvoiceRevenueType_ProductType { get; set; }
		public int InvoiceRevenueType_ServiceFeeCategory { get; set; }
	}
}
