using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ContactPersonTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ContactPersonTransGUID { get; set; }
		public bool InActive { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
		public string RefId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string ContactPersonTrans_Values { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_Position { get; set; }
		public string RelatedPersonTable_RelatedPersonId { get; set; }

	}
}
