using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditTypeGUID { get; set; }
		public string CreditTypeId { get; set; }
		public string Description { get; set; }
	}
}
