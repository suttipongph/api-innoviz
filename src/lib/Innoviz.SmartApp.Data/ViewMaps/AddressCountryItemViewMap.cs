using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressCountryItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressCountryGUID { get; set; }
		public string CountryId { get; set; }
		public string Name { get; set; }
	}
}
