using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqCreditOutStandingListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqCreditOutStandingGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ReserveToBeRefund { get; set; }
		public Guid? CreditLimitTypeGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public DateTime? AsOfDate { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public decimal CreditLimitBalance { get; set; }
		public decimal ARBalance { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_CustomerId { get; set; }

	}
}
