using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class FreeTextInvoiceTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid FreeTextInvoiceTableGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public DateTime DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string FreeTextInvoiceId { get; set; }
		public string InvoiceAddress1 { get; set; }
		public string InvoiceAddress2 { get; set; }
		public Guid InvoiceAddressGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public string InvoiceText { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public DateTime IssuedDate { get; set; }
		public string MailingInvoiceAddress1 { get; set; }
		public string MailingInvoiceAddress2 { get; set; }
		public Guid MailingInvoiceAddressGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public int ProductType { get; set; }
		public string Remark { get; set; }
		public decimal TaxAmount { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public decimal WHTAmount { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public string TaxValueError { get; set; }
		public string WithholdingTaxValueError { get; set; }
		public string InvoiceTable_Values { get; set; }
	}
}
