using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralAgmTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralAgmTableGUID { get; set; }
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		public string BusinessCollateralAgmId { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CreditLimitTypeGUID { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public string CustomerAltName { get; set; }
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public string InternalBusinessCollateralAgmId { get; set; }
		public int ProductType { get; set; }
		public Guid? RefBusinessCollateralAgmTableGUID { get; set; }
		public DateTime? SigningDate { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string ConsortiumTable_ConsortiumId { get; set; }
		public string BusinessCollateralAgmTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public decimal DBDRegistrationAmount { get; set; }
		public DateTime? DBDRegistrationDate { get; set; }
		public string DBDRegistrationDescription { get; set; }
		public string DBDRegistrationId { get; set; }
		public string AttachmentRemark { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string ConsortiumTable_Values { get; set; }
		public string Remark { get; set; }
		public string DocumentReason_Values { get; set; }
		public decimal TotalBusinessCollateralValue { get; set; }
	}
}
