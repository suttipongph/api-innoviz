using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithholdingTaxGroupListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithholdingTaxGroupGUID { get; set; }
		public string Description { get; set; }
		public string WHTGroupId { get; set; }
	}
}
