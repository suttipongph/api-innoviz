using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditTypeGUID { get; set; }
		public string CreditTypeId { get; set; }
		public string Description { get; set; }
	}
}
