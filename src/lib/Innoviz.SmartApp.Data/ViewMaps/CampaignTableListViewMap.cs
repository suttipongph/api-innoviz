using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CampaignTableListViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid CampaignTableGUID { get; set; }
	}
}
