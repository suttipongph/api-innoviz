using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorAgreementTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorAgreementTableGUID { get; set; }
		public bool Affiliate { get; set; }
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int AgreementExtension { get; set; }
		public int AgreementYear { get; set; }
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public DateTime ExpiryDate { get; set; }
		public string GuarantorAgreementId { get; set; }
		public string InternalGuarantorAgreementId { get; set; }
		public Guid MainAgreementTableGUID { get; set; }
		public Guid? ParentCompanyGUID { get; set; }
		public Guid? RefGuarantorAgreementTableGUID { get; set; }
		public DateTime? SigningDate { get; set; }
		public Guid? WitnessCompany1GUID { get; set; }
		public Guid? WitnessCompany2GUID { get; set; }
		public string WitnessCustomer1 { get; set; }
		public string WitnessCustomer2 { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string MainAgreementTable_MainAgreementId { get; set; }
		public int creditApp_ProductType { get; set; }
		public string company_CompanyId { get; set; }
		public int HaveLine { get; set; }
		public string GuarantorAgreementTable_RefValue { get; set; }
		public string GuarantorAgreementTable_RefInternalValue { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public bool  IsBookmarkComplete { get; set; }
		public Guid? MainAgreement_ConsortiumGUID { get; set; }
		public Guid? CustomerTable_CustomerTableGUID { get; set; }
		public string CustomerTable_Value { get; set; }
		public Guid? CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTable_CreditAppTableGUID { get; set; }
		public string Remark { get; set; }
		public string DocumentReason_Values { get; set; }
		public string MainAgreementTable_StatusId { get; set; }
		public int MainAgreementTable_ProductType { get; set; }
		public string MainAgreementTable_CustomerName { get; set; }
	}
}
