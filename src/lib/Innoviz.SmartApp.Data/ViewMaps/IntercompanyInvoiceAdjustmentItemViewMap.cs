using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntercompanyInvoiceAdjustmentItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntercompanyInvoiceAdjustmentGUID { get; set; }
		public decimal Adjustment { get; set; }
		public Guid DocumentReasonGUID { get; set; }
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		public decimal OriginalAmount { get; set; }
		public string IntercompanyInvoice_Values { get; set; }
		public string DocumentReason_Values { get; set; }

	}
}
