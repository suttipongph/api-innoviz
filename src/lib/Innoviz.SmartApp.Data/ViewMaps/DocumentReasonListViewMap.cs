using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentReasonListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentReasonGUID { get; set; }
		public string Description { get; set; }
		public string ReasonId { get; set; }
		public int RefType { get; set; }
	}
}
