using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ServiceFeeCondTemplateLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ServiceFeeCondTemplateLineGUID { get; set; }
		public int Ordering { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public string Description { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public string InvoiceRevenueType_Values { get; set; }
		public Guid? ServiceFeeCondTemplateTableGUID { get; set; }

		
	}
}
