using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CompanyParameterListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CompanyParameterGUID { get; set; }
	}
}
