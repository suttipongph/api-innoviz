﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class AttachmentItemViewMap : ViewBaseEntityMap
    {
        public Guid AttachmentGUID { get; set; }
        public Guid? RefGUID { get; set; }
        public int RefType { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileDescription { get; set; }
        public string Base64Data { get; set; }
        public string RefId { get; set; }
        public int RefId_Int { get; set; }
        public DateTime? RefId_DateTime { get; set; }
    }
}
