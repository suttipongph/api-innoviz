using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceTableListViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid InvoiceTableGUID { get; set; }
		public string InvoiceId { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public DateTime IssuedDate { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_DocumentId { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? BuyerInvoiceTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string InvoiceType_Values { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BuyerInvoiceTable_Values { get; set; }
		public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
		public string DocumentId { get; set; }
		public DateTime DueDate { get; set; }
		public Guid? RefGUID { get; set; }

		#region UnboundField
		public int CustTransStatus { get; set; }
		public int RefType { get; set; }
		#endregion
	}
}
