using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustTransListViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid CustTransGUID { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public int CustTransStatus { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public int ProductType { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public DateTime? DueDate { get; set; }
		public Guid CurrencyGUID { get; set; }
		public decimal TransAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceTable_InvoiceId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string InvoiceType_Values { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string Currency_Values { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string CreditApp_Values { get; set; }
		public string CreditApp_CreditAppId { get; set; }
		public string CustomerId { get; set; }
		public string CustomerName { get; set; }
	}
}
