using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InquiryPurchaseLineOutstandingListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PurchaseLineGUID { get; set; }
		public string PurchaseTable_Values { get; set; }
		public Guid PurchaseTableGUID { get; set; }
		public string PurchaseTable_PurchaseId { get; set; }
		public string CustomerTable_Values { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_Values { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal Outstanding { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime? BillingDate { get; set; }
		public DateTime CollectionDate { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
	}
}
