using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CurrencyItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CurrencyGUID { get; set; }
		public string CurrencyId { get; set; }
		public string Name { get; set; }
		public decimal ExchangeRate_Rate { get; set; }
	}
}
