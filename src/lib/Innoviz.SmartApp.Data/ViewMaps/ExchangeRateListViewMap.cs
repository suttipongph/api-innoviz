using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ExchangeRateListViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid ExchangeRateGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid? HomeCurrencyGUID { get; set; }
		public decimal Rate { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string HomeCurrency_CurrencyId { get; set; }

	}
}
