﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class GenBookmarkDocumentFileParamViewMap
    {
        public string Base64Data { get; set; }
        public int DocumentTemplateType { get; set; }
        public Guid RefGUID { get; set; }
        public Guid BookmarkDocumentTransGUID { get; set; }
    }
}
