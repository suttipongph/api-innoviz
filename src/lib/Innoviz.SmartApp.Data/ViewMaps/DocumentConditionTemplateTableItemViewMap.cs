using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentConditionTemplateTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentConditionTemplateTableGUID { get; set; }
		public string Description { get; set; }
		public string DocumentConditionTemplateTableId { get; set; }
	}
}
