using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithholdingTaxValueItemViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid WithholdingTaxValueGUID { get; set; }
		public decimal Value { get; set; }
		public Guid WithholdingTaxTableGUID { get; set; }
		public string WithholdingTaxTable_Values { get; set; }
	}
}
