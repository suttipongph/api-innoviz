using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InterestRealizedTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InterestRealizedTransGUID { get; set; }
		public int LineNum { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public DateTime AccountingDate { get; set; }
		public int InterestDay { get; set; }
		public decimal AccInterestAmount { get; set; }
		public string DocumentId { get; set; }
		public Guid? RefGUID { get; set; }
		public bool Cancelled { get; set; }
		public Guid? RefProcessTransGUID { get; set; }
		public string ProcessTrans_Value { get; set; }
	}
}
