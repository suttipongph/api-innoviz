﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class AttachmentListViewMap : ViewBaseEntityMap
    {
        public Guid AttachmentGUID { get; set; }
        public Guid? RefGUID { get; set; }
        public int RefType { get; set; }
        public string FileName { get; set; }
        public string FileDescription { get; set; }
    }
}
