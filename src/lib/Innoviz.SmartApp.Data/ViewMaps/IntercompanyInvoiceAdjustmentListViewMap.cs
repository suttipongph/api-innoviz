using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntercompanyInvoiceAdjustmentListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntercompanyInvoiceAdjustmentGUID { get; set; }
		public decimal OriginalAmount { get; set; }
		public decimal Adjustment { get; set; }
		public Guid DocumentReasonGUID { get; set; }
		public string DocumentReason_Values { get; set; }
		public Guid? IntercompanyInvoiceTableGUID { get; set; }
		public string DocumentReason_DocumentReasonId { get; set; }
	}
}
