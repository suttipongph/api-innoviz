using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MainAgreementTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MainAgreementTableGUID { get; set; }
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public int Agreementextension { get; set; }
		public int AgreementYear { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public string BuyerAgreementDescription { get; set; }
		public string BuyerAgreementReferenceId { get; set; }
		public string BuyerName { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? ConsortiumTableGUID { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid? CreditLimitTypeGUID { get; set; }
		public string CustomerAltName { get; set; }
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string Description { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime? ExpiryDate { get; set; }
		public string InternalMainAgreementId { get; set; }
		public string MainAgreementId { get; set; }
		public decimal MaxInterestPct { get; set; }
		public int ProductType { get; set; }
		public string RefMainAgreementId { get; set; }
		public Guid? RefMainAgreementTableGUID { get; set; }
		public string Remark { get; set; }
		public DateTime? SigningDate { get; set; }
		public DateTime? StartDate { get; set; }
		public decimal TotalInterestPct { get; set; }
		public DateTime? WithdrawalTable_DueDate { get; set; }
		public Guid? WithdrawalTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string ConsortiumTable_Values { get; set; }
		public string MainAgreementTable_Values { get; set; }
		public int ServiceFeeCategory { get; set; }
		public string DocumentReason_Values { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
	}
}
