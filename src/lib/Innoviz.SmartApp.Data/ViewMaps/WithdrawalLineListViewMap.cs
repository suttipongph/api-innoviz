using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithdrawalLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithdrawalLineGUID { get; set; }
		public int WithdrawalLineType { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime InterestDate { get; set; }
		public int InterestDay { get; set; }
		public decimal InterestAmount { get; set; }
		public Guid WithdrawalTableGUID { get; set; }
		public int LineNum { get; set; }
	}
}
