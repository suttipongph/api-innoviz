using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class LedgerDimensionListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid LedgerDimensionGUID { get; set; }
		public string Description { get; set; }
		public int Dimension { get; set; }
		public string DimensionCode { get; set; }
	}
}
