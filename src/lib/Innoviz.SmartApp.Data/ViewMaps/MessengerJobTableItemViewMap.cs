using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MessengerJobTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MessengerJobTableGUID { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string ContactPersonExtension { get; set; }
		public string ContactPersonMobile { get; set; }
		public string ContactPersonName { get; set; }
		public string ContactPersonPhone { get; set; }
		public string ContactPersonPosition { get; set; }
		public int ContactTo { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public decimal FeeAmount { get; set; }
		public DateTime? JobDate { get; set; }
		public string JobDetail { get; set; }
		public string JobId { get; set; }
		public DateTime? JobStartTime { get; set; }
		public DateTime? JobEndTime { get; set; }
		public Guid JobTypeGUID { get; set; }
		public bool Map { get; set; }
		public Guid? MessengerTableGUID { get; set; }
		public int Priority { get; set; }
		public int ProductType { get; set; }
		public Guid? RefCreditAppLineGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? RefMessengerJobTableGUID { get; set; }
		public int RefType { get; set; }
		public Guid RequestorGUID { get; set; }
		public int Result { get; set; }
		public string ResultRemark { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string RefID { get; set; }
		public string MessengerTable_Value { get; set; }

		public string MessengerJob_Value { get; set; }
		public string JobType_Value { get; set; }
		public string DocumentStatus_Value { get; set; }
		public string ContactName { get; set; }
		public string CustomerTable_Value { get; set; }

		public string BuyerTable_Value { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public string ContactPersonDepartment { get; set; }
		public decimal ServiceFeeAmount { get; set; }
		public string RefCreditAppLine_Values { get; set; }
		public decimal ExpenseAmount { get; set; }
	}
}