using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqRetentionOutstandingItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqRetentionOutstandingGUID { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public decimal MaximumRetention { get; set; }
		public int ProductType { get; set; }
		public decimal RemainingAmount { get; set; }
	}
}
