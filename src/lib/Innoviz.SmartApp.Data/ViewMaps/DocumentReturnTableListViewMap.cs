using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentReturnTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentReturnTableGUID { get; set; }
		public string DocumentReturnId { get; set; }
		public Guid RequestorGUID { get; set; }
		public int ContactTo { get; set; }
		public string ContactPersonName { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public Guid DocumentReturnMethodGUID { get; set; }
		public DateTime? ExpectedReturnDate { get; set; }
		public string EmployeeTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentReturnMethod_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentReturnMethod_DocumentReturnMethodId { get; set; }
		public string EmployeeTable_EmployeeTableId { get; set; }
	}
}
