using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustomerRefundTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustomerRefundTableGUID { get; set; }
		public string CustomerRefundId { get; set; }
		public string Description { get; set; }
		public DateTime TransDate { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public decimal SuspenseAmount { get; set; }
		public Guid CurrencyGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string Currency_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string Currency_CurrencyId { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public decimal NetAmount { get; set; }
	}
}
