using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VendorPaymentTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VendorPaymentTransGUID { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? Dimension1 { get; set; }
		public Guid? Dimension2 { get; set; }
		public Guid? Dimension3 { get; set; }
		public Guid? Dimension4 { get; set; }
		public Guid? Dimension5 { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public string OffsetAccount { get; set; }
		public DateTime? PaymentDate { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public decimal TaxAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public decimal TotalAmount { get; set; }
		public Guid? VendorTableGUID { get; set; }
	    public string CreditAppTable_Values { get; set; }
		public string VendorTable_VendorId { get; set; }
		public string VendorTable_Values { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string Dimension1_Values { get; set; }
        public string Dimension2_Values { get; set; }
        public string Dimension3_Values { get; set; }
        public string Dimension4_Values { get; set; }
        public string Dimension5_Values { get; set; }
		public string RefId { get; set; }
		public string VendorTaxInvoiceId { get; set; }
	}
}
