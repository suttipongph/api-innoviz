using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PurchaseLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PurchaseLineGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		public decimal AssignmentAmount { get; set; }
		public DateTime? BillingDate { get; set; }
		public decimal BillingFeeAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public decimal BuyerInvoiceAmount { get; set; }
		public Guid BuyerInvoiceTableGUID { get; set; }
		public Guid? BuyerPDCTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public bool ClosedForAdditionalPurchase { get; set; }
		public DateTime CollectionDate { get; set; }
		public Guid CreditAppLineGUID  { get; set; }
		public Guid? CustomerPDCTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public DateTime DueDate { get; set; }
		public decimal InterestAmount { get; set; }
		public DateTime? InterestDate { get; set; }
		public int InterestDay { get; set; }
		public decimal InterestRefundAmount { get; set; }
		public int LineNum { get; set; }
		public decimal LinePurchaseAmount { get; set; }
		public decimal LinePurchasePct { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public decimal NetPurchaseAmount { get; set; }
		public int NumberOfRollbill { get; set; }
		public DateTime? OriginalInterestDate { get; set; }
		public Guid? OriginalPurchaseLineGUID { get; set; }
		public decimal OutstandingBuyerInvoiceAmount  { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal PurchaseFeeAmount { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		public decimal PurchaseFeePct { get; set; }
		public Guid? PurchaseLineInvoiceTableGUID  { get; set; }
		public decimal PurchasePct { get; set; }
		public Guid PurchaseTableGUID { get; set; }
		public decimal ReceiptFeeAmount { get; set; }
		public Guid? RefPurchaseLineGUID { get; set; }
		public decimal ReserveAmount { get; set; }
		public decimal RetentionAmount { get; set; }
		public decimal RollbillInterestAmount { get; set; }
		public int RollbillInterestDay { get; set; }
		public decimal RollbillInterestPct { get; set; }
		public Guid? RollbillPurchaseLineGUID { get; set; }
		public decimal SettleBillingFeeAmount { get; set; }
		public decimal SettleInterestAmount { get; set; }
		public decimal SettlePurchaseFeeAmount { get; set; }
		public decimal SettleReceiptFeeAmount { get; set; }
		public decimal SettleRollBillInterestAmount { get; set; }
		public string PurchaseTable_Values { get; set; }
		public Guid PurchaseTable_CustomerTableGUID { get; set; }
		public Guid PurchaseTable_CreditAppTableGUID { get; set; }
		public DateTime PurchaseTable_PurchaseDate { get; set; }
		public bool CreditLitmitType_ValidateBuyerAgreement { get; set; }
		public string CreditAppLine_Values { get; set; }
		public decimal CreditAppLine_MaxPurchasePct { get; set; }
		public string PurchaseTable_PurchaseId { get; set; }
		public bool PurchaseTable_AdditionalPurchase { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string RefPurchaseLine_Values { get; set; }
		public string PurchaseLineInvoiceTable_Values { get; set; }
		public bool RollBill { get; set; }
		public decimal RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public DateTime? CustomerPDCDate { get; set; }
		public bool AssignmentMethod_ValidateAssignmentBalance { get; set; }
		public string AssignmentMethod_AssignmentMethodGUID { get; set; }
	}
}
