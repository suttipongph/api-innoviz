using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerTableGUID { get; set; }
		public string BuyerId { get; set; }
		public string BuyerName { get; set; }
		public string PassportId { get; set; }
		public string TaxId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid DocumentStatusGUID{ get; set; }

	}
}
