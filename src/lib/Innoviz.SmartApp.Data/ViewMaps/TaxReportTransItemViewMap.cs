using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxReportTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid TaxReportTransGUID { get; set; }
		public decimal Amount { get; set; }
		public string Description { get; set; }
		public int Month { get; set; }
		public Guid? RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public int Year { get; set; }
	}
}
