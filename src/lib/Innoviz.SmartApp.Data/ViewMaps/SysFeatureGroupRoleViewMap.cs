﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class SysFeatureGroupRoleViewMap: ViewBaseEntityMap
    {
        public Guid SysFeatureGroupRoleGUID { get; set; }
        public Guid SysFeatureGroupGUID { get; set; }
        public string GroupId { get; set; }
        public int Create { get; set; }
        public int Read { get; set; }
        public int Update { get; set; }
        public int Delete { get; set; }
        public int Action { get; set; }
        public Guid RoleGUID { get; set; }
        public Guid SysRoleTable_CompanyGUID { get; set; }
        public string SysRoleTable_Displayname { get; set; }
        public int SysRoleTable_SiteLoginType { get; set; }
        public int SysFeatureGroup_FeatureType { get; set; }
    }
}
