using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentConditionTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentConditionTransGUID { get; set; }
		public Guid DocumentTypeGUID { get; set; }
		public bool Mandatory { get; set; }
		public string DocumentType_Values { get; set; }
		public string DocumentType_DocumentTypeId { get; set; }
		public Guid RefGUID { get; set; }
		public int DocConVerifyType { get; set; }
		public Guid? RefDocumentConditionTransGUID { get; set; }
	}
}
