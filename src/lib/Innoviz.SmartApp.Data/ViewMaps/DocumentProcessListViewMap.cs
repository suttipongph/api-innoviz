using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentProcessListViewMap : ViewBaseEntityMap
	{
		public Guid DocumentProcessGUID { get; set; }
		public string ProcessId { get; set; }
		public string Description { get; set; }

	}
}
