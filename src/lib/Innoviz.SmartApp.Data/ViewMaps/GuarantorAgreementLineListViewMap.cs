using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorAgreementLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorAgreementLineGUID { get; set; }
		public int Ordering { get; set; }
		public Guid GuarantorTransGUID { get; set; }
		public string TaxId { get; set; }
		public string PassportId { get; set; }
		public string Name { get; set; }
		public string OperatedBy { get; set; }
		public string GuarantorTrans_Values { get; set; }
		public string GuarantorAgreementTable_InternalGuarantorAgreementId { get; set; }
		public string GuarantorAgreementTableGUID { get; set; }
		public bool IsAffiliateCreated { get; set; }
	}
}
