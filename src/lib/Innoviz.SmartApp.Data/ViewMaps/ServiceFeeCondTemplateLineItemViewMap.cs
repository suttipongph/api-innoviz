using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ServiceFeeCondTemplateLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ServiceFeeCondTemplateLineGUID { get; set; }
		public decimal AmountBeforeTax { get; set; }
		public string Description { get; set; }
		public Guid InvoiceRevenueTypeGUID { get; set; }
		public int Ordering { get; set; }
		public Guid? ServiceFeeCondTemplateTableGUID { get; set; }
		public string serviceFeeCondTemplateTable_Value { get; set; }
		public int ProductType { get; set; }
	}
}
