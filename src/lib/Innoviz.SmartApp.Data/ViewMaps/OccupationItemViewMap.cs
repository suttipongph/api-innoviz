using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class OccupationItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid OccupationGUID { get; set; }
		public string Description { get; set; }
		public string OccupationId { get; set; }
	}
}
