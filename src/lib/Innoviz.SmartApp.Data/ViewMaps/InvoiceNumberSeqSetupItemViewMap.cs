using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceNumberSeqSetupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InvoiceNumberSeqSetupGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public int ProductType { get; set; }
		public Guid? NumberSeqTableGUID { get; set; }
		public Guid BranchGUID { get; set; }
	}
}
