using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InquiryPurchaseLineOutstandingItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid PurchaseLineGUID { get; set; }
		public string PurchaseTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerInvoiceTable_BuyerInvoiceId { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal PurchaseAmount { get; set; }
		public decimal Outstanding { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public DateTime DueDate { get; set; }
		public DateTime? ChequeTable_ChequeDate { get; set; }
		public DateTime? BillingDate { get; set; }
		public DateTime CollectionDate { get; set; }
		public string CreditAppTableBankAccountControl_BankAccountName { get; set; }
		public string CreditAppTablePDCBank_BankAccountName { get; set; }
		public int LineNum { get; set; }
		public string CreditAppLine_Values { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
	}
}
