using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestLineGUID { get; set; }
		public bool AcceptanceDocument { get; set; }
		public string AcceptanceDocumentDescription { get; set; }
		public int ApprovalDecision { get; set; }
		public decimal ApprovedCreditLimitLineRequest { get; set; }
		public string ApproverComment { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		public string AssignmentMethodRemark { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public Guid? BillingContactPersonGUID { get; set; }
		public int BillingDay { get; set; }
		public string BillingDescription { get; set; }
		public string BillingRemark { get; set; }
		public Guid? BillingResponsibleByGUID { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		public decimal BuyerCreditLimit { get; set; }
		public Guid? BuyerCreditTermGUID { get; set; }
		public string BuyerProduct { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public string CreditComment { get; set; }
		public decimal CreditLimitLineRequest { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		public string CreditTermDescription { get; set; }
		public decimal CustomerContactBuyerPeriod { get; set; }
		public decimal CustomerRequest { get; set; }
		public decimal InsuranceCreditLimit { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? KYCSetupGUID { get; set; }
		public int LineNum { get; set; }
		public Guid? MailingReceiptAddressGUID { get; set; }
		public string MarketingComment { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public int MethodOfBilling { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string PaymentCondition { get; set; }
		public int PurchaseFeeCalculateBase { get; set; }
		public decimal PurchaseFeePct { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public Guid? ReceiptContactPersonGUID { get; set; }
		public int ReceiptDay { get; set; }
		public string ReceiptDescription { get; set; }
		public string ReceiptRemark { get; set; }
		public string BlacklistStatus_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string UnboundBillingAddress { get; set; }
		public string UnboundReceiptAddress { get; set; }
		public string UnboundMailingReceiptAddress { get; set; }
		public string UnboundInvoiceAddress { get; set; }
		public string CreditAppRequestTable_StatusId { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BusinessType_Values { get; set; }
		public string LineOfBusiness_Values { get; set; }
		public DateTime? DateOfEstablish { get; set; }
		public int? CreditAppRequestTable_ProductType { get; set; }
		//R02
		public decimal RemainingCreditLoanRequest { get; set; }
		public decimal AllCustomerBuyerOutstanding { get; set; }
		public decimal CustomerBuyerOutstanding { get; set; }
		public Guid? RefCreditAppLineGUID { get; set; }
		public string RefCreditAppLine_Values { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public string LineCondition { get; set; }

	}
}
