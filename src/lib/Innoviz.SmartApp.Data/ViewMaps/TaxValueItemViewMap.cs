using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxValueItemViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid TaxValueGUID { get; set; }
		public Guid TaxTableGUID { get; set; }
		public decimal Value { get; set; }
		public string TaxTable_Values { get; set; }

	}
}
