using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentTemplateTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentTemplateTableGUID { get; set; }
		public string Base64Data { get; set; }
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
		public string FileName { get; set; }
		public string FilePath { get; set; }
		public string TemplateId { get; set; }

	}
}
