﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class CALineOutstandingViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppTableGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppId { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		public string CreditLimitTypeId { get; set; }
		public string CreditLimitType_Values { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerId { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerId { get; set; }
		public int LineNum { get; set; }
		public int ProductType { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public decimal CreditLimitBalance { get; set; }
		public decimal ARBalance { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime ExpiryDate { get; set; }
		public string StartDate_Values { get; set; }
		public string ExpiryDate_Values { get; set; }
		public decimal CreditDeductAmount { get; set; }
		public Guid CreditAppLineGUID { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		public Guid? BillingResponsibleByGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string Status { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public int RefType { get; set; }

	}
}
