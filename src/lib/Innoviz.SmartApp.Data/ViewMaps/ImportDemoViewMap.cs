﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class ImportDemoViewMap : ViewBaseEntityMap
    {
        public string DemoId { get; set; }
        public string DemoGUID { get; set; }
        public decimal DemoValue { get; set; }
        public string DemoDate { get; set; }
        public string DemoDateTime { get; set; }

        public static IReadOnlyDictionary<string, string> GetMapper()
        {
            // ("ExcelColumnName", "ModelColumnName")
            IDictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Demo ID", "DemoId");
            dict.Add("Demo Value", "DemoValue");
            dict.Add("Demo Date", "DemoDate");
            dict.Add("Demo Date & Time", "DemoDateTime");
            return new ReadOnlyDictionary<string, string>(dict);
        }
    }
}
