using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerTableGUID { get; set; }
		public string AltName { get; set; }
		public Guid? BlacklistStatusGUID { get; set; }
		public Guid BusinessSegmentGUID { get; set; }
		public Guid? BusinessSizeGUID { get; set; }
		public Guid? BusinessTypeGUID { get; set; }
		public string BuyerId { get; set; }
		public string BuyerName { get; set; }
		public Guid? CreditScoringGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public DateTime? DateOfEstablish { get; set; }
		public DateTime? DateOfExpiry { get; set; }
		public DateTime? DateOfIssue { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public int FixedAssets { get; set; }
		public int IdentificationType { get; set; }
		public string IssuedBy { get; set; }
		public Guid? KYCSetupGUID { get; set; }
		public int Labor { get; set; }
		public Guid? LineOfBusinessGUID { get; set; }
		public decimal PaidUpCapital { get; set; }
		public string PassportId { get; set; }
		public string ReferenceId { get; set; }
		public decimal RegisteredCapital { get; set; }
		public string TaxId { get; set; }
		public string WorkPermitId { get; set; }
		public string BlacklistStatus_Values { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BusinessType_Values { get; set; }
		public string LineOfBusiness_Values { get; set; }
		public Guid CreditAppTable_CreditAppTableGUID { get; set; }
		public string BuyerAgreementTable_Description { get; set; }
		public string BuyerAgreementTable_ReferenceAgreementID { get; set; }
		public DateTime CreditAppLine_ExpiryDate { get; set; }
		public Guid CreditAppLine_CreditAppLineGUID { get; set; }
		public decimal PurchaseLine_PurchaseFeeCalculateBase { get; set; }
		public Guid? MethodOfPayment_MethodOfPaymentGUID { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string CreditAppLine_Values { get; set; }
		public decimal CreditAppLine_MaxPurchasePct { get; set; }
		public decimal CreditAppLine_PurchaseFeePct { get; set; }
	}
}
