using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptTableListViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid ReceiptTableGUID { get; set; }
		public string ReceiptId { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public DateTime ReceiptDate { get; set; }
		public DateTime TransDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string ReceiptTempTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? InvoiceSettlementDetailGUID { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
	}
}
