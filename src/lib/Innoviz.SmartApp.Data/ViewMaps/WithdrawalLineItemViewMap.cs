using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithdrawalLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithdrawalLineGUID { get; set; }
		public DateTime? BillingDate { get; set; }
		public Guid? BuyerPDCTableGUID { get; set; }
		public bool ClosedForTermExtension { get; set; }
		public DateTime CollectionDate { get; set; }
		public Guid? CustomerPDCTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public DateTime DueDate { get; set; }
		public decimal InterestAmount { get; set; }
		public DateTime InterestDate { get; set; }
		public int InterestDay { get; set; }
		public DateTime StartDate { get; set; }
		public Guid? WithdrawalLineInvoiceTableGUID { get; set; }
		public int WithdrawalLineType { get; set; }
		public Guid WithdrawalTableGUID { get; set; }
		public decimal CustTrans_TransAmount { get; set; }
		public decimal CustTrans_SettleAmount { get; set; }
		public Guid? WithdrawalTable_OriginalWithdrawalTableGUID { get; set; }
		public Guid WithdrawalTable_CustomerTableGUID { get; set; }
		public Guid? WithdrawalTable_BuyerTableGUID { get; set; }
		public DateTime WithdrawalTable_WithdrawalDate { get; set; }
		public string WithdrawalTable_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public int LineNum { get; set; }
		public DateTime? CustomerPDCDate { get; set; }
		public decimal CustomerPDCAmount { get; set; }
		public string WithdrawalTable_StatusId { get; set; }
	}
}
