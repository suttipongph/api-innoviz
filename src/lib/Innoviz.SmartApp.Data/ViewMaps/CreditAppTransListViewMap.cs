using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppTransGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public string DocumentId { get; set; }
		public int ProductType { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public DateTime TransDate { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal CreditDeductAmount { get; set; }
		public int RefType { get; set; }
		public string RefType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppLine_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CreditAppLine_LineNum { get; set; }
	}
}
