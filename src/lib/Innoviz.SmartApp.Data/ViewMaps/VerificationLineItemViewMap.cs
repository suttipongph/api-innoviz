using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VerificationLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VerificationLineGUID { get; set; }
		public string VerificationID { get; set; }
		public bool Pass { get; set; }
		public string Remark { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public Guid VerificationTableGUID { get; set; }
		public Guid VerificationTypeGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string VerificationTable_Values { get; set; }
	}
}
