using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MaritalStatusItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MaritalStatusGUID { get; set; }
		public string Description { get; set; }
		public string MaritalStatusId { get; set; }
	}
}
