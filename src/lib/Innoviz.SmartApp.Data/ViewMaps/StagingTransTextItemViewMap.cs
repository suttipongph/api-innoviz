using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class StagingTransTextItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid StagingTransTextGUID { get; set; }
		public int ProcessTransType { get; set; }
		public string TransText { get; set; }
	}
}
