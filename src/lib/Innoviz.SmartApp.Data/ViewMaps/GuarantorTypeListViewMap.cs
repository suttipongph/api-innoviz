using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorTypeGUID { get; set; }
		public string GuarantorTypeId { get; set; }
		public string Description { get; set; }
	}
}
