using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralSubTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		public int AgreementOrdering { get; set; }
		public string AgreementRefText { get; set; }
		public string BusinessCollateralSubTypeId { get; set; }
		public string Description { get; set; }
		public string BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public bool DebtorClaims { get; set; }
	}
}
