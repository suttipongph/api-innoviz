using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MessengerJobTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MessengerJobTableGUID { get; set; }
		public string JobId { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string JobEndTime { get; set; }
		public string ContactName { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime? JobDate { get; set; }
		public string JobStartTime { get; set; }
		public Guid JobTypeGUID { get; set; }
		public Guid? MessengerTableGUID { get; set; }
		public int Priority { get; set; }
		public int Result { get; set; }
		public Guid RequestorGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string JobType_Values { get; set; }
		public string MessengerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string EmployeeTable_Values { get; set; }
		public int RefType { get; set; }
		public Guid? RefGUID { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string EmployeeTable_EmployeeId { get; set; }
		public string MessengerTable_MessengerId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string JobType_JobTypeId { get; set; }

	}
}
