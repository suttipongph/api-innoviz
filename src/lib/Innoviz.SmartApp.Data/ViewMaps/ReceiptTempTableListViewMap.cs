using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptTempTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ReceiptTempTableGUID { get; set; }
		public string ReceiptTempId { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public DateTime ReceiptDate { get; set; }
		public DateTime TransDate { get; set; }
		public int ProductType { get; set; }
		public int ReceivedFrom { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public decimal ReceiptAmount { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string Currency_CurrencyId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public int ReceiptTempRefType { get; set; }
	}
}
