using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RetentionTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RetentionTransGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public DateTime TransDate { get; set; }
		public decimal Amount { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public int RefType { get; set; }
		public string DocumentId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public Guid? RefGUID { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BuyerAgreementTable_ReferenceAgreementID { get; set; }
		#region Outstanding
		public decimal MaximumRetention { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public decimal RemainingAmount { get; set; }
		public string CreditAppId { get; set; }
		#endregion outstanding

	}
}
