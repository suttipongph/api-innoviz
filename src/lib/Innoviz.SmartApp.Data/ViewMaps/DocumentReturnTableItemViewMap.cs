using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentReturnTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentReturnTableGUID { get; set; }
		public DateTime? ActualReturnDate { get; set; }
		public string Address { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string ContactPersonName { get; set; }
		public int ContactTo { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public string DocumentReturnId { get; set; }
		public Guid DocumentReturnMethodGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime? ExpectedReturnDate { get; set; }
		public string PostAcknowledgementNo { get; set; }
		public string PostNumber { get; set; }
		public string ReceivedBy { get; set; }
		public string Remark { get; set; }
		public Guid RequestorGUID { get; set; }
		public Guid? ReturnByGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public Guid? OriginalDocumentStatusGUID { get; set; }

	}
}
