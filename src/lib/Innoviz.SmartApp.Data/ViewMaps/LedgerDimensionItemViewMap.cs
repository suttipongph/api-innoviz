using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class LedgerDimensionItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid LedgerDimensionGUID { get; set; }
		public string Description { get; set; }
		public int Dimension { get; set; }
		public string DimensionCode { get; set; }
	}
}
