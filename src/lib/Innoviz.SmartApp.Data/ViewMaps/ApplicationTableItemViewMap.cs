using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ApplicationTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ApplicationTableGUID { get; set; }
		public Guid AgreementTypeGUID { get; set; }
		public DateTime? ApplicationDate { get; set; }
		public string ApplicationId { get; set; }
		public Guid? BillingAddressGUID { get; set; }
		public int CalculationType { get; set; }
		public Guid? CreditResultGUID { get; set; }
		public string CreditResultNotes { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? DeliveryAddressGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime? ExpectedExecuteDate { get; set; }
		public DateTime? ExpirationDate { get; set; }
		public string FlexInfo1 { get; set; }
		public string FlexInfo2 { get; set; }
		public Guid? FlexSetupGUID1 { get; set; }
		public Guid? FlexSetupGUID2 { get; set; }
		public DateTime? FollowupDate { get; set; }
		public Guid? IntroducedByGUID { get; set; }
		public Guid? InvoiceAddressGUID { get; set; }
		public Guid? LanguageGUID { get; set; }
		public Guid? LeaseSubTypeGUID { get; set; }
		public Guid LeaseTypeGUID { get; set; }
		public Guid? MailingInvoiceAddressGUID { get; set; }
		public Guid? MailingReceiptAddressGUID { get; set; }
		public Guid? NCBAccountStatusGUID { get; set; }
		public string NCBNotes { get; set; }
		public Guid? OriginalAgreementGUID { get; set; }
		public Guid PaymentFrequencyGUID { get; set; }
		public int ProcessInstanceId { get; set; }
		public Guid? PropertyAddressGUID { get; set; }
		public Guid? ProspectTableGUID { get; set; }
		public string ReasonRemark { get; set; }
		public Guid? ReceiptAddressGUID { get; set; }
		public int RefAgreementExtension { get; set; }
		public Guid? RegisterAddressGUID { get; set; }
		public string Remark { get; set; }
		public Guid? ResponsibleBy { get; set; }
		public bool SalesLeaseBack { get; set; }
		public Guid? WorkingAddressGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
