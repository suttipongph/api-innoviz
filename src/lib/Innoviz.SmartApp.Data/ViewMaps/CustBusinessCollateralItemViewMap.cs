using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustBusinessCollateralItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustBusinessCollateralGUID { get; set; }
		public string AccountNumber { get; set; }
		public Guid? BankGroupGUID { get; set; }
		public Guid? BankTypeGUID { get; set; }
		public Guid BusinessCollateralAgmLineGUID { get; set; }
		public Guid? BusinessCollateralStatusGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public string BuyerName { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string BuyerTaxIdentificationId { get; set; }
		public bool Cancelled { get; set; }
		public decimal CapitalValuation { get; set; }
		public string ChassisNumber { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public string CustBusinessCollateralId { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public DateTime? DateOfValuation { get; set; }
		public decimal DBDRegistrationAmount { get; set; }
		public DateTime? DBDRegistrationDate { get; set; }
		public string DBDRegistrationDescription { get; set; }
		public string DBDRegistrationId { get; set; }
		public string Description { get; set; }
		public decimal GuaranteeAmount { get; set; }
		public string Lessee { get; set; }
		public string Lessor { get; set; }
		public string MachineNumber { get; set; }
		public string MachineRegisteredStatus { get; set; }
		public string Ownership { get; set; }
		public int PreferentialCreditorNumber { get; set; }
		public string ProjectName { get; set; }
		public decimal Quantity { get; set; }
		public DateTime? RefAgreementDate { get; set; }
		public string RefAgreementId { get; set; }
		public string RegisteredPlace { get; set; }
		public string RegistrationPlateNumber { get; set; }
		public string Remark { get; set; }
		public string TitleDeedDistrict { get; set; }
		public string TitleDeedNumber { get; set; }
		public string TitleDeedProvince { get; set; }
		public string TitleDeedSubDistrict { get; set; }
		public string Unit { get; set; }
		public string ValuationCommittee { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string BusinessCollateralAgmLine_Values { get; set; }
		public string BusinessCollateralSubType_Values { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public string BankType_Values { get; set; }
		public string BankGroup_Values { get; set; }
		public string BusinessCollateralAgmId { get; set; }
		public string BuyerId { get; set; }
	}
}
