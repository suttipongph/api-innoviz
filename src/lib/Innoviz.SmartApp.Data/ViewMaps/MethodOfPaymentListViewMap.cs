using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MethodOfPaymentListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MethodOfPaymentGUID { get; set; }
		public string MethodOfPaymentId { get; set; }
		public string Description { get; set; }
		public Guid? CompanyBankGUID { get; set; }
		public int PaymentType { get; set; }
		public string PaymentRemark { get; set; }
		public string CompanyBank_Values { get; set; }
		public string CompanyBank_AccountNumber { get; set; }
		public int AccountType { get; set; }
		public string AccountNum { get; set; }
	}
}
