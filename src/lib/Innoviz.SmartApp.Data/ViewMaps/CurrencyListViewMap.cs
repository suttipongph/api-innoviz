using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CurrencyListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CurrencyGUID { get; set; }
		public string CurrencyId { get; set; }
		public string Name { get; set; }
	}
}
