using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntroducedByListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntroducedByGUID { get; set; }
		public string Description { get; set; }
		public string IntroducedById { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public string VendorTable_VendorId { get; set; }
		public string VendorTable_Name { get; set; }
		public string VendorTable_Values { get; set; }
	}
}
