﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class ActionHistoryItemViewMap : ViewCompanyBaseEntityMap
    {
        public Guid ActionHistoryGUID { get; set; }
        public string SerialNo { get; set; }
        public string WorkflowName { get; set; }
        public string ActivityName { get; set; }
        public string ActionName { get; set; }
        public string Comment { get; set; }
        public Guid RefGUID { get; set; }
    }
}
