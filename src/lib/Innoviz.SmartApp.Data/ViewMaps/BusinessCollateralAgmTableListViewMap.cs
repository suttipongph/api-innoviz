using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralAgmTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralAgmTableGUID { get; set; }
		public string InternalBusinessCollateralAgmId { get; set; }
		public string BusinessCollateralAgmId { get; set; }
		public string Description { get; set; }
		public int AgreementDocType { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public DateTime? SigningDate { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Description { get; set; }
		public int ProductType { get; set; }
		public Guid? RefBusinessCollateralAgmTableGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
	}
}
