using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxReportTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid TaxReportTransGUID { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		public string Description { get; set; }
		public decimal Amount { get; set; }
		public Guid? RefGUID { get; set; }
	}
}
