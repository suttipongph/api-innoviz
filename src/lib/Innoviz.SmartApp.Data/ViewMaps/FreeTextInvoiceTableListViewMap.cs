using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class FreeTextInvoiceTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid FreeTextInvoiceTableGUID { get; set; }
		public string FreeTextInvoiceId { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public DateTime IssuedDate { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string DocumentStatus_Values { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
	}
}
