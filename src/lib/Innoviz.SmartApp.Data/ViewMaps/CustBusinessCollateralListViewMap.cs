using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustBusinessCollateralListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustBusinessCollateralGUID { get; set; }
		public string CustBusinessCollateralId { get; set; }
		public string Description { get; set; }
		public DateTime? DBDRegistrationDate { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public string BuyerName { get; set; }
		public bool Cancelled { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public string BusinessCollateralSubType_Values { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string BusinessCollateralType_BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralSubType_BusinessCollateralSubTypeId { get; set; }
	}
}
