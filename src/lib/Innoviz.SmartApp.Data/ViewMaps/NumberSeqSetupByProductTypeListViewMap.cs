using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NumberSeqSetupByProductTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NumberSeqSetupByProductTypeGUID { get; set; }
		public int ProductType { get; set; }
		public Guid? CreditAppRequestNumberSeqGUID { get; set; }
		public Guid? CreditAppNumberSeqGUID { get; set; }
		public Guid? InternalMainAgreementNumberSeqGUID { get; set; }
		public Guid? MainAgreementNumberSeqGUID { get; set; }
		public Guid? InternalGuarantorAgreementNumberSeqGUID { get; set; }
		public Guid? GuarantorAgreementNumberSeqGUID { get; set; }
		public Guid? TaxInvoiceNumberSeqGUID { get; set; }
		public Guid? TaxCreditNoteNumberSeqGUID { get; set; }
		public Guid? ReceiptNumberSeqGUID { get; set; }
		public Guid? NumberSeqTableGUID { get; set; }
		public string NumberSeqCode { get; set; }
		public string CreditAppRequestNumber_Values { get; set; }
		public string CreditAppNumberSeq_Values { get; set; }
		public string InternalMainAgreementNumber_Values { get; set; }
		public string MainAgreementNumber_Values { get; set; }
		public string InternalGuarantorAgreementNumber_Values { get; set; }
		public string GuarantorAgreementNumber_Values { get; set; }
		public string TaxInvoiceNumberSeq_Values { get; set; }
		public string TaxCreditNoteNumberSeq_Values { get; set; }
		public string ReceiptNumberSeq_Values { get; set; }
	}
}
