using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithdrawalTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithdrawalTableGUID { get; set; }
		public string WithdrawalId { get; set; }
		public string Description { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public DateTime WithdrawalDate { get; set; }
		public DateTime DueDate { get; set; }
		public Guid CreditTermGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditTerm_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditTerm_CreditTermId { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid? OriginWithdrawalTableGUID { get; set; }
		public bool TermExtension { get; set; }
		public int NumberOfExtension { get; set; }
	}
}
