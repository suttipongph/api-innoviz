using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqCreditOutStandingItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqCreditOutStandingGUID { get; set; }
		public decimal AccumRetentionAmount { get; set; }
		public decimal ApprovedCreditLimit { get; set; }
		public decimal ARBalance { get; set; }
		public DateTime? AsOfDate { get; set; }
		public Guid CAReqCreditOutStanding { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public decimal CreditLimitBalance { get; set; }
		public Guid? CreditLimitTypeGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ReserveToBeRefund { get; set; }
	}
}
