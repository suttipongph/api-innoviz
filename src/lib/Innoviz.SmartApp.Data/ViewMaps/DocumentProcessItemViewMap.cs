using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentProcessItemViewMap : ViewBaseEntityMap
	{
		public Guid DocumentProcessGUID { get; set; }
		public string Description { get; set; }
		public string ProcessId { get; set; }
	}
}
