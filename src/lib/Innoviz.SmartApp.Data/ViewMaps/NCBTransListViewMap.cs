using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NCBTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NCBTransGUID { get; set; }
		public Guid CreditTypeGUID { get; set; }
		public decimal CreditLimit { get; set; }
		public decimal OutstandingAR { get; set; }
		public decimal MonthlyRepayment { get; set; }
		public DateTime EndDate { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid NCBAccountStatusGUID { get; set; }
		public string CreditType_Values { get; set; }
		public string BankGroup_Values { get; set; }
		public string NCBAccountStatus_Values { get; set; }
		public string CreditType_CreditTypeId { get; set; }
		public string BankGroup_BankGroupId { get; set; }
		public string NcbAccountStatus_NCBAccStatusId { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
	}
}
