using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorAgreementLineAffiliateItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorAgreementLineAffiliateGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid GuarantorAgreementLineGUID { get; set; }
		public Guid MainAgreementTableGUID { get; set; }
		public int Ordering { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string GuarantorAgreementLine_Values { get; set; }
		public string Customer_CustomerGUID { get; set; }
		public string MainAgreement_ProductType { get; set; }
		public string GuarantorAgreementTable_ParentCompanyGUID { get; set; }
	}
}
