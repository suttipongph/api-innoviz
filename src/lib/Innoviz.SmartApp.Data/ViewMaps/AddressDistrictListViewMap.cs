using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressDistrictListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressDistrictGUID { get; set; }
		public Guid AddressProvinceGUID { get; set; }
		public string DistrictId { get; set; }
		public string ProvinceId { get; set; }
		public string AddressProvince_Values { get; set; }
		public string Name { get; set; }
	}
}
