﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    class GenMainAgmNoticeOfCancelViewMap : ViewCompanyBaseEntityMap
	{
        public Guid MainAgreementTableGUID { get; set; }
        public DateTime AgreementDate { get; set; }
        public int AgreementDocType { get; set; }
        public string BuyerName { get; set; }
        public Guid? BuyerTableGUID { get; set; }
        public string CancelDate { get; set; }
        public Guid CustomerTableGUID { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public Guid? DocumentReasonGUID { get; set; }
        public string InternalMainAgreementId { get; set; }
        public string MainAgreementDescription { get; set; }
        public string MainAgreementId { get; set; }
        public string NoticeOfCancellationInternalMainAgreementId { get; set; }
        public string ReasonRemark { get; set; }
        public string BuyerTable_Values { get; set; }
        public string CustomerTable_Values { get; set; }
        public string DocumentReason_Values { get; set; }
        public bool CreditLimitType_Revolving { get; set; }
    }
}
