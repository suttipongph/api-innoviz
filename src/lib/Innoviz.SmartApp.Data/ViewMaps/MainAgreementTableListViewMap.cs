using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MainAgreementTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MainAgreementTableGUID { get; set; }
		public string InternalMainAgreementId { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public string MainAgreementId { get; set; }
		public DateTime AgreementDate { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public int ProductType { get; set; }
		public int AgreementDocType { get; set; }
		public Guid? CreditLimitTypeGUID { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string CreditLimitType_CreditLimitTypeId { get; set; }
		public Guid? RefMainAgreementTableGUID { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
		public string Description { get; set; }
	}
}
