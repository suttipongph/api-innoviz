using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntercompanyInvoiceSettlementItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntercompanyInvoiceSettlementGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public Guid IntercompanyInvoiceTableGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public Guid? RefIntercompanyInvoiceSettlementGUID { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public decimal SettleWHTAmount { get; set; }
		
		public string IntercompanyInvoiceSettlementId { get; set; }
		public DateTime TransDate { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string IntercompanyInvoice_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public DateTime IntercomapnyInvoice_IssuedDate { get; set; }
		public decimal IntercomapnyInvoice_InvoiceAmount { get; set; }
		public string CNReason_Values { get; set; }
		public string RefIntercompanyInvoiceSettlement_IntercompanyInvoiceSettlementId { get; set; }

	}
}
