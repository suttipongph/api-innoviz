using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class StagingTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid StagingTableGUID { get; set; }
		public string StagingBatchId { get; set; }
		public string InterfaceStagingBatchId { get; set; }
		public int InterfaceStatus { get; set; }
		public int RefType { get; set; }
		public DateTime TransDate { get; set; }
		public int ProcessTransType { get; set; }
		public int AccountType { get; set; }
		public string AccountNum { get; set; }
		public decimal AmountMST { get; set; }
		public Guid? ProcessTransGUID { get; set; }
		public string ProcessTrans_Values { get; set; }
		public string RefId { get; set; }
		public Guid? RefGUID { get; set; }
		public int SourceRefType { get; set; }
		public string SourceRefId { get; set; }
		public string ProcessTrans_ProcessTransId{ get; set; }
		public string InvoiceTable_Values{ get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public string InvoiceTable_invoiceId { get; set; }
}
}
