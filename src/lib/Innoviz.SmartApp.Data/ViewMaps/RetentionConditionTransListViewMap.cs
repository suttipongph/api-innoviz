using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RetentionConditionTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RetentionConditionTransGUID { get; set; }
		public int ProductType { get; set; }
		public int RetentionDeductionMethod { get; set; }
		public int RetentionCalculateBase { get; set; }
		public decimal RetentionPct { get; set; }
		public decimal RetentionAmount { get; set; }
		public Guid RefGUID { get; set; }
	}
}
