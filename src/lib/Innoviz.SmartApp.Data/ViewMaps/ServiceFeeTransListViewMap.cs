using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class ServiceFeeTransListViewMap : ViewCompanyBaseEntityMap
    {
        public Guid ServiceFeeTransGUID { get; set; }
        public int Ordering { get; set; }
        public string Description { get; set; }
        public Guid InvoiceRevenueTypeGUID { get; set; }
        public bool IncludeTax { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public string InvoiceRevenueType_Values { get; set; }
        public string InvoiceRevenueType_RevenueTypeId { get; set; }
        public Guid RefGUID { get; set; }
    }
}
