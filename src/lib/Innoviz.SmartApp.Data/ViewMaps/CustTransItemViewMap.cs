using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustTransItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid CustTransGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public int CustTransStatus { get; set; }
		public string Description { get; set; }
		public DateTime? DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public DateTime? LastSettleDate { get; set; }
		public int ProductType { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAmountMST { get; set; }
		public decimal TransAmount { get; set; }
		public decimal TransAmountMST { get; set; }
		public DateTime TransDate { get; set; }
		 public string InvoiceTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CreditApp_Value { get; set; }
		public Guid? CreditAppLineGUID { get; set; }
		public string CreditAppLine_Value { get; set; }
	}
}
