using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessTypeGUID { get; set; }
		public string BusinessTypeId { get; set; }
		public string Description { get; set; }
	}
}
