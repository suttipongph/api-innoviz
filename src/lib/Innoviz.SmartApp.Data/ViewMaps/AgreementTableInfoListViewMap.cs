using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AgreementTableInfoListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AgreementTableInfoGUID { get; set; }
	}
}
