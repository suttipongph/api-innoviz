﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class DateToWordParam
    {
        public string DateString { get; set; }
        public char Delimiter { get; set; }
        public string Language { get; set; }
        public string Format { get; set; }
    }
    public class DateToWordResult
    {
        public DateTime ValueDate { get; set; }
        public string ValueInWords { get; set; }
    }
}
