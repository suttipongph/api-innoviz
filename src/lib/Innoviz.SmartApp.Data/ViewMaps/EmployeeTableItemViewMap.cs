﻿using Innoviz.SmartApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class EmployeeTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid EmployeeTableGUID { get; set; }
		public Guid? CollectionGroupGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public string EmployeeId { get; set; }
		public Guid? EmplTeamGUID { get; set; }
		public bool InActive { get; set; }
		public bool AssistMD { get; set; }
		public string Name { get; set; }
		public string Signature { get; set; }
		public Guid? UserId { get; set; }
		public string SysuserTable_UserName { get; set; }
		public Guid? DepartmentGUID { get; set; }
		public Guid? BusinessUnitGUID { get; set; }
		public Guid? ReportToEmployeeTableGUID { get; set; }
		public string Position { get; set; }
	}
}
