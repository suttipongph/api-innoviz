﻿using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class MigrationLogTableViewMap : ViewBaseEntityMap
    {
        public int Id { get; set; }
        public string MigrationName { get; set; }
        public DateTime LogStartDateTime { get; set; }
        public DateTime LogEndDateTime { get; set; }
        public string LogMessage { get; set; }
    }
}
