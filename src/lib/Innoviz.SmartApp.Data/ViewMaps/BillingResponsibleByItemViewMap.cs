using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BillingResponsibleByItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BillingResponsibleByGUID { get; set; }
		public int BillingBy { get; set; }
		public string BillingResponsibleById { get; set; }
		public string Description { get; set; }
	}
}
