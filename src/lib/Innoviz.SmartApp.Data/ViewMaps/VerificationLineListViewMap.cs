using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class VerificationLineListViewMap : ViewCompanyBaseEntityMap
    {
        public Guid VerificationLineGUID { get; set; }
        public string VerificationLineID { get; set; }
        public Guid VerificationTypeGUID { get; set; }
        public bool Pass { get; set; }
        public string VerificationType_Values { get; set; }
        public Guid VerificationTableGUID { get; set; }
    }
}
