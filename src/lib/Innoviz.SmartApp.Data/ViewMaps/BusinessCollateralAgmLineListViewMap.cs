using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralAgmLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralAgmLineGUID { get; set; }
		public Guid BusinessCollateralAgmTableGUID { get; set; }
		public int LineNum { get; set; }
		public Guid BusinessCollateralTypeGUID { get; set; }
		public Guid BusinessCollateralSubTypeGUID { get; set; }
		public string Description { get; set; }
		public decimal BusinessCollateralValue { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string BusinessCollateralType_Values { get; set; }
		public string BusinessCollateralSubType_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BusinessCollateralType_BusinessCollateralTypeId { get; set; }
		public string BusinessCollateralSubType_BusinessCollateralSubTypeId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public bool Cancelled { get; set; }
	}
}
