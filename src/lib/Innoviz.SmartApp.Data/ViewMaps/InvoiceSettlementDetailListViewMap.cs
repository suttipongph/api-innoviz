using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceSettlementDetailListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InvoiceSettlementDetailGUID { get; set; }
		public int ProductType { get; set; }
		public bool WHTSlipReceivedByBuyer { get; set; }
		public decimal SettleInvoiceAmount { get; set; }
		public string DocumentId { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public DateTime? InvoiceDueDate { get; set; }
		public Guid? InvoiceTypeGUID { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal BalanceAmount { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal WHTAmount { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string InvoiceTable_InvoiceId { get; set; }
		public string InvoiceType_InvoiceTypeId { get; set; }
		public Guid? InvoiceTable_InvoiceTypeGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public int SuspenseInvoiceType { get; set; }
	}
}
