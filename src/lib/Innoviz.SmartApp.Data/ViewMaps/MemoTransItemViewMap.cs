using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MemoTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MemoTransGUID { get; set; }
		public string Memo { get; set; }
		public Guid RefGUID { get; set; }
		public string RefId { get; set; }
		public int RefType { get; set; }
		public string Topic { get; set; }
	}
}
