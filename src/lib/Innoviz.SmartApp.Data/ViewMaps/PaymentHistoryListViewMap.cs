using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class PaymentHistoryListViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid PaymentHistoryGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public decimal PaymentAmount { get; set; }
		public decimal WHTAmount { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public int ProductType { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public DateTime PaymentDate { get; set; }
		public DateTime ReceivedDate { get; set; }
		public int ReceivedFrom { get; set; }
		public decimal PaymentBaseAmount { get; set; }
		public decimal PaymentTaxAmount { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public bool WHTSlipReceivedByCustomer { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string CreditAppTable_CreditAppTableId{ get; set; }
		public string InvoiceTable_InvoiceTableId{ get; set; }
	}
}
