using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AgreementTableInfoTextListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AgreementTableInfoTextGUID { get; set; }
	}
}
