using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class ServiceFeeTransItemViewMap : ViewCompanyBaseEntityMap
    {
        public Guid ServiceFeeTransGUID { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal AmountIncludeTax { get; set; }
        public Guid? CNReasonGUID { get; set; }
        public string Description { get; set; }
        public Guid? Dimension1GUID { get; set; }
        public Guid? Dimension2GUID { get; set; }
        public Guid? Dimension3GUID { get; set; }
        public Guid? Dimension4GUID { get; set; }
        public Guid? Dimension5GUID { get; set; }
        public decimal FeeAmount { get; set; }
        public bool IncludeTax { get; set; }
        public Guid InvoiceRevenueTypeGUID { get; set; }
        public int Ordering { get; set; }
        public decimal OrigInvoiceAmount { get; set; }
        public string OrigInvoiceId { get; set; }
        public decimal OrigTaxInvoiceAmount { get; set; }
        public string OrigTaxInvoiceId { get; set; }
        public Guid RefGUID { get; set; }
        public int RefType { get; set; }
        public decimal SettleAmount { get; set; }
        public decimal SettleInvoiceAmount { get; set; }
        public decimal SettleWHTAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public Guid? TaxTableGUID { get; set; }
        public decimal WHTAmount { get; set; }
        public Guid? WithholdingTaxTableGUID { get; set; }
        public string RefId { get; set; }
        public int ProductType { get; set; }
        public DateTime? TaxDate { get; set; }
        public bool WHTSlipReceivedByCustomer { get; set; }
        public Guid? InvoiceRevenueType_InterompanyTableGUID { get; set; }
        public decimal TaxValue { get; set; }
        public decimal WHTaxValue { get; set; }
    }
}
