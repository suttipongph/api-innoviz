using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentStatusItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentStatusGUID { get; set; }
		public string Description { get; set; }
		public Guid DocumentProcessGUID { get; set; }
		public string StatusCode { get; set; }
		public string StatusId { get; set; }
		public string ProcessId { get; set; }
		public string DocumentProcess_Values { get; set; }


	}
}
