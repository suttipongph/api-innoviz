using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceTypeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid InvoiceTypeGUID { get; set; }
		public bool DirectReceipt { get; set; }
		public bool ValidateDirectReceiveByCA { get; set; }
		public bool ProductInvoice	 { get; set; }
		public string ARLedgerAccount { get; set; }
		public bool AutoGen { get; set; }
		public string Description { get; set; }	
		public int ProductType { get; set; }
		public string InvoiceTypeId { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public Guid? AutoGenInvoiceRevenueTypeGUID { get; set; }
		public string AutoGenInvoiceRevenueType_values { get; set; }
	}
}
