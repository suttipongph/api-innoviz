using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class EmplTeamItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid EmplTeamGUID { get; set; }
		public string Name { get; set; }
		public string TeamId { get; set; }
	}
}
