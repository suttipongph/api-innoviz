using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class JobChequeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid JobChequeGUID { get; set; }
		public decimal Amount { get; set; }
		public string ChequeBankAccNo { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public DateTime ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public int ChequeSource { get; set; }
		public Guid? ChequeTableGUID { get; set; }
		public Guid? CollectionFollowUpGUID { get; set; }
		public Guid MessengerJobTableGUID { get; set; }
		public string RecipientName { get; set; }
		public Guid? MessengerJobTable_CustomerTableGUID { get; set; }
		public Guid? MessengerJobTable_BuyerTableGUID { get; set; }
		public Guid? MessengerJobTable_RefGUID { get; set; }
	}
}
