using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ConsortiumTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ConsortiumTransGUID { get; set; }
		public string Address { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		public string CustomerName { get; set; }
		public string OperatedBy { get; set; }
		public int Ordering { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string RefId { get; set; }
		public Guid? ConsortiumLineGUID { get; set; }
		public Guid? MainAgreementTable_ConsortiumTableGUID { get; set; }
	}
}
