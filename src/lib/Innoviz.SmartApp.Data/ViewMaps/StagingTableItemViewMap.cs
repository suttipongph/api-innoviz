using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class StagingTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid StagingTableGUID { get; set; }
		public string AccountNum { get; set; }
		public int AccountType { get; set; }
		public decimal AmountMST { get; set; }
		public string CompanyTaxBranchId { get; set; }
		public string DimensionCode1 { get; set; }
		public string DimensionCode2 { get; set; }
		public string DimensionCode3 { get; set; }
		public string DimensionCode4 { get; set; }
		public string DimensionCode5 { get; set; }
		public string DocumentId { get; set; }
		public string InterfaceStagingBatchId { get; set; }
		public int InterfaceStatus { get; set; }
		public Guid? ProcessTransGUID { get; set; }
		public int ProcessTransType { get; set; }
		public int ProductType { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public string StagingBatchId { get; set; }
		public string TaxAccountId { get; set; }
		public string TaxAccountName { get; set; }
		public string TaxAddress { get; set; }
		public decimal TaxBaseAmount { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxCode { get; set; }
		public string TaxId { get; set; }
		public string TaxInvoiceId { get; set; }
		public DateTime TransDate { get; set; }
		public string TransText { get; set; }
		public string VendorTaxInvoiceId { get; set; }
		public string WHTCode { get; set; }
		public string RefId { get; set; }
		public string ProcessTrans_Values { get; set; }
		public string StagingTableCompanyId { get; set; }
		public int SourceRefType { get; set; }
		public string SourceRefId { get; set; }
		public string InvoiceTable_Values { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
	}
}
