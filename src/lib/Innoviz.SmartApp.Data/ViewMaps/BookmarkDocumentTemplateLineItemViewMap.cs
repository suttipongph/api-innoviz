using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BookmarkDocumentTemplateLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BookmarkDocumentTemplateLineGUID { get; set; }
		public Guid BookmarkDocumentGUID { get; set; }
		public Guid? BookmarkDocumentTemplateTableGUID { get; set; }
		public Guid DocumentTemplateTableGUID { get; set; }
		public int DocumentTemplateType { get; set; }
		public string DocumentTemplateTable_Values { get; set; }
	}
}
