using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressTransGUID { get; set; }
		public string Name { get; set; }
		public bool Primary { get; set; }
		public bool CurrentAddress { get; set; }
		public string Address1 { get; set; }
		public Guid? RefGUID { get; set; }
	}
}
