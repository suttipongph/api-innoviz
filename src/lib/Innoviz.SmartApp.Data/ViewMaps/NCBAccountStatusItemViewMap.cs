using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class NCBAccountStatusItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid NCBAccountStatusGUID { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string NCBAccStatusId { get; set; }
	}
}
