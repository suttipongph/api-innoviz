using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class FinancialCreditTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid FinancialCreditTransGUID { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid CreditTypeGUID { get; set; }
		public decimal Amount { get; set; }
		public string BankGroup_Values { get; set; }
		public string BankGroup_BankGroupId { get; set; }
		public string CreditType_Values { get; set; }
		public string CreditType_CreditTypeId { get; set; }
		public Guid RefGUID { get; set; }
	}
}
