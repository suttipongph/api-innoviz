using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class LedgerFiscalPeriodItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid LedgerFiscalPeriodGUID { get; set; }
		public DateTime EndDate { get; set; }
		public Guid LedgerFiscalYearGUID { get; set; }
		public int PeriodStatus { get; set; }
		public DateTime StartDate { get; set; }
		public string LedgerFiscalYear_Values { get; set; }
	}
}
