using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerAgreementTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerAgreementTransGUID { get; set; }
		public Guid BuyerAgreementTableGUID { get; set; }
		public Guid BuyerAgreementLineGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string BuyerAgreementLine_Values { get; set; }
		public string BuyerAgreementTable_BuyerAgreementId { get; set; }
		public int BuyerAgreementLine_Period { get; set; }
		public DateTime? BuyerAgreementLine_DueDate { get; set; }
		public decimal BuyerAgreementLine_Amount { get; set; }
		public Guid RefGUID { get; set; }
	}
}
