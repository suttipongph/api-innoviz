using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class WithholdingTaxTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid WithholdingTaxTableGUID { get; set; }
		public string Description { get; set; }
		public string LedgerAccount { get; set; }
		public string WHTCode { get; set; }
	}
}
