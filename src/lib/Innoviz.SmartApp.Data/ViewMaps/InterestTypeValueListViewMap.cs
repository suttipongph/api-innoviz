using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InterestTypeValueListViewMap : ViewDateEffectiveBaseEntityMap
	{
		public Guid InterestTypeValueGUID { get; set; }
		public decimal Value { get; set; }
		public Guid InterestTypeGUID { get; set; }
		
	}
}
