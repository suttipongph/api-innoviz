using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class JointVentureTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid JointVentureTransGUID { get; set; }
		public int Ordering { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		public string Name { get; set; }
		public string OperatedBy { get; set; }
		public string AuthorizedPersonType_Values { get; set; }
		public string AuthorizedPersonType_AuthorizedPersonTypeId { get; set; }
		public Guid RefGUID { get; set; }
		public string DocumentStatus_StatusCode { get; set; }
	}
}
