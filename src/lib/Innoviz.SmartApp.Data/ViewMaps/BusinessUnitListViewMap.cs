using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessUnitListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessUnitGUID { get; set; }
		public string BusinessUnitId { get; set; }
		public string Description { get; set; }
		public Guid? ParentBusinessUnitGUID { get; set; }
		public string ParentBusinessUnit_Values { get; set; }
		public string ParentBusinessUnit_BusinessUnitId { get; set; }
	}
}
