using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustBankItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustBankGUID { get; set; }
		public string AccountNumber { get; set; }
		public bool BankAccountControl { get; set; }
		public string BankAccountName { get; set; }
		public string BankBranch { get; set; }
		public Guid BankGroupGUID { get; set; }
		public Guid BankTypeGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public bool InActive { get; set; }
		public bool PDC { get; set; }
		public bool Primary { get; set; }
		public string CustomerTable_Values { get; set; }
	}
}
