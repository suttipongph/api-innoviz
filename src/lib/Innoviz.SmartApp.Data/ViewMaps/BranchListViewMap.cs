using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BranchListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BranchGUID { get; set; }
		public string BranchId { get; set; }
		public Guid? TaxBranchGUID { get; set; }
		public string Name { get; set; }
	}
}
