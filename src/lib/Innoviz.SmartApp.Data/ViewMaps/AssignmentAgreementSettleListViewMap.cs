using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AssignmentAgreementSettleListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AssignmentAgreementSettleGUID { get; set; }
		public DateTime SettledDate { get; set; }
		public decimal SettledAmount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid AssignmentAgreementTableGUID { get; set; }
	}
}
