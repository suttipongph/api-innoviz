using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class VerificationTableItemViewMap : ViewCompanyBaseEntityMap
    {
        public Guid VerificationTableGUID { get; set; }
        public Guid BuyerTableGUID { get; set; }
        public Guid CreditAppTableGUID { get; set; }
        public string CreditAppTable_Values { get; set; }
        public string CreditAppTable_Value { get; set; }
        public Guid CustomerTableGUID { get; set; }
        public string CustomerTable_Values { get; set; }
        public string Description { get; set; }
        public Guid DocumentStatusGUID { get; set; }
        public string Remark { get; set; }
        public DateTime VerificationDate { get; set; }
        public string VerificationId { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public int TotalVericationLine { get; set; }
        public Guid originalDocumentStatusGUID { get; set; }
        public string BuyerTable_Values { get; set; }
        public bool HaveLine { get; set; }
        public string CopyVerificationGUID { get; set; }
    }
}
