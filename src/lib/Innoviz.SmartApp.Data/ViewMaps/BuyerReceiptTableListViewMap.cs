using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BuyerReceiptTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BuyerReceiptTableGUID { get; set; }
		public string BuyerReceiptId { get; set; }
		public DateTime BuyerReceiptDate { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public decimal BuyerReceiptAmount { get; set; }
		public bool Cancel { get; set; }
		public string BuyerTable_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
	}
}
