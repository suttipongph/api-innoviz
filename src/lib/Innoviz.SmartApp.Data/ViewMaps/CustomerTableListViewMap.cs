using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustomerTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustomerTableGUID { get; set; }
		public string CustomerId { get; set; }
		public string TaxID { get; set; }
		public string PassportID { get; set; }
		public string Name { get; set; }
		public int RecordType { get; set; }
		public Guid? DocumentStatusGUID { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
	}
}
