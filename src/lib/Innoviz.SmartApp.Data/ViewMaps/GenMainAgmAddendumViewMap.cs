﻿using System;
using Innoviz.SmartApp.Core.ViewModels;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    class GenMainAgmAddendumViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MainAgreementTableGUID { get; set; }
		public string AddendumInternalMainAgreementId { get; set; }
		public DateTime AgreementDate { get; set; }
		public int AgreementDocType { get; set; }
		public Guid BuyerGUID { get; set; }
		public string BuyerId { get; set; }
		public string BuyerName { get; set; }
		public string CreditAppRequestDescription { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public int CreditAppRequestType { get; set; }
		public Guid CreditLimitTypeGUID { get; set; }
		public Guid CustomerGUID { get; set; }
		public string CustomerId { get; set; }
		public string CustomerName { get; set; }
		public string Description { get; set; }
		public string InternalMainAgreementId { get; set; }
		public string MainAgreementDescription { get; set; }
		public string MainAgreementId { get; set; }
		public string RequestDate { get; set; }
		public string MainAgreementTable_Values { get; set; }
		public string CreditAppRequestTable_Values { get; set; }
		public string CreditLimitType_Values { get; set; }
		public string Customer_Values { get; set; }
		public string Buyer_Values { get; set; }
		public bool CreditLimitType_Revolving { get; set; }
	}
}

