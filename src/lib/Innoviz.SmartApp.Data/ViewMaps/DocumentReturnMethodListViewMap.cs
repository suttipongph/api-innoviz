using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentReturnMethodListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentReturnMethodGUID { get; set; }
		public string DocumentReturnMethodId { get; set; }
		public string Description { get; set; }
	}
}
