using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VerificationTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VerificationTransGUID { get; set; }
		public string DocumentId { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid VerificationTableGUID { get; set; }
		public string RefId { get; set; }
	}
}
