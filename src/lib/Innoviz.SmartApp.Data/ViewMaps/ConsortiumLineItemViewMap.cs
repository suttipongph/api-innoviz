using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ConsortiumLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ConsortiumLineGUID { get; set; }
		public string Address { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		public Guid ConsortiumTableGUID { get; set; }
		public string CustomerName { get; set; }
		public bool IsMain { get; set; }
		public string OperatedBy { get; set; }
		public string Position { get; set; }
		public decimal ProportionOfShareholderPct { get; set; }
		public string Remark { get; set; }
		public string ConsortiumTable_Values { get; set; }
		public int Ordering { get; set; }
	}
}
