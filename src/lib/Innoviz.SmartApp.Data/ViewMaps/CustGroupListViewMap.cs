using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustGroupListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustGroupGUID { get; set; }
		public string CustGroupId { get; set; }
		public string Description { get; set; }
		public Guid NumberSeqTableGUID { get; set; }
		public string NumberSeqTable_Value { get; set; }
		public string NumberSeqTable_Description { get; set; }
		public string NumberSeqCode { get; set; }
	}
}
