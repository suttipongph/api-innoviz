using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class IntercompanyItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid IntercompanyGUID { get; set; }
		public string Description { get; set; }
		public string IntercompanyId { get; set; }
	}
}
