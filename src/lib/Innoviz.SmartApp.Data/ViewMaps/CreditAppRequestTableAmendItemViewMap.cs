using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestTableAmendItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestTableAmendGUID { get; set; }
		public bool AmendAuthorizedPerson { get; set; }
		public bool AmendGuarantor { get; set; }
		public bool AmendRate { get; set; }
		public Guid CreditAppRequestTableGUID { get; set; }
		public decimal OriginalCreditLimit { get; set; }
		public decimal OriginalCreditLimitRequestFeeAmount { get; set; }
		public decimal OriginalInterestAdjustmentPct { get; set; }
		public Guid? OriginalInterestTypeGUID { get; set; }
		public string OriginalInterestType_Values { get; set; }
		public decimal OriginalMaxPurchasePct { get; set; }
		public decimal OriginalMaxRetentionAmount { get; set; }
		public decimal OriginalMaxRetentionPct { get; set; }
		public int OriginalPurchaseFeeCalculateBase { get; set; }
		public decimal OriginalPurchaseFeePct { get; set; }
		public decimal OriginalTotalInterestPct { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		#region Unbound
		public string CustomerTable_Values { get; set; }
		public Guid CreditAppRequestTable_CustomerTableGUID { get; set; }
		public string RefCreditAppTable_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public Guid? RefCreditAppTableGUID { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public DateTime? CreditAppRequestTable_RequestDate { get; set; }
		public Guid CreditAppRequestTable_DocumentStatusGUID { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public decimal CustomerTable_CustomerId { get; set; }
		public decimal CreditAppRequestTable_CustomerCreditLimit { get; set; }
		public DateTime? CreditAppRequestTable_StartDate { get; set; }
		public DateTime? CreditAppRequestTable_ExpiryDate { get; set; }
		public string CreditAppRequestTable_RefCreditAppId { get; set; }
		public string CreditAppRequestTable_Remark { get; set; }
		public decimal CreditAppRequestTable_NewCreditLimit { get; set; }
		public decimal CreditAppRequestTable_CreditLimitRequestFeePct { get; set; }
		public decimal CreditAppRequestTable_CreditLimitRequestFeeAmount { get; set; }
		public decimal CreditAppRequestTable_MaxRetentionPct { get; set; }
		public decimal CreditAppRequestTable_MaxRetentionAmount { get; set; }
		public Guid CreditAppRequestTable_NewInterestTypeGUID { get; set; }
		public decimal CreditAppRequestTable_NewInterestAdjustmentPct { get; set; }
		public decimal CreditAppRequestTable_NewTotalInterestPct { get; set; }
		public decimal CreditAppRequestTable_NewCreditLimitRequestFeePct { get; set; }
		public decimal CreditAppRequestTable_NewMaxRetentionPct { get; set; }
		public decimal CreditAppRequestTable_NewMaxRetentionAmount { get; set; }
		public decimal CreditAppRequestTable_NewMaxPurchasePct { get; set; }
		public decimal CreditAppRequestTable_NewPurchaseFeePct { get; set; }
		public int CreditAppRequestTable_NewPurchaseFeeCalculateBase { get; set; }
		public DateTime? CreditAppRequestTable_ApprovedDate { get; set; }
		public string CreditAppRequestTable_MarketingComment { get; set; }
		public string CreditAppRequestTable_CreditComment { get; set; }
		public string CreditAppRequestTable_ApproverComment { get; set; }
		public decimal CreditAppRequestTable_ApprovedCreditLimitRequest { get; set; }
		public string CreditAppRequestTable_CACondition { get; set; }
		public string CreditAppRequestTable_SigningCondition { get; set; }
		public DateTime? CreditAppRequestTable_FinancialCreditCheckedDate { get; set; }
		public string CreditAppRequestTable_FinancialCheckedBy { get; set; }
		public DateTime? CreditAppRequestTable_NCBCheckedDate { get; set; }
		public string CreditAppRequestTable_NCBCheckedBy { get; set; }
		public Guid? CreditAppRequestTable_BlacklistStatusGUID { get; set; }
		public Guid? CreditAppRequestTable_KYCGUID { get; set; }
		public Guid? CreditAppRequestTable_CreditScoringGUID { get; set; }
		public decimal CreditAppRequestTable_NewCreditLimitRequestFeeAmount { get; set; }
		public DateTime? CreditAppTable_InactiveDate { get; set; }
		#endregion
		public int ProcessInstanceId { get; set; }

	}
}
