using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BankGroupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BankGroupGUID { get; set; }
		public string Alias { get; set; }
		public string BankGroupId { get; set; }
		public string Description { get; set; }
	}
}
