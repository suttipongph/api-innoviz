using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class VerificationTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid VerificationTransGUID { get; set; }
		public Guid VerificationTableGUID { get; set; }
		public DateTime VerificationTable_VerificationDate { get; set; }
		public string BuyerId { get; set; }
		public string VerificationTable_Values { get; set; }
		public string VerificationTable_VerificationId { get; set; }
		public Guid RefGUID { get; set; }
	}
}
