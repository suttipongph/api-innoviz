using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressTransGUID { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public Guid AddressCountryGUID { get; set; }
		public Guid AddressDistrictGUID { get; set; }
		public Guid AddressPostalCodeGUID { get; set; }
		public Guid AddressProvinceGUID { get; set; }
		public Guid AddressSubDistrictGUID { get; set; }
		public bool CurrentAddress { get; set; }
		public bool IsTax { get; set; }
		public string Name { get; set; }
		public Guid? OwnershipGUID { get; set; }
		public bool Primary { get; set; }
		public Guid? PropertyTypeGUID { get; set; }
		public Guid? RefGUID { get; set; }
		public int RefType { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public string RefId { get; set; }
		public string AddressSubDistrict_SubDistrictId { get; set; }
		public string AddressSubDistrict_Name { get; set; }
		public string AddressDistrict_DistrictId { get; set; }
		public string AddressDistrict_Name { get; set; }
		public string AddressProvince_ProvinceId { get; set; }
		public string AddressProvince_Name { get; set; }
		public string AddressCountry_CountryId { get; set; }
		public string AddressCountry_Name { get; set; }
		public string AddressPostalCode_PostalCode { get; set; }
		public string UnboundAddress { get; set; }
		public string AltAddress { get; set; }
	}
}
