using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GuarantorTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GuarantorTransGUID { get; set; }
		public bool Affiliate { get; set; }
		public Guid GuarantorTypeGUID { get; set; }
		public bool InActive { get; set; }
		public int Ordering { get; set; }
		public Guid? RefGuarantorTransGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
		public int Age { get; set; }
		public string RelatedPersonTable_BackgroundSummary { get; set; }
		public DateTime? RelatedPersonTable_DateOfBirth { get; set; }
		public string RelatedPersonTable_Email { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Fax { get; set; }
		public int RelatedPersonTable_IdentificationType { get; set; }
		public string RelatedPersonTable_LineId { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_NationalityId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RelatedPersonTable_RaceId { get; set; }
		public string RefID { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public string RelatedPersonTable_RelatedPersonId { get; set; }
		public string RelatedPersonTable_Address1 { get; set; }
		public string RelatedPersonTable_Address2 { get; set; }
		public string RelatedPersonTable_DateOfIssue { get; set; }
		public string GuarantorAgreementTable_GuarantorAgreementTableGUID { get; set; }

		public string RelatedPersonTable_RaceGUID { get; set; }
		public string RelatedPersonTable_NationalityGUID { get; set; }
		public string RelatedPersonTable_OperatedBy { get; set; }
		public string CreditAppTable_CreditAppTableGUID { get; set; }
		public decimal MaximumGuaranteeAmount { get; set; }

	}
}
