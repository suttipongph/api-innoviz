﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class NumberToWordsParam
    {
        public string DecimalString { get; set; }
        public char Delimiter { get; set; }
        public string Language { get; set; }
        public string Suffix { get; set; }
    }
    public class NumberToWordsResult
    {
        public decimal ValueDecimal { get; set; }
        public string ValueInWords { get; set; }
    }
}
