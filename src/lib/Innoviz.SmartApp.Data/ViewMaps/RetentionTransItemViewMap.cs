using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class RetentionTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid RetentionTransGUID { get; set; }
		public decimal Amount { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public string DocumentId { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public DateTime TransDate { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerAgreementTable_Values { get; set; }
		public string CustomerTable_Value{ get; set; }
		public string RefID { get; set; }
	}
}
