using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqAssignmentOutstandingItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqAssignmentOutstandingGUID { get; set; }
		public decimal AssignmentAgreementAmount { get; set; }
		public Guid? AssignmentAgreementTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public Guid? CustomerTableGUID { get; set; }
		public decimal RemainingAmount { get; set; }
		public decimal SettleAmount { get; set; }
	}
}
