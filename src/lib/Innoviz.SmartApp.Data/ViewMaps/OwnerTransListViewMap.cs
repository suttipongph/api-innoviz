using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class OwnerTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid OwnerTransGUID { get; set; }
		public int Ordering { get; set; }
		public bool InActive { get; set; }
		public Guid RefGUID { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
	}
}
