using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AuthorizedPersonTransItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AuthorizedPersonTransGUID { get; set; }
		public Guid? AuthorizedPersonTypeGUID { get; set; }
		public bool InActive { get; set; }
		public string NCBCheckedBy { get; set; }
		public DateTime? NCBCheckedDate { get; set; }
		public int Ordering { get; set; }
		public Guid? RefAuthorizedPersonTransGUID { get; set; }
		public Guid RefGUID { get; set; }
		public int RefType { get; set; }
		public Guid RelatedPersonTableGUID { get; set; }
		public string RefId { get; set; }
		public string RelatedPersonTable_Name { get; set; }
		public int RelatedPersonTable_IdentificationType { get; set; }
		public string RelatedPersonTable_PassportId { get; set; }
		public string RelatedPersonTable_WorkPermitId { get; set; }
		public DateTime? RelatedPersonTable_DateOfBirth { get; set; }
		public string RelatedPersonTable_Phone { get; set; }
		public string RelatedPersonTable_Extension { get; set; }
		public string RelatedPersonTable_Fax { get; set; }
		public string RelatedPersonTable_Mobile { get; set; }
		public string RelatedPersonTable_LineId { get; set; }
		public string RelatedPersonTable_Email { get; set; }
		public string RelatedPersonTable_TaxId { get; set; }
		public string RelatedPersonTable_RelatedPersonId { get; set; }
		public string RelatedPersonTable_RaceId { get; set; }
		public string RelatedPersonTable_NationalityId { get; set; }
		public int RelatedPersonTable_Age { get; set; }
		public string RelatedPersonTable_BackgroundSummary { get; set; }
		public bool Replace { get; set; }
		public Guid? ReplacedAuthorizedPersonTransGUID { get; set; }

	}
}
