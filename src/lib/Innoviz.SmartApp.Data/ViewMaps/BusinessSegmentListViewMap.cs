using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessSegmentListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessSegmentGUID { get; set; }
		public string BusinessSegmentId { get; set; }
		public string Description { get; set; }
		public int BusinessSegmentType { get; set; }
	}
}
