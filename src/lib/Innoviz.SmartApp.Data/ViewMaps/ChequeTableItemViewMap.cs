using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ChequeTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ChequeTableGUID { get; set; }
		public decimal Amount { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string ChequeBankAccNo { get; set; }
		public Guid ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public DateTime ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public DateTime? CompletedDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime? ExpectedDepositDate { get; set; }
		public string IssuedName { get; set; }
		public bool PDC { get; set; }
		public int ReceivedFrom { get; set; }
		public string RecipientName { get; set; }
		public Guid? RefGUID { get; set; }
		public Guid? RefPDCGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string RefID { get; set; }
		public string RefPDC_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string PurchaseTable_Value { get; set; }
		public string CustomerTable_Value { get; set; }
		public string BuyerTable_Value { get; set; }
		public string ChequeBankGroup_Value { get; set; }
	}
}
