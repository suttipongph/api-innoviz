using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AuthorizedPersonTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AuthorizedPersonTypeGUID { get; set; }
		public string AuthorizedPersonTypeId { get; set; }
		public string Description { get; set; }
	}
}
