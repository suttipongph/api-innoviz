using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessSizeItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessSizeGUID { get; set; }
		public string BusinessSizeId { get; set; }
		public string Description { get; set; }
	}
}
