using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CollectionGroupItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CollectionGroupGUID { get; set; }
		public string AgreementBranchId { get; set; }
		public int AgreementFromOverdueDay { get; set; }
		public string AgreementLeaseTypeId { get; set; }
		public int AgreementToOverdueDay { get; set; }
		public string CollectionGroupId { get; set; }
		public string CustGroupId { get; set; }
		public string CustTerritoryId { get; set; }
		public string Description { get; set; }
	}
}
