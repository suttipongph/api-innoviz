using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentReturnLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentReturnLineGUID { get; set; }
		public string Address { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string ContactPersonName { get; set; }
		public string DocumentNo { get; set; }
		public Guid DocumentReturnTableGUID { get; set; }
		public Guid? DocumentTypeGUID { get; set; }
		public int LineNum { get; set; }
		public string Remark { get; set; }
		public int DocumentReturnTable_ContactTo { get; set; }
		public string DocumentReturnTable_DocumentStatus_StatusId { get; set; }
		public string DocumentReturnTable_DocumentReturnId { get; set; }
	}
}
