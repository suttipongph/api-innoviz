using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProductSettledTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProductSettledTransGUID { get; set; }
		public Guid InvoiceSettlementDetailGUID { get; set; }
		public string DocumentId { get; set; }
		public int ProductType { get; set; }
		public DateTime InterestDate { get; set; }
		public DateTime SettledDate { get; set; }
		public int InterestDay { get; set; }
		public int RefType { get; set; }
		public Guid? OriginalRefGUID { get; set; }
		public decimal SettledPurchaseAmount { get; set; }
		public decimal SettledReserveAmount { get; set; }
		public decimal SettledInvoiceAmount { get; set; }
		public decimal InterestCalcAmount { get; set; }
		public string InvoiceSettlementDetail_Values { get; set; }
		public string InvoiceTable_InvoiceId{ get; set; }
		public Guid WithdrawalTable_WithdrawalTableGUID { get; set; }
	}
}
