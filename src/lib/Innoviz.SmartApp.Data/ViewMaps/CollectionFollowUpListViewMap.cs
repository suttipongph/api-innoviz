using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CollectionFollowUpListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CollectionFollowUpGUID { get; set; }
		public DateTime CollectionDate { get; set; }
		public string Description { get; set; }
		public int ReceivedFrom { get; set; }
		public Guid MethodOfPaymentGUID { get; set; }
		public decimal CollectionAmount { get; set; }
		public int CollectionFollowUpResult { get; set; }
		public DateTime? NewCollectionDate { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public Guid? RefGUID { get; set; }
	}
}
