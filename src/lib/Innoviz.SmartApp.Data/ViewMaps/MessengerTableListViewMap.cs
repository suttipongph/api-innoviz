using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class MessengerTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid MessengerTableGUID { get; set; }
		public string Name { get; set; }
		public string TaxId { get; set; }
		public Guid? VendorTableGUID { get; set; }
		public string Phone { get; set; }
		public string PlateNumber { get; set; }
		public string DriverLicenseId { get; set; }
		public string VendorTable_Values { get; set; }
		public string VendorTable_VendorId { get; set; }
	}
}
