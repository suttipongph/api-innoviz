using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class DocumentConditionTemplateLineItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid DocumentConditionTemplateLineGUID { get; set; }
		public Guid? DocumentConditionTemplateTableGUID { get; set; }
		public Guid DocumentTypeGUID { get; set; }
		public bool Mandatory { get; set; }
		public string DocumentTypeId { get; set; }
		public string DocumentType_Values { get; set; }
	}
}
