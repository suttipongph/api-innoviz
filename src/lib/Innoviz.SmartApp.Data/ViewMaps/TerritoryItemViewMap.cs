using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TerritoryItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid TerritoryGUID { get; set; }
		public string Description { get; set; }
		public string TerritoryId { get; set; }
	}
}
