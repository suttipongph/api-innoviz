using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class JobChequeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid JobChequeGUID { get; set; }
		public int ChequeSource { get; set; }
		public Guid? CollectionFollowUpGUID { get; set; }
		public Guid? ChequeTableGUID { get; set; }
		public Guid? ChequeBankGroupGUID { get; set; }
		public string ChequeBranch { get; set; }
		public DateTime ChequeDate { get; set; }
		public string ChequeNo { get; set; }
		public decimal Amount { get; set; }
		public string CollectionFollowUp_Values { get; set; }
		public string ChequeTable_Values { get; set; }
		public string BankGroup_Values { get; set; }
		public Guid? MessengerJobTableGUID { get; set; }
	}
}
