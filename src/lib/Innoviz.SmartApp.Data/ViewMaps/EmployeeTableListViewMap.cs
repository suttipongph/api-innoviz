using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class EmployeeTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid EmployeeTableGUID { get; set; }
		public string EmployeeId { get; set; }
		public string Name { get; set; }
		public string EmplTeam_TeamId { get; set; }
		public string Department_Values { get; set; }
		public bool InActive { get; set; }
		public bool AssistMD { get; set; }
		public string SysuserTable_UserName { get; set; }
		public Guid? DepartmentGUID { get; set; }
		public string Department_DepartmentId { get; set; }
		public string Position { get; set; }
		public string BusinessUnit_Values { get; set; }
		public string BusinessUnit_BusinessUnitId { get; set; }
		public Guid? BusinessUnitGUID { get; set; }
	}
}
