using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CustVisitingTransListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CustVisitingTransGUID { get; set; }
		public DateTime CustVisitingDate { get; set; }
		public Guid ResponsibleByGUID { get; set; }
		public string Description { get; set; }
		public string EmployeeTable_Values { get; set; }
		public string EmployeeTable_EmployeeId { get; set; }
		public Guid? RefGUID { get; set; }
	}
}
