using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class InvoiceTableItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid InvoiceTableGUID { get; set; }
		public int AccountingPeriod { get; set; }
		public Guid? BuyerAgreementTableGUID { get; set; }
		public Guid? BuyerInvoiceTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public string DocumentId { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceAddress1 { get; set; }
		public string InvoiceAddress2 { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public decimal InvoiceAmountBeforeTaxMST { get; set; }
		public decimal InvoiceAmountMST { get; set; }
		public string InvoiceId { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public DateTime IssuedDate { get; set; }
		public string MailingInvoiceAddress1 { get; set; }
		public string MailingInvoiceAddress2 { get; set; }
		public int MarketingPeriod { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public decimal OrigInvoiceAmount { get; set; }
		public string OrigInvoiceId { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public bool ProductInvoice { get; set; }
		public int ProductType { get; set; }
		public Guid RefGUID { get; set; }
		public Guid? RefInvoiceGUID { get; set; }
		public Guid? RefTaxInvoiceGUID { get; set; }
		public int RefType { get; set; }
		public string Remark { get; set; }
		public int SuspenseInvoiceType { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TaxAmountMST { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public decimal WHTAmount { get; set; }
		public decimal WHTBaseAmount { get; set; }
		public Guid? ReceiptTempTableGUID { get; set; }

		#region UnboundField
		public string UnboundInvoiceAddress { get; set; }
		public string UnboundMailingAddress { get; set; }
		public int CustTransStatus { get; set; }
		public decimal CustTrans_SettleAmount { get; set; }
		public decimal CustTrans_TransAmount { get; set; }
		public Guid? CreditAppRequestTable_CreditAppRequestTableGUID { get; set; }
		public string ReferenceID { get; set; }
		#endregion
	}
}
