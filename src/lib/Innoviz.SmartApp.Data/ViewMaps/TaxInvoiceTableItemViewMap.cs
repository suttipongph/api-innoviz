using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class TaxInvoiceTableItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid TaxInvoiceTableGUID { get; set; }
		public Guid CurrencyGUID { get; set; }
		public string CustomerName { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public string DocumentId { get; set; }
		public Guid? CNReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public DateTime DueDate { get; set; }
		public decimal ExchangeRate { get; set; }
		public string InvoiceAddress1 { get; set; }
		public string InvoiceAddress2 { get; set; }
		public decimal InvoiceAmount { get; set; }
		public decimal InvoiceAmountBeforeTax { get; set; }
		public decimal InvoiceAmountBeforeTaxMST { get; set; }
		public decimal InvoiceAmountMST { get; set; }
		public Guid InvoiceTableGUID { get; set; }
		public Guid InvoiceTypeGUID { get; set; }
		public DateTime IssuedDate { get; set; }
		public string MailingInvoiceAddress1 { get; set; }
		public string MailingInvoiceAddress2 { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public decimal OrigTaxInvoiceAmount { get; set; }
		public string OrigTaxInvoiceId { get; set; }
		public Guid TaxInvoiceRefGUID { get; set; }
		public Guid? RefTaxInvoiceGUID { get; set; }
		public int TaxInvoiceRefType { get; set; }
		public string Remark { get; set; }
		public decimal TaxAmount { get; set; }
		public decimal TaxAmountMST { get; set; }
		public string TaxBranchId { get; set; }
		public string TaxBranchName { get; set; }
		public string TaxInvoiceId { get; set; }
		public string TaxInvoiceTable_Values { get; set; }
		public string Currency_Values { get; set; }
		public string CustomerTable_Values { get; set; }
		public string Dimension1_Values { get; set; }
		public string Dimension2_Values { get; set; }
		public string Dimension3_Values { get; set; }
		public string Dimension4_Values { get; set; }
		public string Dimension5_Values { get; set; }
		public string CNReason_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string InvoiceType_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string Ref_Values { get; set; }
		public string RefTaxInvoice_Values { get; set; }
		public string MailingInvoiceAddress_Values { get; set; }
		public string InvoiceAddress_Values{ get; set; }
	}
}
