using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class AddressSubDistrictListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid AddressSubDistrictGUID { get; set; }
		public Guid AddressDistrictGUID { get; set; }
		public Guid AddressProvinceGUID { get; set; }
		public string Name { get; set; }
		public string SubDistrictId { get; set; }
		public string addressDistrict_DistrictId { get; set; }
		public string addressDistrict_Name { get; set; }
		public string addressProvince_ProvinceId { get; set; }
		public string AddressProvince_Values { get; set; }
		public string AddressDistrict_Values { get; set; }
	}
}
