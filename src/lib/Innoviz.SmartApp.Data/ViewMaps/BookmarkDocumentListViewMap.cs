using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BookmarkDocumentListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BookmarkDocumentGUID { get; set; }
		public string BookmarkDocumentId { get; set; }
		public string Description { get; set; }
		public int DocumentTemplateType { get; set; }
		public int BookmarkDocumentRefType { get; set; }
	}
}
