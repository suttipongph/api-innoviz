using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptLineItemViewMap : ViewBranchCompanyBaseEntityMap
	{
		public Guid ReceiptLineGUID { get; set; }
		public DateTime? DueDate { get; set; }
		public decimal InvoiceAmount { get; set; }
		public Guid? InvoiceTableGUID { get; set; }
		public int LineNum { get; set; }
		public Guid ReceiptTableGUID { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleAmountMST { get; set; }
		public decimal SettleBaseAmount { get; set; }
		public decimal SettleBaseAmountMST { get; set; }
		public decimal SettleTaxAmount { get; set; }
		public decimal SettleTaxAmountMST { get; set; }
		public decimal TaxAmount { get; set; }
		public Guid? TaxTableGUID { get; set; }
		public decimal WHTAmount { get; set; }
		public Guid? WithholdingTaxTableGUID { get; set; }
		public string ReceiptTable_Values { get; set; }
		public string InvoiceTable_Values { get; set; }
		public string TaxTable_Code{ get; set; }
		public string WithholdingTaxTable_Code { get; set; }

	}
}
