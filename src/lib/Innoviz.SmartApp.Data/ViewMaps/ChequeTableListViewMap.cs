using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class ChequeTableListViewMap : ViewCompanyBaseEntityMap
    {
        public Guid ChequeTableGUID { get; set; }
        public Guid CustomerTableGUID { get; set; }
        public Guid DocumentStatusGUID { get; set; }
        public Guid? BuyerTableGUID { get; set; }
        public DateTime ChequeDate { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeBankAccNo { get; set; }
        public bool PDC { get; set; }
        public decimal Amount { get; set; }
        public string IssuedName { get; set; }
        public string RecipientName { get; set; }
        public string CustomerTable_Values { get; set; }
        public string BuyerTable_Values { get; set; }
        public string DocumentStatus_Values { get; set; }
        public string CustomerTable_CustomerId { get; set; }
        public string DocumentStatus_StatusCode { get; set; }
        public Guid? RefGUID { get; set; }
        public int RefType { get; set; }
        public string DocumentStatus_StatusId { get; set; }
        public Guid PurchaseLine_Value { get; set; }
        public Guid WithdrawalLine_Value { get; set; }
    }
}
