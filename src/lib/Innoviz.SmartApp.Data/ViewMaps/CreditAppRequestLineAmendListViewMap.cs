using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppRequestLineAmendListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppRequestLineAmendGUID { get; set; }
		public string CreditAppRequestTable_CreditAppRequestId { get; set; }
		public string CreditAppRequestTable_Description { get; set; }
		public int CreditAppRequestTable_CreditAppRequestType { get; set; }
		public DateTime? CreditAppRequestTable_RequestDate { get; set; }
		public string DocumentStatus_Values { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public Guid? CreditAppRequestTable_DocumentStatusGUID { get; set; }
		public Guid? CreditAppRequestTable_CustomerTableGUID { get; set; }
		public Guid? CreditAppRequestTable_BuyerTableGUID { get; set; }
		public Guid? CreditAppRequestLine_RefCreditAppLineGUID { get; set; }
	}
}
