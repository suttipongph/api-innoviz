using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CreditAppLineListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CreditAppLineGUID { get; set; }
		public Guid CreditAppTableGUID { get; set; }
		public int LineNum { get; set; }
		public DateTime? ReviewDate { get; set; }
		public Guid BuyerTableGUID { get; set; }
		public Guid BusinessSegmentGUID { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public string BuyerTable_Values { get; set; }
		public string BusinessSegment_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string BusinessSegment_BusinessSegmentId { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string CustomerTable_CustomerId { get; set; }
		public Guid CustomerTable_CustomerTableGUID { get; set; }
		public DateTime ExpiryDate { get; set; }
		public int ProductType { get; set; }
	}
}
