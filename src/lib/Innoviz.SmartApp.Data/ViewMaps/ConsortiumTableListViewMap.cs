using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ConsortiumTableListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ConsortiumTableGUID { get; set; }
		public string ConsortiumId { get; set; }
		public string Description { get; set; }
	}
}
