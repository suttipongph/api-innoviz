using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ProjectProgressTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ProjectProgressTableGUID { get; set; }
		public string Description { get; set; }
		public string ProjectProgressId { get; set; }
		public string Remark { get; set; }
		public DateTime TransDate { get; set; }
		public Guid WithdrawalTableGUID { get; set; }
		public string Withdrawal_Value { get; set; }
	}
}
