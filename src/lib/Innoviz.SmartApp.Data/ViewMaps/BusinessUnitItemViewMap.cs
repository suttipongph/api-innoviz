using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessUnitItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessUnitGUID { get; set; }
		public string BusinessUnitId { get; set; }
		public string Description { get; set; }
		public Guid? ParentBusinessUnitGUID { get; set; }
	}
}
