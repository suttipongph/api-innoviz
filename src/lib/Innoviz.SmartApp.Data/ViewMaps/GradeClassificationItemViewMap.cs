using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class GradeClassificationItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid GradeClassificationGUID { get; set; }
		public string Description { get; set; }
		public string GradeClassificationId { get; set; }
	}
}
