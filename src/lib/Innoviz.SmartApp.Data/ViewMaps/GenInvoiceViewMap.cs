﻿using Innoviz.SmartApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innoviz.SmartApp.Data.ViewMaps
{
    public class GenInvoiceResultView
    {
        public InvoiceTable InvoiceTable { get; set; }
        public InvoiceLine InvoiceLine { get; set; }
    }
}
