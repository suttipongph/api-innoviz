using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class CAReqBuyerCreditOutstandingListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid CAReqBuyerCreditOutstandingGUID { get; set; }
		public int LineNum { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public string Status { get; set; }
		public decimal ApprovedCreditLimitLine { get; set; }
		public decimal ARBalance { get; set; }
		public decimal MaxPurchasePct { get; set; }
		public Guid? AssignmentMethodGUID { get; set; }
		public Guid? BillingResponsibleByGUID { get; set; }
		public Guid? MethodOfPaymentGUID { get; set; }
		public string BuyerTable_Values { get; set; }
		public string AssignmentMethod_Values { get; set; }
		public string BillingResponsibleBy_Values { get; set; }
		public string MethodOfPayment_Values { get; set; }
		public string BuyerTable_BuyerId { get; set; }
		public string AssignmentMethod_AssignmentMethodId { get; set; }
		public string BillingResponsibleBy_BillingResponsibleById { get; set; }
		public string MethodOfPayment_MethodOfPaymentId { get; set; }
		public Guid? CreditAppRequestTableGUID { get; set; }
		public string CreditAppTable_CreditAppId { get; set; }

	}
}
