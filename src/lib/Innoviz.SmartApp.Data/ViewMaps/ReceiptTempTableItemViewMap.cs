using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class ReceiptTempTableItemViewMap : ViewCompanyBaseEntityMap
	{
		public Guid ReceiptTempTableGUID { get; set; }
		public Guid? BuyerReceiptTableGUID { get; set; }
		public Guid? BuyerTableGUID { get; set; }
		public Guid? CreditAppTableGUID { get; set; }
		public Guid? CurrencyGUID { get; set; }
		public Guid CustomerTableGUID { get; set; }
		public Guid? Dimension1GUID { get; set; }
		public Guid? Dimension2GUID { get; set; }
		public Guid? Dimension3GUID { get; set; }
		public Guid? Dimension4GUID { get; set; }
		public Guid? Dimension5GUID { get; set; }
		public Guid? DocumentReasonGUID { get; set; }
		public Guid DocumentStatusGUID { get; set; }
		public decimal ExchangeRate { get; set; }
		public Guid? MainReceiptTempGUID { get; set; }
		public int ProductType { get; set; }
		public decimal ReceiptAmount { get; set; }
		public DateTime ReceiptDate { get; set; }
		public string ReceiptTempId { get; set; }
		public int ReceiptTempRefType { get; set; }
		public int ReceivedFrom { get; set; }
		public Guid? RefReceiptTempGUID { get; set; }
		public string Remark { get; set; }
		public decimal SettleAmount { get; set; }
		public decimal SettleFeeAmount { get; set; }
		public decimal SuspenseAmount { get; set; }
		public Guid? SuspenseInvoiceTypeGUID { get; set; }
		public DateTime TransDate { get; set; }
		public string DocumentReason_Values { get; set; }
		public string MainReceiptTemp_Values { get; set; }
		public string RefReceiptTemp_Values { get; set; }
		public string DocumentStatus_Values { get; set; }
		public bool InvoiceType_ValidateDirectReceiveByCA { get; set; }
		public string DocumentStatus_StatusId { get; set; }
		public string CustomerTable_Values { get; set; }
		public string BuyerTable_Values { get; set; }
		public string SuspenseInvoiceType_Values { get; set; }
		public string CreditAppTable_Values { get; set; }
		public string Process_Id { get; set; }
	}
}
