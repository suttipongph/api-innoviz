using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralStatusListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralStatusGUID { get; set; }
		public string BusinessCollateralStatusId { get; set; }
		public string Description { get; set; }
	}
}
