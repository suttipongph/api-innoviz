using Innoviz.SmartApp.Core.ViewModels;
using System;

namespace Innoviz.SmartApp.Data.ViewMaps
{
	public class BusinessCollateralTypeListViewMap : ViewCompanyBaseEntityMap
	{
		public Guid BusinessCollateralTypeGUID { get; set; }
		public string BusinessCollateralTypeId { get; set; }
		public string Description { get; set; }
		public int AgreementOrdering { get; set; }
	}
}
