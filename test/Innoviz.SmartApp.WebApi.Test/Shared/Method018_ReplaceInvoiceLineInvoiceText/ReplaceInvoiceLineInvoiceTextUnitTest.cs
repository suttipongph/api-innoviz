﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method018_ReplaceInvoiceLineInvoiceText
{
    public class ReplaceInvoiceLineInvoiceTextUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public ReplaceInvoiceLineInvoiceTextUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "ReplaceInvoiceLineInvoiceText");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }

        [Theory]
        [MemberData(nameof(CaseReplaceInvoiceLineInvoiceText))]
        public void ReplaceInvoiceLineInvoiceText(ViewReplaceInvoiceLineInvoiceTextParameter parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method018_ReplaceInvoiceLineInvoiceText/Case_ExampleData/masterdata.json");

                List<InvoiceLine> expectedInvoiceLines = result.InvoiceLine;

                IInvoiceService invoiceService = new InvoiceService(db);
                List<InvoiceLine> actualInvoiceLine = invoiceService.ReplaceInvoiceLineInvoiceText(parm.InvoiceLine);

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(InvoiceLine), new string[] { } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedInvoiceLines, actualInvoiceLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseReplaceInvoiceLineInvoiceText
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm(), GetResult() }
                };
            }
        }
        public static ViewReplaceInvoiceLineInvoiceTextParameter GetParm()
        {
            string path = "../../../Shared/Method018_ReplaceInvoiceLineInvoiceText/Case_ExampleData/parameter.json";
            ViewReplaceInvoiceLineInvoiceTextParameter parm = TestHelper.JsonToObject<ViewReplaceInvoiceLineInvoiceTextParameter>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../Shared/Method018_ReplaceInvoiceLineInvoiceText/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<InvoiceLine> InvoiceLine { get; set; }
    }
}
