﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method022_PostInvoice_R04
{
    public class PostInvoiceUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostInvoiceUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostInvoice");
        }
        [Theory]
        [MemberData(nameof(CasePostInvoice))]
        public void PostInvoice(string[] refGUIDs, RefType rRefType, string guID)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method022_PostInvoice_R04/masterdata.json");
                IInvoiceService invoiceService = new InvoiceService(db);
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                IInvoiceLineRepo invoiceLineRepo = new InvoiceLineRepo(db);

                List<Guid> invoiceTableGuids = refGUIDs.Select(x => x.StringToGuid()).ToList();
                List<InvoiceTable> invoiceTables = invoiceTableRepo.GetInvoiceTableByIdNoTracking(invoiceTableGuids);
                List<InvoiceLine> invoiceLines = invoiceLineRepo.GetInvoiceLineNotNullTaxTableByInvoiceTableGUIDNoTracking(invoiceTableGuids).ToList();

                PostInvoiceResultView postInvoiceResultView = invoiceService.PostInvoice(invoiceTables, invoiceLines, rRefType, guID.StringToGuid());

                Assert.True(postInvoiceResultView.InvoiceTables.All(a => invoiceTableGuids.Contains(a.InvoiceTableGUID)));
                Assert.True(postInvoiceResultView.InvoiceLines.All(a => invoiceTableGuids.Contains(a.InvoiceTableGUID)));
                Assert.True(postInvoiceResultView.InvoiceTables.All(a => !string.IsNullOrEmpty(a.InvoiceId)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostInvoice
        {
            get
            {
                return new[]
                {
                    new object[] { new[] {  "81F0A6D6-1365-4000-B519-976A29265146","357FCC0A-E562-4BD7-BE85-AB77D917A9B8" }, RefType.WithdrawalLine ,"69585EEE-DFBF-4545-8A50-E9D00C5A4B50"}
                };
            }
        }
    }
}
