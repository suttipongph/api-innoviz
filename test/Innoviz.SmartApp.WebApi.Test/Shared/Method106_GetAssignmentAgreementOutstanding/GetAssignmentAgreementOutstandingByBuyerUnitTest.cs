﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method106_GetAssignmentAgreementOutstanding
{
    public class GetAssignmentAgreementOutstandingByBuyerUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GetAssignmentAgreementOutstandingByBuyerUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetAssignmentAgreementOutstandingByBuyer");
        }
        [Theory]
        [MemberData(nameof(CaseGetAssignmentAgreementOutstanding))]
        public void GetAssignmentAgreementOutstandingByBuyer(string refGUID, int refType)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method106_GetAssignmentAgreementOutstanding/masterdata.json");
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
               
                List<AssignmentAgreementOutstandingView> expectedResults = assignmentAgreementTableService.GetAssignmentAgreementOutstanding(refGUID.StringToGuid(), refType);
                Assert.True(expectedResults.All(a => a.BuyerTableGUID == refGUID));
                Assert.True(expectedResults.All(a => a.RemainingAmount == ((a.AssignmentAgreementAmount > 0) ? a.AssignmentAgreementAmount - a.SettledAmount : 0)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGetAssignmentAgreementOutstanding
        {
            get
            {
                return new[]
                {
                    new object[] { "A27FF0F0-64E6-4B4C-9032-C504BD8558C9", (int)RefType.Buyer }
                };
            }
        }
    }
}
