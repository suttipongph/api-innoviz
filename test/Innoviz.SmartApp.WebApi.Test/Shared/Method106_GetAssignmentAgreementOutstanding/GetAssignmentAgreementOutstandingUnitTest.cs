﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method106_GetAssignmentAgreementOutstanding
{
    public class GetAssignmentAgreementOutstandingUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GetAssignmentAgreementOutstandingUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetAssignmentAgreementOutstanding");
        }
        [Theory]
        [MemberData(nameof(CaseGetAssignmentAgreementOutstanding))]
        public void GetAssignmentAgreementOutstanding(string refGUID, int refType)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method106_GetAssignmentAgreementOutstanding/masterdata.json");
                IAssignmentAgreementTableService assignmentAgreementTableService = new AssignmentAgreementTableService(db);
                List<SearchCondition> conditions = new List<SearchCondition>
                {
                    new SearchCondition
                    {
                        ColumnName = "RefType",
                        Value = refType.ToString(),
                        Type = ColumnType.MASTER,
                        EqualityOperator = Operators.EQUAL,
                        Operator = Operators.AND
                    },
                    new SearchCondition
                    {
                        ColumnName = "PARENT_COLUMN",
                        Value = refGUID,
                        Type = ColumnType.MASTER,
                        EqualityOperator = Operators.EQUAL,
                        Operator = Operators.AND
                    }
                };

                var search = new SearchParameter()
                {
                    BranchFilterMode = "ByCompany",
                    Conditions = conditions,
                    Paginator = new Paginator() { 
                           Rows = 10
                    },
                    IsAscs = new List<bool> {false },
                    SortColumns = new List<string>{ "internalAssignmentAgreementId" }
                };
                SearchResult<AssignmentAgreementOutstandingView> expectedResults = assignmentAgreementTableService.GetAssignmentAgreementOutstanding(search);

                if (refType == (int)RefType.Buyer)
                {
                    Assert.True(expectedResults.Results.All(a => a.BuyerTableGUID == refGUID));
                }
                else if (refType == (int)RefType.Customer)
                {
                    Assert.True(expectedResults.Results.All(a => a.CustomerTableGUID == refGUID));
                }
                else if (refType == (int)RefType.AssignmentAgreement)
                {
                    Assert.True(expectedResults.Results.All(a => a.AssignmentAgreementTableGUID == refGUID));
                }
                Assert.True(expectedResults.Results.All(a => a.RemainingAmount == ((a.AssignmentAgreementAmount > 0) ? a.AssignmentAgreementAmount - a.SettledAmount : 0)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGetAssignmentAgreementOutstanding
        {
            get
            {
                return new[]
                {
                    new object[] { "9a2e629a-9335-46d2-8627-7c776fc6c4b9", (int)RefType.Buyer }
                };
            }
        }
    }
}
