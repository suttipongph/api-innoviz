﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method118_GenIntercompanySettlementStaging
{
    public class GenIntercompanySettlementStagingUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GenIntercompanySettlementStagingUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GenIntercompanySettlementStaging");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }

        [Theory]
        [MemberData(nameof(CaseGenIntercompanySettlementStaging))]
        public void GenIntercompanySettlementStaging(GenIntercompanySettlementStagingParameter parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method118_GenIntercompanySettlementStaging/Case_ExampleData/masterdata.json");

                StagingTableIntercoInvSettle expectedStagingTableIntercoInvSettle = result.StagingTableIntercoInvSettle.FirstOrDefault();
                string expectedIntercompanyInvoiceSettlementId = result.IntercompanyInvoiceSettlementId;

                IStagingTableIntercoInvSettleService stagingTableIntercoInvSettleService = new StagingTableIntercoInvSettleService(db);
                GenIntercompanySettlementStagingView actual = stagingTableIntercoInvSettleService.GenIntercompanySettlementStaging(parm.IntercompanyInvoiceSettlement.FirstOrDefault());
                StagingTableIntercoInvSettle actualStagingTableIntercoInvSettle = actual.StagingTableIntercoInvSettle;
                string actualIntercompanyInvoiceSettlementId = actual.IntercompanyInvoiceSettlementId;

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(StagingTableIntercoInvSettle), new string[] { "StagingTableIntercoInvSettleGUID" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedStagingTableIntercoInvSettle, actualStagingTableIntercoInvSettle);
                Assert.True(expectedIntercompanyInvoiceSettlementId == actualIntercompanyInvoiceSettlementId);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGenIntercompanySettlementStaging
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm(), GetResult() }
                };
            }
        }
        public static GenIntercompanySettlementStagingParameter GetParm()
        {
            string path = "../../../Shared/Method118_GenIntercompanySettlementStaging/Case_ExampleData/parameter.json";
            GenIntercompanySettlementStagingParameter parm = TestHelper.JsonToObject<GenIntercompanySettlementStagingParameter>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../Shared/Method118_GenIntercompanySettlementStaging/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<StagingTableIntercoInvSettle> StagingTableIntercoInvSettle { get; set; }
        public string IntercompanyInvoiceSettlementId { get; set; }
    }
}
