﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method116_GenProcessTransFromInterestRealizedTrans
{
    public class GenProcessTransFromInterestRealizedTransUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GenProcessTransFromInterestRealizedTransUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GenProcessTransFromInterestRealizedTrans");
        }
        [Theory]
        [MemberData(nameof(CaseGenProcessTransFromInterestRealizedTrans))]
        public void GenProcessTransFromInterestRealizedTrans(GenProcessTransFromInterestRealizedTransParameter parameter, Result expectedResult)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method116_GenProcessTransFromInterestRealizedTrans/Case_ExampleData/masterdata.json"); 
                IProcessTransService processTransService = new ProcessTransService(db);

                #region prepare expected results

                if (expectedResult.ProcessTrans == null)
                {
                    expectedResult.ProcessTrans = new List<ProcessTrans>();
                }
                else
                {
                    expectedResult.ProcessTrans = expectedResult.ProcessTrans.OrderBy(o => o.RefGUID).ToList();
                }
                if (expectedResult.InterestRealizedTrans == null)
                {
                    expectedResult.InterestRealizedTrans = new List<InterestRealizedTrans>();
                }
                else
                {
                    expectedResult.InterestRealizedTrans = expectedResult.InterestRealizedTrans.OrderBy(o => o.InterestRealizedTransGUID).ToList();
                }

                #endregion prepare expected results

                GenProcessTransFromInterestRealizedTransResultView actualResult = processTransService.GenProcessTransFromInterestRealizedTrans(parameter.interestRealizedTrans, parameter.transDate.StringToDate());

                #region prepare actual result
                List<ProcessTrans> actualProcessTrans = actualResult.ProcessTrans.OrderBy(o => o.RefGUID).ToList();
                List<InterestRealizedTrans> actualInterestRealizedTrans = actualResult.InterestRealizedTrans.OrderBy(o => o.InterestRealizedTransGUID).ToList();

                Assert.True(expectedResult.ProcessTrans.Count == actualProcessTrans.Count);
                #endregion prepare actual result

                #region assert should equal
                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(ProcessTrans), new string[] { "ProcessTransGUID" } },
                    { typeof(InterestRealizedTrans), new string[] { "RefProcessTransGUID" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedResult.ProcessTrans, actualProcessTrans);
                helper.AssertShouldEqual(expectedResult.InterestRealizedTrans, actualInterestRealizedTrans);
                #endregion assert should equal
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGenProcessTransFromInterestRealizedTrans
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm(), GetResult() }
                };
            }
        }
        public static GenProcessTransFromInterestRealizedTransParameter GetParm()
        {
            string path = "../../../Shared/Method116_GenProcessTransFromInterestRealizedTrans/Case_ExampleData/parameter.json";
            GenProcessTransFromInterestRealizedTransParameter parm = TestHelper.JsonToObject<GenProcessTransFromInterestRealizedTransParameter>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../Shared/Method116_GenProcessTransFromInterestRealizedTrans/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
        public class Result
        {
            public List<ProcessTrans> ProcessTrans { get; set; }
            public List<InterestRealizedTrans> InterestRealizedTrans { get; set; }
        }
    }
}
