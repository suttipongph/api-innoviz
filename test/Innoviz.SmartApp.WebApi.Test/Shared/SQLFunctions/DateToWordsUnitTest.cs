﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.Shared.SQLFunctions
{
    public class DateToWordsUnitTest: IClassFixture<SharedDatabaseFixture>
    {
        private ITestOutputHelper output;
        private SharedDatabaseFixture fixture;
        public DateToWordsUnitTest(ITestOutputHelper output, SharedDatabaseFixture fixture)
        {
            this.output = output;
            this.fixture = fixture;
        }
        [Theory]
        [InlineData("30/04/2021", "TH", "DAY MONTH YEAR", "30 เมษายน 2564")]
        [InlineData("30/04/2021", "EN", "YEAR MONTH DAY", "2021 April 30")]
        [InlineData("02/01/2021", "TH", "DAY MONTH YEAR", "2 มกราคม 2564")]
        [InlineData("02/01/2021", "EN", "YEAR MONTH DAY", "2021 January 2")]
        public void DateToWord(string date, string language, string format, string expected)
        {
            try
            {
                DateTime dateParm = date.StringToDate();
                ISharedService sharedService = new SharedService(fixture.db);
                var result = sharedService.DateToWord(dateParm, language, format);

                Assert.True(result == expected);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
    }
}
