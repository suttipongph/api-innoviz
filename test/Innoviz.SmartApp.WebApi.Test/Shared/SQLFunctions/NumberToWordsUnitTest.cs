﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.Shared.SQLFunctions
{
    public class NumberToWordsUnitTest : IClassFixture<SharedDatabaseFixture>
    {
        private ITestOutputHelper output;
        private SharedDatabaseFixture fixture;
        public NumberToWordsUnitTest(ITestOutputHelper output, SharedDatabaseFixture fixture)
        {
            this.output = output;
            this.fixture = fixture;
        }
        [Theory]
        [InlineData(155, "One Hundred Fifty Five and 00/100")]
        [InlineData(155.68, "One Hundred Fifty Five and 68/100")]
        [InlineData(1255, "One Thousand Two Hundred Fifty Five and 00/100")]
        [InlineData(12355.68, "Twelve Thousand Three Hundred Fifty Five and 68/100")]
        [InlineData(15500000, "Fifteen Million Five Hundred Thousand and 00/100")]
        public void NumberToWords(decimal value, string expected)
        {
            try
            {
                ISharedService sharedService = new SharedService(fixture.db);
                var result = sharedService.NumberToWords(value);

                //Assert.True(result == expected);
                Assert.Equal(expected, result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        [Fact]
        public void NumberToWordsBulk()
        {
            try
            {
                var inputVals = new decimal[]
                {
                    784192.42m, 1053820.65m, 16653.3m, 261113.78m, 4259025.49m, 935100m, 31088.72m, 27272.59m, 8624.05m,
                    1400000m, 0m, 15093.02m, 109112.65m, 296043.92m, 29736.24m, 135103.27m, 1765085.54m, 1282319.86m,
                    475906.24m, 306298.79m, 15178011.41m, 249334.65m, 240000m, 662786.93m, 4259025.49m, 998576.78m,
                };
                ISharedService sharedService = new SharedService(fixture.db);
                var result = sharedService.NumberToWords(inputVals);
                var expected = GetExpectedBulk();
                for(int i=0; i< result.Count(); i++)
                {
                    Assert.Equal(expected[i], result.ElementAt(i).ValueInWords);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        [Theory]
        [InlineData(155, "หนึ่งร้อยห้าสิบห้าบาทถ้วน")]
        [InlineData(155.68, "หนึ่งร้อยห้าสิบห้าบาทหกสิบแปดสตางค์")]
        [InlineData(1255, "หนึ่งพันสองร้อยห้าสิบห้าบาทถ้วน")]
        [InlineData(12355.68, "หนึ่งหมื่นสองพันสามร้อยห้าสิบห้าบาทหกสิบแปดสตางค์")]
        [InlineData(15500000, "สิบห้าล้านห้าแสนบาทถ้วน")]
        [InlineData(10000000, "สิบล้านบาทถ้วน")]
        //R02
        [InlineData(1, "หนึ่ง", "")]
        [InlineData(155, "หนึ่งร้อยห้าสิบห้า", "")]
        [InlineData(1.2, "หนึ่งจุดสอง", "")]
        [InlineData(155.55, "หนึ่งร้อยห้าสิบห้าจุดห้าห้า", "")]
        [InlineData(1, "หนึ่งชิ้น", "ชิ้น")]
        [InlineData(1.8, "หนึ่งจุดแปดเซนติเมคร", "เซนติเมคร")]
        [InlineData(57.68, "ห้าสิบเจ็ดจุดหกแปดกิโลกรัม", "กิโลกรัม")]
        public void NumberToWordsTH(decimal value, string expected, string suffix = null)
        {
            try
            {
                ISharedService sharedService = new SharedService(fixture.db);
                var result = sharedService.NumberToWordsTH(value, suffix);

                //Assert.True(result == expected);
                Assert.Equal(expected, result);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        [Fact]
        public void NumberToWordsTHBulk()
        {
            try
            {
                var inputVals = new decimal[]
                {
                    784192.42m, 1053820.65m, 16653.3m, 261113.78m, 4259025.49m, 935100m, 31088.72m, 27272.59m, 8624.05m,
                    1400000m, 0m, 15093.02m, 109112.65m, 296043.92m, 29736.24m, 135103.27m, 1765085.54m, 1282319.86m,
                    475906.24m, 306298.79m, 15178011.41m, 249334.65m, 240000m, 662786.93m, 4259025.49m, 998576.78m,
                };
                ISharedService sharedService = new SharedService(fixture.db);
                var result = sharedService.NumberToWordsTH(inputVals);
                var expected = GetExpectedBulkTH();
                for (int i = 0; i < result.Count(); i++)
                {
                    Assert.Equal(expected[i], result.ElementAt(i).ValueInWords);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        private string[] GetExpectedBulk()
        {
            return new string[]
            {
                "Seven Hundred Eighty Four Thousand One Hundred Ninety Two and 42/100",
                "One Million Fifty Three Thousand Eight Hundred Twenty and 65/100",
                "Sixteen Thousand Six Hundred Fifty Three and 30/100",
                "Two Hundred Sixty One Thousand One Hundred Thirteen and 78/100",
                "Four Million Two Hundred Fifty Nine Thousand Twenty Five and 49/100",
                "Nine Hundred Thirty Five Thousand One Hundred and 00/100",
                "Thirty One Thousand Eighty Eight and 72/100",
                "Twenty Seven Thousand Two Hundred Seventy Two and 59/100",
                "Eight Thousand Six Hundred Twenty Four and 05/100",
                "One Million Four Hundred Thousand and 00/100",
                "Zero and 00/100",
                "Fifteen Thousand Ninety Three and 02/100",
                "One Hundred Nine Thousand One Hundred Twelve and 65/100",
                "Two Hundred Ninety Six Thousand Forty Three and 92/100",
                "Twenty Nine Thousand Seven Hundred Thirty Six and 24/100",
                "One Hundred Thirty Five Thousand One Hundred Three and 27/100",
                "One Million Seven Hundred Sixty Five Thousand Eighty Five and 54/100",
                "One Million Two Hundred Eighty Two Thousand Three Hundred Nineteen and 86/100",
                "Four Hundred Seventy Five Thousand Nine Hundred Six and 24/100",
                "Three Hundred Six Thousand Two Hundred Ninety Eight and 79/100",
                "Fifteen Million One Hundred Seventy Eight Thousand Eleven and 41/100",
                "Two Hundred Forty Nine Thousand Three Hundred Thirty Four and 65/100",
                "Two Hundred Forty Thousand and 00/100",
                "Six Hundred Sixty Two Thousand Seven Hundred Eighty Six and 93/100",
                "Four Million Two Hundred Fifty Nine Thousand Twenty Five and 49/100",
                "Nine Hundred Ninety Eight Thousand Five Hundred Seventy Six and 78/100",
            };
        }
        private string[] GetExpectedBulkTH()
        {
            return new string[]
            {
                "เจ็ดแสนแปดหมื่นสี่พันหนึ่งร้อยเก้าสิบสองบาทสี่สิบสองสตางค์",
                "หนึ่งล้านห้าหมื่นสามพันแปดร้อยยี่สิบบาทหกสิบห้าสตางค์",
                "หนึ่งหมื่นหกพันหกร้อยห้าสิบสามบาทสามสิบสตางค์",
                "สองแสนหกหมื่นหนึ่งพันหนึ่งร้อยสิบสามบาทเจ็ดสิบแปดสตางค์",
                "สี่ล้านสองแสนห้าหมื่นเก้าพันยี่สิบห้าบาทสี่สิบเก้าสตางค์",
                "เก้าแสนสามหมื่นห้าพันหนึ่งร้อยบาทถ้วน",
                "สามหมื่นหนึ่งพันแปดสิบแปดบาทเจ็ดสิบสองสตางค์",
                "สองหมื่นเจ็ดพันสองร้อยเจ็ดสิบสองบาทห้าสิบเก้าสตางค์",
                "แปดพันหกร้อยยี่สิบสี่บาทห้าสตางค์",
                "หนึ่งล้านสี่แสนบาทถ้วน",
                "ศูนย์บาทถ้วน",
                "หนึ่งหมื่นห้าพันเก้าสิบสามบาทสองสตางค์",
                "หนึ่งแสนเก้าพันหนึ่งร้อยสิบสองบาทหกสิบห้าสตางค์",
                "สองแสนเก้าหมื่นหกพันสี่สิบสามบาทเก้าสิบสองสตางค์",
                "สองหมื่นเก้าพันเจ็ดร้อยสามสิบหกบาทยี่สิบสี่สตางค์",
                "หนึ่งแสนสามหมื่นห้าพันหนึ่งร้อยสามบาทยี่สิบเจ็ดสตางค์",
                "หนึ่งล้านเจ็ดแสนหกหมื่นห้าพันแปดสิบห้าบาทห้าสิบสี่สตางค์",
                "หนึ่งล้านสองแสนแปดหมื่นสองพันสามร้อยสิบเก้าบาทแปดสิบหกสตางค์",
                "สี่แสนเจ็ดหมื่นห้าพันเก้าร้อยหกบาทยี่สิบสี่สตางค์",
                "สามแสนหกพันสองร้อยเก้าสิบแปดบาทเจ็ดสิบเก้าสตางค์",
                "สิบห้าล้านหนึ่งแสนเจ็ดหมื่นแปดพันสิบเอ็ดบาทสี่สิบเอ็ดสตางค์",
                "สองแสนสี่หมื่นเก้าพันสามร้อยสามสิบสี่บาทหกสิบห้าสตางค์",
                "สองแสนสี่หมื่นบาทถ้วน",
                "หกแสนหกหมื่นสองพันเจ็ดร้อยแปดสิบหกบาทเก้าสิบสามสตางค์",
                "สี่ล้านสองแสนห้าหมื่นเก้าพันยี่สิบห้าบาทสี่สิบเก้าสตางค์",
                "เก้าแสนเก้าหมื่นแปดพันห้าร้อยเจ็ดสิบหกบาทเจ็ดสิบแปดสตางค์",
            };
        }
    }
}
