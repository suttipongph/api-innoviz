﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.Shared.NumberSequence
{
    public class GetNumberUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GetNumberUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetNumberUnitTest");
        }
        [Fact]
        public void GetNumber()
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/NumberSequence/masterdata.json");
                INumberSequenceService numberSeqService = new NumberSequenceService(db);
                INumberSeqTableRepo numberSeqTableRepo = new NumberSeqTableRepo(db);
                Company company = db.Company.FirstOrDefault();
                NumberSeqTable receiptTempNumberSeq = numberSeqTableRepo.GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(company.CompanyGUID, ReferenceId.ReceiptTemp);
                NumberSeqTable purchaseNumberSeq = numberSeqTableRepo.GetNumberSeqTableByRefIdAndCompanyGUIDNoTracking(company.CompanyGUID, ReferenceId.Purchase);

                List<Guid> receiptTempTableGuids = new List<Guid>() { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
                List<Guid> purchaseTableGuids = new List<Guid>() { Guid.NewGuid(), Guid.NewGuid() };

                var receiptTempNumberSeqs = receiptTempTableGuids.Select(s => new NumberSequences
                {
                    Key = s,
                    BranchGUID = null,
                    CompanyGUID = company.CompanyGUID,
                    NumberSeqTableGUID = receiptTempNumberSeq.NumberSeqTableGUID
                });
                var purchaseNumberSeqs = purchaseTableGuids.Select(s => new NumberSequences
                {
                    Key = s,
                    BranchGUID = null,
                    CompanyGUID = company.CompanyGUID,
                    NumberSeqTableGUID = purchaseNumberSeq.NumberSeqTableGUID
                });
                List<NumberSequences> parm = receiptTempNumberSeqs.Concat(purchaseNumberSeqs).ToList();
                var result = numberSeqService.GetNumber(parm);
                foreach (var item in result)
                {
                    output.WriteLine(item.GeneratedId);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        
    }
}
