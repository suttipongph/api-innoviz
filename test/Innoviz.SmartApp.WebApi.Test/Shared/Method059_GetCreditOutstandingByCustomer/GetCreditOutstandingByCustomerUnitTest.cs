﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method059_GetCreditOutstandingByCustomer
{
    public class GetCreditOutstandingByCustomerUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GetCreditOutstandingByCustomerUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetCreditOutstandingByCustomer");
        }
        [Theory]
        [MemberData(nameof(CaseGetCreditOutstandingByCustomer))]
        public void GetCreditOutstandingByCustomer(string customerTableGUID, DateTime asOfDate)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method059_GetCreditOutstandingByCustomer/masterdata.json");
                Guid CustomerTableGUID = new Guid(customerTableGUID);
                CreditAppTableRepo creditAppTableRepo = new CreditAppTableRepo(db);
                var expectedCreditOutstanding = creditAppTableRepo.GetCreditOutstandingByCustomer(new Guid(customerTableGUID), DateTime.Now);


                Assert.True(expectedCreditOutstanding.All(a => a.CustomerTableGUID == CustomerTableGUID));
                Assert.True(expectedCreditOutstanding.All(a => a.ExpiryDate > asOfDate));
                Assert.True(expectedCreditOutstanding.All(a => a.ReserveToBeRefund == ((a.ProductType != 1)
                                                                                   ? 0 : a.ReserveToBeRefund )));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGetCreditOutstandingByCustomer
        {
            get
            {
                return new[]
                {
                    new object[] { "DB42159B-3AF2-43A6-8D64-E312B4E3431C", DateTime.Now }
                };
            }
        }
    }
}
