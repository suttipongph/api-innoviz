﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method115_GenStagingTableByProcessTransType
{
    public class GenStagingTableByProcessTransTypeUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GenStagingTableByProcessTransTypeUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GenStagingTableByProcessTransType");
        }
        [Theory]
        [MemberData(nameof(CaseGenStagingTable))]
        public void GenStagingTableByProcessTransType(GenStagingTableParameter parameter, GenStagingTableResult expectedResult)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method115_GenStagingTableByProcessTransType/masterdata.json", true, false, true); 
                IStagingTableService stagingTableService = new StagingTableService(db);

                #region prepare expected results

                if (expectedResult.StagingTable == null) expectedResult.StagingTable = new List<StagingTable>();
                if (expectedResult.StagingTableVendorInfo == null) expectedResult.StagingTableVendorInfo = new List<StagingTableVendorInfo>();
                if (expectedResult.ProcessTrans == null) expectedResult.ProcessTrans = new List<ProcessTrans>();

                #endregion prepare expected results

                GenStagingTableResult actualResult = stagingTableService.GenStagingTableByProcessTransType(parameter);

                #region prepare actual result
                List<StagingTable> actualStagingTable = actualResult.StagingTable;
                List<StagingTableVendorInfo> actualStagingTableVendorInfo = actualResult.StagingTableVendorInfo;
                List<ProcessTrans> actualProcessTrans = actualResult.ProcessTrans.OrderBy(o => o.ProcessTransGUID).ToList();

                Assert.True(expectedResult.StagingTable.Count == actualStagingTable.Count);
                Assert.True(expectedResult.StagingTableVendorInfo.Count == actualStagingTableVendorInfo.Count);
                Assert.True(expectedResult.ProcessTrans.Count == actualProcessTrans.Count);
                #endregion prepare actual result

                #region assert should equal
                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(StagingTable), new string[] { "StagingTableGUID", "StagingBatchId" } },
                    { typeof(StagingTableVendorInfo), new string[] { "StagingTableVendorInfoGUID" } },
                    { typeof(ProcessTrans), new string[] { "StagingBatchId" } }
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedResult.StagingTable, actualStagingTable);
                helper.AssertShouldEqual(expectedResult.StagingTableVendorInfo, actualStagingTableVendorInfo);
                helper.AssertShouldEqual(expectedResult.ProcessTrans, actualProcessTrans);
                #endregion assert should equal
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGenStagingTable
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm("Case1_Invoice"), GetResult("Case1_Invoice") },
                    new object[] { GetParm("Case2_Payment_PaymentDetail"), GetResult("Case2_Payment_PaymentDetail") },
                    new object[] { GetParm("Case3_IncomeRealization"), GetResult("Case3_IncomeRealization") },
                    new object[] { GetParm("Case4_VendorPayment"), GetResult("Case4_VendorPayment") }
                };
            }
        }
        public static GenStagingTableParameter GetParm(string folder)
        {
            string path = "../../../Shared/Method115_GenStagingTableByProcessTransType/" + folder + "/parameter.json";
            GenStagingTableParameter parm = TestHelper.JsonToObject<GenStagingTableParameter>(path);
            return parm;
        }
        public static GenStagingTableResult GetResult(string folder)
        {
            string path = "../../../Shared/Method115_GenStagingTableByProcessTransType/" + folder + "/result.json";
            GenStagingTableResult expectedResult = TestHelper.JsonToObject<GenStagingTableResult>(path);
            return expectedResult;
        }
    }
}
