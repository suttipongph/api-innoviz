﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Shared.GetListInvoiceOutstanding
{
    public class GetListInvoiceOutstandingUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db; 
        public GetListInvoiceOutstandingUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetListInvoiceOutstanding");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, false);
        }
        [Theory]
        [MemberData(nameof(CaseGetListInvoiceOutstanding))]
        public void GetListInvoiceOutstanding(SearchPredicate predicate, Result expectedResult)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/GetListInvoiceOutstanding/masterdata.json");
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);

                #region prepare expected results
                if (expectedResult.InvoiceOutstandingListView == null) expectedResult.InvoiceOutstandingListView = new List<InvoiceOutstandingListView>();
                #endregion

                List<InvoiceOutstandingListView> invoiceOutstandingListViews =
                    invoiceTableRepo.GetListOutstandingvw(predicate);

                TestHelper helper = new TestHelper();
                helper.AssertShouldEqual(expectedResult.InvoiceOutstandingListView, invoiceOutstandingListViews);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGetListInvoiceOutstanding
        {
            get
            {
                return new[]
                {
                    new object[] {
                        new SearchPredicate {
                            Predicates = "ExitsInvoice == @0 AND InvoiceTable_CustomerTableGUID == @1 and SuspenseInvoiceType == @2 and (ProductInvoice == @3 or (ProductInvoice == @4 and ProductType == @5))",
                            Values = new object[]{ "false", "C0000000-0000-0000-0000-100000000000", "0" , "false" , "true" , "0"  }
                        },
                        GetResult("Case1")
                    },
                    new object[] {
                        new SearchPredicate {
                            Predicates = "ExitsInvoice == @0 AND InvoiceTable_CustomerTableGUID == @1 and SuspenseInvoiceType == @2 and ProductInvoice == @3 and ProductType == @4",
                            Values = new object[]{ "false", "C0000000-0000-0000-0000-100000000001", "0" , "true" , "0"  }
                        },
                        GetResult("Case2")
                    },
                    new object[] {
                        new SearchPredicate {
                            Predicates = "ExitsInvoice == @0 AND InvoiceTable_CustomerTableGUID == @1 and SuspenseInvoiceType == @2 and ProductInvoice == @3",
                            Values = new object[]{ "false", "C0000000-0000-0000-0000-100000000002", "0" , "false" }
                        },
                        GetResult("Case3")
                    },
                };
            }
        }
        public static Result GetResult(string folder)
        {
            string path = "../../../Shared/GetListInvoiceOutstanding/" + folder + "/result.json";
            Result expectedResult = TestHelper.JsonToObject<Result>(path);
            return expectedResult;
        }

        public class Result
        {
            public List<InvoiceOutstandingListView> InvoiceOutstandingListView { get; set; }
        }
    }
}
