﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.Shared.Method101_GetRetentionOutstandingByCustomer
{
    public class GetRetentionOutstandingByCustomerUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GetRetentionOutstandingByCustomerUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetRetentionOutstandingByCustomer");
        }
        [Theory]
        [MemberData(nameof(CaseGetRetentionOutstandingByCustomer))]
        public void GetRetentionOutstandingByCustomer(string customerTableGUID)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Shared/Method101_GetRetentionOutstandingByCustomer/masterdata.json");

                IRetentionTransRepo retentionTranRepo = new RetentionTransRepo(db);
                Guid CustomerTableGUID = new Guid(customerTableGUID);
                var expectedRetentionTrans = retentionTranRepo.GetRetentionOutstandingByCustomer(CustomerTableGUID);
                Assert.True(expectedRetentionTrans.All(a => a.CustomerTableGUID == CustomerTableGUID));
                Assert.True(expectedRetentionTrans.All(a => a.RemainingAmount == ((a.MaximumRetention > 0) ? a.MaximumRetention - a.AccumRetentionAmount: 0)));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGetRetentionOutstandingByCustomer
        {
            get
            {
                return new[]
                {
                    new object[] { "90ae571e-b816-4d3d-b3b9-88c376800ab0" }
                };
            }
        }
    }
}
