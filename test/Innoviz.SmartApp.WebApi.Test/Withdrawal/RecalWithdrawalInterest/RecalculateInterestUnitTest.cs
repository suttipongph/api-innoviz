﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.Withdrawal.RecalWithdrawalInterest
{
    public class RecalculateInterestUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public RecalculateInterestUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "RecalWithdrawalInterest");
        }
        [Theory]
        [MemberData(nameof(CaseRecalWithdrawalInterest))]
        public void RecalWithdrawalInterest(RecalWithdrawalInterestParamView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Withdrawal/RecalWithdrawalInterest/Case_ExampleData/masterdata.json");

                WithdrawalTable expectedWithdrawalTable = result.WithdrawalTable.FirstOrDefault();
                List<WithdrawalLine> expectedLines = result.WithdrawalLine.OrderBy(o => o.LineNum).ToList();
                
                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                RecalWithdrawalInterestViewMap actual = withdrawalTableService.InitRecalWithdrawalInterest(parm);

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(WithdrawalTable), new string[] { "WithdrawalTableGUID" } },
                    { typeof(WithdrawalLine), new string[] { "WithdrawalLineGUID" } }
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedWithdrawalTable, actual.WithdrawalTable);
                helper.AssertShouldEqual(expectedLines, actual.WithdrawalLines.OrderBy(o => o.LineNum).ToList());

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseRecalWithdrawalInterest
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm(), GetResult() }
                };
            }
        }
        public static RecalWithdrawalInterestParamView GetParm()
        {
            string path = "../../../Withdrawal/RecalWithdrawalInterest/Case_ExampleData/parameter.json";
            RecalWithdrawalInterestParamView parm = TestHelper.JsonToObject<RecalWithdrawalInterestParamView>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../Withdrawal/RecalWithdrawalInterest/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<WithdrawalTable> WithdrawalTable { get; set; }
        public List<WithdrawalLine> WithdrawalLine { get; set; }
    }
}
