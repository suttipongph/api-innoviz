﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Withdrawal.PostWithdrawal
{
    public class PostWithdrawalUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostWithdrawalUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostWithdrawal");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }

        [Theory]
        [MemberData(nameof(CasePostWithdrawal))]
        public void PostWithdrawal(PostWithdrawalView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Withdrawal/PostWithdrawal/masterdata.json");

                WithdrawalTable expectedWithdrawalTable = result.WithdrawalTable.FirstOrDefault();
                List<WithdrawalLine> expectedWithdrawalLines = result.WithdrawalLine.OrderBy(o => o.WithdrawalTableGUID).ThenBy(o => o.WithdrawalLineGUID).ToList();
                ReceiptTempTable expectedReceiptTempTable = result.ReceiptTempTable.FirstOrDefault();
                List<InvoiceTable> expectedInvoiceTables = result.InvoiceTable.OrderBy(o => o.InvoiceId).ToList();
                List<InvoiceLine> expectedInvoiceLines = (from invoiceLine in result.InvoiceLine
                                                          join invoiceTable in result.InvoiceTable
                                                          on invoiceLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceLine
                                                          from invoiceTable in ljInvoiceTableInvoiceLine.DefaultIfEmpty()
                                                          select new
                                                          {
                                                              invoiceLine,
                                                              invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : invoiceLine.InvoiceTableGUID.ToString()
                                                          }).OrderBy(o => o.invoiceId).Select(s => s.invoiceLine).ToList();
                List<CustTrans> expectedCustTranses = (from custTrans in result.CustTrans
                                                       join invoiceTable in result.InvoiceTable
                                                       on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableCustTrans
                                                       from invoiceTable in ljInvoiceTableCustTrans.DefaultIfEmpty()
                                                       select new
                                                       {
                                                           custTrans,
                                                           invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : custTrans.InvoiceTableGUID.ToString()
                                                       }).OrderBy(o => o.invoiceId).Select(s => s.custTrans).ToList();
                List<CustTrans> expectedCustTranses_Update = result.CustTrans_Update != null ? result.CustTrans_Update.OrderBy(o => o.InvoiceTableGUID).ToList() : new List<CustTrans>();
                List<CreditAppTrans> expectedCreditAppTranses = result.CreditAppTrans;
                List<ProcessTrans> expectedProcessTranses = (from processTrans in result.ProcessTrans
                                                             join invoiceTable in result.InvoiceTable
                                                             on new { processTrans.RefType, InvoiceTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.Invoice, invoiceTable.InvoiceTableGUID } into ljInvoiceTableProcessTrans
                                                             from invoiceTable in ljInvoiceTableProcessTrans.DefaultIfEmpty()
                                                             join vendorPaymentTrans in result.VendorPaymentTrans
                                                             on new { processTrans.RefType, VendorPaymentTransGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.VendorPaymentTrans, vendorPaymentTrans.VendorPaymentTransGUID } into ljVendorPaymentTransProcessTrans
                                                             from vendorPaymentTrans in ljVendorPaymentTransProcessTrans.DefaultIfEmpty()
                                                             join receiptTempTable in result.ReceiptTempTable
                                                             on new { processTrans.RefType, ReceiptTempTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.ReceiptTemp, receiptTempTable.ReceiptTempTableGUID } into ljReceiptTempTableProcessTrans
                                                             from receiptTempTable in ljReceiptTempTableProcessTrans.DefaultIfEmpty()
                                                             select new
                                                             {
                                                                 processTrans,
                                                                 processTrans.RefType,
                                                                 RefId = invoiceTable != null ? invoiceTable.InvoiceId : (vendorPaymentTrans != null ? vendorPaymentTrans.VendorTableGUID.ToString() : (receiptTempTable != null ? receiptTempTable.ReceiptTempId : string.Empty))
                                                             }).OrderBy(o => o.RefType).ThenBy(t => t.RefId).ThenBy(t => t.processTrans.CreditAppTableGUID).Select(s => s.processTrans).ToList();
                List<RetentionTrans> expectedRetentionTranses = result.RetentionTrans;
                List<VendorPaymentTrans> expectedVendorPaymentTranses = result.VendorPaymentTrans.OrderBy(o => o.VendorTableGUID).ToList();
                List<InvoiceSettlementDetail> expectedInvoiceSettlementDetails = (from invoiceSettlementDetail in result.InvoiceSettlementDetail
                                                                                  join invoiceTable in result.InvoiceTable
                                                                                  on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
                                                                                  from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
                                                                                  select new
                                                                                  {
                                                                                      invoiceSettlementDetail,
                                                                                      invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : invoiceSettlementDetail.InvoiceTableGUID.ToString()
                                                                                  }).OrderBy(o => o.invoiceId).Select(s => s.invoiceSettlementDetail).ToList();
                List<InterestRealizedTrans> expectedInterestRealizedTranses = result.InterestRealizedTrans;
                List<ReceiptTempPaymDetail> expectedReceiptTempPaymDetails = result.ReceiptTempPaymDetail;
                List<ReceiptTable> expectedReceiptTable = result.ReceiptTable.OrderBy(o => o.ReceiptId).ToList();
                List<ReceiptLine> expectedReceiptLine = (from receiptLine in result.ReceiptLine
                                                         join receiptTable in result.ReceiptTable
                                                         on receiptLine.ReceiptTableGUID equals receiptTable.ReceiptTableGUID into ljReceiptTableReceiptLine
                                                         from receiptTable in ljReceiptTableReceiptLine.DefaultIfEmpty()
                                                         select new
                                                         {
                                                             receiptLine,
                                                             receiptId = receiptTable != null ? receiptTable.ReceiptId : string.Empty
                                                         }).OrderBy(o => o.receiptId).Select(s => s.receiptLine).ToList();
                List<TaxInvoiceTable> expectedTaxInvoiceTable = result.TaxInvoiceTable.OrderBy(o => o.TaxInvoiceId).ToList();
                List<TaxInvoiceLine> expectedTaxInvoiceLine = (from taxInvoiceLine in result.TaxInvoiceLine
                                                               join taxInvoiceTable in result.TaxInvoiceTable
                                                               on taxInvoiceLine.TaxInvoiceTableGUID equals taxInvoiceTable.TaxInvoiceTableGUID into ljTaxInvoiceTableTaxInvoiceLine
                                                               from taxInvoiceTable in ljTaxInvoiceTableTaxInvoiceLine.DefaultIfEmpty()
                                                               select new
                                                               {
                                                                   taxInvoiceLine,
                                                                   taxInvoiceId = taxInvoiceTable != null ? taxInvoiceTable.TaxInvoiceId : string.Empty
                                                               }).OrderBy(o => o.taxInvoiceId).Select(s => s.taxInvoiceLine).ToList();
                List<PaymentHistory> expectedPaymentHistory = (from paymentHistory in result.PaymentHistory
                                                               join invoiceTable in result.InvoiceTable
                                                               on paymentHistory.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTablePaymentHistory
                                                               from invoiceTable in ljInvoiceTablePaymentHistory.DefaultIfEmpty()
                                                               select new
                                                               {
                                                                   paymentHistory,
                                                                   invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
                                                               }).OrderBy(o => o.invoiceId).ThenBy(t => t.paymentHistory.PaymentBaseAmount).Select(s => s.paymentHistory).ToList();
                List<IntercompanyInvoiceTable> expectedIntercompanyInvoiceTable = result.IntercompanyInvoiceTable.OrderBy(o => o.IntercompanyInvoiceId).ToList();

                IWithdrawalTableService withdrawalTableService = new WithdrawalTableService(db);
                //withdrawalTableService.PostWithdrawal(parm);
                PostWithdrawalViewMap actual = withdrawalTableService.InitPostWithdrawal(parm);

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(WithdrawalTable), new string[] { "WithdrawalTableGUID", "ReceiptTempTableGUID" } },
                    { typeof(WithdrawalLine), new string[] { "WithdrawalLineGUID", "WithdrawalLineInvoiceTableGUID" } },
                    { typeof(ReceiptTempTable), new string[] { "ReceiptTempTableGUID" } },
                    { typeof(InvoiceTable), new string[] { "InvoiceTableGUID" } },
                    { typeof(InvoiceLine), new string[] { "InvoiceLineGUID", "InvoiceTableGUID" } },
                    { typeof(CustTrans), new string[] { "CustTransGUID", "InvoiceTableGUID" } },
                    { typeof(CreditAppTrans), new string[] { "CreditAppTransGUID", "RefGUID" } },
                    { typeof(ProcessTrans), new string[] { "ProcessTransGUID", "RefGUID", "InvoiceTableGUID", "PaymentHistoryGUID", "RefTaxInvoiceGUID" } },
                    { typeof(RetentionTrans), new string[] { "RetentionTransGUID" } },
                    { typeof(VendorPaymentTrans), new string[] { "VendorPaymentTransGUID" } },
                    { typeof(InvoiceSettlementDetail), new string[] { "InvoiceSettlementDetailGUID", "InvoiceTableGUID", "RefGUID", "RefReceiptTempPaymDetailGUID" } },
                    { typeof(InterestRealizedTrans), new string[] { "InterestRealizedTransGUID" } },
                    { typeof(ReceiptTempPaymDetail), new string[] { "ReceiptTempPaymDetailGUID", "ReceiptTempTableGUID" } },
                    { typeof(ReceiptTable), new string[] { "ReceiptTableGUID", "InvoiceSettlementDetailGUID", "RefGUID" } },
                    { typeof(ReceiptLine), new string[] { "ReceiptLineGUID", "ReceiptTableGUID", "InvoiceTableGUID" } },
                    { typeof(TaxInvoiceTable), new string[] { "TaxInvoiceTableGUID", "TaxInvoiceRefGUID", "InvoiceTableGUID" } },
                    { typeof(TaxInvoiceLine), new string[] { "TaxInvoiceLineGUID", "TaxInvoiceTableGUID" } },
                    { typeof(PaymentHistory), new string[] { "PaymentHistoryGUID", "RefGUID", "CustTransGUID", "InvoiceSettlementDetailGUID", "ReceiptTableGUID", "InvoiceTableGUID" } },
                    { typeof(IntercompanyInvoiceTable), new string[] { "IntercompanyInvoiceTableGUID" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedWithdrawalTable, actual.WithdrawalTable);
                helper.AssertShouldEqual(expectedWithdrawalLines, actual.WithdrawalLine);
                helper.AssertShouldEqual(expectedReceiptTempTable, actual.ReceiptTempTable);
                helper.AssertShouldEqual(expectedInvoiceTables, actual.InvoiceTable);
                helper.AssertShouldEqual(expectedInvoiceLines, actual.InvoiceLine);
                helper.AssertShouldEqual(expectedCustTranses, actual.CustTrans);
                helper.AssertShouldEqual(expectedCustTranses_Update, actual.CustTrans_Update);
                helper.AssertShouldEqual(expectedCreditAppTranses, actual.CreditAppTrans);
                helper.AssertShouldEqual(expectedProcessTranses, actual.ProcessTrans);
                helper.AssertShouldEqual(expectedRetentionTranses, actual.RetentionTrans);
                helper.AssertShouldEqual(expectedVendorPaymentTranses, actual.VendorPaymentTrans);
                helper.AssertShouldEqual(expectedInvoiceSettlementDetails, actual.InvoiceSettlementDetail);
                helper.AssertShouldEqual(expectedInterestRealizedTranses, actual.InterestRealizedTrans);
                helper.AssertShouldEqual(expectedReceiptTempPaymDetails, actual.ReceiptTempPaymDetail);
                helper.AssertShouldEqual(expectedReceiptTable, actual.ReceiptTable);
                helper.AssertShouldEqual(expectedReceiptLine, actual.ReceiptLine);
                helper.AssertShouldEqual(expectedTaxInvoiceTable, actual.TaxInvoiceTable);
                helper.AssertShouldEqual(expectedTaxInvoiceLine, actual.TaxInvoiceLine);
                helper.AssertShouldEqual(expectedPaymentHistory, actual.PaymentHistory);
                helper.AssertShouldEqual(expectedIntercompanyInvoiceTable, actual.IntercompanyInvoiceTable);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostWithdrawal
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm("Case1"), GetResult("Case1") },
                    new object[] {GetParm("Case2"), GetResult("Case2") }
                };
            }
        }
        public static PostWithdrawalView GetParm(string folder)
        {
            string path = "../../../Withdrawal/PostWithdrawal/"+ folder + "/parameter.json";
            PostWithdrawalView parm = TestHelper.JsonToObject<PostWithdrawalView>(path);
            return parm;
        }
        public static Result GetResult(string folder)
        {
            string path = "../../../Withdrawal/PostWithdrawal/" + folder + "/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<WithdrawalTable> WithdrawalTable { get; set; }
        public List<WithdrawalLine> WithdrawalLine { get; set; }
        public List<ReceiptTempTable> ReceiptTempTable { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<CustTrans> CustTrans_Update { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
        public List<VendorPaymentTrans> VendorPaymentTrans { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
        public List<InterestRealizedTrans> InterestRealizedTrans { get; set; }
        public List<ReceiptTempPaymDetail> ReceiptTempPaymDetail { get; set; }
        public List<ReceiptTable> ReceiptTable { get; set; }
        public List<ReceiptLine> ReceiptLine { get; set; }
        public List<PaymentHistory> PaymentHistory { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; }
    }
}
