﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.FreeTextInvoice.PostFreeTextInvoice
{
    public class PostFreeTextInvoiceUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostFreeTextInvoiceUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostFreeTextInvoice");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }

        [Theory]
        [MemberData(nameof(CasePostFreeTextInvoice))]
        public void PostFreeTextInvoice(PostFreeTextInvoiceView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../FreeTextInvoice/PostFreeTextInvoice/Case_ExampleData/masterdata.json");

                FreeTextInvoiceTable expectedFreeTextInvoiceTable = result.FreeTextInvoiceTable.FirstOrDefault();
                List<InvoiceTable> expectedInvoiceTables = result.InvoiceTable.OrderBy(o => o.InvoiceId).ToList();
                List<InvoiceLine> expectedInvoiceLines = (from invoiceLine in result.InvoiceLine
                                                          join invoiceTable in result.InvoiceTable
                                                          on invoiceLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceLine
                                                          from invoiceTable in ljInvoiceTableInvoiceLine.DefaultIfEmpty()
                                                          select new
                                                          {
                                                              invoiceLine,
                                                              invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : invoiceLine.InvoiceTableGUID.ToString()
                                                          }).OrderBy(o => o.invoiceId).Select(s => s.invoiceLine).ToList();
                List<CustTrans> expectedCustTranses = (from custTrans in result.CustTrans
                                                       join invoiceTable in result.InvoiceTable
                                                       on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableCustTrans
                                                       from invoiceTable in ljInvoiceTableCustTrans.DefaultIfEmpty()
                                                       select new
                                                       {
                                                           custTrans,
                                                           invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : custTrans.InvoiceTableGUID.ToString()
                                                       }).OrderBy(o => o.invoiceId).Select(s => s.custTrans).ToList();
                List<CreditAppTrans> expectedCreditAppTranses = (result.CreditAppTrans != null) ? result.CreditAppTrans : new List<CreditAppTrans>();
                List<ProcessTrans> expectedProcessTranses = (from processTrans in result.ProcessTrans
                                                             join invoiceTable in result.InvoiceTable
                                                             on new { processTrans.RefType, InvoiceTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.Invoice, invoiceTable.InvoiceTableGUID } into ljInvoiceTableProcessTrans
                                                             from invoiceTable in ljInvoiceTableProcessTrans.DefaultIfEmpty()
                                                             select new
                                                             {
                                                                 processTrans,
                                                                 processTrans.RefType,
                                                                 RefId = invoiceTable != null ? invoiceTable.InvoiceId : string.Empty
                                                             }).OrderBy(o => o.RefType).ThenBy(t => t.RefId).ThenBy(t => t.processTrans.CreditAppTableGUID).Select(s => s.processTrans).ToList();
                List<RetentionTrans> expectedRetentionTranses = (result.RetentionTrans != null) ? result.RetentionTrans: new List<RetentionTrans>();
                List<TaxInvoiceTable> expectedTaxInvoiceTable = (result.TaxInvoiceTable != null) ? result.TaxInvoiceTable.OrderBy(o => o.TaxInvoiceId).ToList() : new List<TaxInvoiceTable>();
                List<TaxInvoiceLine> expectedTaxInvoiceLine = (result.TaxInvoiceLine != null) ? (from taxInvoiceLine in result.TaxInvoiceLine
                                                                                                 join taxInvoiceTable in result.TaxInvoiceTable
                                                                                                 on taxInvoiceLine.TaxInvoiceTableGUID equals taxInvoiceTable.TaxInvoiceTableGUID into ljTaxInvoiceTableTaxInvoiceLine
                                                                                                 from taxInvoiceTable in ljTaxInvoiceTableTaxInvoiceLine.DefaultIfEmpty()
                                                                                                 select new
                                                                                                 {
                                                                                                     taxInvoiceLine,
                                                                                                     taxInvoiceId = taxInvoiceTable != null ? taxInvoiceTable.TaxInvoiceId : string.Empty
                                                                                                 }).OrderBy(o => o.taxInvoiceId).Select(s => s.taxInvoiceLine).ToList() : new List<TaxInvoiceLine>();

                IFreeTextInvoiceTableService freeTextInvoiceTableService = new FreeTextInvoiceTableService(db);
                //freeTextInvoiceTableService.PostFreeTextInvoice(parm);
                PostFreeTextInvoiceViewMap actual = freeTextInvoiceTableService.InitPostFreeTextInvoice(parm);

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(FreeTextInvoiceTable), new string[] { "FreeTextInvoiceTableGUID", "InvoiceTableGUID" } },
                    { typeof(InvoiceTable), new string[] { "InvoiceTableGUID" } },
                    { typeof(InvoiceLine), new string[] { "InvoiceLineGUID", "InvoiceTableGUID" } },
                    { typeof(CustTrans), new string[] { "CustTransGUID", "InvoiceTableGUID" } },
                    { typeof(CreditAppTrans), new string[] { "CreditAppTransGUID", "RefGUID" } },
                    { typeof(ProcessTrans), new string[] { "ProcessTransGUID", "RefGUID", "InvoiceTableGUID", "PaymentHistoryGUID", "RefTaxInvoiceGUID" } },
                    { typeof(RetentionTrans), new string[] { "RetentionTransGUID" } },
                    { typeof(TaxInvoiceTable), new string[] { "TaxInvoiceTableGUID", "TaxInvoiceRefGUID", "InvoiceTableGUID" } },
                    { typeof(TaxInvoiceLine), new string[] { "TaxInvoiceLineGUID", "TaxInvoiceTableGUID" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedFreeTextInvoiceTable, actual.FreeTextInvoiceTable);
                helper.AssertShouldEqual(expectedInvoiceTables, actual.InvoiceTable);
                helper.AssertShouldEqual(expectedInvoiceLines, actual.InvoiceLine);
                helper.AssertShouldEqual(expectedCustTranses, actual.CustTrans);
                helper.AssertShouldEqual(expectedCreditAppTranses, actual.CreditAppTrans);
                helper.AssertShouldEqual(expectedProcessTranses, actual.ProcessTrans);
                helper.AssertShouldEqual(expectedRetentionTranses, actual.RetentionTrans);
                helper.AssertShouldEqual(expectedTaxInvoiceTable, actual.TaxInvoiceTable);
                helper.AssertShouldEqual(expectedTaxInvoiceLine, actual.TaxInvoiceLine);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostFreeTextInvoice
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm(), GetResult() }
                };
            }
        }
        public static PostFreeTextInvoiceView GetParm()
        {
            string path = "../../../FreeTextInvoice/PostFreeTextInvoice/Case_ExampleData/parameter.json";
            PostFreeTextInvoiceView parm = TestHelper.JsonToObject<PostFreeTextInvoiceView>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../FreeTextInvoice/PostFreeTextInvoice/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<FreeTextInvoiceTable> FreeTextInvoiceTable { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
    }
}
