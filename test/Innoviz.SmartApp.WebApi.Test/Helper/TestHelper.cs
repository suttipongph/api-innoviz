﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Innoviz.SmartApp.WebApi.Test.Helper
{
    public class TestHelper
    {
        private Dictionary<Type, string[]> fieldsToIgnore;
        private Stopwatch sw;
        #region ctor
        public TestHelper() { }
        public TestHelper(Dictionary<Type, string[]> fieldsToIgnore)
        {
            this.fieldsToIgnore = fieldsToIgnore;
        }
        #endregion
        #region init
        #region config
        private static IConfigurationRoot GetConfig()
        {
            var con = SystemStaticData.GetConfiguration();
            if (con == null)
            {
                var projectDir = Directory.GetCurrentDirectory();
                var configPath = Path.Combine(projectDir, "appsettings.test.json");
                var config = new ConfigurationBuilder()
                                    .SetBasePath(projectDir)
                                    .AddJsonFile(configPath)
                                    .Build();
                SystemStaticData.SetConfiguration(config);
                return config;
            }
            else
            {
                return con;
            }
            
        }
        public static void InitSystemValues(ITestOutputHelper output)
        {
            var config = GetConfig();
            var testLogLevel = GetConfigurationValue("TestLogLevel");
            if(output != null && testLogLevel == "Debug")
            {
                Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                // add the xunit test output sink to the serilog logger
                // https://github.com/trbenning/serilog-sinks-xunit#serilog-sinks-xunit
                .WriteTo.TestOutput(output)
                .CreateLogger();
            }
        }
        public static string GetConfigurationValue(string key)
        {
            IConfigurationRoot configuration = GetConfig();
            string result = configuration.GetValue<string>(key);
            return result;
        }
        public static List<string> GetConfigurationValues(string key)
        {
            IConfigurationRoot configuration = SystemStaticData.GetConfiguration();
            IConfigurationSection section = configuration.GetSection(key);
            return section.GetChildren().Select(a => a.Value).ToList();
        }
        public static SystemParameter GetSystemParameter()
        {
            SystemParameter sysParm = new SystemParameter
            {
                AccessLevel = new AccessLevelParm { AccessLevel = (int)AccessLevel.Company },
                BusinessUnitGUID = "00000000-0000-0000-0000-000000000001",
                CompanyGUID = "0a099e6f-9ba8-4a06-9e7e-5cc28123a90e",
                UserName = "unit-test-user",
                UserId = "not-used-(?)",
                SiteLogin = 1,
                //RequestId = Guid.NewGuid().GuidNullToString(),
                BranchGUID = "71AA8740-9DC4-45C7-9A69-63F6794BD9DF",
            };

            return sysParm;
        }
        #endregion config
        #region database
        public static SmartAppContext BuildSmartAppContext_UseInMemory(ITestOutputHelper output, string databaseName = "")
        {
            InitSystemValues(output);
            
            DbContextOptionsBuilder<SmartAppContext> builder = new DbContextOptionsBuilder<SmartAppContext>();
            var testLogLevel = GetConfigurationValue("TestLogLevel");
            if (testLogLevel == "Debug")
            {
                builder.UseLoggerFactory(SmartLoggerFactory.LoggerFactory)
                   .EnableDetailedErrors()
                   .EnableSensitiveDataLogging()
                   .UseInMemoryDatabase(databaseName);
            }
            else
            {
                builder.UseInMemoryDatabase(databaseName);
            }
            
            SmartAppContext context = new SmartAppContext(builder.Options);
            context.Database.EnsureDeleted();
            context.SetSystemParameter(GetSystemParameter());
            
            return context;
        }
        public static SmartAppContext BuildSmartAppContext_UseSqlServer(ITestOutputHelper output, string connectionString = null, bool dropAndCreate = false)
        {
            InitSystemValues(output);
            if(string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = GetConfigurationValue("ConnectionStrings:SmartApp");
            }
            DbContextOptionsBuilder<SmartAppContext> builder = new DbContextOptionsBuilder<SmartAppContext>();
            var testLogLevel = GetConfigurationValue("TestLogLevel");
            if(testLogLevel == "Debug")
            {
                builder.UseLoggerFactory(SmartLoggerFactory.LoggerFactory)
                    .EnableDetailedErrors()
                    .EnableSensitiveDataLogging()
                    .UseSqlServer(connectionString);
            }
            else
            {
                builder.UseSqlServer(connectionString);
            }
            
            SmartAppContext context = new SmartAppContext(builder.Options);
            if(dropAndCreate)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }
            else
            {
                context.Database.EnsureCreated();
            }
            context.SetSystemParameter(GetSystemParameter());

            InitStoredProcAndFunctions(connectionString, Directory.GetCurrentDirectory() + "\\StoredProceduresAndFunctions");

            return context;
        }
        private static void InitStoredProcAndFunctions(string connectionString, string filePath)
        {
            SharedService sharedService = new SharedService();
            sharedService.InitializeStoredProceduresAndFunctions(filePath, connectionString);
        }
        #endregion
        #region add test data
        public static void AddTestData(ITestOutputHelper output, SmartAppContext context, string jsonPath, bool inMemory = true, bool showProgress = false, bool addDefaultBusinessUnit = false)
        {
            try
            {
                if(showProgress)
                {
                    output.WriteLine("=== AddTestData: Start ===");
                }
                string jsonData = File.ReadAllText(jsonPath);
                JObject ob = JObject.Parse(jsonData);
                PropertyInfo[] rootProps = typeof(SmartAppContext).GetProperties();
                bool commitTrn = false;
                using(var txn = !inMemory ? context.Database.BeginTransaction() : null)
                {
                    try
                    {
                        foreach (var rootProp in rootProps)
                        {
                            bool dataChange = false;
                            if (rootProp.PropertyType.IsGenericType)
                            {
                                Type itemType = rootProp.PropertyType.GetGenericArguments()[0];
                                JToken data = ob[itemType.Name];
                                if (data != null)
                                {
                                    if (showProgress)
                                        output.WriteLine(string.Format("Adding {{0}}..."), itemType.Name);
                                    
                                    var mi = typeof(TestHelper).GetMethod("AddTestDataFromJson");
                                    var addTestDataRef = mi.MakeGenericMethod(itemType);
                                    bool result = Convert.ToBoolean(addTestDataRef.Invoke(null, new object[] { context, data }));
                                    dataChange = result ? result : false;
                                }
                            }
                            if (dataChange)
                            {
                                commitTrn = true;
                            }

                        }

                        if(addDefaultBusinessUnit)
                        {
                            BusinessUnit businessUnit = new BusinessUnit
                            {
                                BusinessUnitGUID = "00000000-0000-0000-0000-000000000001".StringToGuid(),
                                CompanyGUID = "0a099e6f-9ba8-4a06-9e7e-5cc28123a90e".StringToGuid(),
                                BusinessUnitId = "UnitTest",
                                Description = "For unit test"
                            };
                            context.Set<BusinessUnit>().Add(businessUnit);
                            commitTrn = true;
                        }

                        if (commitTrn)
                            context.SaveChanges();

                        if (commitTrn && !inMemory)
                            txn.Commit();

                        

                        if (showProgress)
                            output.WriteLine("=== AddTestData: Done. ===");
                    }
                    catch (Exception e)
                    {
                        if(!inMemory) txn.Rollback();
                        throw SmartAppUtil.AddStackTrace(e);
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }            
        }
        public static bool AddTestDataFromJson<T>(SmartAppContext context, JToken jsonObject) where T: class
        {
            try
            {
                bool dataChange = false;
                
                var serializer = new JsonSerializer();
                List<string> dateFormats = GetConfigurationValues("AppSetting:ImportExcelDateFormat");
                dateFormats.Add(GetConfigurationValue("AppSetting:DateFormat"));
                dateFormats.Add(GetConfigurationValue("AppSetting:DateTimeFormat"));
                
                serializer.Converters.Add(new JsonCustomDateTimeConverter(dateFormats));
                
                List<T> data = jsonObject.ToObject<List<T>>(serializer);
                if(data != null && data.Count > 0)
                {
                    CheckDataAttributes(data);

                    context.Set<T>().AddRange(data);
                    dataChange = true;
                }
                return dataChange;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private static bool CheckDataAttributes<T>(List<T> data)
        {
            try
            {
                Type modelType = typeof(T);
                StringBuilder sb = new StringBuilder();
                PropertyInfo[] props = modelType.GetProperties();
                List<string> fieldsToIgnore = new List<string>() { TextConstants.CreatedBy, TextConstants.CreatedDateTime,
                                                        TextConstants.ModifiedBy, TextConstants.ModifiedDateTime,
                                                        TextConstants.Owner, TextConstants.OwnerBusinessUnitGUID,
                                                        TextConstants.RowVersion };

                var propsToCheck = props.Where(w => w.GetCustomAttribute<InversePropertyAttribute>() == null &&
                                w.GetCustomAttribute<ForeignKeyAttribute>() == null &&
                                !fieldsToIgnore.Any(a => a == w.Name));
                for(int i=0; i<data.Count; i++)
                {
                    var item = data[i];
                    bool hasError = false;
                    foreach (var prop in propsToCheck)
                    {
                        var value = prop.GetValue(item);
                        if(prop.IsPropRequired() && value == null)
                        {
                            sb.AppendLine(string.Concat("Row ", i, ": ", modelType.Name, ".", prop.Name, " does not allow null."));
                            hasError = true;
                        }
                        if(prop.PropertyType == typeof(Guid) && value != null && (Guid)value == Guid.Empty)
                        {
                            sb.AppendLine(string.Concat("Row ", i, ": ", modelType.Name, ".", prop.Name, " does not allow null. (Guid.Empty)"));
                            hasError = true;
                        }
                        if(prop.PropertyType == typeof(string))
                        {
                            int strlen = prop.GetPropStringLength();
                            int len = value != null ? ((string)value).Length : 0;

                            if (strlen > 0 && len > strlen)
                            {
                                sb.AppendLine(string.Concat("Row ", i, ": ", modelType.Name, ".", prop.Name, " string length (", len, ") is longer than is allowed (", strlen, ")."));
                                hasError = true;
                            }
                        }
                    }
                    
                    if(hasError) sb.AppendLine();
                }

                if(sb.Length > 0)
                {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.AppendLine(string.Concat("Error AddTestData :", modelType.Name));
                    sb2.AppendLine(sb.ToString());
                    SmartAppException ex = new SmartAppException(sb2.ToString());
                    throw ex;
                }

                return true;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region json to object
        public static T JsonToObject<T>(string jsonPath)
        {
            try
            {
                TestHelper.InitSystemValues(null);

                string jsonData = File.ReadAllText(jsonPath);
                JObject jsonObject = JObject.Parse(jsonData);
                var serializer = new JsonSerializer();
                List<string> dateFormats = GetConfigurationValues("AppSetting:ImportExcelDateFormat");
                dateFormats.Add(GetConfigurationValue("AppSetting:DateFormat"));
                dateFormats.Add(GetConfigurationValue("AppSetting:DateTimeFormat"));

                serializer.Converters.Add(new JsonCustomDateTimeConverter(dateFormats));
                T result = jsonObject.ToObject<T>(serializer);
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #endregion

        #region should equal
        public void AssertShouldEqual<T>(T expected, T actual)
        {
            try
            {
                if(expected != null && actual != null)
                {
                    ActualExpectedResult result = CompareObject<T>(expected, actual);
                    if (!result.IsEqual)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine(typeof(T).Name + ":");
                        sb.AppendLine(string.Format("Expected: {0}", result.Expected));
                        sb.AppendLine(string.Format("Actual: {0}", result.Actual));

                        XunitException ex = new XunitException(sb.ToString());
                        throw ex;
                    }
                }
                else
                {
                    string errorMsg = "";
                    if (expected != null && actual == null)
                    {
                        errorMsg = "Expected is not null but actual is null.";
                    }
                    else if (actual != null && expected == null)
                    {
                        errorMsg = "Expected is null but actual is not null.";
                    }
                    else
                    {
                        return;
                    }
                    XunitException ex = new XunitException(errorMsg);
                    throw ex;
                }

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void AssertShouldEqual<T>(List<T> expected, List<T> actual)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                
                if(expected == null || actual == null)
                {
                    sb.AppendLine(string.Format("List of {0}: ", typeof(T).Name));
                    if (expected != null && actual == null)
                    {
                        sb.AppendLine("Expected is not null but actual is null.");
                    }
                    else if (actual != null && expected == null)
                    {
                        sb.AppendLine("Expected is null but actual is not null.");
                    }
                    else
                    {
                        return;
                    }
                    XunitException ex = new XunitException(sb.ToString());
                    throw ex;
                }
                else
                {
                    int expectedCount = expected.Count();
                    int actualCount = actual.Count();
                    if(expectedCount != actualCount)
                    {
                        sb.AppendLine(string.Format("Expected item count: {0}", expectedCount));
                        sb.AppendLine(string.Format("Actual item count: {0}", actualCount));
                    }
                    else
                    {
                        for(int i=0; i<expectedCount; i++)
                        {
                            ActualExpectedResult result = CompareObject<T>(expected[i], actual[i]);
                            if(!result.IsEqual)
                            {
                                sb.AppendLine(string.Format("Row {0}", i));
                                sb.AppendLine(string.Format("Expected: {0}", result.Expected));
                                sb.AppendLine(string.Format("Actual: {0}", result.Actual));
                                sb.AppendLine();
                            }
                        }
                    }
                    var errors = sb.ToString();
                    if(!string.IsNullOrWhiteSpace(errors))
                    {
                        sb.Clear();
                        sb.AppendLine(string.Format("List of {0}: ", typeof(T).Name));
                        sb.AppendLine(errors);
                        XunitException ex = new XunitException(sb.ToString());
                        throw ex;
                    }
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        public void AssertExceptionMessageListEqual(SmartAppException expected, Exception actual)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                if(actual.GetType() != typeof(SmartAppException))
                {
                    sb.AppendLine(string.Format("Expected exception type: SmartAppException, Actual type: {0}", actual.GetType().Name));
                    XunitException ex = new XunitException(sb.ToString());
                    throw ex;
                }
                else
                {
                    List<string> expectedMsgList = expected.MessageList;
                    List<string> actualMsgList = ((SmartAppException)actual).MessageList;
                    AssertShouldEqual<string>(expectedMsgList, actualMsgList);
                }
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        private ActualExpectedResult CompareObject<T>(T expected, T actual)
        {
            try
            {
                List<string> fieldsToIgnore = new List<string>() { TextConstants.CreatedBy, TextConstants.CreatedDateTime,
                                                        TextConstants.ModifiedBy, TextConstants.ModifiedDateTime,
                                                        TextConstants.Owner, TextConstants.OwnerBusinessUnitGUID,
                                                        TextConstants.RowVersion };

                string[] fieldsToIgnoreConfig = null;
                bool tryGetResult = false;
                if (this.fieldsToIgnore != null)
                {
                    tryGetResult = this.fieldsToIgnore.TryGetValue(typeof(T), out fieldsToIgnoreConfig);
                    if (tryGetResult) fieldsToIgnore.AddRange(fieldsToIgnoreConfig);
                }

                PropertyInfo[] props = typeof(T).GetProperties();
                var onlyInputProps = props.Where(w => w.GetCustomAttribute<InversePropertyAttribute>() == null &&
                                w.GetCustomAttribute<ForeignKeyAttribute>() == null &&
                                !fieldsToIgnore.Any(a => a == w.Name));

                #region compare prop values
                bool isEqual = true;
                List<string> expectedNotEqual = new List<string>();
                List<string> actualNotEqual = new List<string>();
                int propsLength = onlyInputProps.Count();
                for(int i = 0; i < propsLength; i++)
                {
                    var prop = onlyInputProps.ElementAt(i);
                    var expectedVal = prop.GetValue(expected);
                    var actualVal = prop.GetValue(actual);
                    if (prop.PropertyType == typeof(Guid))
                    {
                        if ((Guid)expectedVal != (Guid)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", ((Guid)expectedVal).GuidNullToString()));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", ((Guid)actualVal).GuidNullToString()));
                        }
                    }
                    else if (prop.PropertyType == typeof(Guid?))
                    {
                        if ((Guid?)expectedVal != (Guid?)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", ((Guid?)expectedVal).GuidNullToString()));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", ((Guid?)actualVal).GuidNullToString())); ;
                        }
                    }
                    else if (prop.PropertyType == typeof(bool))
                    {
                        if ((bool)expectedVal != (bool)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", (bool)expectedVal));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", (bool)actualVal));
                        }
                    }
                    else if (prop.PropertyType == typeof(bool?))
                    {
                        if ((bool?)expectedVal != (bool?)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", (bool?)expectedVal));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", (bool?)actualVal));
                        }
                    }
                    else if (prop.PropertyType == typeof(int))
                    {
                        if ((int)expectedVal != (int)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", (int)expectedVal));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", (int)actualVal));
                        }
                    }
                    else if (prop.PropertyType == typeof(int?))
                    {
                        if ((int?)expectedVal != (int?)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", (int?)expectedVal));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", (int?)actualVal));
                        }
                    }
                    else if (prop.PropertyType == typeof(decimal))
                    {
                        if ((decimal)expectedVal != (decimal)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", (decimal)expectedVal));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", (decimal)actualVal));
                        }
                    }
                    else if (prop.PropertyType == typeof(decimal?))
                    {
                        if ((decimal?)expectedVal != (decimal?)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", (decimal?)expectedVal));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", (decimal?)actualVal));
                        }
                    }
                    else if (prop.PropertyType == typeof(DateTime))
                    {
                        if ((DateTime)expectedVal != (DateTime)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", ((DateTime)expectedVal).DateTimeToString()));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", ((DateTime)actualVal).DateTimeToString()));
                        }
                    }
                    else if (prop.PropertyType == typeof(DateTime?))
                    {
                        if ((DateTime?)expectedVal != (DateTime?)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", ((DateTime?)expectedVal).DateTimeNullToString()));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", ((DateTime?)actualVal).DateTimeNullToString()));
                        }
                    }
                    else if (prop.PropertyType == typeof(string))
                    {
                        if (string.IsNullOrEmpty((string)expectedVal) && string.IsNullOrEmpty((string)actualVal))
                        {
                            //isEqual = true;
                        }
                        else if ((string)expectedVal != (string)actualVal)
                        {
                            isEqual = false;
                            expectedNotEqual.Add(string.Concat(prop.Name, ": ", (string)expectedVal));
                            actualNotEqual.Add(string.Concat(prop.Name, ": ", (string)actualVal));
                        }
                    }
                    else
                    {
                        throw new SmartAppException("Compare Object unsupported property type: " + prop.PropertyType.Name);
                    }
                }
                #endregion

                string expectedResult = null, actualResult = null;
                if(expectedNotEqual.Count > 0)
                {
                    StringBuilder expectedSb = new StringBuilder();
                    StringBuilder actualSb = new StringBuilder();

                    expectedResult = expectedSb.AppendJoin(", ", expectedNotEqual).ToString();
                    actualResult = actualSb.AppendJoin(", ", actualNotEqual).ToString();
                }
                ActualExpectedResult result = new ActualExpectedResult
                {
                    Expected = expectedResult,
                    Actual = actualResult,
                    IsEqual = isEqual
                };
                return result;
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e);
            }
        }
        #endregion
        #region stop watch
        public void LogTimeStart()
        {
            if(sw != null)
            {
                sw.Restart();
            }
            else
            {
                sw = Stopwatch.StartNew();
            }
        }
        public void LogElapsedTime(ITestOutputHelper output, string message = "")
        {
            if(sw != null)
            {
                output.WriteLine(string.Concat(message," => Elapsed ", sw.Elapsed.TotalMilliseconds, " ms"));
            }
        }
        #endregion
    }
    public class ActualExpectedResult
    {
        public string Expected { get; set; }
        public string Actual { get; set; }
        public bool IsEqual { get; set; }
    }
}
