﻿using Innoviz.SmartApp.Data;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace Innoviz.SmartApp.WebApi.Test.Helper
{
    public class SharedDatabaseFixture: IDisposable
    {
        private string connectionString = "Data Source=(LocalDb)\\MSSQLLocalDB;Initial Catalog=SmartApp_LIT_UnitTest;trusted_connection=yes;";
        public SmartAppContext db { get; set; }
        public DbConnection Connection { get; set; }
        public SharedDatabaseFixture()
        {
            Connection = new SqlConnection(connectionString);
            db = TestHelper.BuildSmartAppContext_UseSqlServer(null, connectionString);
            Connection.Open();
        }

        public void Dispose() => Connection.Dispose();
    }
}
