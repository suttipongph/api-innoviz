﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Innoviz.SmartApp.WebApi.Test.Helper
{
    public static class TestHandler
    {
        public static Exception LogToTestOutput(this Exception ex, ITestOutputHelper output)
        {
            if (ex.GetType().Name == "TargetInvocationException")
            {
                ex = SmartAppUtil.AddStackTrace(ex);
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Error Message: ");
            List<string> msgList = (List<string>)ex.Data[ExceptionDataKey.MessageList];
            
            if (msgList != null)
            {
                int count = msgList.Count();
                if(count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        sb.AppendLine(msgList[i]);
                    }
                }
                    
            }
            else
            {
                sb.AppendLine("\t" + ex.Message);
            }
            sb.AppendLine();
            sb.AppendLine("StackTrace: ");
            string stackTrace = ex.Data[ExceptionDataKey.StackTrace]?.ToString();
            if(!string.IsNullOrWhiteSpace(stackTrace))
            {
                sb.AppendLine(stackTrace);
            }
            else
            {
                sb.AppendLine(ex.StackTrace);
            }

            output.WriteLine(sb.ToString());
            return ex;
        }
    }
}
