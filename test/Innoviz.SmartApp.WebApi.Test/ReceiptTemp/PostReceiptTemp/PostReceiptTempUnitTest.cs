﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.ReceiptTemp.PostReceiptTemp
{
    public class PostReceiptTempUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostReceiptTempUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostReceiptTemp");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }
        [Theory]
        [MemberData(nameof(CasePostReceiptTemp))]
        public void PostReceiptTemp(Guid receiptTempTableGuid, PostReceiptTempResultViewMap expectedResult, string folder)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../ReceiptTemp/PostReceiptTemp/" + folder + "/masterdata.json", true, false, true);
                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                //receiptTempTableService.PostReceiptTemp(new PostReceiptTempParamView { ReceiptTempTableGUID = receiptTempTableGuid.GuidNullToString() });
                #region prepare expected results
                var dbInvoiceSettlementDetails = db.Set<InvoiceSettlementDetail>().AsNoTracking().ToList();

                if (expectedResult.NewReceiptTempTable == null) expectedResult.NewReceiptTempTable = new List<ReceiptTempTable>();
                if (expectedResult.InvoiceTable == null) expectedResult.InvoiceTable = new List<InvoiceTable>();
                if (expectedResult.InvoiceLine == null) expectedResult.InvoiceLine = new List<InvoiceLine>();
                if (expectedResult.ProcessTrans == null) expectedResult.ProcessTrans = new List<ProcessTrans>();
                if (expectedResult.CustTransCreate == null) expectedResult.CustTransCreate = new List<CustTrans>();
                if (expectedResult.CustTransUpdate == null) expectedResult.CustTransUpdate = new List<CustTrans>();
                if (expectedResult.CreditAppTrans == null) expectedResult.CreditAppTrans = new List<CreditAppTrans>();
                if (expectedResult.TaxInvoiceTable == null) expectedResult.TaxInvoiceTable = new List<TaxInvoiceTable>();
                if (expectedResult.TaxInvoiceLine == null) expectedResult.TaxInvoiceLine = new List<TaxInvoiceLine>();
                if (expectedResult.RetentionTrans == null) expectedResult.RetentionTrans = new List<RetentionTrans>();
                if (expectedResult.InterestRealizedTrans == null) expectedResult.InterestRealizedTrans = new List<InterestRealizedTrans>();
                if (expectedResult.AssignmentAgreementSettle == null) expectedResult.AssignmentAgreementSettle = new List<AssignmentAgreementSettle>();
                if (expectedResult.ReceiptTempPaymDetail == null) expectedResult.ReceiptTempPaymDetail = new List<ReceiptTempPaymDetail>();
                if (expectedResult.InvoiceSettlementDetail == null) expectedResult.InvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                if (expectedResult.ReceiptTable == null) expectedResult.ReceiptTable = new List<ReceiptTable>();
                if (expectedResult.ReceiptLine == null) expectedResult.ReceiptLine = new List<ReceiptLine>();
                if (expectedResult.PaymentHistory == null) expectedResult.PaymentHistory = new List<PaymentHistory>();
                if (expectedResult.IntercompanyInvoiceTable == null) expectedResult.IntercompanyInvoiceTable = new List<IntercompanyInvoiceTable>();
                if (expectedResult.ProductSettledTrans == null) expectedResult.ProductSettledTrans = new List<Data.Models.ProductSettledTrans>();

                List<ReceiptTempTable> expectedNewReceiptTempTable = expectedResult.NewReceiptTempTable.OrderBy(o => o.ReceiptTempId).ToList();
                List<InvoiceTable> expectedInvoiceTable = expectedResult.InvoiceTable.OrderBy(o => o.InvoiceId).ToList();
                #region InvoiceLine
                List<InvoiceLine> expectedInvoiceLine =
                    (from invoiceTable in expectedInvoiceTable
                     join invoiceLine in expectedResult.InvoiceLine
                     on invoiceTable.InvoiceTableGUID equals invoiceLine.InvoiceTableGUID
                     select new { invoiceTable, invoiceLine })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .ThenBy(b => b.invoiceLine.LineNum)
                     .Select(s => s.invoiceLine).ToList();
                #endregion
                #region CustTrans
                List<CustTrans> expectedCustTransCreate =
                    (from invoiceTable in expectedInvoiceTable
                     join custTrans in expectedResult.CustTransCreate
                     on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                     select new { invoiceTable, custTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.custTrans).ToList();
                List<CustTrans> expectedCustTransUpdate = expectedResult.CustTransUpdate.OrderBy(o => o.CustTransGUID).ToList();
                #endregion
                #region ProcessTrans
                List<ProcessTrans> expectedProcessTrans = new List<ProcessTrans>();
                if (expectedResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Payment))
                {
                    var paymentProcessTrans = expectedResult.ProcessTrans.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment);
                    if (paymentProcessTrans.Any(a => !a.InvoiceTableGUID.HasValue))
                    {
                        expectedProcessTrans.AddRange(paymentProcessTrans);
                    }
                    else
                    {
                        var custTransPayment = expectedCustTransUpdate.Concat(expectedCustTransCreate);
                        paymentProcessTrans =
                            (from custTrans in custTransPayment
                             join processTrans in paymentProcessTrans
                             on custTrans.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                             select processTrans);
                        expectedProcessTrans.AddRange(paymentProcessTrans);
                    }
                }
                if (expectedResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Invoice))
                {
                    var invoiceProcessTrans = expectedResult.ProcessTrans.Where(a => a.ProcessTransType == (int)ProcessTransType.Invoice);
                    invoiceProcessTrans =
                        (from invoiceTable in expectedInvoiceTable
                         join processTrans in invoiceProcessTrans
                         on invoiceTable.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                         select new { invoiceTable, processTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.processTrans);
                    expectedProcessTrans.AddRange(invoiceProcessTrans);
                }
                Assert.True(expectedProcessTrans.Count == expectedResult.ProcessTrans.Count);
                #endregion
                #region InvoiceSettlementDetail
                List<InvoiceSettlementDetail> expectedInvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                if (expectedNewReceiptTempTable.Count > 0)
                {
                    var pdcInvSettleDetail =
                        (from receiptTemp in expectedNewReceiptTempTable
                         join invSettleDetail in expectedResult.InvoiceSettlementDetail.Where(w => expectedNewReceiptTempTable.Any(a => a.ReceiptTempTableGUID == w.RefGUID))
                         on receiptTemp.ReceiptTempTableGUID equals invSettleDetail.RefGUID
                         join invoiceTable in expectedInvoiceTable
                         on invSettleDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into lj
                         from invoiceTable in lj.DefaultIfEmpty()
                         select new { receiptTemp, invSettleDetail, InvoiceId = (invoiceTable != null) ? invoiceTable.InvoiceId : invSettleDetail.InvoiceTableGUID.GuidNullToString() })
                         .OrderBy(o => o.receiptTemp.ReceiptTempId)
                         .ThenBy(b => b.InvoiceId)
                         .Select(s => s.invSettleDetail);
                    expectedInvoiceSettlementDetail.AddRange(pdcInvSettleDetail);
                }
                if (expectedInvoiceTable.Any(a => a.RefType == (int)RefType.ServiceFeeTrans))
                {
                    var serviceFeeInvSettleDetail =
                        (from invoiceTable in expectedInvoiceTable.Where(a => a.RefType == (int)RefType.ServiceFeeTrans)
                         join invSettleDetail in expectedResult.InvoiceSettlementDetail
                         on invoiceTable.InvoiceTableGUID equals invSettleDetail.InvoiceTableGUID
                         select new { invoiceTable, invSettleDetail })
                         .OrderBy(o => o.invoiceTable.InvoiceId)
                         .Select(s => s.invSettleDetail);
                    expectedInvoiceSettlementDetail.AddRange(serviceFeeInvSettleDetail);
                }
                Assert.True(expectedInvoiceSettlementDetail.Count == expectedResult.InvoiceSettlementDetail.Count);
                #endregion
                List<InvoiceSettlementDetail> baseExpectedInvoiceSettlementDetails = new List<InvoiceSettlementDetail>();
                baseExpectedInvoiceSettlementDetails.AddRange(dbInvoiceSettlementDetails);
                baseExpectedInvoiceSettlementDetails.AddRange(expectedInvoiceSettlementDetail);
                #region CreditAppTrans
                List<CreditAppTrans> expectedCreditAppTrans = new List<CreditAppTrans>();
                if (expectedResult.CreditAppTrans.Any(w => w.RefType == (int)RefType.InvoiceSettlementDetail))
                {
                    var invSettleDetailCreditAppTrans =
                        expectedResult.CreditAppTrans.Where(w => w.RefType == (int)RefType.InvoiceSettlementDetail);

                    var newInvSettleDetailCreditAppTrans =
                        (from invsettleDetail in baseExpectedInvoiceSettlementDetails
                         join creditAppTrans in invSettleDetailCreditAppTrans
                         on invsettleDetail.InvoiceSettlementDetailGUID equals creditAppTrans.RefGUID
                         select new { invsettleDetail, creditAppTrans })
                         .Select(s => s.creditAppTrans);
                    expectedCreditAppTrans.AddRange(newInvSettleDetailCreditAppTrans);
                }
                if (expectedResult.CreditAppTrans.Any(a => a.RefType != (int)RefType.InvoiceSettlementDetail))
                {
                    var invoiceCreditAppTrans = expectedResult.CreditAppTrans.Where(a => a.RefType != (int)RefType.InvoiceSettlementDetail)
                                                            .OrderBy(o => o.RefType)
                                                            .ThenBy(b => b.TransDate)
                                                            .ThenBy(b => b.InvoiceAmount)
                                                            .ThenBy(b => b.CreditDeductAmount).ToList();

                    expectedCreditAppTrans.AddRange(invoiceCreditAppTrans);
                }
                Assert.True(expectedCreditAppTrans.Count == expectedResult.CreditAppTrans.Count);
                #endregion
                List<TaxInvoiceTable> expectedTaxInvoiceTable = expectedResult.TaxInvoiceTable.OrderBy(o => o.TaxInvoiceId).ToList();

                #region TaxInvoiceLine
                List<TaxInvoiceLine> expectedTaxInvoiceLine =
                    (from taxInvoiceTable in expectedTaxInvoiceTable
                     join taxInvoiceLine in expectedResult.TaxInvoiceLine
                     on taxInvoiceTable.TaxInvoiceTableGUID equals taxInvoiceLine.TaxInvoiceTableGUID
                     select new { taxInvoiceTable, taxInvoiceLine })
                     .OrderBy(o => o.taxInvoiceTable.TaxInvoiceId)
                     .ThenBy(b => b.taxInvoiceLine.LineNum)
                     .Select(s => s.taxInvoiceLine).ToList();
                #endregion
                List<RetentionTrans> expectedRetentionTrans = expectedResult.RetentionTrans.OrderBy(o => o.RefType)
                                                                                            .ThenBy(b => b.Amount).ToList();
                List<InterestRealizedTrans> expectedInterestRealizedTrans = expectedResult.InterestRealizedTrans
                                                                                    .OrderBy(o => o.RefGUID)
                                                                                    .ThenBy(b => b.AccountingDate).ToList();
                List<AssignmentAgreementSettle> expectedAssignmentAgreementSettle = expectedResult.AssignmentAgreementSettle
                                                                                    .OrderBy(o => o.SettledDate)
                                                                                    .ThenBy(b => b.SettledAmount).ToList();
                #region ReceiptTempPaymDetail
                List<ReceiptTempPaymDetail> expectedReceiptTempPaymDetail =
                    (from receiptTemp in expectedNewReceiptTempTable
                     join paymDetail in expectedResult.ReceiptTempPaymDetail
                     on receiptTemp.ReceiptTempTableGUID equals paymDetail.ReceiptTempTableGUID
                     select new { receiptTemp, paymDetail })
                     .OrderBy(o => o.receiptTemp.ReceiptTempId)
                     .ThenBy(b => b.paymDetail.MethodOfPaymentGUID)
                     .Select(s => s.paymDetail).ToList();
                #endregion
                List<ReceiptTable> expectedReceiptTable = expectedResult.ReceiptTable.OrderBy(o => o.ReceiptId).ToList();

                #region ReceiptLine
                List<ReceiptLine> expectedReceiptLine =
                    (from receiptTable in expectedReceiptTable
                     join receiptLine in expectedResult.ReceiptLine
                     on receiptTable.ReceiptTableGUID equals receiptLine.ReceiptTableGUID
                     select new { receiptTable, receiptLine })
                     .OrderBy(o => o.receiptTable.ReceiptId)
                     .ThenBy(b => b.receiptLine.LineNum)
                     .Select(s => s.receiptLine).ToList();
                #endregion
                #region PaymentHistory
                List<PaymentHistory> expectedPaymentHistory = 
                    (from invsettleDetail in baseExpectedInvoiceSettlementDetails
                     join paymentHistory in expectedResult.PaymentHistory
                     on invsettleDetail.InvoiceSettlementDetailGUID equals paymentHistory.InvoiceSettlementDetailGUID
                     select new { invsettleDetail, paymentHistory })
                     .Select(s => s.paymentHistory).ToList();
                #endregion

                List<IntercompanyInvoiceTable> expectedIntercompanyInvoiceTable = expectedResult.IntercompanyInvoiceTable.OrderBy(o => o.IntercompanyInvoiceId).ToList();

                List<Data.Models.ProductSettledTrans> expectedProductSettledTrans = expectedResult.ProductSettledTrans.OrderBy(o => o.ProductSettledTransGUID).ToList();
                #endregion

                PostReceiptTempResultViewMap actualResult = receiptTempTableService.InitPostReceiptTemp(receiptTempTableGuid);

                #region prepare actual result
                List<ReceiptTempTable> actualNewReceiptTempTable = actualResult.NewReceiptTempTable.OrderBy(o => o.ReceiptTempId).ToList();
                List<InvoiceTable> actualInvoiceTable = actualResult.InvoiceTable.OrderBy(o => o.InvoiceId).ToList();
                #region InvoiceLine
                List<InvoiceLine> actualInvoiceLine =
                    (from invoiceTable in actualInvoiceTable
                     join invoiceLine in actualResult.InvoiceLine
                     on invoiceTable.InvoiceTableGUID equals invoiceLine.InvoiceTableGUID
                     select new { invoiceTable, invoiceLine })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .ThenBy(b => b.invoiceLine.LineNum)
                     .Select(s => s.invoiceLine).ToList();
                #endregion
                #region CustTrans
                List<CustTrans> actualCustTransCreate =
                    (from invoiceTable in actualInvoiceTable
                     join custTrans in actualResult.CustTransCreate
                     on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                     select new { invoiceTable, custTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.custTrans).ToList();
                List<CustTrans> actualCustTransUpdate = actualResult.CustTransUpdate.OrderBy(o => o.CustTransGUID).ToList();
                #endregion
                #region ProcessTrans
                List<ProcessTrans> actualProcessTrans = new List<ProcessTrans>();
                if (actualResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Payment))
                {
                    var paymentProcessTrans = actualResult.ProcessTrans.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment);
                    if (paymentProcessTrans.Any(a => !a.InvoiceTableGUID.HasValue))
                    {
                        actualProcessTrans.AddRange(paymentProcessTrans);
                    }
                    else
                    {
                        var custTransPayment = actualCustTransUpdate.Concat(actualCustTransCreate);
                        paymentProcessTrans =
                            (from custTrans in custTransPayment
                             join processTrans in paymentProcessTrans
                             on custTrans.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                             select processTrans);
                        actualProcessTrans.AddRange(paymentProcessTrans);
                    }
                }
                if (actualResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Invoice))
                {
                    var invoiceProcessTrans = actualResult.ProcessTrans.Where(a => a.ProcessTransType == (int)ProcessTransType.Invoice);
                    invoiceProcessTrans =
                        (from invoiceTable in actualInvoiceTable
                         join processTrans in invoiceProcessTrans
                         on invoiceTable.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                         select new { invoiceTable, processTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.processTrans);
                    actualProcessTrans.AddRange(invoiceProcessTrans);
                }
                Assert.True(actualProcessTrans.Count == actualResult.ProcessTrans.Count);
                #endregion
                #region InvoiceSettlementDetail
                List<InvoiceSettlementDetail> actualInvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                if (actualNewReceiptTempTable.Count > 0)
                {
                    var pdcInvSettleDetail =
                        (from receiptTemp in actualNewReceiptTempTable
                         join invSettleDetail in actualResult.InvoiceSettlementDetail.Where(w => actualNewReceiptTempTable.Any(a => a.ReceiptTempTableGUID == w.RefGUID))
                         on receiptTemp.ReceiptTempTableGUID equals invSettleDetail.RefGUID
                         join invoiceTable in actualInvoiceTable
                         on invSettleDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into lj
                         from invoiceTable in lj.DefaultIfEmpty()
                         select new { receiptTemp, invSettleDetail, InvoiceId = (invoiceTable != null) ? invoiceTable.InvoiceId : invSettleDetail.InvoiceTableGUID.GuidNullToString() })
                         .OrderBy(o => o.receiptTemp.ReceiptTempId)
                         .ThenBy(b => b.InvoiceId)
                         .Select(s => s.invSettleDetail);
                    actualInvoiceSettlementDetail.AddRange(pdcInvSettleDetail);
                }
                if (actualInvoiceTable.Any(a => a.RefType == (int)RefType.ServiceFeeTrans))
                {
                    var serviceFeeInvSettleDetail =
                        (from invoiceTable in actualInvoiceTable.Where(a => a.RefType == (int)RefType.ServiceFeeTrans)
                         join invSettleDetail in actualResult.InvoiceSettlementDetail
                         on invoiceTable.InvoiceTableGUID equals invSettleDetail.InvoiceTableGUID
                         select new { invoiceTable, invSettleDetail })
                         .OrderBy(o => o.invoiceTable.InvoiceId)
                         .Select(s => s.invSettleDetail);
                    actualInvoiceSettlementDetail.AddRange(serviceFeeInvSettleDetail);
                }
                Assert.True(actualInvoiceSettlementDetail.Count == actualResult.InvoiceSettlementDetail.Count);
                #endregion
                List<InvoiceSettlementDetail> baseActualInvoiceSettlementDetails = new List<InvoiceSettlementDetail>();
                baseActualInvoiceSettlementDetails.AddRange(dbInvoiceSettlementDetails);
                baseActualInvoiceSettlementDetails.AddRange(actualInvoiceSettlementDetail);
                #region CreditAppTrans
                List<CreditAppTrans> actualCreditAppTrans = new List<CreditAppTrans>();
                if (actualResult.CreditAppTrans.Any(w => w.RefType == (int)RefType.InvoiceSettlementDetail))
                {
                    var invSettleDetailCreditAppTrans =
                        actualResult.CreditAppTrans.Where(w => w.RefType == (int)RefType.InvoiceSettlementDetail);

                    var newInvSettleDetailCreditAppTrans =
                        (from invsettleDetail in baseActualInvoiceSettlementDetails
                         join creditAppTrans in invSettleDetailCreditAppTrans
                         on invsettleDetail.InvoiceSettlementDetailGUID equals creditAppTrans.RefGUID
                         select new { invsettleDetail, creditAppTrans })
                         .Select(s => s.creditAppTrans);
                    actualCreditAppTrans.AddRange(newInvSettleDetailCreditAppTrans);
                }
                if (actualResult.CreditAppTrans.Any(a => a.RefType != (int)RefType.InvoiceSettlementDetail))
                {
                    var invoiceCreditAppTrans = actualResult.CreditAppTrans.Where(a => a.RefType != (int)RefType.InvoiceSettlementDetail)
                                                            .OrderBy(o => o.RefType)
                                                            .ThenBy(b => b.TransDate)
                                                            .ThenBy(b => b.InvoiceAmount)
                                                            .ThenBy(b => b.CreditDeductAmount).ToList();

                    actualCreditAppTrans.AddRange(invoiceCreditAppTrans);
                }
                Assert.True(actualCreditAppTrans.Count == actualResult.CreditAppTrans.Count);
                #endregion
                List<TaxInvoiceTable> actualTaxInvoiceTable = actualResult.TaxInvoiceTable.OrderBy(o => o.TaxInvoiceId).ToList();

                #region TaxInvoiceLine
                List<TaxInvoiceLine> actualTaxInvoiceLine =
                    (from taxInvoiceTable in actualTaxInvoiceTable
                     join taxInvoiceLine in actualResult.TaxInvoiceLine
                     on taxInvoiceTable.TaxInvoiceTableGUID equals taxInvoiceLine.TaxInvoiceTableGUID
                     select new { taxInvoiceTable, taxInvoiceLine })
                     .OrderBy(o => o.taxInvoiceTable.TaxInvoiceId)
                     .ThenBy(b => b.taxInvoiceLine.LineNum)
                     .Select(s => s.taxInvoiceLine).ToList();
                #endregion
                List<RetentionTrans> actualRetentionTrans = actualResult.RetentionTrans.OrderBy(o => o.RefType)
                                                                                            .ThenBy(b => b.Amount).ToList();
                List<InterestRealizedTrans> actualInterestRealizedTrans = actualResult.InterestRealizedTrans
                                                                                    .OrderBy(o => o.RefGUID)
                                                                                    .ThenBy(b => b.AccountingDate).ToList();
                List<AssignmentAgreementSettle> actualAssignmentAgreementSettle = actualResult.AssignmentAgreementSettle
                                                                                    .OrderBy(o => o.SettledDate)
                                                                                    .ThenBy(b => b.SettledAmount).ToList();
                #region ReceiptTempPaymDetail
                List<ReceiptTempPaymDetail> actualReceiptTempPaymDetail =
                    (from receiptTemp in actualNewReceiptTempTable
                     join paymDetail in actualResult.ReceiptTempPaymDetail
                     on receiptTemp.ReceiptTempTableGUID equals paymDetail.ReceiptTempTableGUID
                     select new { receiptTemp, paymDetail })
                     .OrderBy(o => o.receiptTemp.ReceiptTempId)
                     .ThenBy(b => b.paymDetail.MethodOfPaymentGUID)
                     .Select(s => s.paymDetail).ToList();
                #endregion
                List<ReceiptTable> actualReceiptTable = actualResult.ReceiptTable.OrderBy(o => o.ReceiptId).ToList();

                #region ReceiptLine
                List<ReceiptLine> actualReceiptLine =
                    (from receiptTable in actualReceiptTable
                     join receiptLine in actualResult.ReceiptLine
                     on receiptTable.ReceiptTableGUID equals receiptLine.ReceiptTableGUID
                     select new { receiptTable, receiptLine })
                     .OrderBy(o => o.receiptTable.ReceiptId)
                     .ThenBy(b => b.receiptLine.LineNum)
                     .Select(s => s.receiptLine).ToList();
                #endregion
                #region PaymentHistory
                List<PaymentHistory> actualPaymentHistory =
                    (from invsettleDetail in baseActualInvoiceSettlementDetails
                     join paymentHistory in actualResult.PaymentHistory
                     on invsettleDetail.InvoiceSettlementDetailGUID equals paymentHistory.InvoiceSettlementDetailGUID
                     select new { invsettleDetail, paymentHistory })
                     .Select(s => s.paymentHistory).ToList();
                #endregion
                List<IntercompanyInvoiceTable> actualIntercompanyInvoiceTable = actualResult.IntercompanyInvoiceTable.OrderBy(o => o.IntercompanyInvoiceId).ToList();
                List<Data.Models.ProductSettledTrans> actualProductSettledTrans = actualResult.ProductSettledTrans.OrderBy(o => o.ProductSettledTransGUID).ToList();
                #endregion
                #region assert should equal
                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(ReceiptTempTable), new string[] { "ReceiptTempTableGUID" } },
                    { typeof(InvoiceTable), new string[] { "InvoiceTableGUID" } },
                    { typeof(InvoiceLine), new string[] { "InvoiceLineGUID", "InvoiceTableGUID" } },
                    { typeof(ProcessTrans), new string[] { "ProcessTransGUID", "InvoiceTableGUID", "RefGUID", "PaymentHistoryGUID", "RefTaxInvoiceGUID" } },
                    { typeof(CustTrans), new string[] { "CustTransGUID", "InvoiceTableGUID" } },
                    { typeof(CreditAppTrans), new string[] { "CreditAppTransGUID", "RefGUID" } },
                    { typeof(TaxInvoiceTable), new string[] { "TaxInvoiceTableGUID", "InvoiceTableGUID", "TaxInvoiceRefGUID" } },
                    { typeof(TaxInvoiceLine), new string[] { "TaxInvoiceLineGUID", "TaxInvoiceTableGUID" } },
                    { typeof(RetentionTrans), new string[] { "RetentionTransGUID", "RefGUID" } },
                    { typeof(InterestRealizedTrans), new string[] { "InterestRealizedTransGUID" } },
                    { typeof(AssignmentAgreementSettle), new string[] { "AssignmentAgreementSettleGUID" } },
                    { typeof(ReceiptTempPaymDetail), new string[] { "ReceiptTempPaymDetailGUID", "ReceiptTempTableGUID" } },
                    { typeof(InvoiceSettlementDetail), new string[] { "InvoiceSettlementDetailGUID", "InvoiceTableGUID", "RefGUID" } },
                    { typeof(BuyerReceiptTable), new string[] { "BuyerReceiptTableGUID" } },
                    { typeof(ReceiptTable), new string[] { "ReceiptTableGUID", "RefGUID", "InvoiceSettlementDetailGUID" } },
                    { typeof(ReceiptLine), new string[] { "ReceiptLineGUID", "RefGUID", "ReceiptTableGUID", "InvoiceTableGUID" } },
                    { typeof(PaymentHistory), new string[] { "PaymentHistoryGUID", "RefGUID", "InvoiceSettlementDetailGUID", "CustTransGUID", "ReceiptTableGUID", "InvoiceTableGUID" } },
                    { typeof(IntercompanyInvoiceTable), new string[] { "IntercompanyInvoiceTableGUID" } }
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedResult.ReceiptTempTable, actualResult.ReceiptTempTable);
                helper.AssertShouldEqual(expectedNewReceiptTempTable, actualNewReceiptTempTable);
                helper.AssertShouldEqual(expectedInvoiceTable, actualInvoiceTable);
                helper.AssertShouldEqual(expectedInvoiceLine, actualInvoiceLine);
                helper.AssertShouldEqual(expectedProcessTrans, actualProcessTrans);
                helper.AssertShouldEqual(expectedCustTransCreate, actualCustTransCreate);
                helper.AssertShouldEqual(expectedCustTransUpdate, actualCustTransUpdate);
                helper.AssertShouldEqual(expectedCreditAppTrans, actualCreditAppTrans);
                helper.AssertShouldEqual(expectedTaxInvoiceTable, actualTaxInvoiceTable);
                helper.AssertShouldEqual(expectedTaxInvoiceLine, actualTaxInvoiceLine);
                helper.AssertShouldEqual(expectedRetentionTrans, actualRetentionTrans);
                helper.AssertShouldEqual(expectedInterestRealizedTrans, actualInterestRealizedTrans);
                helper.AssertShouldEqual(expectedAssignmentAgreementSettle, actualAssignmentAgreementSettle);
                helper.AssertShouldEqual(expectedReceiptTempPaymDetail, actualReceiptTempPaymDetail);
                helper.AssertShouldEqual(expectedInvoiceSettlementDetail, actualInvoiceSettlementDetail);
                helper.AssertShouldEqual(expectedResult.BuyerReceiptTable, actualResult.BuyerReceiptTable);
                helper.AssertShouldEqual(expectedReceiptTable, actualReceiptTable);
                helper.AssertShouldEqual(expectedReceiptLine, actualReceiptLine);
                helper.AssertShouldEqual(expectedPaymentHistory, actualPaymentHistory);
                helper.AssertShouldEqual(expectedIntercompanyInvoiceTable, actualIntercompanyInvoiceTable);
                helper.AssertShouldEqual(expectedProductSettledTrans, actualProductSettledTrans);
                #endregion

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostReceiptTemp
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm("Case1"), GetResult("Case1"), "Case1" }
                };
            }
        }
        public static Guid GetParm(string folder)
        {
            string path = "../../../ReceiptTemp/PostReceiptTemp/" + folder + "/parameter.json";
            PostReceiptTempParamView parm = TestHelper.JsonToObject<PostReceiptTempParamView>(path);
            return parm.ReceiptTempTableGUID.StringToGuid();
        }
        public static PostReceiptTempResultViewMap GetResult(string folder)
        {
            string path = "../../../ReceiptTemp/PostReceiptTemp/" + folder + "/result.json";
            PostReceiptTempResultViewMap expectedResult = TestHelper.JsonToObject<PostReceiptTempResultViewMap>(path);
            return expectedResult;
        }
    }
    
}
