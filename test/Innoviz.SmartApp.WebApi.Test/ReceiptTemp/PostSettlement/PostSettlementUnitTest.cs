﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.ReceiptTemp.PostSettlement
{
    public class PostSettlementUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostSettlementUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostSettlement");
        }
        [Theory]
        [MemberData(nameof(CasePostSettlement))]
        public void PostSettlement(ViewPostSettlementParameter parameter, ViewPostSettlementResult expectedResult, string folder)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../ReceiptTemp/PostSettlement/" + folder + "/masterdata.json");

                #region prepare expected results
                if (expectedResult.UpdateCustTrans == null) expectedResult.UpdateCustTrans = new List<CustTrans>();
                if (expectedResult.CreateCreditAppTrans == null) expectedResult.CreateCreditAppTrans = new List<CreditAppTrans>();
                if (expectedResult.CreateReceiptTable == null) expectedResult.CreateReceiptTable = new List<ReceiptTable>();
                if (expectedResult.CreateReceiptLine == null) expectedResult.CreateReceiptLine = new List<ReceiptLine>();
                if (expectedResult.CreateTaxInvoiceTable == null) expectedResult.CreateTaxInvoiceTable = new List<TaxInvoiceTable>();
                if (expectedResult.CreateTaxInvoiceLine == null) expectedResult.CreateTaxInvoiceLine = new List<TaxInvoiceLine>();
                if (expectedResult.CreatePaymentHistory == null) expectedResult.CreatePaymentHistory = new List<PaymentHistory>();
                if (expectedResult.CreateProcessTrans == null) expectedResult.CreateProcessTrans = new List<ProcessTrans>();
              
                #endregion

                IReceiptTempTableService receiptTempTableService = new ReceiptTempTableService(db);
                ViewPostSettlementResult actualResult = receiptTempTableService.PostSettlement(parameter);

                #region prepare actual result
 
                #endregion
                #region assert should equal
                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(ReceiptTempTable), new string[] { "ReceiptTempTableGUID", "ReceiptTempId" } },
                    { typeof(InvoiceTable), new string[] { "InvoiceTableGUID", "InvoiceId" } },
                    { typeof(InvoiceLine), new string[] { "InvoiceLineGUID", "InvoiceTableGUID", "InvoiceText" } },
                    { typeof(ProcessTrans), new string[] { "ProcessTransGUID", "InvoiceTableGUID", "RefGUID" } },
                    { typeof(CustTrans), new string[] { "CustTransGUID", "InvoiceTableGUID" } },
                    { typeof(CreditAppTrans), new string[] { "CreditAppTransGUID", "RefGUID" } },
                    { typeof(TaxInvoiceTable), new string[] { "TaxInvoiceTableGUID", "TaxInvoiceId", "InvoiceTableGUID" } },
                    { typeof(TaxInvoiceLine), new string[] { "TaxInvoiceLineGUID", "TaxInvoiceTableGUID" } },
                    { typeof(RetentionTrans), new string[] { "RetentionTransGUID", "RefGUID" } },
                    { typeof(InterestRealizedTrans), new string[] { "InterestRealizedTransGUID" } },
                    { typeof(AssignmentAgreementSettle), new string[] { "AssignmentAgreementSettleGUID" } },
                    { typeof(ReceiptTempPaymDetail), new string[] { "ReceiptTempPaymDetailGUID", "ReceiptTempTableGUID" } },
                    { typeof(InvoiceSettlementDetail), new string[] { "InvoiceSettlementDetailGUID", "InvoiceTableGUID", "RefGUID" } },
                    { typeof(BuyerReceiptTable), new string[] { "BuyerReceiptTableGUID", "BuyerReceiptId" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                // helper.AssertShouldEqual(expectedResult.ReceiptTempTable, actualResult.ReceiptTempTable);
                // helper.AssertShouldEqual(expectedNewReceiptTempTable, actualNewReceiptTempTable);
                #endregion

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostSettlement
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm("Case1"), GetResult("Case1"), "Case1" }
                };
            }
        }
        public static ViewPostSettlementParameter GetParm(string folder)
        {
            string path = "../../../ReceiptTemp/PostSettlement/" + folder + "/parameter.json";
            ViewPostSettlementParameter parm = TestHelper.JsonToObject<ViewPostSettlementParameter>(path);
            return parm;
        }
        public static ViewPostSettlementResult GetResult(string folder)
        {
            string path = "../../../ReceiptTemp/PostSettlement/" + folder + "/result.json";
            ViewPostSettlementResult expectedResult = TestHelper.JsonToObject<ViewPostSettlementResult>(path);
            return expectedResult;
        }
    }

}
