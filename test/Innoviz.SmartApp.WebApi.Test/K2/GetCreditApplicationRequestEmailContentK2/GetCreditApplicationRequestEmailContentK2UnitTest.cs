﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.K2.GetCreditApplicationRequestEmailContentK2
{
    public class GetCreditApplicationRequestEmailContentK2UnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GetCreditApplicationRequestEmailContentK2UnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetCreditApplicationRequestEmailContentK2");
        }
        [Theory]
        [MemberData(nameof(CaseGetCreditApplicationRequestEmailContentK2))]
        public void GetCreditApplicationRequestEmailContentK2(GetCreditApplicationRequestEmailContentK2ParmView parm, GetCreditApplicationRequestEmailContentK2ResultView expected, string caseName)

        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../K2/GetCreditApplicationRequestEmailContentK2/masterData.json");
                TestHelper.AddTestData(output, db, $"../../../K2/GetCreditApplicationRequestEmailContentK2/{caseName}/masterData.json");

                var CreditAppRequestTable = db.Set<CreditAppRequestTable>().ToList();
                var CustomerTable = db.Set<CustomerTable>().ToList();
                ICreditAppRequestTableService creditAppRequestTableService = new CreditAppRequestTableService(db);
                GetCreditApplicationRequestEmailContentK2ResultView actual = creditAppRequestTableService.GetCreditApplicationRequestEmailContentK2(parm);
                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(GetCreditApplicationRequestEmailContentK2ResultView), new string[] 
                        { 
                            "RequestDate", "RequestDate",
                            "MainAgreementDate", "MainAgreementDate",
                            "RefCreditAppTableGUID", "RefCreditAppTableGUID" 
                        }},
                };
                // var json = JsonConvert.SerializeObject(actual);
                TestHelper helper = new TestHelper(fieldsToIgnore);
                helper.AssertShouldEqual(expected, actual);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGetCreditApplicationRequestEmailContentK2
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm("MainCreditLimit"), GetResult("MainCreditLimit"), "MainCreditLimit" },
                    new object[] { GetParm("ActiveAmendCustomerCreditLimit"), GetResult("ActiveAmendCustomerCreditLimit"), "ActiveAmendCustomerCreditLimit" },
                    new object[] { GetParm("AmendCustomerInfo"), GetResult("AmendCustomerInfo"), "AmendCustomerInfo" },
                    new object[] { GetParm("AmendCustomerBuyerCreditLimit"), GetResult("AmendCustomerBuyerCreditLimit"), "AmendCustomerBuyerCreditLimit" },

                };
            }
        }
        public static GetCreditApplicationRequestEmailContentK2ParmView GetParm(string caseName)
        {
            string path = $"../../../K2/GetCreditApplicationRequestEmailContentK2/{caseName}/parameter.json";
            GetCreditApplicationRequestEmailContentK2ParmView parm = TestHelper.JsonToObject<GetCreditApplicationRequestEmailContentK2ParmView>(path);
            return parm;
        }
        public static GetCreditApplicationRequestEmailContentK2ResultView GetResult(string caseName)
        {
            string path = $"../../../K2/GetCreditApplicationRequestEmailContentK2/{caseName}/result.json";
            GetCreditApplicationRequestEmailContentK2ResultView result = TestHelper.JsonToObject<GetCreditApplicationRequestEmailContentK2ResultView>(path);
            return result;
        }
    }
}
