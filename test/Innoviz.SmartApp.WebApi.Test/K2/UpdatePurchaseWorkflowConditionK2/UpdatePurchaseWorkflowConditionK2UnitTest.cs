﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.K2.UpdatePurchaseWorkflowConditionK2
{
    public class UpdatePurchaseWorkflowConditionK2UnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public UpdatePurchaseWorkflowConditionK2UnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "UpdatePurchaseWorkflowConditionK2");
        }
        [Theory]
        [MemberData(nameof(CaseUpdatePurchaseWorkflowConditionK2))]
        public void UpdatePurchaseWorkflowConditionK2(UpdatePurchaseWorkflowConditionK2ParmView parm, PurchaseTableItemView result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../K2/UpdatePurchaseWorkflowConditionK2/Case_ExampleData/masterdata.json");

                IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                PurchaseTableItemView actual = purchaseTableService.UpdatePurchaseWorkflowConditionK2(parm.PurchaseTableGUID);

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(PurchaseTableItemView), new string[] { "PurchaseTableGUID",
                                                                    "AdditionalPurchase",
                                                                    "CreditAppTableGUID",
                                                                    "CustomerTableGUID",
                                                                    "Description",
                                                                    "Dimension1GUID",
                                                                    "Dimension2GUID",
                                                                    "Dimension3GUID",
                                                                    "Dimension4GUID",
                                                                    "Dimension5GUID",
                                                                    "DocumentStatusGUID",
                                                                    "ProductType",
                                                                    "PurchaseDate",
                                                                    "PurchaseId",
                                                                    "ReceiptTempTableGUID",
                                                                    "RetentionCalculateBase",
                                                                    "RetentionFixedAmount",
                                                                    "RetentionPct",
                                                                    "Rollbill",
                                                                    "TotalInterestPct",
                                                                    "CreditAppTable_Values",
                                                                    "CustomerTable_Values",
                                                                    "DocumentStatus_StatusId",
                                                                    "DocumentStatus_Description",
                                                                    "NetPaid",
                                                                    "ProcessInstanceId",
                                                                    "AccessModeView"} }
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(result, actual);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseUpdatePurchaseWorkflowConditionK2
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm(), GetResult() }
                };
            }
        }
        public static UpdatePurchaseWorkflowConditionK2ParmView GetParm()
        {
            string path = "../../../K2/UpdatePurchaseWorkflowConditionK2/Case_ExampleData/parameter.json";
            UpdatePurchaseWorkflowConditionK2ParmView parm = TestHelper.JsonToObject<UpdatePurchaseWorkflowConditionK2ParmView>(path);
            return parm;
        }
        public static PurchaseTableItemView GetResult()
        {
            string path = "../../../K2/UpdatePurchaseWorkflowConditionK2/Case_ExampleData/result.json";
            PurchaseTableItemView result = TestHelper.JsonToObject<PurchaseTableItemView>(path);
            return result;
        }
    }
}
