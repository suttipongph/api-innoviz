﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.SharedQuery.SharedQuery01_CustomerAging
{
    public class GetCustomerAgingUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public GetCustomerAgingUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "GetCustomerAging");
        }
        [Theory]
        [MemberData(nameof(CaseGetCustomerAging))]
        public void GetCustomerAging(ReportAgingView parameter, Result expectedResult)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../SharedQuery/SharedQuery01_CustomerAging/masterdata.json", true, false, true);
                ICustomerAgingService customerAgingService = new CustomerAgingService(db);

                #region prepare expected results
                if (expectedResult == null) expectedResult = new Result();
                #endregion prepare expected results

                List<CustomerAgingViewMap> actualResult = customerAgingService.GetCustomerAging(parameter);

                #region prepare actual result
                Assert.True(expectedResult.CustomerAgingViewMap.Count == actualResult.Count);
                #endregion prepare actual result

                #region assert should equal
                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(CustomerAgingViewMap), new string[] { "RowAuthorize", "AccessModeView" } }
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedResult.CustomerAgingViewMap, actualResult);
                #endregion assert should equal
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseGetCustomerAging
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm("Case1_Factoring"), GetResult("Case1_Factoring") },
                };
            }
        }
        public static ReportAgingView GetParm(string folder)
        {
            string path = "../../../SharedQuery/SharedQuery01_CustomerAging/" + folder + "/parameter.json";
            ReportAgingView parm = TestHelper.JsonToObject<ReportAgingView>(path);
            return parm;
        }
        public static Result GetResult(string folder)
        {
            string path = "../../../SharedQuery/SharedQuery01_CustomerAging/" + folder + "/result.json";
            Result expectedResult = TestHelper.JsonToObject<Result>(path);
            return expectedResult;
        }
        public class Result
        {
            public List<CustomerAgingViewMap> CustomerAgingViewMap { get; set; }
        }
    }
}
