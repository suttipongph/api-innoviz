﻿using Innoviz.SmartApp.Core;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.System.Condition
{
    public class EnumUnitTest
    {
        private readonly ITestOutputHelper output;
        public EnumUnitTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void IsDefined()
        {
            try
            {
                CreditAppTable creditAppTable = new CreditAppTable();
                creditAppTable.ProductType = 1;
                Assert.True(ConditionService.IsEnumDefined<ProductType>(creditAppTable.ProductType));
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
            
        }
    }
}
