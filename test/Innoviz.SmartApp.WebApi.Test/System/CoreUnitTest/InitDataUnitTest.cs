﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.System.CoreUnitTest
{
    public class InitDataUnitTest
    {
        private ITestOutputHelper output;
        public InitDataUnitTest(ITestOutputHelper output)
        {
            TestHelper.InitSystemValues(output);
            this.output = output;
        }

        [Fact]
        public void ExcelDataToJson()
        {
            try
            {
                FileHelper fileHelper = new FileHelper();
                string inputFilePath = @"D:\Projects\Project_Docs\LIT\spec\LIT1370_Post Withdrawal\LIT1370_Post Withdrawal_TestData.xlsx";
                string outputFilePath = @"D:\LeasingTest\ExcelToJson\sample_testdata.json";

                if (!File.Exists(inputFilePath))
                {
                    output.WriteLine("File path does not exist: " + inputFilePath);
                    return;
                }

                fileHelper.ReadMultiTableExcelTestDataToJson<SmartAppContext>(inputFilePath, outputFilePath);
                
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        [Fact]
        public void ExcelTestResultToJson()
        {
            try
            {
                FileHelper fileHelper = new FileHelper();
                string inputFilePath = @"D:\Projects\Project_Docs\LIT\spec\LIT1383_Recalculate interest\TestData_result.xlsx";
                string outputFilePath = @"D:\LeasingTest\ExcelToJson\recalwithdrawal_testdata_result.json";

                if (!File.Exists(inputFilePath))
                {
                    output.WriteLine("File path does not exist: " + inputFilePath);
                    return;
                }

                fileHelper.ReadMultiTableExcelTestDataToJson<RecalWithdrawalInterestViewMap>(inputFilePath, outputFilePath);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        [Fact]
        public void ExcelTestParameterToJson()
        {
            try
            {
                FileHelper fileHelper = new FileHelper();
                string inputFilePath = @"D:\Projects\Project_Docs\LIT\spec\LIT1383_Recalculate interest\TestData_parameter.xlsx";
                string outputFilePath = @"D:\LeasingTest\ExcelToJson\recalwithdrawal_testdata_parameter.json";

                if (!File.Exists(inputFilePath))
                {
                    output.WriteLine("File path does not exist: " + inputFilePath);
                    return;
                }

                fileHelper.ReadTestDataParameterToJson<RecalWithdrawalInterestParamView>(inputFilePath, outputFilePath);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        [Fact]
        public void AddTestData()
        {
            try
            {
                string jsonPath = @"D:\LeasingTest\ExcelToJson\sample_data.json";
                if(!File.Exists(jsonPath))
                {
                    output.WriteLine("File path does not exist: " + jsonPath);
                    return;
                }
                
                string connectionString = "Data Source=(LocalDb)\\MSSQLLocalDB;Initial Catalog=SmartApp_LIT_TestAddData;trusted_connection=yes;";

                SmartAppContext context = TestHelper.BuildSmartAppContext_UseSqlServer(output, connectionString, true);
                TestHelper.AddTestData(output, context, jsonPath, false, true);

                //SmartAppContext context = TestHelper.BuildSmartAppContext_UseInMemory(output, "test");
                //TestHelper.AddTestData(output, context, jsonPath, true, true);

                output.WriteLine(context.Company.FirstOrDefault().CompanyGUID.GuidNullToString());
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
    }
}
