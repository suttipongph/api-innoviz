﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Innoviz.SmartApp.WebApi.Test.System.CoreUnitTest
{
    public class ShouldEqualUnitTest
    {
        private readonly ITestOutputHelper output;
        public ShouldEqualUnitTest(ITestOutputHelper output)
        {
            TestHelper.InitSystemValues(output);
            this.output = output;
        }

        [Fact]
        public void SingleObjectShouldEqual()
        {
            CreditAppRequestTable creditAppRequestTable1 = new CreditAppRequestTable { ProductType = 1, ApproverComment = "", ApprovedDate = "04/05/2021".StringToDate(), MaxRetentionAmount = 1000.0m };
            CreditAppRequestTable creditAppRequestTable2 = new CreditAppRequestTable { ProductType = 1, ApprovedDate = "04/05/2021 00:00:00".StringToDateTime(), MaxRetentionAmount = 1000 };
            TestHelper helper = new TestHelper();
            helper.AssertShouldEqual(creditAppRequestTable1, creditAppRequestTable2);
        }
        [Fact]
        public void SingleObjectShouldEqualIgnoreField()
        {
            CreditAppRequestTable creditAppRequestTable1 = new CreditAppRequestTable { ProductType = 1, ApprovedDate = "04/05/2021".StringToDate(), MaxRetentionAmount = 1000.0m };
            CreditAppRequestTable creditAppRequestTable2 = new CreditAppRequestTable { ProductType = 1, ApprovedDate = "04/05/2021 00:00:01".StringToDateTime(), MaxRetentionAmount = 1000 };
                
            Dictionary<Type, string[]> config = new Dictionary<Type, string[]>() { { typeof(CreditAppRequestTable), new string[] { "ApprovedDate" } } } ;
            TestHelper helper = new TestHelper(config);
                
            helper.AssertShouldEqual(creditAppRequestTable1, creditAppRequestTable2);
        }
        [Fact]
        public void SingleObjectShouldNotEqual()
        {
            CreditAppRequestTable creditAppRequestTable1 = new CreditAppRequestTable { ProductType = 1, ApproverComment = " ", MaxRetentionAmount = 1000.0m };
            CreditAppRequestTable creditAppRequestTable2 = new CreditAppRequestTable { ProductType = 1, MaxRetentionAmount = 1000 };

            TestHelper helper = new TestHelper();

            Assert.Throws<XunitException>(() => helper.AssertShouldEqual(creditAppRequestTable1, creditAppRequestTable2));
        }
        [Fact]
        public void CollectionShouldEqual()
        {
            int rowCount = 3;
            List<CreditAppRequestTable> expected = new List<CreditAppRequestTable>();
            List<CreditAppRequestTable> actual = new List<CreditAppRequestTable>();
            for (int i=0; i<rowCount; i++)
            {
                CreditAppRequestTable creditAppRequestTable = new CreditAppRequestTable { ProductType = 1, ApprovedDate = "11/05/2021".StringToDate(), CreditAppRequestTableGUID = Guid.NewGuid() };
                expected.Add(creditAppRequestTable);
                actual.Add(creditAppRequestTable);
            }

            Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
            {
                { typeof(CreditAppRequestTable), new string[] { "CreditAppRequestTableGUID" } }
            };
            TestHelper helper = new TestHelper(fieldsToIgnore);
            helper.AssertShouldEqual(expected, actual);
        }
        [Fact]
        public void CollectionShouldNotEqual()
        {
            int rowCount = 2;
            List<CreditAppRequestTable> expected = new List<CreditAppRequestTable>();
            List<CreditAppRequestTable> actual = new List<CreditAppRequestTable>();
            CreditAppRequestTable creditAppRequestTable0 = new CreditAppRequestTable { ProductType = 1, ApprovedDate = "11/05/2021".StringToDate(), CreditAppRequestTableGUID = Guid.NewGuid() };
            expected.Add(creditAppRequestTable0);

            Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
            {
                { typeof(CreditAppRequestTable), new string[] { "CreditAppRequestTableGUID" } }
            };
            TestHelper helper = new TestHelper(fieldsToIgnore);

            Assert.Throws<XunitException>(() => helper.AssertShouldEqual(expected, actual));

            actual.Add(creditAppRequestTable0);
            for (int i = 0; i < rowCount; i++)
            {
                CreditAppRequestTable creditAppRequestTable = new CreditAppRequestTable { ProductType = 1, ApprovedDate = "11/05/2021".StringToDate(), CreditAppRequestTableGUID = Guid.NewGuid() };
                expected.Add(creditAppRequestTable);

                CreditAppRequestTable creditAppRequestTable1 = new CreditAppRequestTable { ProductType = 1, ApprovedDate = "11/05/2021".StringToDate(), CreditAppRequestTableGUID = Guid.NewGuid() };
                creditAppRequestTable1.ApproverComment = "Comment1";
                creditAppRequestTable1.CreditAppRequestType = 2;
                creditAppRequestTable1.ApprovedCreditLimitRequest = 0.01m;
                if(i == rowCount -1)
                {
                    creditAppRequestTable1.CACondition = " ";
                }
                actual.Add(creditAppRequestTable1);
            }
            
            Assert.Throws<XunitException>(() => helper.AssertShouldEqual(expected, actual));
        }
    }
}
