﻿using Innoviz.SmartApp.Core.Constants;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.System
{
    public class LinqForForeachUnitTest
    {
        private readonly ITestOutputHelper output;
        public LinqForForeachUnitTest(ITestOutputHelper output)
        {
            this.output = output;
        }
        [Fact]
        public void LinqForForeachPerformanceTest()
        {
            int rows = 1000000;
            List<CreditAppRequestTable> list00 = new List<CreditAppRequestTable>();
            List<CreditAppRequestTable> list01 = new List<CreditAppRequestTable>();
            List<CreditAppRequestTable> list02 = new List<CreditAppRequestTable>();
            List<CreditAppRequestTable> list03 = new List<CreditAppRequestTable>();
            for (int i=0; i< rows; i++)
            {
                list00.Add(new CreditAppRequestTable { CreditAppRequestId = "string " + i });
                list01.Add(new CreditAppRequestTable { CreditAppRequestId = "string " + i });
                list02.Add(new CreditAppRequestTable { CreditAppRequestId = "string " + i });
                list03.Add(new CreditAppRequestTable { CreditAppRequestId = "string " + i });
            }
            Stopwatch sw;
            // list projects to new list
            sw = Stopwatch.StartNew();
            List<CreditAppRequestTable> list2 = list00.Select(s => s).ToList();
            sw.Stop();
            output.WriteLine("List Linq Select => Elapsed " + sw.Elapsed.TotalMilliseconds + " ms");


            List<CreditAppRequestTable> list5 = new List<CreditAppRequestTable>();
            sw = Stopwatch.StartNew();
            foreach (var item in list01)
            {
                list5.Add(item);
            }
            sw.Stop();
            output.WriteLine("List foreach loop => Elapsed " + sw.Elapsed.TotalMilliseconds + " ms");

            

            List<CreditAppRequestTable> list3 = new List<CreditAppRequestTable>();
            sw = Stopwatch.StartNew();
            //list02.ForEach(f => list3.Add(f));
            foreach (var item in list02)
            {
                list3.Add(item);
            }
            sw.Stop();
            output.WriteLine("List ForEach => Elapsed " + sw.Elapsed.TotalMilliseconds + " ms");

            List<CreditAppRequestTable> list4 = new List<CreditAppRequestTable>();
            sw = Stopwatch.StartNew();
            for(int i=0; i< rows; i++)
            {
                list4.Add(list03[i]);
            }
            sw.Stop();
            output.WriteLine("List for loop => Elapsed " + sw.Elapsed.TotalMilliseconds + " ms");
        }
    
        [Fact]
        public void LoopSearchPredicateForCheckDuplicate()
        {
            try
            {
                #region setup
                int rows = 10000;
                List<WithdrawalLine> list00 = new List<WithdrawalLine>();
                WithdrawalLine wl = new WithdrawalLine
                {
                    LineNum = 1,
                    WithdrawalTableGUID = Guid.NewGuid(),
                    WithdrawalLineGUID = Guid.NewGuid(),
                    CompanyGUID = Guid.NewGuid()
                };
                for (int i = 0; i < rows; i++)
                {
                    list00.Add(wl);
                }
                Type modelType = typeof(WithdrawalLine);
                var primaryKey = modelType.GetProperties().Where(s => s.Name == "WithdrawalLineGUID");
                var checkProperties = modelType.GetProperties().Where(s => s.Name == "WithdrawalTableGUID" ||
                                                                                            s.Name == "LineNum" ||
                                                                                            s.Name == "CompanyGUID");
                var items = list00.Select(s => s);
                int checkPropCount = checkProperties.Count();
                int pkCount = primaryKey.Count();
                int itemsCount = items.Count();
                #endregion
                
                TestHelper helper = new TestHelper();
                helper.LogTimeStart();

                int take = 100;
                int startIdx = 0;
                int endIdx = itemsCount <= take ? itemsCount : take;
                while (endIdx <= itemsCount)
                {
                    List<SearchCondition> searchConditions = new List<SearchCondition>();
                    #region loop collection
                    for (int idx = startIdx; idx < endIdx; idx++)
                    {
                        var item = items.ElementAt(idx);
                        // properties to check
                        for (int i = 0; i < checkPropCount; i++)
                        {
                            var checkProperty = checkProperties.ElementAt(i);
                            var value = checkProperty.GetValue(item);

                            SearchCondition searchCondition = new SearchCondition
                            {
                                ColumnName = checkProperty.Name,
                                Operator = (idx == startIdx && i == 0) ? Operators.AND : i == 0 ? Operators.OR : Operators.AND,
                                Type = checkProperty.PropertyType.Name,
                                Value = value != null ? value.ToString() : "",
                                Bracket = (idx == startIdx && i == 0) ? (int)BracketType.DoubleStart :
                                        i == 0 ? (int)BracketType.SingleStart : (int)BracketType.None
                            };
                            searchConditions.Add(searchCondition);
                        }
                        // pk
                        for (int i = 0; i < pkCount; i++)
                        {
                            var key = primaryKey.ElementAt(i);
                            var value = key.GetValue(item);
                            if (value != null)
                            {
                                SearchCondition searchCondition = new SearchCondition
                                {
                                    ColumnName = key.Name,
                                    Operator = Operators.AND,
                                    Type = key.PropertyType.Name,
                                    Value = value.ToString(),
                                    EqualityOperator = Operators.NOT_EQUAL,
                                    Bracket = (idx == endIdx - 1 && i == pkCount - 1) ? (int)BracketType.DoubleEnd :
                                        (i == pkCount - 1) ? (int)BracketType.SingleEnd : (int)BracketType.None
                                };
                                searchConditions.Add(searchCondition);
                            }
                        }
                    }
                    #endregion
                    SearchParameter search = new SearchParameter
                    {
                        Conditions = searchConditions
                    };
                    var predicate = search.GetSearchPredicate(modelType, true);
                    output.WriteLine(predicate.Predicates);
                    output.WriteLine("");
                    //if (Entity.Where(predicate.Predicates, predicate.Values).Count() > 0)
                    //{
                    //    ex.AddData("ERROR.DUPLICATE", errParams);
                    //}

                    startIdx += take;
                    endIdx += take;
                }
                helper.LogElapsedTime(output, "Loop build search conditions " + rows + "of WithdrawalLine");
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        [Fact]
        public void LinqSearchPredicateForCheckDuplicate()
        {
            try
            {
                #region setup
                int rows = 10000;
                List<WithdrawalLine> list00 = new List<WithdrawalLine>();
                WithdrawalLine wl = new WithdrawalLine
                {
                    LineNum = 1,
                    WithdrawalTableGUID = Guid.NewGuid(),
                    WithdrawalLineGUID = Guid.NewGuid(),
                    CompanyGUID = Guid.NewGuid()
                };
                for (int i = 0; i < rows; i++)
                {
                    list00.Add(wl);
                }
                Type modelType = typeof(WithdrawalLine);
                var primaryKey = modelType.GetProperties().Where(s => s.Name == "WithdrawalLineGUID");
                var checkProperties = modelType.GetProperties().Where(s => s.Name == "WithdrawalTableGUID" ||
                                                                                            s.Name == "LineNum" ||
                                                                                            s.Name == "CompanyGUID");
                var items = list00.Select(s => s);
                int checkPropCount = checkProperties.Count();
                int pkCount = primaryKey.Count();
                int itemsCount = items.Count();
                #endregion

                List<SearchCondition> searchConditions = new List<SearchCondition>();
                TestHelper helper = new TestHelper();
                helper.LogTimeStart();
                #region loop collection
                var x =
                items.Select((item, idx) => new
                {
                    CheckConds = checkProperties.Select((checkProperty, i) => new SearchCondition
                    {
                        ColumnName = checkProperty.Name,
                        Operator = idx == 0 ? Operators.AND : i == 0 ? Operators.OR : Operators.AND,
                        Type = checkProperty.PropertyType.Name,
                        Value = checkProperty.GetValue(item) != null ? checkProperty.GetValue(item).ToString() : "",
                        //Bracket = idx == 0 ? (int)BracketType.DoubleStart :
                        //        i == 0 ? (int)BracketType.SingleStart : (int)BracketType.None
                    }),
                    PKConds = primaryKey.Select((key, i) => new SearchCondition
                    {
                        ColumnName = key.Name,
                        Operator = Operators.AND,
                        Type = key.PropertyType.Name,
                        Value = key.GetValue(item).ToString(),
                        EqualityOperator = Operators.NOT_EQUAL,
                        //Bracket = (idx == itemsCount - 1) ? (int)BracketType.DoubleEnd :
                        //    (i == pkCount - 1) ? (int)BracketType.SingleEnd : (int)BracketType.None
                    })
                });
                searchConditions = x.SelectMany(sm => sm.CheckConds.Concat(sm.PKConds)).ToList();
                
                #endregion
                SearchParameter search = new SearchParameter
                {
                    Conditions = searchConditions
                };
                var predicate = search.GetSearchPredicate(modelType, true);
                helper.LogElapsedTime(output, "Linq build search conditions " + searchConditions.Count + "conds");
                output.WriteLine(predicate.Predicates);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
    }
}
