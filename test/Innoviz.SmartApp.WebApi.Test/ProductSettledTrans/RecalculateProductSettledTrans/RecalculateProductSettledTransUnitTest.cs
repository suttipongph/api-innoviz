﻿using Innoviz.SmartApp.Core.Models;
using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModels;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelHandlerV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace Innoviz.SmartApp.WebApi.Test.ProductSettledTrans.RecalculateProductSettledTrans
{
    public class RecalculateProductSettledTransUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public RecalculateProductSettledTransUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "RecalculateProductSettledTrans");
        }
        [Theory]
        [MemberData(nameof(CaseRecalculateProductSettledTrans))]
        public void RecalculateProductSettledTrans(RecalProductSettledTransView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../ProductSettledTrans/RecalculateProductSettledTrans/Case_ExampleData/masterdata.json");

                InvoiceSettlementDetail expectedInvoiceSettlementDetail = result.InvoiceSettlementDetail;

                IInvoiceSettlementDetailService invoiceSettlementDetailService = new InvoiceSettlementDetailService(db);
                InvoiceSettlementDetailItemView actualInvoiceSettlementDetailItemView = invoiceSettlementDetailService.GetInvoiceSettlementDetailById(parm.InvoiceSettlementDetailGUID);
                InvoiceSettlementDetail actualInvoiceInvoiceSettlementDetail = actualInvoiceSettlementDetailItemView.ToInvoiceSettlementDetail();

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(InvoiceSettlementDetailItemView), new string[] { "InvoiceSettlementDetailGUID" } }
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedInvoiceSettlementDetail, actualInvoiceInvoiceSettlementDetail);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }

        public static IEnumerable<object[]> CaseRecalculateProductSettledTrans
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm(), GetResult() }
                };
            }
        }
        public static RecalProductSettledTransView GetParm()
        {
            string path = "../../../ProductSettledTrans/RecalculateProductSettledTrans/Case_ExampleData/parameter.json";
            RecalProductSettledTransView parm = TestHelper.JsonToObject<RecalProductSettledTransView>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../ProductSettledTrans/RecalculateProductSettledTrans/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public InvoiceSettlementDetail InvoiceSettlementDetail { get; set; }
    }
}
