﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.FreeTextInvoice.UpdateAssignmentSettlement
{
    public class UpdateAssignmentSettlementUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public UpdateAssignmentSettlementUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "UpdateAssignmentSettlement");
        }

        [Theory]
        [MemberData(nameof(CaseUpdateAssignmentSettlement))]
        public void UpdateAssignmentSettlement(UpdateAssignmentAgreementSettleView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../AssignmentagreementSettle/UpdateAssignmentSettlement/Case_1_PassValidate/masterdata.json");
                AssignmentAgreementSettle expectedRevertAssignmentAgreementSettle = result.AssignmentAgreementSettle.First();
                AssignmentAgreementSettle expectedNewAssignmentAgreementSettle = result.AssignmentAgreementSettle.Last();

                IAssignmentAgreementSettleService assignmentAgreementSettleService = new AssignmentAgreementSettleService(db);
                List<AssignmentAgreementSettle> resultList = assignmentAgreementSettleService.InitUpdateAssignmentSettlement(parm);
                UpdateAssignmentAgreementSettleResultView actual = new UpdateAssignmentAgreementSettleResultView();
                actual.RevertAssignmentAgreementSettle = resultList.First();
                actual.NewSettledAmount = resultList.Last();

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(AssignmentAgreementSettle), new string[] { "AssignmentAgreementSettleGUID" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedRevertAssignmentAgreementSettle, actual.RevertAssignmentAgreementSettle);
                helper.AssertShouldEqual(expectedNewAssignmentAgreementSettle, actual.NewSettledAmount);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseUpdateAssignmentSettlement
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm(), GetResult() }
                };
            }
        }
        public static UpdateAssignmentAgreementSettleView GetParm()
        {
            string path = "../../../AssignmentagreementSettle/UpdateAssignmentSettlement/Case_1_PassValidate/parameter.json";
            UpdateAssignmentAgreementSettleView parm = TestHelper.JsonToObject<UpdateAssignmentAgreementSettleView>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../AssignmentagreementSettle/UpdateAssignmentSettlement/Case_1_PassValidate/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<AssignmentAgreementSettle> AssignmentAgreementSettle { get; set; }
    }
}
