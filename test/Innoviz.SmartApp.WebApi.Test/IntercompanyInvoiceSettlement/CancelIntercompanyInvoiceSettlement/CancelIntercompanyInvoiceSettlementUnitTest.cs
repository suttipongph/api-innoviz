﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.IntercompanyInvSettlement.CancelIntercompanyInvoiceSettlement
{
    public class CancelIntercompanyInvoiceSettlementUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public CancelIntercompanyInvoiceSettlementUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "CancelIntercompanyInvoiceSettlement");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }

        [Theory]
        [MemberData(nameof(CaseCancelIntercompanyInvoiceSettlement))]
        public void CancelIntercompanyInvoiceSettlement(CancelIntercompanyInvSettlementView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../IntercompanyInvoiceSettlement/CancelIntercompanyInvoiceSettlement/Case_ExampleData/masterdata.json");

                IntercompanyInvoiceSettlement expectedCreateIntercompanyInvoiceSettlement = result.CreateIntercompanyInvoiceSettlement.FirstOrDefault();
                IntercompanyInvoiceSettlement expectedUpdateIntercompanyInvoiceSettlement = result.UpdateIntercompanyInvoiceSettlement.FirstOrDefault();
                StagingTableIntercoInvSettle expectedStagingTableIntercoInvSettle = result.StagingTableIntercoInvSettle.FirstOrDefault();
                IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
                //intercompanyInvoiceSettlementService.CancelIntercompanyInvSettlement(parm);
                CancelIntercompanyInvSettlementViewMap actual = intercompanyInvoiceSettlementService.InitCancelIntercompanyInvSettlement(parm);

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(IntercompanyInvoiceSettlement), new string[] { "IntercompanyInvoiceSettlementGUID", "RefIntercompanyInvoiceSettlementGUID" } },
                    { typeof(StagingTableIntercoInvSettle), new string[] { "StagingTableIntercoInvSettleGUID", "IntercompanyInvoiceSettlementGUID" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedCreateIntercompanyInvoiceSettlement, actual.CreateIntercompanyInvoiceSettlement);
                helper.AssertShouldEqual(expectedUpdateIntercompanyInvoiceSettlement, actual.UpdateIntercompanyInvoiceSettlement);
                helper.AssertShouldEqual(expectedStagingTableIntercoInvSettle, actual.StagingTableIntercoInvSettle);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CaseCancelIntercompanyInvoiceSettlement
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm(), GetResult() }
                };
            }
        }
        public static CancelIntercompanyInvSettlementView GetParm()
        {
            string path = "../../../IntercompanyInvoiceSettlement/CancelIntercompanyInvoiceSettlement/Case_ExampleData/parameter.json";
            CancelIntercompanyInvSettlementView parm = TestHelper.JsonToObject<CancelIntercompanyInvSettlementView>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../IntercompanyInvoiceSettlement/CancelIntercompanyInvoiceSettlement/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<IntercompanyInvoiceSettlement> CreateIntercompanyInvoiceSettlement { get; set; }
        public List<IntercompanyInvoiceSettlement> UpdateIntercompanyInvoiceSettlement { get; set; }
        public List<StagingTableIntercoInvSettle> StagingTableIntercoInvSettle { get; set; }
    }
}
