﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.IntercompanyInvSettlement.PostIntercompanyInvoiceSettlement
{
    public class PostIntercompanyInvoiceSettlementUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostIntercompanyInvoiceSettlementUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostIntercompanyInvoiceSettlement");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }

        [Theory]
        [MemberData(nameof(CasePostIntercompanyInvoiceSettlement))]
        public void PostIntercompanyInvoiceSettlement(PostIntercompanyInvSettlementView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../IntercompanyInvoiceSettlement/PostIntercompanyInvoiceSettlement/Case_ExampleData/masterdata.json");

                IntercompanyInvoiceSettlement expectedIntercompanyInvoiceSettlement = result.IntercompanyInvoiceSettlement.FirstOrDefault();
                StagingTableIntercoInvSettle expectedStagingTableIntercoInvSettle = result.StagingTableIntercoInvSettle.FirstOrDefault();
                IIntercompanyInvoiceSettlementService intercompanyInvoiceSettlementService = new IntercompanyInvoiceSettlementService(db);
                //intercompanyInvoiceSettlementService.PostIntercompanyInvSettlement(parm);
                PostIntercompanyInvSettlementViewMap actual = intercompanyInvoiceSettlementService.InitPostIntercompanyInvSettlement(parm.IntercompanyInvoiceSettlementGUID.StringToGuid());

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(IntercompanyInvoiceSettlement), new string[] { } },
                    { typeof(StagingTableIntercoInvSettle), new string[] { "StagingTableIntercoInvSettleGUID" } },
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedIntercompanyInvoiceSettlement, actual.IntercompanyInvoiceSettlement);
                helper.AssertShouldEqual(expectedStagingTableIntercoInvSettle, actual.StagingTableIntercoInvSettle);
            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostIntercompanyInvoiceSettlement
        {
            get
            {
                return new[]
                {
                    new object[] {GetParm(), GetResult() }
                };
            }
        }
        public static PostIntercompanyInvSettlementView GetParm()
        {
            string path = "../../../IntercompanyInvoiceSettlement/PostIntercompanyInvoiceSettlement/Case_ExampleData/parameter.json";
            PostIntercompanyInvSettlementView parm = TestHelper.JsonToObject<PostIntercompanyInvSettlementView>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../IntercompanyInvoiceSettlement/PostIntercompanyInvoiceSettlement/Case_ExampleData/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public List<IntercompanyInvoiceSettlement> IntercompanyInvoiceSettlement { get; set; }
        public List<StagingTableIntercoInvSettle> StagingTableIntercoInvSettle { get; set; }
    }
}
