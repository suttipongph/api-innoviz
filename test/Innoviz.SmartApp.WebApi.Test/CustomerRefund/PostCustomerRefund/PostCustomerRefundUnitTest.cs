﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.CustomerRefund.PostCustomerRefunds
{
    public class PostCustomerRefundUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostCustomerRefundUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostCustomerRefund");
        }
        [Theory]
        [MemberData(nameof(CasePostCustomerRefund))]
        public void PostCustomerRefund(PostCustomerRefundView parm, Result result)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../CustomerRefund/PostCustomerRefund/Case1/masterdata.json");

                if (result.InvoiceTable == null) result.InvoiceTable = new List<InvoiceTable>();
                if (result.InvoiceLine == null) result.InvoiceLine = new List<InvoiceLine>();
                if (result.InvoiceSettlementDetail == null) result.InvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                if (result.VendorPaymentTrans == null) result.VendorPaymentTrans = new List<VendorPaymentTrans>();
                if (result.RetentionTrans == null) result.RetentionTrans = new List<RetentionTrans>();
                if (result.ProcessTrans == null) result.ProcessTrans = new List<ProcessTrans>();
                if (result.CustTrans == null) result.CustTrans = new List<CustTrans>();
                if (result.CreditAppTrans == null) result.CreditAppTrans = new List<CreditAppTrans>();
                if (result.TaxInvoiceTable == null) result.TaxInvoiceTable = new List<TaxInvoiceTable>();
                if (result.TaxInvoiceLine == null) result.TaxInvoiceLine = new List<TaxInvoiceLine>();
                if (result.ReceiptTable == null) result.ReceiptTable = new List<ReceiptTable>();
                if (result.ReceiptLine == null) result.ReceiptLine = new List<ReceiptLine>();
                if (result.PaymentHistory == null) result.PaymentHistory = new List<PaymentHistory>();

                if (result.InvoiceSettlementDetail_Update == null) result.InvoiceSettlementDetail_Update = new List<InvoiceSettlementDetail>();
                if (result.CustTrans_Update == null) result.CustTrans_Update = new List<CustTrans>();

                CustomerRefundTable expectedCustomerRefundTable = result.CustomerRefundTable;
                List<InvoiceTable> expectedInvoiceTable = result.InvoiceTable.OrderBy(o => o.InvoiceId).ToList();
                List<InvoiceLine> expectedInvoiceLine = (from invoiceLine in result.InvoiceLine
                                                         join invoiceTable in result.InvoiceTable
                                                         on invoiceLine.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
                                                         from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
                                                         select new
                                                         {
                                                             invoiceLine,
                                                             invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : invoiceLine.InvoiceTableGUID.ToString()
                                                         }).OrderBy(o => o.invoiceId).Select(s => s.invoiceLine).ToList();
                List<InvoiceSettlementDetail> expectedInvoiceSettlementDetail = (from invoiceSettlementDetail in result.InvoiceSettlementDetail
                                                                                 join invoiceTable in result.InvoiceTable
                                                                                 on invoiceSettlementDetail.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
                                                                                 from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
                                                                                 select new
                                                                                 {
                                                                                     invoiceSettlementDetail,
                                                                                     invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : invoiceSettlementDetail.InvoiceTableGUID.ToString()
                                                                                 }).OrderBy(o => o.invoiceId).Select(s => s.invoiceSettlementDetail).ToList();
                List<VendorPaymentTrans> expectedVendorPaymentTrans = result.VendorPaymentTrans.OrderBy(o => o.VendorTableGUID).ToList();
                List<RetentionTrans> expectedRetentionTrans = result.RetentionTrans;
                List<ProcessTrans> expectedProcessTranses = (from processTrans in result.ProcessTrans
                                                             join invoiceTable in result.InvoiceTable
                                                             on new { processTrans.RefType, InvoiceTableGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.Invoice, invoiceTable.InvoiceTableGUID } into ljInvoiceTableProcessTrans
                                                             from invoiceTable in ljInvoiceTableProcessTrans.DefaultIfEmpty()
                                                             join vendorPaymentTrans in result.VendorPaymentTrans
                                                             on new { processTrans.RefType, VendorPaymentTransGUID = processTrans.RefGUID.Value } equals new { RefType = (int)RefType.VendorPaymentTrans, vendorPaymentTrans.VendorPaymentTransGUID } into ljVendorPaymentTransProcessTrans
                                                             from vendorPaymentTrans in ljVendorPaymentTransProcessTrans.DefaultIfEmpty()
                                                             select new
                                                             {
                                                                 processTrans,
                                                                 processTrans.RefType,
                                                                 RefId = invoiceTable != null ? invoiceTable.InvoiceId : (vendorPaymentTrans != null ? vendorPaymentTrans.VendorTableGUID.ToString() : string.Empty)
                                                             }).OrderBy(o => o.RefType).ThenBy(t => t.RefId).ThenBy(t => t.processTrans.DocumentId).Select(s => s.processTrans).ToList();
                List<CustTrans> expectedCustTrans = (from custTrans in result.CustTrans
                                                       join invoiceTable in result.InvoiceTable
                                                       on custTrans.InvoiceTableGUID equals invoiceTable.InvoiceTableGUID into ljInvoiceTableInvoiceSettlementDetail
                                                       from invoiceTable in ljInvoiceTableInvoiceSettlementDetail.DefaultIfEmpty()
                                                       select new
                                                       {
                                                           custTrans,
                                                           invoiceId = invoiceTable != null ? invoiceTable.InvoiceId : custTrans.InvoiceTableGUID.ToString()
                                                       }).OrderBy(o => o.invoiceId).Select(s => s.custTrans).ToList();
                List<CreditAppTrans> expectedCreditAppTrans = result.CreditAppTrans;
                List<TaxInvoiceTable> expectedTaxInvoiceTable = result.TaxInvoiceTable.OrderBy(t => t.TaxInvoiceId).ToList();
                List<TaxInvoiceLine> expectedTaxInvoiceLine = result.TaxInvoiceLine.OrderBy(t => t.UnitPrice).ThenBy(t => t.TotalAmountBeforeTax).ThenBy(t => t.TaxAmount).ThenBy(t => t.TotalAmount).ToList();

                List<PaymentHistory> expectedPaymentHistory = result.PaymentHistory.OrderBy(t => t.PaymentBaseAmount).ToList();
                List<ReceiptTable> expectedReceiptTable = result.ReceiptTable.OrderBy(t =>t.ReceiptId).ToList();
                List<ReceiptLine> expectedReceiptLine = result.ReceiptLine.OrderBy(t => t.InvoiceAmount).ThenBy(t => t.TaxAmount).ToList();
                List<IntercompanyInvoiceTable> expectedIntercompanyInvoiceTable = result.IntercompanyInvoiceTable;

                List<InvoiceSettlementDetail> expectedTaxInvoiceSettlementDetail_Update = result.InvoiceSettlementDetail_Update;
                List<CustTrans> expectedCustTrans_Update = result.CustTrans_Update;


                ICustomerRefundTableService customerRefundTableService = new CustomerRefundTableService(db);
                PostCustomerRefundResultView actual = customerRefundTableService.InitPostCustomerRefundTable(parm);

                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(CustomerRefundTable), new string[] { "CustomerRefundTableGUID" } },
                    { typeof(InvoiceTable), new string[] { "InvoiceTableGUID", "InvoiceId" } },
                    { typeof(InvoiceLine), new string[] { "InvoiceLineGUID", "InvoiceTableGUID" } },
                    { typeof(InvoiceSettlementDetail), new string[] { "InvoiceSettlementDetailGUID", "InvoiceTableGUID" } },
                    { typeof(VendorPaymentTrans), new string[] { "VendorPaymentTransGUID" } },
                    { typeof(RetentionTrans), new string[] { "RetentionTransGUID" } },
                    { typeof(ProcessTrans), new string[] { "ProcessTransGUID","RefGUID","InvoiceTableGUID", "PaymentHistoryGUID", "RefTaxInvoiceGUID" } },
                    { typeof(CustTrans), new string[] { "CustTransGUID", "InvoiceTableGUID" } },
                    { typeof(CreditAppTrans), new string[] { "CreditAppTransGUID"} },
                    { typeof(TaxInvoiceTable), new string[] { "TaxInvoiceTableGUID", "TaxInvoiceRefGUID", "InvoiceTableGUID" } },
                    { typeof(TaxInvoiceLine), new string[] { "TaxInvoiceLineGUID", "TaxInvoiceTableGUID" } },

                    { typeof(PaymentHistory), new string[] { "PaymentHistoryGUID", "CustTransGUID", "InvoiceSettlementDetailGUID", "InvoiceTableGUID", "ReceiptTableGUID" } },
                    { typeof(ReceiptTable), new string[] { "ReceiptTableGUID", "InvoiceSettlementDetailGUID" } },
                    { typeof(ReceiptLine), new string[] { "ReceiptLineGUID", "ReceiptTableGUID", "InvoiceTableGUID" } },
                    { typeof(IntercompanyInvoiceTable), new string[] { "IntercompanyInvoiceTableGUID" } },

                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedCustomerRefundTable, actual.CustomerRefundTable);
                helper.AssertShouldEqual(expectedInvoiceTable, actual.InvoiceTable);
                helper.AssertShouldEqual(expectedInvoiceLine.ToList(), actual.InvoiceLine.ToList());
                helper.AssertShouldEqual(expectedInvoiceSettlementDetail.ToList(), actual.InvoiceSettlementDetail.ToList());
                helper.AssertShouldEqual(expectedVendorPaymentTrans.ToList(), actual.VendorPaymentTrans.ToList());
                helper.AssertShouldEqual(expectedRetentionTrans.OrderBy(t => t.RefGUID).ToList(), actual.RetentionTrans.OrderBy(t => t.RefGUID).ToList());
                
                helper.AssertShouldEqual(expectedProcessTranses.ToList(), 
                                            actual.ProcessTrans);
                
                helper.AssertShouldEqual(expectedCustTrans.ToList(), 
                                            actual.CustTrans.ToList());
                
                helper.AssertShouldEqual(expectedCreditAppTrans, actual.CreditAppTrans);
                helper.AssertShouldEqual(expectedTaxInvoiceTable, actual.TaxInvoiceTable);
                helper.AssertShouldEqual(expectedTaxInvoiceLine, actual.TaxInvoiceLine);

                helper.AssertShouldEqual(expectedPaymentHistory, actual.PaymentHistory);
                helper.AssertShouldEqual(expectedReceiptTable, actual.ReceiptTable);
                helper.AssertShouldEqual(expectedReceiptLine, actual.ReceiptLine);
                helper.AssertShouldEqual(expectedIntercompanyInvoiceTable, actual.IntercompanyInvoiceTable);

                helper.AssertShouldEqual(expectedTaxInvoiceSettlementDetail_Update, actual.InvoiceSettlementDetail_Updated);
                helper.AssertShouldEqual(expectedCustTrans_Update, actual.CustTrans_Update);

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostCustomerRefund
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm(), GetResult() }
                };
            }
        }
        public static PostCustomerRefundView GetParm()
        {
            string path = "../../../CustomerRefund/PostCustomerRefund/Case1/parameter.json";
            PostCustomerRefundView parm = TestHelper.JsonToObject<PostCustomerRefundView>(path);
            return parm;
        }
        public static Result GetResult()
        {
            string path = "../../../CustomerRefund/PostCustomerRefund/Case1/result.json";
            Result result = TestHelper.JsonToObject<Result>(path);
            return result;
        }
    }
    public class Result
    {
        public CustomerRefundTable CustomerRefundTable { get; set; }
        public List<InvoiceTable> InvoiceTable { get; set; }
        public List<InvoiceLine> InvoiceLine { get; set; }
        public List<ProcessTrans> ProcessTrans { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail { get; set; }
        public List<VendorPaymentTrans> VendorPaymentTrans { get; set; }
        public List<RetentionTrans> RetentionTrans { get; set; }
        public List<CustTrans> CustTrans { get; set; }
        public List<CreditAppTrans> CreditAppTrans { get; set; }
        public List<TaxInvoiceTable> TaxInvoiceTable { get; set; }
        public List<TaxInvoiceLine> TaxInvoiceLine { get; set; }

        public List<ReceiptTable> ReceiptTable { get; set; }
        public List<ReceiptLine> ReceiptLine { get; set; }
        public List<PaymentHistory> PaymentHistory { get; set; }
        public List<IntercompanyInvoiceTable> IntercompanyInvoiceTable { get; set; }
        public List<InvoiceSettlementDetail> InvoiceSettlementDetail_Update { get; set; }
        public List<CustTrans> CustTrans_Update { get; set; }
    }
}
