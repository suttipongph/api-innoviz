﻿using Innoviz.SmartApp.Core.Service;
using Innoviz.SmartApp.Core.ViewModelHandler;
using Innoviz.SmartApp.Data;
using Innoviz.SmartApp.Data.Models;
using Innoviz.SmartApp.Data.RepositoriesV2;
using Innoviz.SmartApp.Data.ServicesV2;
using Innoviz.SmartApp.Data.ViewMaps;
using Innoviz.SmartApp.Data.ViewModelsV2;
using Innoviz.SmartApp.WebApi.Test.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using static Innoviz.SmartApp.Data.Models.Enum;

namespace Innoviz.SmartApp.WebApi.Test.Purchase.PostPurchase
{
    public class PostPurchaseUnitTest
    {
        private readonly ITestOutputHelper output;
        private readonly SmartAppContext db;
        public PostPurchaseUnitTest(ITestOutputHelper output)
        {
            this.output = output;
            db = TestHelper.BuildSmartAppContext_UseInMemory(output, "PostPurchase");
            //db = TestHelper.BuildSmartAppContext_UseSqlServer(output, null, true);
        }
        [Theory]
        [MemberData(nameof(CasePostPurchase))]
        public void PostPurchase(Guid purchaseTableGuid, PostPurchaseResultViewMap expectedResult)
        {
            try
            {
                TestHelper.AddTestData(output, db, "../../../Purchase/PostPurchase/masterdata.json", true, false, true); 
                 IPurchaseTableService purchaseTableService = new PurchaseTableService(db);
                //purchaseTableService.PostPurchase(new PostPurchaseParamView { PurchaseTableGUID = purchaseTableGuid.GuidNullToString() });
                IInvoiceSettlementDetailRepo invoiceSettlementDetailRepo = new InvoiceSettlementDetailRepo(db);
                IInvoiceTableRepo invoiceTableRepo = new InvoiceTableRepo(db);
                ICustTransRepo custTransRepo = new CustTransRepo(db);

                List<InvoiceSettlementDetail> purchaseInvoiceSettlementDetail =
                    invoiceSettlementDetailRepo.GetByReference(Data.Models.Enum.RefType.PurchaseTable, purchaseTableGuid);
                
                List<CustTrans> purchInvSettleDetailCustTrans =
                    custTransRepo.GetCustTransByInvoiceTableNoTracking(purchaseInvoiceSettlementDetail
                                                                        .Select(s => s.InvoiceTableGUID.Value)
                                                                        .ToList())
                                .OrderBy(o => o.InvoiceTableGUID).ToList();
                
                #region prepare expected results

                if (expectedResult.PurchaseLine == null) expectedResult.PurchaseLine = new List<PurchaseLine>();
                if (expectedResult.InvoiceTable == null) expectedResult.InvoiceTable = new List<InvoiceTable>();
                if (expectedResult.InvoiceLine == null) expectedResult.InvoiceLine = new List<InvoiceLine>();
                if (expectedResult.InvoiceSettlementDetail == null) expectedResult.InvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                if (expectedResult.InterestRealizedTransCreate == null) expectedResult.InterestRealizedTransCreate = new List<InterestRealizedTrans>();
                if (expectedResult.InterestRealizedTransUpdate == null) expectedResult.InterestRealizedTransUpdate = new List<InterestRealizedTrans>();
                if (expectedResult.VendorPaymentTrans == null) expectedResult.VendorPaymentTrans = new List<VendorPaymentTrans>();
                if (expectedResult.ProcessTrans == null) expectedResult.ProcessTrans = new List<ProcessTrans>();
                if (expectedResult.ReceiptTempPaymDetail == null) expectedResult.ReceiptTempPaymDetail = new List<ReceiptTempPaymDetail>();
                if (expectedResult.CustTransCreate == null) expectedResult.CustTransCreate = new List<CustTrans>();
                if (expectedResult.CustTransUpdate == null) expectedResult.CustTransUpdate = new List<CustTrans>();
                if (expectedResult.CreditAppTrans == null) expectedResult.CreditAppTrans = new List<CreditAppTrans>();
                if (expectedResult.TaxInvoiceTable == null) expectedResult.TaxInvoiceTable = new List<TaxInvoiceTable>();
                if (expectedResult.TaxInvoiceLine == null) expectedResult.TaxInvoiceLine = new List<TaxInvoiceLine>();
                if (expectedResult.RetentionTrans == null) expectedResult.RetentionTrans = new List<RetentionTrans>();
                if (expectedResult.ReceiptTable == null) expectedResult.ReceiptTable = new List<ReceiptTable>();
                if (expectedResult.ReceiptLine == null) expectedResult.ReceiptLine = new List<ReceiptLine>();
                if (expectedResult.PaymentHistory == null) expectedResult.PaymentHistory = new List<PaymentHistory>();
                if (expectedResult.IntercompanyInvoiceTable == null) expectedResult.IntercompanyInvoiceTable = new List<IntercompanyInvoiceTable>();

                List<PurchaseLine> expectedPurchaseLine = expectedResult.PurchaseLine.OrderBy(o => o.PurchaseTableGUID).ThenBy(b => b.LineNum).ToList();
                List<InvoiceTable> expectedInvoiceTable = expectedResult.InvoiceTable.OrderBy(o => o.InvoiceId).ToList();
                #region InvoiceLine
                List<InvoiceLine> expectedInvoiceLine =
                    (from invoiceTable in expectedInvoiceTable
                     join invoiceLine in expectedResult.InvoiceLine
                     on invoiceTable.InvoiceTableGUID equals invoiceLine.InvoiceTableGUID
                     select new { invoiceTable, invoiceLine })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .ThenBy(b => b.invoiceLine.LineNum)
                     .Select(s => s.invoiceLine).ToList();
                #endregion
                #region InvoiceSettlementDetail
                List<InvoiceSettlementDetail> expectedInvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                var newInvoiceSettlementDetailexpected =
                        (from invoiceTable in expectedInvoiceTable
                         join invSettleDetail in expectedResult.InvoiceSettlementDetail
                         on invoiceTable.InvoiceTableGUID equals invSettleDetail.InvoiceTableGUID
                         select new { invoiceTable, invSettleDetail })
                         .OrderBy(o => o.invoiceTable.InvoiceId)
                         .Select(s => s.invSettleDetail).ToList();
                expectedInvoiceSettlementDetail.AddRange(newInvoiceSettlementDetailexpected);
                var purchRefRollbillInvoiceSettlementDetailexpected =
                    expectedResult.InvoiceSettlementDetail
                        .Where(w => !newInvoiceSettlementDetailexpected.Any(a => a.InvoiceSettlementDetailGUID == w.InvoiceSettlementDetailGUID))
                        .OrderBy(o => o.InvoiceTableGUID);
                expectedInvoiceSettlementDetail.AddRange(purchRefRollbillInvoiceSettlementDetailexpected);
                Assert.True(expectedInvoiceSettlementDetail.Count == expectedResult.InvoiceSettlementDetail.Count);
                #endregion
                #region InterestRealizedTrans
                List<InterestRealizedTrans> expectedInterestRealizedTransCreate =
                    (from purchaseLine in expectedPurchaseLine
                     join interestRealizedTrans in expectedResult.InterestRealizedTransCreate
                     on purchaseLine.PurchaseLineGUID equals interestRealizedTrans.RefGUID
                     select new { purchaseLine, interestRealizedTrans })
                         .OrderBy(o => o.purchaseLine.LineNum)
                         .ThenBy(b => b.interestRealizedTrans.StartDate)
                         .Select(s => s.interestRealizedTrans).ToList();
                List<InterestRealizedTrans> expectedInterestRealizedTransUpdate =
                    expectedResult.InterestRealizedTransUpdate.OrderBy(o => o.InterestRealizedTransGUID).ToList();
                #endregion
                List<VendorPaymentTrans> expectedVendorPaymentTrans =
                    expectedResult.VendorPaymentTrans.OrderBy(o => o.TotalAmount).ThenBy(b => b.VendorTableGUID).ToList();
                #region CustTrans
                List<CustTrans> expectedCustTransCreate =
                    (from invoiceTable in expectedInvoiceTable
                     join custTrans in expectedResult.CustTransCreate
                     on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                     select new { invoiceTable, custTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.custTrans).ToList();
                List<CustTrans> expectedCustTransUpdate = expectedResult.CustTransUpdate.OrderBy(o => o.CustTransGUID).ToList();
                #endregion
                #region ProcessTrans
                List<ProcessTrans> expectedProcessTrans = new List<ProcessTrans>();
                if (expectedVendorPaymentTrans.Count > 0)
                {
                    var processTransVendorPaym =
                        (from processTrans in expectedResult.ProcessTrans.Where(w => w.ProcessTransType == (int)ProcessTransType.VendorPayment)
                         join vendorPaymTrans in expectedVendorPaymentTrans
                         on processTrans.RefGUID equals vendorPaymTrans.VendorPaymentTransGUID
                         select new { processTrans, vendorPaymTrans })
                            .OrderBy(o => o.vendorPaymTrans.TotalAmount)
                            .ThenBy(b => b.vendorPaymTrans.VendorTableGUID)
                            .Select(s => s.processTrans).ToList();
                    expectedProcessTrans.AddRange(processTransVendorPaym);
                }
                if (expectedResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Payment))
                {
                    var paymentProcessTrans = expectedResult.ProcessTrans.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment);
                    if (paymentProcessTrans.Any(a => !a.InvoiceTableGUID.HasValue))
                    {
                        expectedProcessTrans.AddRange(paymentProcessTrans);
                    }
                    else
                    {
                        var custTransPayment = expectedCustTransUpdate.Concat(expectedCustTransCreate);
                        paymentProcessTrans =
                            (from custTrans in custTransPayment
                             join processTrans in paymentProcessTrans
                             on custTrans.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                             select processTrans);
                        expectedProcessTrans.AddRange(paymentProcessTrans);
                    }
                }
                if (expectedResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Invoice))
                {
                    var invoiceProcessTrans = expectedResult.ProcessTrans.Where(a => a.ProcessTransType == (int)ProcessTransType.Invoice);
                    invoiceProcessTrans =
                        (from invoiceTable in expectedInvoiceTable
                         join processTrans in invoiceProcessTrans
                         on invoiceTable.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                         select new { invoiceTable, processTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.processTrans);
                    expectedProcessTrans.AddRange(invoiceProcessTrans);
                }
                if(expectedResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.PaymentDetail))
                {
                    var paymDetailProcessTrans = expectedResult.ProcessTrans.Where(a => a.ProcessTransType == (int)ProcessTransType.PaymentDetail)
                                                                            .OrderBy(o => o.Amount)
                                                                            .ThenBy(o => o.AmountMST)
                                                                            .ThenBy(o => o.TaxAmount)
                                                                            .ThenBy(o => o.TaxAmountMST);
                    expectedProcessTrans.AddRange(paymDetailProcessTrans);    
                }
                Assert.True(expectedProcessTrans.Count == expectedResult.ProcessTrans.Count);
                #endregion

                List<ReceiptTempPaymDetail> expectedReceiptTempPaymDetail = expectedResult.ReceiptTempPaymDetail.OrderBy(o => o.ReceiptAmount).ToList();

                #region CreditAppTrans
                List<CreditAppTrans> expectedCreditAppTrans = new List<CreditAppTrans>();
                if (expectedResult.CreditAppTrans.Any(w => w.RefType == (int)RefType.InvoiceSettlementDetail))
                {
                    var invSettleDetailCreditAppTrans =
                        expectedResult.CreditAppTrans.Where(w => w.RefType == (int)RefType.InvoiceSettlementDetail);
                    
                    var purchRefRollbillInvSettleDetailCreditAppTrans =
                        (from invsettleDetail in purchRefRollbillInvoiceSettlementDetailexpected
                         join creditAppTrans in invSettleDetailCreditAppTrans
                         on invsettleDetail.InvoiceSettlementDetailGUID equals creditAppTrans.RefGUID
                         select new { invsettleDetail, creditAppTrans })
                         .OrderBy(o => o.invsettleDetail.InvoiceTableGUID)
                         .Select(s => s.creditAppTrans);
                    expectedCreditAppTrans.AddRange(purchRefRollbillInvSettleDetailCreditAppTrans);
                    var newInvSettleDetailCreditAppTrans =
                        (from invsettleDetail in newInvoiceSettlementDetailexpected
                         join creditAppTrans in invSettleDetailCreditAppTrans
                         on invsettleDetail.InvoiceSettlementDetailGUID equals creditAppTrans.RefGUID
                         select new { invsettleDetail, creditAppTrans })
                         .Select(s => s.creditAppTrans);
                    expectedCreditAppTrans.AddRange(newInvSettleDetailCreditAppTrans);
                    Assert.True(expectedCreditAppTrans.Count == invSettleDetailCreditAppTrans.Count());
                }
                if (expectedResult.CreditAppTrans.Any(a => a.RefType != (int)RefType.InvoiceSettlementDetail))
                {
                    var invoiceCreditAppTrans = expectedResult.CreditAppTrans.Where(a => a.RefType != (int)RefType.InvoiceSettlementDetail)
                                                            .OrderBy(o => o.RefType)
                                                            .ThenBy(b => b.TransDate)
                                                            .ThenBy(b => b.InvoiceAmount)
                                                            .ThenBy(b => b.CreditDeductAmount).ToList();

                    expectedCreditAppTrans.AddRange(invoiceCreditAppTrans);
                }
                Assert.True(expectedCreditAppTrans.Count == expectedResult.CreditAppTrans.Count);
                #endregion

                List<TaxInvoiceTable> expectedTaxInvoiceTable = expectedResult.TaxInvoiceTable.OrderBy(o => o.TaxInvoiceId).ToList();

                #region TaxInvoiceLine
                List<TaxInvoiceLine> expectedTaxInvoiceLine =
                    (from taxInvoiceTable in expectedTaxInvoiceTable
                     join taxInvoiceLine in expectedResult.TaxInvoiceLine
                     on taxInvoiceTable.TaxInvoiceTableGUID equals taxInvoiceLine.TaxInvoiceTableGUID
                     select new { taxInvoiceTable, taxInvoiceLine })
                     .OrderBy(o => o.taxInvoiceTable.TaxInvoiceId)
                     .ThenBy(b => b.taxInvoiceLine.LineNum)
                     .Select(s => s.taxInvoiceLine).ToList();
                #endregion
                List<RetentionTrans> expectedRetentionTrans = expectedResult.RetentionTrans.OrderBy(o => o.RefType)
                                                                                        .ThenBy(b => b.Amount).ToList();

                List<ReceiptTable> expectedReceiptTable = expectedResult.ReceiptTable.OrderBy(o => o.ReceiptId).ToList();

                #region ReceiptLine
                List<ReceiptLine> expectedReceiptLine =
                    (from receiptTable in expectedReceiptTable
                     join receiptLine in expectedResult.ReceiptLine
                     on receiptTable.ReceiptTableGUID equals receiptLine.ReceiptTableGUID
                     select new { receiptTable, receiptLine })
                     .OrderBy(o => o.receiptTable.ReceiptId)
                     .ThenBy(b => b.receiptLine.LineNum)
                     .Select(s => s.receiptLine).ToList();
                #endregion
                #region PaymentHistory
                List<PaymentHistory> expectedPaymentHistory = new List<PaymentHistory>();
                var purchRefRollbillInvSettleDetailPaymentHistexpected =
                        (from invsettleDetail in purchRefRollbillInvoiceSettlementDetailexpected
                         join paymentHistory in expectedResult.PaymentHistory
                         on invsettleDetail.InvoiceSettlementDetailGUID equals paymentHistory.InvoiceSettlementDetailGUID
                         select new { invsettleDetail, paymentHistory })
                         .OrderBy(o => o.invsettleDetail.InvoiceTableGUID)
                         .Select(s => s.paymentHistory);
                expectedPaymentHistory.AddRange(purchRefRollbillInvSettleDetailPaymentHistexpected);
                var newInvSettleDetailPaymentHistoryexpected =
                    (from invsettleDetail in newInvoiceSettlementDetailexpected
                     join paymentHistory in expectedResult.PaymentHistory
                     on invsettleDetail.InvoiceSettlementDetailGUID equals paymentHistory.InvoiceSettlementDetailGUID
                     select new { invsettleDetail, paymentHistory })
                     .Select(s => s.paymentHistory);
                expectedPaymentHistory.AddRange(newInvSettleDetailPaymentHistoryexpected);
                Assert.True(expectedPaymentHistory.Count == expectedResult.PaymentHistory.Count());
                #endregion

                List<IntercompanyInvoiceTable> expectedIntercompanyInvoicetable = expectedResult.IntercompanyInvoiceTable.OrderBy(o => o.IntercompanyInvoiceId).ToList();
                #endregion

                PostPurchaseResultViewMap actualResult = purchaseTableService.InitPostPurchase(purchaseTableGuid);

                #region prepare actual result
                List<PurchaseLine> actualPurchaseLine = actualResult.PurchaseLine.OrderBy(o => o.PurchaseTableGUID).ThenBy(b => b.LineNum).ToList();
                List<InvoiceTable> actualInvoiceTable = actualResult.InvoiceTable.OrderBy(o => o.InvoiceId).ToList();
                #region InvoiceLine
                List<InvoiceLine> actualInvoiceLine =
                    (from invoiceTable in actualInvoiceTable
                     join invoiceLine in actualResult.InvoiceLine
                     on invoiceTable.InvoiceTableGUID equals invoiceLine.InvoiceTableGUID
                     select new { invoiceTable, invoiceLine })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .ThenBy(b => b.invoiceLine.LineNum)
                     .Select(s => s.invoiceLine).ToList();
                #endregion
                #region InvoiceSettlementDetail
                List<InvoiceSettlementDetail> actualInvoiceSettlementDetail = new List<InvoiceSettlementDetail>();
                var newInvoiceSettlementDetailActual =
                        (from invoiceTable in actualInvoiceTable
                         join invSettleDetail in actualResult.InvoiceSettlementDetail
                         on invoiceTable.InvoiceTableGUID equals invSettleDetail.InvoiceTableGUID
                         select new { invoiceTable, invSettleDetail })
                         .OrderBy(o => o.invoiceTable.InvoiceId)
                         .Select(s => s.invSettleDetail).ToList();
                actualInvoiceSettlementDetail.AddRange(newInvoiceSettlementDetailActual);
                var purchRefRollbillInvoiceSettlementDetailActual =
                    actualResult.InvoiceSettlementDetail
                        .Where(w => !newInvoiceSettlementDetailActual.Any(a => a.InvoiceSettlementDetailGUID == w.InvoiceSettlementDetailGUID))
                        .OrderBy(o => o.InvoiceTableGUID);
                actualInvoiceSettlementDetail.AddRange(purchRefRollbillInvoiceSettlementDetailActual);
                Assert.True(actualInvoiceSettlementDetail.Count == actualResult.InvoiceSettlementDetail.Count);
                #endregion
                #region InterestRealizedTrans
                List<InterestRealizedTrans> actualInterestRealizedTransCreate =
                    (from purchaseLine in actualPurchaseLine
                     join interestRealizedTrans in actualResult.InterestRealizedTransCreate
                     on purchaseLine.PurchaseLineGUID equals interestRealizedTrans.RefGUID
                     select new { purchaseLine, interestRealizedTrans })
                         .OrderBy(o => o.purchaseLine.LineNum)
                         .ThenBy(b => b.interestRealizedTrans.StartDate)
                         .Select(s => s.interestRealizedTrans).ToList();
                List<InterestRealizedTrans> actualInterestRealizedTransUpdate =
                    actualResult.InterestRealizedTransUpdate.OrderBy(o => o.InterestRealizedTransGUID).ToList();
                #endregion
                List<VendorPaymentTrans> actualVendorPaymentTrans =
                    actualResult.VendorPaymentTrans.OrderBy(o => o.TotalAmount).ThenBy(b => b.VendorTableGUID).ToList();
                #region CustTrans
                List<CustTrans> actualCustTransCreate =
                    (from invoiceTable in actualInvoiceTable
                     join custTrans in actualResult.CustTransCreate
                     on invoiceTable.InvoiceTableGUID equals custTrans.InvoiceTableGUID
                     select new { invoiceTable, custTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.custTrans).ToList();
                List<CustTrans> actualCustTransUpdate = actualResult.CustTransUpdate.OrderBy(o => o.CustTransGUID).ToList();
                #endregion
                #region ProcessTrans
                List<ProcessTrans> actualProcessTrans = new List<ProcessTrans>();
                if (actualVendorPaymentTrans.Count > 0)
                {
                    var processTransVendorPaym =
                        (from processTrans in actualResult.ProcessTrans.Where(w => w.ProcessTransType == (int)ProcessTransType.VendorPayment)
                         join vendorPaymTrans in actualVendorPaymentTrans
                         on processTrans.RefGUID equals vendorPaymTrans.VendorPaymentTransGUID
                         select new { processTrans, vendorPaymTrans })
                            .OrderBy(o => o.vendorPaymTrans.TotalAmount)
                            .ThenBy(b => b.vendorPaymTrans.VendorTableGUID)
                            .Select(s => s.processTrans).ToList();
                    actualProcessTrans.AddRange(processTransVendorPaym);
                }
                if (actualResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Payment))
                {
                    var paymentProcessTrans = actualResult.ProcessTrans.Where(w => w.ProcessTransType == (int)ProcessTransType.Payment);
                    if (paymentProcessTrans.Any(a => !a.InvoiceTableGUID.HasValue))
                    {
                        actualProcessTrans.AddRange(paymentProcessTrans);
                    }
                    else
                    {
                        var custTransPayment = actualCustTransUpdate.Concat(actualCustTransCreate);
                        paymentProcessTrans =
                            (from custTrans in custTransPayment
                             join processTrans in paymentProcessTrans
                             on custTrans.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                             select processTrans);
                        actualProcessTrans.AddRange(paymentProcessTrans);
                    }
                }
                if (actualResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.Invoice))
                {
                    var invoiceProcessTrans = actualResult.ProcessTrans.Where(a => a.ProcessTransType == (int)ProcessTransType.Invoice);
                    invoiceProcessTrans =
                        (from invoiceTable in actualInvoiceTable
                         join processTrans in invoiceProcessTrans
                         on invoiceTable.InvoiceTableGUID equals processTrans.InvoiceTableGUID
                         select new { invoiceTable, processTrans })
                     .OrderBy(o => o.invoiceTable.InvoiceId)
                     .Select(s => s.processTrans);
                    actualProcessTrans.AddRange(invoiceProcessTrans);
                }
                if (actualResult.ProcessTrans.Any(a => a.ProcessTransType == (int)ProcessTransType.PaymentDetail))
                {
                    var paymDetailProcessTrans = actualResult.ProcessTrans.Where(a => a.ProcessTransType == (int)ProcessTransType.PaymentDetail)
                                                                            .OrderBy(o => o.Amount)
                                                                            .ThenBy(o => o.AmountMST)
                                                                            .ThenBy(o => o.TaxAmount)
                                                                            .ThenBy(o => o.TaxAmountMST);
                    actualProcessTrans.AddRange(paymDetailProcessTrans);
                }
                Assert.True(actualProcessTrans.Count == actualResult.ProcessTrans.Count);
                #endregion

                List<ReceiptTempPaymDetail> actualReceiptTempPaymDetail = actualResult.ReceiptTempPaymDetail.OrderBy(o => o.ReceiptAmount).ToList();

                #region CreditAppTrans
                List<CreditAppTrans> actualCreditAppTrans = new List<CreditAppTrans>();
                if (actualResult.CreditAppTrans.Any(w => w.RefType == (int)RefType.InvoiceSettlementDetail))
                {
                    var invSettleDetailCreditAppTrans =
                        actualResult.CreditAppTrans.Where(w => w.RefType == (int)RefType.InvoiceSettlementDetail);
                    
                    var purchRefRollbillInvSettleDetailCreditAppTrans =
                        (from invsettleDetail in purchRefRollbillInvoiceSettlementDetailActual
                         join creditAppTrans in invSettleDetailCreditAppTrans
                         on invsettleDetail.InvoiceSettlementDetailGUID equals creditAppTrans.RefGUID
                         select new { invsettleDetail, creditAppTrans })
                         .OrderBy(o => o.invsettleDetail.InvoiceTableGUID)
                         .Select(s => s.creditAppTrans);
                    actualCreditAppTrans.AddRange(purchRefRollbillInvSettleDetailCreditAppTrans);
                    var newInvSettleDetailCreditAppTrans =
                        (from invsettleDetail in newInvoiceSettlementDetailActual
                         join creditAppTrans in invSettleDetailCreditAppTrans
                         on invsettleDetail.InvoiceSettlementDetailGUID equals creditAppTrans.RefGUID
                         select new { invsettleDetail, creditAppTrans })
                         .Select(s => s.creditAppTrans);
                    actualCreditAppTrans.AddRange(newInvSettleDetailCreditAppTrans);
                    Assert.True(actualCreditAppTrans.Count == invSettleDetailCreditAppTrans.Count());
                }
                if (actualResult.CreditAppTrans.Any(a => a.RefType != (int)RefType.InvoiceSettlementDetail))
                {
                    var invoiceCreditAppTrans = actualResult.CreditAppTrans.Where(a => a.RefType != (int)RefType.InvoiceSettlementDetail)
                                                            .OrderBy(o => o.RefType)
                                                            .ThenBy(b => b.TransDate)
                                                            .ThenBy(b => b.InvoiceAmount)
                                                            .ThenBy(b => b.CreditDeductAmount).ToList();

                    actualCreditAppTrans.AddRange(invoiceCreditAppTrans);
                }
                Assert.True(actualCreditAppTrans.Count == actualResult.CreditAppTrans.Count);
                #endregion

                List<TaxInvoiceTable> actualTaxInvoiceTable = actualResult.TaxInvoiceTable.OrderBy(o => o.TaxInvoiceId).ToList();

                #region TaxInvoiceLine
                List<TaxInvoiceLine> actualTaxInvoiceLine =
                    (from taxInvoiceTable in actualTaxInvoiceTable
                     join taxInvoiceLine in actualResult.TaxInvoiceLine
                     on taxInvoiceTable.TaxInvoiceTableGUID equals taxInvoiceLine.TaxInvoiceTableGUID
                     select new { taxInvoiceTable, taxInvoiceLine })
                     .OrderBy(o => o.taxInvoiceTable.TaxInvoiceId)
                     .ThenBy(b => b.taxInvoiceLine.LineNum)
                     .Select(s => s.taxInvoiceLine).ToList();
                #endregion
                List<RetentionTrans> actualRetentionTrans = actualResult.RetentionTrans.OrderBy(o => o.RefType)
                                                                                        .ThenBy(b => b.Amount).ToList();

                List<ReceiptTable> actualReceiptTable = actualResult.ReceiptTable.OrderBy(o => o.ReceiptId).ToList();

                #region ReceiptLine
                List<ReceiptLine> actualReceiptLine =
                    (from receiptTable in actualReceiptTable
                     join receiptLine in actualResult.ReceiptLine
                     on receiptTable.ReceiptTableGUID equals receiptLine.ReceiptTableGUID
                     select new { receiptTable, receiptLine })
                     .OrderBy(o => o.receiptTable.ReceiptId)
                     .ThenBy(b => b.receiptLine.LineNum)
                     .Select(s => s.receiptLine).ToList();
                #endregion
                #region PaymentHistory
                List<PaymentHistory> actualPaymentHistory = new List<PaymentHistory>();
                var purchRefRollbillInvSettleDetailPaymentHistActual =
                        (from invsettleDetail in purchRefRollbillInvoiceSettlementDetailActual
                         join paymentHistory in actualResult.PaymentHistory
                         on invsettleDetail.InvoiceSettlementDetailGUID equals paymentHistory.InvoiceSettlementDetailGUID
                         select new { invsettleDetail, paymentHistory })
                         .OrderBy(o => o.invsettleDetail.InvoiceTableGUID)
                         .Select(s => s.paymentHistory);
                actualPaymentHistory.AddRange(purchRefRollbillInvSettleDetailPaymentHistActual);
                var newInvSettleDetailPaymentHistoryActual =
                    (from invsettleDetail in newInvoiceSettlementDetailActual
                     join paymentHistory in actualResult.PaymentHistory
                     on invsettleDetail.InvoiceSettlementDetailGUID equals paymentHistory.InvoiceSettlementDetailGUID
                     select new { invsettleDetail, paymentHistory })
                     .Select(s => s.paymentHistory);
                actualPaymentHistory.AddRange(newInvSettleDetailPaymentHistoryActual);
                Assert.True(actualPaymentHistory.Count == actualResult.PaymentHistory.Count());
                #endregion
                List<IntercompanyInvoiceTable> actualIntercompanyInvoiceTable = actualResult.IntercompanyInvoiceTable.OrderBy(o => o.IntercompanyInvoiceId).ToList();
                #endregion
                #region assert should equal
                Dictionary<Type, string[]> fieldsToIgnore = new Dictionary<Type, string[]>()
                {
                    { typeof(ReceiptTempTable), new string[] { "ReceiptTempTableGUID", "ReceiptTempId" } },
                    //{ typeof(PurchaseTable), new string[] { "PurchaseTableGUID" } },
                    //{ typeof(CreditAppTable), new string[] { "CreditAppTableGUID" } },
                    { typeof(PurchaseLine), new string[] { "PurchaseLineInvoiceTableGUID" } },
                    { typeof(InvoiceTable), new string[] { "InvoiceTableGUID" } },
                    { typeof(InvoiceLine), new string[] { "InvoiceLineGUID", "InvoiceTableGUID" } },
                    { typeof(InvoiceSettlementDetail), new string[] { "InvoiceSettlementDetailGUID", "InvoiceTableGUID", "RefGUID", "RefReceiptTempPaymDetailGUID" } },
                    { typeof(InterestRealizedTrans), new string[] { "InterestRealizedTransGUID" } },
                    { typeof(VendorPaymentTrans), new string[] { "VendorPaymentTransGUID" } },
                    { typeof(ProcessTrans), new string[] { "ProcessTransGUID", "InvoiceTableGUID", "RefGUID", "PaymentHistoryGUID", "RefTaxInvoiceGUID" } },
                    { typeof(ReceiptTempPaymDetail), new string[] { "ReceiptTempPaymDetailGUID", "ReceiptTempTableGUID" } },
                    { typeof(CustTrans), new string[] { "CustTransGUID", "InvoiceTableGUID" } },
                    { typeof(CreditAppTrans), new string[] { "CreditAppTransGUID", "RefGUID" } },
                    { typeof(TaxInvoiceTable), new string[] { "TaxInvoiceTableGUID", "TaxInvoiceId", "InvoiceTableGUID", "TaxInvoiceRefGUID" } },
                    { typeof(TaxInvoiceLine), new string[] { "TaxInvoiceLineGUID", "TaxInvoiceTableGUID" } },
                    { typeof(RetentionTrans), new string[] { "RetentionTransGUID", "RefGUID" } },
                    { typeof(ReceiptTable), new string[] { "ReceiptTableGUID", "RefGUID", "InvoiceSettlementDetailGUID" } },
                    { typeof(ReceiptLine), new string[] { "ReceiptLineGUID", "RefGUID", "ReceiptTableGUID", "InvoiceTableGUID" } },
                    { typeof(PaymentHistory), new string[] { "PaymentHistoryGUID", "RefGUID", "InvoiceSettlementDetailGUID", "CustTransGUID", "ReceiptTableGUID", "InvoiceTableGUID" } },
                    { typeof(IntercompanyInvoiceTable), new string[] { "IntercompanyInvoiceTableGUID" } }
                };
                TestHelper helper = new TestHelper(fieldsToIgnore);

                helper.AssertShouldEqual(expectedResult.ReceiptTempTable, actualResult.ReceiptTempTable);
                // puchaseTable
                Assert.True(expectedResult.PurchaseTable.DocumentStatusGUID == actualResult.PurchaseTable.DocumentStatusGUID);
                Assert.True(actualResult.PurchaseTable.ReceiptTempTableGUID.HasValue);

                helper.AssertShouldEqual(expectedResult.CreditAppTable, actualResult.CreditAppTable);

                helper.AssertShouldEqual(expectedPurchaseLine, actualPurchaseLine);
                helper.AssertShouldEqual(expectedInvoiceTable, actualInvoiceTable);
                helper.AssertShouldEqual(expectedInvoiceLine, actualInvoiceLine);
                helper.AssertShouldEqual(expectedInvoiceSettlementDetail, actualInvoiceSettlementDetail);
                helper.AssertShouldEqual(expectedInterestRealizedTransCreate, actualInterestRealizedTransCreate);
                helper.AssertShouldEqual(expectedInterestRealizedTransUpdate, actualInterestRealizedTransUpdate);
                helper.AssertShouldEqual(expectedVendorPaymentTrans, actualVendorPaymentTrans);
                helper.AssertShouldEqual(expectedProcessTrans, actualProcessTrans);
                helper.AssertShouldEqual(expectedReceiptTempPaymDetail, actualReceiptTempPaymDetail);
                helper.AssertShouldEqual(expectedCustTransCreate, actualCustTransCreate);
                helper.AssertShouldEqual(expectedCustTransUpdate, actualCustTransUpdate);
                helper.AssertShouldEqual(expectedCreditAppTrans, actualCreditAppTrans);
                helper.AssertShouldEqual(expectedTaxInvoiceTable, actualTaxInvoiceTable);
                helper.AssertShouldEqual(expectedTaxInvoiceLine, actualTaxInvoiceLine);
                helper.AssertShouldEqual(expectedRetentionTrans, actualRetentionTrans);
                helper.AssertShouldEqual(expectedReceiptTable, actualReceiptTable);
                helper.AssertShouldEqual(expectedReceiptLine, actualReceiptLine);
                helper.AssertShouldEqual(expectedPaymentHistory, actualPaymentHistory);
                helper.AssertShouldEqual(expectedIntercompanyInvoicetable, actualIntercompanyInvoiceTable);
                #endregion

            }
            catch (Exception e)
            {
                throw SmartAppUtil.AddStackTrace(e).LogToTestOutput(output);
            }
        }
        public static IEnumerable<object[]> CasePostPurchase
        {
            get
            {
                return new[]
                {
                    new object[] { GetParm("Case1_Normal"), GetResult("Case1_Normal") }
                };
            }
        }
        public static Guid GetParm(string folder)
        {
            string path = "../../../Purchase/PostPurchase/" + folder + "/parameter.json";
            PostPurchaseParamView parm = TestHelper.JsonToObject<PostPurchaseParamView>(path);
            return parm.PurchaseTableGUID.StringToGuid();
        }
        public static PostPurchaseResultViewMap GetResult(string folder)
        {
            string path = "../../../Purchase/PostPurchase/" + folder + "/result.json";
            PostPurchaseResultViewMap expectedResult = TestHelper.JsonToObject<PostPurchaseResultViewMap>(path);
            return expectedResult;
        }
    }
}
